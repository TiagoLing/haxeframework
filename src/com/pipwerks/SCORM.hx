/*

pipwerks SCORM Wrapper for ActionScript 3
v1.1.20111123

Created by Philip Hutchison, January 2008
https://github.com/pipwerks/scorm-api-wrapper

Copyright (c) Philip Hutchison
MIT-style license: http://pipwerks.mit-license.org/

FLAs published using this file must be published using AS3.
SWFs will only work in Flash Player 9 or higher.

This wrapper is designed to be SCORM version-neutral (it works
with SCORM 1.2 and SCORM 2004). It also requires the pipwerks
SCORM API JavaScript wrapper in the course's HTML file. The
wrapper can be downloaded from http://github.com/pipwerks/scorm-api-wrapper/

This class uses ExternalInterface. Testing in a local environment
will FAIL unless you set your Flash Player settings to allow local
SWFs to execute ExternalInterface commands.

Use at your own risk! This class is provided as-is with no implied warranties or guarantees.

*/

/**
 * pipwerks SCORM Wrapper Haxe port
 * Created by Tiago Ling Alexandre, April 2014
 * @ Unbox Learning Experience, 2009 - 2014
 */

package com.pipwerks;

import openfl.external.ExternalInterface;

class SCORM
{
    public var debugMode(get_debugMode, set_debugMode) : Bool;
	var __connectionActive : Bool = false;
	var _debugMode : Bool = true;
	
	public function new()
    {
		var is_EI_available : Bool = ExternalInterface.available;
        var wrapperFound : Bool = false;
        var debugMsg : String = "Initializing SCORM class. Checking dependencies: ";
		
		if (is_EI_available)
		{ 
			debugMsg += "ExternalInterface.available evaluates true. ";
			wrapperFound = cast(ExternalInterface.call("pipwerks.SCORM.isAvailable"), Bool);
			
			debugMsg += "SCORM.isAvailable() evaluates " + Std.string(wrapperFound) + ". ";
			
			if (wrapperFound) 
			{
				debugMsg += "SCORM class file ready to go!  :) ";
            } else {
				debugMsg += "The required JavaScript SCORM API wrapper cannot be found in the HTML document.  Course cannot load.";
            }
        } else {
			debugMsg += "ExternalInterface is NOT available (this may be due to an outdated version of Flash Player).  Course cannot load.";
        }
		
		__displayDebugInfo(debugMsg);
    }
	
	// --- public functions --------------------------------------------- //
	function set_debugMode(status : Bool) : Bool 
	{
		this._debugMode = status;
        return status;
    }
	
	function get_debugMode() : Bool 
	{
		return this._debugMode;
    }
	
	public function connect() : Bool 
	{
		__displayDebugInfo("pipwerks.SCORM.connect() called from class file");
		return __connect();
    }
	
	public function disconnect() : Bool 
	{
		return __disconnect();
    }
	
	public function get(param : String) : String 
	{
		var str : String = __get(param);
		__displayDebugInfo("public function get returned: " + str);
		return str;
    }
	
	public function set(parameter : String, value : String) : Bool 
	{
		return __set(parameter, value);
    }
	
	public function save() : Bool 
	{
		return __save();
    }
	
	// --- functions --------------------------------------------- //
	function __connect() : Bool 
	{
		var result : Bool = false;
		
		if (!__connectionActive) 
		{
			var eiCall : String = Std.string(ExternalInterface.call("pipwerks.SCORM.init"));
			result = eiCall == "true" ? true : false;
			
			if (result)
			{
				__connectionActive = true;
            } else {
				var errorCode : Int = __getDebugCode();
				if (errorCode != 0) 
				{
					var debugInfo : String = __getDebugInfo(errorCode);
					__displayDebugInfo("pipwerks.SCORM.init() failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
                } else {
					__displayDebugInfo("pipwerks.SCORM.init failed: no response from server.");
                }
            }
        } else {
			__displayDebugInfo("pipwerks.SCORM.init aborted: connection already active.");
        }
		__displayDebugInfo("__connectionActive: " + __connectionActive);
		
		return result;
    }
	
	function __disconnect() : Bool 
	{
		var result : Bool = false;
		if (__connectionActive) 
		{
			var eiCall : String = Std.string(ExternalInterface.call("pipwerks.SCORM.quit"));
			result = eiCall == "true" ? true : false;
			if (result) 
			{
				__connectionActive = false;
            } else {
				var errorCode : Int = __getDebugCode();
				var debugInfo : String = __getDebugInfo(errorCode);
				__displayDebugInfo("pipwerks.SCORM.quit() failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
            }
        } else {
			__displayDebugInfo("pipwerks.SCORM.quit aborted: connection already inactive.");
        }
		return result;
    }
	
	function __get(parameter : String) : String
	{
		var returnedValue : String = "";
		if (__connectionActive) 
		{
			returnedValue = Std.string(ExternalInterface.call("pipwerks.SCORM.get", parameter));
			var errorCode : Int = __getDebugCode();
			//GetValue returns an empty string on errors
			//Double-check errorCode to make sure empty string
			//is really an error and not field value
			if (returnedValue == "" && errorCode != 0) 
			{
				var debugInfo : String = __getDebugInfo(errorCode);
				__displayDebugInfo("pipwerks.SCORM.get(" + parameter + ") failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
            }
        } else {
			__displayDebugInfo("pipwerks.SCORM.get(" + parameter + ") failed: connection is inactive.");
        }
		return returnedValue;
    }
	
	function __set(parameter : String, value : String) : Bool 
	{
		var result : Bool = false;
		if (__connectionActive) 
		{
			var eiCall : String = Std.string(ExternalInterface.call("pipwerks.SCORM.set", parameter, value));
			result = eiCall == "true" ? true : false;
			if (!result) 
			{
				var errorCode : Int = __getDebugCode();
				var debugInfo : String = __getDebugInfo(errorCode);
				__displayDebugInfo("pipwerks.SCORM.set(" + parameter + ") failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
            }
        } else {
			__displayDebugInfo("pipwerks.SCORM.set(" + parameter + ") failed: connection is inactive.");
        }
		return result;
    }
	
	function __save():Bool
	{
		var result : Bool = false;
		if (__connectionActive) 
		{
			var eiCall : String = Std.string(ExternalInterface.call("pipwerks.SCORM.save"));
			result = eiCall == "true" ? true : false;
			
			if (!result) 
			{
				var errorCode : Int = __getDebugCode();
				var debugInfo : String = __getDebugInfo(errorCode);
				__displayDebugInfo("pipwerks.SCORM.save() failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
            }
        } else {
			__displayDebugInfo("pipwerks.SCORM.save() failed: API connection is inactive.");
        }
		return result;
    }
	
	// --- debug functions ----------------------------------------------- //
	function __getDebugCode() : Int 
	{
		var code : Int = Std.parseInt(ExternalInterface.call("pipwerks.SCORM.debug.getCode"));
		return code;
    }
	
	function __getDebugInfo(errorCode : Int) : String 
	{
		var result : String = Std.string(ExternalInterface.call("pipwerks.SCORM.debug.getInfo", errorCode));
		return result;
    }
	
	function __getDiagnosticInfo(errorCode : Int) : String 
	{
		var result : String = Std.string(ExternalInterface.call("pipwerks.SCORM.debug.getDiagnosticInfo", errorCode));
		return result;
    }
	
	function __displayDebugInfo(msg : String) : Void 
	{
		if (_debugMode) 
		{  
			ExternalInterface.call("pipwerks.UTILS.trace", msg);
        }
    }	
}