/**
The MIT License

Copyright (c) 2008 Duncan Reid ( http://www.hy-brid.com )
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
**/
   
package com.hybrid.ui;

import motion.Actuate;
import openfl.display.*;
import openfl.errors.Error;
import openfl.events.*;
import openfl.filters.*;
import openfl.geom.*;
import openfl.text.*;
import openfl.utils.Timer;
import openfl.text.AntiAliasType;
import openfl.text.GridFitType;
import openfl.text.TextFieldAutoSize;

/**
 * Public Setters:
 *		tipWidth 				Number				Set the width of the tooltip
 *		titleFormat				TextFormat			Format for the title of the tooltip
 * 		stylesheet				StyleSheet			StyleSheet object
 *		contentFormat			TextFormat			Format for the bodycopy of the tooltip
 *		titleEmbed				Boolean				Allow font embed for the title
 *		contentEmbed			Boolean				Allow font embed for the content
 *		align					String				left, right, center
 *		showDelay				Number				Time in milliseconds to delay the display of the tooltip
 *		hideDelay				Number				Time in milliseconds to delay the hide of the tooltip
 *		hook					Boolean				Displays a hook on the bottom of the tooltip
 *		hookSize				Number				Size of the hook
 *		cornerRadius			Number				Corner radius of the tooltip, same for all 4 sides
 *		colors					Array				Array of 2 color values ( [0xXXXXXX, 0xXXXXXX] );
 *		autoSize				Boolean				Will autosize the fields and size of the tip with no wrapping or multi-line capabilities,
 *													helpful with 1 word items like "Play" or "Pause"
 * 		border					Number				Color Value: 0xFFFFFF
 *		borderSize				Number				Size Of Border
 *		buffer					Number				text buffer
 * 		bgAlpha					Number				0 - 1, transparency setting for the background of the ToolTip
 * 		padding					Number				Sets the minimum distance from the edges of the stage / screen
 *
 * Example:
   var tf:TextFormat = new TextFormat();
   tf.bold = true;
   tf.size = 12;
   tf.color = 0xff0000;
   var tt:ToolTip = new ToolTip();
   tt.hook = true;
   tt.hookSize = 20;
   tt.cornerRadius = 20;
   tt.align = "center";
   tt.titleFormat = tf;
   tt.show( DisplayObject, "Title Of This ToolTip", "Some Copy that would go below the ToolTip Title" );
 *
 *
 * @author Duncan Reid, www.hy-brid.com
 * 		Haxe version: Tiago Ling Alexandre
 * 		01/07/2014 - 13:58
 * @date October 17, 2008
 * @version 1.2
 */
class ToolTip extends Sprite
{
    public var buffer(get, set) : Float;
    public var bgAlpha(get, set) : Float;
    public var tipWidth(never, set) : Float;
    public var titleFormat(never, set) : TextFormat;
    public var contentFormat(never, set) : TextFormat;
	#if flash
    public var stylesheet(never, set) : flash.text.StyleSheet;
	#end
    public var align(never, set) : String;
    public var showDelay(never, set) : Float;
    public var hideDelay(never, set) : Float;
    public var hook(never, set) : Bool;
    public var hookSize(never, set) : Float;
    public var cornerRadius(never, set) : Float;
    public var colors(never, set) : Array<UInt>;
    public var autoSize(never, set) : Bool;
    public var border(never, set) : UInt;
    public var borderSize(never, set) : Float;
    public var tipHeight(never, set) : Float;
    public var titleEmbed(never, set) : Bool;
    public var contentEmbed(never, set) : Bool;
    public var followCursor(get, set) : Bool;
    public var padding(get, set) : Float;
	
	//objects  
	var _stage : Stage;
	var _parentObject : DisplayObject;
	var _tf : TextField;
	
	// title field  
	var _cf : TextField;
	
	//content field  
	var _contentContainer : Sprite = new Sprite();
	
	var hookSp : Sprite = new Sprite();
	var boxSp : Sprite = new Sprite();
	
	//formats  
	var _titleFormat : TextFormat;
	var _contentFormat : TextFormat;
	
	//stylesheet  
	#if flash
	var _stylesheet : flash.text.StyleSheet;
	#end
	
	/* check for stylesheet override */ 
	var _styleOverride : Bool = false;
	
	/* check for format override */
	var _titleOverride : Bool = false;
	var _contentOverride : Bool = false;
	
	// font embedding  
	var _titleEmbed : Bool = false;
	var _contentEmbed : Bool = false;
	
	//defaults  
	var _defaultWidth : Float = 200;
	var _defaultHeight : Float;
	var _buffer : Float = 10;
	var _align : String = "center";
	var _cornerRadius : Float = 12;
	var _bgColors : Array<UInt> = [0xFFFFFF, 0x9C9C9C];
	var _autoSize : Bool = false;
	var _hookEnabled : Bool = false;
	var _showDelay : Float = 0;
	
	//millilseconds  
	var _hideDelay : Float = 0;
	
	//millilseconds  
	var _hookSize : Float = 20;
	//var _border : Float;
	var _border : UInt;
	var _borderSize : Float = 1;
	var _bgAlpha : Float = 1;
	
	// transparency setting for the background of the tooltip
	var _followCursor : Bool = true;
	var _padding : Float = 0;
	
	//Distance from the edges of the stage
    //offsets
	var _offSet : Float;
	var _hookOffSet : Float;
	
	//-- hook 
	var defaultHookPos : Point = new Point();
	
	public function new()
    {
        super();  
		//do not disturb parent display object mouse events
		this.mouseEnabled = false;
		this.buttonMode = false;
		this.mouseChildren = false;
    }

	
	public function setContent(title : String, content : String = null) : Void
	{
		boxSp.graphics.clear();
		this.addCopy(title, content);
		this.setOffset();
		this.drawBG();
    }
	
	public function show(p : DisplayObject, title : String, content : String = null) : Void
	{
		//get the stage from the parent
		this._stage = p.stage;
		this._parentObject = p;
		
		// added : DR : 04.29.2010
		var onStage : Bool = this.addedToStage(this._contentContainer);
		if (!onStage) {
			this.addChild(this.boxSp);
			this.addChild(this.hookSp);
			this.addChild(this._contentContainer);
        }
		// end add
		
		this.addCopy(title, content);
		this.setOffset();
		this.drawBG();
		this.bgGlow();
		
		//initialize coordinates
		var parentCoords : Point = new Point(_parentObject.mouseX, _parentObject.mouseY);
		var globalPoint : Point = p.localToGlobal(parentCoords);
		this.x = globalPoint.x + this._offSet;
		this.y = globalPoint.y - this.height - _hookSize;
		this.alpha = 0;
		this._stage.addChild(this);
		this._parentObject.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
		this.follow(_followCursor);
		
		//-- clean any hide command
		Actuate.stop(this);
		
		Actuate.timer(0.05).onComplete(animate, [true]);
    }
	
	public function hide() : Void 
	{
		this._parentObject.removeEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
		
		Actuate.timer(0.05).onComplete(animate, [false]);
    }
	
	function onMouseOut(event : MouseEvent) : Void
	{
		event.currentTarget.removeEventListener(event.type, this.onMouseOut);
		Actuate.timer(0.05).onComplete(animate, [false]);
	}
	
	function follow(value : Bool) : Void
	{
		if (value) 
		{
			addEventListener(Event.ENTER_FRAME, this.eof);
		} else {
			removeEventListener(Event.ENTER_FRAME, this.eof);
		}
	}
	
	function eof(event : Event) : Void
	{
		this.position();
	}
	
	function position() : Void
	{
		var speed : Float = .5;
		var parentCoords : Point = new Point(_parentObject.mouseX, _parentObject.mouseY);
		var globalPoint : Point = _parentObject.localToGlobal(parentCoords);
		var xp : Float = globalPoint.x + this._offSet;
		var yp : Float = globalPoint.y - this.height - _buffer;
		
		var overhangRight : Float = xp + width;
		if (overhangRight > stage.stageWidth) 
		{
			xp = stage.stageWidth - this.width - _padding;
		}
		
		if (xp < 0) 
		{
			xp = _padding;
		}
		
		if (yp < 0) 
		{
			yp = globalPoint.y + _hookSize + _buffer;
		}
		
		if (_hookEnabled) 
		{
			//-- check hook Position
			var mouseLocalX : Float = this.mouseX - (_hookSize / 2);
			var hookX : Float = _hookOffSet - (_hookSize / 2);
			var hookY : Float = 1;
			var hookXMin : Float = _buffer;
			var hookXMax : Float = _defaultWidth - _hookSize - _buffer;
			
			if (overhangRight > stage.stageWidth)
				hookX = ((mouseLocalX < hookXMax)) ? mouseLocalX : hookXMax;
				
			if (globalPoint.x + this._offSet < 0)
				hookX = ((mouseLocalX < hookXMin)) ? hookXMin : mouseLocalX;
				
			if (globalPoint.y - this.height - _buffer < 0) 
			{
				hookY = 1;
				hookSp.scaleY = -1;
			} else {
				hookY = defaultHookPos.y;
				hookSp.scaleY = 1;
			}
			
			hookSp.y = hookY;
			hookSp.x = hookX;
		}
		
		this.x += (xp - this.x) * speed;
		this.y += (yp - this.y) * speed;
	}


	function addCopy(title : String, content : String = null) : Void
	{
		if (this._tf == null) 
		{
			this._tf = this.createField(this._titleEmbed);
		}
		
		#if flash
		// if using a stylesheet for title field  
		if (this._styleOverride) 
		{
			this._tf.styleSheet = this._stylesheet;
		}
		#end
		
		this._tf.htmlText = title;
		
		// if not using a stylesheet  
		if (!this._styleOverride) 
		{
			// if format has not been set, set default  
			if (!this._titleOverride) 
			{
				this.initTitleFormat();
			}
			
			this._tf.setTextFormat(this._titleFormat);
		}
		
		if (this._autoSize) 
		{
			this._defaultWidth = this._tf.textWidth + 4 + (_buffer * 2);
		} else {
			_tf.multiline = true;
			_tf.wordWrap = true;
			this._tf.width = this._defaultWidth - (_buffer * 2);
		}
		
		this._tf.x = this._tf.y = this._buffer;
		this._contentContainer.addChild(this._tf);
		
		//if using content
		if (content != null) 
		{
			if (this._cf == null) 
			{
				this._cf = this.createField(this._contentEmbed);
			}
			
			#if flash
			// if using a stylesheet for title field
			if (this._styleOverride) 
			{
				this._cf.styleSheet = this._stylesheet;
			}
			#end
			
			this._cf.htmlText = content;
			
			// if not using a stylesheet
			if (!this._styleOverride) 
			{
				// if format has not been set, set default
				if (!this._contentOverride) 
				{
					this.initContentFormat();
				}
				
				this._cf.setTextFormat(this._contentFormat);
			}
			
			var bounds : Rectangle = this.getBounds(this);
			this._cf.x = this._buffer;
			this._cf.y = this._tf.y + this._tf.textHeight;
			
			if (this._autoSize) 
			{
				var cfWidth : Float = this._cf.textWidth + 4 + (_buffer * 2);
				this._defaultWidth = cfWidth > (this._defaultWidth) ? cfWidth : this._defaultWidth;
			} else {
				_cf.multiline = true;
				_cf.wordWrap = true;
				this._cf.width = this._defaultWidth - (_buffer * 2);
			}
			this._contentContainer.addChild(this._cf);
		}
	}
	
	//create field
	function createField(embed : Bool) : TextField
	{
		var tf : TextField = new TextField();
		tf.embedFonts = embed;
		tf.antiAliasType = AntiAliasType.ADVANCED;
		tf.gridFitType = GridFitType.PIXEL;
		tf.autoSize = TextFieldAutoSize.LEFT;
		tf.selectable = false;
		
		if (!this._autoSize) 
		{
			tf.multiline = true;
			tf.wordWrap = true;
		}
		
		return tf;
	}
	
	//draw background, use drawing api if we need a hook  
	function drawBG() : Void
	{
		boxSp.graphics.clear();
		hookSp.graphics.clear();
		
		var bounds : Rectangle = this.getBounds(this);
		var h : Float = (Math.isNaN(this._defaultHeight)) ? bounds.height + (this._buffer * 2) : this._defaultHeight;
		var fillType : GradientType = GradientType.LINEAR;
		var alphas : Array<Dynamic> = [this._bgAlpha, this._bgAlpha];
		var ratios : Array<Dynamic> = [0x00, 0xFF];
		var matr : Matrix = new Matrix();
		var radians : Float = 90 * Math.PI / 180;
		matr.createGradientBox(this._defaultWidth, h, radians, 0, 0);
		var spreadMethod : SpreadMethod = SpreadMethod.PAD;
	
/*		if (!Math.isNaN(this._border)) 
		{
			boxSp.graphics.lineStyle(_borderSize, _border, 1);
		}*/
		
		boxSp.graphics.beginGradientFill(fillType, this._bgColors, alphas, ratios, matr, spreadMethod);
		boxSp.graphics.drawRoundRect(0, 0, this._defaultWidth, h, this._cornerRadius);
		
		if (this._hookEnabled) 
		{
			var xp : Float = 0;
			var yp : Float = 0;
			hookSp.x = _hookOffSet - (_hookSize / 2);
			hookSp.y = h - 1;
			defaultHookPos = new Point(hookSp.x, hookSp.y);
			hookSp.graphics.beginFill(_bgColors[1], alphas[1]);
			hookSp.graphics.moveTo(0, 0);
			hookSp.graphics.lineTo(_hookSize, 0);
			hookSp.graphics.lineTo(_hookSize / 2, _hookSize / 2);
			hookSp.graphics.lineTo(0, 0);
			hookSp.graphics.endFill();
		}
	}
	
	function animate(show : Bool) : Void
	{
		if (show) 
		{
			Actuate.tween(this, .5, { alpha:1 } );
		} else {
			Actuate.tween(this, .2, { alpha:0 }).onComplete(onComplete);
		}
	}
	
	function onComplete() : Void
	{
		this.cleanUp();
	}
  
	function set_buffer(value : Float) : Float
	{
		this._buffer = value;
		return value;
	}
	
	function get_buffer() : Float
	{
		return this._buffer;
	}
	
	function set_bgAlpha(value : Float) : Float
	{
		this._bgAlpha = value;
		return value;
	}
	
	function get_bgAlpha() : Float
	{
		return this._bgAlpha;
	}
	
	function set_tipWidth(value : Float) : Float
	{
		this._defaultWidth = value;
		return value;
	}
	
	function set_titleFormat(tf : TextFormat) : TextFormat
	{
		this._titleFormat = tf;
		if (this._titleFormat.font == null) 
		{
			this._titleFormat.font = "_sans";
		}
		
		this._titleOverride = true;
		return tf;
	}
	
	function set_contentFormat(tf : TextFormat) : TextFormat
	{
		this._contentFormat = tf;
		if (this._contentFormat.font == null) 
		{
			this._contentFormat.font = "_sans";
		}
		
		this._contentOverride = true;
		return tf;
	}
	
	#if flash
	function set_stylesheet(ts : flash.text.StyleSheet) : flash.text.StyleSheet
	{
		this._stylesheet = ts;
		this._styleOverride = true;
		return ts;
	}
	#end
	
	function set_align(value : String) : String
	{
		var a : String = value.toLowerCase();
		var values : String = "right left center";
		if (values.indexOf(value) == -1) 
		{
			throw new Error(this + " : Invalid Align Property, options are: 'right', 'left' & 'center'");
		} else {
			this._align = a;
		}
		return value;
	}
	
	function set_showDelay(value : Float) : Float
	{
		this._showDelay = value;
		return value;
	}
	
	function set_hideDelay(value : Float) : Float
	{
		this._hideDelay = value;
		return value;
	}
	
	function set_hook(value : Bool) : Bool
	{
		this._hookEnabled = value;
		return value;
	}
	
	function set_hookSize(value : Float) : Float
	{
		this._hookSize = value;
		return value;
	}
	
	function set_cornerRadius(value : Float) : Float
	{
		this._cornerRadius = value;
		return value;
	}
	
	function set_colors(colArray : Array<UInt>) : Array<UInt>
	{
		this._bgColors = colArray;
		return colArray;
	}
	
	function set_autoSize(value : Bool) : Bool
	{
		this._autoSize = value;
		return value;
	}
	
	function set_border(value : UInt) : UInt
	{
		this._border = value;
		return value;
	}
	
	function set_borderSize(value : Float) : Float
	{
		this._borderSize = value;
		return value;
	}
	
	function set_tipHeight(value : Float) : Float
	{
		this._defaultHeight = value;
		return value;
	}
	
	function set_titleEmbed(value : Bool) : Bool
	{
		this._titleEmbed = value;
		return value;
	}
	
	function set_contentEmbed(value : Bool) : Bool
	{
		this._contentEmbed = value;
		return value;
	}
	
	function get_followCursor() : Bool
	{
		return _followCursor;
	}
	
	function set_followCursor(value : Bool) : Bool
	{
		_followCursor = value;
		return value;
	}
	
	function get_padding() : Float
	{
		return _padding;
	}
	
	function set_padding(value : Float) : Float
	{
		_padding = value;
		return value;
	}
  
	function textGlow(field : TextField) : Void
	{
		var color : UInt = 0x000000;
		var alpha : Float = 0.35;
		var blurX : Float = 2;
		var blurY : Float = 2;
		var strength : Float = 1;
		var inner : Bool = false;
		var knockout : Bool = false;
		var quality : Int = BitmapFilterQuality.HIGH;
		var filter : GlowFilter = new GlowFilter(color, alpha, blurX, blurY, strength, quality, inner, knockout);
		var myFilters : Array<BitmapFilter> = new Array<BitmapFilter>();
		myFilters.push(filter);
		field.filters = myFilters;
	}
	
	function bgGlow() : Void
	{
		var color : UInt = 0x000000;
		var alpha : Float = 0.60;
		var blurX : Float = 5;
		var blurY : Float = 5;
		var strength : Float = 1;
		var inner : Bool = false;
		var knockout : Bool = false;
		var quality : Int = BitmapFilterQuality.HIGH;
		var filter : GlowFilter = new GlowFilter(color, alpha, blurX, blurY, strength, quality, inner, knockout);
		var myFilters : Array<BitmapFilter> = new Array<BitmapFilter>();
		myFilters.push(filter);
		filters = myFilters;
	}
	
	function initTitleFormat() : Void
	{
		_titleFormat = new TextFormat();
		_titleFormat.font = "_sans";
		_titleFormat.bold = true;
		_titleFormat.size = 20;
		_titleFormat.color = 0x333333;
	}
	
	function initContentFormat() : Void
	{
		_contentFormat = new TextFormat();
		_contentFormat.font = "_sans";
		_contentFormat.bold = false;
		_contentFormat.size = 14;
		_contentFormat.color = 0x333333;
	}
    
	function addedToStage(displayObject : DisplayObject) : Bool
	{
		var hasStage : Stage = displayObject.stage;
		return hasStage == (null) ? false : true;
	}

	function setOffset() : Void
	{
		var _sw0_ = (this._align);
        switch (_sw0_)
        {
			case "left":
				this._offSet = -_defaultWidth + _buffer + this._hookSize;
				this._hookOffSet = this._defaultWidth - _buffer - this._hookSize;
			case "right":
				this._offSet = _buffer - this._hookSize;
				this._hookOffSet = _buffer + this._hookSize;
			case "center":
				this._offSet = -(_defaultWidth / 2);
				this._hookOffSet = (_defaultWidth / 2);
			default:
				this._offSet = -(_defaultWidth / 2);
				this._hookOffSet = (_defaultWidth / 2);
        }
    }
	
	function cleanUp() : Void
	{
		this._parentObject.removeEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
		this.follow(false);
		this._tf.filters = [];
		this.filters = [];
		this._contentContainer.removeChild(this._tf);
		this._tf = null;
		
		if (this._cf != null)
		{
			this._cf.filters = [];
			this._contentContainer.removeChild(this._cf);
        }
		
		boxSp.graphics.clear();
		removeChild(this._contentContainer);
		parent.removeChild(this);
		
		if (_hookEnabled && hookSp != null)
		{
			hookSp.graphics.clear();
			removeChild(this.hookSp);
        }
    }
}
