package unbox.framework.components;
import haxe.xml.Fast;
import motion.Actuate;
import msignal.Signal.Signal1;
import unbox.framework.utils.ArrayUtils;
import unbox.framework.view.CoursePage;

/**
 * Creates a sequence of exercises (components)
 * and register their results in the LMS.
 * @author Tiago Ling Alexandre
 */
class Assessment
{
	var parent:CoursePage;
	var data:Fast;
	#if flash
	var style:flash.text.StyleSheet;
	#end
	//General Params
	var currentExerciseId:Int;
	var totalExercises:Int;
	var maxTries:Int;
	var minScore:Int;
	var scorePerExercise:Int;
	var userScore:Int;
	var numberOfQuestions:Int;
	var randomizeExercises:Bool;
	var keepHigherScore:Bool;
	
	var exerciseSequence:Array<Int>;
	var currentExercise:IExercise;
	
	//Signal - Getters only
	var _onExerciseChanged:Signal1<Int>;
	var _onComplete:Signal1<Int>;
	
	#if flash
	public function new(parent:CoursePage, data:Fast, style:flash.text.StyleSheet) 
	#else
	public function new(parent:CoursePage, data:Fast)
	#end
	{
		this.parent = parent;
		this.data = data;
		#if flash
		this.style = style;
		#end
		
		_onExerciseChanged = new Signal1<Int>();
		_onComplete = new Signal1<Int>();
	}
	
	public function init():Void
	{
		currentExerciseId = 0;
		userScore = 0;
		totalExercises = data.node.exercises.nodes.content.length;
		maxTries = Std.parseInt(data.node.config.att.maxTries);
		minScore = Std.parseInt(data.node.config.att.minScore);
		scorePerExercise = Std.parseInt(data.node.config.att.scorePerExercise);
		numberOfQuestions = Std.parseInt(data.node.config.att.numberOfQuestions);
		trace( "numberOfQuestions : " + numberOfQuestions );
		randomizeExercises = data.node.config.att.randomizeExercises == "true" ? true : false;
		keepHigherScore = data.node.config.att.keepHigherScore == "true" ? true : false;
		
		exerciseSequence = new Array<Int>();
		for (i in 0...totalExercises)
		{
			exerciseSequence.push(i);
		}
		
		var randomSequence:Array<Int> = ArrayUtils.sortNonRepeatSeq(0, exerciseSequence.length, exerciseSequence.length + 1);
		randomizeExercises == true ? exerciseSequence = randomSequence : false;
		
		//TODO: Set currentModule and currentPage to the id of introPage (set inside XML) (NavController)
		//TODO: Set minScore & maxScore (DataController)
		//TODO: Save the course (DataController)
		
		parent.context.dataController.ebookData.scoreMax = 100;
		parent.context.dataController.status.currentPage = parent.data.globalIndex - 1; //Set to intro page
		parent.context.dataController.save();
		
		loadExercise();
	}
	
	function loadExercise():Void
	{
		var exerciseData:Fast = null;
		var count:Int = 0;
		for (data in data.node.exercises.nodes.content)
		{
			if (count == exerciseSequence[currentExerciseId])
			{
				exerciseData = data;
			}
			count++;
		}
		
		var exerciseType:String = exerciseData.att.type;
		
		if (currentExercise != null)
		{
			parent.view.removeChild(currentExercise.view);
			currentExercise = null;
		}
		
		//Instantiate class
		var classRef = Type.resolveClass(exerciseType);
		#if flash
		var exercise:IExercise = Type.createInstance(classRef, [parent, exerciseData, style]);
		#else
		var exercise:IExercise = Type.createInstance(classRef, [parent, exerciseData]);
		#end
		
		exercise.onComplete.add(onExerciseCompleted);
		exercise.exerciseId = currentExerciseId + 1;
		exercise.init();
		exercise.view.alpha = 0;
		exercise.view.name = "exercise_" + currentExerciseId;
		Actuate.tween(exercise.view, 0.5, { alpha:1 });
		currentExercise = exercise;
		
		//TODO: Enable next button
		parent.context.navController.setNavButtons("000");
		parent.context.navController.onBeforeNextPage = null;
		
		//Dispatch onExerciseChanged
		onExerciseChanged.dispatch(currentExerciseId + 1);
	}
	
	function onExerciseCompleted(points:Int):Void
	{
		trace( "Assessment.onExerciseCompleted > points : " + points );
		points = points < currentExercise.maxPoints ? 0 : 1;
		userScore += points;
		
		if (currentExerciseId < numberOfQuestions - 1)
		{
			currentExerciseId++;
			
			trace("Next exercise ID : " + currentExerciseId);
			
			//TODO: Hijack the NavBar.nextButton to call 'disposeOfCurrentExercise'
			parent.context.navController.onBeforeNextPage = disposeOfCurrentExercise;
		} else {
			
			trace("This was the last question!");
			
			//Release NavBar.nextButton (set it to null)
			parent.context.navController.onBeforeNextPage = null;
			
			var scoreMultiplier = Std.int(100 / numberOfQuestions);
			var newScore = userScore == numberOfQuestions ? 100 : userScore * scoreMultiplier;
			
			if (keepHigherScore)
			{
				//Keep the higher score (Status.quizScore)
				//Must compare to the old score, must get it from status somehow
				parent.context.dataController.status.quizScore = parent.context.dataController.status.quizScore > newScore ? parent.context.dataController.status.quizScore : newScore;
			} else {
				//TODO: Set the new score, regardless of being lower (Status.quizScore)
				parent.context.dataController.status.quizScore = newScore;
			}
			
			//Increment Status.quizTries
			parent.context.dataController.status.quizTries++;
			
			//Return the module and page to its current settings and save
			parent.context.dataController.status.currentPage = parent.data.globalIndex;
		}
		
		//Enable next button or dispatch onComplete
		//helpBtnBtn, menuBtn, backBtn, bookmarkBtn, nextBtn
		parent.context.navController.setNavButtons("00001");
	}
	
	function disposeOfCurrentExercise():Void
	{
		Actuate.tween(currentExercise.view, 0.5, { alpha:0 } ).onComplete(loadExercise);
	}
	
	function get_onExerciseChanged():Signal1<Int>
	{
		return _onExerciseChanged;
	}
	
	function set_onExerciseChanged(value:Signal1<Int>):Signal1<Int> 
	{
		return _onExerciseChanged = value;
	}
	
	public var onExerciseChanged(get_onExerciseChanged, set_onExerciseChanged):Signal1<Int>;
	
	function get_onComplete():Signal1<Int> 
	{
		return _onComplete;
	}
	
	function set_onComplete(value:Signal1<Int>):Signal1<Int> 
	{
		return _onComplete = value;
	}
	
	public var onComplete(get_onComplete, set_onComplete):Signal1<Int>;
}