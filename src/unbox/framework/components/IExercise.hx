package unbox.framework.components;
import msignal.Signal.Signal1;
import openfl.display.Sprite;

/**
 * Common interface shared between all components
 * that can be used in an assessment.
 * @author Tiago Ling Alexandre
 */
interface IExercise 
{
	public var view(get_view, set_view):Sprite;
	public var onComplete(get_onComplete, set_onComplete):Signal1<Int>;
	public var maxPoints(get_maxPoints, set_maxPoints):Int;
	public var exerciseId(get_exerciseId, set_exerciseId):Int;
	
	public function init():Void;
}