package unbox.framework.components;

import haxe.xml.Fast;
import msignal.Signal.Signal1;
import openfl.display.Sprite;
import openfl.text.AntiAliasType;
import openfl.text.GridFitType;
import openfl.text.TextField;
import unbox.framework.utils.ContentParser;
import unbox.framework.utils.StringUtils;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.StyleButton;
import unbox.framework.view.CoursePage;

/**
 * Basic Quiz with keyboard navigation. Currently
 * handles only single alternative type questions.
 * @author Tiago Ling Alexandre
 */
class Quiz implements IExercise
{
	var data:Fast;
	var questions:Array<Fast>;
	var parent:CoursePage;
	#if flash
	var stylesheet:flash.text.StyleSheet;
	#end
	
	var _view:Sprite;
	var _onComplete:Signal1<Int>;
	var _maxPoints:Int;
	var _exerciseId:Int;
	public var exId:Int;
	
	var questionTxt:TextField;
	var options:Array<StyleButton>;
	var selectedOption:Int;
	var submitBtn:GenericButton;
	
	var correctAnswer:Int;
	var tickers:Array<Sprite>;
	var tWrong:Sprite;
	var tRight:Sprite;
	var tAnswer:Sprite;
	
	#if flash
	public function new(parent:CoursePage, data:Fast, style:flash.text.StyleSheet)
	#else
	public function new(parent:CoursePage, data:Fast)
	#end
	{
		this.data = data;
		this.parent = parent;
		#if flash
		this.stylesheet = style;
		#end
		
		trace("view : " + data.node.config.node.view.att.classDef);
		//TODO: Make alternative for HTML5 version (put the compiler conditional just to shut the compiler up)
		#if flash
		var classRef = parent.appDomain.getDefinition(data.node.config.node.view.att.classDef);
		_view = Type.createInstance(classRef, []);
		
		tWrong = Type.createInstance(parent.appDomain.getDefinition("view.TickerWrongSymbol"), []);
		tWrong.tabEnabled = tWrong.tabChildren = false;
		tRight = Type.createInstance(parent.appDomain.getDefinition("view.TickerCorrectSymbol"), []);
		tRight.tabEnabled = tRight.tabChildren = false;
		tAnswer = Type.createInstance(parent.appDomain.getDefinition("view.TickerAnswerSymbol"), []);
		tAnswer.tabEnabled = tAnswer.tabChildren = false;
		#else
		_view = new Sprite();
		#end
		
		_onComplete = new Signal1<Int>();
		_maxPoints = 1;
	}
	
	public function init():Void
	{
		_view.x = Std.parseFloat(data.node.config.node.view.att.x);
		_view.y = Std.parseFloat(data.node.config.node.view.att.y);
		
		exId = _exerciseId;
		
		//TODO: Use ContentParser instead of setting everything manually for the questionTxt
		questionTxt = cast(_view.getChildByName("question_txt"), TextField);
		#if flash
		questionTxt.styleSheet = stylesheet;
		questionTxt.mouseWheelEnabled = false;
		#end
		questionTxt.embedFonts = true;
		questionTxt.antiAliasType = AntiAliasType.ADVANCED;
		questionTxt.gridFitType = GridFitType.SUBPIXEL;
		
		var strippedText:String = data.node.question.node.content.innerHTML;
		strippedText = StringTools.replace(strippedText, "<![CDATA[", "");
		strippedText = StringTools.replace(strippedText, "]]>", "");
		strippedText = StringTools.trim(strippedText);
		strippedText = StringUtils.parseTextVars(strippedText, this);
		questionTxt.htmlText = strippedText;
		
		#if flash
		var accProps:flash.accessibility.AccessibilityProperties = new flash.accessibility.AccessibilityProperties();
		accProps.name = questionTxt.text;
		questionTxt.accessibilityProperties = accProps;
		#end
		
		correctAnswer = Std.parseInt(data.node.config.node.alternatives.att.correct);
		trace( "correctAnswer : " + correctAnswer );
		
		var count:Int = 0;
		options = new Array<StyleButton>();
		tickers = new Array<Sprite>();
		selectedOption = -1;
		for (option in data.node.options.nodes.button)
		{
			var btnView:Sprite = cast(_view.getChildByName("option_" + count), Sprite);
			var button:StyleButton = new StyleButton(btnView.x, btnView.y, btnView, "option_" + count);
			_view.removeChild(btnView);
			
			button.setClasses("bOver", "bDown", "bUp", "bSelected");
			button.label = ContentParser.parseTextField(option, button.view);
			button.add(_view);
			button.addHandlers(handleOptionDown);
			options.push(button);
			
			#if flash
			var accProps:flash.accessibility.AccessibilityProperties = new flash.accessibility.AccessibilityProperties();
			accProps.name = "Não selecionada. " + button.label.accessibilityProperties.name + ". " + button.label.text;
			accProps.silent = false;
			accProps.noAutoLabeling = true;
			accProps.forceSimple = true;
			button.view.accessibilityProperties = accProps;
			#end
			
			var ticker = new Sprite();
			ticker.name = "ticker";
			ticker.x = btnView.x - 30;
			ticker.y = btnView.y;
			_view.addChild(ticker);
			tickers.push(ticker);
			ticker.mouseEnabled = ticker.mouseChildren = false;
			//tabEnabled does not exist in Neko and CPP
			#if flash
			ticker.tabEnabled = ticker.tabChildren = false;
			#end
			count++;
		}
		
		var submitView = cast(_view.getChildByName("btCheck"), Sprite);
		submitBtn = new GenericButton(submitView.x, submitView.y, submitView, "submitBtn");
		_view.removeChild(submitView);
		
		submitBtn.label = ContentParser.parseTextField(data.node.submitButton, submitBtn.view);
		submitBtn.add(_view);
		submitBtn.addHandlers(handleSubmitDown);
		submitBtn.setEnabled(false);
		
		_view.tabChildren = true;
		
		parent.view.addChild(_view);
	}
	
	function handleOptionDown(e:Dynamic):Void
	{
		#if flash
		parent.context.accessibilityController.jsTrace("Quiz.handleOptionDown -> '" + e.name + "' pressed! previous selectedOption : '" + selectedOption + "'");
		#end
		
		if (selectedOption == Std.parseInt(e.name.split("_")[1]))
		{
			options[selectedOption].setSelected(false);
			#if flash
			options[selectedOption].view.accessibilityProperties.name = StringTools.replace(options[selectedOption].view.accessibilityProperties.name, "Selecionada. ", "");
			options[selectedOption].view.accessibilityProperties.name = "Não selecionada. " + options[selectedOption].view.accessibilityProperties.name;
			parent.context.accessibilityController.say(options[selectedOption].view, options[selectedOption].view.accessibilityProperties.name);
			#end
			selectedOption = -1;
		} else {
			if (selectedOption > -1) options[selectedOption].setSelected(false);
			selectedOption = Std.parseInt(e.name.split("_")[1]);
			options[selectedOption].setSelected(true);
			#if flash
			options[selectedOption].view.accessibilityProperties.name = StringTools.replace(options[selectedOption].view.accessibilityProperties.name, "Não selecionada. ", "");
			options[selectedOption].view.accessibilityProperties.name = "Selecionada. " + options[selectedOption].view.accessibilityProperties.name;
			parent.context.accessibilityController.say(options[selectedOption].view, options[selectedOption].view.accessibilityProperties.name);
			#end
		}
		
		selectedOption == -1 ? submitBtn.setEnabled(false) : submitBtn.setEnabled(true);
	}
	
	function handleSubmitDown(e:Dynamic):Void
	{
		trace("Submit button down! Selected opt : " + selectedOption + " | Correct opt : " + correctAnswer);
		var isCorrect:Bool = selectedOption == correctAnswer;
		var points:Int = 0;
		if (isCorrect)
		{
			//TODO: Show correct ticker
			tickers[selectedOption].addChild(tRight);
			points++;
			trace("Correct answer!");
			#if flash
			parent.context.accessibilityController.say(submitBtn.view, "Resposta " + (selectedOption + 1) + " correta!");
			#end
		} else {
			//TODO: Put wrong ticker on selected. Put indication on correct answer
			tickers[selectedOption].addChild(tWrong);
			tickers[correctAnswer].addChild(tAnswer);
			trace("Incorrect answer!");
			#if flash
			parent.context.accessibilityController.say(submitBtn.view, "Resposta " + (selectedOption + 1) + " incorreta. A alternativa correta é a de número " + (correctAnswer + 1) + ".");
			#end
		}
		
		for (btn in options)
		{
			btn.setEnabled(false);
		}
		
		submitBtn.setEnabled(false);
		_onComplete.dispatch(points);
	}
	
	function get_view():Sprite 
	{
		return _view;
	}
	
	function set_view(value:Sprite):Sprite
	{
		return _view = value;
	}
	
	public var view(get_view, set_view):Sprite;
	
	function get_onComplete():Signal1<Int> 
	{
		return _onComplete;
	}
	
	function set_onComplete(value:Signal1<Int>):Signal1<Int> 
	{
		return _onComplete = value;
	}
	
	public var onComplete(get_onComplete, set_onComplete):Signal1<Int>;
	
	function get_maxPoints():Int 
	{
		return _maxPoints;
	}
	
	function set_maxPoints(value:Int):Int 
	{
		return _maxPoints = value;
	}
	
	public var maxPoints(get_maxPoints, set_maxPoints):Int;
	
	function get_exerciseId():Int 
	{
		return _exerciseId;
	}
	
	function set_exerciseId(value:Int):Int 
	{
		return _exerciseId = value;
	}
	
	public var exerciseId(get_exerciseId, set_exerciseId):Int;
}