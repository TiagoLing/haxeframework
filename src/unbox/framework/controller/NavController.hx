package unbox.framework.controller;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import openfl.display.Sprite;
import unbox.framework.model.vo.PageData;
import unbox.framework.view.button.IButton;

/**
 * Handles course navigation
 * @author Tiago Ling Alexandre
 */
class NavController
{
	var pages:Array<PageData>;
	var navLayer:Sprite;
	
	var _lastPage:Int;
	var currentPageLocalID:Int;
	var _currentPageGlobalID:Int;
	var currentModuleID:Int;
	
	var _buttons:Array<IButton>;
	var _onChange:Signal2<Int, Int>;
	var _onChangeNavStatus:Signal1<String>;
	
	public var onBeforeNextPage:Void->Void;
	public var onBeforePreviousPage:Void->Void;
	
	public function new(pages:Array<PageData>, navLayer:Sprite) 
	{
		this.pages = pages;
		trace("Page length : " + pages.length);
		this.navLayer = navLayer;
		currentPageLocalID = 0;
		_currentPageGlobalID = 0;
		currentModuleID = 0;
		_buttons = new Array<IButton>();
		_onChange = new Signal2<Int, Int>();
		_onChangeNavStatus = new Signal1<String>();
	}
	
	public function handleNavInput(id:Int):Void
	{
		trace( "NavController.handleNavInput > id : " + id );
		switch (id)
		{
			case -2:
				trace("Button functionality not implemented!");
			case -1:
				gotoPreviousPage();
			case 0:
				gotoMenu();
			case 1:
				gotoNextPage();
			default:
				id == _currentPageGlobalID ? return : gotoPage(id);
		}
	}
	
	public function gotoNextPage():Void
	{
		if (onBeforeNextPage != null)
		{
			onBeforeNextPage();
		} else {
			if (_currentPageGlobalID < pages.length - 1)
			{
				_lastPage = _currentPageGlobalID;
				_currentPageGlobalID++;
				currentModuleID = pages[_currentPageGlobalID].moduleIndex;
				_onChange.dispatch(_currentPageGlobalID, 1);
			}
		}
	}
	
	public function gotoPreviousPage():Void
	{
		if (onBeforePreviousPage != null)
		{
			onBeforePreviousPage();
		} else {
			//_currentPageGlobalID == 0 ->> Cover Page, can only be seen once
			if (_currentPageGlobalID > 0)
			{
				_lastPage = _currentPageGlobalID;
				_currentPageGlobalID--;
				currentModuleID = pages[_currentPageGlobalID].moduleIndex;
				_onChange.dispatch(_currentPageGlobalID, -1);
			}
		}
	}
	
	public function gotoPage(id:Int):Void
	{
		if (id > 0 && id <= pages.length - 1)
		{
			_lastPage = _currentPageGlobalID;
			_currentPageGlobalID = id;
			var direction:Int = _currentPageGlobalID < _lastPage ? -1 : 1;
			_onChange.dispatch(_currentPageGlobalID, direction);
		} else {
			trace("Page id '" + id + "'does not exist!");
		}
	}
	
	// ### Used in Accessibility Mode only ###
	public function gotoMenu():Void
	{
		trace( "NavController.gotoMenu" );
		if (_currentPageGlobalID != 1)
		{
			_lastPage = _currentPageGlobalID;
			_currentPageGlobalID = 1;
			_onChange.dispatch(_currentPageGlobalID, -1);
		}
	}
	// ###
	
	//TODO: Remove the loop and handle this better
	public function handlePanelRequest(page:String):Void
	{
		trace( "NavController.handlePanelRequest > page : " + page );
		for (i in 0...pages.length)
		{
			var currentPageSrc = pages[i].src.split("/")[2].split(".")[0];
			if (currentPageSrc == page)
			{
				trace("page '" + page + "' found! Global index : " + pages[i].globalIndex);
				gotoPage(pages[i].globalIndex);
			}
		}
	}
	
	/**
	 * Sends a signal to the NavBar class to set its
	 * buttons as enabled/disabled.
	 * @param	value	A string in which each char corresponds
	 * to a button id in an array, using format '00000'.
	 * The ids are, in order:
	 * helpBtn, menuBtn, backBtn, bookmarkBtn, nextBtn
	 * Possible values:
		 * 0 = disabled and visible
		 * 1 = enabled and visible
		 * 2 = disabled and invisible
		 * 3 = ignore this button - leave as it is
	 */
	public function setNavButtons(value:String):Void
	{
		trace( "NavController.setNavButtons > value : " + value );
		//Dispatch signal to navBar to set next button status accordingly
		_onChangeNavStatus.dispatch(value);
	}
	
	public function onPageDataRequest(response:Signal2<Array<PageData>, String>, keyword:String):Void
	{
		trace("SearchRequest received. Sending response...");
		response.dispatch(pages, keyword);
	}
	
	function get_onChange():Signal2<Int, Int>
	{
		return _onChange;
	}
	
	public var onChange(get_onChange, null):Signal2<Int, Int>;
	
	function get_currentPageGlobalID():Int 
	{
		return _currentPageGlobalID;
	}
	
	function set_currentPageGlobalID(value:Int):Int
	{
		return _currentPageGlobalID = value;
	}
	
	public var currentPageGlobalID(get_currentPageGlobalID, set_currentPageGlobalID):Int;
	
	function get_lastPage():Int 
	{
		return _lastPage;
	}
	
	public var lastPage(get_lastPage, null):Int;
	
	function get_onChangeNavStatus():Signal1<String> 
	{
		return _onChangeNavStatus;
	}
	
	function set_onChangeNavStatus(value:Signal1<String>):Signal1<String> 
	{
		return _onChangeNavStatus = value;
	}
	
	public var onChangeNavStatus(get_onChangeNavStatus, set_onChangeNavStatus):Signal1<String>;
}