#if flash
package unbox.framework.controller;
import motion.Actuate;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.InteractiveObject;
import openfl.display.Sprite;
import openfl.events.FocusEvent;
import openfl.external.ExternalInterface;
import openfl.system.Capabilities;
import openfl.text.TextField;
import unbox.framework.view.CoursePage;

/**
 * Handles all accessibility related tasks
 * such as enabling keyboard tab navigation
 * and screen reader integration.
 * @author Tiago Ling Alexandre
 */
class AccessibilityController
{
	public var isAvailable:Bool;
	var hasExtInterface:Bool;
	var pageView:Sprite;
	var currentPage:CoursePage;
	var navView:Sprite;
	var lastTabIndex:Int;
	var navController:NavController;
	var tabObjs:Array<InteractiveObject>;
	var currentTabIndex:Int;
	
	public function new(navView:Sprite, navController:NavController)
	{
		this.navView = navView;
		this.navController = navController;
		//isAvailable = Capabilities.hasAccessibility && Accessibility.active;
		#if flash
		isAvailable = Capabilities.hasAccessibility;
		#end
		hasExtInterface = ExternalInterface.available;
		trace( "isAvailable : " + isAvailable );
		
		if (hasExtInterface) {
			ExternalInterface.marshallExceptions = true;
			ExternalInterface.addCallback("handleOnFocus", handleOnFocus);
			jsTrace("AccessibilityController ->> handleOnFocus callback added!");
		}
		
		//See if the delay is really needed before calling updateProperties
		//and/or in between updateProperties and sendEvent for the
		//accessibility to work correctly with JAWS
		navView.stage.addEventListener(FocusEvent.KEY_FOCUS_CHANGE, onKeyFocusChange);
	}
	
	/**
	 * Helper function to delay page parsing by the controller.
	 * @param	page	The current course page being displayed.
	 */
	public function prepareToUpdate(page:CoursePage):Void 
	{
		Actuate.timer(0.1).onComplete(setPageObjects, [page]);
	}
	
	/**
	 * Main controller function, parses the page display list and sets
	 * the correct tabbing order for each relevant object.
	 * @param	page	The current course page being displayed.
	 */
	public function setPageObjects(page:CoursePage):Void
	{
		jsTrace("1. AccessibilityController.setPageTexts");
		lastTabIndex = 0;
		
		currentPage = page;
		pageView = currentPage.view;
		
		pageView.tabChildren = true;
		pageView.tabIndex = 1;
		
		tabObjs = new Array<InteractiveObject>();
		
		//KEY TABBING
		var currentTabIndex:Int = 2;
		var pageViewChildren:Int = pageView.numChildren;
		for (i in 0...pageViewChildren)
		{
			var child:DisplayObject = pageView.getChildAt(i);
			
			if (child.name.indexOf("text") > -1)
			{
				var text:TextField = cast(child, TextField);
				var textTabIndex:Int = Std.parseInt(text.name.split("_")[1]);
				
				//KEY TABBING
				text.tabEnabled = true;
				text.tabIndex = textTabIndex + 2;
				currentTabIndex++;
				tabObjs.push(text);
			} else if (child.name.indexOf("button") > -1) {
				var intObj:InteractiveObject = cast(child, InteractiveObject);
				
				intObj.tabEnabled = true;
				intObj.tabIndex = currentTabIndex;
				currentTabIndex++;
				tabObjs.push(intObj);
			} else if (child.name.indexOf("exercise") > -1) {
				var container:DisplayObjectContainer = cast(child, DisplayObjectContainer);
				
				var cChildren:Int = container.numChildren;
				for (j in 0...cChildren)
				{
					var subChild:DisplayObject = container.getChildAt(j);
					if (Std.is(subChild, InteractiveObject) && subChild.name.indexOf("instance") == -1)
					{
						var subIntObj:InteractiveObject = cast(subChild, InteractiveObject);
						subIntObj.tabEnabled = true;
						subIntObj.tabIndex = currentTabIndex;
						currentTabIndex++;
						tabObjs.push(subIntObj);
					}
				}
			} else {
				#if flash
				var accProps:flash.accessibility.AccessibilityProperties = new flash.accessibility.AccessibilityProperties();
				accProps.silent = true;
				child.accessibilityProperties = accProps;
				#end
			}
		}
		
		//NavView settings
		//TODO: Change to reflect all needed buttons (back, next, menu, favorites, help)
		var navContainer:Sprite = cast(navView.getChildAt(0));
		var nViewChildren:Int = navContainer.numChildren;
		for (j in 0...nViewChildren)
		{
			var child:Dynamic = navContainer.getChildAt(j);
			if (child.name.indexOf("Btn") > -1)
			{
				var btn:Sprite = cast(child, Sprite);
				
				#if flash
				var accProps:flash.accessibility.AccessibilityProperties = new flash.accessibility.AccessibilityProperties();
				switch (btn.name) {
					case "backBtn":
						accProps.description = "Navega para a página anterior";
					case "nextBtn":
						accProps.description = "Navega para a próxima página";
					case "menuBtn":
						accProps.description = "Navega até o menu do curso";
						
				}
				btn.accessibilityProperties = accProps;
				#end
				
				btn.tabEnabled = true;
				if (j < nViewChildren)
				{
					btn.tabIndex = currentTabIndex;
					currentTabIndex++;
				}
				tabObjs.push(btn);
			} else {
				#if flash
				var accProps:flash.accessibility.AccessibilityProperties = new flash.accessibility.AccessibilityProperties();
				accProps.silent = true;
				child.accessibilityProperties = accProps;
				#end
			}
		}
		
		lastTabIndex = currentTabIndex;
		
		//Change focus through js and flash
		pageView.stage.focus = null;
		getPageFocus();
		
		dumpObjects();
	}
	
	/**
	 * Calls a javascript function to set focus on the flash object
	 * directly from the html page.
	 */
	public function getPageFocus():Void
	{
		jsTrace( "2. AccessibilityController.getPageFocus" );
		if (hasExtInterface)
		{
			ExternalInterface.call("setFocusOnFlash");
		}
	}
	
	/**
	 * Callback from setting focus on the html page in
	 * which the swf object is displayed. Captures focus
	 * from the html page so the swf can have focus without
	 * using the mouse.
	 */
	public function handleOnFocus():Void
	{
		jsTrace( "3. @@@@ AccessibilityController.handleOnFocus" );
		
		if (currentPage != null) {
			currentPage.view.stage.focus = currentPage.view;
		} else {
			jsTrace("CurrentPage.view is null!");
		}
		
		updateAccessibility();
	}
	
	/**
	 * Updates accessibility properties if accessibility is
	 * available and schedules function to set screen reader
	 * focus.
	 */
	public function updateAccessibility():Void
	{
		if (isAvailable) {
			jsTrace( "4. AccessibilityController.updateAccessibility" );
			//Tells Jaws to update accessibility properties for all accessible objects.
			#if flash
			flash.accessibility.Accessibility.updateProperties();
			#end
			
			//Tells Jaws to focus on selected object (currentPage);
			Actuate.timer(0.1).onComplete(setJawsFocus);
		}
	}
	
	/**
	 * Function called to set screen reader focus to the first
	 * interactive object in a new page.
	 */
	function setJawsFocus():Void {
		//FOCUS EVENT
		jsTrace("setJawsFocus ->> jaws will say the contents of this object : " + tabObjs[0].name);
		#if flash
		flash.accessibility.Accessibility.sendEvent(tabObjs[0], 0, 0x8005, false);
		#end
	}
	
	//TODO: This can be inside the Logger class
	public function jsTrace(content:Dynamic):Void
	{
		//Comment console.log calls because of IE
		//(Javascript console only gets instantiated
		// when the developer tools (F12) are active)
		#if debug
		if (hasExtInterface)
		{
			ExternalInterface.call("jsTrace", Std.string(content));
		}
		#end
		trace(content);
	}
	
	function onKeyFocusChange(e:FocusEvent):Void
	{
		if (e.relatedObject.name.indexOf("option") > -1)
		{
			
		} else {
			#if flash
			say(e.relatedObject, cast(e.relatedObject, InteractiveObject).accessibilityProperties.name);
			#end
		}
	}
	
	/**
	 * Debug function. This will trace all objects currently in the
	 * display list of the view of the current page. Some parts are
	 * commented for less verbose output.
	 */
	function dumpObjects():Void
	{
		trace( " ### dumpObjects ### " );
		jsTrace("** VIEW named : '" + currentPage.view.name + "' and tabIndex : '" + currentPage.view.tabIndex + "' has '" + currentPage.view.numChildren + "' children.");
		var vNumChildren:Int = currentPage.view.numChildren;
		for (i in 0...vNumChildren)
		{
			var child:DisplayObject = currentPage.view.getChildAt(i);
			if (Std.is(child, DisplayObjectContainer))
			{
				var container:DisplayObjectContainer = cast(child, DisplayObjectContainer);
				var cNumChildren:Int = container.numChildren;
				for (j in 0...cNumChildren)
				{
					var subChild:DisplayObject = container.getChildAt(j);
					if (Std.is(subChild, DisplayObjectContainer) && subChild.name.indexOf("instance") == -1)
					{
						var subContainer:DisplayObjectContainer = cast(subChild, DisplayObjectContainer);
						var sNumChildren:Int = subContainer.numChildren;
						jsTrace("\t** ->> SUBCONTAINER at '" + j + "' with tabIndex : '" + subContainer.tabIndex + "' named : '" + subContainer.name + "' has '" + sNumChildren + "' children.");
/*						for (k in 0...sNumChildren)
						{
							var sSubChild:DisplayObject = subContainer.getChildAt(k);
							jsTrace("\t\t** ->> SUBSUBCHILD at '" + k + "' named : '" + sSubChild.name + "' is a DisplayObject.");
						}*/
					} else {
						if (Std.is(subChild, InteractiveObject) && subChild.name.indexOf("instance") == -1) {
							jsTrace("\t** ->> SUBCHILD at '" + j + "' with tabIndex : '" + cast(subChild, InteractiveObject).tabIndex + "' named : '" + subChild.name + "' is an InteractiveObject.");
						} /*else {
							jsTrace("\t** ->> SUBCHILD at '" + j + "' named : '" + subChild.name + "' is a DisplayObject.");
						}*/
					}
				}
			} else {
				if (Std.is(child, InteractiveObject) && child.name.indexOf("instance") == -1) {
					jsTrace("** ->> CHILD at '" + i + "' with tabIndex : '" + cast(child, InteractiveObject).tabIndex + "' named : '" + child.name + "' is am InteractiveObject.");
					#if flash
					jsTrace("** ->> CHILD at '" + i + "' accessProps ->> name : '" + cast(child, InteractiveObject).accessibilityProperties.name + "' | description : '" + cast(child, InteractiveObject).accessibilityProperties.description);
					#end
				} /*else {
					jsTrace("** ->> CHILD at '" + i + "' named : '" + child.name + "' is a DisplayObject.");
				}*/
			}
		}
	}
	
	/**
	 * Sends an event telling the screen reader to focus and read on
	 * demand.
	 * @param	target	The display object for the screen reader to focus
	 * @param	text	The text string to be read by the screen reader
	 */
	public function say(target:DisplayObject, text:String):Void
	{
		if (target != null && isAvailable)
		{
			#if flash
			var accProperties:flash.accessibility.AccessibilityProperties;
			if (target.accessibilityProperties != null)
				accProperties = target.accessibilityProperties;
			else
				accProperties = new flash.accessibility.AccessibilityProperties();
			
			accProperties.name = text;
			accProperties.silent = false;
			accProperties.noAutoLabeling = true;
			accProperties.forceSimple = true;
			target.accessibilityProperties = accProperties;
			flash.accessibility.Accessibility.updateProperties();
			flash.accessibility.Accessibility.sendEvent(target, 0, 32773);
			#end
		}
	}
}
#end