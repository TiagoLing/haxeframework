package unbox.framework.controller;
import haxe.xml.Fast;
import msignal.Signal.Signal1;
import openfl.Assets.AssetLibrary;
import openfl.display.DisplayObject;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.system.ApplicationDomain;
import unbox.framework.model.vo.PageData;
import unbox.framework.utils.ContentParser;
import unbox.framework.utils.PageAnimator;
import unbox.framework.utils.TweenParser;
import unbox.framework.view.CoursePage;


/**
 * Handles page instantiation, lifecycle and 
 * method calls such as transitionIn / Out.
 * @author Tiago Ling Alexandre
 */
class PageController
{
	var currentPage:CoursePage;
	var lastPage:CoursePage;
	var pView:Sprite;
	var currentPageContent:Fast;
	var lastPageContent:Fast;
	var currentPageData:PageData;
	var lastPageData:PageData;
	var nextPageDirection:Int;
	var _onBeforePageChanged:Signal1<CoursePage>;
	var _onPageChanged:Signal1<CoursePage>;
	var pageLayer:Sprite;
	#if flash
	public var stylesheet:flash.text.StyleSheet;
	#end
	
	//TODO: Make a better way to set this reference for the CoursePages
	public var context:CourseContext;
	
	var tParser:TweenParser;
	
	public function new(pageLayer:Sprite, context:CourseContext) 
	{
		this.pageLayer = pageLayer;
		this.context = context;
		
		nextPageDirection = 0;
		_onPageChanged = new Signal1<CoursePage>();
		_onBeforePageChanged = new Signal1<CoursePage>();
		tParser = new TweenParser();
	}
	
	public function setup(nextPageData:PageData, direction:Int):Void
	{
		lastPage = currentPage;
		lastPageContent = currentPageContent;
		lastPageData = currentPageData;
		currentPage = null;
		pView = null;
		currentPageContent = null;
		currentPageData = nextPageData;
		
		nextPageDirection = direction;
		#if flash
		ContentParser.stylesheet = stylesheet;
		#end
	}
	
	#if flash
	public function onPageAssetsLoaded(pageAssets:Map<String, Dynamic>, ?appDomain:ApplicationDomain):Void
	#else
	public function onPageAssetsLoaded(pageAssets:Map<String, Dynamic>):Void
	#end
	{
		for (key in pageAssets.keys())
		{
			//TODO: Refactor and remove hardcoding
			if (key == "contentXML")
			{
				var pageXml:Xml = Xml.parse(pageAssets.get(key));
				currentPageContent = new Fast(pageXml.firstElement());
				currentPageData.assets.set(key, currentPageContent);
			} else if ((key.indexOf("_P_") > -1) || (key.indexOf("cover") > -1)) {
				var p:Sprite = null;
				#if flash
				p = cast(pageAssets.get(key), Sprite);
				#else
				p = cast(pageAssets.get(key), AssetLibrary).getMovieClip("com.unbox.assets.PageView");
				#end
				pView = p;
				
				if (pView.name.indexOf("instance") < 0)
					pView.name = "pageView";
			} else {
				var asset = pageAssets.get(key);
				var additionalContent:Fast = null;
				if (Type.getClassName(Type.getClass(asset)) == "String")
				{
					var addData:Xml = Xml.parse(asset);
					additionalContent = new Fast(addData.firstElement());
				}
				currentPageData.assets.set(key, additionalContent);
			}
		}
		
		if (pView != null && currentPageContent != null)
		{
			var mask = new Shape();
			mask.graphics.beginFill(0xFF00FF, 1);
			mask.graphics.drawRect(0, 0, pageLayer.stage.stageWidth, pageLayer.stage.stageHeight);
			pView.mask = mask;
			pView.addChild(mask);
			
			if (currentPageContent.hasNode.page && currentPageContent.node.page.att.className != "null")
			{
				var className:String = currentPageContent.node.page.att.className;
				var classRef = Type.resolveClass(className);
				currentPage = Type.createInstance(classRef, []);
				currentPage.view = pView;
				currentPage.name = currentPageContent.node.page.att.name;
			} else {
				currentPage = new CoursePage();
				currentPage.view = pView;
				currentPage.name = currentPageContent.node.page.att.name;
			}
			
			currentPage.name = currentPageContent.node.page.att.name;
			currentPage.data = currentPageData;
			currentPage.context = context;
			#if flash
			currentPage.stylesheet = stylesheet;
			currentPage.appDomain = appDomain;
			ContentParser.currentAppDomain = appDomain;
			#end
			
			ContentParser.parsePageContent(currentPageContent, currentPage, changePage);
		}
	}
	
	function changePage():Void
	{
		trace( "PageController.changePage -> lastPage : " + lastPage + " currentPage : " + currentPage );
		
		//TODO: lastPage must transitionOut its content first then animate the view out.
		if (lastPage != null && lastPageData.pageTransitionOut != "none") 
		{
			lastPage.transitionOut();
			
			PageAnimator.animate(lastPage.view, 0.5, nextPageDirection, lastPageData.pageTransitionOut, animateContent, [lastPage, lastPageData]);
			
			//TODO: Release page from memory (nullify all references) and unload appDomain
			//lastPage.dispose();
			
		} else if (lastPage != null) {
			lastPage.transitionOut();
			animateContent(lastPage, lastPageData);
		}
		
		//Clear tween references for the last page before transitioning in the new current page.
		tParser.flushTweens();
		
		pageLayer.addChild(currentPage.view);
		
		_onBeforePageChanged.dispatch(currentPage);
		
		if (currentPageData.pageTransitionIn != "none")
		{
			currentPage.transitionIn();
				
			PageAnimator.animate(currentPage.view, 0.5, nextPageDirection, currentPageData.pageTransitionIn, animateContent, [currentPage, currentPageData]);
		} else {
			currentPage.transitionIn();
			animateContent(currentPage, currentPageData);
		}
	}
	
	function animateContent(page:CoursePage, pageData:PageData):Void
	{
		if (page == currentPage)
		{
			if (pageData.contentTransitionIn != "none") 
			{
				PageAnimator.contentFadeIn(page.view, 0.5, 0, onTransitionComplete, [page]);
			} else {
				onTransitionComplete(page);
			}
		} else {
			if (pageData.contentTransitionOut != "none") 
			{
				PageAnimator.contentFadeOut(page.view, 0.5, 0, onTransitionComplete, [page]);
			} else {
				onTransitionComplete(page);
			}
		}
	}
	
	function onTransitionComplete(page:CoursePage):Void 
	{
		if (page == currentPage) 
		{
			//TweenParser
			if (currentPageContent.hasNode.tween)
				tParser.parse(page.view, currentPageContent.node.tween);
				
			page.transitionInComplete();
			_onPageChanged.dispatch(page);
		} else {
			page.transitionOutComplete();
			pageLayer.removeChild(page.view);
		}
	}
	
	function get_onPageChanged():Signal1<CoursePage> 
	{
		return _onPageChanged;
	}
	
	function set_onPageChanged(value:Signal1<CoursePage>):Signal1<CoursePage> 
	{
		return _onPageChanged = value;
	}
	
	public var onPageChanged(get_onPageChanged, set_onPageChanged):Signal1<CoursePage>;
	
	function get_onBeforePageChanged():Signal1<CoursePage>
	{
		return _onBeforePageChanged;
	}
	
	function set_onBeforePageChanged(value:Signal1<CoursePage>):Signal1<CoursePage> 
	{
		return _onBeforePageChanged = value;
	}
	
	public var onBeforePageChanged(get_onBeforePageChanged, set_onBeforePageChanged):Signal1<CoursePage>;
}