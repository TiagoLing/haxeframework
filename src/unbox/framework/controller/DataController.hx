package unbox.framework.controller;

import flash.external.ExternalInterface;
import haxe.xml.Fast;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import unbox.framework.model.SessionTimer;
import unbox.framework.model.Status;
import unbox.framework.model.vo.EbookData;
import unbox.framework.model.vo.PageData;
import unbox.framework.services.IEbookDataService;
import unbox.framework.services.ScormDataService;
import unbox.framework.services.SolDataService;
import unbox.framework.utils.ArrayUtils;
import unbox.framework.view.CoursePage;

/**
 * ...
 * @author Tiago Ling Alexandre @ UNBOX® - http://www.unbox.com.br
 */  
class DataController
{
    public var isConsultMode(get, set) : Bool;
    public var ebookData(get, set) : EbookData;
    public var dataServiceType(get, set) : String;
    public var enableAlerts(get, set) : Bool;
    public var hasAccessibility(get, set) : Bool;
    public var enableDebugPanel(get, set) : Bool;
	public var status(get, set) : Status;
	
	var dataService : IEbookDataService;
	var sessionTimer : SessionTimer;
	var isExtIntAvaiable : Bool;
	var isDataServiceAvaiable : Bool;
	var _dataServiceType : String;
	var _ebookData : EbookData;
	var _isConsultMode : Bool;
	var _enableAlerts : Bool;
	var _hasAccessibility : Bool;
	var _enableDebugPanel : Bool;
	var _status:Status;
	
	public var onBookmarkUpdated:Signal2<Bool, PageData>;
	public var onPageBookmarkChecked:Signal1<Bool>;
	
	public var populateBookmarks:Signal1<Array<PageData>>;
	
	public function new()
    {
		_isConsultMode = false;
		_enableAlerts = false;
		_dataServiceType = "SharedObject";
		isDataServiceAvaiable = false;
		isExtIntAvaiable = ExternalInterface.available;
    }
	
	/********************* Start *********************/
	public function start(data:Fast, maxPageId:Int, maxModuleId:Int):Void 
	{
		trace("DataController.start");
		_ebookData = new EbookData();
		
		_status = new Status();
		_status.maxPage = maxPageId;
		_status.maxModule = maxModuleId;
		
		//-- start custom data
		_isConsultMode = data.att.consultMode == "true" ? true : false;
		_dataServiceType = data.att.dataServiceType;
		_enableDebugPanel = data.att.enableDebugPanel == "true" ? true : false;
		
		_status.lessonStatus.maxPoints = ArrayUtils.toNumber(data.node.customData.node.maxPoints.innerHTML.split(","));
		_status.lessonStatus.lessonStatus = ArrayUtils.toNumber(data.node.customData.node.lessonStatus.innerHTML.split(","));
		_status.lessonStatus.userPoints = ArrayUtils.fillArray(status.lessonStatus.maxPoints.length, -1);
		
		//TODO: Implement DebugPanel
		//-- start debug panel
/*		if (dataController.enableDebugPanel)
		{
			debugPanel = new DebugPanel(this.contextView, nav.totalPages);
			debugPanel.setCallbacks(nav.backPage, nav.nextPage, nav.navigateToPageIndex);
			Logger.callback = debugPanel.logToPanel;
			ebook.setDebugPanel(debugPanel);
		}*/
		
		// Signals
		// Comunicates with Dashboard.updateBookmarks
		onBookmarkUpdated = new Signal2<Bool, PageData>();
		// Comunicates with NavBar.setBookmarkStatus
		onPageBookmarkChecked = new Signal1<Bool>();
		// More like a command, sends persistent bookmarks to Dashboard
		populateBookmarks = new Signal1<Array<PageData>>();
		
		sessionTimer = new SessionTimer();
		(_isConsultMode) ? startBrowseMode() : initDataService();
    }
	
	public function startBrowseMode():Void
	{
		trace("DataController.startBrowseMode");
		trace("*** RUNNING BROWSE MODE ***");
		
		if (isExtIntAvaiable && _enableAlerts)
			ExternalInterface.call("alert", "Iniciando o treinamento em modo CONSULTA.\n\nSeus dados não serão gravados.");
			
		_isConsultMode = true;
		_status.currentModule = 0;
		_status.currentPage = 0;
    }
	
	function initDataService():Void
	{
		trace("DataController.initDataService");
		if (_dataServiceType == "SCORM" && isExtIntAvaiable) 
		{
			dataService = new ScormDataService();
        } else {
			trace("DataController.initDataService >> DataService SCORM not available! Using SharedObject instead.");
			_dataServiceType = "SharedObject";
			dataService = new SolDataService();
        }
		
		if (isExtIntAvaiable)
			ExternalInterface.addCallback("jsCall", jsCall);
			
		dataService.onLoaded.add(onDataLoaded);
		dataService.onSaved.add(onDataSaved);
		dataService.onLoadError.add(onDataLoadError);
		dataService.onSaveError.add(onDataSaveError);
		dataService.load();
    }
	
	function onDataLoaded(data:EbookData):Void
	{
		trace("DataController.onDataLoaded");
		isDataServiceAvaiable = true;
		_ebookData = data;
		for (i in Reflect.fields(_ebookData))
			trace("	LOAD SCORM DATA >> " + i + ", value : " + Reflect.field(_ebookData, i));
			
		//-- check browse mode
		if (_ebookData.lesson_mode == EbookData.MODE_BROWSE)
		{
			startBrowseMode();
        } else {
			if (_ebookData.suspend_data == null || _ebookData.suspend_data == "" || _ebookData.lesson_status == EbookData.STATUS_NOT_ATTEMPTED) 
			{
				trace("DataController.initDataService >> SUSPEND_DATA null OR empty. New user!");
				_ebookData.lesson_status = EbookData.STATUS_INCOMPLETE;
				_status.status = Status.STATUS_INITIALIZED;
				sessionTimer.initSession();
				//-- save all data
				save();
            } else {
				trace("DataController.initDataService >> SUSPEND_DATA read.");
				//-- read saved data
				_status.parseData(_ebookData.suspend_data);
				trace(_status.lessonStatus.toString());
				sessionTimer.initSession();
            }
        }
    }
	
	function onDataSaved() : Void 
	{
		trace("DataController.onDataSaved >> Saved successfully!");
    }
	
	function onDataLoadError(msg : String) : Void 
	{
		trace("DataController.onDataLoadError >> " + msg);
		if (isExtIntAvaiable && _enableAlerts)
			ExternalInterface.call("alert", "Erro ao carregar dados do DATASERVICE (" + _dataServiceType + ").\n\nContate o administrador do sistema.\n\n" + msg);
			
		isDataServiceAvaiable = false;
		startBrowseMode();
    }
	
	function onDataSaveError(msg : String) : Void 
	{
		trace("DataController.onDataSaveError >> " + msg);
		if (isExtIntAvaiable && _enableAlerts)
			ExternalInterface.call("alert", "Erro ao salvar dados no DATASERVICE (" + _dataServiceType + ").\n\nContate o administrador do sistema.\n\n" + msg);
    }
	
	/********************* SAVE *********************/
	public function save(?page:CoursePage) : Void 
	{
		if (page != null)
		{
			status.currentPage = page.data.globalIndex;
		}
		
		trace("DataController.save");
		if (isDataServiceAvaiable && !_isConsultMode && _ebookData.lesson_status == EbookData.STATUS_INCOMPLETE) 
		{
			if (_status.status == Status.STATUS_COMPLETED)
				_ebookData.lesson_status = EbookData.STATUS_COMPLETED;
				
			//TODO: Check if this is working
			//_ebookData.suspend_data = Std.string(_status);
			_ebookData.suspend_data = _status.toString();
			
			_ebookData.session_time = sessionTimer.getCMISessionTime();
			for (i in Reflect.fields(_ebookData))
				trace("	SAVE EBOOK DATA >> " + i + ", value : " + Reflect.field(_ebookData, i));
				
			//-- contact dataService to SAVE Data
			dataService.save(_ebookData);
        } else {
			trace("DataController.save >> Not allowed to save!");
        }
    }
	
	public function finishEbook() : Void 
	{
		trace("DataController.finishEbook");
		if (!_isConsultMode && _status.status == Status.STATUS_INITIALIZED) 
		{
			_status.endDate = Date.now();
			_status.status = Status.STATUS_COMPLETED;
			_ebookData.exit = EbookData.EXIT_LOGOUT;
			save();
        } else {
			trace("DataController.finishEbook >> Course STATUS_COMPLETED, not allowed to save any more data!");
        }
    }
	
	public function closeBrowser() : Void 
	{
		trace("DataController.closeBrowser");
		if (isExtIntAvaiable) 
		{
			ExternalInterface.call("scorm.save");
			ExternalInterface.call("API_Extended.SetNavCommand('exit')");
			// SUMTOTAL LMS API
			ExternalInterface.call("scorm.quit");
			ExternalInterface.call("exit");
        } else {
			trace("DataController.closeBrowser >> ExternalInterface not available!");
        }
    }
	
	public function jsCall(functionName : String, params : String) : Void 
	{
		trace("DataController.jsCall > functionName : " + functionName + ", params : " + params);

        switch (functionName)
        {
			case "save":
				save();
			case "exit":
				closeBrowser();
			default:
				
        }
    }
	
	/**
	 * Receives data about the current page from NavBar
	 * and adds it to the bookmarks. If the bookmark is already
	 * saved it is removed instead.
	 * TODO: Change to method name to reflect it's real functionality
	 * @param	data	PageData object from the current page of the course
	 */
	public function addBookmark(data:PageData):Void
	{
		var pageFound:Int = ArrayUtils.binarySearch(status.lessonStatus.bookmarks, data.globalIndex);
		var added:Bool = false;
		if (pageFound == -1)
		{
			status.lessonStatus.bookmarks.push(data.globalIndex);
			status.lessonStatus.bookmarks.sort(arrayCompare);
			added = true;
			trace("Bookmark '" + data.globalIndex + "' Added : " + status.lessonStatus.bookmarks);
		} else {
			status.lessonStatus.bookmarks.splice(pageFound, 1);
			trace("Bookmark '" + data.globalIndex + "' Removed : " + status.lessonStatus.bookmarks);
			added = false;
		}
		
		save();
		
		onBookmarkUpdated.dispatch(added, data);
	}
	
	/**
	 * Checks wether the currentPage is bookmarked. This will make NavBar
	 * toggle the bookmark button to reflect its current status
	 * @param	page	PageData object from the current page of the course
	 */
	public function checkBookmark(page:CoursePage):Void
	{
		trace( "DataController.checkBookmark > page : " + page );
		var pageFound:Int = ArrayUtils.binarySearch(status.lessonStatus.bookmarks, page.data.globalIndex);
		//TODO: Apparently ToggleButton on/off -> true/false is inverted
		var pageHasBookmark:Bool = pageFound > -1 ? true : false;
		trace( "pageHasBookmark : " + pageHasBookmark );
		//Dispatch signal to NavBar to toggle BookmarkButton
		onPageBookmarkChecked.dispatch(pageHasBookmark);
	}
	
	//Internal method to compare values from the array and sort it
	static inline function arrayCompare(a:Int, b:Int):Int
	{
		if (a > b)
			return 1;
		else if (a < b)
			return -1;
		else
			return 0;
	}
	
	/**
	 * Called from CourseContext to populate the bookmark list with
	 * persistent data loaded from a data service
	 * @param	pages	Array containing all data from the course pages.
	 */
	public function loadBookmarks(pages:Array<PageData>):Void
	{
		var bookmarks:Array<PageData> = new Array<PageData>();
		var numPages:Int = pages.length;
		for (i in 0...numPages)
		{
			var numBookmarks:Int = status.lessonStatus.bookmarks.length;
			for (j in 0...numBookmarks)
			{
				if (status.lessonStatus.bookmarks[j] == pages[i].globalIndex) {
					bookmarks.push(pages[i]);
				}
			}
		}
		
		if (bookmarks.length > 0)
			populateBookmarks.dispatch(bookmarks);
	}
	
	/********************* GETTERS and SETTERS *********************/
	function get_isConsultMode() : Bool 
	{
		return _isConsultMode;
    }
	
	function set_isConsultMode(value : Bool) : Bool 
	{
		_isConsultMode = value;
        return value;
    }
	
	function get_ebookData() : EbookData 
	{
		return _ebookData;
    }
	
	function set_ebookData(value : EbookData) : EbookData 
	{
		_ebookData = value;
        return value;
    }
	
	function get_dataServiceType() : String 
	{
		return _dataServiceType;
    }
	
	function set_dataServiceType(value : String) : String 
	{
		_dataServiceType = value;
        return value;
    }
	
	function get_enableAlerts() : Bool 
	{
		return _enableAlerts;
    }
	
	function set_enableAlerts(value : Bool) : Bool 
	{
		_enableAlerts = value;
        return value;
    }
	
	function get_hasAccessibility() : Bool 
	{
		return _hasAccessibility;
    }
	
	function set_hasAccessibility(value : Bool) : Bool 
	{
		_hasAccessibility = value;
        return value;
    }
	
	function get_enableDebugPanel() : Bool 
	{
		return _enableDebugPanel;
    }
	
	function set_enableDebugPanel(value : Bool):Bool 
	{
		_enableDebugPanel = value;
        return value;
    }
	
	function get_status():Status 
	{
		return _status;
	}
	
	function set_status(value:Status):Status 
	{
		return _status = value;
	}
}