package unbox.framework.responder;
import unbox.framework.model.vo.PageData;
import msignal.Signal.Signal2;

/**
 * Simple construct to allow the class containing
 * this object to directly request and receive
 * external data through signals.
 * @author Tiago Ling Alexandre
 */
class SearchResponder
{
	public var request:Signal2<Signal2<Array<PageData>, String>, String>;
	public var response:Signal2<Array<PageData>, String>;
	
	public function new() 
	{
		request = new Signal2<Signal2<Array<PageData>, String>, String>();
		response = new Signal2<Array<PageData>, String>();
	}
	
	public function removeAll():Void
	{
		request.removeAll();
		response.removeAll();
	}
	
}