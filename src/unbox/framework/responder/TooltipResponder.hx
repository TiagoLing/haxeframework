package unbox.framework.responder;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import com.hybrid.ui.ToolTip;
import unbox.framework.model.vo.TooltipData;

/**
 * Simple construct to allow the class containing
 * this object to directly request and receive
 * external data through signals.
 * @author Tiago Ling Alexandre @ UNBOX Learning Experience | 2009 - 2014
 */
class TooltipResponder
{
	public var request:Signal2<Signal1<ToolTip>, TooltipData>;
	public var response:Signal1<ToolTip>;
	
	public function new() 
	{
		request = new Signal2<Signal1<ToolTip>, TooltipData>();
		response = new Signal1<ToolTip>();
	}

	public function removeAll():Void
	{
		request.removeAll();
		response.removeAll();
	}
}