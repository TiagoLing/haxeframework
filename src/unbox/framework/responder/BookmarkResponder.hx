package unbox.framework.responder;
import msignal.Signal.Signal1;

/**
 * Simple construct to allow the class containing
 * this object to directly request and receive
 * external data through signals.
 * @author Tiago Ling Alexandre
 */
class BookmarkResponder
{
	public var request:Signal1<T>;
	public var response:Signal1<T>;
	
	public function new() 
	{
		request = new Signal1<T>();
		response = new Signal1<T>();
	}

	public function removeAll():Void
	{
		request.removeAll();
		response.removeAll();
	}
}