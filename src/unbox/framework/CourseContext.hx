package unbox.framework;
#if flash
import unbox.framework.controller.AccessibilityController;
#end
import haxe.xml.Fast;
import motion.Actuate;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.system.ApplicationDomain;
import unbox.framework.controller.DataController;
import unbox.framework.controller.NavController;
import unbox.framework.controller.PageController;
import unbox.framework.model.CourseModel;
import unbox.framework.model.UIModel;
import unbox.framework.model.vo.PageData;
import unbox.framework.services.LoadService;
import unbox.framework.services.TooltipService;
import unbox.framework.view.components.BarMeter;
import unbox.framework.view.components.SearchBox;
import unbox.framework.view.components.SearchPanel;
import unbox.framework.view.CoursePage;
import unbox.framework.view.Dashboard;
import unbox.framework.view.INavBar;
import unbox.framework.view.NavBar;
import unbox.framework.view.NavBarAcc;

/**
 * Main context class for the framework. This is where 
 * initial assets are loaded, all course layers are
 * mantained and all controllers are instantiated.
 * @author Tiago Ling Alexandre @ Unbox 2009-2014
 */
class CourseContext 
{
	//Services
	var loader:LoadService;
	var ttService:TooltipService;
	
	//Model
	var courseModel:CourseModel;
	var uiModel:UIModel;
	
	//Controllers
	public var navController:NavController;
	public var pageController:PageController;
	#if flash
	public var accessibilityController:AccessibilityController;
	#end
	public var dataController:DataController;
	
	//Container layers
	var bgLayer:Sprite;
	var pageLayer:Sprite;
	var uiLayer:Sprite;
	
	//View
	var navBar:INavBar;
	var progressMeter:BarMeter;
	var dashboard:Dashboard;
	
	public function new(dataUrl:String, view:Sprite) 
	{
		bgLayer = new Sprite();
		bgLayer.name = "bgLayer";
		pageLayer = new Sprite();
		pageLayer.name = "pageLayer";
		uiLayer = new Sprite();
		uiLayer.name = "uiLayer";
		view.addChild(bgLayer);
		view.addChild(pageLayer);
		view.addChild(uiLayer);
		
		loader = new LoadService();
		loader.loadSingleString(dataUrl, init);
	}
	
	/**
	 * Instantiates initial classes and sets up loading of remaining assets
	 * It's a callback from the first loading operation, which returns
	 * the main framework config file: ebook_nav.xml
	 * @param	e
	 */
	function init(e:Dynamic):Void 
	{
		//Once initial data is loaded, map it to Xml
		var courseData:Fast = new Fast(Xml.parse(e.target.data).firstElement());
		
		//Instantiates courseModel and courseView, sets up communication between them
		courseModel = new CourseModel(courseData);
		
		//DataController is responsible for all status and persistent data handling
		//(including SCORM and SharedObject services).
		dataController = new DataController();
		dataController.start(courseData.node.config, courseModel.totalPages - 1, courseModel.totalModules - 1);
		
		//ebook_ui.xml
		loader.add(courseModel.uiModelUrl, "xml/ebook_ui.xml");
		
		//styles.css
		loader.add(courseModel.stylesUrl);
		
		#if flash
		loader.add(courseModel.indexViewUrl);
		loader.add(courseModel.uiViewUrl);
		loader.add(courseModel.fontsUrl);
		#else
		//index.swf
		loader.add("index", courseModel.indexViewUrl);
		//nav.swf
		loader.add("nav", courseModel.uiViewUrl);
		//fonts.swf
		//loader.add("fonts", "fonts");
		#end
		
		//Starts loader with handler for assets (gets called only when everything is loaded)
		loader.start(onAssetsLoaded);
	}
	
	/**
	 * Callback from LoadService which gets called when all course assets are loaded.
	 * @param	assets		Map containing all loaded assets. Their are accessed by their string IDs.
	 * @param	appDomain	Application domain from which the assets were loaded. This is necessary
	 * 						for flash to correctly recognize package unbox.framework.names for certain asset types.
	 */
	#if flash
	function onAssetsLoaded(assets:Map<String, Dynamic>, ?appDomain:ApplicationDomain):Void
	#else
	function onAssetsLoaded(assets:Map<String, Dynamic>):Void
	#end
	{
		trace( "CourseContext.onAssetsLoaded");
		
		//Controllers
		createControllers();
		
		//View
		createView(assets);
		
		//signals
		createSignals();
		
		//Init course
		navController.currentPageGlobalID = dataController.status.currentPage;
		navController.onChange.dispatch(navController.currentPageGlobalID, 0);
		
		//TODO: Solve temporal coupling and remove this timer.
		//dataController.loadBookmarks(courseModel.pages);
		Actuate.timer(0.1).onComplete(dataController.loadBookmarks, [courseModel.pages]);
	}
	
	/**
	 * Instantiates and sets up framework controller classes
	 */
	function createControllers():Void
	{
		trace( "CourseContext.createControllers" );
		//TODO: Check for possible "temporal coupling" between controller instances
		//and group signal handler additions to be more readable
		navController = new NavController(courseModel.pages, uiLayer);
		pageController = new PageController(pageLayer, this);
		
		//TODO: Create flag inside ebook_nav.xml to use access. control.
		#if flash
		accessibilityController = new AccessibilityController(uiLayer, navController);
		if (accessibilityController.isAvailable)
		{
			pageController.onPageChanged.add(accessibilityController.prepareToUpdate);
		}
		#end
	}
	
	/**
	 * Instantiates and sets up framework view classes
	 * @param	assets	A list containing all loaded asset classes
	 */
	function createView(assets:Map<String, Dynamic>):Void
	{
		trace( "CourseContext.createView" );
		//UI Data Model class
		var uiData:Xml = Xml.parse(assets.get(courseModel.uiModelUrl));
		uiModel = new UIModel(uiData);
		
		//Dashboard
		dashboard = null;
		#if flash
		dashboard = new Dashboard(uiLayer, uiModel.dashboardData, loader.loaderDomain);
		#else
		dashboard = new Dashboard(uiLayer, uiModel.dashboardData, assets.get(courseModel.uiViewUrl));
		#end
		
		//NavBar
		#if flash
		if (courseModel.configData.accessible)
		{
			navBar = new NavBarAcc(uiLayer, uiModel.navBarData, loader.loaderDomain);
		} else {
			navBar = new NavBar(uiLayer, uiModel.navBarData, loader.loaderDomain);
		}
		#else
		navBar = new NavBar(uiLayer, uiModel.navBarData, assets.get(courseModel.uiViewUrl));
		#end
		
		//Progress meter bar
		//TODO: This should be moved inside either NavBar or Dashboard
		//TODO: Implement Bullet Meter
		progressMeter = new BarMeter(uiLayer, uiModel.progressMeterData);
		progressMeter.hide();
		
		//Stylesheet
		#if flash
		var stylesheet:flash.text.StyleSheet = new flash.text.StyleSheet();
		stylesheet.parseCSS(assets.get(courseModel.stylesUrl));
		courseModel.stylesheet = stylesheet;
		pageController.stylesheet = stylesheet;
		#end
		
		#if flash
		ttService = new TooltipService(stylesheet);
		#else
		ttService = new TooltipService();
		#end
		
		//IndexView (background)
		var indexView:Sprite = null;
		#if flash
		indexView = assets.get(courseModel.indexViewUrl);
		#else
		indexView = cast(assets.get(courseModel.indexViewUrl), AssetLibrary).getMovieClip("");
		#end
		bgLayer.addChild(indexView);
		courseModel.indexView = indexView;
		
		//UIView / NavView
		var navView:Sprite = null;
		#if flash
		navView = assets.get(courseModel.uiViewUrl);
		#else
		navView = cast(assets.get(courseModel.uiViewUrl), AssetLibrary).getMovieClip("");
		#end
		courseModel.navView = navView;
		
		//Fonts
		#if flash
		var fontContainer:Sprite = assets.get(courseModel.fontsUrl);
		courseModel.registerFonts(fontContainer, loader.loaderDomain);
		#end
	}
	
	/**
	 * Sets up all necessary signals for component communications
	 * TODO: These may go inside their own class (?)
	 * TODO: Too many signals currently. Check if reducing their number
	 * is needed / wise.
	 */
	function createSignals():Void
	{
		//NavBar Signals
		//TODO: Move this logic inside the appropriate conditional (only use Dashboard if NavBar is used)
		navBar.onButtonPressed.add(navController.handleNavInput);
		navBar.onDashboardToggle.add(dashboard.toggle);
		navBar.onHelpToggle.add(dashboard.toggleHelp);
		
		navBar.onBookmarkToggle.add(dataController.addBookmark);
		
		//DataController Signals
		dataController.onBookmarkUpdated.add(dashboard.updateBookmarks);
		dataController.onPageBookmarkChecked.add(navBar.setBookmarkStatus);
		dataController.populateBookmarks.add(dashboard.populateBookmarks);
		
		//PageController Signals
		pageController.onBeforePageChanged.add(navBar.updateNavView);
		
		//Does not work - Bookmark button does not get toggled (why?)
		//pageController.onBeforePageChanged.add(dataController.checkBookmark);
		
		pageController.onPageChanged.add(navBar.afterPageChange);
		pageController.onPageChanged.add(dataController.save);
		pageController.onPageChanged.add(dataController.checkBookmark);
		pageController.onPageChanged.add(updateProgress);
		
		//NavController Signals
		navController.onChangeNavStatus.add(navBar.setButtons);
		navController.onChange.add(onPageRequest);
		
		//SearchPanel / Box responder
		var searchBox:SearchBox = dashboard.getSearchBox();
		searchBox.responder.request.add(navController.onPageDataRequest);
		searchBox.responder.response.add(cast(dashboard.getPanel(3), SearchPanel).onSearchDataReceived);
		
		//Navigation from Index / Bookmark / Search panels to pages.
		dashboard.onPanelButtonPressed.add(navController.handlePanelRequest);
		
		//Tooltip Responders
		#if flash
		var nb:NavBar = cast(navBar, NavBar);
		nb.ttResponder.request.add(ttService.getTooltip);	//TooltipService
		nb.ttResponder.response.add(nb.onTooltipReceived);	//NavBar
		nb.requestTooltip();
		
		//Dashboard responder
		dashboard.ttResponder.request.add(ttService.getTooltip);
		dashboard.ttResponder.response.add(dashboard.onTooltipReceived);
		dashboard.requestTooltip();
		#end
		
		//TODO: Change lots of bookmark signals / handlers to a single responder (see SearchResponder)
	}
	
	/**
	 * Callback from NavController.onChange, gets called
	 * whenever the user requests a new page through the
	 * navBar buttons (back, next) or other methods.
	 * @param	pageId		The page to load
	 * @param	direction	Page transition type: 1 = From right to left | -1 = From left to right
	 */
	function onPageRequest(pageId:Int, direction:Int):Void
	{
		var pData:PageData = courseModel.getPage(pageId);
		trace( "Starting load operation for page '" + pageId + "' with url : " + pData.src );
		
		pageController.setup(courseModel.getPage(pageId), direction);
		#if flash
		loader.add(pData.src, pData.id);
		#else
		loader.add(pData.id, pData.src);
		#end
		
		//Setting navBar logo visibility in cover page
		//TODO: Move this to a better place.
		if (!courseModel.configData.accessible)
			pageId == 0 ? cast(navBar, NavBar).setLogoVisible(false) : cast(navBar, NavBar).setLogoVisible(true);
		
		for (key in pData.contentUrls.keys())
		{
			//trace("Page data asset key : " + key + " | value type : " + pData.contentUrls.get(key));
			loader.add(pData.contentUrls.get(key), key);
		}
		
		if (dashboard.isOpened)
			dashboard.toggle();
			
		loader.start(pageController.onPageAssetsLoaded);
	}
	
	/**
	 * Handler to update ProgressMeter according to
	 * the number of pages in the current module.
	 * Called from PageController.onPageChanged signal
	 * dispatch.
	 * @param	page	The current page object.
	 */
	function updateProgress(page:CoursePage)
	{
		page.data.showProgress ? progressMeter.show() : progressMeter.hide();
		progressMeter.setProgress(page.data.localIndex, page.data.numPagesInModule);
	}
}