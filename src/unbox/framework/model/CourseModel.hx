package unbox.framework.model;
import haxe.xml.Fast;
import openfl.display.Sprite;
import openfl.system.ApplicationDomain;
import openfl.text.AntiAliasType;
import openfl.text.Font;
import openfl.text.GridFitType;
import openfl.text.TextField;
import unbox.framework.model.vo.ConfigData;
import unbox.framework.model.vo.PageData;
import unbox.framework.utils.Logger;

/**
 * Stores parsed data from ebook_nav.xml
 * @author Tiago Ling Alexandre
 */
class CourseModel
{
	var rawData:Fast;
	var _pages:Array<PageData>;
	var _totalModules:Int;
	var _totalPages:Int;
	
	//Config
	var _configData:ConfigData;
	
	//Index
	var _indexViewUrl:String;
	var _fontClasses:Array<String>;
	var _fontsUrl:String;
	var _stylesUrl:String;
	
	//Nav
	var _uiViewUrl:String;
	var _uiModelUrl:String;
	
	//Views (TODO: Change these properties to another more specialized class perhaps?)
	var _navView:Sprite;
	var _indexView:Sprite;
	#if flash
	var _stylesheet:flash.text.StyleSheet;
	#end
	var _fonts:Sprite;
	
	public function new(courseData:Fast) 
	{
		rawData = courseData;
		init();
	}

	function init():Void
	{
		//Config -> General course params
		var config:Fast = rawData.node.config;
		_configData = new ConfigData();
		_configData.browseMode = config.att.consultMode == "true" ? true : false;
		_configData.accessible = config.att.accessible == "true" ? true : false;
		_configData.debugActive = config.att.debugActive == "true" ? true : false;
		_configData.dataServiceType = config.att.dataServiceType;
		
		//Index -> Contains background view, font and stylesheet assets
		var indexPage:Fast = rawData.node.page;
		_indexViewUrl = indexPage.att.src; //IndexView == background layer view
		trace( "_indexViewUrl : " + _indexViewUrl );
		for (asset in indexPage.nodes.asset)
		{
			switch (asset.att.id)
			{
				case "fonts":
					_fontClasses = asset.att.fonts.split(",");
					_fontsUrl = asset.att.src;
				case "styles":
					_stylesUrl = asset.att.src;
			}
		}
		
		//Nav -> Contains navigator view and rawData for navController
		var navPage:Fast = indexPage.node.page;
		_uiViewUrl = navPage.att.src;
		_uiModelUrl = navPage.node.asset.att.src;
		
		var modules:Array<Fast> = new Array<Fast>();
		_pages = new Array<PageData>();
		var localCounter:Int = 0;
		var globalCounter:Int = 0;
		var moduleCounter:Int = 0;
		for (module in navPage.nodes.page)
		{
			//BrowseMode / CoverPage
			if (module.att.id == "cover")
			{
				var cpData:PageData = new PageData();
				cpData.id = module.att.id;
				cpData.src = module.att.src;
				cpData.title = module.att.title;
				cpData.localIndex = localCounter;
				cpData.globalIndex = globalCounter;
				cpData.numPagesInModule = 1;
				cpData.moduleId = module.att.id;
				cpData.moduleIndex = moduleCounter;
				cpData.contentUrls = new Map<String, String>();
				cpData.assets = new Map<String, Dynamic>();
				for (asset in module.nodes.asset)
				{
					cpData.contentUrls.set(asset.att.id, asset.att.src);
				}
				
				cpData.pageTransitionIn = module.att.pageTransitionIn;
				cpData.pageTransitionOut = module.att.pageTransitionOut;
				cpData.contentTransitionIn = module.att.contentTransitionIn;
				cpData.contentTransitionOut = module.att.contentTransitionOut;
				cpData.showProgress = module.att.showProgress == "true" ? true : false;
				
				var pgProps = module.att.navbarStatus.split("");
				if (pgProps != null && pgProps.length > 0) {
					cpData.pageProps = new Array<Int>();
					for (i in 0...pgProps.length) {
						cpData.pageProps[i] = Std.parseInt(pgProps[i]);
					}
				}
				
				_pages.push(cpData);
				//localCounter++;
				globalCounter++;
			}
			
			modules.push(module);
			for (page in module.nodes.page)
			{
				var pData:PageData = new PageData();
				pData.id = page.att.id;
				pData.src = page.att.src;
				pData.title = page.att.title;
				pData.localIndex = localCounter;
				pData.globalIndex = globalCounter;
				pData.numPagesInModule = module.nodes.page.length;
				pData.moduleId = module.att.id;
				pData.moduleIndex = moduleCounter;
				
				pData.contentUrls = new Map<String, String>();
				pData.assets = new Map<String, Dynamic>();
				
				for (asset in page.nodes.asset)
				{
					pData.contentUrls.set(asset.att.id, asset.att.src);
				}
				
				pData.pageTransitionIn = page.att.pageTransitionIn;
				pData.pageTransitionOut = page.att.pageTransitionOut;
				pData.contentTransitionIn = page.att.contentTransitionIn;
				pData.contentTransitionOut = page.att.contentTransitionOut;
				pData.showProgress = page.att.showProgress == "true" ? true : false;
				
				var pgProps = page.att.navbarStatus.split("");
				if (pgProps != null && pgProps.length > 0) {
					pData.pageProps = new Array<Int>();
					for (i in 0...pgProps.length) {
						pData.pageProps[i] = Std.parseInt(pgProps[i]);
					}
				}
				_pages.push(pData);
				localCounter++;
				globalCounter++;
			}
			localCounter = 0;
			moduleCounter++;
		}
		
		_totalModules = modules.length;
		_totalPages = pages.length;
	}
	
	public function getPage(id:Int):PageData
	{
		if (id < 0 || id >= pages.length)
		{
			Logger.log("Invalid page id!");
			return null;
		}
		
		return pages[id];
	}
	
	public function registerFonts(container:Sprite, appDom:ApplicationDomain):Void
	{
		var numFonts:Int = _fontClasses.length;
		for (i in 0...numFonts)
		{
			if (appDom.hasDefinition(_fontClasses[i]) == true)
			{
				#if flash
				var fontClass:flash.utils.Object = appDom.getDefinition(_fontClasses[i]);
				Font.registerFont(fontClass);
				#end
				//TODO: Check the need to store the Font classes inside an array
			}
			else
			{
				Logger.log("ERROR : Font class '" + _fontClasses[i] + "' is NOT available!");
			}
		}
	}
	
	//TODO: Remove this method
	public function debugFonts(view:Sprite):Void
	{
		Logger.log("Registered fonts : " + Font.enumerateFonts(false));
		
		var numFonts:Int = _fontClasses.length;
		for (i in 0...numFonts)
		{
			var txt:TextField = new TextField();
			#if flash
			txt.styleSheet = stylesheet;
			#end
			txt.embedFonts = true;
			
			txt.antiAliasType = AntiAliasType.ADVANCED;
			txt.gridFitType = GridFitType.SUBPIXEL;
			#if flash
			txt.mouseWheelEnabled = false;
			#end
			txt.wordWrap = true;
			txt.selectable = false;
			
			txt.width = 100;
			txt.height = 300;
			txt.x = 20 + (100 * i) + (i * 20);
			txt.y = 20;
			
			switch (i)
			{
				case 0:
					txt.htmlText = '<p class="tipText">abcdefghi ABCDEFGHI<p>';
				case 1:
					txt.htmlText = '<p class="altDown">abcdefghi ABCDEFGHI<p>';
				case 2:
					txt.htmlText = '<p class="button">abcdefghi ABCDEFGHI<p>';
				case 3:
					txt.htmlText = '<h5>abcdefghi ABCDEFGHI<h5>';
				case 4:
					txt.htmlText = '<h3>abcdefghi ABCDEFGHI<h3>';
			}
			
			view.addChild(txt);
		}
	}
	
	//###########################
	//### GETTERS AND SETTERS ###
	//###########################
	function get_totalModules():Int
	{
		return _totalModules;
	}
	
	function set_totalModules(value:Int):Int
	{
		return _totalModules = value;
	}
	public var totalModules(get_totalModules, set_totalModules):Int;
	
	function get_totalPages():Int 
	{
		return _totalPages;
	}
	
	function set_totalPages(value:Int):Int 
	{
		return _totalPages = value;
	}
	public var totalPages(get_totalPages, set_totalPages):Int;
	
	function get_indexViewUrl():String 
	{
		return _indexViewUrl;
	}
	public var indexViewUrl(get_indexViewUrl, null):String;
	
	function get_fontClasses():Array<String> 
	{
		return _fontClasses;
	}
	public var fontClasses(get_fontClasses, null):Array<String>;
	
	function get_fontsUrl():String 
	{
		return _fontsUrl;
	}
	public var fontsUrl(get_fontsUrl, null):String;
	
	function get_stylesUrl():String 
	{
		return _stylesUrl;
	}
	public var stylesUrl(get_stylesUrl, null):String;
	
	function get_uiViewUrl():String 
	{
		return _uiViewUrl;
	}
	public var uiViewUrl(get_uiViewUrl, null):String;
	
	function get_uiModelUrl():String 
	{
		return _uiModelUrl;
	}
	public var uiModelUrl(get_uiModelUrl, null):String;
	
	function get_navView():Sprite 
	{
		return _navView;
	}
	
	function set_navView(value:Sprite):Sprite 
	{
		return _navView = value;
	}
	
	public var navView(get_navView, set_navView):Sprite;
	
	function get_indexView():Sprite 
	{
		return _indexView;
	}
	
	function set_indexView(value:Sprite):Sprite 
	{
		return _indexView = value;
	}
	
	public var indexView(get_indexView, set_indexView):Sprite;
	
	#if flash
	function get_stylesheet():flash.text.StyleSheet 
	{
		return _stylesheet;
	}
	
	function set_stylesheet(value:flash.text.StyleSheet):flash.text.StyleSheet 
	{
		return _stylesheet = value;
	}
	
	public var stylesheet(get_stylesheet, set_stylesheet):flash.text.StyleSheet;
	#end
	
	function get_fonts():Sprite 
	{
		return _fonts;
	}
	
	function set_fonts(value:Sprite):Sprite 
	{
		return _fonts = value;
	}
	
	public var fonts(get_fonts, set_fonts):Sprite;
	
	function get_pages():Array<PageData> 
	{
		return _pages;
	}
	
	public var pages(get_pages, null):Array<PageData>;
	
	function get_configData():ConfigData
	{
		return _configData;
	}
	
	function set_configData(value:ConfigData):ConfigData
	{
		return _configData = value;
	}
	
	public var configData(get_configData, set_configData):ConfigData;
}