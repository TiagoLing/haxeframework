package unbox.framework.model;

import unbox.framework.utils.NumberUtils;

/**
 * Controls session time
 * TODO: Move this to 'unbox.framework.utils'
 * @author UNBOX® - http://www.unbox.com.br
 */
class SessionTimer
{
	static var startTime : Float;
	static var endTime : Float;
	static var sessionTime : Float;
	static var d : Date;
	
	public function new() {}
	
	public function initSession() : Void 
	{
		d = Date.now();
		startTime = d.getTime();
    }
	
	/**
	 * Return session time in seconds
	 */  
	public function getSessionTime() : Float 
	{
		d = Date.now();
		sessionTime = (d.getTime() - startTime) / 1000;
		return sessionTime;
    }
	
	/**
	 * Return Session time in CMITimespan format: "hh:mm:ss.ss"
	 */  
	public function getCMISessionTime() : String 
	{
		return secondsToCMITime(getSessionTime());
    }
	
	public function secondsToCMITime(seconds : Float) : String 
	{
		var hr : Float = Std.int(seconds / 3600);
		var min : Float = Std.int((seconds / 60) - (hr * 60));
		var sec : Float = NumberUtils.fmtNumDec(Std.int(seconds % 60), 2);
		var cmiTime : String = NumberUtils.fixPadding(hr, 4) + ":" + NumberUtils.fixPadding(min, 2) + ":" + NumberUtils.fixPadding(sec, 2);
		return cmiTime;
    }
	
	public function cmiTimeToSeconds(cmiTime : String) : Float 
	{
		var timeArr : Array<Dynamic> = cmiTime.split(":");
		var seconds : Float = 0;
		for (i in 0...timeArr.length) 
		{
			var time : Float = Std.parseFloat(timeArr[i]);
			seconds += time * Math.pow(60, timeArr.length - 1 - i);
        }
		
		return seconds;
    }
}