package unbox.framework.model;

import haxe.Json;
import openfl.errors.Error;
import unbox.framework.utils.StringUtils;
import unbox.framework.model.vo.CustomData;

/**
 * Stores course data. Used with DataController
 * @author UNBOX® - http://www.unbox.com.br
 */
class Status
{
    public var ebookVersion(get_ebookVersion, set_ebookVersion) : String;
    public var maxPage(get_maxPage, set_maxPage) : Int;
    public var currentPage(get_currentPage, set_currentPage) : Int;
    public var currentModule(get_currentModule, set_currentModule) : Int;
    public var currentLesson(get_currentLesson, set_currentLesson) : Int;
    public var quizTries(get_quizTries, set_quizTries) : Int;
    public var quizScore(get_quizScore, set_quizScore) : Int;
    public var quizStatus(get_quizStatus, set_quizStatus) : Int;
    public var lessonStatus(get_lessonStatus, set_lessonStatus) : CustomData;
    public var status(get_status, set_status) : Int;
    public var maxModule(get_maxModule, set_maxModule) : Int;
    public var startDate(get_startDate, set_startDate) : Date;
    public var endDate(get_endDate, set_endDate) : Date;
	
	//-- STATUS CONSTANTS
	public static inline var STATUS_NOT_INITIALIZED : Int = 0;
	public static inline var STATUS_INITIALIZED : Int = 1;
	public static inline var STATUS_COMPLETED : Int = 2;
	static inline var DELIMITER : String = "|";
	static inline var SUBDELIMITER : String = ",";
	//-- ebook version
	var _ebookVersion : String;
	var _status : Int;
	var _maxPage : Int;
	var _maxModule : Int;
	var _currentPage : Int;
	var _currentModule : Int;
	var _currentLesson : Int;
	//-- quiz data
	var _quizTries : Int;
	var _quizScore : Int;
	var _quizStatus : Int;
	var _lessonStatus : CustomData;
	var _startDate : Date;
	var _endDate : Date;
	
	public function new()
    {
		_ebookVersion = "UNBOX_HAXEFRAMEWORK_V0.3";
		_status = Status.STATUS_NOT_INITIALIZED;
		_maxPage = 0;
		_maxModule = 0;
		_currentPage = 0;
		_currentModule = 0;
		_currentLesson = 0;
		_quizTries = 0;
		//_quizScore = 0;
		_quizStatus = Status.STATUS_NOT_INITIALIZED;
		_lessonStatus = new CustomData();
		_startDate = Date.now();
		_endDate = Date.now();
    }
	
	//TODO Put inside a MODEL Class
	public function parseData(values : String) : Void 
	{
		if (values != null) 
		{
			if (values.length > 4096)
				throw new Error("*** WARNING: Data overflow! ParseData string > 4096 chars ***");
			
			// RELOAD SCORM PLAYER Compatibility fix - Replaces quotation mark
			values = StringUtils.replace(values, String.fromCharCode(39), String.fromCharCode(34));
			
			// WebAULA LMS compatibility fix
			values = StringUtils.replace(values, String.fromCharCode(180), String.fromCharCode(34));
			
			//-- Parse Data
			var data : Array<Dynamic> = values.split(DELIMITER);
			_ebookVersion = data[0];
			_status = Std.parseInt(data[1]);
			_maxPage = Std.parseInt(data[2]);
			_maxModule = Std.parseInt(data[3]);
			_currentPage = Std.parseInt(data[4]);
			_currentModule = Std.parseInt(data[5]);
			_currentLesson = Std.parseInt(data[6]);
			_quizStatus = Std.parseInt(data[7]);
			_quizTries = Std.parseInt(data[8]);
			_quizScore = Std.parseInt(data[9]);
			_startDate = Date.fromString(data[10]);
			_endDate = Date.fromString(data[11]);
			_lessonStatus = new CustomData(Std.string(data[12]));
        }
    } 
	
	public function toString() : String 
	{
		var data : Array<Dynamic> = new Array<Dynamic>();
		data.push(_ebookVersion);
		data.push(_status);
		data.push(_maxPage);
		data.push(_maxModule);
		data.push(_currentPage);
		data.push(_currentModule);
		data.push(_currentLesson);
		data.push(_quizStatus);
		data.push(_quizTries);
		data.push(_quizScore);
		data.push(Std.string(_startDate));
		data.push(Std.string(_endDate));
		data.push(Json.stringify(_lessonStatus));
		// Replaces quotation mark >>  RELOAD SCORM PLAYER Compatibility
		var ret : String = data.join(DELIMITER);
		ret = StringUtils.replace(ret, String.fromCharCode(34), String.fromCharCode(39));
		return ret;
    }
	
	/*** GETTERS and SETTERS ***/
	function get_ebookVersion() : String 
	{
		return _ebookVersion;
    }
	
	function set_ebookVersion(ebookVersion : String) : String 
	{
		_ebookVersion = ebookVersion;
        return ebookVersion;
    }
	
	function get_maxPage() : Int 
	{
		return _maxPage;
    }
	
	function set_maxPage(maxPage : Int) : Int 
	{
		_maxPage = maxPage;
        return maxPage;
    }
	
	function get_currentPage() : Int 
	{
		return _currentPage;
    }
	
	function set_currentPage(currentPage : Int) : Int 
	{
		_currentPage = currentPage;
        return currentPage;
    }
	
	function get_currentModule() : Int 
	{
		return _currentModule;
    }
	
	function set_currentModule(currentModule : Int) : Int 
	{
		_currentModule = currentModule;
        return currentModule;
    }
	
	function get_currentLesson() : Int 
	{
		return _currentLesson;
    }
	
	function set_currentLesson(currentLesson : Int) : Int 
	{
		_currentLesson = currentLesson;
        return currentLesson;
    }
	
	function get_quizTries() : Int 
	{
		return _quizTries;
    }
	
	function set_quizTries(quizCount : Int) : Int 
	{
		_quizTries = quizCount;
        return quizCount;
    }
	
	function get_quizScore() : Int 
	{
		return _quizScore;
    }
	
	function set_quizScore(quizScore : Int) : Int 
	{
		_quizScore = quizScore;
        return quizScore;
    }
	
	function get_quizStatus() : Int 
	{
		return _quizStatus;
    }
	
	function set_quizStatus(quizStatus : Int) : Int 
	{
		_quizStatus = quizStatus;
        return quizStatus;
    }
	
	function get_lessonStatus() : CustomData 
	{
		return _lessonStatus;
    }
	
	function set_lessonStatus(lessonStatus : CustomData) : CustomData 
	{
		_lessonStatus = lessonStatus;
        return lessonStatus;
    }
	
	function get_status() : Int 
	{
		return _status;
    }
	
	function set_status(status : Int) : Int 
	{
		_status = status;
        return status;
    }
	
	function get_maxModule() : Int 
	{
		return _maxModule;
    }
	
	function set_maxModule(value : Int) : Int 
	{
		_maxModule = value;
        return value;
    }
	
	function get_startDate() : Date 
	{
		return _startDate;
    }
	
	function set_startDate(value : Date) : Date 
	{
		_startDate = value;
        return value;
    }
	
	function get_endDate() : Date 
	{
		return _endDate;
    }
	
	function set_endDate(value : Date) : Date 
	{
		_endDate = value;
        return value;
    }
}