package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class DashboardData implements IData
{
	var _x:Float;
	var _y:Float;
	var _openX:Float;
	var _openY:Float;
	var _aboutPanel:AboutPanelData;
	var _bookmarkPanel:BookmarkPanelData;
	var _indexPanel:IndexPanelData;
	var _searchPanel:SearchPanelData;
	var _searchBox:SearchBoxData;
	var _helpPanel:HelpPanelData;
	var _tooltips:TooltipData;
	
	public var viewClass(default, default):String;
	
	public function new() { }
	
	public function toString():String
	{
/*		return "### DashboardData ### \naboutPanel : " + _aboutPanel.toString() + "\nbookmarkPanel : " + _bookmarkPanel.toString() + "\nindexPanel : " +
			_indexPanel.toString() + "\nsearchPanel : " + _searchPanel.toString() + "\nsearchBox : " + _searchBox.toString() + "\nhelpPanel : " +
			_helpPanel.toString() + "\ntooltips : " + _tooltips.toString() + "\n### END ###";*/
			
		return "";
	}
	
	function get_x():Float
	{
		return _x;
	}
	
	function set_x(value:Float):Float
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float
	{
		return _y;
	}
	
	function set_y(value:Float):Float
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_openX():Float
	{
		return _openX;
	}
	
	function set_openX(value:Float):Float
	{
		return _openX = value;
	}
	
	public var openX(get_openX, set_openX):Float;
	
	function get_openY():Float
	{
		return _openY;
	}
	
	function set_openY(value:Float):Float
	{
		return _openY = value;
	}
	
	public var openY(get_openY, set_openY):Float;
	
	function get_aboutPanel():AboutPanelData 
	{
		return _aboutPanel;
	}
	
	function set_aboutPanel(value:AboutPanelData):AboutPanelData 
	{
		return _aboutPanel = value;
	}
	
	public var aboutPanel(get_aboutPanel, set_aboutPanel):AboutPanelData;
	
	function get_bookmarkPanel():BookmarkPanelData 
	{
		return _bookmarkPanel;
	}
	
	function set_bookmarkPanel(value:BookmarkPanelData):BookmarkPanelData 
	{
		return _bookmarkPanel = value;
	}
	
	public var bookmarkPanel(get_bookmarkPanel, set_bookmarkPanel):BookmarkPanelData;
	
	function get_indexPanel():IndexPanelData 
	{
		return _indexPanel;
	}
	
	function set_indexPanel(value:IndexPanelData):IndexPanelData 
	{
		return _indexPanel = value;
	}
	
	public var indexPanel(get_indexPanel, set_indexPanel):IndexPanelData;
	
	function get_searchPanel():SearchPanelData 
	{
		return _searchPanel;
	}
	
	function set_searchPanel(value:SearchPanelData):SearchPanelData 
	{
		return _searchPanel = value;
	}
	
	public var searchPanel(get_searchPanel, set_searchPanel):SearchPanelData;
	
	function get_searchBox():SearchBoxData 
	{
		return _searchBox;
	}
	
	function set_searchBox(value:SearchBoxData):SearchBoxData 
	{
		return _searchBox = value;
	}
	
	public var searchBox(get_searchBox, set_searchBox):SearchBoxData;
	
	
	function get_helpPanel():HelpPanelData 
	{
		return _helpPanel;
	}
	
	function set_helpPanel(value:HelpPanelData):HelpPanelData 
	{
		return _helpPanel = value;
	}
	
	public var helpPanel(get_helpPanel, set_helpPanel):HelpPanelData;
	
	function get_tooltips():TooltipData 
	{
		return _tooltips;
	}
	
	function set_tooltips(value:TooltipData):TooltipData 
	{
		return _tooltips = value;
	}
	
	public var tooltips(get_tooltips, set_tooltips):TooltipData;
}