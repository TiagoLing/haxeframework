package unbox.framework.model.vo;

/**
 * ...
 * @author UNBOX® - http://www.unbox.com.br
 */
class EbookData implements IData
{ 
	//ENTRY CONSTANTS
	public static inline var ENTRY_ABINITIO : String = "ab-initio";
	public static inline var ENTRY_RESUME : String = "resume";
	
	//-- STATUS CONSTANTS
	public static inline var STATUS_PASSED : String = "passed";
	public static inline var STATUS_COMPLETED : String = "completed";
	public static inline var STATUS_FAILED : String = "failed";
	public static inline var STATUS_INCOMPLETE : String = "incomplete";
	public static inline var STATUS_BROWSED : String = "browsed";
	public static inline var STATUS_NOT_ATTEMPTED : String = "not attempted";
	
	//-- LESSON MODES
	public static inline var MODE_BROWSE : String = "browse";
	public static inline var MODE_NORMAL : String = "normal";
	public static inline var MODE_REVIEW : String = "review";
	
	//-- EXIT CONSTANTS
	public static inline var EXIT_TIMEOUT : String = "time-out";
	public static inline var EXIT_SUSPEND : String = "suspend";
	public static inline var EXIT_LOGOUT : String = "logout";
	
	//-- SCORM fields 1.2
	var _comments : String;
	var _credit : String;
	var _entry : String;
	var _exit : String;
	var _launch_data : String;
	var _lesson_location : String;
	var _lesson_mode : String;
	var _lesson_status : String;
	var _scoreMax : Int;
	var _scoreMin : Int;
	var _scoreRaw : Int;
	var _session_time : String;
	var _student_id : String;
	var _student_name : String;
	var _suspend_data : String;
	var _total_time : String;
	
	public function new()
    {
		_comments = "comments";
		_credit = "credit";
		_entry = ENTRY_RESUME;
		_exit = EXIT_SUSPEND;
		_launch_data = "";
		_lesson_location = "";
		_lesson_mode = MODE_NORMAL;
		_lesson_status = STATUS_NOT_ATTEMPTED;
		_scoreMax = 100;
		_scoreMin = 0;
		//_scoreRaw = 0;
		_session_time = "0000:00:00.00";
		_student_id = "";
		_student_name = "TREINANDO";
		_suspend_data = "";
		_total_time = "0000:00:00.00";
    }
	
	//Getters and Setters
    public var comments(get_comments, set_comments) : String;
    public var credit(get_credit, set_credit) : String;
    public var entry(get_entry, set_entry) : String;
    public var exit(get_exit, set_exit) : String;
    public var launch_data(get_launch_data, set_launch_data) : String;
    public var lesson_location(get_lesson_location, set_lesson_location) : String;
    public var lesson_mode(get_lesson_mode, set_lesson_mode) : String;
    public var lesson_status(get_lesson_status, set_lesson_status) : String;
    public var scoreMax(get_scoreMax, set_scoreMax) : Int;
    public var scoreMin(get_scoreMin, set_scoreMin) : Int;
    public var scoreRaw(get_scoreRaw, set_scoreRaw) : Int;
    public var session_time(get_session_time, set_session_time) : String;
    public var student_id(get_student_id, set_student_id) : String;
    public var student_name(get_student_name, set_student_name) : String;
    public var suspend_data(get_suspend_data, set_suspend_data) : String;
    public var total_time(get_total_time, set_total_time) : String;
	
	function get_comments() : String 
	{
		return _comments;
    }
	
	function set_comments(value : String) : String 
	{
		if (value != null)
			_comments = value;
        return value;
    }
	
	function get_credit() : String 
	{
		return _credit;
    }
	
	function set_credit(value : String) : String 
	{
		if (value != null)
			_credit = value;
        return value;
    }
	
	function get_entry() : String 
	{
		return _entry;
    }
	
	function set_entry(value : String) : String 
	{
		if (value != null)
			_entry = value;
        return value;
    }
	
	function get_exit() : String 
	{
		return _exit;
    }
	
	function set_exit(value : String) : String 
	{
		if (value != null)
			_exit = value;
        return value;
    }
	
	function get_launch_data() : String 
	{
		return _launch_data;
    }
	
	function set_launch_data(value : String) : String 
	{
		if (value != null)
			_launch_data = value;
        return value;
    }
	
	function get_lesson_location() : String 
	{
		return _lesson_location;
    }
	
	function set_lesson_location(value : String) : String 
	{
		if (value != null)
			_lesson_location = value;
		return value;
    }
	
	function get_lesson_mode() : String 
	{
		return _lesson_mode;
    }
	
	function set_lesson_mode(value : String) : String 
	{
		if (value != null)
			_lesson_mode = value;
        return value;
    }
	
	function get_lesson_status() : String 
	{
		return _lesson_status;
    }
	
	function set_lesson_status(value : String) : String 
	{
		if (value != null)
			_lesson_status = value;
        return value;
    }
	
	function get_scoreMax() : Int 
	{
		return _scoreMax;
    }
	
	function set_scoreMax(value : Int) : Int 
	{
		if (!Math.isNaN(value))
			_scoreMax = value;
        return value;
    }
	
	function get_scoreMin() : Int 
	{
		return _scoreMin;
    }
	
	function set_scoreMin(value : Int) : Int 
	{
		if (!Math.isNaN(value))
			_scoreMin = value;
        return value;
    }
	
	function get_scoreRaw() : Int 
	{
		return _scoreRaw;
    }
	
	function set_scoreRaw(value : Int) : Int 
	{
		if (!Math.isNaN(value))
			_scoreRaw = value;
        return value;
    }
	
	function get_session_time() : String 
	{
		return _session_time;
    }
	
	function set_session_time(value : String) : String 
	{
		if (value != null)
			_session_time = value;
        return value;
    }
	
	function get_student_id() : String 
	{
		return _student_id;
    }
	
	function set_student_id(value : String) : String 
	{
		if (value != null)
			_student_id = value;
        return value;
    }
	
	function get_student_name() : String 
	{
		return _student_name;
    }
	
	function set_student_name(value : String) : String 
	{
		if (value != null)
			_student_name = value;
        return value;
    }
	
	function get_suspend_data() : String 
	{
		return _suspend_data;
    }
	
	function set_suspend_data(value : String) : String 
	{
		if (value != null)
			_suspend_data = value;
        return value;
    }
	
	function get_total_time() : String 
	{
		return _total_time;
    }
	
	function set_total_time(value : String) : String 
	{
		if (value != null)
			_total_time = value;
        return value;
    }
	
	public function toString() : String 
	{
		//TODO: Implement this
		var str : String = "";
			
		return str;
    }
}