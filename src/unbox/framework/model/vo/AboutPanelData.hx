package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class AboutPanelData implements IData
{
	var _title:String;
	var _x:Float;
	var _y:Float;
	//TODO: Only one text have an instanceName. So, Map string index will not work
	var _texts:Map<String, Fast>;
	
	public var viewClass(default, default):String;
	
	public function new() { }
	
	public function toString():String
	{
		return "### AboutPanelData ### \ntitle : " + _title + "\nx : " + _x + "\ny : " + _y + "\ntexts : " + _texts.toString() + "\n### END ###";
	}
	
	function get_title():String 
	{
		return _title;
	}
	
	function set_title(value:String):String
	{
		return _title = value;
	}
	
	public var title(get_title, set_title):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_texts():Map<String, Fast> 
	{
		return _texts;
	}
	
	function set_texts(value:Map<String, Fast>):Map<String, Fast> 
	{
		return _texts = value;
	}
	
	public var texts(get_texts, set_texts):Map<String, Fast>;
	
}