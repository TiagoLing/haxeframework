package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class SearchBoxData implements IData
{
	var _title:String;
	var _x:Float;
	var _y:Float;
	var _loadTitle:String;
	var _minChars:Int;
	var _texts:Map<String, Fast>;
	
	public var viewClass(default, default):String;
	public var alwaysVisible(default, default):Bool;
	
	public function new() { }
	
	public function toString():String
	{
		return "### SearchBoxData ### \ntitle : " + _title + "\nx : " + _x + "\ny : " + _y + "\nloadTitle : " + _loadTitle + "\nminChars : " + _minChars + "\n### END ###";
	}
	
	function get_title():String 
	{
		return _title;
	}
	
	function set_title(value:String):String 
	{
		return _title = value;
	}
	
	public var title(get_title, set_title):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_loadTitle():String 
	{
		return _loadTitle;
	}
	
	function set_loadTitle(value:String):String 
	{
		return _loadTitle = value;
	}
	
	public var loadTitle(get_loadTitle, set_loadTitle):String;
	
	function get_minChars():Int 
	{
		return _minChars;
	}
	
	function set_minChars(value:Int):Int 
	{
		return _minChars = value;
	}
	
	public var minChars(get_minChars, set_minChars):Int;
	
	function get_texts():Map<String, Fast> 
	{
		return _texts;
	}
	
	function set_texts(value:Map<String, Fast>):Map<String, Fast> 
	{
		return _texts = value;
	}
	
	public var texts(get_texts, set_texts):Map<String, Fast>;
	
}