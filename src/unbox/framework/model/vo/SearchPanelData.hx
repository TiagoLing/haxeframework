package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class SearchPanelData implements IData
{
	var _title:String;
	var _x:Float;
	var _y:Float;
	var _summaryLength:Int;
	var _list:ListData;
	var _scrollbar:ScrollbarData;
	var _texts:Map<String, Fast>;
	
	public var viewClass(default, default):String;
	
	public function new() { }
	
	public function toString():String
	{
		return "### SearchPanelData ### \ntitle : " + _title + "\nx : " + _x + "\ny : " + _y + "\nsummaryLength : " + _summaryLength + "\nlist : " + _list.toString() + "\nscrollbar : " + _scrollbar.toString() + 
			"\ntexts : "  + _texts.toString() + "\n### END ###";
	}
	
	function get_title():String 
	{
		return _title;
	}
	
	function set_title(value:String):String 
	{
		return _title = value;
	}
	
	public var title(get_title, set_title):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_summaryLength():Int 
	{
		return _summaryLength;
	}
	
	function set_summaryLength(value:Int):Int 
	{
		return _summaryLength = value;
	}
	
	public var summaryLength(get_summaryLength, set_summaryLength):Int;
	
	function get_list():ListData 
	{
		return _list;
	}
	
	function set_list(value:ListData):ListData 
	{
		return _list = value;
	}
	
	public var list(get_list, set_list):ListData;
	
	function get_scrollbar():ScrollbarData 
	{
		return _scrollbar;
	}
	
	function set_scrollbar(value:ScrollbarData):ScrollbarData 
	{
		return _scrollbar = value;
	}
	
	public var scrollbar(get_scrollbar, set_scrollbar):ScrollbarData;
	
	function get_texts():Map<String, Fast> 
	{
		return _texts;
	}
	
	function set_texts(value:Map<String, Fast>):Map<String, Fast> 
	{
		return _texts = value;
	}
	
	public var texts(get_texts, set_texts):Map<String, Fast>;
	
}