package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class ProgressMeterData implements IData
{
	var _className:String;
	var _depth:Int;
	var _instanceName:String;
	var _x:Float;
	var _y:Float;
	var _openX:Float;
	var _openY:Float;
	var _width:Int;
	var _height:Int;
	var _scrollbarColor:Int;
	var _scrollbarColorAlpha:Float;
	var _thumbColor:Int;
	var _thumbColorAlpha:Float;
	var _thumbFillMode:Bool;
	var _autoHideThumb:Bool;
	
	public function new() { }
	
	public function toString():String
	{
		return "### ProgressMeterData ### \nclassName : " + _className + "\ndepth : " + _depth + "\ninstanceName : " + _instanceName + "\nx : " + _x + "\ny : " + _y +
			"\nopenX : " + _openX + "\nopenY : " + _openY + "\nwidth : " + _width + "\nheight : " + _height + "\nscrollbarColor : " + _scrollbarColor + "\nscrollbarColorAlpha : " + _scrollbarColorAlpha +
				"\nthumbColor : " + _thumbColor + "\nthumbColorAlpha : " + _thumbColorAlpha + "\nthumbFillMode : " + _thumbFillMode + "\n### END ###";
	}
	
	function get_className():String 
	{
		return _className;
	}
	
	function set_className(value:String):String 
	{
		return _className = value;
	}
	
	public var className(get_className, set_className):String;
	
	function get_depth():Int 
	{
		return _depth;
	}
	
	function set_depth(value:Int):Int 
	{
		return _depth = value;
	}
	
	public var depth(get_depth, set_depth):Int;
	
	function get_instanceName():String 
	{
		return _instanceName;
	}
	
	function set_instanceName(value:String):String 
	{
		return _instanceName = value;
	}
	
	public var instanceName(get_instanceName, set_instanceName):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_openX():Float 
	{
		return _openX;
	}
	
	function set_openX(value:Float):Float 
	{
		return _openX = value;
	}
	
	public var openX(get_openX, set_openX):Float;
	
	function get_openY():Float 
	{
		return _openY;
	}
	
	function set_openY(value:Float):Float 
	{
		return _openY = value;
	}
	
	public var openY(get_openY, set_openY):Float;
	
	function get_width():Int 
	{
		return _width;
	}
	
	function set_width(value:Int):Int 
	{
		return _width = value;
	}
	
	public var width(get_width, set_width):Int;
	
	function get_height():Int 
	{
		return _height;
	}
	
	function set_height(value:Int):Int 
	{
		return _height = value;
	}
	
	public var height(get_height, set_height):Int;
	
	function get_scrollbarColor():Int 
	{
		return _scrollbarColor;
	}
	
	function set_scrollbarColor(value:Int):Int 
	{
		return _scrollbarColor = value;
	}
	
	public var scrollbarColor(get_scrollbarColor, set_scrollbarColor):Int;
	
	function get_scrollbarColorAlpha():Float 
	{
		return _scrollbarColorAlpha;
	}
	
	function set_scrollbarColorAlpha(value:Float):Float 
	{
		return _scrollbarColorAlpha = value;
	}
	
	public var scrollbarColorAlpha(get_scrollbarColorAlpha, set_scrollbarColorAlpha):Float;
	
	function get_thumbColor():Int 
	{
		return _thumbColor;
	}
	
	function set_thumbColor(value:Int):Int 
	{
		return _thumbColor = value;
	}
	
	public var thumbColor(get_thumbColor, set_thumbColor):Int;
	
	function get_thumbColorAlpha():Float 
	{
		return _thumbColorAlpha;
	}
	
	function set_thumbColorAlpha(value:Float):Float 
	{
		return _thumbColorAlpha = value;
	}
	
	public var thumbColorAlpha(get_thumbColorAlpha, set_thumbColorAlpha):Float;
	
	function get_thumbFillMode():Bool 
	{
		return _thumbFillMode;
	}
	
	function set_thumbFillMode(value:Bool):Bool 
	{
		return _thumbFillMode = value;
	}
	
	public var thumbFillMode(get_thumbFillMode, set_thumbFillMode):Bool;
	
	function get_autoHideThumb():Bool 
	{
		return _autoHideThumb;
	}
	
	function set_autoHideThumb(value:Bool):Bool 
	{
		return _autoHideThumb = value;
	}
	
	public var autoHideThumb(get_autoHideThumb, set_autoHideThumb):Bool;
	
	
}