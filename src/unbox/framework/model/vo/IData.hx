package unbox.framework.model.vo;

/**
 * @author Tiago Ling Alexandre
 */

interface IData 
{
	public function toString():String;
	
}