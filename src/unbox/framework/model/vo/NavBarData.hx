package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class NavBarData implements IData
{
	//TODO: Find an appropriate way to store tween information
	var _texts:Map<String, String>;
	var _tooltips:TooltipData;
	
	var _x:Float;
	var _y:Float;
	var _instanceName:String;
	var _tweenData:Fast;
	
	public function new() { }
	
	public function toString():String
	{
		return "### NavBarData ###" + "\ninstanceName : " + _instanceName + "\nposition : " + _x + "," + _y + "\ntexts : " + _texts.toString() + "\ntooltips : " + _tooltips.toString() + "\n### END ###";
	}
	
	function get_texts():Map<String, String> 
	{
		return _texts;
	}
	
	function set_texts(value:Map<String, String>):Map<String, String> 
	{
		return _texts = value;
	}
	
	public var texts(get_texts, set_texts):Map<String, String>;
	
	function get_tooltips():TooltipData 
	{
		return _tooltips;
	}
	
	function set_tooltips(value:TooltipData):TooltipData 
	{
		return _tooltips = value;
	}
	
	public var tooltips(get_tooltips, set_tooltips):TooltipData;
	
	function get_instanceName():String 
	{
		return _instanceName;
	}
	
	function set_instanceName(value:String):String 
	{
		return _instanceName = value;
	}
	
	public var instanceName(get_instanceName, set_instanceName):String;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_tweenData():Fast 
	{
		return _tweenData;
	}
	
	function set_tweenData(value:Fast):Fast 
	{
		return _tweenData = value;
	}
	
	public var tweenData(get_tweenData, set_tweenData):Fast;
}