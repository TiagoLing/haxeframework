package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class TooltipData implements IData
{
	var _hook:Bool;
	var _hookSize:Int;
	var _autoSize:Bool;
	var _followCursor:Bool;
	var _align:String;
	var _cornerRadius:Float;
	var _showDelay:Int;
	var _hideDelay:Int;
	var _bgAlpha:Float;
	var _width:Int;
	var _colors:Array<UInt>;
	var _items:Map<String, String>;
	var _padding:Float;
	
	public function new() { }
	
	public function toString():String
	{
		return "### TooltipData ### \nhook : " + _hook + "\nhookSize : " + _hookSize + "\nautoSize : " + _autoSize + "\nfollowCursor : " + _followCursor + "\nalign : " + _align + "\ncornerRadius : " + _cornerRadius +
			"\nshowDelay : " + _showDelay + "\nhideDelay : " + _hideDelay + "\nbgAlpha : " + _bgAlpha + "\nwidth : " + _width + "\npadding : " + _padding + "\ncolors : " + _colors + "\nitems : " + _items.toString() + "\n### END ###";
	}
	
	function get_hook():Bool 
	{
		return _hook;
	}
	
	function set_hook(value:Bool):Bool 
	{
		return _hook = value;
	}
	
	public var hook(get_hook, set_hook):Bool;
	
	function get_hookSize():Int 
	{
		return _hookSize;
	}
	
	function set_hookSize(value:Int):Int 
	{
		return _hookSize = value;
	}
	
	public var hookSize(get_hookSize, set_hookSize):Int;
	
	function get_autoSize():Bool 
	{
		return _autoSize;
	}
	
	function set_autoSize(value:Bool):Bool 
	{
		return _autoSize = value;
	}
	
	public var autoSize(get_autoSize, set_autoSize):Bool;
	
	function get_followCursor():Bool 
	{
		return _followCursor;
	}
	
	function set_followCursor(value:Bool):Bool 
	{
		return _followCursor = value;
	}
	
	public var followCursor(get_followCursor, set_followCursor):Bool;
	
	function get_align():String 
	{
		return _align;
	}
	
	function set_align(value:String):String 
	{
		return _align = value;
	}
	
	public var align(get_align, set_align):String;
	
	function get_cornerRadius():Float 
	{
		return _cornerRadius;
	}
	
	function set_cornerRadius(value:Float):Float 
	{
		return _cornerRadius = value;
	}
	
	public var cornerRadius(get_cornerRadius, set_cornerRadius):Float;
	
	function get_showDelay():Int 
	{
		return _showDelay;
	}
	
	function set_showDelay(value:Int):Int 
	{
		return _showDelay = value;
	}
	
	public var showDelay(get_showDelay, set_showDelay):Int;
	
	function get_hideDelay():Int 
	{
		return _hideDelay;
	}
	
	function set_hideDelay(value:Int):Int 
	{
		return _hideDelay = value;
	}
	
	public var hideDelay(get_hideDelay, set_hideDelay):Int;
	
	function get_bgAlpha():Float 
	{
		return _bgAlpha;
	}
	
	function set_bgAlpha(value:Float):Float 
	{
		return _bgAlpha = value;
	}
	
	public var bgAlpha(get_bgAlpha, set_bgAlpha):Float;
	
	function get_width():Int 
	{
		return _width;
	}
	
	function set_width(value:Int):Int 
	{
		return _width = value;
	}
	
	public var width(get_width, set_width):Int;
	
	function get_colors():Array<UInt> 
	{
		return _colors;
	}
	
	function set_colors(value:Array<UInt>):Array<UInt> 
	{
		return _colors = value;
	}
	
	public var colors(get_colors, set_colors):Array<UInt>;
	
	function get_items():Map<String, String> 
	{
		return _items;
	}
	
	function set_items(value:Map<String, String>):Map<String, String> 
	{
		return _items = value;
	}
	
	public var items(get_items, set_items):Map<String, String>;
	
	function get_padding():Float
	{
		return _padding;
	}
	
	function set_padding(value:Float):Float 
	{
		return _padding = value;
	}
	
	public var padding(get_padding, set_padding):Float;
	
}