package unbox.framework.model.vo;

import haxe.Json;

/**
 * ...
 * @author Davi Naizer
 * @version 0.1
 */
class CustomData implements IData 
{
    var _maxPoints : Array<Int>;
    var _userPoints : Array<Int>;
    var _lessonStatus : Array<Int>;
    var _bookmarks : Array<Int>;
    
    public function new(dataString : String = null)
    {
        _maxPoints = [0];
        _userPoints = [-1];
        _lessonStatus = [0];
        _bookmarks = new Array<Int>();
        
        if (dataString != null && dataString != "") 
            parseData(dataString);
    }
    
    /* ----------- GETTERS SETTERS ----------- */
    public function setLessonStatus(lessonId : Int, status : Int) : Void
    {
        _lessonStatus[lessonId] = status;
    }
    
    public function getLessonStatus(lessonId : Int) : Int
    {
        return _lessonStatus[lessonId];
    }
    
    public function setPoints(value : Int, index : Int) : Void
    {
        userPoints[index] = value;
    }
    
    public function getPoints(index : Int) : Int
    {
        return userPoints[index];
    }
    
    public function getTotalPoints() : Int
    {
        var total : Int = 0;
        for (i in 0...userPoints.length){
            if (userPoints[i] > 0) 
                total += userPoints[i];
        }
        return total;
    }
    
    public function getTotalMaxPoints() : Int
    {
        var total : Int = 0;
        for (i in 0...maxPoints.length){
            total += maxPoints[i];
        }
        return total;
    }
    
    public function getGrade() : Int
    {
        var grade : Int = Std.int(100 * getTotalPoints() / getTotalMaxPoints());
        return grade;
    }
    
    public function isTestDone(index : Int) : Bool
    {
        return userPoints[index] > -1;
    }
    
    function get_maxPoints() : Array<Int>
    {
        return _maxPoints;
    }
    
    function set_maxPoints(value : Array<Int>) : Array<Int>
    {
        _maxPoints = value;
        return value;
    }
	
	public var maxPoints(get_maxPoints, set_maxPoints) : Array<Int>;
    
    function get_userPoints() : Array<Int>
    {
        return _userPoints;
    }
    
    function set_userPoints(value : Array<Int>) : Array<Int>
    {
        _userPoints = value;
        return value;
    }
    
	public var userPoints(get_userPoints, set_userPoints) : Array<Int>;
	
    function get_lessonStatus() : Array<Int>
    {
        return _lessonStatus;
    }
    
    function set_lessonStatus(value : Array<Int>) : Array<Int>
    {
        _lessonStatus = value;
        return value;
    }
	
	public var lessonStatus(get_lessonStatus, set_lessonStatus) : Array<Int>;
    
    function get_bookmarks() : Array<Int>
    {
        return _bookmarks;
    }
    
    function set_bookmarks(value : Array<Int>) : Array<Int>
    {
        _bookmarks = value;
        return value;
    }
	
	public var bookmarks(get_bookmarks, set_bookmarks) : Array<Int>;
    
    /* ----------- REQUIRED ----------- */
    public function parseData(dataStr : String) : Void
    {
        var retObj : Dynamic = Json.parse(dataStr);
		
        for (i in Reflect.fields(retObj))
        {
			Reflect.setField(this, i, Reflect.field(retObj, i));
        }
    }
    
    public function toString():String
    {
        return "maxPoints : " + _maxPoints + "\nuserPoints : " + _userPoints +
		"\nlessonStatus : " + _lessonStatus + "\nbookmarks : " + _bookmarks;
    }
}
