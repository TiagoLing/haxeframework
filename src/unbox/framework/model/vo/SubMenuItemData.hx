package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class SubMenuItemData implements IData
{
	var _pageId:String;
	var _lastPageId:String;
	//var _title:String;
	var _title:Fast;
	
	public function new() { }
	
	public function toString():String
	{
		return "### SubMenuItemData ### \npageId : " + _pageId + "\nlastPageId : " + _lastPageId + "\ntitle : " + _title + "\n### END ###";
	}
	
	function get_pageId():String 
	{
		return _pageId;
	}
	
	function set_pageId(value:String):String 
	{
		return _pageId = value;
	}
	
	public var pageId(get_pageId, set_pageId):String;
	
	function get_lastPageId():String 
	{
		return _lastPageId;
	}
	
	function set_lastPageId(value:String):String 
	{
		return _lastPageId = value;
	}
	
	public var lastPageId(get_lastPageId, set_lastPageId):String;
	
	function get_title():Fast 
	{
		return _title;
	}
	
	function set_title(value:Fast):Fast 
	{
		return _title = value;
	}
	
	public var title(get_title, set_title):Fast;
	
}