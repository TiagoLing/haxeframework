package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class IndexPanelData implements IData
{
	var _title:String;
	var _x:Float;
	var _y:Float;
	var _menuLists:Array<ListData>;
	var _scrollbars:Array<ScrollbarData>;
	var _texts:Map<String, Fast>;
	var _menuData:Array<MenuItemData>;
	
	public var viewClass(default, default):String;
	
	public function new() { }
	
	public function toString():String
	{
		return "### IndexPanelData ### \ntitle : " + _title + "\nx : " + _x + "\ny : " + _y + "\nmenuLists : " + _menuLists.toString() + "\nscrollbars : " + _scrollbars.toString() +
			"\ntexts : " + _texts.toString() + "\nmenuData : " + _menuData.toString() + "\n### END ###";
	}
	
	function get_title():String 
	{
		return _title;
	}
	
	function set_title(value:String):String
	{
		return _title = value;
	}
	
	public var title(get_title, set_title):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_texts():Map<String, Fast> 
	{
		return _texts;
	}
	
	function set_texts(value:Map<String, Fast>):Map<String, Fast> 
	{
		return _texts = value;
	}
	
	public var texts(get_texts, set_texts):Map<String, Fast>;
	
	function get_menuLists():Array<ListData> 
	{
		return _menuLists;
	}
	
	function set_menuLists(value:Array<ListData>):Array<ListData> 
	{
		return _menuLists = value;
	}
	
	public var menuLists(get_menuLists, set_menuLists):Array<ListData>;
	
	function get_scrollbars():Array<ScrollbarData> 
	{
		return _scrollbars;
	}
	
	function set_scrollbars(value:Array<ScrollbarData>):Array<ScrollbarData> 
	{
		return _scrollbars = value;
	}
	
	public var scrollbars(get_scrollbars, set_scrollbars):Array<ScrollbarData>;
	
	function get_menuData():Array<MenuItemData> 
	{
		return _menuData;
	}
	
	function set_menuData(value:Array<MenuItemData>):Array<MenuItemData> 
	{
		return _menuData = value;
	}
	
	public var menuData(get_menuData, set_menuData):Array<MenuItemData>;
	
}