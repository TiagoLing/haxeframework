package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class PageData implements IData
{
	var _id:String;
	//TODO: Change this to _url ???
	var _src:String;
	var _title:String;
	
	//Transitions
	var _pageTransitionIn:String;
	var _pageTransitionOut:String;
	var _contentTransitionIn:String;
	var _contentTransitionOut:String;
	var _showProgress:Bool;
	
	var _localIndex:Int;
	var _globalIndex:Int;
	var _numPagesInModule:Int;
	
	var _moduleId:String;
	var _moduleIndex:Int;
	
	var _contentUrls:Map<String, String>;
	
	//Various assets (additional XMLs, Bitmaps, etc)
	var _assets:Map<String, Dynamic>;
	
	//Page props
	//helpBtn, menuBtn, backBtn, bookmarkBtn, nextBtn
	var _pageProps:Array<Int>;
	
	public function new() {}
	
	public function toString():String
	{
		return "\n### PageData ###\n\t • id : " + _id + "\n\t • src : " + _src + "\n\t • title : " + _title + "\n\t • localIndex : " + _localIndex + "\n\t • globalIndex : " + _globalIndex +
			"\n\t • moduleId : " + _moduleId + "\n\t • moduleIndex : " + _moduleIndex + "\n\t • numPagesInModule : " + _numPagesInModule + "\n\t • contentUrl : " + _contentUrls + "\n### END ###";
	}
	
	//Page id
	function get_id():String
	{
		return _id;
	}
	
	function set_id(value:String):String 
	{
		return _id = value;
	}
	public var id(get_id, set_id):String;
	
	//Page src
	function get_src():String 
	{
		return _src;
	}
	
	function set_src(value:String):String 
	{
		return _src = value;
	}
	public var src(get_src, set_src):String;
	
	//Page title
	function get_title():String 
	{
		return _title;
	}
	
	function set_title(value:String):String 
	{
		return _title = value;
	}
	public var title(get_title, set_title):String;
	
	//Page localIndex (index inside module)
	function get_localIndex():Int 
	{
		return _localIndex;
	}
	
	function set_localIndex(value:Int):Int 
	{
		return _localIndex = value;
	}
	public var localIndex(get_localIndex, set_localIndex):Int;
	
	//Page globalIndex
	function get_globalIndex():Int 
	{
		return _globalIndex;
	}
	
	function set_globalIndex(value:Int):Int 
	{
		return _globalIndex = value;
	}
	public var globalIndex(get_globalIndex, set_globalIndex):Int;
	
	//numPagesInModule
	function get_numPagesInModule():Int 
	{
		return _numPagesInModule;
	}
	
	function set_numPagesInModule(value:Int):Int 
	{
		return _numPagesInModule = value;
	}
	public var numPagesInModule(get_numPagesInModule, set_numPagesInModule):Int;
	
	//Module id
	function get_moduleId():String 
	{
		return _moduleId;
	}
	
	function set_moduleId(value:String):String 
	{
		return _moduleId = value;
	}
	public var moduleId(get_moduleId, set_moduleId):String;
	
	//Module Index
	function get_moduleIndex():Int 
	{
		return _moduleIndex;
	}
	
	function set_moduleIndex(value:Int):Int 
	{
		return _moduleIndex = value;
	}
	public var moduleIndex(get_moduleIndex, set_moduleIndex):Int;
	
	function get_contentUrls():Map<String, String>
	{
		return _contentUrls;
	}
	
	function set_contentUrls(value:Map<String, String>):Map<String, String>
	{
		return _contentUrls = value;
	}
	
	public var contentUrls(get_contentUrls, set_contentUrls):Map<String, String>;
	
	function get_pageTransitionIn():String 
	{
		return _pageTransitionIn;
	}
	
	function set_pageTransitionIn(value:String):String 
	{
		return _pageTransitionIn = value;
	}
	
	public var pageTransitionIn(get_pageTransitionIn, set_pageTransitionIn):String;
	
	//_showProgress
	function get_showProgress():Bool 
	{
		return _showProgress;
	}
	
	function set_showProgress(value:Bool):Bool 
	{
		return _showProgress = value;
	}
	
	public var showProgress(get_showProgress, set_showProgress):Bool;
	
	function get_pageTransitionOut():String 
	{
		return _pageTransitionOut;
	}
	
	function set_pageTransitionOut(value:String):String 
	{
		return _pageTransitionOut = value;
	}
	
	public var pageTransitionOut(get_pageTransitionOut, set_pageTransitionOut):String;
	
	function get_contentTransitionIn():String 
	{
		return _contentTransitionIn;
	}
	
	function set_contentTransitionIn(value:String):String 
	{
		return _contentTransitionIn = value;
	}
	
	public var contentTransitionIn(get_contentTransitionIn, set_contentTransitionIn):String;
	
	function get_contentTransitionOut():String 
	{
		return _contentTransitionOut;
	}
	
	function set_contentTransitionOut(value:String):String 
	{
		return _contentTransitionOut = value;
	}
	
	public var contentTransitionOut(get_contentTransitionOut, set_contentTransitionOut):String;
	
	function get_pageProps():Array<Int> 
	{
		return _pageProps;
	}
	
	function set_pageProps(value:Array<Int>):Array<Int> 
	{
		return _pageProps = value;
	}
	
	public var pageProps(get_pageProps, set_pageProps):Array<Int>;
	
	function get_assets():Map<String, Dynamic> 
	{
		return _assets;
	}
	
	function set_assets(value:Map<String, Dynamic>):Map<String, Dynamic> 
	{
		return _assets = value;
	}
	
	public var assets(get_assets, set_assets):Map<String, Dynamic>;
}