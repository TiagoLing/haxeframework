package unbox.framework.model.vo;

/**
 * ...
 * @author UNBOX Design Studio
 */
class ScormParams
{
	// SCORM 1.2 PARAMS
	public static inline var PARAM_COMMENTS : String = "cmi.comments";
	public static inline var PARAM_CREDIT : String = "cmi.core.credit";
	public static inline var PARAM_ENTRY : String = "cmi.core.entry";
	public static inline var PARAM_EXIT : String = "cmi.core.exit";
	public static inline var PARAM_LAUNCHA_DATA : String = "cmi.launch_data";
	public static inline var PARAM_LESSON_LOCATION : String = "cmi.core.lesson_location";
	public static inline var PARAM_LESSON_MODE : String = "cmi.core.lesson_mode";
	public static inline var PARAM_LESSON_STATUS : String = "cmi.core.lesson_status";
	public static inline var PARAM_SCORE_MAX : String = "cmi.core.score.max";
	public static inline var PARAM_SCORE_MIN : String = "cmi.core.score.min";
	public static inline var PARAM_SCORE_RAW : String = "cmi.core.score.raw";
	public static inline var PARAM_SESSION_TIME : String = "cmi.core.session_time";
	public static inline var PARAM_STUDENT_ID : String = "cmi.core.student_id";
	public static inline var PARAM_STUDENT_NAME : String = "cmi.core.student_name";
	public static inline var PARAM_SUSPEND_DATA : String = "cmi.suspend_data";
	public static inline var PARAM_TOTAL_TIME : String = "cmi.core.total_time";
	
	public function new() {}
}