package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class ConfigData implements IData
{
	//Maybe use _courseMode:String instead? ("browse" | "release" | "editable")
	var _browseMode:Bool;
	var _accessible:Bool;
	var _debugActive:Bool;
	var _dataServiceType:String;
	
	public function new() {}
	
	public function toString():String
	{
		return "### ConfigData ### \nbrowseMode : " + _browseMode + "\ndebugActive : " + _debugActive + "\ndataServiceType : " + _dataServiceType + "\n### END ###";
	}
	
	function get_browseMode():Bool 
	{
		return _browseMode;
	}
	
	function set_browseMode(value:Bool):Bool 
	{
		return _browseMode = value;
	}
	public var browseMode(get_browseMode, set_browseMode):Bool;
	
	function get_accessible():Bool
	{
		return _accessible;
	}
	
	function set_accessible(value:Bool):Bool
	{
		return _accessible = value;
	}
	
	public var accessible(get_accessible, set_accessible):Bool;
	
	function get_debugActive():Bool 
	{
		return _debugActive;
	}
	
	function set_debugActive(value:Bool):Bool 
	{
		return _debugActive = value;
	}
	public var debugActive(get_debugActive, set_debugActive):Bool;
	
	function get_dataServiceType():String 
	{
		return _dataServiceType;
	}
	
	function set_dataServiceType(value:String):String 
	{
		return _dataServiceType = value;
	}
	public var dataServiceType(get_dataServiceType, set_dataServiceType):String;
	
}