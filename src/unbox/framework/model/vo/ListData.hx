package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class ListData implements IData
{
	var _name:String;
	var _x:Float;
	var _y:Float;
	var _width:Int;
	var _height:Int;
	var _columns:Int;
	var _rows:Int;
	var _spacing:Int;
	
	public var viewClass(default, default):String;
	
	public function new() {}
	
	public function toString():String
	{
		return "### ListData ### \nname : " + _name + "\nx : " + _x + "\ny : " + _y + "\nwidth : " + _width + "\nheight : " + _height + "\ncolumns : " + _columns +
			"\nrows : " + _rows + "\nspacing : " + _spacing + "\n### END ###";
	}
	
	function get_name():String 
	{
		return _name;
	}
	
	function set_name(value:String):String 
	{
		return _name = value;
	}
	
	public var name(get_name, set_name):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_width():Int 
	{
		return _width;
	}
	
	function set_width(value:Int):Int 
	{
		return _width = value;
	}
	
	public var width(get_width, set_width):Int;
	
	function get_height():Int 
	{
		return _height;
	}
	
	function set_height(value:Int):Int 
	{
		return _height = value;
	}
	
	public var height(get_height, set_height):Int;
	
	function get_columns():Int 
	{
		return _columns;
	}
	
	function set_columns(value:Int):Int 
	{
		return _columns = value;
	}
	
	public var columns(get_columns, set_columns):Int;
	
	function get_rows():Int 
	{
		return _rows;
	}
	
	function set_rows(value:Int):Int 
	{
		return _rows = value;
	}
	
	public var rows(get_rows, set_rows):Int;
	
	function get_spacing():Int 
	{
		return _spacing;
	}
	
	function set_spacing(value:Int):Int 
	{
		return _spacing = value;
	}
	
	public var spacing(get_spacing, set_spacing):Int;
	
}