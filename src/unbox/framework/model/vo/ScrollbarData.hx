package unbox.framework.model.vo;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class ScrollbarData implements IData
{
	var _x:Float;
	var _y:Float;
	var _width:Int;
	var _height:Int;
	var _scrollbarColor:Int;
	var _scrollbarColorAlpha:Float;
	var _thumbColor:Int;
	var _thumbColorAlpha:Float;
	var _autoHideThumb:Bool;
	
	public function new() { }
	
	public function toString():String
	{
		return "### ScrollbarData ### \nx : " + _x + "\ny : " + _y + "\nwidth : " + _width + "\nheight : " + _height + "\nscrollbarColor : " + _scrollbarColor + "\nscrollbarColorAlpha : " + _scrollbarColorAlpha +
			"\nthumbColor : " + _thumbColor + "\nthumbColorAlpha : " + _thumbColorAlpha + "\nautoHideThumb : " + _autoHideThumb + "\n### END ###";
	}
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_width():Int 
	{
		return _width;
	}
	
	function set_width(value:Int):Int 
	{
		return _width = value;
	}
	
	public var width(get_width, set_width):Int;
	
	function get_height():Int 
	{
		return _height;
	}
	
	function set_height(value:Int):Int 
	{
		return _height = value;
	}
	
	public var height(get_height, set_height):Int;
	
	function get_scrollbarColor():Int 
	{
		return _scrollbarColor;
	}
	
	function set_scrollbarColor(value:Int):Int 
	{
		return _scrollbarColor = value;
	}
	
	public var scrollbarColor(get_scrollbarColor, set_scrollbarColor):Int;
	
	function get_scrollbarColorAlpha():Float 
	{
		return _scrollbarColorAlpha;
	}
	
	function set_scrollbarColorAlpha(value:Float):Float 
	{
		return _scrollbarColorAlpha = value;
	}
	
	public var scrollbarColorAlpha(get_scrollbarColorAlpha, set_scrollbarColorAlpha):Float;
	
	function get_thumbColor():Int 
	{
		return _thumbColor;
	}
	
	function set_thumbColor(value:Int):Int 
	{
		return _thumbColor = value;
	}
	
	public var thumbColor(get_thumbColor, set_thumbColor):Int;
	
	function get_thumbColorAlpha():Float 
	{
		return _thumbColorAlpha;
	}
	
	function set_thumbColorAlpha(value:Float):Float 
	{
		return _thumbColorAlpha = value;
	}
	
	public var thumbColorAlpha(get_thumbColorAlpha, set_thumbColorAlpha):Float;
	
	function get_autoHideThumb():Bool 
	{
		return _autoHideThumb;
	}
	
	function set_autoHideThumb(value:Bool):Bool 
	{
		return _autoHideThumb = value;
	}
	
	public var autoHideThumb(get_autoHideThumb, set_autoHideThumb):Bool;
	
}