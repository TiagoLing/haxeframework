package unbox.framework.model.vo;

/**
 * @author Tiago Ling Alexandre
 */

enum PageDataType 
{
	Scorm;
	Navigation;
	Custom;
}