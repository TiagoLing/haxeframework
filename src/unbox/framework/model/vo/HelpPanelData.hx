package unbox.framework.model.vo;
import haxe.xml.Fast;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class HelpPanelData implements IData
{
	var _depth:Int;
	var _title:String;
	var _x:Float;
	var _y:Float;
	var _width:Int;
	var _height:Int;
	var _texts:Map<String, Fast>;
	
	public var viewClass(default, default):String;
	
	public function new() { }
	
	public function toString():String
	{
		return "### HelpPanelData ### \ndepth : " + _depth + "\ntitle : " + _title + "\nx : " + _x + "\ny : " + _y + "\nwidth : " + _width + "\nheight : " + _height + "\ntexts : " + _texts.toString() + "\n### END ###";
	}
	
	function get_depth():Int 
	{
		return _depth;
	}
	
	function set_depth(value:Int):Int 
	{
		return _depth = value;
	}
	
	public var depth(get_depth, set_depth):Int;
	
	function get_title():String 
	{
		return _title;
	}
	
	function set_title(value:String):String 
	{
		return _title = value;
	}
	
	public var title(get_title, set_title):String;
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_width():Int 
	{
		return _width;
	}
	
	function set_width(value:Int):Int 
	{
		return _width = value;
	}
	
	public var width(get_width, set_width):Int;
	
	function get_height():Int 
	{
		return _height;
	}
	
	function set_height(value:Int):Int 
	{
		return _height = value;
	}
	
	public var height(get_height, set_height):Int;
	
	function get_texts():Map<String, Fast> 
	{
		return _texts;
	}
	
	function set_texts(value:Map<String, Fast>):Map<String, Fast> 
	{
		return _texts = value;
	}
	
	public var texts(get_texts, set_texts):Map<String, Fast>;
	
}