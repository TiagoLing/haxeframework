package unbox.framework.model;
import haxe.xml.Fast;
import unbox.framework.model.vo.AboutPanelData;
import unbox.framework.model.vo.BookmarkPanelData;
import unbox.framework.model.vo.DashboardData;
import unbox.framework.model.vo.HelpPanelData;
import unbox.framework.model.vo.IndexPanelData;
import unbox.framework.model.vo.ListData;
import unbox.framework.model.vo.MenuItemData;
import unbox.framework.model.vo.NavBarData;
import unbox.framework.model.vo.ProgressMeterData;
import unbox.framework.model.vo.ScrollbarData;
import unbox.framework.model.vo.SearchBoxData;
import unbox.framework.model.vo.SearchPanelData;
import unbox.framework.model.vo.SubMenuItemData;
import unbox.framework.model.vo.TooltipData;
import unbox.framework.utils.Logger;

/**
 * Stores parsed data from ebook_ui.xml
 * @author Tiago Ling Alexandre
 */
class UIModel
{
	var _rawData:Fast;
	var _progressMeterData:ProgressMeterData;
	var _navBarData:NavBarData;
	var _dashboardData:DashboardData;

	public function new(navData:Xml) 
	{
		_rawData = new Fast(navData.firstElement());
		init();
	}
	
	function init():Void
	{
		for (object in _rawData.nodes.object)
		{
			switch (object.att.type)
			{
				case "ProgressMeter":
					parseProgressMeter(object);
				case "NavBar":
					parseNavBar(object);
				case "Dashboard":
					parseDashboard(object);
/*				case "HelpPanel":
					parseHelpPanel(object);*/
				default:
					Logger.log("Xml parsing problem: Nav <object> type '" + object.att.type + "' mapping not yet implemented.");
			}
		}
	}
	
	function tracelogData():Void
	{
		Logger.log("@@@ UI MODEL @@@ \n" + _progressMeterData.toString() + "\n" + _navBarData.toString() + "\n" + _dashboardData.toString() + "\n@@@ END @@@");
	}
	
	function parseProgressMeter(data:Fast):Void
	{
		trace( "UIModel.parseProgressMeter" );
		_progressMeterData = new ProgressMeterData();
		_progressMeterData.className = data.att.className;
		_progressMeterData.depth = Std.parseInt(data.att.depth);
		_progressMeterData.instanceName = data.att.instanceName;
		_progressMeterData.x = Std.parseFloat(data.att.x);
		_progressMeterData.y = Std.parseFloat(data.att.y);
		_progressMeterData.openX = Std.parseFloat(data.att.openX);
		_progressMeterData.openY = Std.parseFloat(data.att.openY);
		_progressMeterData.width = Std.parseInt(data.att.width);
		_progressMeterData.height = Std.parseInt(data.att.height);
		_progressMeterData.scrollbarColor = Std.parseInt(data.att.scrollbarColor);
		_progressMeterData.scrollbarColorAlpha = Std.parseFloat(data.att.scrollbarColorAlpha);
		_progressMeterData.thumbColor = Std.parseInt(data.att.thumbColor);
		_progressMeterData.thumbColorAlpha = Std.parseFloat(data.att.thumbColorAlpha);
		_progressMeterData.thumbFillMode = data.att.thumbFillMode == "true" ? true : false;
		_progressMeterData.autoHideThumb = data.att.autoHideThumb == "true" ? true : false;
	}
	
	function parseNavBar(data:Fast):Void
	{
		trace( "UIModel.parseNavBar" );
		_navBarData = new NavBarData();
		_navBarData.texts = new Map<String, String>();
		_navBarData.instanceName = data.att.instanceName;
		_navBarData.x = Std.parseFloat(data.att.x);
		_navBarData.y = Std.parseFloat(data.att.y);
		
		//TODO: Map <tween> tags
		for (content in data.nodes.content)
		{
			switch (content.att.type)
			{
				case "tooltip":
					_navBarData.tooltips = new TooltipData();
					_navBarData.tooltips.hook = content.att.hook == "true" ? true : false;
					_navBarData.tooltips.hookSize = Std.parseInt(content.att.hookSize);
					_navBarData.tooltips.autoSize = content.att.autoSize == "true" ? true : false;
					_navBarData.tooltips.followCursor = content.att.followCursor == "true" ? true : false;
					_navBarData.tooltips.align = content.att.align;
					_navBarData.tooltips.cornerRadius = Std.parseFloat(content.att.cornerRadius);
					_navBarData.tooltips.showDelay = Std.parseInt(content.att.showDelay);
					_navBarData.tooltips.hideDelay = Std.parseInt(content.att.hideDelay);
					_navBarData.tooltips.bgAlpha = Std.parseFloat(content.att.bgAlpha);
					_navBarData.tooltips.width = Std.parseInt(content.att.width);
					_navBarData.tooltips.padding = Std.parseFloat(content.att.padding);
					_navBarData.tooltips.colors = new Array<UInt>();
					var rawColors:Array<String> = content.att.colors.split(",");
					_navBarData.tooltips.colors[0] = Std.parseInt(rawColors[0]);
					_navBarData.tooltips.colors[1] = Std.parseInt(rawColors[1]);
					_navBarData.tooltips.items = new Map<String, String>();
					var i:Int = 0;
					for (tooltip in content.nodes.content)
					{
						var nodeData:String = tooltip.hasNode.body ? tooltip.node.body.innerHTML : (tooltip.hasNode.title ? tooltip.node.title.innerHTML : "<![CDATA[<p>ERROR:NODATA</p>]]>");
						if (tooltip.has.instanceName)
							_navBarData.tooltips.items.set(tooltip.att.instanceName, nodeData);
						else
							_navBarData.tooltips.items.set("tooltip_" + i, nodeData);
							
						i++;
					}
					
				case "text":
					for (text in content.nodes.content)
					{
						_navBarData.texts.set(text.att.instanceName, text.node.title.innerHTML);
					}
					
				case "tween":
					_navBarData.tweenData = content;
				default:
					trace("NavBarData XML element not recognized : " + content.att.type);
			} 
		}
	}
	
	function parseDashboard(data:Fast):Void
	{
		_dashboardData = new DashboardData();
		_dashboardData.x = Std.parseFloat(data.att.x);
		_dashboardData.y = Std.parseFloat(data.att.y);
		_dashboardData.openX = Std.parseFloat(data.att.openX);
		_dashboardData.openY = Std.parseFloat(data.att.openY);
		
		for (object in data.nodes.object)
		{
			switch (object.att.type)
			{
				case "aboutPanel":
					parseAboutPanel(object);
				case "bookmarkPanel":
					parseBookmarkPanel(object);
				case "indexPanel":
					parseIndexPanel(object);
				case "searchPanel":
					parseSearchPanel(object);
				case "searchBox":
					parseSearchBox(object);
				case "HelpPanel":
					parseHelpPanel(object);
				case "tooltip":
					parseTooltip(object);
				default:
					Logger.log("Xml parsing problem: Dashboard <object> type '" + object.att.type + "' mapping not yet implemented.");
			}
		}
	}
	
	function parseAboutPanel(data:Fast):Void {
		trace( "UIModel.parseAboutPanel");
		_dashboardData.aboutPanel = new AboutPanelData();
		_dashboardData.aboutPanel.title = data.att.title;
		_dashboardData.aboutPanel.x = Std.parseFloat(data.att.x);
		_dashboardData.aboutPanel.y = Std.parseFloat(data.att.y);
		_dashboardData.aboutPanel.viewClass = data.att.viewClass;
		_dashboardData.aboutPanel.texts = new Map<String, Fast>();
		var i:Int = 0;
		for (text in data.node.content.nodes.content)
		{
			//Using Fast instead of String for use with ContentParser + Vars
			if (text.has.instanceName)
			{
				_dashboardData.aboutPanel.texts.set(text.att.instanceName, text);
			} else {
				_dashboardData.aboutPanel.texts.set("text_" + i, text);
				i++;
			}
		}
	}
	
	function parseBookmarkPanel(data:Fast):Void {
		trace( "UIModel.parseBookmarkPanel");
		_dashboardData.bookmarkPanel = new BookmarkPanelData();
		_dashboardData.bookmarkPanel.title = data.att.title;
		_dashboardData.bookmarkPanel.x = Std.parseFloat(data.att.x);
		_dashboardData.bookmarkPanel.y = Std.parseFloat(data.att.y);
		_dashboardData.bookmarkPanel.list = new ListData();
		_dashboardData.bookmarkPanel.scrollbar = new ScrollbarData();
		_dashboardData.bookmarkPanel.texts = new Map<String, Fast>();
		_dashboardData.bookmarkPanel.viewClass = data.att.viewClass;
		for (subObject in data.nodes.object)
		{
			if (subObject.att.type == "List")
			{
				_dashboardData.bookmarkPanel.list.name = subObject.att.name;
				_dashboardData.bookmarkPanel.list.x = Std.parseFloat(subObject.att.x);
				_dashboardData.bookmarkPanel.list.y = Std.parseFloat(subObject.att.y);
				_dashboardData.bookmarkPanel.list.width = Std.parseInt(subObject.att.width);
				_dashboardData.bookmarkPanel.list.height = Std.parseInt(subObject.att.height);
				_dashboardData.bookmarkPanel.list.columns = Std.parseInt(subObject.att.columns);
				_dashboardData.bookmarkPanel.list.rows = Std.parseInt(subObject.att.rows);
				_dashboardData.bookmarkPanel.list.spacing = Std.parseInt(subObject.att.spacing);
				_dashboardData.bookmarkPanel.list.viewClass = subObject.att.viewClass;
			}
			else if (subObject.att.type == "Scrollbar")
			{
				_dashboardData.bookmarkPanel.scrollbar.x = Std.parseFloat(subObject.att.x);
				_dashboardData.bookmarkPanel.scrollbar.y = Std.parseFloat(subObject.att.y);
				_dashboardData.bookmarkPanel.scrollbar.width = Std.parseInt(subObject.att.width);
				_dashboardData.bookmarkPanel.scrollbar.height = Std.parseInt(subObject.att.height);
				_dashboardData.bookmarkPanel.scrollbar.scrollbarColor = Std.parseInt(subObject.att.scrollbarColor);
				_dashboardData.bookmarkPanel.scrollbar.scrollbarColorAlpha = Std.parseFloat(subObject.att.scrollbarColorAlpha);
				_dashboardData.bookmarkPanel.scrollbar.thumbColor = Std.parseInt(subObject.att.thumbColor);
				_dashboardData.bookmarkPanel.scrollbar.thumbColorAlpha = Std.parseFloat(subObject.att.thumbColorAlpha);
				_dashboardData.bookmarkPanel.scrollbar.autoHideThumb = subObject.att.autoHideThumb == "true" ? true : false;
			}
		}
		
		var i:Int = 0;
		for (text in data.node.content.nodes.content)
		{
			if (text.has.instanceName) {
				_dashboardData.bookmarkPanel.texts.set(text.att.instanceName, text);
			} else {
				_dashboardData.bookmarkPanel.texts.set("text_" + i, text);
				i++;
			}
		}
	}
	
	function parseIndexPanel(data:Fast):Void {
		trace( "UIModel.parseIndexPanel");
		_dashboardData.indexPanel = new IndexPanelData();
		_dashboardData.indexPanel.title = data.att.title;
		_dashboardData.indexPanel.x = Std.parseFloat(data.att.x);
		_dashboardData.indexPanel.y = Std.parseFloat(data.att.y);
		_dashboardData.indexPanel.viewClass = data.att.viewClass;
		_dashboardData.indexPanel.menuLists = new Array<ListData>();
		_dashboardData.indexPanel.scrollbars = new Array<ScrollbarData>();
		for (subObject in data.nodes.object)
		{
			if (subObject.att.type == "List")
			{
				var list:ListData = new ListData();
				list.name = subObject.att.name;
				list.x = Std.parseFloat(subObject.att.x);
				list.y = Std.parseFloat(subObject.att.y);
				list.width = Std.parseInt(subObject.att.width);
				list.height = Std.parseInt(subObject.att.height);
				list.columns = Std.parseInt(subObject.att.columns);
				list.rows = Std.parseInt(subObject.att.rows);
				list.spacing = Std.parseInt(subObject.att.spacing);
				list.viewClass = subObject.att.viewClass;
				_dashboardData.indexPanel.menuLists.push(list);
			}
			else if (subObject.att.type == "Scrollbar")
			{
				var scrollbar:ScrollbarData = new ScrollbarData();
				scrollbar.x = Std.parseFloat(subObject.att.x);
				scrollbar.y = Std.parseFloat(subObject.att.y);
				scrollbar.width = Std.parseInt(subObject.att.width);
				scrollbar.height = Std.parseInt(subObject.att.height);
				scrollbar.scrollbarColor = Std.parseInt(subObject.att.scrollbarColor);
				scrollbar.scrollbarColorAlpha = Std.parseFloat(subObject.att.scrollbarColorAlpha);
				scrollbar.thumbColor = Std.parseInt(subObject.att.thumbColor);
				scrollbar.thumbColorAlpha = Std.parseFloat(subObject.att.thumbColorAlpha);
				scrollbar.autoHideThumb = subObject.att.autoHideThumb == "true" ? true : false;
				_dashboardData.indexPanel.scrollbars.push(scrollbar);
			}
		}
		_dashboardData.indexPanel.menuData = new Array<MenuItemData>();
		_dashboardData.indexPanel.texts = new Map<String, Fast>();
		var i:Int = 0;
		for (content in data.nodes.content)
		{
			if (content.att.type == "menu")
			{
				for (menuItem in content.nodes.content)
				{
					var menuData:MenuItemData = new MenuItemData();
					menuData.title = menuItem;
					menuData.pageId = menuItem.att.pageID;
					menuData.lastPageId = menuItem.att.lastPageID;
					menuData.submenuItems = new Array<SubMenuItemData>();
					for (submenuItem in menuItem.nodes.content)
					{
						var submenuItemData:SubMenuItemData = new SubMenuItemData();
						submenuItemData.title = submenuItem;
						submenuItemData.pageId = submenuItem.att.pageID;
						submenuItemData.lastPageId = submenuItem.att.lastPageID;
						menuData.submenuItems.push(submenuItemData);
					}
					_dashboardData.indexPanel.menuData.push(menuData);
				}
			}
			else if (content.att.type == "text")
			{
				for (textNode in content.nodes.content)
				{
					if (textNode.has.instanceName) {
						_dashboardData.indexPanel.texts.set(textNode.att.instanceName, textNode);
					} else {
						_dashboardData.indexPanel.texts.set("text_" + i, textNode);
						i++;
					}
				}
			}
		}
	}
	
	function parseSearchPanel(data:Fast):Void {
		trace( "UIModel.parseSearchPanel");
		_dashboardData.searchPanel = new SearchPanelData();
		_dashboardData.searchPanel.title = data.att.title;
		_dashboardData.searchPanel.x = Std.parseFloat(data.att.x);
		_dashboardData.searchPanel.y = Std.parseFloat(data.att.y);
		_dashboardData.searchPanel.summaryLength = Std.parseInt(data.att.summaryLength);
		_dashboardData.searchPanel.viewClass = data.att.viewClass;
		_dashboardData.searchPanel.list = new ListData();
		_dashboardData.searchPanel.scrollbar = new ScrollbarData();
		_dashboardData.searchPanel.texts = new Map<String, Fast>();
		for (object in data.nodes.object)
		{
			if (object.att.type == "List")
			{
				_dashboardData.searchPanel.list.name = object.att.name;
				_dashboardData.searchPanel.list.x = Std.parseFloat(object.att.x);
				_dashboardData.searchPanel.list.y = Std.parseFloat(object.att.y);
				_dashboardData.searchPanel.list.width = Std.parseInt(object.att.width);
				_dashboardData.searchPanel.list.height = Std.parseInt(object.att.height);
				_dashboardData.searchPanel.list.columns = Std.parseInt(object.att.columns);
				_dashboardData.searchPanel.list.rows = Std.parseInt(object.att.rows);
				_dashboardData.searchPanel.list.spacing = Std.parseInt(object.att.spacing);
				_dashboardData.searchPanel.list.viewClass = object.att.viewClass;
			}
			else if (object.att.type == "Scrollbar")
			{
				_dashboardData.searchPanel.scrollbar.x = Std.parseFloat(object.att.x);
				_dashboardData.searchPanel.scrollbar.y = Std.parseFloat(object.att.y);
				_dashboardData.searchPanel.scrollbar.width = Std.parseInt(object.att.width);
				_dashboardData.searchPanel.scrollbar.height = Std.parseInt(object.att.height);
				_dashboardData.searchPanel.scrollbar.scrollbarColor = Std.parseInt(object.att.scrollbarColor);
				_dashboardData.searchPanel.scrollbar.scrollbarColorAlpha = Std.parseFloat(object.att.scrollbarColorAlpha);
				_dashboardData.searchPanel.scrollbar.thumbColor = Std.parseInt(object.att.thumbColor);
				_dashboardData.searchPanel.scrollbar.thumbColorAlpha = Std.parseFloat(object.att.thumbColorAlpha);
				_dashboardData.searchPanel.scrollbar.autoHideThumb = object.att.autoHideThumb == "true" ? true : false;
			}
		}
		var i:Int = 0;
		for (text in data.node.content.nodes.content)
		{
			if (text.has.instanceName) {
				_dashboardData.searchPanel.texts.set(text.att.instanceName, text);
			} else {
				_dashboardData.searchPanel.texts.set("text_" + i, text);
				i++;
			}
		}
	}
	
	function parseSearchBox(data:Fast):Void {
		trace( "UIModel.parseSearchBox");
		_dashboardData.searchBox = new SearchBoxData();
		_dashboardData.searchBox.title = data.att.title;
		_dashboardData.searchBox.x = Std.parseFloat(data.att.x);
		_dashboardData.searchBox.y = Std.parseFloat(data.att.y);
		_dashboardData.searchBox.loadTitle = data.att.loadTitle;
		_dashboardData.searchBox.minChars = Std.parseInt(data.att.minChars);
		_dashboardData.searchBox.viewClass = data.att.viewClass;
		_dashboardData.searchBox.texts = new Map<String, Fast>();
		_dashboardData.searchBox.alwaysVisible = data.att.alwaysVisible == "true" ? true : false;
		
		var i:Int = 0;
		for (text in data.node.content.nodes.content)
		{
			if (text.has.instanceName) {
				_dashboardData.searchBox.texts.set(text.att.instanceName, text);
			} else {
				_dashboardData.searchBox.texts.set("text_" + i, text);
				i++;
			}
		}
	}
	
	function parseTooltip(data:Fast):Void {
		trace( "UIModel.parseTooltip");
		_dashboardData.tooltips = new TooltipData();
		_dashboardData.tooltips.hook = data.att.hook == "true" ? true : false;
		_dashboardData.tooltips.hookSize = Std.parseInt(data.att.hookSize);
		_dashboardData.tooltips.autoSize = data.att.autoSize == "true" ? true : false;
		_dashboardData.tooltips.followCursor = data.att.followCursor == "true" ? true : false;
		_dashboardData.tooltips.align = data.att.align;
		_dashboardData.tooltips.cornerRadius = Std.parseFloat(data.att.cornerRadius);
		_dashboardData.tooltips.showDelay = Std.parseInt(data.att.showDelay);
		_dashboardData.tooltips.hideDelay = Std.parseInt(data.att.hideDelay);
		_dashboardData.tooltips.bgAlpha = Std.parseFloat(data.att.bgAlpha);
		_dashboardData.tooltips.width = Std.parseInt(data.att.width);
		_dashboardData.tooltips.padding = Std.parseFloat(data.att.padding);
		_dashboardData.tooltips.colors = new Array<UInt>();
		var rawColors:Array<String> = data.att.colors.split(",");
		_dashboardData.tooltips.colors[0] = Std.parseInt(rawColors[0]);
		_dashboardData.tooltips.colors[1] = Std.parseInt(rawColors[1]);
		_dashboardData.tooltips.items = new Map<String, String>();
		var i:Int = 0;
		for (tooltip in data.nodes.content)
		{
			var nodeData:String = tooltip.hasNode.body ? tooltip.node.body.innerHTML : (tooltip.hasNode.title ? tooltip.node.title.innerHTML : "<![CDATA[<p>ERROR:NODATA</p>]]>");
			if (tooltip.has.instanceName)
				_dashboardData.tooltips.items.set(tooltip.att.instanceName, nodeData);
			else
				_dashboardData.tooltips.items.set("tooltip_" + i, nodeData);
			i++;
		}
	}
	
	function parseHelpPanel(data:Fast):Void
	{
		trace( "UIModel.parseHelpPanel");
		_dashboardData.helpPanel = new HelpPanelData();
		_dashboardData.helpPanel.depth = Std.parseInt(data.att.depth);
		_dashboardData.helpPanel.title = data.att.title;
		_dashboardData.helpPanel.x = Std.parseFloat(data.att.x);
		_dashboardData.helpPanel.y = Std.parseFloat(data.att.y);
		_dashboardData.helpPanel.width = Std.parseInt(data.att.width);
		_dashboardData.helpPanel.height = Std.parseInt(data.att.height);
		_dashboardData.helpPanel.viewClass = data.att.viewClass;
		_dashboardData.helpPanel.texts = new Map<String, Fast>();
		var i:Int = 0;
		for (text in data.node.content.nodes.content)
		{
			if (text.has.instanceName) {
				_dashboardData.helpPanel.texts.set(text.att.instanceName, text);
			} else {
				_dashboardData.helpPanel.texts.set("text_" + i, text);
				i++;
			}
		}
	}
	
	function get_progressMeterData():ProgressMeterData 
	{
		return _progressMeterData;
	}
	
	function set_progressMeterData(value:ProgressMeterData):ProgressMeterData 
	{
		return _progressMeterData = value;
	}
	
	public var progressMeterData(get_progressMeterData, set_progressMeterData):ProgressMeterData;
	
	function get_navBarData():NavBarData 
	{
		return _navBarData;
	}
	
	function set_navBarData(value:NavBarData):NavBarData 
	{
		return _navBarData = value;
	}
	
	public var navBarData(get_navBarData, set_navBarData):NavBarData;
	
	function get_dashboardData():DashboardData 
	{
		return _dashboardData;
	}
	
	function set_dashboardData(value:DashboardData):DashboardData 
	{
		return _dashboardData = value;
	}
	
	public var dashboardData(get_dashboardData, set_dashboardData):DashboardData;
}