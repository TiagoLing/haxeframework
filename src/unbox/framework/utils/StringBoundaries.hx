/*
Copyright (c) 2007 FlexLib Contributors.  See:
    http://code.google.com/p/flexlib/wiki/ProjectContributors

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package unbox.framework.utils;

import openfl.geom.Rectangle;
import openfl.text.TextField;

class StringBoundaries
{
	/**
	* The TextField the string is in.
	*/
	var textField:TextField;
	
	/**
	* The first line of text the string inhabits.
	*/
	var startLine:Int;
	
	/**
	* The last line of text the string inhabits.
	*/
	var endLine:Int;
	
	/**
	* The index of the first character in the string.
	*/
	var startIndex:Int;
	
	/**
	* The index of the last character in the string.
	*/
	var endIndex:Int;
	
	/**
	* The horizontal offset to apply to the bounding rectangle.
	*/
	var xOffset:Float;
	
	/**
	* The vertical offset to apply to the bounding rectangle.
	*/
	var yOffset:Float;
	
	/**
	* TextField.getCharBoundaries seems to be consistently innaccurate by this amount in the x-axis.
	*/
	public var X_CORRECTION:Float = 1;
	
	/**
	* TextField.getCharBoundaries seems to be consistently innaccurate by this amount in the y-axis.
	*/
	public var Y_CORRECTION:Float = 2;
	
	/**
	* Finds the bounding rectangle of a character range within a TextField object.  If the character range spans multiple lines, bounding rectangles are calculated for each line.
	* 
	* @param textField The TextField the string is in.
	* @param startIndex The start index of the character range.
	* @param endIndex The end index of the character range.
	* @param xOffset The horizontal offset to apply to the boundary rectangle.
	* @param yOffset The vertical offset to apply to the bounding rectangle.
	*/
	public function new (textField:TextField,startIndex:Int,endIndex:Int,xOffset:Float=0,yOffset:Float=0)
	{
		this.textField = textField;
		this.startLine = textField.getLineIndexOfChar(startIndex);
		this.endLine = textField.getLineIndexOfChar(endIndex);
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}
	
	/**
	* Returns the bounding rectangles of the current character range.
	*/
	public function getRectangles():Array<Rectangle>{
		var rects:Array<Rectangle>;

		// Only return rects in the visible area of the TextField
		var firstVisibleIndex:Int = this.textField.getLineOffset(this.textField.scrollV-1);
		var lastVisibleIndex:Int = this.getLineEndOffset(this.textField.bottomScrollV-1);
		
		// If the visible area of the TextField is larger than the actual character range, only return rects within the character range instead.
		firstVisibleIndex = Std.int(Math.max(firstVisibleIndex, this.startIndex));
		lastVisibleIndex = Std.int(Math.min(lastVisibleIndex, this.endIndex));
		
		// If the character range spans multiple lines, get bounding rects for each line.
		// Otherwise, just get one bounding rect for the whole character range.
		if(this.getIsMultiline() == true){
			rects = this.getLineRects(firstVisibleIndex, lastVisibleIndex);
		}else{
			rects = [this.stringRect(firstVisibleIndex, lastVisibleIndex)];
		}
		
		return rects;
	}
	
	/**
	* Indicates whether or not the character range spans multiple lines.
	*/
	function getIsMultiline():Bool{
		if(this.endLine > this.startLine){
			return true;
		}
		return false;
	}
	
	/**
	* Indicates whether or not the character range has visible characters.
	*/
	public function getIsVisible():Bool{
		var firstVisibleChar:Int = this.textField.getLineOffset(this.textField.scrollV-1);
		var lastVisibleChar:Int = this.getLineEndOffset(this.textField.bottomScrollV - 1);
		
		if(this.endIndex >= firstVisibleChar && this.startIndex <= lastVisibleChar){
			return true;
		}
		
		return false;
	}
	
	/** 	
	* Returns an array of Rectangles representing the boundaries of a multiline character range.
	* 
	* @param startIndex The start index of the character range
	* @param endIndex The end index of the character range
	*/
	function getLineRects(startIndex:Int, endIndex:Int):Array<Rectangle>{
		var r:Array<Rectangle> = new Array<Rectangle>();
		
		var startLn:Int = this.textField.getLineIndexOfChar(startIndex);
		var endLn:Int = this.textField.getLineIndexOfChar(endIndex);
		
		var numLines:Int = endLn - startLn;
		
		var startLineEndOffset:Int = this.getLineEndOffset(startLn);
		
		r.push(this.stringRect(startIndex,startLineEndOffset));
		
		for (i in 1...numLines) {
			var line:Int = startLn + i;
			var ind1:Int = textField.getLineOffset(line);
			var ind2:Int = this.getLineEndOffset(line);
			r.push(this.stringRect(ind1,ind2));
		}
		
		var endLineOffset:Int = textField.getLineOffset(endLn);
		r.push(this.stringRect(endLineOffset,endIndex));
		
		return r;
	}
	
	/**	
	* Returns a Rectangle representing the boundaries of a singleline character range.
	* 
	* @param startIndex The start index of the character range
	* @param endIndex The end index of the character range
	*/
	function stringRect(startIndex:Int, endIndex:Int):Rectangle{
		
		// If this is a single character, simply return the character boundaries
		if(startIndex == endIndex){
			var rect:Rectangle = this.getAdjustedCharBoundaries(startIndex,true);
			return rect;
		}
		
		// Use lineHeight instead of Rectangle.height so that multi-line 
		// highlights don't have spaces between lines
		var thisLine:Int = this.textField.getLineIndexOfChar(startIndex);
		var lineHeight:Float = this.textField.getLineMetrics(thisLine).height;
		
		var rect1:Rectangle = this.getAdjustedCharBoundaries(startIndex,true);
		var rect2:Rectangle = this.getAdjustedCharBoundaries(endIndex,false);
		
		var r:Rectangle = new Rectangle(rect1.x, rect1.y, rect2.right - rect1.x, lineHeight);
		return r;
	}
	
	/**
	* Returns the index of the last character in a line of text 
	*/
	function getLineEndOffset(lineIndex:Int):Int{
		return textField.getLineOffset(lineIndex) + textField.getLineLength(lineIndex);
	}
	
	/**
	* Returns the Y position of a character's bounding rect in consideration of the vertical scroll position of the TextField.
	* <p>Because getCharBoundaries returns the same bounding rect regardless of the TextField's scroll position, it is necessary to adjust for it.</p> 
	* <p>adjustedCharY = the Y position of the character - the Y position of the first visible line of text.</p>
	*/
	function getAdjustedCharY():Int{
		
		/*
			Get the rect of the first visible character
		*/
		var lineIndex:Int = this.textField.scrollV-1;
		var ind:Int = this.textField.getLineOffset(lineIndex);
		var rect:Rectangle = this.textField.getCharBoundaries(ind);
		
		/*
			For some characters (such as carriage returns), getCharBoundaries returns null.
			In those situations, we find the next character that does not return null,
			sum the line heights up to that poInt, and subtract that sum from the y
			position of the character.
		*/
		var lineHeights:Float = 0;
		while(rect == null){
			lineHeights += this.textField.getLineMetrics(lineIndex).height;
			lineIndex++;
			if(lineIndex == (this.textField.maxScrollV+this.textField.bottomScrollV-1)){
				rect = new Rectangle(0,0,0,0);
				break;
			}
			rect = this.textField.getCharBoundaries(this.textField.getLineOffset(lineIndex));
		}
		rect.y -= lineHeights;

		return Std.int(rect.y);
	}
	
	/**
	* Returns the bounding rectangle of a character in consideration of the vertical scroll position of the TextField and other manual offsets.  Also, prevents TextField.getCharBoundaries from returning null and causing an error.
	*
	* @param index The index of the character to get boundaries for.
	* @param applyScrollVOffset Controls whether or not to adjust for the TextField's vertical scroll position.  Since calls to get adjustedCharY can be expensive, we only call it when necessary.
	*/
	function getAdjustedCharBoundaries(index:Int, applyScrollVOffset:Bool):Rectangle{
		var rect:Rectangle = this.textField.getCharBoundaries(index);
		if(rect == null){
			rect = new Rectangle(0,0,0,0);
		}else{
			var scrollVOffset:Float = 0;
			if(applyScrollVOffset){
				scrollVOffset = this.getAdjustedCharY();
			}
			rect.x += this.X_CORRECTION + this.xOffset;
			rect.y += this.Y_CORRECTION + this.yOffset - scrollVOffset;
		}
		return rect;
	}
}