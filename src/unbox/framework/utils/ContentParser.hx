package unbox.framework.utils;

import com.hybrid.ui.ToolTip;
import haxe.xml.Fast;
import openfl.Assets;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.Rectangle;
import openfl.system.ApplicationDomain;
import openfl.text.AntiAliasType;
import openfl.text.GridFitType;
import openfl.text.TextField;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.CoursePage;

/**
 * Parses page data from XMLs and creates course
 * objects like textfields, buttons and components.
 * @author Tiago Ling Alexandre
 */
class ContentParser
{
	#if flash
	public static var currentAppDomain:ApplicationDomain;
	public static var stylesheet:flash.text.StyleSheet;
	#end
	public static var currentPage:CoursePage;
	
	//Map to store events and create them
	//only when all instances are added.
	public static var events:Map<String, Fast>;
	//Array to store parsed objects to be added to the page view
	public static var objects:Array<DisplayObject>;
	//Map to store objects to be added to custom targets, defined as keys 
	public static var cObjects:Map<String, DisplayObject>;
	/**
	 * 
	 * @param	pageContent	A Fast object containing all data to be parsed for the page
	 * @param	page		A reference to the page in which ContentParser will operate
	 * @param	onComplete	A callback to be called when all operations are done.
	 */
	public static function parsePageContent(pageContent:Fast, page:CoursePage, ?onComplete:Void->Void):Void
	{
		currentPage = page;
		trace( "currentPage : " + pageContent + " page : " + page + " onComplete : " + onComplete );
		
		events = new Map<String, Fast>();
		objects = new Array<DisplayObject>();
		cObjects = new Map<String, DisplayObject>();
		
		/**
		 * ContentParser order of operations:
		 * I - Data parsing:
			 * 1 - Buttons;
			 * 2 - Sprites;
			 * 3 - Components;
			 * 4 - TextFields;
			 * 5 - Tooltips;
		 * II - Instantiation:
			 * Same order as parsing - loop through all objects and add them sequencially;
		 * III - Attaching events (actions):
			 * Same order as parsing - loop through all events and add them sequencially;
		 */
		
		//Object parsing
		for (content in pageContent.nodes.content) { if (content.att.type == "object") parseObjects(content); }
		
		//TextField parsing
		for (content in pageContent.nodes.content) { if (content.att.type == "text") for (textData in content.nodes.content) { parseTextField(textData, null, page); } }
		
		//Checking for any tweens on targets and preparing them accordingly
		prepareForTween(pageContent);
		
		//After all the parsing is done, adds all display objects in sequence
		for (i in 0...objects.length)
		{
			currentPage.view.addChild(objects[i]);
		}
		
		//Adding objects to custom targets
		for (key in cObjects.keys())
		{
			var cTarget:DisplayObjectContainer = cast(currentPage.view.getChildByName(key), DisplayObjectContainer);
			if (cTarget != null) {
				cTarget.addChild(cObjects.get(key));
			} else {
				trace("Custom target '" + key + "' for object '" + cObjects.get(key).name + "' is null!");
			}
		}
		
		//Tooltips depend on existing targets, so they must be added after instantiation.
		for (content in pageContent.nodes.content) { if (content.att.type == "tooltip") parseTooltips(content, page); }
		
		//Parse actions after adding all children to the page.
		//This is needed to solve temporal coupling in XML
		for (key in events.keys())
		{
			parseEvent(key, events.get(key));
		}
		
		if (onComplete != null)
			onComplete();
	}
	
	/**
	 * Parses and instantiates new TextFields based on XML data.
	 * @param	textData	Fast object containing data about the TextField to be created.
	 * @param	target		If provided, adds the created TextField to it.
	 * @param	context		If provided, replaces {$xxx} tags with values from public vars present in the context object
	 * @return	Returns the TextField just created.
	 */
	public static function parseTextField(textData:Fast, ?target:DisplayObjectContainer, ?context:Dynamic):TextField
	{
		
		var txt:TextField = new TextField();
		#if flash
		txt.styleSheet = stylesheet;
		#end
		txt.embedFonts = true;
		
		txt.antiAliasType = AntiAliasType.ADVANCED;
		txt.gridFitType = GridFitType.SUBPIXEL;
		#if flash
		txt.mouseWheelEnabled = false;
		#end
		txt.wordWrap = true;
		
		if (textData.has.instanceName)
			txt.name = textData.att.instanceName;
		else 
			txt.name = "text";
			
		var fTarget:String = null;
		if (textData.has.target) {
			fTarget = textData.att.target;
		}
		
		//Parsing object variables
		if (textData.hasNode.vars) {
			parseVars(txt, textData.node.vars);
		}
		
		//This is needed for <br> tags to work
		txt.multiline = true;
		txt.selectable = false;
		
		//Autosizing normally causes problems
		//txt.autoSize = TextFieldAutoSize.NONE;
		
		//Removing <!CDATA[[]]> tags or text does not appear
		var strippedText:String = "";
		if (textData.hasNode.body)
			strippedText = textData.node.body.innerHTML;
		else
			strippedText = textData.node.title.innerHTML;
			
		strippedText = StringUtils.removeCDATA(strippedText);
		
		if (strippedText.indexOf("{$") > -1 && context != null)
		{
			strippedText = StringUtils.parseTextVars(strippedText, context);
		}
		
		txt.htmlText = strippedText;
		if (target == null) {
			if (fTarget == null)
				objects.push(txt);
			else
				cObjects.set(fTarget, txt);
		} else {
			target.addChild(txt);
		}
		
		//TODO: This is here for use with PageAnimator. Remove from here and put inside
		//PageController or PageAnimator if possible
		if (target == null && currentPage.data.contentTransitionIn != "none")
		{
			txt.alpha = 0;
			txt.visible = false;
		}
		
		return txt;
	}
	
	/**
	 * Parses and instantiates sprites over TextFields which will show tooltips
	 * when MouseEvent.MOUSE_OVER events are dispatched.
	 * @param	tooltipData	A Fast object containing the list of tooltips
	 * @param	page		The target page to vinculate the tooltips.
	 */
	public static function parseTooltips(tooltipData:Fast, page:CoursePage):Void
	{
		page.tt = new ToolTip();
		#if flash
		page.tt.stylesheet = page.stylesheet;
		#end
		page.tt.titleEmbed = true;
		page.tt.contentEmbed = true;
		
		page.tt.align = tooltipData.has.align ? tooltipData.att.align : "left";
		page.tt.autoSize = (tooltipData.has.autoSize && tooltipData.att.autoSize == "true") ? true : false;
		page.tt.followCursor = (tooltipData.has.followCursor && tooltipData.att.followCursor == "true") ? true : false;
		page.tt.hook = (tooltipData.has.hook && tooltipData.att.hook == "true") ? true : false;
		page.tt.hookSize = tooltipData.has.hookSize ? Std.parseFloat(tooltipData.att.hookSize) : 5;
		page.tt.cornerRadius = tooltipData.has.cornerRadius ? Std.parseFloat(tooltipData.att.cornerRadius) : 5;
		page.tt.showDelay = tooltipData.has.showDelay ? Std.parseFloat(tooltipData.att.showDelay) : 1;
		page.tt.hideDelay = tooltipData.has.hideDelay ? Std.parseFloat(tooltipData.att.hideDelay) : 1;
		page.tt.bgAlpha = tooltipData.has.bgAlpha ? Std.parseFloat(tooltipData.att.bgAlpha) : 1;
		page.tt.tipWidth = tooltipData.has.width ? Std.parseFloat(tooltipData.att.width) : 120;
		var xmlColors:Array<String> = tooltipData.has.colors ? tooltipData.att.colors.split(",") : ["0xFFFFFF,0xFFFFFF"];
		var colors:Array<UInt> = new Array<UInt>();
		colors[0] = Std.parseInt(xmlColors[0]);
		colors[1] = Std.parseInt(xmlColors[1]);
		page.tt.colors = colors;
		page.tt.padding = tooltipData.has.padding ? Std.parseFloat(tooltipData.att.padding) : 10;
		
		page.ttTexts = new Map<String, String>();
		for (content in tooltipData.nodes.content)
		{
			var target:Dynamic = null;
			var firstId:Int = -1;
			var lastId:Int = -1;
			if (content.att.type == "text") {
				target = cast(page.view.getChildByName(content.att.target), TextField);
				
				firstId = target.text.indexOf(content.att.highlight);
				lastId = firstId + (content.att.highlight.length);
				
				var spr:Sprite = new Sprite();
				spr.name = "tt_" + content.att.target;
				
				//Checking for mandatory interactions
				checkInteraction(content, spr.name);
				
				//TODO: Must create names for the tooltips
				//and allow the user to enable / disable them
				page.view.addChild(spr);
				
				spr.x = target.x;
				spr.y = target.y;
				spr.buttonMode = true;
				spr.useHandCursor = true;
				page.ttTexts.set(spr.name, StringUtils.removeCDATA(content.innerHTML));
				
				for (i in firstId...lastId)
				{
					var curBounds:Rectangle = target.getCharBoundaries(i);
					if (content.has.debug && content.att.debug == "true")
						spr.graphics.beginFill(0xCC00CC, 0.4);
					else
						spr.graphics.beginFill(0xFFFFFF, 0);
					spr.graphics.drawRect(curBounds.x, curBounds.y, curBounds.width, curBounds.height);
					spr.graphics.endFill();
				}
				
				spr.addEventListener(MouseEvent.ROLL_OVER, page.handleTooltips, false, 0, true);
			} else {
				//Checking for mandatory interactions
				checkInteraction(content, content.att.target);
				
				target = cast(page.view.getChildByName(content.att.target), Sprite);
				page.ttTexts.set(target.name, StringUtils.removeCDATA(content.innerHTML));
				target.addEventListener(MouseEvent.ROLL_OVER, page.handleTooltips, false, 0, true);
			}
		}
	}
	
	public static function parseObjects(objData:Fast) 
	{
		for (content in objData.nodes.content)
		{
			//Checking for mandatory interactions
			checkInteraction(content);
			
			var obj:Sprite = null;
			var fromStage:Bool = false;
			if (content.has.fromStage && content.att.fromStage == "true")
				fromStage = true;
			
			//Checking if the sprite has to be instantiated or is already present in the stage.
			if (!fromStage)
			{
				//Checking the type of object to be created (content.att.type attribute)
				switch (content.att.type) {
					case "sprite" | "Sprite":
						if (content.att.viewClass == "openfl.display.Sprite")
						{
							obj = new Sprite();
						} else {
							var sprClass:Class<Dynamic> = currentAppDomain.getDefinition(content.att.viewClass);
							obj = Type.createInstance(sprClass, []);
						}
						obj.name = content.att.instanceName;
					case "button" | "Button":
						#if flash
						var viewClass:Class<Dynamic> = currentAppDomain.getDefinition(content.att.viewClass);
						obj = Type.createInstance(viewClass, []);
						#else
						obj = cast(Assets.getMovieClip(currentPage.data.id + ":" + content.att.viewClass), Sprite);
						#end
						var button:GenericButton = new GenericButton(0, 0, obj, content.att.instanceName);
						currentPage.buttons.push(button);
					case "component" | "Component":
						trace("Component parsing not yet available!");
					default:
						trace("Object type '" + content.att.type + "' not recognized!");
				}
			} else {
				//This should work the same regardless of the type of object (sprite, button, component)
				obj = cast(currentPage.view.getChildByName(content.att.instanceName), Sprite);
				if (obj == null) {
					trace("Sprite '" + content.att.instanceName + "' not found in page view! Aborting...");
					continue;
				}
			}
			
			obj.mouseChildren = false;
			
			var fTarget:String = null;
			if (content.has.target) {
				fTarget = content.att.target;
			}
			
			if (content.hasNode.vars)
				parseVars(obj, content.node.vars);
				
			//EXPERIMENTAL code for creating simple sprite graphics
			//This should be used with viewClass="openfl.display.Sprite"
			//in the XML file.
			if (content.hasNode.graphic)
			{
				var color:Int = Std.parseInt("0x" + content.node.graphic.att.color.substr(2, content.node.graphic.att.color.length));
				var width:Float = Std.parseFloat(content.node.graphic.att.w);
				var height:Float = Std.parseFloat(content.node.graphic.att.h);
				obj.graphics.beginFill(color, 1);
				switch (content.node.graphic.att.type) {
					case "rect":
						obj.graphics.drawRect(0, 0, width, height);
					case "circle":
						obj.graphics.drawCircle(0, 0, width / 2);
					default:
						trace("Graphic type not recognized! Drawing rectangle instead...");
						obj.graphics.drawRect(0, 0, width, height);
				}
				obj.graphics.endFill();
			}
			
			if (content.hasNode.label) {
				for (label in content.nodes.label) {
					parseTextField(label, obj);
				}
			}
			
			if (fTarget == null) {
				objects.push(obj);
			} else {
				cObjects.set(fTarget, obj);
			}
			
			if (content.hasNode.event)
			{
				if (fTarget == null)
					events.set(obj.name, content.node.event);
				else
					events.set(fTarget + "." + obj.name, content.node.event);
			}
		}
	}
	
	static function parseEvent(owner:String, data:Fast)
	{
		var page = currentPage;
		var eventOwner:Sprite = null;
		if (owner.indexOf(".") > -1) {
			var tgts:Array<String> = owner.split(".");
			for (i in 0...tgts.length)
			{
				if (i == 0) {
					eventOwner = cast(page.view.getChildByName(tgts[i]), Sprite);
				} else {
					eventOwner = cast(eventOwner.getChildByName(tgts[i]), Sprite);
				}
				eventOwner.mouseEnabled = true;
				eventOwner.mouseChildren = true;
			}
		} else {
			eventOwner = cast(page.view.getChildByName(owner), Sprite);
		}
		
		eventOwner.useHandCursor = true;
		eventOwner.buttonMode = true;
		
		var handler:Dynamic->Void = currentPage.handleMouseClick;
		
		var func:Void->Void = function f() {
			for (vars in data.nodes.vars) {
				parseVars(page.view.getChildByName(vars.att.target), vars);
			}
			for (tween in data.nodes.tween) {
				var tParser:TweenParser = new TweenParser();
				var tgts:Array<String> = tween.att.target.split(",");
				for (i in 0...tgts.length) {
					var tgt = page.view.getChildByName(tgts[i]);
					tParser.addTween(tgt, tween);
				}
			}
			for (method in data.nodes.method) {
				var args:Array<String> = method.att.params.split(",");
				Reflect.callMethod(page, Reflect.field(page, method.att.name), args);
			}
			//Only one navigation is allowed per object
			if (data.hasNode.navigation) {
				switch (data.node.navigation.att.type) {
					case "nextPage", "next":
						page.context.navController.gotoNextPage();
					case "backPage", "back":
						page.context.navController.gotoPreviousPage();
					case "goto", "goTo":
						page.context.navController.gotoPage(Std.parseInt(data.node.navigation.att.page));
					default:
						trace("Navigation type attribute not recognized!");
				}
			}
			
			if (data.has.addOnce && data.att.addOnce == "true") {
				eventOwner.removeEventListener(MouseEvent.CLICK, handler);
				eventOwner.useHandCursor = false;
				eventOwner.buttonMode = false;
			}
		};
		
		switch (data.att.type)
		{
			case "click", "Click", "CLICK":
				eventOwner.addEventListener(MouseEvent.CLICK, currentPage.handleMouseClick);
				currentPage.clickActions.set(eventOwner.name, func);
			case "over", "Over", "OVER":
				eventOwner.addEventListener(MouseEvent.MOUSE_OVER, currentPage.handleMouseOver);
				currentPage.overActions.set(eventOwner.name, func);
			default:
				trace("parseEvent -> event type not recognized!");
		}
	}
	
	static function checkInteraction(data:Fast, ?target:String)
	{
		var mustInteract:Bool = false;
		if (data.has.mustInteract && data.att.mustInteract == "true")
			mustInteract = true;
			
		if (mustInteract)
		{
			trace("Target HAS 'mustInteract' tag");
			
			if (target != null)
				currentPage.mInteractions.set(target, false);
			else 
				currentPage.mInteractions.set(data.att.instanceName, false);
				
			currentPage.totalInteractions++;
		}
	}
	
	/**
	 * Parses and applies a list of variables to an object.
	 * @param	obj		The object which contains the vars
	 * @param	vars	A Fast object containing the vars and its values
	 */
	public static function parseVars(obj:DisplayObject, vars:Fast) 
	{
		if (obj == null || vars == null) {
			trace("parseVars -> object '" + obj + "' and/or vars '" + vars + "' are null! Aborting...");
			return;
		}
		
		for (field in vars.elements) {
			if (field.elements.hasNext() && Reflect.isObject(field)) {
				var subObj:Dynamic = null;
				#if flash
				if (field.name == "accessibilityProperties") {
					subObj = new flash.accessibility.AccessibilityProperties();
				} else {
					subObj = Reflect.getProperty(obj, field.name);
				}
				#else
				subObj = Reflect.getProperty(obj, field.name);
				#end
				
				for (subField in field.elements) {
					if (subObj != null && Reflect.hasField(subObj, subField.name)) {
						Reflect.setField(subObj, subField.name, subField.innerHTML);
					}
				}
				Reflect.setField(obj, field.name, subObj);
			} else {
				//Reflect.setField works alright parsing strings to ints or floats,
				//however, it does not seem to work with Bools. This conditional
				//is here to make it work.
				if (field.innerHTML == "true" || field.innerHTML == "false") {
					var value:Bool = field.innerHTML == "true" ? true : false;
					Reflect.setField(obj, field.name, value);
				} else {
					Reflect.setField(obj, field.name, field.innerHTML);
				}
			}
		}
	}
	
	static function prepareForTween(currentPageContent:Fast)
	{
		trace( "ContentParser.prepareForTween" );
		if (currentPageContent.hasNode.tween)
		{
			for (tween in currentPageContent.node.tween.nodes.tween)
			{
				if (tween.has.type && tween.att.type == "from")
				{
					if (tween.att.target.split(",").length > 1)
					{
						var tgts:Array<String> = tween.att.target.split(",");
						for (i in 0...tgts.length)
						{
							//var target:DisplayObject = currentPage.view.getChildByName(tgts[i]);
							//target.visible = false;
							for (j in 0...objects.length)
							{
								if (objects[j].name == tgts[i]) {
									objects[j].visible = false;
									break;
								}
							}
						}
					} else {
						//var target:DisplayObject = currentPage.view.getChildByName(tween.att.target);
						//target.visible = false;
						for (i in 0...objects.length)
						{
							if (objects[i].name == tween.att.target) {
								objects[i].visible = false;
								break;
							}
						}
					}
				}
			}
		}
	}
}