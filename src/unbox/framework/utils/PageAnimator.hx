package unbox.framework.utils;
import motion.Actuate;
import motion.easing.Expo;
import openfl.display.DisplayObject;
import openfl.display.Sprite;
import openfl.text.TextField;

/**
 * Handles all types of transitions between pages.
 * Ported directly from AS3 EbookFramework.
 * @author Tiago Ling Alexandre
 */
class PageAnimator
{
	public function new() {}
	
	public static function animate(target:DisplayObject, ?duration:Float, ?direction:Int, ?transition:String, ?cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		switch (transition)
		{
			case "slideXIn":
				slideXIn(target, duration, direction, cBack, cBackParams);
			case "slideXOut":
				slideXOut(target, duration, direction, cBack, cBackParams);
			case "slideYIn":
				slideYIn(target, duration, direction, cBack, cBackParams);
			case "slideYOut":
				slideYOut(target, duration, direction, cBack, cBackParams);
			case "fadeIn":
				fadeIn(target, duration, direction, cBack, cBackParams);
			case "fadeOut":
				fadeOut(target, duration, direction, cBack, cBackParams);
			default:
				return;
		}
	}
	
	//-- SLIDE X
	static function slideXIn(target:DisplayObject, duration:Float = 0.5, direction:Int = 1, cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (target != null)
		{
			target.alpha = 1;
			target.x = direction * target.stage.stageWidth;
			Actuate.tween(target, duration, {x: 0}).ease(Expo.easeInOut).onComplete(cBack, cBackParams);
		}
	}
	
	static function slideXOut(target:DisplayObject, duration:Float = 0.5, direction:Int = -1, cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (target != null)
		{
			target.x = 0;
			Actuate.tween(target, duration, {x: direction * -target.stage.stageWidth}).ease(Expo.easeInOut).onComplete(cBack, cBackParams);
		}
	}
	
	//-- SLIDE Y
	static function slideYIn(target:DisplayObject, duration:Float = 0.5, direction:Int = 1, cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (target != null)
		{
			target.alpha = 1;
			target.y = -direction * target.stage.stageHeight;
			Actuate.tween(target, duration, {y: 0}).ease(Expo.easeInOut).onComplete(cBack, cBackParams);
		}
	}
	
	static function slideYOut(target:DisplayObject, duration:Float = 0.5, direction:Int = -1, cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (target != null)
		{
			target.y = 0;
			Actuate.tween(target, duration, {y: -direction * -target.stage.stageHeight}).ease(Expo.easeInOut).onComplete(cBack, cBackParams);
		}
	}
	
	static function fadeIn(target:DisplayObject, duration:Float = .3, direction:Int = 1, cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (target != null)
		{
			target.alpha = 0;
			Actuate.tween(target, duration, {alpha: 1}).onComplete(cBack, cBackParams);
		}
	}
	
	static function fadeOut(target:DisplayObject, duration:Float = .3, direction:Int = -1, cBack:Dynamic->Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (target != null)
		{
			Actuate.tween(target, duration, {alpha: 0}).onComplete(cBack, cBackParams);
		}
	}
	
	//-- CONTENT ANIMATOR
	public static function contentFadeIn(view:Sprite, duration:Float = .5, delay:Float = 0, cBack:Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		if (view != null)
		{
			var itemCount:Int = 0;
			var i:Int = 0;
			for (i in 0...view.numChildren)
			{
				var displayitem:DisplayObject = view.getChildAt(i);
				if (Std.is(displayitem, TextField))
				{
					var myFieldLabel:DisplayObject = cast(displayitem, DisplayObject);
					myFieldLabel.x -= 20;
					Actuate.tween(myFieldLabel, duration, { alpha:1, x:myFieldLabel.x + 20 } ).delay(delay + (itemCount * 0.1)).ease(Expo.easeOut);
					itemCount++;
				}
			}
			
			Actuate.timer(duration).onComplete(cBack, cBackParams);
		}
	}
	
	public static function contentFadeOut(page:Sprite, duration:Float = .5, delay:Float = 0, cBack:Dynamic->Void, ?cBackParams:Array<Dynamic>):Void
	{
		var target:Sprite = cast(page.getChildByName("view"), Sprite);
		if (target != null)
		{
			var itemCount:Int = 0;
			var i:Int = 0;
			for (i in 0...target.numChildren)
			{
				var displayitem:DisplayObject = target.getChildAt(i);
				if (Std.is(displayitem, TextField))
				{
					var myFieldLabel:DisplayObject = cast(displayitem, DisplayObject);
					Actuate.tween(myFieldLabel, duration, { alpha:0, x:myFieldLabel.x - 20 } ).delay(delay + (itemCount * 0.1)).ease(Expo.easeOut);
					itemCount++;
				}
			}
			
			Actuate.timer(duration).onComplete(cBack, cBackParams);
		}
	}
}