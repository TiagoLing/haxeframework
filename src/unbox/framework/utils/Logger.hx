package unbox.framework.utils;
import haxe.Log;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class Logger
{

	public function new() {}
	
	//TODO: Extend haxe.Log to keep caller class and line functionality
	public static function log(text:String, ?pos:haxe.PosInfos):Void
	{
		trace(text);
	}
}