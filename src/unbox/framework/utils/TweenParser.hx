package unbox.framework.utils;
import haxe.Json;
import haxe.xml.Fast;
import motion.Actuate;
import motion.actuators.GenericActuator;
import motion.actuators.GenericActuator.IGenericActuator;
import motion.actuators.SimpleActuator;
import motion.easing.Back;
import motion.easing.Bounce;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.IEasing;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.filters.BlurFilter;
import openfl.filters.DropShadowFilter;
import openfl.filters.GlowFilter;
import openfl.geom.ColorTransform;

/**
 * Used to parse data from page XMLs, create and dispose of tweens.
 * @author Tiago Ling Alexandre
 */
class TweenParser
{
	//Holds all tween references for the current page.
	//var tweenRefs:Array<IGenericActuator> = new Array<IGenericActuator>();
	var tweenRefs:Array<SimpleActuator> = new Array<SimpleActuator>();
	var tweenCompletes:Array<Fast> = new Array<Fast>();
	
	//var defaultActuator:Class<GenericActuator> = SimpleActuator;
	
	public function new() {}
	
	/**
	 * Parses all tweens added to a specific page, creating and starting them.
	 * @param	context	The parent DisplayObjectContainer of all the tween targets.
	 * 					Usually this is a CoursePage view.
	 * @param	data	The Fast data containing the tweening info. Usually extracted
	 * 					from a CoursePage content xml file.
	 */
	public function parse(context:DisplayObjectContainer, data:Fast):Void
	{
		for (tween in data.nodes.tween)
		{
			var targets:Array<String> = tween.att.target.split(",");
			
			for (i in 0...targets.length)
			{
				var target:DisplayObject = null;
				
				//Check if it is a nested target
				if (targets[i].split(".").length > 1)
				{
					var tgts = targets[i].split(".");
					//TODO: Currently this works only with immediate children. Modify to handle any depth of nesting (recursion ?).
					var tgtParent:DisplayObjectContainer = cast(context.getChildByName(tgts[0]), DisplayObjectContainer);
					target = tgtParent.getChildByName(tgts[1]);
				} else {
					target = context.getChildByName(targets[i]);
				}
				
				if (target != null)
				{
					addTween(target, tween, i);
				} else {
					trace("Doing nothing => tween target '" + targets[i] + "' is null!");
				}
			}
		}
	}
	
	/**
	 * 
	 * @param	target
	 * @param	tween
	 * @param	iteration
	 */
	public function addTween(target:DisplayObject, tween:Fast, iteration:Int = 0)
	{
		if (target == null || tween == null) {
			trace("addTween -> target '" + target + "' and/or tween '" + tween + "' is null! Aborting...");
			return;
		}
		var tType:String = tween.att.type;
		var tName:String = target.name;
		if (tType == null || tType == "") {
			trace("Tween for target '" + tName + "' must have a 'type' parameter (use either 'to' || 'from'). Aborting...");
			return;
		}
		
		var attributes:Dynamic = null;
		if (tween.hasNode.vars)
			attributes = parseVars(target, tween.node.vars);
		var duration:Float = Std.parseFloat(tween.att.duration);
		var reverse:Bool = false;
		var reflect:Bool = false;
		var repeat:Int = 0;
		var ease:IEasing = Linear.easeNone;
		var delay:Float = 0;
		
		//Checks
		var hasEase:Bool = tween.has.ease;
		var hasRepeat:Bool = tween.has.repeat;
		var hasYoyo:Bool = tween.has.yoyo;
		var hasDelay:Bool = tween.has.delay;
		var hasStagger:Bool = tween.has.stagger;
		var hasOnComplete:Bool = tween.hasNode.onComplete;
		var hasOnUpdate:Bool = tween.hasNode.onUpdate;
		var hasAddOnce:Bool = tween.has.addOnce && tween.att.addOnce == "true" ? true : false;
		var hasTransform:Bool = tween.hasNode.transform;
		var hasFilter:Bool = tween.hasNode.filter;
		
		if (tType == "from" || tType == "allFrom") {
			reverse = true;
		}
			
		if (hasEase) {
			ease = setEase(tween.att.ease);
		}
		
		if (hasRepeat) {
			repeat = Std.parseInt(tween.att.repeat);
		}
		
		if (hasYoyo) {
			reflect = tween.att.yoyo == "true" ? true : false;
			if (!tween.has.repeat)
				repeat = 1;
		}
		
		if (hasDelay) {
			delay = Std.parseFloat(tween.att.delay);
		}
		
/*		if (hasDelay && hasStagger)
			trace("Delay data for target '" + target.name + "' -> delay '" + delay + "' + stagger '" + Std.parseFloat(tween.att.stagger) * (iteration + 1) + "' : ");*/
		
		if (hasStagger) {
			delay += Std.parseFloat(tween.att.stagger) * (iteration + 1);
		}
		
		//Enables the user to do any kind of action already possible
		//through XML when onComplete is called. This allows
		//for complex logic to be done directly through XML.
		if (hasOnComplete) {
			var onCompleteData:Fast = tween.node.onComplete;
			tweenCompletes.push(onCompleteData);
			//trace( "onCompleteData for target '" + tName + "' : " + onCompleteData );
		}
		
		if (hasOnUpdate) {
			trace("Tween option 'onUpdate' for target '" + tName + "' not yet implemented!");
		}
		
		//addOnce -> Must find a way to remove tweens when the user
		//interacts with the object
		if (hasAddOnce) {
			var tgt:Sprite = cast(target, Sprite);
			if (tgt.hasEventListener(MouseEvent.MOUSE_OVER)) {
				tgt.addEventListener(MouseEvent.MOUSE_OVER, onTargetOver);
			} else if (tgt.hasEventListener(MouseEvent.CLICK)) {
				tgt.addEventListener(MouseEvent.CLICK, onTargetClick);
			}
		}
		
		//When doing the tween, store its reference to prevent the delay from happening on every iteraction. 
		//This is the default behavior - a XML attribute should be created to allow the user to override this
		if (attributes != null) {
			var tweenActuator:Class<SimpleActuator> = SimpleActuator;
			var tweenRef = Type.createInstance(tweenActuator, [target, duration, attributes]);
			
			if (hasOnComplete) {
				tweenRef.reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay)
					.onRepeat(removeDelay, [tweenRefs.length]).onComplete(onTweenComplete, [tName, tweenCompletes.length - 1]);
			} else {
				tweenRef.reverse(reverse).reflect(reflect).repeat(repeat)
					.ease(ease).delay(delay).onRepeat(removeDelay, [tweenRefs.length]);
					
			}
			
			tweenRefs.push(tweenRef);
		}
		
		//There is no "from" in Actuate. To emulate the behavior this hack is necessary
		//This is done through PageController, which "prepares" objects with the "from"
		//type to be tweened - they are set as visible = false, then here a timer is set
		//to set the object as visible again just after the tween starts.
		if (tType == "from" || tType == "allFrom") {
			var visibilityTime:Float = delay + 0.1;
			//trace( "time to set visible = true for target '" + target.name + "' : " + visibilityTime );
			Actuate.timer(visibilityTime).onComplete(function f() { target.visible = true; } );
		}
			
		if (hasTransform) {
			var transformVars:Dynamic = parseVars(target, tween.node.transform);
			switch (tween.node.transform.att.type) {
				case "Tint" | "tint":
					if (hasOnComplete) {
						trace("#### Has onComplete!");
						Actuate.transform(target, duration).color(transformVars.color, transformVars.strength, transformVars.alpha).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay).onComplete(onTweenComplete, [tName, tweenCompletes.length - 1]);
					} else {
						Actuate.transform(target, duration).color(transformVars.color, transformVars.strength, transformVars.alpha).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay);
					}
				case "Sound" | "sound":
					trace("Sound transformation not yet implemented!");
					//TODO: Is SoundTransform needed? What are the possible use cases?
					//Actuate.transform(target, duration).sound(transformVars.volume, transformVars.pan).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay);
			}
		}
		
		//Initial effect support (auto-setting some properties to avoid ugliness)
		//For more control over the filters, the user should create and add it to the
		//target DisplayObject before tweening.
		if (hasFilter) {
			var filterVars:Dynamic = parseVars(target, tween.node.filter);
			switch(tween.node.filter.att.type)
			{
				case "Glow" | "glow":
					if (target.filters.length == 0) {
						var filter:GlowFilter = new GlowFilter();
						filter.color = filterVars.color;
						filter.alpha = 0;
						target.filters = [filter];
					}
					if (hasOnComplete)
						Actuate.effects(target, duration).filter(GlowFilter, filterVars).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay).onComplete(onTweenComplete, [tName, tweenCompletes.length - 1]);
					else
						Actuate.effects(target, duration).filter(GlowFilter, filterVars).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay);
					
				case "Blur" | "blur":
					if (target.filters.length == 0) {
						var filter:BlurFilter = new BlurFilter();
						filter.blurX = 0;
						filter.blurY = 0;
						target.filters = [filter];
					}
					if (hasOnComplete)
						Actuate.effects(target, duration).filter(BlurFilter, filterVars).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay).onComplete(onTweenComplete, [tName, tweenCompletes.length - 1]);
					else
						Actuate.effects(target, duration).filter(BlurFilter, filterVars).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay);
					
				case "DropShadow" | "dropshadow":
					if (target.filters.length == 0) {
						var filter:DropShadowFilter = new DropShadowFilter();
						filter.alpha = 0;
						filter.angle = filterVars.angle;
						filter.color = filterVars.color;
						target.filters = [filter];
					}
					if (hasOnComplete)
						Actuate.effects(target, duration).filter(DropShadowFilter, filterVars).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay).onComplete(onTweenComplete, [tName, tweenCompletes.length - 1]);
					else
						Actuate.effects(target, duration).filter(DropShadowFilter, filterVars).reverse(reverse).reflect(reflect).repeat(repeat).ease(ease).delay(delay);
			}
		}
		
		for (i in 0...tweenRefs.length)
		{
			tweenRefs[i].move();
		}
	}
	
	function onTweenComplete(targetName:String, index:Int)
	{
		//trace( "TweenParser.onTweenComplete -> target '" + targetName + "' -> index : " + index );
		//trace("Tween option 'onComplete' not yet implemented!");
		
		//TODO: This method should be able to handle any tags
		//contained within a normal XML page. This means using
		//ContentParser, which has its static maps and arrays being
		//used by the page. The solution would be to create an instance
		//of ContentParser instead of using everything static.
		if (tweenCompletes[index] != null) {
			ContentParser.parsePageContent(tweenCompletes[index], ContentParser.currentPage);
			
			//Allows onComplete tweens to be parsed
			if (tweenCompletes[index].hasNode.tween)
				parse(ContentParser.currentPage.view, tweenCompletes[index].node.tween);
		}
	}
	
	function onTargetOver(e:MouseEvent)
	{
		e.target.removeEventListener(MouseEvent.MOUSE_OVER, onTargetOver);
		
		TweenParser.removeTween(e.target, true, true, true);
	}
	
	function onTargetClick(e:MouseEvent)
	{
		e.target.removeEventListener(MouseEvent.CLICK, onTargetClick);
		
		TweenParser.removeTween(e.target, true, true, true);
	}
	
	/**
	 * Called by default after the first repetition
	 * of a tween with delay to remove it. 
	 * TODO: Create flag to let the user choose wether
	 * to remove the delay or not.
	 * @param	index	The position of the tween
	 * 					reference inside the array
	 */
	function removeDelay(index:Int)
	{
		//trace("Removed delay");
		tweenRefs[index].delay(0);
		tweenRefs[index].onRepeat(null);
	}
	
	/**
	 * Used to flush and renew the tween references
	 * array when changing pages
	 */
	public function flushTweens()
	{
		for (i in 0...tweenRefs.length) {
			Actuate.stop(tweenRefs[i]);
			tweenRefs[i] = null;
			tweenCompletes[i] = null;
		}
		
		//tweenRefs = new Array<IGenericActuator>();
		tweenRefs = new Array<SimpleActuator>();
		tweenCompletes = new Array<Fast>();
	}
	
	/**
	 * Removes the tween, effects and/or color transform from the target
	 * @param	tween	If true, removes all Actuate.tween operations
	 * @param	effect	If true, removes all Actuate.effects operations
	 * @param	cTrans	If true, removes all Actuate.transform.color operations
	 */
	public static function removeTween(target:Dynamic, tween:Bool, ?effect:Bool, ?cTrans:Bool)
	{
		if (tween)
			Actuate.stop(target);
			
		if (effect)
			cast(target, Sprite).filters = [];
			
		if (cTrans)
			cast(target, Sprite).transform.colorTransform = new ColorTransform();
	}
	
	/**
	 * Parse and applies vars to the a tween target
	 * @param	target	The object in which the vars will be applied
	 * @param	data	The Fast object describing the variables
	 * @return	A Dynamic object containing all the parsed vars and their values
	 */
	public function parseVars(target:DisplayObject, data:Fast):Dynamic
	{
		var obj:Dynamic = null;
		var str:String = "{";
		
		for (prop in data.elements)
		{
			var propData:String = prop.innerData;
			
			//Checking for relative values
			if (propData.indexOf("#") > -1) {
				var tValue:Dynamic = Reflect.getProperty(target, prop.name);
				if (propData.indexOf(".") > -1) {
					var floatValue:Float = Std.parseFloat(StringTools.replace(propData, "#", ""));
					floatValue = tValue + floatValue;
					propData = Std.string(floatValue);
				} else {
					var intValue:Int = Std.parseInt(StringTools.replace(propData, "#", ""));
					intValue = tValue + intValue;
					propData = Std.string(intValue);
				}
			}
			
			//Checking for colors (must use format 0x000000)
			if (propData.indexOf("0x") > -1)	//Convert color string to hex int value
				str += "\"" + prop.name + "\":" + Std.parseInt("0x" + propData.substr(2, propData.length)) + ",";
			else
				str += "\"" + prop.name + "\":" + propData + ",";
		}
		
		str += "}";
		str = StringTools.replace(str, ",}", "}");
		obj = Json.parse(str);
		
		//Check for variables
		var fields:Array<String> = Reflect.fields(obj);
		for (i in 0...fields.length)
		{
			var fieldValue = Reflect.field(obj, fields[i]);
		}
		
		return obj;
	}
	
	//TODO: Find a better way to do this
	public function setEase(data:String):IEasing
	{
		var easeClass:String = data.split(".")[0];
		var easeType:String = data.split(".")[1];
		
		switch (easeClass)
		{
			case "Back" | "back":
				switch (easeType)
				{
					case "easeIn", "easein":
						return Back.easeIn;
					case "easeOut", "easeout":
						return Back.easeOut;
					case "easeInOut", "easeinout":
						return Back.easeInOut;
				}
			case "Bounce" | "bounce":
				switch (easeType)
				{
					case "easeIn", "easein":
						return Bounce.easeIn;
					case "easeOut", "easeout":
						return Bounce.easeOut;
					case "easeInOut", "easeinout":
						return Bounce.easeInOut;
				}
			case "Cubic" | "cubic":
				switch (easeType)
				{
					case "easeIn", "easein":
						return Cubic.easeIn;
					case "easeOut", "easeout":
						return Cubic.easeOut;
					case "easeInOut", "easeinout":
						return Cubic.easeInOut;
				}
			case "Elastic" | "elastic":
				switch (easeType)
				{
					case "easeIn", "easein":
						return Elastic.easeIn;
					case "easeOut", "easeout":
						return Elastic.easeOut;
					case "easeInOut", "easeinout":
						return Elastic.easeInOut;
				}
			case "Expo" | "expo":
				switch (easeType)
				{
					case "easeIn", "easein":
						return Expo.easeIn;
					case "easeOut", "easeout":
						return Expo.easeOut;
					case "easeInOut", "easeinout":
						return Expo.easeInOut;
				}
			case "Linear" | "linear":
				return Linear.easeNone;
			case "Quad" | "quad":
				switch (easeType)
				{
					case "easeIn" | "easein":
						return Quad.easeIn;
					case "easeOut" | "easeout":
						return Quad.easeOut;
					case "easeInOut" | "easeinout":
						return Quad.easeInOut;
				}
			case "Quart" | "quart":
				switch (easeType)
				{
					case "easeIn" | "easein":
						return Quart.easeIn;
					case "easeOut" | "easeout":
						return Quart.easeOut;
					case "easeInOut" | "easeinout":
						return Quart.easeInOut;
				}
			case "Quint" | "quint":
				switch (easeType)
				{
					case "easeIn" | "easein":
						return Quint.easeIn;
					case "easeOut" | "easeout":
						return Quint.easeOut;
					case "easeInOut" | "easeinout":
						return Quint.easeInOut;
				}
			case "Sine" | "sine":
				switch (easeType)
				{
					case "easeIn" | "easein":
						return Sine.easeIn;
					case "easeOut" | "easeout":
						return Sine.easeOut;
					case "easeInOut" | "easeinout":
						return Sine.easeInOut;
				}
			default:
				return Linear.easeNone;
		}
		
		trace("=> Warning : Tween easing class and/or type not recognized. Using Linear.easeNone instead");
		return Linear.easeNone;
	}
}