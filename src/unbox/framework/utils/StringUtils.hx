package unbox.framework.utils;

/**
 * Common string utilities ported from the AS3 EbookFramework
 * @author UNBOX Design Studio © 2009-2012
 */
class StringUtils
{
	public function new() {}
	
	/**
	 * Validate an email
	 * @param	email
	 * @return TRUE or FALSE
	 */  
	public static function isValidEmail(email:String):Bool 
	{
		var emailExpression : EReg = new EReg('^[a-z][\\w.-]+@\\w[\\w.-]+\\.[\\w.-]*[a-z][a-z]$', "i");
		return emailExpression.match(email);
    }
	
	public static function removeHTML(htmlStr : String):String 
	{
		var removeHTML : EReg = new EReg("<[^>]*>", "gi");
		
		var safeStr:String = removeHTML.replace(htmlStr, "");
		
		return safeStr;
    }
	
	public static function replace(string : String, pattern : String, replacement : String) : String 
	{
		return string.split(pattern).join(replacement);
    }
	
	/**
	 * Translate variables from a text in the format {$xxxx}, where xxxx is the flash variable to display.
	 * @param	text the string to parse
	 * @param	context the scope where the variables are
	 * @return	the parsed text
	 */  
	public static function parseTextVars(text : String, context : Dynamic) : String 
	{
		var output : String = text;
		var i : Int = 0;
		while (i < text.length) 
		{
			var varName : String = "";
			if (text.charAt(i++) == "{" && text.charAt(i++) == "$") 
			{
				while (text.charAt(i) != "}" && i < text.length)
					varName += text.charAt(i++);
					
				output = StringTools.replace(output, "{$" + varName + "}", Reflect.field(context, varName));
            }
        }
		return output;
    }
	
	public static function trimWhitespace(string : String) : String 
	{
		if (String == null) 
		{
			return "";
        }
		
		var expr = new EReg('^\\s+|\\s+$', "g");
		var trimmed:String = expr.replace(string, "");
		
		return trimmed;
    }
	
	/**
	 * Removes CDATA tags from text strings
	 * @param	txt	The text to remove the tags from
	 * @return	The original text without CDATA tags
	 */
	public static function removeCDATA(txt:String):String
	{
		txt = StringTools.replace(txt, "<![CDATA[", "");
		txt = StringTools.replace(txt, "]]>", "");
		txt = StringTools.trim(txt);
		txt = StringUtils.trimWhitespace(txt);
		
		return txt;
	}
	
	/**
	 * A Function to return the complete word when you have only part of the text.
	 * @param	fullStr The complete String where the word is
	 * @param	str The string you want to search and get the entire word
	 * @return	Returns the first word ocurrence containing the str
	 */
	public static function getFullWord(fullStr : String, str : String) : String 
	{
		//This only returns the index of the first occurrence of str
		var foundStr : Int = fullStr.indexOf(str);
		var fstInd : Int = foundStr;
		var lstInd : Int = foundStr;
		var char : String = "";
		if (foundStr > -1)
		{
			var hasFoundFst : Bool = false;
			var hasFoundLst : Bool = false;
			var hasFound : Bool = false;
			while (!hasFound) {
				if (!hasFoundFst) 
				{
					char = fullStr.charAt(--fstInd);
					if (char == " " || fstInd < 0) 
					{
						fstInd++;
						hasFoundFst = true;
                    }
                }
				if (!hasFoundLst) 
				{
					char = fullStr.charAt(++lstInd);
					if (char == " " || lstInd == fullStr.length) 
					{
						hasFoundLst = true;
                    }
                }
				hasFound = (hasFoundFst && hasFoundLst);
            }
        }
		var word : String = "";
		word = fullStr.substring(fstInd, lstInd);
		return word;
    }
	
	/**
	* Remove's all &lt; and &gt; based tags from a string
	* @param p_string The source string.
	* @return String
	*/
	public static function stripTags(data:String):String {
		if (data == null) { return ""; }
		return new EReg("<\\/?[^>]+>", "igm").replace(data, "");
	}
	
	
	/**
	 * http://stackoverflow.com/questions/587242/replacing-accents-w-their-counterparts-in-as3
	 * Helper arrays for unicode decomposition
	 */
/*	static var pattern:Array<EReg> = new Array<EReg>();
	pattern[0] = new EReg('Š', 'g');
	pattern[1] = new EReg('Œ', 'g');
	pattern[2] = new EReg('Ž', 'g');
	pattern[3] = new EReg('š', 'g');
	pattern[4] = new EReg('œ', 'g');
	pattern[5] = new EReg('ž', 'g');
	pattern[6] = new EReg('[ÀÁÂÃÄÅ]', 'g');
	pattern[7] = new EReg('Æ', 'g');
	pattern[8] = new EReg('Ç', 'g');
	pattern[9] = new EReg('[ÈÉÊË]', 'g');
	pattern[10] = new EReg('[ÌÍÎÏ]', 'g');
	pattern[11] = new EReg('Ð', 'g');
	pattern[12] = new EReg('Ñ', 'g');
	pattern[13] = new EReg('[ÒÓÔÕÖØ]', 'g');
	pattern[14] = new EReg('[ÙÚÛÜ]', 'g');
	pattern[15] = new EReg('[ŸÝ]', 'g');
	pattern[16] = new EReg('Þ', 'g');
	pattern[17] = new EReg('ß', 'g');
	pattern[18] = new EReg('[àáâãäå]', 'g');
	pattern[19] = new EReg('æ', 'g');
	pattern[20] = new EReg('ç', 'g');
	pattern[21] = new EReg('[èéêë]', 'g');
	pattern[22] = new EReg('[ìíîï]', 'g');
	pattern[23] = new EReg('ð', 'g');
	pattern[24] = new EReg('ñ', 'g');
	pattern[25] = new EReg('[òóôõöø]', 'g');
	pattern[26] = new EReg('[ùúûü]', 'g');
	pattern[27] = new EReg('[ýÿ]', 'g');
	pattern[28] = new EReg('þ', 'g');
	
	static var patternReplace:Array<String> = ['S', 'Oe', 'Z', 's', 'oe', 'z', 'A', 'Ae', 'C', 'E', 'I', 'D', 'N', 'O', 'U', 'Y', 'Th', 'ss', 'a', 'ae', 'c', 'e', 'i', 'd', 'n', 'o', 'u', 'y', 'th'];*/
	
	/**
	 * Returns the Unicode decomposition of a given run of accented text.
	 * @param value The original string
	 * @return The string without accents
	 */
	static function decomposeUnicode(str:String):String
	{
		var pattern:Array<EReg> = new Array<EReg>();
		pattern[0] = new EReg('Š', 'g');
		pattern[1] = new EReg('Œ', 'g');
		pattern[2] = new EReg('Ž', 'g');
		pattern[3] = new EReg('š', 'g');
		pattern[4] = new EReg('œ', 'g');
		pattern[5] = new EReg('ž', 'g');
		pattern[6] = new EReg('[ÀÁÂÃÄÅ]', 'g');
		pattern[7] = new EReg('Æ', 'g');
		pattern[8] = new EReg('Ç', 'g');
		pattern[9] = new EReg('[ÈÉÊË]', 'g');
		pattern[10] = new EReg('[ÌÍÎÏ]', 'g');
		pattern[11] = new EReg('Ð', 'g');
		pattern[12] = new EReg('Ñ', 'g');
		pattern[13] = new EReg('[ÒÓÔÕÖØ]', 'g');
		pattern[14] = new EReg('[ÙÚÛÜ]', 'g');
		pattern[15] = new EReg('[ŸÝ]', 'g');
		pattern[16] = new EReg('Þ', 'g');
		pattern[17] = new EReg('ß', 'g');
		pattern[18] = new EReg('[àáâãäå]', 'g');
		pattern[19] = new EReg('æ', 'g');
		pattern[20] = new EReg('ç', 'g');
		pattern[21] = new EReg('[èéêë]', 'g');
		pattern[22] = new EReg('[ìíîï]', 'g');
		pattern[23] = new EReg('ð', 'g');
		pattern[24] = new EReg('ñ', 'g');
		pattern[25] = new EReg('[òóôõöø]', 'g');
		pattern[26] = new EReg('[ùúûü]', 'g');
		pattern[27] = new EReg('[ýÿ]', 'g');
		pattern[28] = new EReg('þ', 'g');
		
		var patternReplace:Array<String> = ['S', 'Oe', 'Z', 's', 'oe', 'z', 'A', 'Ae', 'C', 'E', 'I', 'D', 'N', 'O', 'U', 'Y', 'Th', 'ss', 'a', 'ae', 'c', 'e', 'i', 'd', 'n', 'o', 'u', 'y', 'th'];
		
		for (i in 0...pattern.length)
		{
			//str = StringTools.replace(str, pattern[i], patternReplace[i]);
			str = pattern[i].replace(str, patternReplace[i]);
			
		}
		return str;
	}
	
	public static function cleanSpecialChars(s:String):String
	{
		return decomposeUnicode(s);
	}
	
	public static function formatNumber(s:String, separator:String):String
	{
		var result:String = "";

		while (s.length > 3)
		{
			var chunk:String = s.substr( -3);
			s = s.substr(0, s.length - 3);
			result = separator + chunk + result;
		}

		if (s.length > 0)
		{
			result = s + result;
		}

		return result;
	}
}