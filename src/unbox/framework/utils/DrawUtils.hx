package unbox.framework.utils;
import openfl.display.Sprite;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class DrawUtils
{

	public function new() 
	{
		
	}
	
	public static function drawOutline(target:Sprite, color:UInt, thickness:Float = 1):Void
	{
		target.graphics.lineStyle(thickness, color);
		target.graphics.moveTo(target.x, target.y);
		target.graphics.lineTo(target.x, target.height);
		target.graphics.lineTo(target.width, target.height);
		target.graphics.lineTo(target.width, target.y);
		target.graphics.lineTo(target.x, target.y);
	}
	
}