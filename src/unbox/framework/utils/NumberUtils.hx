package unbox.framework.utils;

/**
 * Common utilities for dealing with numeric values.
 * @author UNBOX Design Studio © 2009-2012
 */
class NumberUtils
{
	public function new() {}
	
	public static function fmtNumDec(valor : Int, decimals : Int) : Int 
	{
		var decMult : Int = Std.int(Math.pow(10, decimals));
		return Std.int(Math.round(valor * decMult) / decMult);
    }
	
	public static function getRandomInt(start : Int, end : Int) : Int 
	{
		var rndNum : Int = start + Math.floor(Math.random() * (end - start));
		return rndNum;
    }
	
	public static function fixPadding(value : Float, padding : Int) : String 
	{
		var ret : String = "" + value;
		while (ret.length < padding)
			ret = "0" + ret;
			
		return ret;
    }
}