package unbox.framework.utils;

/**
 * Some array modification utilities.
 * Ported directly from AS3 EbookFramework
 * @author UNBOX Design Studio
 */
class ArrayUtils
{
    public function new()
    {
        
    }
    
    /*
	 * Procura um numero inteiro dentro de um array usando busca binaria e retorna
	 * o indice onde se encontra o nr inteiro, caso contrario retorna -1.
	 */
    public static function binarySearch(arr : Array<Int>, find : Int) : Int
    {
        var array : Array<Int> = arr.slice(0);
        var l : Int = 0;
        var r : Int = array.length - 1;
        var k : Int = 0;
        var found : Bool = false;
        
        while (l <= r && !found)
        {
            k = Std.int((l + r) / 2);
            
            if (find == array[k]) 
            {
                found = !found;
            }
            else if (find < array[k]) 
            {
                r = k - 1;
            }
            else 
            {
                l = k + 1;
            }
        }
        
        if (found) 
        {
            return k;
        }
        else 
        {
            return -1;
        }
    }
    
    public static function search(arr : Array<Dynamic>, value : Float) : Int
    {
        if (arr != null && !Math.isNaN(value)) 
        {
            var i : Int = 0;
            while (i < arr.length)
            {
                if (arr[i] == value) 
                    return i;
                i++;
            }
        }
        
        return -1;
    }
    
    /*
	 * Converte um vetor contendo string para um vetor contendo numeros
	 *
	 * @param: array - vetor a ser convertido
	 * @param: empty - especifique um valor para atribuir ao indice caso valor lido seja undefined
	 **/
    public static function toNumber(array : Array<String>, empty : Int = 0) : Array<Int>
    {
        var tmpArr : Array<Int> = new Array<Int>();
        for (i in 0...array.length){
            var tmpNum : Int = Std.parseInt(array[i]);
            
            if (!(Math.isNaN(tmpNum))) 
            {
                tmpArr[i] = tmpNum;
            }
            else if (empty != 0) 
            {
                tmpArr[i] = empty;
            }
        }
        
        return tmpArr;
    }
    
    /**
	 * Creates a random sequence and return an array of end-start length
	 * @param start The start value for the sequence
	 * @param end The end value for the sequence
	 * @param length the lenght of the new array. This value must be greater than end-start.
	 * @return
	 */
    public static function sortNonRepeatSeq(start : Int, end : Int, length : Int = 0) : Array<Int>
    {
        var sortedSeq : Array<Int> = [];
        
        if (length == 0) 
            length = end - start;
        
        var amount : Int = Std.int(Math.min(length, end - start));
        
        for (j in 0...amount){
            var rndNum : Int = NumberUtils.getRandomInt(start, end);
            
            // check if exists
            var n = 0;
			while (n < sortedSeq.length)
			{
				if (rndNum == sortedSeq[n])
				{
					rndNum = NumberUtils.getRandomInt(start, end);
					n = -1;
				}
				n++;
			}
			
            sortedSeq[j] = rndNum;
        }
        
        return sortedSeq;
    }
    
    /**
	 *
	 * @param	array The array to shuffle
	 * @return	a new randomized array. The functions does not modify the original array.
	 */
    public static function randomizeArray(array : Array<Dynamic>) : Array<Dynamic>
    {
        var newArray : Array<Dynamic> = new Array<Dynamic>();
        while (array.length > 0)
        {
            newArray.push(array.splice(Math.floor(Math.random() * array.length), 1));
        }
        
        return newArray;
    }
    
    /**
	 *
	 * @param	The size of the filled array
	 * @return	a new array filled with the specified value
	 */
    public static function fillArray(size : Int, value : Int) : Array<Int>
    {
        var newArray : Array<Int> = [];
        while (size > 0)
        {
            newArray.push(value);
            size--;
        }
        
        return newArray;
    }
    
    /**
	 * Fill an array with a sequence, Ascending or Descending
	 * @param	start	The intial sequence value
	 * @param	length	The size of the array
	 * @param	ascending	True or false.
	 * @return	A sequence filled array
	 */
    public static function fillSequence(start : Int, length : Int, ascending : Bool = true) : Array<Int>
    {
        var seqArray : Array<Int> = [];
        var i : Int = 0;
        
        if (ascending) 
        {
			var end = start + length;
            for (i in start...end) {
				seqArray.push(i);
            }
        }
        else 
        {
            i = start;
            while (i > start - length) {
				seqArray.push(i);
                i--;
            }
        }
        
        return seqArray;
    }
}
