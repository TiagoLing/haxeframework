package unbox.framework.utils;
import openfl.display.Sprite;
import openfl.geom.Rectangle;
import openfl.text.TextField;

/**
 * TODO: Deprecated. Remove
 * @author Tiago Ling Alexandre
 */
class TooltipParser
{

	public function new() 
	{
		
	}
	
	// #### TOOLTIP AUTO-ASSIGN TEST ####
	/**
	 * Checks the textfield for tooltip tags, removes them and returns an array with the enclosed words
	 * @param	tf	The textfield to search
	 * @return	An array with the words which were enclosed in tooltip tags
	 */
	public static function checkTooltips(tf:TextField):Array<String> {
		var tooltips:Array<String> = [];
		var currentIndex:Int = 0;
		trace("last tooltip Index : " + tf.htmlText.lastIndexOf("$ts"));
		while (currentIndex != tf.htmlText.lastIndexOf("$ts")) {
			
			if (tf.htmlText.indexOf("$ts", currentIndex) > -1) {
				var tooltipIndex:Int = tf.htmlText.indexOf("$ts", currentIndex);
				
				var tooltip:String = tf.htmlText.substring(tf.htmlText.indexOf("$ts") + 3, tf.htmlText.indexOf("$te"));
				var tooltipWithTags:String = "$ts" + tooltip + "$te";
				tf.htmlText = StringTools.replace(tf.htmlText, tooltipWithTags, tooltip);
				trace( "tooltip : " + tooltip );
				tooltips.push(tooltip);
				currentIndex = tooltipIndex;
				trace( "currentIndex : " + currentIndex );
			} else {
				break;
			}
		}
		
		
		trace("tooltips : " + tooltips.toString());
		return tooltips;
	}
	
	/**
	 * Draws all rects onto a Sprite
	 * @param	chars	
	 * @param	tf
	 * @param	view
	 */
	static function drawChars(chars:Array<Rectangle>, tf:TextField, view:Sprite):Void {
		for (i in 0...chars.length) {
			view.graphics.beginFill(0x009900, 0.4);
			view.graphics.drawRect(tf.x + chars[i].x, tf.y + chars[i].y, chars[i].width, chars[i].height);
			view.graphics.endFill();
		}
	}
	
	public static function checkForTooltips(tf:TextField, view:Sprite):Void {
		var tooltip:Sprite = new Sprite();
		tooltip.x = 0;
		tooltip.y = 0;
		view.addChild(tooltip);
		
		//while (tf.htmlText.indexOf("$ts") > -1) {
			var tfText:String = tf.htmlText;
			
			var startIndex:Int = tfText.indexOf("$ts") + 3;
			//trace("charAt '" + startIndex + "'startIndex : " + tf.htmlText.charAt(startIndex));
			var endIndex:Int = tfText.indexOf("$te");
			//trace("charAt '" + endIndex + "'endIndex : " + tf.htmlText.charAt(endIndex));
			
			var sb:StringBoundaries = new StringBoundaries(tf, startIndex, endIndex);
			
			//var ttWord:String = tfText.substring(startIndex, endIndex);
			var ttWord:String = tf.htmlText.substring(startIndex, endIndex);
			trace( "ttWord : " + ttWord );
			
			var ttWithTags:String = "$ts" + ttWord + "$te";
			trace( "ttWithTags : " + ttWithTags );
			
			//var length:Int = endIndex - startIndex - 1;
			var length:Int = endIndex - startIndex;
			trace( "length : " + length );
			
			//var charToDraw:Array<Int> = checkForTagsInTooltip(tf, startIndex, startIndex + length + 1);
			var charToDraw:Array<Int> = checkForTagsInTooltip(tf, startIndex, startIndex + length);
			
			trace( "charToDraw : " + charToDraw );
			trace(" Current word : " + tfText.substr(startIndex, length));
			checkForSpecialChars(tf);
			for (i in 0...length) {
				if (charToDraw[i] == 1) {
					
					var charRect:Rectangle = tf.getCharBoundaries((startIndex + i));
					
					//trace( "charRect : " + charRect.toString());
					
					if (charRect == null) {
						trace("Char '" + tfText.charAt((startIndex + i)) + "' at : '" + Std.string((startIndex + i)) + " has a null rect");
						continue;
					}
					
					trace("Gonna get the char at : '" + tfText.charAt((startIndex + i)) + "' : " + Std.string(startIndex + i));
					
					tooltip.graphics.beginFill(0x009900, 0.4);
					tooltip.graphics.drawRect(tf.x + charRect.x, tf.y + charRect.y, charRect.width, charRect.height);
					tooltip.graphics.endFill();
				} else {
					trace("Skipped char at '" + tfText.charAt((startIndex + i)) + "' : " + Std.string((startIndex + i)));
				}
			}
			
			trace("### tf before replacement : " + tfText);
			tf.htmlText = StringTools.replace(tf.htmlText, ttWithTags, ttWord);
			//tf.text = StringTools.replace(tfText, ttWithTags, ttWord);
			trace("### tf after replacement : " + tfText);
		//}
	}
	
	static function checkForSpecialChars(tf:TextField):Void {
		//var carriage:Int = tf.htmlText.indexOf("\r");
		//var tab:Int = tf.htmlText.indexOf("\t");
			
		var newLines:Array<Int> = [];
		var currentIndex:Int = 0;
		while (currentIndex != tf.htmlText.lastIndexOf("\n")) {
			if (tf.htmlText.indexOf("\n", currentIndex) > -1) {
				var newLineIndex:Int = tf.htmlText.indexOf("\n", currentIndex);
				newLines.push(newLineIndex);
				currentIndex = newLineIndex;
			}
		}
		trace("'" + newLines.length + "' new lines in textfield at positions : " + newLines.toString());
	}
	
	static function checkForTagsInTooltip(tf:TextField, startIndex:Int, length:Int):Array<Int> {
		
/*		trace("tf htmlText : " + tf.htmlText.substring(startIndex, length) + " | length : " + tf.htmlText.substring(startIndex, length).length);
		trace("tf htmlText has <br> : " + tf.htmlText.substring(startIndex, length).indexOf("<br>"));
		trace("tf htmlText has \\r : " + tf.htmlText.substring(startIndex, length).indexOf("\r"));
		trace("tf htmlText has \\n : " + tf.htmlText.substring(startIndex, length).indexOf("\n"));*/
		
		trace("tf text : " + tf.htmlText.substring(startIndex, length) + " | length : " + tf.htmlText.substring(startIndex, length).length);
		trace("tf text has <br> : " + tf.htmlText.substring(startIndex, length).indexOf("<br>"));
		trace("tf text has \\r : " + tf.htmlText.substring(startIndex, length).indexOf("\r"));
		trace("tf text has \\n : " + tf.htmlText.substring(startIndex, length).indexOf("\n"));
		
		var chars:Array<Int> = [];
		var rCharIndex:Int = 0;
		//for (i in startIndex...length) {
		var i:Int = startIndex;
		trace("Total chars : " + Std.string(length - startIndex));
		while (i < length) {
			var char:String = tf.htmlText.charAt(i);
			trace( "char at '" + i + "' : " + char );
			
			if (char == "\r") {
				chars.push( -1);
				rCharIndex = i;
				trace("!!! Char is \\r !!!");
				i++;
				continue;
			}
			
			if (char == "\n") {
				chars.push( -1);
				rCharIndex = i;
				trace("!!! Char is \\n !!!");
				i++;
				continue;
			}
			
			if (char == "<") {
				var br:String = tf.text.substr(i, 4);
				trace( "br : " + br );
				if (br == "<br>") {
					for (j in 0...4) {
						chars.push( -1);
						trace( "char at '" + Std.string(i + j) + "' : " + tf.htmlText.charAt(i + j) );
					}
					
					i += 4;
				}
				
/*				var bold:String = tf.htmlText.substr(i, 3);
				trace( "bold : " + bold );
				if (bold == "<b>") {
					for (j in 0...4) {
						chars.push( -1);
					}
				}*/
			}
			
			chars.push(1);
			i++;
		}
		
		return chars;
	}
	// #### END TEST TOOLTIP AUTO-ASSIGN
}