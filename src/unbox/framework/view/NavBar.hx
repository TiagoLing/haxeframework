package unbox.framework.view;

import com.hybrid.ui.ToolTip;
import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import openfl.Assets.AssetLibrary;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;
import openfl.system.ApplicationDomain;
import openfl.ui.Keyboard;
import unbox.framework.model.vo.NavBarData;
import unbox.framework.model.vo.PageData;
import unbox.framework.responder.TooltipResponder;
import unbox.framework.utils.StringUtils;
import unbox.framework.utils.TweenParser;
import unbox.framework.view.button.ButtonEvent;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;
import unbox.framework.view.button.ToggleButton;


/**
 * ...
 * @author Tiago Ling Alexandre
 */
class NavBar extends Sprite implements INavBar
{
	public var onCustomKey:Signal1<Dynamic>;
	public var onDashboardToggle:Signal0;
	public var onHelpToggle:Signal0;
	public var onBookmarkToggle:Signal1<PageData>;
	public var ttResponder:TooltipResponder;
	
	var _buttons:Array<IButton>;
	var navLayer:Sprite;
	var _onButtonPressed:Signal1<Int>;
	var isChangingPage:Bool;
	var currentPage:CoursePage;
	var data:NavBarData;
	var tt:ToolTip;
	var navBarView:Sprite;
	
	#if flash
	public function new(navLayer:Sprite, data:NavBarData, appDomain:ApplicationDomain) 
	#else
	public function new(navLayer:Sprite, data:NavBarData, mcLibrary:AssetLibrary) 
	#end
	{
		super();
		this.navLayer = navLayer;
		this.data = data;
		
		isChangingPage = false;
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain):Void
	#else
	function init(mcLibrary:AssetLibrary):Void
	#end
	{
		navBarView = null;
		
		#if flash
		var obj:Class<Dynamic> = appDomain.getDefinition("com.unbox.assets.NavBarView");
		var navBarInstance:Dynamic = Type.createInstance(obj, []);
		navBarView = cast(navBarInstance, Sprite);
		#else
		navBarView = mcLibrary.getMovieClip("com.unbox.assets.NavBarView");
		#end
		
		navBarView.x = 0;
		navBarView.y = 0;
		navLayer.addChild(navBarView);
		
		var navBarBtns:Array<String> = ["helpBtn", "menuBtn", "backBtn", "bookmarkBtn", "nextBtn"];
		
		_buttons = new Array<IButton>();
		for (i in 0...navBarBtns.length) {
			var btnView:Sprite = cast(navBarView.getChildByName(navBarBtns[i]), Sprite);
			if (btnView == null) {
				trace(navBarBtns[i] + " view does not exist! Please verify your nav.swf file.");
				continue;
			}
			
			var button:IButton;
			if (navBarBtns[i] == "menuBtn" || navBarBtns[i] == "bookmarkBtn") {
				button = new ToggleButton(btnView.x, btnView.y, btnView, navBarBtns[i]);
			} else {
				button = new GenericButton(btnView.x, btnView.y, btnView, navBarBtns[i]);
			}
			
			navBarView.removeChild(btnView);
			button.addHandlers(onButtonDown);
			button.add(navBarView);
			
			if (navBarBtns[i] == "nextBtn")
				button.view.addEventListener(MouseEvent.MOUSE_OVER, handleNextBtnTween);
			
			_buttons.push(button);
		}
		
		for (j in 0..._buttons.length)
		{
			trace("button '" + j + "' : " + _buttons[j].name + " | " + Type.typeof(_buttons[j]) + " | pos : " + _buttons[j].view.x + "," + _buttons[j].view.y);
		}
		
		//Keyboard handler
		navLayer.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyPress);
		
		// Signals
		//TODO: Check dependencies and remove this
		onCustomKey = new Signal1<Dynamic>();
		
		//Comunicates with NavController.handleNavInput
		_onButtonPressed = new Signal1<Int>();
		
		//Comunicates with Dashboard.toggle
		onDashboardToggle = new Signal0();
		
		//Comunicates with Dashboard.toggleHelp
		onHelpToggle = new Signal0();
		
		//Comunicates with DataController.addBookmark
		onBookmarkToggle = new Signal1<PageData>();
		
		//Tooltip Responder
		ttResponder = new TooltipResponder();
	}
	
	//This method is necessary due to temporal coupling.
	public function requestTooltip():Void
	{
		ttResponder.request.dispatch(ttResponder.response, data.tooltips);
	}
	
	public function onTooltipReceived(tt:ToolTip)
	{
		this.tt = tt;
		
		for (key in data.tooltips.items.keys())
		{
			var btn:IButton = getButton(key);
			if (btn != null)
			{
				var btnView:Sprite = btn.view;
				btnView.addEventListener(MouseEvent.ROLL_OVER, tooltipHandler, false, 0, true);
			}
		}
	}
	
	function tooltipHandler(e:MouseEvent):Void
	{
		var btn:Sprite = cast(e.target, Sprite);
		tt.show(btn, StringUtils.removeCDATA(data.tooltips.items.get(btn.name)), "");
	}
	
	function handleNextBtnTween(e:MouseEvent)
	{
		//TODO: Check if it is a transform tween and remove it accordingly
		var upState:Sprite = cast(cast(e.target, DisplayObjectContainer).getChildByName("up"), Sprite);
		TweenParser.removeTween(upState, true, true, true);
		
		//upState.filters = [];
		//upState.transform.colorTransform = new ColorTransform();
		//Actuate.stop(upState);
	}
	
	//TODO: Change _buttons to Map<String, IButton> and remove this
	//Must refactor the whole class for this change
	function getButton(name:String):IButton
	{
		for (i in 0..._buttons.length)
		{
			if (_buttons[i].name == name)
			{
				return _buttons[i];
			}
		}
		
		return null;
	}
	
	public function updateNavView(page:CoursePage):Void
	{
		if (page.data.pageProps != null)
		{
			for (i in 0...page.data.pageProps.length)
			{
				setButtonStatus(_buttons[i], page.data.pageProps[i]);
			}
		} else {
			trace("updateNavView -> pageProps do not exist! Enabling everything...");
			for (i in 0..._buttons.length) {
				_buttons[i].setEnabled(true);
				_buttons[i].setVisible(true);
			}
		}
	}
	
	/**
	 * Signal receiver from NavController, sets
	 * NavBar buttons as enabled or disabled
	 * @param	ids	A string in which each char corresponds
	 * to a button id in an array. The ids are, in order:
	 * helpBtn, menuBtn, backBtn, bookmarkBtn, nextBtn
	 */
	public function setButtons(ids:String):Void
	{
		trace( "NavBar.setButtons > ids : " + ids );
		trace( "_buttons.length : " + _buttons.length );
		
		var values:Array<String> = ids.split("");
		for (i in 0..._buttons.length)
		{
			setButtonStatus(_buttons[i], Std.parseInt(values[i]));
		}
		isChangingPage = false;
	}
	
	function setButtonStatus(btn:IButton, status:Int)
	{
		/* PageProps (ebook_nav.xml -> navbarStatus)
		 *  0 = disabled and visible;
		 *  1 = enabled and visible;
		 *  2 = disabled and invisible;
		 *  3 = ignore - left the values as they are.
		 * Order: helpBtn, menuBtn, backBtn, bookmarkBtn, nextBtn
		 */
		var enabledValue:Bool = status == 0 ? false : true;
		var visibleValue:Bool = status == 2 ? false : true;
		if (btn != null && status != 3)
		{
			btn.setVisible(visibleValue);
			btn.setEnabled(enabledValue);
			
			if (btn.name == "nextBtn" && enabledValue == true)
			{
				//Add Tween
				var tParser:TweenParser = new TweenParser();
				tParser.parse(navBarView, data.tweenData);
			}
		}
	}
	
	//TEST:
	//Logo visibility - currently used in cover page only
	public function setLogoVisible(value:Bool)
	{
		var logo:Sprite = cast(navBarView.getChildByName("logo"), Sprite);
		if (logo != null)
			logo.visible = value;
	}
	
	public function afterPageChange(?page:CoursePage):Void
	{
		isChangingPage = false;
		currentPage = page;
		
	}
	
	public function setBookmarkStatus(status:Bool):Void
	{
		cast(_buttons[3], ToggleButton).setToggle(status);
	}
	
	//Button Callback Handler
	function onButtonDown(e:Dynamic):Void
	{
		if (isChangingPage)
			return;
			
		var btnId:Int = 0;
		switch (e.name)
		{
			case "backBtn":
				btnId = -1;
				isChangingPage = true;
				_onButtonPressed.dispatch(btnId);
			case "nextBtn":
				trace("nextBtn");
				btnId = 1;
				isChangingPage = true;
				_onButtonPressed.dispatch(btnId);
			case "menuBtn": //Implemented for Accessibility (just goes to page 0)
				onDashboardToggle.dispatch();
			case "helpBtn":
				onHelpToggle.dispatch();
				return;
			case "bookmarkBtn":
				onBookmarkToggle.dispatch(currentPage.data);
				return;
			default:
				trace("Button name not recognized : " + e.name);
				return;
		}
	}
	
	function handleKeyPress(e:KeyboardEvent):Void 
	{
		if (isChangingPage == true || e.target.name == "searchText")
		{
			e.stopImmediatePropagation();
			return;
		}
			
		switch (e.keyCode)
		{
			//Used for accessibility: goes to the first page (menu)
/*			case Keyboard.NUMPAD_5:
				cast(_buttons[0], GenericButton).activate();*/
			case Keyboard.NUMPAD_4:
				cast(_buttons[2], GenericButton).activate();
			case Keyboard.NUMPAD_6:
				cast(_buttons[4], GenericButton).activate();
			case Keyboard.SPACE:
				e.target.dispatchEvent(new ButtonEvent(ButtonEvent.ACTIVATE, e.target, true));
			//HTML5 Debug
			#if html5
			case Keyboard.E:
				trace("currentPage view stats : " + currentPage.view.x + "," + currentPage.view.y + " | " + currentPage.view.width + "x" + currentPage.view.height);
				trace("currentPage visibility and alpha : " + currentPage.view.visible + " | " + currentPage.view.alpha);
				currentPage.view.graphics.beginFill(0x0000FF, 0.7);
				currentPage.view.graphics.drawRect(0, 0, 1000, 600);
				currentPage.view.graphics.endFill();
			#end
		}
	}
	
	function get_onButtonPressed():Signal1<Int> 
	{
		return _onButtonPressed;
	}
	
	function set_onButtonPressed(value:Signal1<Int>):Signal1<Int> 
	{
		return _onButtonPressed = value;
	}
	
	public var onButtonPressed(get_onButtonPressed, set_onButtonPressed):Signal1<Int>;
}