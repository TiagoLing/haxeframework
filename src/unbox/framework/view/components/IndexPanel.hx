package unbox.framework.view.components;
import unbox.framework.model.vo.IndexPanelData;
import motion.Actuate;
import motion.easing.Quint;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import unbox.framework.utils.ContentParser;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class IndexPanel implements IPanel
{
	public var view:Sprite;
	public var data:IndexPanelData;
	public var isVisible:Bool;
	
	var parent:Sprite;
	var list:UIList;
	var subList:UIList;
	
	var listScrollbar:Scrollbar;
	var subListScrollbar:Scrollbar;
	
	var buttons:Array<IButton>;
	
	public var dashboard:Dashboard;
	
	#if flash
	public function new(parent:Sprite, data:IndexPanelData, appDomain:ApplicationDomain) 
	#else
	public function new(parent:Sprite, data:IndexPanelData, mcLibrary:AssetLibrary) 
	#end
	{
		this.data = data;
		this.parent = parent;
		
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain):Void
	#else
	function init(mcLibrary:AssetLibrary):Void
	#end
	{
		//TODO: Set view (position, buttons, texts, etc)
		#if flash
		var classId:Class<Dynamic> = appDomain.getDefinition(data.viewClass);
		view = Type.createInstance(classId, []);
		#else
		view = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
		#end
		view.x = data.x;
		view.y = data.y;
		view.alpha = 0;
		view.visible = false;
		parent.addChild(view);
		
		buttons = new Array<IButton>();
		for (i in 0...4)
		{
			var btnView:Sprite = cast(view.getChildByName("btn_" + i), Sprite);
			var button:GenericButton = new GenericButton(btnView.x, btnView.y, btnView, "button_" + i);
			button.addHandlers(onButtonDown);
			view.removeChild(btnView);
			button.add(view);
			buttons.push(button);
		}
		
		#if flash
		list = new UIList(view, data.menuLists[0], appDomain);
		#else
		list = new UIList(view, data.menuLists[0], mcLibrary);
		#end
		list.onChange.add(onListChange);
		list.onClick.add(onListClick);
		
		#if flash
		subList = new UIList(view, data.menuLists[1], appDomain);
		#else
		subList = new UIList(view, data.menuLists[1], mcLibrary);
		#end
		subList.onChange.add(onListChange);
		subList.onClick.add(onListClick);
		
		//Populate menu list (delay needed for Stylesheet integration - TODO: Remove temporal coupling)
		//Actuate.timer(0.1).onComplete(populateList, [list]);
		Actuate.timer(0.1).onComplete(insertTexts);
		
		listScrollbar = new Scrollbar(view, data.scrollbars[0]);
		subListScrollbar = new Scrollbar(view, data.scrollbars[1]);
		
		view.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
	}
	
	function insertTexts() 
	{
		for (textData in data.texts)
		{
			var txt:TextField = ContentParser.parseTextField(textData, view);
		}
		
		populateList(list);
	}
	
	function onButtonDown(e:Dynamic) 
	{
		trace("Button '" + e.name + "' clicked!");
		var btnIndex:Int = Std.int(e.name.split("_")[1]);
		switch (btnIndex)
		{
			case 0:
				list.backPage();
			case 1:
				list.nextPage();
			case 2:
				subList.backPage();
			case 3:
				subList.nextPage();
		}
	}
	
	function onMouseWheel(e:MouseEvent):Void 
	{
		var tList:UIList = e.stageX > view.stage.stageWidth / 2 ? subList : list;
		e.delta > 0 ? tList.backPage() : tList.nextPage();
	}
	
	function populateList(pList:UIList, ?id:Int):Void
	{
		if (pList != null)
		{
			if (id != null)
			{
				subList.destroy();
				
				for (i in 0...data.menuData[id].submenuItems.length)
				{
					subList.insert(data.menuData[id].submenuItems[i].title);
				}
				
				subList.update();
				
			} else {
				list.destroy();
				
				for (i in 0...data.menuData.length)
				{
					list.insert(data.menuData[i].title);
				}
				
				list.update();
			}
		}
	}
	
	function onListChange(pList:UIList) 
	{
		var isNextEnabled:Bool = pList.totalItems > 0 && !(pList.currentPage == pList.totalPages - 1);
		var isBackEnabled:Bool = pList.totalItems > 0 && pList.currentPage != 0;
		
		if (pList == list && listScrollbar != null)
		{
			listScrollbar.steps = pList.totalPages;
			listScrollbar.current = pList.currentPage + 1;
			buttons[0].setEnabled(isBackEnabled);
			buttons[1].setEnabled(isNextEnabled);
		} else if (pList == subList && subListScrollbar != null) {
			subListScrollbar.steps = pList.totalPages;
			subListScrollbar.current = pList.currentPage + 1;
			buttons[2].setEnabled(isBackEnabled);
			buttons[3].setEnabled(isNextEnabled);
		}
	}
	
	function onListClick(pList:UIList, listButton:IButton) 
	{
		var btnId:Int = Std.parseInt(listButton.name.split("_")[1]);
		//Populates sublist
		if (pList == list)
		{
			populateList(subList, btnId);
		} else { //Navigates to page
			//TODO: How to get the correct page ID for navigation?
			dashboard.navigateTo(listButton.name);
		}
	}
	
	public function show():Void
	{
		view.visible = true;
		Actuate.tween(view, .25, { alpha:1 } ).ease(Quint.easeOut);
	}
	
	public function hide():Void
	{
		Actuate.tween(view, .25, { alpha:0 } ).ease(Quint.easeOut).onComplete(function f() { view.visible = false; } );
	}
}