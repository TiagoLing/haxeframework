package unbox.framework.view.components;
import haxe.xml.Fast;
import motion.Actuate;
import motion.easing.Quint;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import unbox.framework.model.vo.BookmarkPanelData;
import unbox.framework.model.vo.PageData;
import unbox.framework.utils.ContentParser;
import unbox.framework.utils.StringUtils;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class BookmarkPanel implements IPanel
{
	public var view:Sprite;
	public var data:BookmarkPanelData;
	public var isVisible:Bool;
	
	public var pageTitle:String;
	public var pageModuleIndex:Int;
	public var pageLocalIndex:Int;
	
	public var firstItem:UInt;
	public var lastItem:UInt;
	public var totalItems:UInt;
	
	var parent:Sprite;
	var list:UIList;
	var scrollbar:Scrollbar;
	var buttons:Array<IButton>;
	
	var status:String;
	var itemLabel:String;
	var statusTxt:TextField;
	
	//TODO: Find a better place to store this data
	var bookmarks:Array<PageData>;
	var hasInit:Bool;
	
	public var dashboard:Dashboard;
	
	#if flash
	public function new(parent:Sprite, data:BookmarkPanelData, appDomain:ApplicationDomain) 
	#else
	public function new(parent:Sprite, data:BookmarkPanelData, mcLibrary:AssetLibrary) 
	#end
	{
		this.parent = parent;
		this.data = data;
		
		hasInit = false;
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain):Void
	#else
	function init(mcLibrary:AssetLibrary):Void
	#end
	{
		#if flash
		var classId:Class<Dynamic> = appDomain.getDefinition(data.viewClass);
		view = Type.createInstance(classId, []);
		#else
		view = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
		#end
		view.x = data.x;
		view.y = data.y;
		view.alpha = 0;
		view.visible = false;
		parent.addChild(view);
		
		//TODO: Next and Back buttons
		buttons = new Array<IButton>();
		for (i in 0...2)
		{
			var btnView:Sprite = cast(view.getChildByName("btn_" + i), Sprite);
			var button:GenericButton = new GenericButton(btnView.x, btnView.y, btnView, "button_" + i);
			button.addHandlers(onButtonDown);
			view.removeChild(btnView);
			button.add(view);
			buttons.push(button);
		}
		
		//TODO: HTML5 Version (AssetLibrary)
		#if flash
		list = new UIList(view, data.list, appDomain);
		#else
		list = new UIList(view, data.list, mcLibrary);
		#end
		list.onChange.add(onListChange);
		list.onClick.add(onListClick);
		
		//TODO: Create scrollbar
		scrollbar = new Scrollbar(view, data.scrollbar);
		
		//TODO: Pages text logic
		Actuate.timer(0.1).onComplete(insertTexts);
		
		//TODO: Create events (mouseWheel & mouseClicks)
		view.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
		
		bookmarks = new Array<PageData>();
		hasInit = true;
	}
	
	function insertTexts() 
	{
		for (textData in data.texts)
		{
			var txt:TextField = ContentParser.parseTextField(textData, view, this);
			if (txt.name == "statusTxt") {
				status = StringUtils.removeCDATA(textData.node.title.innerHTML);
				statusTxt = txt;
				//Does not work
				//statusTxt.autoSize = TextFieldAutoSize.RIGHT;
			}
		}
	}
	
	function onButtonDown(e:Dynamic) 
	{
		var btnIndex:Int = Std.int(e.name.split("_")[1]);
		switch (btnIndex)
		{
			case 0:
				list.backPage();
			case 1:
				list.nextPage();
		}
	}
	
	function onMouseWheel(e:MouseEvent):Void 
	{
		e.delta > 0 ? list.backPage() : list.nextPage();
	}
	
	function onListChange(pList:UIList) 
	{
		var isNextEnabled:Bool = pList.totalItems > 0 && !(pList.currentPage == pList.totalPages - 1);
		var isBackEnabled:Bool = pList.totalItems > 0 && pList.currentPage != 0;
		
		if (scrollbar != null)
		{
			scrollbar.steps = pList.totalPages;
			scrollbar.current = pList.currentPage + 1;
			buttons[0].setEnabled(isBackEnabled);
			buttons[1].setEnabled(isNextEnabled);
		}
		
		//TODO: Result report (see EbookFramework)
		if (hasInit)
			updateStatus();
	}
	
	function updateStatus():Void
	{
		if (list.totalItems > 0)
		{
			firstItem = list.maxItemsPerPage * list.currentPage + 1;
			lastItem = list.maxItemsPerPage * (list.currentPage + 1);
			lastItem = lastItem > list.totalItems ? list.totalItems : lastItem;
			totalItems = list.totalItems;
			
			statusTxt.htmlText = StringUtils.parseTextVars(status, this);
		} else {
			statusTxt.text = "NULL";
		}
	}
	
	function onListClick(pList:UIList, listButton:IButton) 
	{
		var btnId:Int = Std.parseInt(listButton.name.split("_")[1]);
		//trace("Bookmark panel item '" + btnId + "' clicked.");
		dashboard.navigateTo(listButton.name);
	}
	
	//TODO: Implement insert and remove (check if PageData is really needed)
	public function insert(page:PageData):Void
	{
		pageTitle = page.title;
		pageModuleIndex = page.moduleIndex + 1;
		pageLocalIndex = page.localIndex + 1;
		
		var labelNode:Fast = data.texts.get("itemLabel");
		list.insert(labelNode, this);
		bookmarks.push(page);
		list.update();
	}
	
	public function remove(page:PageData):Void
	{
		for (i in 0...bookmarks.length)
		{
			if (bookmarks[i].globalIndex == page.globalIndex)
			{
				bookmarks.splice(i, 1);
				list.remove(i);
				break;
			}
		}
		list.update();
	}
	
	public function update(id:Int = -1):Void
	{
		list.update();
	}
	
	/**
	 * 
	 * @param	added
	 * @param	bookmarkData
	 */
	public function setBookmarks(added:Bool, bookmarkData:PageData):Void
	{
		if (added)
		{
			insert(bookmarkData);
		} else {
			remove(bookmarkData);
		}
	}
	
	/**
	 * Simple sorting function to organize an array of PageData objects
	 */
	static inline function arrayCompare(a:PageData, b:PageData):Int
	{
		if (a.globalIndex > b.globalIndex)
			return 1;
		else if (a.globalIndex < b.globalIndex)
			return -1;
		else
			return 0;
	}
	
	public function show():Void
	{
		//TODO: Get array of bookmarks from Status and insert them into the list
		view.visible = true;
		Actuate.tween(view, .25, { alpha:1 } ).ease(Quint.easeOut);
	}
	
	public function hide():Void
	{
		Actuate.tween(view, .25, { alpha:0 } ).ease(Quint.easeOut).onComplete(function f() { view.visible = false; } );
	}
}