package unbox.framework.view.components;
import openfl.display.Sprite;
import unbox.framework.view.Dashboard;

/**
 * @author Tiago Ling Alexandre
 */

interface IPanel 
{
	public var view:Sprite;
	public var isVisible:Bool;
	
	//Is it worth to change this into signal?
	//(each panel would have its own - very bloating)
	public var dashboard:Dashboard;
	
	public function show():Void;
	public function hide():Void;
}