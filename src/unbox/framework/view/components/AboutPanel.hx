package unbox.framework.view.components;
import motion.Actuate;
import motion.easing.Quint;
import openfl.display.Sprite;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import openfl.Assets;
import unbox.framework.model.vo.AboutPanelData;
import unbox.framework.utils.ContentParser;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class AboutPanel implements IPanel
{
	public var view:Sprite;
	public var data:AboutPanelData;
	public var isVisible:Bool;
	
	var parent:Sprite;
	
	public var dashboard:Dashboard;
	
	#if flash
	public function new(parent:Sprite, data:AboutPanelData, appDomain:ApplicationDomain) 
	#else
	public function new(parent:Sprite, data:AboutPanelData, mcLibrary:AssetLibrary) 
	#end
	{
		this.parent = parent;
		this.data = data;
		
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain):Void
	#else
	function init(mcLibrary:AssetLibrary):Void
	#end
	{
		#if flash
		var classId:Class<Dynamic> = appDomain.getDefinition(data.viewClass);
		view = Type.createInstance(classId, []);
		#else
		view = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
		#end
		view.x = data.x;
		view.y = data.y;
		view.alpha = 0;
		view.visible = false;
		parent.addChild(view);
		
		Actuate.timer(0.1).onComplete(insertTexts);
	}
	
	function insertTexts() 
	{
		for (textData in data.texts)
		{
			var txt:TextField = ContentParser.parseTextField(textData, view);
		}
	}
	
	public function show():Void
	{
		Actuate.tween(view, .25, { alpha:1 } ).ease(Quint.easeOut);
	}
	
	public function hide():Void
	{
		Actuate.tween(view, .25, { alpha:0 } ).ease(Quint.easeOut);
	}
	
}