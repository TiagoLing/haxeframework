package unbox.framework.view.components;
import unbox.framework.model.vo.ProgressMeterData;
import motion.Actuate;
import openfl.display.Sprite;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class BarMeter
{
	var barWidth:Float;
	var barHeight:Float;
	var barColor:UInt;
	var barColorAlpha:Float;
	var thumbColor:UInt;
	var thumbColorAlpha:Float;
	
	var steps:Int;
	var current:Int;
	var autoHideThumb:Bool;
	
	var thumb:Sprite;
	var scrollbar:Sprite;
	var thumbWidth:Float;
	var thumbHeight:Float;
	
	var isVerticalScrolling:Bool;
	var thumbFillMode:Bool;
	var hasInit:Bool;
	
	var parent:Sprite;
	var data:ProgressMeterData;
	var view:Sprite;
	
	public function new(parent:Sprite, data:ProgressMeterData) 
	{
		current = 1;
		steps = 1;
		barColor = 0xAE1F22;
		barColorAlpha = 1;
		thumbColor = 0xFFFFFF;
		thumbColorAlpha = 1;
		autoHideThumb = false;
		
		this.parent = parent;
		this.data = data;
		hasInit = false;
		
		init();
	}
	
	function init() 
	{
		view = new Sprite();
		view.name = "barMeterView";
		parent.addChild(view);
		view.x = data.x;
		view.y = data.y;
		barWidth = data.width;
		barHeight = data.height;
		barColor = data.scrollbarColor;
		barColorAlpha = data.scrollbarColorAlpha;
		thumbColor = data.thumbColor;
		thumbColorAlpha = data.thumbColorAlpha;
		thumbFillMode = data.thumbFillMode;
		autoHideThumb = data.autoHideThumb;
		
		scrollbar = new Sprite();
		thumb = new Sprite();
		
		view.addChild(scrollbar);
		view.addChild(thumb);
		
		isVerticalScrolling = barHeight > barWidth;
		
		draw();
		update();
		hasInit = true;
	}
	
	function draw()
	{
		steps = steps < 1 ? 1 : steps;
		
		scrollbar.graphics.clear();
		scrollbar.graphics.beginFill(barColor, barColorAlpha);
		scrollbar.graphics.drawRect(0, 0, barWidth, barHeight);
		scrollbar.graphics.endFill();
		
		thumbWidth = isVerticalScrolling ? barWidth : barWidth / steps;
		thumbHeight = isVerticalScrolling ? barHeight / steps : barHeight;
		
		thumb.graphics.clear();
		thumb.graphics.beginFill(thumbColor, thumbColorAlpha);
		thumb.graphics.drawRect(0, 0, thumbWidth, thumbHeight);
		thumb.graphics.endFill();
	}
	
	function update()
	{
		current = current > steps ? steps : current;
		current = current < 1 ? 1 : current;
		
		if (thumbFillMode)
		{
			var nW:Float = isVerticalScrolling ? thumbWidth : thumbWidth * current;
			var nH:Float = isVerticalScrolling ? thumbHeight * current : thumbHeight;
			Actuate.tween(thumb, 0.25, { width:nW, height:nH, alpha:1 }, true).onComplete(function f() { if (autoHideThumb) Actuate.tween(thumb, 1, { alpha:0 } ).delay(.5); } );
		} else {
			var nY:Float = isVerticalScrolling ? thumbHeight * (current - 1) : 0;
			var nX:Float = isVerticalScrolling ? 0 : thumbWidth * (current - 1);
			Actuate.tween(thumb, 0.25, { x:nX, y:nY, alpha:1 }, true).onComplete(function f() { if (autoHideThumb) Actuate.tween(thumb, 1, { alpha:0 } ).delay(.5); } );
		}
	}
	
	public function setCurrentIndex(index:Int)
	{
		current = index;
		
		if (hasInit)
			update();
	}
	
	public function setProgress(current:Int, total:Int)
	{
		var lastTotal:Int = steps;
		
		this.current = current + 1;
		steps = total;
		
		if (lastTotal != total)
			draw();
		
		if (hasInit)
			update();
	}
	
	public function show():Void
	{
		Actuate.tween(view, 0.25, { alpha:1 } );
	}
	
	public function hide():Void
	{
		Actuate.tween(view, 0.25, { alpha:0 } );
	}
}