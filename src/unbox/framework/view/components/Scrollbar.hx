package unbox.framework.view.components;
import motion.Actuate;
import openfl.display.Sprite;
import unbox.framework.model.vo.ScrollbarData;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class Scrollbar
{
	public var view:Sprite;
	var parent:Sprite;
	var data:ScrollbarData;
	
	var barHeight:Float;
	var barWidth:Float;
	var thumbHeight:Float;
	var thumbWidth:Float;
	var autoHideThumb:Bool;
	var barColorAlpha:Float;
	var barColor:UInt;
	
	var hasInit:Bool;
	
	var _steps:Int;
	public var steps(default, set):Int;
	var _current:Int;
	public var current(default, set):Int;
	
	var bar:Sprite;
	var thumb:Sprite;
	var isVerticalScrolling:Bool;
	var thumbColorAlpha:Float;
	var thumbColor:UInt;
	
	public function new(parent:Sprite, data:ScrollbarData) 
	{
		this.parent = parent;
		this.data = data;
		
		hasInit = false;
		_current = 1;
		_steps = 1;
		barColor = 0xAE1F22;
		barColorAlpha = 1;
		autoHideThumb = false;
		thumbColor = 0xFFFFFF;
		thumbColorAlpha = 1;
		barWidth = 0;
		barHeight = 0;
		
		init();
	}
	
	function init() 
	{
		barWidth = data.width;
		barHeight = data.height;
		barColor = data.scrollbarColor;
		barColorAlpha = data.scrollbarColorAlpha;
		thumbColor = data.thumbColor;
		thumbColorAlpha = data.thumbColorAlpha;
		autoHideThumb = data.autoHideThumb;
		
		view = new Sprite();
		bar = new Sprite();
		thumb = new Sprite();
		
		view.addChild(bar);
		view.addChild(thumb);
		parent.addChild(view);
		view.x = data.x;
		view.y = data.y;
		
		isVerticalScrolling = barHeight > barWidth;
		
		hasInit = true;
		
		draw();
		update();
	}
	
	function draw()
	{
		_steps = _steps < 1 ? 1 : _steps;
		
		bar.graphics.clear();
		bar.graphics.beginFill(barColor, barColorAlpha);
		bar.graphics.drawRect(0, 0, barWidth, barHeight);
		bar.graphics.endFill();
		
		thumbWidth = isVerticalScrolling ? barWidth : barWidth / _steps;
		thumbHeight = isVerticalScrolling ? barHeight / _steps : barHeight;
		
		thumb.graphics.clear();
		thumb.graphics.beginFill(thumbColor, thumbColorAlpha);
		thumb.graphics.drawRect(0, 0, thumbWidth, thumbHeight);
		thumb.graphics.endFill();
	}
	
	function update() 
	{
		_current = _current > _steps ? _steps : _current;
		_current = _current < 1 ? 1 : _current;
		
		var nY:Float = isVerticalScrolling ? thumbHeight * (_current - 1) : 0;
		var nX:Float = isVerticalScrolling ? 0 : thumbWidth * (_current - 1);
		
		Actuate.stop(thumb);
		Actuate.tween(thumb, 0.25, { x:nX, y:nY, alpha:1 }, true).onComplete(function f() { if (autoHideThumb) Actuate.tween(thumb, 1, { alpha:0 } ).delay(0.5); } );
	}
	
	public function set_steps(value:Int):Int
	{
		_steps = value;
		
		if (hasInit)
		{
			draw();
			update();
		}
		
		return _steps;
	}
	
	public function set_current(value:Int):Int
	{
		_current = value;
		
		if (hasInit)
			update();
			
		return _current;
	}
}