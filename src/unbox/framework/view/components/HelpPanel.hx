package unbox.framework.view.components;
import motion.Actuate;
import motion.easing.Quint;
import msignal.Signal.Signal0;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import unbox.framework.model.vo.HelpPanelData;
import unbox.framework.utils.ContentParser;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class HelpPanel implements IPanel
{
	public var view:Sprite;
	public var data:HelpPanelData;
	
	var parent:Sprite;
	
	public var onUserClick:Signal0;
	public var isVisible:Bool;
	
	public var dashboard:Dashboard;
	
	#if flash
	public function new(parent:Sprite, data:HelpPanelData, appDomain:ApplicationDomain) 
	#else
	public function new(parent:Sprite, data:HelpPanelData, mcLibrary:AssetLibrary) 
	#end
	{
		this.parent = parent;
		this.data = data;
		
		onUserClick = new Signal0();
		isVisible = false;
		
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain) 
	#else
	function init(mcLibrary:AssetLibrary) 
	#end
	{
		#if flash
		var classId:Class<Dynamic> = appDomain.getDefinition(data.viewClass);
		view = Type.createInstance(classId, []);
		#else
		view = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
		#end
		view.x = data.x;
		view.y = data.y;
		view.alpha = 0;
		view.visible = false;
		parent.addChild(view);
		
		Actuate.timer(0.1).onComplete(insertTexts);
	}
	
	function insertTexts() 
	{
		for (textData in data.texts)
		{
			var txt:TextField = ContentParser.parseTextField(textData, view);
		}
	}
	
	public function show():Void
	{
		Actuate.tween(view, 0.1, { x:0 } ).onComplete(function f() 
		{
			Actuate.tween(view, .25, { alpha:1 } ).ease(Quint.easeOut);
			view.useHandCursor = true;
			view.addEventListener(MouseEvent.CLICK, onMouseClick);
			isVisible = true;
		} );
	}
	
	public function hide():Void
	{
		Actuate.tween(view, .25, { alpha:0 } ).ease(Quint.easeOut).onComplete(function f() 
		{
			Actuate.tween(view, .1, { x: -1000 } );
			view.useHandCursor = false;
			view.removeEventListener(MouseEvent.CLICK, onMouseClick);
			isVisible = false;
		} );
	}
	
	function onMouseClick(e:MouseEvent):Void 
	{
		onUserClick.dispatch();
	}
	
}