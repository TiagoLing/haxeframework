package unbox.framework.view.components;
import haxe.xml.Fast;
import motion.Actuate;
import motion.easing.Quint;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.geom.Rectangle;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import unbox.framework.model.vo.ListData;
import unbox.framework.utils.ContentParser;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class UIList
{
	public var view:Sprite;
	public var data:ListData;
	
	var parent:Sprite;
	var columnWidth:Float;
	var rowHeight:Float;
	
	var buttons:Array<IButton>;
	var maxWidth:Float;
	var maxHeight:Float;
	
	public var maxItemsPerPage(default, null):UInt;
	public var totalPages(default, null):UInt;
	public var totalItems(default, null):UInt;
	public var currentPage:UInt;
	
	#if flash
	var buttonClass:Class<Dynamic>;
	#else
	var mcLibrary:AssetLibrary;
	#end
	
	var buttonPrefix:String;
	
	/**
	 * TODO: Comments on this signal
	 */
	public var onChange:Signal1<UIList>;
	
	/**
	 * TODO: Comments on this signal
	 */
	public var onClick:Signal2<UIList, IButton>;
	
	var scrollRect:Rectangle;
	
	#if flash
	public function new(parent:Sprite, data:ListData, appDomain:ApplicationDomain) 
	#else
	public function new(parent:Sprite, data:ListData, mcLibrary:AssetLibrary) 
	#end
	{
		this.parent = parent;
		this.data = data;
		
		onChange = new Signal1<UIList>();
		onClick = new Signal2<UIList, IButton>();
		buttons = new Array<IButton>();
		
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain) 
	#else
	function init(mcLibrary:AssetLibrary) 
	#end
	{
		currentPage = 0;
		totalPages = 0;
		totalItems = 0;
		maxWidth = data.width;
		maxHeight = data.height;
		maxItemsPerPage = data.columns * data.rows;
		rowHeight = maxHeight / data.rows;
		columnWidth = maxWidth / data.columns;
		buttonPrefix = "btn";
		#if flash
		buttonClass = appDomain.getDefinition(data.viewClass);
		#else
		this.mcLibrary = mcLibrary;
		#end
		
		view = new Sprite();
		view.name = "uiListView";
		view.x = data.x;
		view.y = data.y;
		
		parent.addChild(view);
		view.scrollRect = new Rectangle(0, 0, maxWidth, maxHeight);
		scrollRect = view.scrollRect.clone();
		
		onChange.dispatch(this);
	}
	
	public function nextPage():Void
	{
		if (currentPage < totalPages - 1)
			currentPage++;
			
		loadPage();
	}
	
	public function backPage():Void
	{
		if (currentPage > 0)
			currentPage--;
			
		loadPage();
	}
	
	public function gotoPage(index:UInt):Void
	{
		if (index >= totalPages)
			index = totalPages - 1;
		if (index < 0)
			index = 0;
			
		currentPage = index;
		
		loadPage();
	}
	
	function loadPage():Void
	{
		var nY:Float = maxHeight * currentPage;
		Actuate.tween(scrollRect, .5, { x:0, y:nY } ).ease(Quint.easeOut).onUpdate(function f() { view.scrollRect = scrollRect; } );
		onChange.dispatch(this);
	}
	
	public function insert(label:Fast = null, ?context:Dynamic):Void
	{
		var value:Int = buttons.length;
		var contains:Bool = view.getChildByName(buttonPrefix + "_" + value) != null;
		if (!contains && label != null)
		{
			var btView:Sprite = null;
			#if flash
			btView = Type.createInstance(buttonClass, []);
			#else
			btView = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
			#end
			
			var btn:GenericButton = null;
			if (label.has.pageID)
				btn = new GenericButton(0, 0, btView, label.att.pageID.split("/")[3]);
			else
				btn = new GenericButton(0, 0, btView, buttonPrefix + "_" + value);
			
			var phTxt:TextField = cast(btView.getChildByName("text"), TextField);
			btView.removeChild(phTxt);
			var txt = ContentParser.parseTextField(label, btn.view, context);
			txt.x = phTxt.x;
			txt.y = phTxt.y;
			txt.width = phTxt.width;
			txt.height = phTxt.height;
			btn.label = txt;
			
			//TODO: Find a way to register Module and page data here -> Maybe even pageGlobalID
			//trace("### label page id : " + label.att.pageID);
			
			btn.view.alpha = 0;
			btn.addHandlers(onBtnClick);
			buttons.push(btn);
			
			totalItems = buttons.length;
			totalPages = Math.ceil(totalItems / maxItemsPerPage);
			
			var index:Int = buttons.length - 1;
			var colId:Int = index % data.columns;
			var rowId:Int = Std.int(index / data.columns);
			var sp:Float = (maxHeight - (data.rows * btn.view.height)) / (2 * data.rows);
			var nX:Float = colId * columnWidth;
			var nY:Float = rowId * rowHeight + sp;
			
			if (data.columns == 1)
			{
				nX = 0;
				nY = index * rowHeight;
			}
			
			btn.view.x = nX;
			btn.view.y = nY;
			
			btn.add(view);
			
			onChange.dispatch(this);
		}
	}
	
	function onBtnClick(e:Dynamic):Void 
	{
		//trace("list button '" + e.name + "' has been clicked!");
		onClick.dispatch(this, e);
	}
	
	public function remove(value:Int):Void
	{
		var btn:IButton = buttons[value];
		view.removeChild(btn.view);
		buttons.splice(value, 1);
		
		onChange.dispatch(this);
	}
	
	public function update(value:Int = 0):Void
	{
		totalItems = buttons.length;
		totalPages = Math.ceil(totalItems / maxItemsPerPage);
		if (value < 0) value = 0;
		
		if (totalItems > 0)
		{
			for (i in value...buttons.length)
			{
				var btn:IButton = buttons[i];
				var colId:Int = i % data.columns;
				var rowId:Int = Std.int(i / data.columns);
				var sp:Float = (maxHeight - (data.rows * btn.view.height)) / (2 * data.rows);
				var nX:Float = colId * columnWidth;
				var nY:Float = rowId * rowHeight + sp;
				
				if (data.columns == 1)
				{
					nX = 0;
					nY = i * rowHeight + sp;
				}
				
				Actuate.tween(btn.view, .25, { x:nX, y:nY, alpha:1 } ).delay((i - value) * .01).ease(Quint.easeOut);
			}
		}
		
		gotoPage(currentPage);
	}
	
	public function destroy():Void
	{
		var count:Int = buttons.length - 1;
		while (count > -1)
		{
			remove(count);
			count--;
		}
		
		totalItems = 0;
		totalPages = 1;
		currentPage = 0;
		buttons = new Array<IButton>();
		
		onChange.dispatch(this);
	}
	
	public function getButton(index:Int):IButton
	{
		return buttons[index];
	}
}