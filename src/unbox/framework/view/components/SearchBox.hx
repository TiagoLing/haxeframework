package unbox.framework.view.components;
import motion.Actuate;
import openfl.Assets.AssetLibrary;
import openfl.display.MovieClip;
import openfl.display.Sprite;
import openfl.events.FocusEvent;
import openfl.events.KeyboardEvent;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.ui.Keyboard;
import unbox.framework.model.vo.SearchBoxData;
import unbox.framework.responder.SearchResponder;
import unbox.framework.utils.ContentParser;
import unbox.framework.view.button.GenericButton;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class SearchBox
{
	public var view:Sprite;
	public var searchText:TextField;
	
	var parent:Sprite;
	var data:SearchBoxData;
	var button:GenericButton;
	var loadAnim:MovieClip;
	var keyword:String;
	var isFocusSearch:Bool;
	
	public var responder:SearchResponder;
	
	#if flash
	public function new(parent:Sprite, data:SearchBoxData, appDomain:ApplicationDomain)
	#else
	public function new(parent:Sprite, data:SearchBoxData, mcLibrary:AssetLibrary)
	#end
	{
		this.parent = parent;
		this.data = data;
		
		responder = new SearchResponder();
		
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain) 
	#else
	function init(mcLibrary:AssetLibrary) 
	#end
	{
		#if flash
		var classId:Class<Dynamic> = appDomain.getDefinition(data.viewClass);
		view = Type.createInstance(classId, []);
		#else
		view = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
		#end
		view.x = data.x;
		view.y = data.y;
		parent.addChild(view);
		
		var btnView:Sprite = cast(view.getChildByName("btn_0"), Sprite);
		button = new GenericButton(btnView.x, btnView.y, btnView, "button_0");
		button.addHandlers(searchHandler);
		view.removeChild(btnView);
		button.add(view);
		
		isFocusSearch = false;
		
		loadAnim = cast(view.getChildByName("loaderIcon"), MovieClip);
		loadAnim.visible = false;
		loadAnim.stop();
		
		Actuate.timer(0.1).onComplete(insertTexts);
	}
	
	function insertTexts() 
	{
		//BUG: Using embedded fonts + Stylesheet for Input texts does not seem to work.
		
		//searchText = ContentParser.parseTextField(data.texts.get("searchText"), view);
		searchText = new TextField();
		searchText.type = TextFieldType.INPUT;
		ContentParser.parseVars(searchText, data.texts.get("searchText").node.vars);
		//searchText.styleSheet = ContentParser.stylesheet;
		searchText.embedFonts = false;
		searchText.htmlText = data.loadTitle;
		
		searchText.name = "searchText";
		searchText.multiline = false;
		searchText.selectable = true;
		//searchText.border = true;
		view.addChild(searchText);
		
		view.stage.focus = searchText;
		//Events
		view.stage.addEventListener(KeyboardEvent.KEY_UP, keyboardHandler);
		searchText.addEventListener(FocusEvent.FOCUS_IN, searchFocusHandler);
		searchText.addEventListener(FocusEvent.FOCUS_OUT, searchFocusHandler);
		
		enable(true);
	}
	
	function keyboardHandler(e:KeyboardEvent):Void 
	{
		if (e.target == searchText)
		{
			var hasMinLength:Bool = searchText.text.length >= data.minChars;
			var hasChanged:Bool = searchText.text != keyword;
			
			button.setEnabled(hasMinLength && hasChanged);
			button.setVisible(hasMinLength && hasChanged);
			
			if (e.keyCode == Keyboard.ENTER && hasMinLength && hasChanged)
			{
				dispatchKeyword();
				button.setEnabled(false);
				Actuate.timer(0.025).onComplete(searchText.setSelection, [0, searchText.text.length]);
			}
			
			//Is this really needed?
			e.stopPropagation();
		}
	}
	
	function searchFocusHandler(e:FocusEvent):Void
	{
		switch (e.type)
		{
			case FocusEvent.FOCUS_IN:
				if (searchText.text == data.title)
					searchText.text = "";
					
				isFocusSearch = true;
				Actuate.timer(0.05).onComplete(searchText.setSelection, [0, searchText.text.length]);
				
			case FocusEvent.FOCUS_OUT:
				if (searchText.text.length == 0)
				{
					button.setEnabled(false);
					searchText.text = data.title;
				}
				
				isFocusSearch = false;
				searchText.setSelection(0, 0);
		}
		
		e.stopPropagation();
	}
	
	function searchHandler(e:Dynamic)
	{
		loadAnim.visible = true;
		loadAnim.play();
		
		dispatchKeyword();
	}
	
	function dispatchKeyword():Void
	{
		if (searchText.text != keyword && searchText.text != "")
		{
			keyword = searchText.text;
			
			enable(false);
			
			//Dispatch signal to load and search all page XMLs
			trace("Responder SearchRequest dispatched! keyword : " + keyword);
			responder.request.dispatch(responder.response, keyword);
		}
	}
	
	public function reset():Void
	{
		keyword = "";
		
		searchText.text = data.title;
		searchText.type = TextFieldType.INPUT;
		
		button.setEnabled(false);
	}
	
	public function enable(value:Bool):Void
	{
		trace( "SearchBox.enable > value : " + value );
		button.setVisible(value);
		searchText.selectable = value;
		loadAnim.visible = !value;
		
		if (value)
		{
			if (searchText.text == data.loadTitle)
				searchText.text = data.title;
			searchText.type = TextFieldType.INPUT;
		} else {
			searchText.type = TextFieldType.DYNAMIC;
		}
	}
	
	public function show():Void
	{
		view.visible = true;
		Actuate.tween(view, 0.25, { alpha:1 } );
	}
	
	public function hide():Void
	{
		Actuate.tween(view, 0.25, { alpha:0 } ).onComplete(function f() { view.visible = false; } );
	}
}