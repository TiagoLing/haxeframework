package unbox.framework.view.components;
import haxe.xml.Fast;
import motion.Actuate;
import motion.easing.Quint;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.Lib;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import unbox.framework.model.vo.PageData;
import unbox.framework.model.vo.SearchPanelData;
import unbox.framework.services.LoadService;
import unbox.framework.utils.ContentParser;
import unbox.framework.utils.StringUtils;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;

/**
 * ...
 * @author Tiago Ling Alexandre
 */
class SearchPanel implements IPanel
{
	public var view:Sprite;
	public var data:SearchPanelData;
	public var isVisible:Bool;
	
	var parent:Sprite;
	var hasInit:Bool;
	var buttons:Array<IButton>;
	var list:UIList;
	var scrollbar:Scrollbar;
	
	var cachedSearchData:Array<String>;
	//Stores the array string indices for correct name assignment
	var cachedSearchIndices:Array<String>;
	
	//If possible, find a way to ditch this array
	var cachedPageData:Array<PageData>;
	
	var loadStart:Int;
	var keyword:String;
	
	//Public variables to be set by StringUtils.parseTextVars
	public var totalItems:Int;
	public var pageModuleIndex:Int;
	public var pageLocalIndex:Int;
	public var summary:String;
	
	public var firstItem:Int;
	public var lastItem:UInt;
	
	var itemLabel:String;
	var status:String;
	var statusTxt:TextField;
	
	public var dashboard:Dashboard;
	
	#if flash
	public function new(parent:Sprite, data:SearchPanelData, appDomain:ApplicationDomain) 
	#else
	public function new(parent:Sprite, data:SearchPanelData, mcLibrary:AssetLibrary) 
	#end
	{
		this.parent = parent;
		this.data = data;
		
		hasInit = false;
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain) 
	#else
	function init(mcLibrary:AssetLibrary) 
	#end
	{
		#if flash
		var classId:Class<Dynamic> = appDomain.getDefinition(data.viewClass);
		view = Type.createInstance(classId, []);
		#else
		view = cast(mcLibrary.getMovieClip(data.viewClass), Sprite);
		#end
		view.x = data.x;
		view.y = data.y;
		view.alpha = 0;
		view.visible = false;
		parent.addChild(view);
		
		buttons = new Array<IButton>();
		for (i in 0...2)
		{
			var btnView:Sprite = cast(view.getChildByName("btn_" + i), Sprite);
			var button:GenericButton = new GenericButton(btnView.x, btnView.y, btnView, "button_" + i);
			button.addHandlers(onButtonDown);
			view.removeChild(btnView);
			button.add(view);
			buttons.push(button);
		}
		
		#if flash
		list = new UIList(view, data.list, appDomain);
		#else
		list = new UIList(view, data.list, mcLibrary);
		#end
		list.onChange.add(onListChange);
		list.onClick.add(onListClick);
		
		scrollbar = new Scrollbar(view, data.scrollbar);
		
		Actuate.timer(0.1).onComplete(insertTexts);
		
		view.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
		
		cachedSearchData = new Array<String>();
		cachedSearchIndices = new Array<String>();
		hasInit = true;
		keyword = "";
		
		itemLabel = data.texts.get("itemLabel").node.title.innerHTML;
	}
	
	function insertTexts() 
	{
		for (textData in data.texts) 
		{
			var txt:TextField = ContentParser.parseTextField(textData, view);
			if (txt.name == "statusTxt") {
				status = StringUtils.removeCDATA(textData.node.title.innerHTML);
				statusTxt = txt;
				statusTxt.autoSize = TextFieldAutoSize.RIGHT;
			}
		}
	}
	
	function onButtonDown(e:Dynamic)
	{
		var btnIndex:Int = Std.int(e.name.split("_")[1]);
		switch (btnIndex)
		{
			case 0:
				list.backPage();
			case 1:
				list.nextPage();
		}
	}
	
	function onMouseWheel(e:MouseEvent)
	{
		e.delta > 0 ? list.backPage() : list.nextPage();
	}
	
	function onListChange(pList:UIList)
	{
		var isBackEnabled:Bool = pList.totalItems > 0 && pList.currentPage != 0;
		var isNextEnabled:Bool = pList.totalItems > 0 && !(pList.currentPage == pList.totalPages - 1);
		
		buttons[0].setEnabled(isBackEnabled);
		buttons[1].setEnabled(isNextEnabled);
		
		if (scrollbar != null)
		{
			scrollbar.steps = pList.totalPages;
			scrollbar.current = pList.currentPage + 1;
		}
		
		if (pList.totalItems > 0)
		{
			firstItem = list.maxItemsPerPage * list.currentPage + 1;
			lastItem = (list.maxItemsPerPage * (list.currentPage + 1));
			lastItem = lastItem > list.totalItems ? list.totalItems : lastItem;
			totalItems = list.totalItems;
			
			statusTxt.htmlText = StringUtils.parseTextVars(status, this);
		} else {
			statusTxt.htmlText = "";
		}
	}
	
	function onListClick(pList:UIList, listButton:IButton)
	{
		//trace("Button '" + listButton.name + "' clicked!");
		
		//TODO: How to get the correct page ID for navigation?
		dashboard.navigateTo(listButton.name);
	}
	
	public function show():Void
	{
		Actuate.tween(view, .25, { alpha:1 } ).ease(Quint.easeOut);
	}
	
	public function hide():Void
	{
		Actuate.tween(view, .25, { alpha:0 } ).ease(Quint.easeOut);
	}
	
	/**
	 * Response received from NavController when a request is made
	 * through the responder in SearchBox class.
	 * @param	data	Array of data from every page in the course.
	 */
	public function onSearchDataReceived(data:Array<PageData>, keyword:String)
	{
		this.keyword = keyword;
		
		cachedPageData = data;
		
		if (cachedSearchData.length == 0)
		{
			var numPages:Int = cachedPageData.length;
			var loader:LoadService = new LoadService();
			for (i in 0...numPages)
			{
				var textData:Fast = cast(data[i].assets.get("contentXML"), Fast);
				loader.add(data[i].contentUrls.get("contentXML"), "Module " + data[i].moduleIndex + ", Page " + data[i].localIndex);
				cachedSearchIndices.push("Module " + data[i].moduleIndex + ", Page " + data[i].localIndex);
			}
			
			loadStart = Lib.getTimer();
			loader.start(onSearchDataFetched);
		} else {
			search();
		}
	}
	
	/**
	 * Callback containing all contentXML files from all pages in the course.
	 * @param	data		Map containing all string assets from pages (contentXMLs)
	 * @param	appDomain	Optional application domain. Not used but present here for consistency.
	 */
	#if flash
	function onSearchDataFetched(data:Map<String, Dynamic>, ?appDomain:ApplicationDomain) 
	#else
	function onSearchDataFetched(data:Map<String, Dynamic>) 
	#end
	{
		trace("SearchData fetched! Load time : " + Std.string(Lib.getTimer() - loadStart) + " miliseconds.");
		var count:Int = 0;
		
		for (i in 0...cachedSearchIndices.length)
		{
			var rawData:Xml = Xml.parse(data.get(cachedSearchIndices[i]));
			var textData:Fast = new Fast(rawData.firstElement());
			
			for (contentData in textData.nodes.content)
			{
				if (contentData.att.type == "text")
				{
					var rawText:String = "";
					for (textContent in contentData.nodes.content)
					{
						var tempText:String = StringUtils.removeCDATA(textContent.node.body.innerHTML);
						tempText = StringUtils.stripTags(tempText);
					
						rawText += " " + tempText;
					}
					
					cachedSearchData.push(rawText);
				}
			}
		}
		
		search();
	}
	
	function search():Void
	{
		list.destroy();
		
		if (keyword != "" && cachedSearchData.length > 0)
		{
			for (i in 0...cachedSearchData.length)
			{
				var strIndex:Int = cachedSearchData[i].toLowerCase().indexOf(keyword, 0);
				if (strIndex > -1)
				{
					summary = parseSummary(cachedSearchData[i], keyword);
					if (summary.length > 3)
					{
						insert(cachedPageData[i]);
					}
				}
			}
			
			list.update();
			
			dashboard.changePanel(3);
			dashboard.getSearchBox().enable(true);
		}
	}
	
	public function insert(pageData:PageData):Void
	{
		pageModuleIndex = pageData.moduleIndex + 1;
		pageLocalIndex = pageData.localIndex + 1;
		totalItems = list.totalItems + 1;
		
		statusTxt.htmlText = StringUtils.parseTextVars(status, this);
		
		list.insert(data.texts.get("itemLabel"), this);
		list.update();
	}
	
	//TODO: invalidate -> If in training mode, check whether any of the
	//		search results are inside pages that are locked.
	function parseSummary(value:String, keyword:String):String
	{
		var kArr:Array<String> = value.split("]]>");
		var kArrLen:Int = kArr.length;
		var tmpStr:String = "";
		
		for (i in 0...kArrLen)
		{
			var str:String = StringUtils.cleanSpecialChars(kArr[i].toLowerCase());
			var foundStr:Int = str.indexOf(StringUtils.cleanSpecialChars(keyword));
			if (foundStr > -1)
			{
				var fstInd:Int = 0;
				var lstInd:Int = 0;
				var strLen:Int = data.summaryLength;
				
				strLen -= keyword.length;
				lstInd = Std.int(Math.min(foundStr + strLen * .5, str.length));
				
				strLen -= lstInd - foundStr;
				fstInd = foundStr - strLen;
				
				//-- find word begining
				while (fstInd >= 0)
				{
					if (str.charAt(fstInd) == " " || str.charAt(fstInd) == ".")
					{
						fstInd++;
						break;
					}
					fstInd--;
				}
				
				while (lstInd <= str.length)
				{
					if (str.charAt(lstInd) == " " || str.charAt(lstInd) == ".")
						break;
					lstInd++;
				}
				
				if (fstInd < 0)
					fstInd = 0;
				
				tmpStr = kArr[i].substring(fstInd, lstInd);
				
				if (fstInd > 0)
					tmpStr = "..." + tmpStr;
				
				if (lstInd < str.length)
					tmpStr += "...";
				
				break;
			}
		}
		
		return tmpStr;
	}
}