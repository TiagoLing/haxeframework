package unbox.framework.view;
import msignal.Signal.Signal2;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.system.ApplicationDomain;
import com.hybrid.ui.ToolTip;
import unbox.framework.model.vo.PageData;
import unbox.framework.model.vo.PageDataType;
import unbox.framework.view.button.IButton;

/**
 * Base class for course pages, all types of pages must extends this.
 * @author Tiago Ling Alexandre
 */
class CoursePage implements IPage
{
	public var view:Sprite;
	public var data:PageData;
	public var context:CourseContext;
	public var name:String;
	#if flash
	public var stylesheet:flash.text.StyleSheet;
	public var appDomain:ApplicationDomain;
	#end
	
	//This array is here so ContentParser can read and add buttons to the page
	//TODO: Remove this buttons array - this can be made simpler 
	public var buttons:Array<IButton>;
	public var tt:ToolTip;
	public var ttTexts:Map<String, String>;
	public var clickActions:Map<String, Void->Void>;
	public var overActions:Map < String, Void->Void > ;
	
	//Mandatory interaction -> Set through XML with the 'mustInteract' attribute
	public var totalInteractions:Int;
	public var mInteractions:Map<String, Bool>;
	
	var _sendData:Signal2<PageDataType, Array<Int>>;
	
	public function new() 
	{
		buttons = new Array<IButton>();
		_sendData = new Signal2<PageDataType, Array<Int>>();
		clickActions = new Map<String,Void->Void>();
		overActions = new Map < String, Void->Void > ();
		totalInteractions = 0;
		mInteractions = new Map<String, Bool>();
		
		init();
	}
	
	public function handleTooltips(e:MouseEvent):Void
	{
		var spr:Sprite = cast(e.target, Sprite);
		tt.show(spr, ttTexts.get(spr.name), "");
	}
	
	public function handleMouseClick(e:MouseEvent)
	{
		var func:Void->Void = clickActions.get(e.target.name);
		func();
		
		checkMandatoryInteractions(e.target.name);
	}
	
	public function handleMouseOver(e:MouseEvent)
	{
		var func:Void->Void = overActions.get(e.target.name);
		func();
		
		checkMandatoryInteractions(e.target.name);
	}
	
	function checkMandatoryInteractions(target:String)
	{
		var interactions:Int = 0;
		for (key in mInteractions.keys())
		{
			if (key == target && mInteractions.get(target) == false)
				mInteractions.set(target, true);
				
			if (mInteractions.get(key) == true)
				interactions++;
		}
		
		if (totalInteractions > 0 && interactions == totalInteractions)
			context.navController.setNavButtons("33331");
	}
	
	//TODO: Remove this method eventually - it is used only for testing
	//method call from the XML
	public function testMethod(a:Int, b:Bool, c:String, d:Float)
	{
		trace("'A' should be Int    : " + Std.is(a, Int));
		trace("'B' should be Bool   : " + Std.is(b, Bool));
		trace("'C' should be String : " + Std.is(c, String));
		trace("'D' should be Float  : " + Std.is(d, Float));
		
		var spr = cast(view.getChildByName(c), Sprite);
		trace("spr '" + c + "' mouse enabled and children : " + spr.mouseEnabled + " | " + spr.mouseChildren);
	}
	
	public function init():Void {}
	
	public function transitionIn(data:Dynamic = null):Void {}
	
	public function transitionInComplete(data:Dynamic = null):Void {}
	
	public function transitionOut(data:Dynamic = null):Void {}
	
	public function transitionOutComplete(data:Dynamic = null):Void {}
	
	//TODO: Create method for page disposal.
	
	function get_sendData():Signal2<PageDataType, Array<Int>> 
	{
		return _sendData;
	}
	
	function set_sendData(value:Signal2<PageDataType, Array<Int>>):Signal2<PageDataType, Array<Int>> 
	{
		return _sendData = value;
	}
	
	public var sendData(get_sendData, set_sendData):Signal2<PageDataType, Array<Int>>;
}