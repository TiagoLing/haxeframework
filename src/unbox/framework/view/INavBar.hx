package unbox.framework.view;
import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import unbox.framework.model.vo.PageData;

/**
 * @author Tiago Ling Alexandre
 */

interface INavBar 
{
  public function updateNavView(page:CoursePage):Void;
  public function setButtons(ids:String):Void;
  public function afterPageChange(?page:CoursePage):Void;
  
  public function setBookmarkStatus(status:Bool):Void;
  
  public var onButtonPressed(get_onButtonPressed, set_onButtonPressed):Signal1<Int>;
  public var onCustomKey:Signal1<Dynamic>;
  public var onDashboardToggle:Signal0;
  public var onHelpToggle:Signal0;
  public var onBookmarkToggle:Signal1<PageData>;
}