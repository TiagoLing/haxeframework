package unbox.framework.view;

import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import openfl.display.Sprite;
import openfl.events.KeyboardEvent;
import openfl.system.ApplicationDomain;
import openfl.text.TextField;
import openfl.ui.Keyboard;
import unbox.framework.model.vo.NavBarData;
import unbox.framework.model.vo.PageData;
import unbox.framework.utils.Logger;
import unbox.framework.view.button.ButtonEvent;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;


/**
 * ...
 * @author Tiago Ling Alexandre
 */
class NavBarAcc extends Sprite implements INavBar
{
	var _buttons:Array<IButton>;
	var navBarText:TextField;
	var navLayer:Sprite;
	var _onButtonPressed:Signal1<Int>;
	var isChangingPage:Bool;
	
	public var onCustomKey:Signal1<Dynamic>;
	public var onDashboardToggle:Signal0;
	public var onHelpToggle:Signal0;
	public var onBookmarkToggle:Signal1<PageData>;
	var data:NavBarData;
	
	#if flash
	public function new(navLayer:Sprite, data:NavBarData, appDomain:ApplicationDomain) 
	#else
	public function new(navLayer:Sprite, data:NavBarData) 
	#end
	{
		super();
		this.navLayer = navLayer;
		this.data = data;
		isChangingPage = false;
		#if flash
		init(appDomain);
		#else
		init();
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain):Void
	#else
	function init():Void
	#end
	{
		//### Simplified navigation for Accessibility support ###
		var navBarInstance:Dynamic = null;
		#if flash
		var obj:Class<Dynamic> = appDomain.getDefinition("com.unbox.assets.NavBarViewAlt");
		navBarInstance = Type.createInstance(obj, []);
		#end
		
		var navBarView:Sprite = cast(navBarInstance, Sprite);
		navBarView.x = 0;
		navBarView.y = 0;
		navLayer.addChild(navBarView);
		
		//### Simplified navigation for Accessibility support ###
		var navBarBtns:Array<String> = ["", "menuBtn", "backBtn", "", "nextBtn"];
		_buttons = new Array<IButton>();
		for (i in 0...navBarBtns.length) 
		{
			var btnView:Sprite = cast(navBarInstance.getChildByName(navBarBtns[i]), Sprite);
			if (btnView == null) 
			{
				Logger.log(navBarBtns[i] + " view does not exist! Please verify your nav.swf file.");
				continue;
			}
			
			var button:IButton = new GenericButton(btnView.x, btnView.y, btnView, navBarBtns[i]);
			
			navBarInstance.removeChild(btnView);
			button.addHandlers(onButtonDown);
			button.add(navBarView);
			_buttons.push(button);
		}
		
		onCustomKey = new Signal1<Dynamic>();
		navLayer.stage.addEventListener(KeyboardEvent.KEY_DOWN, handleKeyPress);
		
		_onButtonPressed = new Signal1<Int>();
		onDashboardToggle = new Signal0();
		onHelpToggle = new Signal0();
	}
	
	public function updateNavView(page:CoursePage):Void
	{
		trace( "NavBar.updateNavView > page : " + page );
		//Page Props implementation
		if (page.data.pageProps != null)
		{
			for (i in 0...page.data.pageProps.length)
			{
				var value:Bool = page.data.pageProps[i] == 0 ? false : true;
				if (_buttons[i] != null)
				{
					_buttons[i].setVisible(true);
					_buttons[i].setEnabled(value);
				}
			}
		} else {
			//Cover Page. Set everything but next button invisible
			trace("NavBar ->> _buttons.length : " + _buttons.length);
			for (i in 0..._buttons.length)
			{
				_buttons[i].name == "nextBtn" ? _buttons[i].setEnabled(true) : _buttons[i].setVisible(false);
			}
		}
	}
	
	public function setButtons(ids:String):Void
	{
		trace( "NavBar.setButtons > ids : " + ids );
		var values:Array<String> = ids.split("");
		for (i in 0..._buttons.length)
		{
			var value = Std.parseInt(values[i]);
			value == 0 ? _buttons[i].setEnabled(false) : _buttons[i].setEnabled(true);
		}
		isChangingPage = false;
	}
	
	//Temp signal handler ->> Find a better way to do this
	public function afterPageChange(?page:CoursePage):Void
	{
		isChangingPage = false;
	}
	
	function onButtonDown(e:Dynamic):Void
	{
		if (isChangingPage)
			return;
			
		var btnId:Int = 0;
		switch (e.name)
		{
			case "backBtn":
				btnId = -1;
				isChangingPage = true;
			case "nextBtn":
				btnId = 1;
				isChangingPage = true;
			case "menuBtn": //Implemented for Accessibility (just goes to page 0)
				btnId = 0;
				isChangingPage = true;
			case "helpBtn":
				Logger.log("helpBtn logic still not implemented");
			case "bookmarkBtn":
				Logger.log("bookmarkBtn logic still not implemented");
			default:
				Logger.log("Button name not recognized : " + e.target.name);
		}
		
		_onButtonPressed.dispatch(btnId);
	}
	
	function handleKeyPress(e:KeyboardEvent):Void 
	{
		if (isChangingPage == true)
		{
			e.stopImmediatePropagation();
			return;
		}
			
		switch (e.keyCode)
		{
			case Keyboard.NUMPAD_5:
				cast(_buttons[0], GenericButton).activate();
			case Keyboard.NUMPAD_4:
				cast(_buttons[1], GenericButton).activate();
			case Keyboard.NUMPAD_6:
				cast(_buttons[2], GenericButton).activate();
			case Keyboard.SPACE:
				e.target.dispatchEvent(new ButtonEvent(ButtonEvent.ACTIVATE, e.target, true));
		}
	}
	
	public function setBookmarkStatus(status:Bool):Void { }
	
	function get_onButtonPressed():Signal1<Int> 
	{
		return _onButtonPressed;
	}
	
	function set_onButtonPressed(value:Signal1<Int>):Signal1<Int> 
	{
		return _onButtonPressed = value;
	}
	
	public var onButtonPressed(get_onButtonPressed, set_onButtonPressed):Signal1<Int>;
}