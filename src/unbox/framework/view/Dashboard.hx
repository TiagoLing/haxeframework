package unbox.framework.view;
import com.hybrid.ui.ToolTip;
import motion.Actuate;
import motion.easing.Expo;
import msignal.Signal.Signal1;
import openfl.Assets.AssetLibrary;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.system.ApplicationDomain;
import unbox.framework.model.vo.DashboardData;
import unbox.framework.model.vo.PageData;
import unbox.framework.responder.TooltipResponder;
import unbox.framework.utils.StringUtils;
import unbox.framework.view.button.GenericButton;
import unbox.framework.view.button.IButton;
import unbox.framework.view.components.AboutPanel;
import unbox.framework.view.components.BookmarkPanel;
import unbox.framework.view.components.HelpPanel;
import unbox.framework.view.components.IndexPanel;
import unbox.framework.view.components.IPanel;
import unbox.framework.view.components.SearchBox;
import unbox.framework.view.components.SearchPanel;

/**
 * Sliding panel class used as container for different lists and UI items
 * @author Tiago Ling Alexandre
           Unbox Design Studio © 2009-2014
 */
class Dashboard
{
	var navLayer:Sprite;
	var closedX:Float;
	var closedY:Float;
	var openedX:Float;
	var openedY:Float;
	var view:Sprite;
	
	var buttons:Array<IButton>;
	var currentPanel:Int;
	var panels:Array<IPanel>;
	var data:DashboardData;
	var searchBox:SearchBox;
	var tt:ToolTip;
	
	public var isOpened:Bool;
	public var onPanelButtonPressed:Signal1<String>;
	public var ttResponder:TooltipResponder;
	
	#if flash
	public function new(navLayer:Sprite, data:DashboardData, appDomain:ApplicationDomain) 
	#else
	public function new(navLayer:Sprite, data:DashboardData, mcLibrary:AssetLibrary)
	#end
	{
		this.navLayer = navLayer;
		this.data = data;
		#if flash
		init(appDomain);
		#else
		init(mcLibrary);
		#end
	}
	
	#if flash
	function init(appDomain:ApplicationDomain) 
	#else
	function init(mcLibrary:AssetLibrary) 
	#end
	{
		trace( "Dashboard.init" );
		//View
		#if flash
		var obj:Class<Dynamic> = appDomain.getDefinition("com.unbox.assets.DashboardView");
		var navBarInstance:Dynamic = Type.createInstance(obj, []);
		view = cast(navBarInstance, Sprite);
		#else
		view = cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardView"), Sprite);
		#end
		
		openedX = data.openX;
		openedY = data.openY;
		closedX = data.x;
		closedY = data.y;
		
		view.x = closedX;
		view.y = closedY;
		
		navLayer.addChild(view);
		isOpened = false;
		
		//TODO: Remove hardcoding (must include this info inside DashboardData)
		//Buttons
		buttons = new Array<IButton>();
		
		var indexViewInstance:Sprite = null;
		#if flash
		var idObj:Class<Dynamic> = appDomain.getDefinition("com.unbox.assets.DashboardIndexBtn");
		indexViewInstance = Type.createInstance(idObj, []);
		#else
		indexViewInstance = cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardIndexBtn"), Sprite);
		#end
		var indexButton:GenericButton = new GenericButton(133, 50, indexViewInstance, "indexBtn");
		indexButton.addHandlers(handleButtonDown);
		indexButton.add(view);
		buttons.push(indexButton);
		
		var bookmarkViewInstance:Sprite = null;
		#if flash
		var bkObj:Class<Dynamic> = appDomain.getDefinition("com.unbox.assets.DashboardBookmarkBtn");
		bookmarkViewInstance = Type.createInstance(bkObj, []);
		#else
		bookmarkViewInstance = cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardBookmarkBtn"), Sprite);
		#end
		var bookmarkButton:GenericButton = new GenericButton(183, 50, bookmarkViewInstance, "bookmarkBtn");
		bookmarkButton.addHandlers(handleButtonDown);
		bookmarkButton.add(view);
		buttons.push(bookmarkButton);
		
		var aboutViewInstance:Sprite = null;
		#if flash
		var atObj:Class<Dynamic> = appDomain.getDefinition("com.unbox.assets.DashboardAboutBtn");
		aboutViewInstance = Type.createInstance(atObj, []);
		#else
		aboutViewInstance =  cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardAboutBtn"), Sprite);
		#end
		var aboutButton:GenericButton = new GenericButton(233, 50, aboutViewInstance, "aboutBtn");
		aboutButton.addHandlers(handleButtonDown);
		aboutButton.add(view);
		buttons.push(aboutButton);
		
		//Panels
		panels = new Array<IPanel>();
		
		var indexPanel:IndexPanel = null;
		#if flash
		indexPanel = new IndexPanel(view, data.indexPanel, appDomain);
		#else
		indexPanel = new IndexPanel(view, data.indexPanel, mcLibrary);
		#end
		indexPanel.dashboard = this;
		panels.push(indexPanel);
		
		var bookmarkPanel:BookmarkPanel = null;
		#if flash
		bookmarkPanel = new BookmarkPanel(view, data.bookmarkPanel, appDomain);
		#else
		bookmarkPanel = new BookmarkPanel(view, data.bookmarkPanel, mcLibrary);
		#end
		bookmarkPanel.dashboard = this;
		panels.push(bookmarkPanel);
		
		var aboutPanel:AboutPanel = null;
		#if flash
		aboutPanel = new AboutPanel(view, data.aboutPanel, appDomain);
		#else
		aboutPanel = new AboutPanel(view, data.aboutPanel, mcLibrary);
		#end
		aboutPanel.dashboard = this;
		panels.push(aboutPanel);
		
		var searchPanel:SearchPanel = null;
		#if flash
		searchPanel = new SearchPanel(view, data.searchPanel, appDomain);
		#else
		searchPanel = new SearchPanel(view, data.searchPanel, mcLibrary);
		#end
		searchPanel.dashboard = this;
		panels.push(searchPanel);
		
		var helpPanel:HelpPanel = null;
		#if flash
		helpPanel = new HelpPanel(cast(view.parent, Sprite), data.helpPanel, appDomain);
		#else
		helpPanel = new HelpPanel(cast(view.parent, Sprite), data.helpPanel, mcLibrary);
		#end
		helpPanel.onUserClick.add(toggleHelp);
		helpPanel.dashboard = this;
		panels.push(helpPanel);
		
		//Search Box
		var sbParent:Sprite = data.searchBox.alwaysVisible ? navLayer : view;
		#if flash
		searchBox = new SearchBox(sbParent, data.searchBox, appDomain);
		#else
		searchBox = new SearchBox(sbParent, data.searchBox, mcLibrary);
		#end
		
		currentPanel = 0;
		
		onPanelButtonPressed = new Signal1<String>();
		
		ttResponder = new TooltipResponder();
	}
	
	//TODO: Put this inside CourseContext
	public function requestTooltip():Void
	{
		//Must send view
		ttResponder.request.dispatch(ttResponder.response, data.tooltips);
	}
	
	//TODO: Put this inside TooltipService
	public function onTooltipReceived(tt:ToolTip)
	{
		this.tt = tt;
		
		for (key in data.tooltips.items.keys())
		{
			var btn:IButton = getButton(key);
			if (btn != null)
			{
				var btnView:Sprite = btn.view;
				btnView.addEventListener(MouseEvent.ROLL_OVER, tooltipHandler, false, 0, true);
			}
		}
	}
	
	//TODO: Put this inside TooltipService
	function tooltipHandler(e:MouseEvent):Void
	{
		var btn:Sprite = cast(e.target, Sprite);
		tt.show(btn, StringUtils.removeCDATA(data.tooltips.items.get(btn.name)), "");
	}
	
	//TODO: Change _buttons to Map<String, IButton> and remove this
	//Must refactor the whole class 
	function getButton(name:String):IButton
	{
		for (i in 0...buttons.length)
		{
			if (buttons[i].name == name)
			{
				return buttons[i];
			}
		}
		
		return null;
	}
	
	/**
	 * Internal handler for Dashboard button
	 * clicks.
	 * @param	view	The view of the pressed
	 * 					button
	 */
	function handleButtonDown(view:Dynamic)
	{
		switch (view.name)
		{
			case "indexBtn":
				currentPanel == 0 ? return : changePanel(0);
			case "bookmarkBtn":
				currentPanel == 1 ? return : changePanel(1);
			case "aboutBtn":
				currentPanel == 2 ? return : changePanel(2);
		}
	}
	
	/**
	 * Internal method to update the Dashboard
	 * to correct display enabled buttons and 
	 * its current panel.
	 */
	function updateStatus()
	{
		isOpened = !isOpened;
		
		if (isOpened)
		{
			panels[currentPanel].show();
			if (currentPanel < 3)
				buttons[currentPanel].setEnabled(false);
		}
	}
	
	/**
	 * Hides the current panel and show a
	 * new one.
	 * @param	id	The new panel index to
	 * 				displayed. 0 = index | 1 = bookmark
	 * 				2 = about | 3 = search | 4 = help
	 */
	public function changePanel(id:Int):Void
	{
		//TODO: Refactor the hell out of this method
		if (id != 3)
			searchBox.reset();
			
		panels[id].show();
		if (id < 3)
			buttons[id].setEnabled(false);
		
		if (id != currentPanel)
		{
			panels[currentPanel].hide();
			if (currentPanel < 3)
				buttons[currentPanel].setEnabled(true);
			currentPanel = id;
		}
	}
	
	/**
	 * Shows or hides the Dashboard instance
	 * according to its current state. Called
	 * from NavBar -> menuButton
	 * 
	 * TODO: Must toggle the button state in NavBar also.
	 * 		 (Or else the button visual state does not change).
	 */
	public function toggle():Void
	{
		if (isOpened)
			Actuate.tween(view, 0.15, { x:closedX, y:closedY } ).ease(Expo.easeOut).onComplete(updateStatus);
		else
			Actuate.tween(view, 0.15, { x:openedX, y:openedY } ).ease(Expo.easeOut).onComplete(updateStatus);
	}
	
	/**
	 * Shows or hides the HelpPanel instance
	 * according to its current state. Called
	 * from NavBar -> helpButton
	 */
	public function toggleHelp():Void
	{
		if (panels[4].isVisible) {
			panels[4].hide();
			toggle();
		} else {
			panels[4].show();
			changePanel(0);
			if (!isOpened)
				toggle();
		}
	}
	
	/**
	 * Callback from DataController.populateBookmars to insert
	 * all persisent bookmark data items into its panel. Not
	 * supposed to be called by the user, it gets called at
	 * the course initialization only.
	 * @param	pages	Array of pageData items from bookmarked pages
	 */
	public function populateBookmarks(pages:Array<PageData>):Void
	{
		var bkPanel:BookmarkPanel = cast(panels[1], BookmarkPanel);
		var numBookmarks:Int = pages.length;
		for (i in 0...numBookmarks)
		{
			bkPanel.insert(pages[i]);
		}
	}
	
	/**
	 * Callback from DataController.onBookmarkUpdated signal to
	 * update the current bookmark list. Not supposed to be called
	 * by the user, it gets called whenever a bookmark item is added
	 * or removed from the panel.
	 * @param	added			Whether the bookmark is added or removed
	 * @param	bookmarkData	The PageData to be bookmarked.
	 */
	public function updateBookmarks(added:Bool, bookmarkData:PageData):Void
	{
		cast(panels[1], BookmarkPanel).setBookmarks(added, bookmarkData);
	}
	
	/**
	 * Returns the panel with the corresponding id.
	 * @param	id	0 = index; 1 = bookmark; 2 = about; 3 = search; 4 = help;
	 * @return	An IPanel interface instance. You'll have to cast it to the
	 * 			corresponding Panel class in order to access specific methods
	 * 			and properties.
	 */
	public function getPanel(id:Int):IPanel
	{
		if (panels[id] == null)
		{
			trace("Panel at '" + id + "' is null!");
			return null;
		}
		
		return panels[id];
	}
	
	/**
	 * Returns the SearchBox instance
	 * @return	A reference the SearchBox instance 
	 */
	public function getSearchBox():SearchBox
	{
		if (searchBox == null)
		{
			trace("SearchBox is null!");
			return null;
		}
		
		return searchBox;
	}
	
	/**
	 * Called from an IPanel, this method serves to send
	 * page index information to the NavController which
	 * will trigger a page change.
	 * @param	page	A string containing the page source info.
	 */
	public function navigateTo(page:String):Void
	{
		trace( "Dashboard.navigateTo > page : " + page );
		onPanelButtonPressed.dispatch(page);
	}
}