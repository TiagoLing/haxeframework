package unbox.framework.view;

/**
 * Interface listing all common methods for different kinds of pages.
 * @author Tiago Ling Alexandre
 */
interface IPage {
	function transitionIn(data:Dynamic = null):Void;
	function transitionInComplete(data:Dynamic = null):Void;
	function transitionOut(data:Dynamic = null):Void;
	function transitionOutComplete(data:Dynamic = null):Void;
}