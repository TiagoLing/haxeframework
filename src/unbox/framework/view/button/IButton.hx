package unbox.framework.view.button;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;

/**
 * Contains common methods and properties
 * shared amongst all button types.
 * @author Tiago Ling Alexandre
 */
interface IButton
{
	function add(parent:DisplayObjectContainer):Void;
	function remove(parent:DisplayObjectContainer):Void;
	function addHandlers(?mouseDown:Dynamic->Void, ?mouseUp:Dynamic->Void):Void;
	function setEnabled(value:Bool):Void;
	function setVisible(value:Bool):Void;
	function flip(horizontal:Bool, vertical:Bool):Void;
	
	public var view(get_view, null):Sprite;
	public var name:String;
}