package unbox.framework.view.button;
//import openfl.events.Event;

/**
 * Custom button event used for communication
 * between button classes and their respective
 * views.
 * @author Tiago Ling Alexandre
 */
class ButtonEvent extends openfl.events.Event
{
	public static var ACTIVATE:String = "Activate";
	
	public var data:Dynamic;
	
	public function new(type:String, data:Dynamic, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
		this.data = data;
	}
	
	override public function clone():openfl.events.Event
	{
		return new ButtonEvent(type, bubbles, cancelable);
	}
}