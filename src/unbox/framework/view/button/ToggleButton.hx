package unbox.framework.view.button;
import msignal.Signal.Signal1;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;
import openfl.events.Event;

/**
 * Container class which acts as a toggle button
 * allowing for different graphics for each
 * toggle state.
 * @author Tiago Ling Alexandre
 */
class ToggleButton implements IButton
{
	//TODO: Encapsulate on & offViews for button to be recognized as a single entity
	//var _view:Sprite;
	
	var isToggled:Bool;
	var onBtn:GenericButton;
	var offBtn:GenericButton;
	
	var downSignal:Signal1<Dynamic>;
	var upSignal:Signal1<Dynamic>;
	
	var mouseDown:Dynamic->Void;
	var mouseUp:Dynamic->Void;
	
	var enabled:Bool;
	var visible:Bool;
	
	public var name:String;
	
	public function new(x:Float, y:Float, view:Sprite, ?name:String) 
	{
		this.view = view;
		
		view.addEventListener(ButtonEvent.ACTIVATE, onButtonActivated);
		offBtn = new GenericButton(0, 0, cast(this.view.getChildByName("offView"), Sprite), name);
		onBtn = new GenericButton(0, 0, cast(this.view.getChildByName("onView"), Sprite), name);
		
		onBtn.setVisible(false);
		
		isToggled = false;
		enabled = true;
		visible = true;
		this.name = name;
	}
	
	public function add(parent:DisplayObjectContainer):Void
	{
		//offBtn is always added on top - instances appear turned off as default
		view.addEventListener(Event.ADDED, onAdded);
		parent.addChild(view);
	}
	
	function onAdded(e:Event):Void 
	{
		view.removeEventListener(Event.ADDED, onAdded);
		onBtn.add(e.target);
		offBtn.add(e.target);
	}
	
	public function remove(parent:DisplayObjectContainer):Void
	{
		parent.removeChild(view);
	}
	
	public function addHandlers(?mouseDown:Dynamic->Void, ?mouseUp:Dynamic->Void):Void
	{
		if (mouseDown != null)
		{
			downSignal = new Signal1<Dynamic>();
			downSignal.add(mouseDown);
			
			offBtn.addHandlers(handleMouseDown);
			onBtn.addHandlers(handleMouseDown);
		}
		
		if (mouseUp != null) 
		{
			upSignal = new Signal1<Dynamic>();
			upSignal.add(mouseUp);
			
			offBtn.addHandlers(null, handleMouseUp);
			onBtn.addHandlers(null, handleMouseUp);
		}
	}
	
	function handleMouseDown(target:Dynamic):Void
	{
		if (isToggled)	//Turning it off
		{
			onBtn.setVisible(false);
			offBtn.setVisible(true);
			isToggled = false;
		}
		else			//Turning it on
		{
			onBtn.setVisible(true);
			offBtn.setVisible(false);
			isToggled = true;
		}
		
		downSignal.dispatch(target);
	}
	
	function handleMouseUp(target:Dynamic):Void
	{
		if (isToggled)	//Turning it off
		{
			onBtn.setVisible(false);
			offBtn.setVisible(true);
			isToggled = false;
		}
		else			//Turning it on
		{
			onBtn.setVisible(true);
			offBtn.setVisible(false);
			isToggled = true;
		}
		
		upSignal.dispatch(target);
	}
	
	function onButtonActivated(e:ButtonEvent):Void
	{
		trace("Button activated! Data : " + e.data);
		checkToggle();
	}
	
	public function checkToggle():Void
	{
		if (isToggled)
		{
			offBtn.activate();
		} else {
			onBtn.activate();
		}
	}
	
	/**
	 * Updates the view to correctly display
	 * the button status. Do not confuse it with
	 * the code inside handleMouseUp and Down.
	 */
	function updateView()
	{
		if (isToggled)
		{
			onBtn.setVisible(true);
			offBtn.setVisible(false);
		} else {
			onBtn.setVisible(false);
			offBtn.setVisible(true);
		}
	}
	
	public function setToggle(status:Bool):Void
	{
		isToggled = status;
		
		updateView();
	}
	
	public function flip(horizontal:Bool, vertical:Bool):Void {
		offBtn.flip(horizontal, vertical);
		onBtn.flip(horizontal, vertical);
	}
	
	public function setEnabled(value:Bool):Void {
		offBtn.setEnabled(value);
		onBtn.setEnabled(value);
	}
	
	public function setVisible(value:Bool):Void {
		view.visible = value;
	}
	
	function get_view():Sprite 
	{
		return view;
	}
	
	public var view(get_view, null):Sprite;
}