package unbox.framework.view.button;
import openfl.display.Sprite;
import openfl.events.MouseEvent;

/**
 * Button which uses CSS classes to differentiate states
 * instead of Sprites
 * @author Tiago Ling Alexandre
 */
class StyleButton extends GenericButton
{
	var classes:Array<String>;
	var currentState:String;
	public var isSelected:Bool;
	
	public function new(x:Float, y:Float, view:Sprite, ?name:String) 
	{
		super(x, y, view, name);
		isSelected = false;
	}
	
	override function init():Void
	{
		super.init();
	}
	
	public function setClasses(over:String, down:String, up:String, ?selected:String):Void
	{
		classes = new Array<String>();
		classes.push('<p class="' + over + '">');
		classes.push('<p class="' + down + '">');
		classes.push('<p class="' + up + '">');
		if (selected != null) classes.push('<p class="' + selected + '">');
		currentState = classes[2];
	}
	
	override function handleOver(e:MouseEvent):Void
	{
		super.handleOver(e);
		
		if (!isSelected)
		{
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[0]);
			currentState = classes[0];
		}
	}
	
	override function handleOut(e:MouseEvent):Void
	{
		super.handleOut(e);
			
		if (!isSelected)
		{
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[2]);
			currentState = classes[2];
		} else {
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[3]);
			currentState = classes[3];
		}
	}
	
	override function handleDown(?e:MouseEvent):Void
	{
		super.handleDown(e);
			
		_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[1]);
		currentState = classes[1];
	}
	
	override function handleUp(?e:MouseEvent):Void
	{
		super.handleUp(e);
			
		if (!isSelected)
		{
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[0]);
			currentState = classes[0];
		} else {
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[3]);
			currentState = classes[3];
		}
	}
	
	public function setSelected(value:Bool):Void
	{
		isSelected = value;
			
		if (!isSelected)
		{
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[2]);
			currentState = classes[2];
		}
	}
	
	override public function activate(?data:Dynamic):Void
	{
		//Here it is expected that the button has only one listener
		//either down or up.
		if (downSignal.numListeners > 0)
		{
			_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[1]);
			currentState = classes[1];
		
			data != null ? downSignal.dispatch(data) : downSignal.dispatch(this.view);
		} else if (upSignal.numListeners > 0)
		{
			
			if (!isSelected)
			{
				_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[0]);
				currentState = classes[0];
			} else {
				_label.htmlText = StringTools.replace(_label.htmlText, currentState, classes[3]);
				currentState = classes[3];
			}
			data != null ? upSignal.dispatch(data) : upSignal.dispatch(this.view);
		}
	}
}