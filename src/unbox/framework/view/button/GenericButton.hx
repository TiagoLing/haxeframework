package unbox.framework.view.button;
import msignal.Signal.Signal1;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.text.TextField;

/**
 * Basic button class, from which all other
 * button types must inherit.
 * @author Tiago Ling Alexandre
 */
class GenericButton implements IButton
{
	var _view:Sprite;
	var upState:DisplayObject;
	var downState:DisplayObject;
	var disabledState:DisplayObject;
	var overState:DisplayObject;
	var enabled:Bool;
	var isOver:Bool;
	
	var downSignal:Signal1<Dynamic>;
	var upSignal:Signal1<Dynamic>;
	
	public var name:String;
	
	var _x:Float;
	var _y:Float;
	var _visible:Bool;
	
	var _label:TextField;
	
	public function new(x:Float, y:Float, view:Sprite, ?name:String) 
	{
		this._view = view;
		_view.addEventListener(ButtonEvent.ACTIVATE, onButtonActivated);
		this._view.x = x;
		this._view.y = y;
		
		enabled = true;
		_visible = true;
		
		//TODO: Remove all stage instances (NavController) before setting this
		if (view.name.indexOf("instance") > -1) {
			this.name = _view.name = name;
		} else {
			this.name = name;
		}
		
		init();
	}
	
	function init():Void
	{
		upState = _view.getChildByName("up");
		downState = _view.getChildByName("down");
		disabledState = _view.getChildByName("disabled");
		overState = _view.getChildByName("over");
		
		_view.addEventListener(MouseEvent.ROLL_OVER, handleOver);
		_view.addEventListener(MouseEvent.ROLL_OUT, handleOut);
		_view.addEventListener(MouseEvent.MOUSE_DOWN, handleDown);
		_view.addEventListener(MouseEvent.MOUSE_UP, handleUp);
		
		upState.visible = true;
		overState.visible = false;
		disabledState.visible = false;
		downState.visible = false;
		
		_view.buttonMode = true;
		_view.mouseChildren = false;
		_view.tabChildren = false;
		//tabEnabled does not exist in Neko and CPP
		#if flash
		_view.tabEnabled = false;
		#end
	}
	
	public function add(parent:DisplayObjectContainer):Void
	{
		parent.addChild(_view);
	}
	
	public function remove(parent:DisplayObjectContainer):Void
	{
		_view.removeEventListener(MouseEvent.ROLL_OVER, handleOver);
		_view.removeEventListener(MouseEvent.ROLL_OUT, handleOut);
		_view.removeEventListener(MouseEvent.MOUSE_DOWN, handleDown);
		_view.removeEventListener(MouseEvent.MOUSE_UP, handleUp);
		upState = null;
		downState = null;
		disabledState = null;
		overState = null;
		parent.removeChild(_view);
	}
	
	public function addHandlers(?mouseDown:Dynamic->Void, ?mouseUp:Dynamic->Void):Void
	{
		if (mouseDown != null)
		{
			downSignal = new Signal1<Dynamic>();
			downSignal.add(mouseDown);
		}
		
		if (mouseUp != null) 
		{
			upSignal = new Signal1<Dynamic>();
			upSignal.add(mouseUp);
		}
	}
	
	public function setEnabled(value:Bool):Void
	{
		enabled = value;
		
		if (!enabled)
		{
			upState.visible = false;
			overState.visible = false;
			disabledState.visible = true;
			downState.visible = false;
			//buttonMode is commented out for
			//compatibility with Accessibility
			//_view.buttonMode = false;
			_view.useHandCursor = false;
			_view.mouseEnabled = false;
		}
		else 
		{
			//_view.buttonMode = true;
			_view.useHandCursor = true;
			upState.visible = true;
			overState.visible = false;
			disabledState.visible = false;
			downState.visible = false;
			_view.mouseEnabled = true;
		}
	}
	
	public function setVisible(value:Bool):Void
	{
		_visible = _view.visible = value;
		
		if (_visible)
		{
			if (enabled)
			{
				upState.visible = true;
				overState.visible = false;
				disabledState.visible = false;
				downState.visible = false;
			}
			else
			{
				upState.visible = false;
				overState.visible = false;
				disabledState.visible = true;
				downState.visible = false;
			}
		}
	}
	
	function handleUp(?e:MouseEvent):Void 
	{
		if (!enabled) return;
		if (isOver)
		{
			upState.visible = false;
			overState.visible = true;
			disabledState.visible = false;
			downState.visible = false;

		} else {
			upState.visible = true;
			overState.visible = false;
			disabledState.visible = false;
			downState.visible = false;
		}
		
		if (upSignal != null)
			upSignal.dispatch(this);
	}
	
	function handleDown(?e:MouseEvent):Void 
	{
		if (!enabled) return;
		upState.visible = false;
		overState.visible = false;
		disabledState.visible = false;
		downState.visible = true;
		
		if (downSignal != null)
			downSignal.dispatch(this);
	}
	
	function handleOut(e:MouseEvent):Void 
	{
		if (!enabled) return;
		upState.visible = true;
		overState.visible = false;
		disabledState.visible = false;
		downState.visible = false;
		
		if (isOver) isOver = false;
	}
	
	function handleOver(e:MouseEvent):Void 
	{
		if (!enabled) return;
		upState.visible = false;
		overState.visible = true;
		disabledState.visible = false;
		downState.visible = false;
		
		if (!isOver) isOver = true;
	}
	
	public function flip(horizontal:Bool,vertical:Bool):Void
	{
		if (horizontal == true) {
			_view.scaleX = -1;
			upState.x -= upState.width;
			overState.x -= overState.width;
			disabledState.x -= disabledState.width;
			downState.x -= downState.width;
		}
		
		if (vertical == true) {
			_view.scaleY = -1;
			upState.y -= upState.height;
			overState.y -= overState.height;
			disabledState.y -= disabledState.height;
			downState.y -= downState.height;
		}
	}
	
	//#### External Button Activation 			####
	//#### Maybe this belongs in another class	####
	//#### or Interface (Currently used in		####
	//#### accessibility implementation only)	####
	public function activate(?data:Dynamic):Void
	{
		//Here it is expected that the button has only one listener
		//either down or up.
		if (downSignal.numListeners > 0)
		{
			data != null ? downSignal.dispatch(data) : downSignal.dispatch(this.view);
		} else if (upSignal.numListeners > 0)
		{
			data != null ? upSignal.dispatch(data) : upSignal.dispatch(this.view);
		}
	}
	
	function onButtonActivated(e:ButtonEvent):Void
	{
		this.activate();
	}
	
	function get_x():Float 
	{
		return _x;
	}
	
	function set_x(value:Float):Float 
	{
		_view.x = value;
		return _x = value;
	}
	
	public var x(get_x, set_x):Float;
	
	function get_y():Float 
	{
		return _y;
	}
	
	function set_y(value:Float):Float 
	{
		_view.y = value;
		return _y = value;
	}
	
	public var y(get_y, set_y):Float;
	
	function get_view():Sprite 
	{
		return _view;
	}
	
	public var view(get_view, null):Sprite;
	
	function get_visible():Bool 
	{
		return _visible;
	}
	
	function set_visible(value:Bool):Bool 
	{
		return _visible = value;
	}
	
	public var visible(get_visible, set_visible):Bool;
	
	function get_label():TextField 
	{
		return _label;
	}
	
	function set_label(value:TextField):TextField 
	{
		_label = value;
		//tabEnabled does not exist in Neko and CPP
		#if flash
		_label.tabEnabled = false;
		#end
		return _label;
	}
	
	public var label(get_label, set_label):TextField;
}