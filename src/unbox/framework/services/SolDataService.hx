package unbox.framework.services;

import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import openfl.net.SharedObject;
import unbox.framework.model.vo.EbookData;
import unbox.framework.model.vo.ScormParams;

/**
 * ...
 * @author UNBOX® - http://www.unbox.com.br
 */  
class SolDataService implements IEbookDataService
{
    public var isAvailable(get_isAvailable, never) : Bool;
    public var onLoadError(get_onLoadError, never) : Signal1<String>;
    public var onSaveError(get_onSaveError, never) : Signal1<String>;
    public var onLoaded(get_onLoaded, never) : Signal1<EbookData>;
    public var onSaved(get_onSaved, never) : Signal0;
	var sol : SharedObject;
	// Interface methods
	var _isAvailable : Bool;
	var _onLoaded : Signal1<EbookData>;
	var _onSaved : Signal0;
	var _onLoadError : Signal1<String>;
	var _onSaveError : Signal1<String>;
	
	public function new()
    {
		init();
    }
	
	function init() : Void 
	{
		trace("SolDataService.init");
		sol = SharedObject.getLocal("EbookSolData");
		_isAvailable = true;
		_onLoaded = new Signal1<EbookData>();
		_onSaved = new Signal0();
		_onLoadError = new Signal1<String>();
		_onSaveError = new Signal1<String>();
    }
	
	/* INTERFACE com.unboxds.ebook.model.IEbookDataService */  
	public function load() : Void 
	{
		trace("SolDataService.load");
		var ebookData : EbookData = new EbookData();  
		// check if first time creating data
		if (getParam(ScormParams.PARAM_SUSPEND_DATA) != null) 
		{
			ebookData.comments = getParam(ScormParams.PARAM_COMMENTS);
			ebookData.credit = getParam(ScormParams.PARAM_CREDIT);
			ebookData.entry = getParam(ScormParams.PARAM_ENTRY);
			ebookData.launch_data = getParam(ScormParams.PARAM_LAUNCHA_DATA);
			ebookData.lesson_location = getParam(ScormParams.PARAM_LESSON_LOCATION);
			ebookData.lesson_mode = getParam(ScormParams.PARAM_LESSON_MODE);
			ebookData.lesson_status = getParam(ScormParams.PARAM_LESSON_STATUS);
			ebookData.scoreMax = Std.parseInt(getParam(ScormParams.PARAM_SCORE_MAX));
			ebookData.scoreMin = Std.parseInt(getParam(ScormParams.PARAM_SCORE_MIN));
			ebookData.scoreRaw = Std.parseInt(getParam(ScormParams.PARAM_SCORE_RAW));
			ebookData.student_id = getParam(ScormParams.PARAM_STUDENT_ID);
			ebookData.student_name = getParam(ScormParams.PARAM_STUDENT_NAME);
			ebookData.suspend_data = getParam(ScormParams.PARAM_SUSPEND_DATA);
			ebookData.total_time = getParam(ScormParams.PARAM_TOTAL_TIME);
        }
		_onLoaded.dispatch(ebookData);
    }
	
	/**
	 * Update and commit to the LMS the EbookData
	 */  
	public function save(data : EbookData) : Void 
	{
		update(data);
		sol.flush();
		#if flash
		sol.close();
		#end
		_onSaved.dispatch();
    } 
	
	/* INTERFACE com.unboxds.ebook.services.IEbookDataService */  
	function get_isAvailable() : Bool 
	{
		return _isAvailable;
    } 
	
	/**
	 * Update without commiting to the LMS
	 */  
	function update(data : EbookData) : Void 
	{
		setParam(ScormParams.PARAM_EXIT, data.exit);
		setParam(ScormParams.PARAM_LESSON_LOCATION, data.lesson_location);
		setParam(ScormParams.PARAM_LESSON_STATUS, data.lesson_status);
		setParam(ScormParams.PARAM_SCORE_MAX, Std.string(data.scoreMax));
		setParam(ScormParams.PARAM_SCORE_MIN, Std.string(data.scoreMin));
		setParam(ScormParams.PARAM_SCORE_RAW, Std.string(data.scoreRaw));
		setParam(ScormParams.PARAM_SESSION_TIME, data.session_time);
		setParam(ScormParams.PARAM_SUSPEND_DATA, data.suspend_data);
    }
	
	function setParam(param : String, value : String) : Void 
	{
		trace("setParam > param : " + param + ", value : " + value);
		Reflect.setField(sol.data, param, value);
    }
	
	function getParam(param : String) : String 
	{
		var ret : String = Reflect.field(sol.data, param);
		trace("SolDataService.getParam > param: " + param + " ret: " + ret);
		return ret;
    }
	
	function get_onLoadError() : Signal1<String> 
	{
		return _onLoadError;
    }
	
	function get_onSaveError() : Signal1<String> 
	{
		return _onSaveError;
    }
	
	function get_onLoaded() : Signal1<EbookData> 
	{
		return _onLoaded;
    }
	
	function get_onSaved() : Signal0
	{
		return _onSaved;
    }
}