package unbox.framework.services;

import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import unbox.framework.model.vo.EbookData;

/**
 * ...
 * @author UNBOX® - http://www.unbox.com.br
 */
interface IEbookDataService
{
    var isAvailable(get, never) : Bool;
    var onLoaded(get, never) : Signal1<EbookData>;
    var onSaved(get, never) : Signal0;
    var onLoadError(get, never) : Signal1<String>;
    var onSaveError(get, never) : Signal1<String>;

	function load() : Void;
	function save(data : EbookData) : Void;
}