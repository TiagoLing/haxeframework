package unbox.framework.services;
import msignal.Signal.Signal1;
import com.hybrid.ui.ToolTip;
import unbox.framework.model.vo.TooltipData;

/**
 * ...
 * @author Tiago Ling Alexandre @ UNBOX Learning Experience | 2009 - 2014
 */
class TooltipService
{
	#if flash
	var style:flash.text.StyleSheet;
	
	public function new(stylesheet:flash.text.StyleSheet) 
	{
		style = stylesheet;
	}
	#else
	public function new() 
	{
		
	}
	#end
	
	public function getTooltip(response:Signal1<ToolTip>, data:TooltipData):Void
	{
		var tt = new ToolTip();
		#if flash
		tt.stylesheet = style;
		#end
		tt.titleEmbed = true;
		tt.contentEmbed = true;
		
		tt.align = data.align;
		tt.autoSize = data.autoSize;
		tt.followCursor = data.followCursor;
		tt.hook = data.hook;
		tt.hookSize = data.hookSize;
		tt.cornerRadius = data.cornerRadius;
		tt.showDelay = data.showDelay;
		tt.hideDelay = data.hideDelay;
		tt.bgAlpha = data.bgAlpha;
		tt.tipWidth = data.width;
		tt.colors = data.colors;
		tt.padding = data.padding;
		
		response.dispatch(tt);
	}
	
}