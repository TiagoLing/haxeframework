package unbox.framework.services;
import msignal.Signal.Signal1;
import msignal.Signal.Signal2;
import openfl.Assets;
import openfl.display.Loader;
import openfl.display.LoaderInfo;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.HTTPStatusEvent;
import openfl.events.IOErrorEvent;
import openfl.events.ProgressEvent;
import openfl.events.SecurityErrorEvent;
import openfl.net.URLLoader;
import openfl.net.URLRequest;
import openfl.system.ApplicationDomain;
import openfl.system.LoaderContext;

/**
 * Handles all loading operations for the framework.
 * @author Tiago Ling Alexandre
 */
class LoadService
{
	var stringLoader:URLLoader;
	var viewLoader:Loader;
	#if flash
	var loaderContext:LoaderContext;
	var onCompleteAll:Signal2<Map<String, Dynamic>, ApplicationDomain>;
	#else
	var onCompleteAll:Signal1<Map<String, Dynamic>>;
	#end
	var loadedAssets:Map<String, Dynamic>;
	var loadQueue:Array<String>;
	var loadIds:Array<String>;
	var loadIndex:Int;
	var isLoading:Bool;
	var _loaderDomain:ApplicationDomain;
	
	//Experimental: setting the parent for adding and removing
	//the loader progress bar.
	public var parent:Sprite;
	var progressBar:Sprite;
	
	public function new() 
	{
		init();
	}
	
	function init():Void
	{
		stringLoader = new URLLoader();
		stringLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, loaderEvent);
		stringLoader.addEventListener(Event.COMPLETE, loaderEvent);
		stringLoader.addEventListener(IOErrorEvent.IO_ERROR, loaderEvent);
		stringLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, loaderEvent);
		
		viewLoader = new Loader();
		_loaderDomain = new ApplicationDomain(ApplicationDomain.currentDomain);
		#if flash
		loaderContext = new LoaderContext(false, _loaderDomain, null);
		#end
		viewLoader.contentLoaderInfo.addEventListener(HTTPStatusEvent.HTTP_STATUS, loaderEvent);
		viewLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, loaderEvent);
		viewLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, loaderEvent);
		viewLoader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, loaderEvent);
		
		#if flash
		onCompleteAll = new Signal2<Map<String, Dynamic>, ApplicationDomain>();
		#else
		onCompleteAll = new Signal1<Map<String, Dynamic>>();
		#end
		loadedAssets = new Map<String, Dynamic>();
		loadQueue = new Array<String>();
		loadIds = new Array<String>();
		isLoading = false;
		
	}
	
	/**
	 * Adds a new object object to the load queue. This does not
	 * start the loading process. Accepted types of files are
	 * xml, css, swf, jpeg, png and gif.
	 * @param	url	Path to the file
	 * @param	id	Optional id to easily identify the file later.
	 */
	public function add(url:String, ?id:String):Void
	{
		if (isLoading)
		{
			return;
		}
		
		loadQueue.push(url);
		if (id == null) {
			#if flash
			loadIds.push(url);
			#else
			trace("Adding swf to queue with id '" + id + "'");
			loadIds.push(id);
			#end
		} else {
			loadIds.push(id);
		}
	}
	
	/**
	 * Starts the loading operation for all files in the queue. The load
	 * operation happens in the same order as they are in the queue.
	 * @param	globalCallback	A callback which gets called when all files are loaded.
	 */
	#if flash
	public function start(globalCallback:Map<String, Dynamic>->?ApplicationDomain->Void):Void
	#else
	public function start(globalCallback:Map<String, Dynamic>->Void):Void
	#end
	{
		trace( "LoadService.start");
		
		//Testing progressBar
/*		progressBar = new Sprite();
		parent.addChild(progressBar);
		progressBar.graphics.beginFill(0xFF0000, 1);
		progressBar.graphics.drawRoundRect(parent.stage.stageWidth / 2 - 50, parent.stage.stageHeight / 2 - 25, 100, 50, 10, 10);
		progressBar.graphics.endFill();
		progressBar.addEventListener(ProgressEvent.PROGRESS, onLoadProgress);
		progressBar.scaleX = 0;*/
		
		//Testing renewing the domain and context for load operations
		_loaderDomain = new ApplicationDomain(ApplicationDomain.currentDomain);
		#if flash
		loaderContext = new LoaderContext(false, _loaderDomain, null);
		#end
		
		loadIndex = 0;
		isLoading = true;
		loadedAssets = null;
		loadedAssets = new Map<String, Dynamic>();
		onCompleteAll.addOnce(globalCallback);
		
		load();
	}
	
	function onLoadProgress(e:ProgressEvent):Void 
	{
		progressBar.scaleX = e.bytesTotal / e.bytesLoaded;
	}
	
	function load():Void
	{
		trace( "LoadService.load -> ID : " + loadQueue[loadIndex]);
		var fileExt:String = loadQueue[loadIndex].substr(loadQueue[loadIndex].length - 4, 4);
		
		//HTML5 Temp solution
		#if html5
		if (fileExt.indexOf(".") < 0)
		{
			Assets.loadLibrary(loadQueue[loadIndex], onLoadComplete);
		}
		#end
		
		switch (fileExt)
		{
			case ".xml", ".css":
				stringLoader.load(new URLRequest(loadQueue[loadIndex]));
			#if flash
			case ".swf":
				viewLoader.load(new URLRequest(loadQueue[loadIndex]), loaderContext);
			#end
			case ".jpeg", ".png", ".gif":
				trace("Image loading not implemented yet!");
			default:
				isLoading = false;
		}
	}
	
	function onLoadComplete(e:Dynamic):Void
	{
		trace( "LoadService.onLoadComplete > ID : " + loadQueue[loadIndex] );
		
		var data:Dynamic = null;
		
		#if flash
		if (Std.is(e.target, LoaderInfo))
			data = e.target.content;
		else 
			data = e.target.data;
		loadedAssets.set(loadIds[loadIndex], data);
		#else
		if (Std.is(e, AssetLibrary)) {
			data = e;
		} else {
			data = e.target.data;
		}
		loadedAssets.set(loadIds[loadIndex], data);
		#end
		
		//loadedAssets.set(loadIds[loadIndex], data);
		if (loadIndex < loadQueue.length - 1)
		{
			loadIndex++;
			load();
		}
		else
		{
			clear();
			#if flash
			onCompleteAll.dispatch(loadedAssets, loaderContext.applicationDomain);
			#else
			onCompleteAll.dispatch(loadedAssets);
			#end
		}
	}
	
	function clear():Void
	{
		loadQueue = null;
		loadIds = null;
		isLoading = false;
		loadQueue = new Array<String>();
		loadIds = new Array<String>();
	}
	
	#if !flash
	function loadLibComplete(lib:AssetLibrary)
	{
		trace( "LoadService.loadLibComplete > lib : " + lib );
		trace("Works!");
	}
	#end
	
	//TODO: Change to use class scope loader
	public function loadSingleString(url:String, onComplete:Dynamic->Void):Void
	{
		var request:URLRequest = new URLRequest(url);
		var loader:URLLoader = new URLLoader();
		loader.addEventListener(Event.COMPLETE, onComplete);
		loader.load(request);
	}
	
	//TODO: Change to use class scope loader
	public function loadSingleView(url:String, onComplete:Dynamic->Void):Void
	{
		var request:URLRequest = new URLRequest(url);
		var loader:Loader = new Loader();
		var context = new LoaderContext(false, _loaderDomain, null);
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
		#if flash
		loader.load(request, loaderContext);
		#end
	}
	
	function loaderEvent(e:Dynamic):Void
	{
		switch (e.type)
		{
			case HTTPStatusEvent.HTTP_STATUS:
				loadStatus(e.status);
			
			case Event.COMPLETE:
				onLoadComplete(e);

			case IOErrorEvent.IO_ERROR:
				loadError(e.toString());
			
			case SecurityErrorEvent.SECURITY_ERROR:
				loadSecurityError(e.toString());
		}
	}
	
	function loadStatus(status:Int):Void
	{
		//trace("Http Status : " + status);
	}
	
	function loadError(error:String):Void
	{
		trace("Error while loading asset with id '" + loadIds[loadIndex] + "' - Error details : " + error);
	}
	
	function loadSecurityError(error:String):Void
	{
		trace("Security Error while loading asset with id '" + loadIds[loadIndex] + "' : " + error);
	}
	
	#if flash
	function get_loaderDomain():ApplicationDomain 
	{
		return _loaderDomain;
	}
	
	public var loaderDomain(get_loaderDomain, null):ApplicationDomain;
	#end
	
}