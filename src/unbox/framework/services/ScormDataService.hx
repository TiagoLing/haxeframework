package unbox.framework.services;

import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import com.pipwerks.SCORM;
import unbox.framework.model.vo.EbookData;
import unbox.framework.model.vo.ScormParams;

/**
 * ...
 * @author UNBOX® - http://www.unbox.com.br
 */  
class ScormDataService implements IEbookDataService
{
    public var isAvailable(get_isAvailable, never) : Bool;
    public var onLoadError(get_onLoadError, never) : Signal1<String>;
    public var onSaveError(get_onSaveError, never) : Signal1<String>;
    public var onLoaded(get_onLoaded, never) : Signal1<EbookData>;
    public var onSaved(get_onSaved, never) : Signal0;
	
	var _isConnected : Bool;
	var scormAPI : SCORM;
	
	// Interface vars
	var _isAvailable : Bool;
	var _onLoaded : Signal1<EbookData>;
	var _onSaved : Signal0;
	var _onLoadError : Signal1<String>;
	var _onSaveError : Signal1<String>;
	
	public function new()
    {
		trace("ScormDataService.ScormDataService");
		init();
    }
	
	function init() : Void 
	{
		trace("ScormDataService.init");
		scormAPI = new SCORM();
		_isAvailable = _isConnected = scormAPI.connect();
		trace("ScormDataService.isAvailable: " + _isAvailable);
		_onLoaded = new Signal1<EbookData>();
		_onSaved = new Signal0();
		_onLoadError = new Signal1<String>();
		_onSaveError = new Signal1<String>();
    }
	
	function get_isAvailable() : Bool 
	{
		return _isAvailable;
    }
	
	public function load() : Void 
	{
		trace("ScormDataService.load");
		if (_isConnected) 
		{
			var ebookData : EbookData = new EbookData();
			ebookData.comments = getParam(ScormParams.PARAM_COMMENTS);
			ebookData.credit = getParam(ScormParams.PARAM_CREDIT);
			ebookData.entry = getParam(ScormParams.PARAM_ENTRY);
			ebookData.launch_data = getParam(ScormParams.PARAM_LAUNCHA_DATA);
			ebookData.lesson_location = getParam(ScormParams.PARAM_LESSON_LOCATION);
			ebookData.lesson_mode = getParam(ScormParams.PARAM_LESSON_MODE);
			ebookData.lesson_status = getParam(ScormParams.PARAM_LESSON_STATUS);
			ebookData.scoreMax = Std.parseInt(getParam(ScormParams.PARAM_SCORE_MAX));
			ebookData.scoreMin = Std.parseInt(getParam(ScormParams.PARAM_SCORE_MIN));
			ebookData.scoreRaw = Std.parseInt(getParam(ScormParams.PARAM_SCORE_RAW));
			ebookData.student_id = getParam(ScormParams.PARAM_STUDENT_ID);
			ebookData.student_name = getParam(ScormParams.PARAM_STUDENT_NAME);
			ebookData.suspend_data = getParam(ScormParams.PARAM_SUSPEND_DATA);
			ebookData.total_time = getParam(ScormParams.PARAM_TOTAL_TIME);
			_onLoaded.dispatch(ebookData);
        } else {
			_onLoadError.dispatch("ScormDataService Load Error. ScormDataService.connect: " + _isConnected);
        }
    } 
	
	/**
	 * Update and commit to the LMS the EbookData
	 */  
	public function save(data : EbookData) : Void 
	{
		update(data);
		var success : Bool = scormAPI.save();
		if (_isConnected && success) 
		{
			_onSaved.dispatch();
        } else {
			_onSaveError.dispatch("ScormDataService Save Error. ScormDataService.connect: " + _isConnected);
        }
    }
	
	/**
	 * Update without commiting to the LMS
	 */  
	function update(data : EbookData) : Void
	{
		//success = scorm.set(PARAM_COMMENTS, scormVO.comments);
		setParam(ScormParams.PARAM_EXIT, data.exit);
		setParam(ScormParams.PARAM_LESSON_LOCATION, data.lesson_location);
		setParam(ScormParams.PARAM_LESSON_STATUS, data.lesson_status);
		setParam(ScormParams.PARAM_SCORE_MAX, Std.string(data.scoreMax));
		setParam(ScormParams.PARAM_SCORE_MIN, Std.string(data.scoreMin));
		setParam(ScormParams.PARAM_SCORE_RAW, Std.string(data.scoreRaw));
		setParam(ScormParams.PARAM_SESSION_TIME, data.session_time);
		setParam(ScormParams.PARAM_SUSPEND_DATA, data.suspend_data);
    }
	
	function setParam(param : String, value : String) : Bool 
	{
		var ret : Bool = scormAPI.set(param, value);
		trace("function setParam > param: " + param + " value: " + value + " saved: " + ret);
		return ret;
    }
	
	function getParam(param : String) : String 
	{
		var ret : String = scormAPI.get(param);
		trace("ScormDataService.getParam > param: " + param + " ret: " + ret);
		return ret;
    }
	
	function get_onLoadError() : Signal1<String> 
	{
		return _onLoadError;
    }
	
	function get_onSaveError() : Signal1<String> 
	{
		return _onSaveError;
    }
	
	function get_onLoaded() : Signal1<EbookData> 
	{
		return _onLoaded;
    }
	
	function get_onSaved() : Signal0
	{
		return _onSaved;
    }
}