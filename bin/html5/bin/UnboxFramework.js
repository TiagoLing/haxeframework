(function ($hx_exports) { "use strict";
$hx_exports.openfl = {};
var $hxClasses = {},$estr = function() { return js.Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var ApplicationMain = function() { };
$hxClasses["ApplicationMain"] = ApplicationMain;
ApplicationMain.__name__ = ["ApplicationMain"];
ApplicationMain.preloader = null;
ApplicationMain.embed = $hx_exports.openfl.embed = function(elementName,width,height,background) {
	var element = null;
	if(elementName != null) element = window.document.getElementById(elementName);
	var color = null;
	if(background != null) {
		background = StringTools.replace(background,"#","");
		if(background.indexOf("0x") > -1) color = Std.parseInt(background); else color = Std.parseInt("0x" + background);
	}
	openfl.Lib.create(element,width,height,color);
	ApplicationMain.preloader = new NMEPreloader();
	openfl.Lib.current.addChild(ApplicationMain.preloader);
	ApplicationMain.preloader.onInit();
	var sounds = [];
	var id;
	if(ApplicationMain.total == 0) ApplicationMain.start(); else {
		var $it0 = ApplicationMain.urlLoaders.keys();
		while( $it0.hasNext() ) {
			var path = $it0.next();
			var urlLoader = ApplicationMain.urlLoaders.get(path);
			urlLoader.addEventListener("complete",ApplicationMain.loader_onComplete);
			urlLoader.load(new openfl.net.URLRequest(path));
		}
		var _g = 0;
		while(_g < sounds.length) {
			var soundName = sounds[_g];
			++_g;
			var sound = new openfl.media.Sound();
			sound.addEventListener(openfl.events.Event.COMPLETE,ApplicationMain.sound_onComplete);
			sound.addEventListener(openfl.events.IOErrorEvent.IO_ERROR,ApplicationMain.sound_onIOError);
			sound.load(new openfl.net.URLRequest(soundName + ".ogg"));
		}
	}
};
ApplicationMain.main = function() {
};
ApplicationMain.start = function() {
	ApplicationMain.preloader.addEventListener(openfl.events.Event.COMPLETE,ApplicationMain.preloader_onComplete);
	ApplicationMain.preloader.onLoaded();
};
ApplicationMain.image_onLoad = function(_) {
	ApplicationMain.assetsLoaded++;
	ApplicationMain.preloader.onUpdate(ApplicationMain.assetsLoaded,ApplicationMain.total);
	if(ApplicationMain.assetsLoaded == ApplicationMain.total) ApplicationMain.start();
};
ApplicationMain.loader_onComplete = function(event) {
	ApplicationMain.assetsLoaded++;
	ApplicationMain.preloader.onUpdate(ApplicationMain.assetsLoaded,ApplicationMain.total);
	if(ApplicationMain.assetsLoaded == ApplicationMain.total) ApplicationMain.start();
};
ApplicationMain.preloader_onComplete = function(event) {
	ApplicationMain.preloader.removeEventListener(openfl.events.Event.COMPLETE,ApplicationMain.preloader_onComplete);
	openfl.Lib.current.removeChild(ApplicationMain.preloader);
	ApplicationMain.preloader = null;
	var hasMain = false;
	var _g = 0;
	var _g1 = Type.getClassFields(unbox.framework.Main);
	while(_g < _g1.length) {
		var methodName = _g1[_g];
		++_g;
		if(methodName == "main") {
			hasMain = true;
			break;
		}
	}
	if(hasMain) Reflect.callMethod(unbox.framework.Main,Reflect.field(unbox.framework.Main,"main"),[]); else {
		var instance = Type.createInstance(DocumentClass,[]);
		if(js.Boot.__instanceof(instance,openfl.display.DisplayObject)) openfl.Lib.current.addChild(instance); else {
			haxe.Log.trace("Error: No entry point found",{ fileName : "ApplicationMain.hx", lineNumber : 180, className : "ApplicationMain", methodName : "preloader_onComplete"});
			haxe.Log.trace("If you are using DCE with a static main, you may need to @:keep the function",{ fileName : "ApplicationMain.hx", lineNumber : 181, className : "ApplicationMain", methodName : "preloader_onComplete"});
		}
	}
};
ApplicationMain.sound_onComplete = function(event) {
	ApplicationMain.assetsLoaded++;
	ApplicationMain.preloader.onUpdate(ApplicationMain.assetsLoaded,ApplicationMain.total);
	if(ApplicationMain.assetsLoaded == ApplicationMain.total) ApplicationMain.start();
};
ApplicationMain.sound_onIOError = function(event) {
	ApplicationMain.assetsLoaded++;
	ApplicationMain.preloader.onUpdate(ApplicationMain.assetsLoaded,ApplicationMain.total);
	if(ApplicationMain.assetsLoaded == ApplicationMain.total) ApplicationMain.start();
};
var openfl = {};
openfl.events = {};
openfl.events.IEventDispatcher = function() { };
$hxClasses["openfl.events.IEventDispatcher"] = openfl.events.IEventDispatcher;
openfl.events.IEventDispatcher.__name__ = ["openfl","events","IEventDispatcher"];
openfl.events.IEventDispatcher.prototype = {
	__class__: openfl.events.IEventDispatcher
};
openfl.events.EventDispatcher = function(target) {
	if(target != null) this.__targetDispatcher = target;
};
$hxClasses["openfl.events.EventDispatcher"] = openfl.events.EventDispatcher;
openfl.events.EventDispatcher.__name__ = ["openfl","events","EventDispatcher"];
openfl.events.EventDispatcher.__interfaces__ = [openfl.events.IEventDispatcher];
openfl.events.EventDispatcher.__sortByPriority = function(l1,l2) {
	if(l1.priority == l2.priority) return 0; else if(l1.priority > l2.priority) return -1; else return 1;
};
openfl.events.EventDispatcher.prototype = {
	addEventListener: function(type,listener,useCapture,priority,useWeakReference) {
		if(useWeakReference == null) useWeakReference = false;
		if(priority == null) priority = 0;
		if(useCapture == null) useCapture = false;
		if(this.__eventMap == null) this.__eventMap = new haxe.ds.StringMap();
		if(!this.__eventMap.exists(type)) {
			var list = new Array();
			list.push(new openfl.events._EventDispatcher.Listener(listener,useCapture,priority));
			this.__eventMap.set(type,list);
		} else {
			var list1 = this.__eventMap.get(type);
			list1.push(new openfl.events._EventDispatcher.Listener(listener,useCapture,priority));
			list1.sort(openfl.events.EventDispatcher.__sortByPriority);
		}
	}
	,dispatchEvent: function(event) {
		if(this.__eventMap == null || event == null) return false;
		var list = this.__eventMap.get(event.type);
		if(list == null) return false;
		if(event.target == null) {
			if(this.__targetDispatcher != null) event.target = this.__targetDispatcher; else event.target = this;
		}
		event.currentTarget = this;
		var capture = event.eventPhase == openfl.events.EventPhase.CAPTURING_PHASE;
		var index = 0;
		var listener;
		while(index < list.length) {
			listener = list[index];
			if(listener.useCapture == capture) {
				listener.callback(event);
				if(event.__isCancelledNow) return true;
			}
			if(listener == list[index]) index++;
		}
		return true;
	}
	,hasEventListener: function(type) {
		if(this.__eventMap == null) return false;
		return this.__eventMap.exists(type);
	}
	,removeEventListener: function(type,listener,capture) {
		if(capture == null) capture = false;
		if(this.__eventMap == null) return;
		var list = this.__eventMap.get(type);
		if(list == null) return;
		var _g1 = 0;
		var _g = list.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(list[i].match(listener,capture)) {
				list.splice(i,1);
				break;
			}
		}
		if(list.length == 0) this.__eventMap.remove(type);
		if(!this.__eventMap.iterator().hasNext()) this.__eventMap = null;
	}
	,toString: function() {
		var full = Type.getClassName(Type.getClass(this));
		var short = full.split(".").pop();
		return "[object " + short + "]";
	}
	,willTrigger: function(type) {
		return this.hasEventListener(type);
	}
	,__class__: openfl.events.EventDispatcher
};
openfl.display = {};
openfl.display.IBitmapDrawable = function() { };
$hxClasses["openfl.display.IBitmapDrawable"] = openfl.display.IBitmapDrawable;
openfl.display.IBitmapDrawable.__name__ = ["openfl","display","IBitmapDrawable"];
openfl.display.IBitmapDrawable.prototype = {
	__class__: openfl.display.IBitmapDrawable
};
openfl.display.DisplayObject = function() {
	openfl.events.EventDispatcher.call(this);
	this.set_alpha(1);
	this.set_rotation(0);
	this.set_scaleX(1);
	this.set_scaleY(1);
	this.set_visible(true);
	this.set_x(0);
	this.set_y(0);
	this.__worldAlpha = 1;
	this.__worldTransform = new openfl.geom.Matrix();
	this.__worldVisible = true;
	this.set_name("instance" + ++openfl.display.DisplayObject.__instanceCount);
};
$hxClasses["openfl.display.DisplayObject"] = openfl.display.DisplayObject;
openfl.display.DisplayObject.__name__ = ["openfl","display","DisplayObject"];
openfl.display.DisplayObject.__interfaces__ = [openfl.display.IBitmapDrawable];
openfl.display.DisplayObject.__super__ = openfl.events.EventDispatcher;
openfl.display.DisplayObject.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	dispatchEvent: function(event) {
		var result = openfl.events.EventDispatcher.prototype.dispatchEvent.call(this,event);
		if(event.__isCancelled) return true;
		if(event.bubbles && this.parent != null && this.parent != this) {
			event.eventPhase = openfl.events.EventPhase.BUBBLING_PHASE;
			this.parent.dispatchEvent(event);
		}
		return result;
	}
	,getBounds: function(targetCoordinateSpace) {
		var matrix = this.__getTransform();
		if(targetCoordinateSpace != null) {
			matrix = new openfl.geom.Matrix(matrix.a,matrix.b,matrix.c,matrix.d,matrix.tx,matrix.ty);
			matrix.concat(targetCoordinateSpace.__worldTransform.clone().invert());
		}
		var bounds = new openfl.geom.Rectangle();
		this.__getBounds(bounds,matrix);
		return bounds;
	}
	,getRect: function(targetCoordinateSpace) {
		return this.getBounds(targetCoordinateSpace);
	}
	,globalToLocal: function(pos) {
		return this.__getTransform().clone().invert().transformPoint(pos);
	}
	,hitTestObject: function(obj) {
		return false;
	}
	,hitTestPoint: function(x,y,shapeFlag) {
		if(shapeFlag == null) shapeFlag = false;
		return false;
	}
	,localToGlobal: function(point) {
		return this.__getTransform().transformPoint(point);
	}
	,__applyStyle: function(renderSession,setTransform,setAlpha,setClip) {
		if(setTransform && this.__worldTransformChanged) this.__style.setProperty(renderSession.transformProperty,this.__worldTransform.to3DString(renderSession.roundPixels),null);
		if(this.__worldZ != ++renderSession.z) {
			this.__worldZ = renderSession.z;
			this.__style.setProperty("z-index","" + this.__worldZ,null);
		}
		if(setAlpha && this.__worldAlphaChanged) {
			if(this.__worldAlpha < 1) this.__style.setProperty("opacity","" + this.__worldAlpha,null); else this.__style.removeProperty("opacity");
		}
		if(setClip && this.__worldClipChanged) {
			if(this.__worldClip == null) this.__style.removeProperty("clip"); else {
				var clip = this.__worldClip.transform(this.__worldTransform.clone().invert());
				this.__style.setProperty("clip","rect(" + clip.y + "px, " + clip.get_right() + "px, " + clip.get_bottom() + "px, " + clip.x + "px)",null);
			}
		}
	}
	,__broadcast: function(event,notifyChilden) {
		if(this.__eventMap != null && this.hasEventListener(event.type)) {
			var result = openfl.events.EventDispatcher.prototype.dispatchEvent.call(this,event);
			if(event.__isCancelled) return true;
			return result;
		}
		return false;
	}
	,__getBounds: function(rect,matrix) {
	}
	,__getInteractive: function(stack) {
	}
	,__getLocalBounds: function(rect) {
		this.__getTransform();
		this.__getBounds(rect,new openfl.geom.Matrix());
	}
	,__getTransform: function() {
		if(openfl.display.DisplayObject.__worldTransformDirty > 0) {
			var list = [];
			var current = this;
			var transformDirty = this.__transformDirty;
			while(current.parent != null) {
				list.push(current);
				current = current.parent;
				if(current.__transformDirty) transformDirty = true;
			}
			if(transformDirty) {
				var i = list.length;
				while(--i >= 0) list[i].__update(true,false);
			}
		}
		return this.__worldTransform;
	}
	,__hitTest: function(x,y,shapeFlag,stack,interactiveOnly) {
		return false;
	}
	,__initializeElement: function(element,renderSession) {
		this.__style = element.style;
		this.__style.setProperty("position","absolute",null);
		this.__style.setProperty("top","0",null);
		this.__style.setProperty("left","0",null);
		this.__style.setProperty(renderSession.transformOriginProperty,"0 0 0",null);
		renderSession.element.appendChild(element);
		this.__worldAlphaChanged = true;
		this.__worldClipChanged = true;
		this.__worldTransformChanged = true;
		this.__worldVisibleChanged = true;
		this.__worldZ = -1;
	}
	,__renderCanvas: function(renderSession) {
	}
	,__renderDOM: function(renderSession) {
	}
	,__renderGL: function(renderSession) {
	}
	,__renderMask: function(renderSession) {
	}
	,__setStageReference: function(stage) {
		if(this.stage != stage) {
			if(this.stage != null) this.dispatchEvent(new openfl.events.Event(openfl.events.Event.REMOVED_FROM_STAGE,false,false));
			this.stage = stage;
			if(stage != null) this.dispatchEvent(new openfl.events.Event(openfl.events.Event.ADDED_TO_STAGE,false,false));
		}
	}
	,__setRenderDirty: function() {
		if(!this.__renderDirty) {
			this.__renderDirty = true;
			openfl.display.DisplayObject.__worldRenderDirty++;
		}
	}
	,__setTransformDirty: function() {
		if(!this.__transformDirty) {
			this.__transformDirty = true;
			openfl.display.DisplayObject.__worldTransformDirty++;
		}
	}
	,__update: function(transformOnly,updateChildren) {
		this.__renderable = this.get_visible() && this.get_scaleX() != 0 && this.get_scaleY() != 0 && !this.__isMask;
		if(this.get_rotation() != this.__rotationCache) {
			this.__rotationCache = this.get_rotation();
			var radians = this.get_rotation() * (Math.PI / 180);
			this.__rotationSine = Math.sin(radians);
			this.__rotationCosine = Math.cos(radians);
		}
		if(this.parent != null) {
			var parentTransform = this.parent.__worldTransform;
			var a00 = this.__rotationCosine * this.get_scaleX();
			var a01 = this.__rotationSine * this.get_scaleX();
			var a10 = -this.__rotationSine * this.get_scaleY();
			var a11 = this.__rotationCosine * this.get_scaleY();
			var b00 = parentTransform.a;
			var b01 = parentTransform.b;
			var b10 = parentTransform.c;
			var b11 = parentTransform.d;
			this.__worldTransform.a = a00 * b00 + a01 * b10;
			this.__worldTransform.b = a00 * b01 + a01 * b11;
			this.__worldTransform.c = a10 * b00 + a11 * b10;
			this.__worldTransform.d = a10 * b01 + a11 * b11;
			if(this.get_scrollRect() == null) {
				this.__worldTransform.tx = this.get_x() * b00 + this.get_y() * b10 + parentTransform.tx;
				this.__worldTransform.ty = this.get_x() * b01 + this.get_y() * b11 + parentTransform.ty;
			} else {
				this.__worldTransform.tx = (this.get_x() - this.get_scrollRect().x) * b00 + (this.get_y() - this.get_scrollRect().y) * b10 + parentTransform.tx;
				this.__worldTransform.ty = (this.get_x() - this.get_scrollRect().x) * b01 + (this.get_y() - this.get_scrollRect().y) * b11 + parentTransform.ty;
			}
		} else {
			this.__worldTransform.a = this.__rotationCosine * this.get_scaleX();
			this.__worldTransform.c = -this.__rotationSine * this.get_scaleY();
			this.__worldTransform.b = this.__rotationSine * this.get_scaleX();
			this.__worldTransform.d = this.__rotationCosine * this.get_scaleY();
			if(this.get_scrollRect() == null) {
				this.__worldTransform.tx = this.get_x();
				this.__worldTransform.ty = this.get_y();
			} else {
				this.__worldTransform.tx = this.get_y() - this.get_scrollRect().x;
				this.__worldTransform.ty = this.get_y() - this.get_scrollRect().y;
			}
		}
		if(updateChildren && this.__transformDirty) {
			this.__transformDirty = false;
			openfl.display.DisplayObject.__worldTransformDirty--;
		}
		if(!transformOnly) {
			this.__worldTransformChanged = !this.__worldTransform.equals(this.__worldTransformCache);
			this.__worldTransformCache = this.__worldTransform.clone();
			var worldClip = null;
			if(this.parent != null) {
				var worldVisible = this.parent.__worldVisible && this.get_visible();
				this.__worldVisibleChanged = this.__worldVisible != worldVisible;
				this.__worldVisible = worldVisible;
				var worldAlpha = this.get_alpha() * this.parent.__worldAlpha;
				this.__worldAlphaChanged = this.__worldAlpha != worldAlpha;
				this.__worldAlpha = worldAlpha;
				if(this.parent.__worldClip != null) worldClip = this.parent.__worldClip.clone();
				if(this.get_scrollRect() != null) {
					var bounds = this.get_scrollRect().clone();
					bounds = bounds.transform(this.__worldTransform);
					if(worldClip != null) bounds.__contract(worldClip.x - this.get_scrollRect().x,worldClip.y - this.get_scrollRect().y,worldClip.width,worldClip.height);
					worldClip = bounds;
				}
			} else {
				this.__worldAlpha = this.get_alpha();
				this.__worldVisibleChanged = this.__worldVisible != this.get_visible();
				this.__worldVisible = this.get_visible();
				this.__worldAlphaChanged = this.__worldAlpha != this.get_alpha();
				if(this.get_scrollRect() != null) worldClip = this.get_scrollRect().clone().transform(this.__worldTransform);
			}
			this.__worldClipChanged = worldClip == null && this.__worldClip != null || worldClip != null && !worldClip.equals(this.__worldClip);
			this.__worldClip = worldClip;
			if(updateChildren && this.__renderDirty) this.__renderDirty = false;
		}
	}
	,__updateChildren: function(transformOnly) {
		this.__renderable = this.get_visible() && this.get_scaleX() != 0 && this.get_scaleY() != 0 && !this.__isMask;
		if(!this.__renderable && !this.__isMask) return;
		this.__worldAlpha = this.get_alpha();
		if(this.__transformDirty) {
			this.__transformDirty = false;
			openfl.display.DisplayObject.__worldTransformDirty--;
		}
	}
	,get_alpha: function() {
		return this.__alpha;
	}
	,set_alpha: function(value) {
		if(value != this.__alpha) {
			if(!this.__renderDirty) {
				this.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
		}
		return this.__alpha = value;
	}
	,get_filters: function() {
		if(this.__filters == null) return new Array(); else return this.__filters.slice();
	}
	,set_filters: function(value) {
		return value;
	}
	,get_height: function() {
		var bounds = new openfl.geom.Rectangle();
		this.__getTransform();
		this.__getBounds(bounds,new openfl.geom.Matrix());
		return bounds.height * this.get_scaleY();
	}
	,set_height: function(value) {
		var bounds = new openfl.geom.Rectangle();
		this.__getTransform();
		this.__getBounds(bounds,new openfl.geom.Matrix());
		if(value != bounds.height) this.set_scaleY(value / bounds.height); else this.set_scaleY(1);
		return value;
	}
	,get_mask: function() {
		return this.__mask;
	}
	,set_mask: function(value) {
		if(value != this.__mask) {
			if(!this.__renderDirty) {
				this.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
		}
		if(this.__mask != null) this.__mask.__isMask = false;
		if(value != null) value.__isMask = true;
		return this.__mask = value;
	}
	,get_mouseX: function() {
		if(this.stage != null) return this.globalToLocal(new openfl.geom.Point(this.stage.__mouseX,0)).x;
		return 0;
	}
	,get_mouseY: function() {
		if(this.stage != null) return this.globalToLocal(new openfl.geom.Point(0,this.stage.__mouseY)).y;
		return 0;
	}
	,get_name: function() {
		return this.__name;
	}
	,set_name: function(value) {
		return this.__name = value;
	}
	,get_root: function() {
		if(this.stage != null) return openfl.Lib.current;
		return null;
	}
	,get_rotation: function() {
		return this.__rotation;
	}
	,set_rotation: function(value) {
		if(value != this.__rotation) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
		}
		return this.__rotation = value;
	}
	,get_scaleX: function() {
		return this.__scaleX;
	}
	,set_scaleX: function(value) {
		if(value != this.__scaleX) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
		}
		return this.__scaleX = value;
	}
	,get_scaleY: function() {
		return this.__scaleY;
	}
	,set_scaleY: function(value) {
		if(this.__scaleY != value) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
		}
		return this.__scaleY = value;
	}
	,get_scrollRect: function() {
		return this.__scrollRect;
	}
	,set_scrollRect: function(value) {
		if(value != this.__scrollRect) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
			if(!this.__renderDirty) {
				this.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
		}
		return this.__scrollRect = value;
	}
	,get_transform: function() {
		if(this.__transform == null) this.__transform = new openfl.geom.Transform(this);
		return this.__transform;
	}
	,set_transform: function(value) {
		if(value == null) throw new openfl.errors.TypeError("Parameter transform must be non-null.");
		if(this.__transform == null) this.__transform = new openfl.geom.Transform(this);
		if(!this.__transformDirty) {
			this.__transformDirty = true;
			openfl.display.DisplayObject.__worldTransformDirty++;
		}
		this.__transform.set_matrix(value.get_matrix().clone());
		this.__transform.colorTransform = new openfl.geom.ColorTransform(value.colorTransform.redMultiplier,value.colorTransform.greenMultiplier,value.colorTransform.blueMultiplier,value.colorTransform.alphaMultiplier,value.colorTransform.redOffset,value.colorTransform.greenOffset,value.colorTransform.blueOffset,value.colorTransform.alphaOffset);
		return this.__transform;
	}
	,get_visible: function() {
		return this.__visible;
	}
	,set_visible: function(value) {
		if(value != this.__visible) {
			if(!this.__renderDirty) {
				this.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
		}
		return this.__visible = value;
	}
	,get_width: function() {
		var bounds = new openfl.geom.Rectangle();
		this.__getTransform();
		this.__getBounds(bounds,new openfl.geom.Matrix());
		return bounds.width * this.get_scaleX();
	}
	,set_width: function(value) {
		var bounds = new openfl.geom.Rectangle();
		this.__getTransform();
		this.__getBounds(bounds,new openfl.geom.Matrix());
		if(value != bounds.width) this.set_scaleX(value / bounds.width); else this.set_scaleX(1);
		return value;
	}
	,get_x: function() {
		return this.__x;
	}
	,set_x: function(value) {
		if(value != this.__x) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
		}
		return this.__x = value;
	}
	,get_y: function() {
		return this.__y;
	}
	,set_y: function(value) {
		if(value != this.__y) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
		}
		return this.__y = value;
	}
	,__class__: openfl.display.DisplayObject
	,__properties__: {set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_width:"set_width",get_width:"get_width",set_visible:"set_visible",get_visible:"get_visible",set_transform:"set_transform",get_transform:"get_transform",set_scrollRect:"set_scrollRect",get_scrollRect:"get_scrollRect",set_scaleY:"set_scaleY",get_scaleY:"get_scaleY",set_scaleX:"set_scaleX",get_scaleX:"get_scaleX",set_rotation:"set_rotation",get_rotation:"get_rotation",get_root:"get_root",set_name:"set_name",get_name:"get_name",get_mouseY:"get_mouseY",get_mouseX:"get_mouseX",set_mask:"set_mask",get_mask:"get_mask",set_height:"set_height",get_height:"get_height",set_filters:"set_filters",get_filters:"get_filters",set_alpha:"set_alpha",get_alpha:"get_alpha"}
});
openfl.display.InteractiveObject = function() {
	openfl.display.DisplayObject.call(this);
	this.doubleClickEnabled = false;
	this.mouseEnabled = true;
	this.needsSoftKeyboard = false;
	this.tabEnabled = true;
	this.tabIndex = -1;
};
$hxClasses["openfl.display.InteractiveObject"] = openfl.display.InteractiveObject;
openfl.display.InteractiveObject.__name__ = ["openfl","display","InteractiveObject"];
openfl.display.InteractiveObject.__super__ = openfl.display.DisplayObject;
openfl.display.InteractiveObject.prototype = $extend(openfl.display.DisplayObject.prototype,{
	requestSoftKeyboard: function() {
		openfl.Lib.notImplemented("InteractiveObject.requestSoftKeyboard");
		return false;
	}
	,__getInteractive: function(stack) {
		stack.push(this);
		if(this.parent != null) this.parent.__getInteractive(stack);
	}
	,__class__: openfl.display.InteractiveObject
});
openfl.display.DisplayObjectContainer = function() {
	openfl.display.InteractiveObject.call(this);
	this.mouseChildren = true;
	this.__children = new Array();
	this.__removedChildren = new Array();
};
$hxClasses["openfl.display.DisplayObjectContainer"] = openfl.display.DisplayObjectContainer;
openfl.display.DisplayObjectContainer.__name__ = ["openfl","display","DisplayObjectContainer"];
openfl.display.DisplayObjectContainer.__super__ = openfl.display.InteractiveObject;
openfl.display.DisplayObjectContainer.prototype = $extend(openfl.display.InteractiveObject.prototype,{
	addChild: function(child) {
		if(child != null) {
			if(child.parent != null) child.parent.removeChild(child);
			this.__children.push(child);
			child.parent = this;
			if(this.stage != null) child.__setStageReference(this.stage);
			if(!child.__transformDirty) {
				child.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
			if(!child.__renderDirty) {
				child.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
			child.dispatchEvent(new openfl.events.Event(openfl.events.Event.ADDED,true));
		}
		return child;
	}
	,addChildAt: function(child,index) {
		if(index > this.__children.length || index < 0) throw "Invalid index position " + index;
		if(child.parent == this) HxOverrides.remove(this.__children,child); else {
			if(child.parent != null) child.parent.removeChild(child);
			child.parent = this;
			if(this.stage != null) child.__setStageReference(this.stage);
			if(!child.__transformDirty) {
				child.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
			if(!child.__renderDirty) {
				child.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
			child.dispatchEvent(new openfl.events.Event(openfl.events.Event.ADDED,true));
		}
		this.__children.splice(index,0,child);
		return child;
	}
	,areInaccessibleObjectsUnderPoint: function(point) {
		return false;
	}
	,contains: function(child) {
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			if(i == child) return true;
		}
		return false;
	}
	,getChildAt: function(index) {
		if(index >= 0 && index < this.__children.length) return this.__children[index];
		return null;
	}
	,getChildByName: function(name) {
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(child.get_name() == name) return child;
		}
		return null;
	}
	,getChildIndex: function(child) {
		var _g1 = 0;
		var _g = this.__children.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.__children[i] == child) return i;
		}
		return -1;
	}
	,getObjectsUnderPoint: function(point) {
		point = this.localToGlobal(point);
		var stack = new Array();
		this.__hitTest(point.x,point.y,false,stack,false);
		stack.shift();
		return stack;
	}
	,removeChild: function(child) {
		if(child != null && child.parent == this) {
			if(this.stage != null) child.__setStageReference(null);
			child.parent = null;
			HxOverrides.remove(this.__children,child);
			this.__removedChildren.push(child);
			if(!child.__transformDirty) {
				child.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
			if(!child.__renderDirty) {
				child.__renderDirty = true;
				openfl.display.DisplayObject.__worldRenderDirty++;
			}
			child.dispatchEvent(new openfl.events.Event(openfl.events.Event.REMOVED,true));
		}
		return child;
	}
	,removeChildAt: function(index) {
		if(index >= 0 && index < this.__children.length) return this.removeChild(this.__children[index]);
		return null;
	}
	,removeChildren: function(beginIndex,endIndex) {
		if(endIndex == null) endIndex = 2147483647;
		if(beginIndex == null) beginIndex = 0;
		if(endIndex == 2147483647) {
			endIndex = this.__children.length - 1;
			if(endIndex < 0) return;
		}
		if(beginIndex > this.__children.length - 1) return; else if(endIndex < beginIndex || beginIndex < 0 || endIndex > this.__children.length) throw new openfl.errors.RangeError("The supplied index is out of bounds.");
		var numRemovals = endIndex - beginIndex;
		while(numRemovals >= 0) {
			this.removeChildAt(beginIndex);
			numRemovals--;
		}
	}
	,setChildIndex: function(child,index) {
		if(index >= 0 && index <= this.__children.length && child.parent == this) {
			HxOverrides.remove(this.__children,child);
			this.__children.splice(index,0,child);
		}
	}
	,swapChildren: function(child1,child2) {
		if(child1.parent == this && child2.parent == this) {
			var index1 = -1;
			var index2 = -1;
			var _g1 = 0;
			var _g = this.__children.length;
			while(_g1 < _g) {
				var i = _g1++;
				if(this.__children[i] == child1) index1 = i; else if(this.__children[i] == child2) index2 = i;
			}
			this.__children[index1] = child2;
			this.__children[index2] = child1;
		}
	}
	,swapChildrenAt: function(child1,child2) {
		var swap = this.__children[child1];
		this.__children[child1] = this.__children[child2];
		this.__children[child2] = swap;
		swap = null;
	}
	,__broadcast: function(event,notifyChilden) {
		if(event.target == null) event.target = this;
		if(notifyChilden) {
			var _g = 0;
			var _g1 = this.__children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.__broadcast(event,true);
				if(event.__isCancelled) return true;
			}
		}
		return openfl.display.InteractiveObject.prototype.__broadcast.call(this,event,notifyChilden);
	}
	,__getBounds: function(rect,matrix) {
		if(this.__children.length == 0) return;
		var matrixCache = null;
		if(matrix != null) {
			matrixCache = this.__worldTransform;
			this.__worldTransform = matrix;
			this.__updateChildren(true);
		}
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			if(!child.__renderable) continue;
			child.__getBounds(rect,null);
		}
		if(matrix != null) {
			this.__worldTransform = matrixCache;
			this.__updateChildren(true);
		}
	}
	,__hitTest: function(x,y,shapeFlag,stack,interactiveOnly) {
		if(!this.get_visible() || interactiveOnly && !this.mouseEnabled) return false;
		var i = this.__children.length;
		if(interactiveOnly && (stack == null || !this.mouseChildren)) {
			while(--i >= 0) if(this.__children[i].__hitTest(x,y,shapeFlag,null,interactiveOnly)) {
				if(stack != null) stack.push(this);
				return true;
			}
		} else if(stack != null) {
			var length = stack.length;
			while(--i >= 0) if(this.__children[i].__hitTest(x,y,shapeFlag,stack,interactiveOnly)) {
				stack.splice(length,0,this);
				return true;
			}
		}
		return false;
	}
	,__renderCanvas: function(renderSession) {
		if(!this.__renderable || this.__worldAlpha <= 0) return;
		if(this.get_scrollRect() != null) renderSession.maskManager.pushRect(this.get_scrollRect(),this.__worldTransform);
		if(this.__mask != null) renderSession.maskManager.pushMask(this.__mask);
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.__renderCanvas(renderSession);
		}
		this.__removedChildren = [];
		if(this.__mask != null) renderSession.maskManager.popMask();
		if(this.get_scrollRect() != null) renderSession.maskManager.popMask();
	}
	,__renderDOM: function(renderSession) {
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.__renderDOM(renderSession);
		}
		var _g2 = 0;
		var _g11 = this.__removedChildren;
		while(_g2 < _g11.length) {
			var orphan = _g11[_g2];
			++_g2;
			if(orphan.stage == null) orphan.__renderDOM(renderSession);
		}
		this.__removedChildren = [];
	}
	,__renderGL: function(renderSession) {
		if(!this.__renderable || this.__worldAlpha <= 0) return;
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.__renderGL(renderSession);
		}
		this.__removedChildren = [];
	}
	,__renderMask: function(renderSession) {
		var bounds = new openfl.geom.Rectangle();
		this.__getTransform();
		this.__getBounds(bounds,new openfl.geom.Matrix());
		renderSession.context.rect(0,0,bounds.width,bounds.height);
	}
	,__setStageReference: function(stage) {
		if(this.stage != stage) {
			if(this.stage != null) this.dispatchEvent(new openfl.events.Event(openfl.events.Event.REMOVED_FROM_STAGE,false,false));
			this.stage = stage;
			if(stage != null) this.dispatchEvent(new openfl.events.Event(openfl.events.Event.ADDED_TO_STAGE,false,false));
			var _g = 0;
			var _g1 = this.__children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.__setStageReference(stage);
			}
		}
	}
	,__update: function(transformOnly,updateChildren) {
		openfl.display.InteractiveObject.prototype.__update.call(this,transformOnly,updateChildren);
		if(!this.__renderable && !this.__worldAlphaChanged && !this.__worldClipChanged && !this.__worldTransformChanged && !this.__worldVisibleChanged) return;
		if(updateChildren) {
			var _g = 0;
			var _g1 = this.__children;
			while(_g < _g1.length) {
				var child = _g1[_g];
				++_g;
				child.__update(transformOnly,true);
			}
		}
	}
	,__updateChildren: function(transformOnly) {
		openfl.display.InteractiveObject.prototype.__updateChildren.call(this,transformOnly);
		var _g = 0;
		var _g1 = this.__children;
		while(_g < _g1.length) {
			var child = _g1[_g];
			++_g;
			child.__update(transformOnly,true);
		}
	}
	,get_numChildren: function() {
		return this.__children.length;
	}
	,__class__: openfl.display.DisplayObjectContainer
	,__properties__: $extend(openfl.display.InteractiveObject.prototype.__properties__,{get_numChildren:"get_numChildren"})
});
openfl.display.Sprite = function() {
	openfl.display.DisplayObjectContainer.call(this);
	this.buttonMode = false;
	this.useHandCursor = true;
};
$hxClasses["openfl.display.Sprite"] = openfl.display.Sprite;
openfl.display.Sprite.__name__ = ["openfl","display","Sprite"];
openfl.display.Sprite.__super__ = openfl.display.DisplayObjectContainer;
openfl.display.Sprite.prototype = $extend(openfl.display.DisplayObjectContainer.prototype,{
	startDrag: function(lockCenter,bounds) {
		if(lockCenter == null) lockCenter = false;
		if(this.stage != null) this.stage.__startDrag(this,lockCenter,bounds);
	}
	,stopDrag: function() {
		if(this.stage != null) this.stage.__stopDrag(this);
	}
	,__getBounds: function(rect,matrix) {
		openfl.display.DisplayObjectContainer.prototype.__getBounds.call(this,rect,matrix);
		if(this.__graphics != null) this.__graphics.__getBounds(rect,matrix != null?matrix:this.__worldTransform);
	}
	,__hitTest: function(x,y,shapeFlag,stack,interactiveOnly) {
		if(!this.get_visible() || interactiveOnly && !this.mouseEnabled) return false;
		var length = 0;
		if(stack != null) length = stack.length;
		if(openfl.display.DisplayObjectContainer.prototype.__hitTest.call(this,x,y,shapeFlag,stack,interactiveOnly)) return true; else if(this.__graphics != null && this.__graphics.__hitTest(x,y,shapeFlag,this.__worldTransform)) {
			if(stack != null) stack.splice(length,0,this);
			return true;
		}
		return false;
	}
	,__renderCanvas: function(renderSession) {
		if(!this.__renderable || this.__worldAlpha <= 0) return;
		if(this.__graphics != null) {
			this.__graphics.__render();
			if(this.__graphics.__canvas != null) {
				if(this.__mask != null) renderSession.maskManager.pushMask(this.__mask);
				var context = renderSession.context;
				context.globalAlpha = this.__worldAlpha;
				var transform = this.__worldTransform;
				if(renderSession.roundPixels) context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx | 0,transform.ty | 0); else context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx,transform.ty);
				if(this.get_scrollRect() == null) context.drawImage(this.__graphics.__canvas,this.__graphics.__bounds.x,this.__graphics.__bounds.y); else context.drawImage(this.__graphics.__canvas,this.get_scrollRect().x - this.__graphics.__bounds.x,this.get_scrollRect().y - this.__graphics.__bounds.y,this.get_scrollRect().width,this.get_scrollRect().height,this.__graphics.__bounds.x + this.get_scrollRect().x,this.__graphics.__bounds.y + this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height);
				if(this.__mask != null) renderSession.maskManager.popMask();
			}
		}
		openfl.display.DisplayObjectContainer.prototype.__renderCanvas.call(this,renderSession);
	}
	,__renderDOM: function(renderSession) {
		if(this.stage != null && this.__worldVisible && this.__renderable && this.__graphics != null) {
			if(this.__graphics.__dirty || this.__worldAlphaChanged || this.__canvas == null && this.__graphics.__canvas != null) {
				this.__graphics.__render();
				if(this.__graphics.__canvas != null) {
					if(this.__canvas == null) {
						this.__canvas = window.document.createElement("canvas");
						this.__canvasContext = this.__canvas.getContext("2d");
						this.__initializeElement(this.__canvas,renderSession);
					}
					this.__canvas.width = this.__graphics.__canvas.width;
					this.__canvas.height = this.__graphics.__canvas.height;
					this.__canvasContext.globalAlpha = this.__worldAlpha;
					this.__canvasContext.drawImage(this.__graphics.__canvas,0,0);
				} else if(this.__canvas != null) {
					renderSession.element.removeChild(this.__canvas);
					this.__canvas = null;
					this.__style = null;
				}
			}
			if(this.__canvas != null) {
				if(this.__worldTransformChanged) {
					var transform = new openfl.geom.Matrix();
					transform.translate(this.__graphics.__bounds.x,this.__graphics.__bounds.y);
					transform = transform.mult(this.__worldTransform);
					this.__style.setProperty(renderSession.transformProperty,renderSession.roundPixels?"matrix3d(" + transform.a + ", " + transform.b + ", " + "0, 0, " + transform.c + ", " + transform.d + ", " + "0, 0, 0, 0, 1, 0, " + (transform.tx | 0) + ", " + (transform.ty | 0) + ", 0, 1)":"matrix3d(" + transform.a + ", " + transform.b + ", " + "0, 0, " + transform.c + ", " + transform.d + ", " + "0, 0, 0, 0, 1, 0, " + transform.tx + ", " + transform.ty + ", 0, 1)",null);
				}
				this.__applyStyle(renderSession,false,false,true);
			}
		} else if(this.__canvas != null) {
			renderSession.element.removeChild(this.__canvas);
			this.__canvas = null;
			this.__style = null;
		}
		openfl.display.DisplayObjectContainer.prototype.__renderDOM.call(this,renderSession);
	}
	,__renderMask: function(renderSession) {
		if(this.__graphics != null) this.__graphics.__renderMask(renderSession); else openfl.display.DisplayObjectContainer.prototype.__renderMask.call(this,renderSession);
	}
	,get_graphics: function() {
		if(this.__graphics == null) this.__graphics = new openfl.display.Graphics();
		return this.__graphics;
	}
	,__class__: openfl.display.Sprite
	,__properties__: $extend(openfl.display.DisplayObjectContainer.prototype.__properties__,{get_graphics:"get_graphics"})
});
var unbox = {};
unbox.framework = {};
unbox.framework.Main = function() {
	openfl.display.Sprite.call(this);
	this.addEventListener(openfl.events.Event.ADDED_TO_STAGE,$bind(this,this.added));
};
$hxClasses["unbox.framework.Main"] = unbox.framework.Main;
unbox.framework.Main.__name__ = ["unbox","framework","Main"];
unbox.framework.Main.main = function() {
	openfl.Lib.current.stage.align = openfl.display.StageAlign.TOP_LEFT;
	openfl.Lib.current.stage.scaleMode = openfl.display.StageScaleMode.NO_SCALE;
	openfl.Lib.current.addChild(new unbox.framework.Main());
};
unbox.framework.Main.__super__ = openfl.display.Sprite;
unbox.framework.Main.prototype = $extend(openfl.display.Sprite.prototype,{
	resize: function(e) {
		if(!this.inited) this.init();
	}
	,init: function() {
		if(this.inited) return;
		this.inited = true;
		var warning = "The Unbox Framework should not be executed directly. Please run the sample project 'UFSample' - Get it by cloning the repository 'git@unboxserver:ufsample'";
		var txt = new openfl.text.TextField();
		txt.multiline = true;
		txt.set_wordWrap(true);
		txt.selectable = false;
		txt.set_defaultTextFormat(new openfl.text.TextFormat("Arial",20,0,false,false,false,"",null,openfl.text.TextFormatAlign.CENTER));
		txt.set_text(warning);
		txt.set_width(this.stage.stageWidth);
		txt.set_height(100);
		txt.set_x(0);
		txt.set_y(this.stage.stageHeight / 2 - 50);
		this.addChild(txt);
		haxe.Log.trace(warning,{ fileName : "Main.hx", lineNumber : 54, className : "unbox.framework.Main", methodName : "init"});
	}
	,added: function(e) {
		this.removeEventListener(openfl.events.Event.ADDED_TO_STAGE,$bind(this,this.added));
		this.stage.addEventListener(openfl.events.Event.RESIZE,$bind(this,this.resize));
		this.init();
	}
	,__class__: unbox.framework.Main
});
var DocumentClass = function() {
	this.stage = openfl.Lib.current.stage;
	unbox.framework.Main.call(this);
	this.dispatchEvent(new openfl.events.Event(openfl.events.Event.ADDED_TO_STAGE,false,false));
};
$hxClasses["DocumentClass"] = DocumentClass;
DocumentClass.__name__ = ["DocumentClass"];
DocumentClass.__super__ = unbox.framework.Main;
DocumentClass.prototype = $extend(unbox.framework.Main.prototype,{
	__class__: DocumentClass
});
openfl.AssetLibrary = function() {
};
$hxClasses["openfl.AssetLibrary"] = openfl.AssetLibrary;
openfl.AssetLibrary.__name__ = ["openfl","AssetLibrary"];
openfl.AssetLibrary.prototype = {
	exists: function(id,type) {
		return false;
	}
	,getBitmapData: function(id) {
		return null;
	}
	,getBytes: function(id) {
		return null;
	}
	,getFont: function(id) {
		return null;
	}
	,getMovieClip: function(id) {
		return null;
	}
	,getMusic: function(id) {
		return this.getSound(id);
	}
	,getPath: function(id) {
		return null;
	}
	,getSound: function(id) {
		return null;
	}
	,getText: function(id) {
		var bytes = this.getBytes(id);
		if(bytes == null) return null; else return bytes.readUTFBytes(bytes.length);
	}
	,isLocal: function(id,type) {
		return true;
	}
	,list: function(type) {
		return null;
	}
	,load: function(handler) {
		handler(this);
	}
	,loadBitmapData: function(id,handler) {
		handler(this.getBitmapData(id));
	}
	,loadBytes: function(id,handler) {
		handler(this.getBytes(id));
	}
	,loadFont: function(id,handler) {
		handler(this.getFont(id));
	}
	,loadMovieClip: function(id,handler) {
		handler(this.getMovieClip(id));
	}
	,loadMusic: function(id,handler) {
		handler(this.getMusic(id));
	}
	,loadSound: function(id,handler) {
		handler(this.getSound(id));
	}
	,loadText: function(id,handler) {
		var callback = function(bytes) {
			if(bytes == null) handler(null); else handler(bytes.readUTFBytes(bytes.length));
		};
		this.loadBytes(id,callback);
	}
	,__class__: openfl.AssetLibrary
};
var DefaultAssetLibrary = function() {
	this.type = new haxe.ds.StringMap();
	this.path = new haxe.ds.StringMap();
	this.className = new haxe.ds.StringMap();
	openfl.AssetLibrary.call(this);
	var id;
};
$hxClasses["DefaultAssetLibrary"] = DefaultAssetLibrary;
DefaultAssetLibrary.__name__ = ["DefaultAssetLibrary"];
DefaultAssetLibrary.__super__ = openfl.AssetLibrary;
DefaultAssetLibrary.prototype = $extend(openfl.AssetLibrary.prototype,{
	exists: function(id,type) {
		var assetType = this.type.get(id);
		if(assetType != null) {
			if(assetType == type || (type == openfl.AssetType.SOUND || type == openfl.AssetType.MUSIC) && (assetType == openfl.AssetType.MUSIC || assetType == openfl.AssetType.SOUND)) return true;
			if(type == openfl.AssetType.BINARY || type == null) return true;
		}
		return false;
	}
	,getBitmapData: function(id) {
		return openfl.display.BitmapData.fromImage((function($this) {
			var $r;
			var key = $this.path.get(id);
			$r = ApplicationMain.images.get(key);
			return $r;
		}(this)));
	}
	,getBytes: function(id) {
		var bytes = null;
		var data = ((function($this) {
			var $r;
			var key = $this.path.get(id);
			$r = ApplicationMain.urlLoaders.get(key);
			return $r;
		}(this))).data;
		if(typeof(data) == "string") {
			bytes = new openfl.utils.ByteArray();
			bytes.writeUTFBytes(data);
		} else if(js.Boot.__instanceof(data,openfl.utils.ByteArray)) bytes = data; else bytes = null;
		if(bytes != null) {
			bytes.position = 0;
			return bytes;
		} else return null;
	}
	,getFont: function(id) {
		return js.Boot.__cast(Type.createInstance(this.className.get(id),[]) , openfl.text.Font);
	}
	,getMusic: function(id) {
		var sound = new openfl.media.Sound();
		sound.__buffer = true;
		sound.load(new openfl.net.URLRequest(this.path.get(id)));
		return sound;
	}
	,getPath: function(id) {
		return this.path.get(id);
	}
	,getSound: function(id) {
		return new openfl.media.Sound(new openfl.net.URLRequest(this.path.get(id)));
	}
	,getText: function(id) {
		var bytes = null;
		var data = ((function($this) {
			var $r;
			var key = $this.path.get(id);
			$r = ApplicationMain.urlLoaders.get(key);
			return $r;
		}(this))).data;
		if(typeof(data) == "string") return data; else if(js.Boot.__instanceof(data,openfl.utils.ByteArray)) bytes = data; else bytes = null;
		if(bytes != null) {
			bytes.position = 0;
			return bytes.readUTFBytes(bytes.length);
		} else return null;
	}
	,isLocal: function(id,type) {
		return true;
	}
	,list: function(type) {
		var items = [];
		var $it0 = this.type.keys();
		while( $it0.hasNext() ) {
			var id = $it0.next();
			if(type == null || this.exists(id,type)) items.push(id);
		}
		return items;
	}
	,loadBitmapData: function(id,handler) {
		if(this.path.exists(id)) {
			var loader = new openfl.display.Loader();
			loader.contentLoaderInfo.addEventListener(openfl.events.Event.COMPLETE,function(event) {
				handler((js.Boot.__cast(event.currentTarget.content , openfl.display.Bitmap)).bitmapData);
			});
			loader.load(new openfl.net.URLRequest(this.path.get(id)));
		} else handler(this.getBitmapData(id));
	}
	,loadBytes: function(id,handler) {
		if(this.path.exists(id)) {
			var loader = new openfl.net.URLLoader();
			loader.addEventListener(openfl.events.Event.COMPLETE,function(event) {
				var bytes = new openfl.utils.ByteArray();
				bytes.writeUTFBytes(event.currentTarget.data);
				bytes.position = 0;
				handler(bytes);
			});
			loader.load(new openfl.net.URLRequest(this.path.get(id)));
		} else handler(this.getBytes(id));
	}
	,loadFont: function(id,handler) {
		handler(this.getFont(id));
	}
	,loadMusic: function(id,handler) {
		handler(this.getMusic(id));
	}
	,loadSound: function(id,handler) {
		handler(this.getSound(id));
	}
	,loadText: function(id,handler) {
		if(this.path.exists(id)) {
			var loader = new openfl.net.URLLoader();
			loader.addEventListener(openfl.events.Event.COMPLETE,function(event) {
				handler(event.currentTarget.data);
			});
			loader.load(new openfl.net.URLRequest(this.path.get(id)));
		} else handler(this.getText(id));
	}
	,__class__: DefaultAssetLibrary
});
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
$hxClasses["EReg"] = EReg;
EReg.__name__ = ["EReg"];
EReg.prototype = {
	match: function(s) {
		if(this.r.global) this.r.lastIndex = 0;
		this.r.m = this.r.exec(s);
		this.r.s = s;
		return this.r.m != null;
	}
	,replace: function(s,by) {
		return s.replace(this.r,by);
	}
	,__class__: EReg
};
var HxOverrides = function() { };
$hxClasses["HxOverrides"] = HxOverrides;
HxOverrides.__name__ = ["HxOverrides"];
HxOverrides.dateStr = function(date) {
	var m = date.getMonth() + 1;
	var d = date.getDate();
	var h = date.getHours();
	var mi = date.getMinutes();
	var s = date.getSeconds();
	return date.getFullYear() + "-" + (m < 10?"0" + m:"" + m) + "-" + (d < 10?"0" + d:"" + d) + " " + (h < 10?"0" + h:"" + h) + ":" + (mi < 10?"0" + mi:"" + mi) + ":" + (s < 10?"0" + s:"" + s);
};
HxOverrides.strDate = function(s) {
	var _g = s.length;
	switch(_g) {
	case 8:
		var k = s.split(":");
		var d = new Date();
		d.setTime(0);
		d.setUTCHours(k[0]);
		d.setUTCMinutes(k[1]);
		d.setUTCSeconds(k[2]);
		return d;
	case 10:
		var k1 = s.split("-");
		return new Date(k1[0],k1[1] - 1,k1[2],0,0,0);
	case 19:
		var k2 = s.split(" ");
		var y = k2[0].split("-");
		var t = k2[1].split(":");
		return new Date(y[0],y[1] - 1,y[2],t[0],t[1],t[2]);
	default:
		throw "Invalid date format : " + s;
	}
};
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
};
HxOverrides.indexOf = function(a,obj,i) {
	var len = a.length;
	if(i < 0) {
		i += len;
		if(i < 0) i = 0;
	}
	while(i < len) {
		if(a[i] === obj) return i;
		i++;
	}
	return -1;
};
HxOverrides.remove = function(a,obj) {
	var i = HxOverrides.indexOf(a,obj,0);
	if(i == -1) return false;
	a.splice(i,1);
	return true;
};
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
};
var List = function() {
	this.length = 0;
};
$hxClasses["List"] = List;
List.__name__ = ["List"];
List.prototype = {
	add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,iterator: function() {
		return { h : this.h, hasNext : function() {
			return this.h != null;
		}, next : function() {
			if(this.h == null) return null;
			var x = this.h[0];
			this.h = this.h[1];
			return x;
		}};
	}
	,__class__: List
};
var IMap = function() { };
$hxClasses["IMap"] = IMap;
IMap.__name__ = ["IMap"];
IMap.prototype = {
	__class__: IMap
};
Math.__name__ = ["Math"];
var NMEPreloader = function() {
	openfl.display.Sprite.call(this);
	var backgroundColor = this.getBackgroundColor();
	var r = backgroundColor >> 16 & 255;
	var g = backgroundColor >> 8 & 255;
	var b = backgroundColor & 255;
	var perceivedLuminosity = 0.299 * r + 0.587 * g + 0.114 * b;
	var color = 0;
	if(perceivedLuminosity < 70) color = 16777215;
	var x = 30;
	var height = 9;
	var y = this.getHeight() / 2 - height / 2;
	var width = this.getWidth() - x * 2;
	var padding = 3;
	this.outline = new openfl.display.Sprite();
	this.outline.get_graphics().lineStyle(1,color,0.15,true);
	this.outline.get_graphics().drawRoundRect(0,0,width,height,padding * 2,padding * 2);
	this.outline.set_x(x);
	this.outline.set_y(y);
	this.addChild(this.outline);
	this.progress = new openfl.display.Sprite();
	this.progress.get_graphics().beginFill(color,0.35);
	this.progress.get_graphics().drawRect(0,0,width - padding * 2,height - padding * 2);
	this.progress.set_x(x + padding);
	this.progress.set_y(y + padding);
	this.progress.set_scaleX(0);
	this.addChild(this.progress);
};
$hxClasses["NMEPreloader"] = NMEPreloader;
NMEPreloader.__name__ = ["NMEPreloader"];
NMEPreloader.__super__ = openfl.display.Sprite;
NMEPreloader.prototype = $extend(openfl.display.Sprite.prototype,{
	getBackgroundColor: function() {
		return 16777215;
	}
	,getHeight: function() {
		var height = 0;
		if(height > 0) return height; else return openfl.Lib.current.stage.stageHeight;
	}
	,getWidth: function() {
		var width = 0;
		if(width > 0) return width; else return openfl.Lib.current.stage.stageWidth;
	}
	,onInit: function() {
	}
	,onLoaded: function() {
		this.dispatchEvent(new openfl.events.Event(openfl.events.Event.COMPLETE));
	}
	,onUpdate: function(bytesLoaded,bytesTotal) {
		var percentLoaded = bytesLoaded / bytesTotal;
		if(percentLoaded > 1) percentLoaded == 1;
		this.progress.set_scaleX(percentLoaded);
	}
	,__class__: NMEPreloader
});
var Reflect = function() { };
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = ["Reflect"];
Reflect.hasField = function(o,field) {
	return Object.prototype.hasOwnProperty.call(o,field);
};
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		return null;
	}
};
Reflect.setField = function(o,field,value) {
	o[field] = value;
};
Reflect.getProperty = function(o,field) {
	var tmp;
	if(o == null) return null; else if(o.__properties__ && (tmp = o.__properties__["get_" + field])) return o[tmp](); else return o[field];
};
Reflect.setProperty = function(o,field,value) {
	var tmp;
	if(o.__properties__ && (tmp = o.__properties__["set_" + field])) o[tmp](value); else o[field] = value;
};
Reflect.callMethod = function(o,func,args) {
	return func.apply(o,args);
};
Reflect.fields = function(o) {
	var a = [];
	if(o != null) {
		var hasOwnProperty = Object.prototype.hasOwnProperty;
		for( var f in o ) {
		if(f != "__id__" && f != "hx__closures__" && hasOwnProperty.call(o,f)) a.push(f);
		}
	}
	return a;
};
Reflect.isFunction = function(f) {
	return typeof(f) == "function" && !(f.__name__ || f.__ename__);
};
Reflect.compareMethods = function(f1,f2) {
	if(f1 == f2) return true;
	if(!Reflect.isFunction(f1) || !Reflect.isFunction(f2)) return false;
	return f1.scope == f2.scope && f1.method == f2.method && f1.method != null;
};
Reflect.isObject = function(v) {
	if(v == null) return false;
	var t = typeof(v);
	return t == "string" || t == "object" && v.__enum__ == null || t == "function" && (v.__name__ || v.__ename__) != null;
};
Reflect.deleteField = function(o,field) {
	if(!Object.prototype.hasOwnProperty.call(o,field)) return false;
	delete(o[field]);
	return true;
};
var Std = function() { };
$hxClasses["Std"] = Std;
Std.__name__ = ["Std"];
Std.string = function(s) {
	return js.Boot.__string_rec(s,"");
};
Std.int = function(x) {
	return x | 0;
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
Std.parseFloat = function(x) {
	return parseFloat(x);
};
var StringBuf = function() {
	this.b = "";
};
$hxClasses["StringBuf"] = StringBuf;
StringBuf.__name__ = ["StringBuf"];
StringBuf.prototype = {
	add: function(x) {
		this.b += Std.string(x);
	}
	,addSub: function(s,pos,len) {
		if(len == null) this.b += HxOverrides.substr(s,pos,null); else this.b += HxOverrides.substr(s,pos,len);
	}
	,__class__: StringBuf
};
var StringTools = function() { };
$hxClasses["StringTools"] = StringTools;
StringTools.__name__ = ["StringTools"];
StringTools.urlEncode = function(s) {
	return encodeURIComponent(s);
};
StringTools.urlDecode = function(s) {
	return decodeURIComponent(s.split("+").join(" "));
};
StringTools.htmlEscape = function(s,quotes) {
	s = s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
	if(quotes) return s.split("\"").join("&quot;").split("'").join("&#039;"); else return s;
};
StringTools.startsWith = function(s,start) {
	return s.length >= start.length && HxOverrides.substr(s,0,start.length) == start;
};
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c > 8 && c < 14 || c == 32;
};
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
};
StringTools.rtrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,l - r - 1)) r++;
	if(r > 0) return HxOverrides.substr(s,0,l - r); else return s;
};
StringTools.trim = function(s) {
	return StringTools.ltrim(StringTools.rtrim(s));
};
StringTools.replace = function(s,sub,by) {
	return s.split(sub).join(by);
};
StringTools.hex = function(n,digits) {
	var s = "";
	var hexChars = "0123456789ABCDEF";
	do {
		s = hexChars.charAt(n & 15) + s;
		n >>>= 4;
	} while(n > 0);
	if(digits != null) while(s.length < digits) s = "0" + s;
	return s;
};
StringTools.fastCodeAt = function(s,index) {
	return s.charCodeAt(index);
};
var ValueType = $hxClasses["ValueType"] = { __ename__ : ["ValueType"], __constructs__ : ["TNull","TInt","TFloat","TBool","TObject","TFunction","TClass","TEnum","TUnknown"] };
ValueType.TNull = ["TNull",0];
ValueType.TNull.toString = $estr;
ValueType.TNull.__enum__ = ValueType;
ValueType.TInt = ["TInt",1];
ValueType.TInt.toString = $estr;
ValueType.TInt.__enum__ = ValueType;
ValueType.TFloat = ["TFloat",2];
ValueType.TFloat.toString = $estr;
ValueType.TFloat.__enum__ = ValueType;
ValueType.TBool = ["TBool",3];
ValueType.TBool.toString = $estr;
ValueType.TBool.__enum__ = ValueType;
ValueType.TObject = ["TObject",4];
ValueType.TObject.toString = $estr;
ValueType.TObject.__enum__ = ValueType;
ValueType.TFunction = ["TFunction",5];
ValueType.TFunction.toString = $estr;
ValueType.TFunction.__enum__ = ValueType;
ValueType.TClass = function(c) { var $x = ["TClass",6,c]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; };
ValueType.TEnum = function(e) { var $x = ["TEnum",7,e]; $x.__enum__ = ValueType; $x.toString = $estr; return $x; };
ValueType.TUnknown = ["TUnknown",8];
ValueType.TUnknown.toString = $estr;
ValueType.TUnknown.__enum__ = ValueType;
var Type = function() { };
$hxClasses["Type"] = Type;
Type.__name__ = ["Type"];
Type.getClass = function(o) {
	if(o == null) return null;
	if((o instanceof Array) && o.__enum__ == null) return Array; else return o.__class__;
};
Type.getClassName = function(c) {
	var a = c.__name__;
	return a.join(".");
};
Type.getEnumName = function(e) {
	var a = e.__ename__;
	return a.join(".");
};
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
};
Type.resolveEnum = function(name) {
	var e = $hxClasses[name];
	if(e == null || !e.__ename__) return null;
	return e;
};
Type.createInstance = function(cl,args) {
	var _g = args.length;
	switch(_g) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw "Too many arguments";
	}
	return null;
};
Type.createEmptyInstance = function(cl) {
	function empty() {}; empty.prototype = cl.prototype;
	return new empty();
};
Type.createEnum = function(e,constr,params) {
	var f = Reflect.field(e,constr);
	if(f == null) throw "No such constructor " + constr;
	if(Reflect.isFunction(f)) {
		if(params == null) throw "Constructor " + constr + " need parameters";
		return f.apply(e,params);
	}
	if(params != null && params.length != 0) throw "Constructor " + constr + " does not need parameters";
	return f;
};
Type.createEnumIndex = function(e,index,params) {
	var c = e.__constructs__[index];
	if(c == null) throw index + " is not a valid enum constructor index";
	return Type.createEnum(e,c,params);
};
Type.getClassFields = function(c) {
	var a = Reflect.fields(c);
	HxOverrides.remove(a,"__name__");
	HxOverrides.remove(a,"__interfaces__");
	HxOverrides.remove(a,"__properties__");
	HxOverrides.remove(a,"__super__");
	HxOverrides.remove(a,"prototype");
	return a;
};
Type.getEnumConstructs = function(e) {
	var a = e.__constructs__;
	return a.slice();
};
Type["typeof"] = function(v) {
	var _g = typeof(v);
	switch(_g) {
	case "boolean":
		return ValueType.TBool;
	case "string":
		return ValueType.TClass(String);
	case "number":
		if(Math.ceil(v) == v % 2147483648.0) return ValueType.TInt;
		return ValueType.TFloat;
	case "object":
		if(v == null) return ValueType.TNull;
		var e = v.__enum__;
		if(e != null) return ValueType.TEnum(e);
		var c;
		if((v instanceof Array) && v.__enum__ == null) c = Array; else c = v.__class__;
		if(c != null) return ValueType.TClass(c);
		return ValueType.TObject;
	case "function":
		if(v.__name__ || v.__ename__) return ValueType.TObject;
		return ValueType.TFunction;
	case "undefined":
		return ValueType.TNull;
	default:
		return ValueType.TUnknown;
	}
};
var XmlType = $hxClasses["XmlType"] = { __ename__ : ["XmlType"], __constructs__ : [] };
var Xml = function() {
};
$hxClasses["Xml"] = Xml;
Xml.__name__ = ["Xml"];
Xml.Element = null;
Xml.PCData = null;
Xml.CData = null;
Xml.Comment = null;
Xml.DocType = null;
Xml.ProcessingInstruction = null;
Xml.Document = null;
Xml.parse = function(str) {
	return haxe.xml.Parser.parse(str);
};
Xml.createElement = function(name) {
	var r = new Xml();
	r.nodeType = Xml.Element;
	r._children = new Array();
	r._attributes = new haxe.ds.StringMap();
	r.set_nodeName(name);
	return r;
};
Xml.createPCData = function(data) {
	var r = new Xml();
	r.nodeType = Xml.PCData;
	r.set_nodeValue(data);
	return r;
};
Xml.createCData = function(data) {
	var r = new Xml();
	r.nodeType = Xml.CData;
	r.set_nodeValue(data);
	return r;
};
Xml.createComment = function(data) {
	var r = new Xml();
	r.nodeType = Xml.Comment;
	r.set_nodeValue(data);
	return r;
};
Xml.createDocType = function(data) {
	var r = new Xml();
	r.nodeType = Xml.DocType;
	r.set_nodeValue(data);
	return r;
};
Xml.createProcessingInstruction = function(data) {
	var r = new Xml();
	r.nodeType = Xml.ProcessingInstruction;
	r.set_nodeValue(data);
	return r;
};
Xml.createDocument = function() {
	var r = new Xml();
	r.nodeType = Xml.Document;
	r._children = new Array();
	return r;
};
Xml.prototype = {
	get_nodeName: function() {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._nodeName;
	}
	,set_nodeName: function(n) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._nodeName = n;
	}
	,set_nodeValue: function(v) {
		if(this.nodeType == Xml.Element || this.nodeType == Xml.Document) throw "bad nodeType";
		return this._nodeValue = v;
	}
	,get: function(att) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._attributes.get(att);
	}
	,set: function(att,value) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		this._attributes.set(att,value);
	}
	,exists: function(att) {
		if(this.nodeType != Xml.Element) throw "bad nodeType";
		return this._attributes.exists(att);
	}
	,iterator: function() {
		if(this._children == null) throw "bad nodetype";
		return { cur : 0, x : this._children, hasNext : function() {
			return this.cur < this.x.length;
		}, next : function() {
			return this.x[this.cur++];
		}};
	}
	,elements: function() {
		if(this._children == null) throw "bad nodetype";
		return { cur : 0, x : this._children, hasNext : function() {
			var k = this.cur;
			var l = this.x.length;
			while(k < l) {
				if(this.x[k].nodeType == Xml.Element) break;
				k += 1;
			}
			this.cur = k;
			return k < l;
		}, next : function() {
			var k1 = this.cur;
			var l1 = this.x.length;
			while(k1 < l1) {
				var n = this.x[k1];
				k1 += 1;
				if(n.nodeType == Xml.Element) {
					this.cur = k1;
					return n;
				}
			}
			return null;
		}};
	}
	,elementsNamed: function(name) {
		if(this._children == null) throw "bad nodetype";
		return { cur : 0, x : this._children, hasNext : function() {
			var k = this.cur;
			var l = this.x.length;
			while(k < l) {
				var n = this.x[k];
				if(n.nodeType == Xml.Element && n._nodeName == name) break;
				k++;
			}
			this.cur = k;
			return k < l;
		}, next : function() {
			var k1 = this.cur;
			var l1 = this.x.length;
			while(k1 < l1) {
				var n1 = this.x[k1];
				k1++;
				if(n1.nodeType == Xml.Element && n1._nodeName == name) {
					this.cur = k1;
					return n1;
				}
			}
			return null;
		}};
	}
	,firstElement: function() {
		if(this._children == null) throw "bad nodetype";
		var cur = 0;
		var l = this._children.length;
		while(cur < l) {
			var n = this._children[cur];
			if(n.nodeType == Xml.Element) return n;
			cur++;
		}
		return null;
	}
	,addChild: function(x) {
		if(this._children == null) throw "bad nodetype";
		if(x._parent != null) HxOverrides.remove(x._parent._children,x);
		x._parent = this;
		this._children.push(x);
	}
	,toString: function() {
		if(this.nodeType == Xml.PCData) return StringTools.htmlEscape(this._nodeValue);
		if(this.nodeType == Xml.CData) return "<![CDATA[" + this._nodeValue + "]]>";
		if(this.nodeType == Xml.Comment) return "<!--" + this._nodeValue + "-->";
		if(this.nodeType == Xml.DocType) return "<!DOCTYPE " + this._nodeValue + ">";
		if(this.nodeType == Xml.ProcessingInstruction) return "<?" + this._nodeValue + "?>";
		var s = new StringBuf();
		if(this.nodeType == Xml.Element) {
			s.b += "<";
			s.b += this._nodeName;
			var $it0 = this._attributes.keys();
			while( $it0.hasNext() ) {
				var k = $it0.next();
				s.b += " ";
				s.b += k;
				s.b += "=\"";
				s.add(this._attributes.get(k));
				s.b += "\"";
			}
			if(this._children.length == 0) {
				s.b += "/>";
				return s.b;
			}
			s.b += ">";
		}
		var $it1 = this.iterator();
		while( $it1.hasNext() ) {
			var x = $it1.next();
			s.add(x.toString());
		}
		if(this.nodeType == Xml.Element) {
			s.b += "</";
			s.b += this._nodeName;
			s.b += ">";
		}
		return s.b;
	}
	,__class__: Xml
	,__properties__: {set_nodeValue:"set_nodeValue",set_nodeName:"set_nodeName",get_nodeName:"get_nodeName"}
};
var haxe = {};
haxe.StackItem = $hxClasses["haxe.StackItem"] = { __ename__ : ["haxe","StackItem"], __constructs__ : ["CFunction","Module","FilePos","Method","LocalFunction"] };
haxe.StackItem.CFunction = ["CFunction",0];
haxe.StackItem.CFunction.toString = $estr;
haxe.StackItem.CFunction.__enum__ = haxe.StackItem;
haxe.StackItem.Module = function(m) { var $x = ["Module",1,m]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; };
haxe.StackItem.FilePos = function(s,file,line) { var $x = ["FilePos",2,s,file,line]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; };
haxe.StackItem.Method = function(classname,method) { var $x = ["Method",3,classname,method]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; };
haxe.StackItem.LocalFunction = function(v) { var $x = ["LocalFunction",4,v]; $x.__enum__ = haxe.StackItem; $x.toString = $estr; return $x; };
haxe.CallStack = function() { };
$hxClasses["haxe.CallStack"] = haxe.CallStack;
haxe.CallStack.__name__ = ["haxe","CallStack"];
haxe.CallStack.exceptionStack = function() {
	return [];
};
haxe.CallStack.toString = function(stack) {
	var b = new StringBuf();
	var _g = 0;
	while(_g < stack.length) {
		var s = stack[_g];
		++_g;
		b.b += "\nCalled from ";
		haxe.CallStack.itemToString(b,s);
	}
	return b.b;
};
haxe.CallStack.itemToString = function(b,s) {
	switch(s[1]) {
	case 0:
		b.b += "a C function";
		break;
	case 1:
		var m = s[2];
		b.b += "module ";
		b.b += m;
		break;
	case 2:
		var line = s[4];
		var file = s[3];
		var s1 = s[2];
		if(s1 != null) {
			haxe.CallStack.itemToString(b,s1);
			b.b += " (";
		}
		b.b += file;
		b.b += " line ";
		b.b += "" + line;
		if(s1 != null) b.b += ")";
		break;
	case 3:
		var meth = s[3];
		var cname = s[2];
		b.b += cname;
		b.b += ".";
		b.b += meth;
		break;
	case 4:
		var n = s[2];
		b.b += "local function #";
		b.b += "" + n;
		break;
	}
};
haxe.Log = function() { };
$hxClasses["haxe.Log"] = haxe.Log;
haxe.Log.__name__ = ["haxe","Log"];
haxe.Log.trace = function(v,infos) {
	js.Boot.__trace(v,infos);
};
haxe.Serializer = function() {
	this.buf = new StringBuf();
	this.cache = new Array();
	this.useCache = haxe.Serializer.USE_CACHE;
	this.useEnumIndex = haxe.Serializer.USE_ENUM_INDEX;
	this.shash = new haxe.ds.StringMap();
	this.scount = 0;
};
$hxClasses["haxe.Serializer"] = haxe.Serializer;
haxe.Serializer.__name__ = ["haxe","Serializer"];
haxe.Serializer.run = function(v) {
	var s = new haxe.Serializer();
	s.serialize(v);
	return s.toString();
};
haxe.Serializer.prototype = {
	toString: function() {
		return this.buf.b;
	}
	,serializeString: function(s) {
		var x = this.shash.get(s);
		if(x != null) {
			this.buf.b += "R";
			this.buf.b += "" + x;
			return;
		}
		this.shash.set(s,this.scount++);
		this.buf.b += "y";
		s = encodeURIComponent(s);
		this.buf.b += "" + s.length;
		this.buf.b += ":";
		this.buf.b += s;
	}
	,serializeRef: function(v) {
		var vt = typeof(v);
		var _g1 = 0;
		var _g = this.cache.length;
		while(_g1 < _g) {
			var i = _g1++;
			var ci = this.cache[i];
			if(typeof(ci) == vt && ci == v) {
				this.buf.b += "r";
				this.buf.b += "" + i;
				return true;
			}
		}
		this.cache.push(v);
		return false;
	}
	,serializeFields: function(v) {
		var _g = 0;
		var _g1 = Reflect.fields(v);
		while(_g < _g1.length) {
			var f = _g1[_g];
			++_g;
			this.serializeString(f);
			this.serialize(Reflect.field(v,f));
		}
		this.buf.b += "g";
	}
	,serialize: function(v) {
		{
			var _g = Type["typeof"](v);
			switch(_g[1]) {
			case 0:
				this.buf.b += "n";
				break;
			case 1:
				var v1 = v;
				if(v1 == 0) {
					this.buf.b += "z";
					return;
				}
				this.buf.b += "i";
				this.buf.b += "" + v1;
				break;
			case 2:
				var v2 = v;
				if(Math.isNaN(v2)) this.buf.b += "k"; else if(!Math.isFinite(v2)) if(v2 < 0) this.buf.b += "m"; else this.buf.b += "p"; else {
					this.buf.b += "d";
					this.buf.b += "" + v2;
				}
				break;
			case 3:
				if(v) this.buf.b += "t"; else this.buf.b += "f";
				break;
			case 6:
				var c = _g[2];
				if(c == String) {
					this.serializeString(v);
					return;
				}
				if(this.useCache && this.serializeRef(v)) return;
				switch(c) {
				case Array:
					var ucount = 0;
					this.buf.b += "a";
					var l = v.length;
					var _g1 = 0;
					while(_g1 < l) {
						var i = _g1++;
						if(v[i] == null) ucount++; else {
							if(ucount > 0) {
								if(ucount == 1) this.buf.b += "n"; else {
									this.buf.b += "u";
									this.buf.b += "" + ucount;
								}
								ucount = 0;
							}
							this.serialize(v[i]);
						}
					}
					if(ucount > 0) {
						if(ucount == 1) this.buf.b += "n"; else {
							this.buf.b += "u";
							this.buf.b += "" + ucount;
						}
					}
					this.buf.b += "h";
					break;
				case List:
					this.buf.b += "l";
					var v3 = v;
					var $it0 = v3.iterator();
					while( $it0.hasNext() ) {
						var i1 = $it0.next();
						this.serialize(i1);
					}
					this.buf.b += "h";
					break;
				case Date:
					var d = v;
					this.buf.b += "v";
					this.buf.add(HxOverrides.dateStr(d));
					break;
				case haxe.ds.StringMap:
					this.buf.b += "b";
					var v4 = v;
					var $it1 = v4.keys();
					while( $it1.hasNext() ) {
						var k = $it1.next();
						this.serializeString(k);
						this.serialize(v4.get(k));
					}
					this.buf.b += "h";
					break;
				case haxe.ds.IntMap:
					this.buf.b += "q";
					var v5 = v;
					var $it2 = v5.keys();
					while( $it2.hasNext() ) {
						var k1 = $it2.next();
						this.buf.b += ":";
						this.buf.b += "" + k1;
						this.serialize(v5.get(k1));
					}
					this.buf.b += "h";
					break;
				case haxe.ds.ObjectMap:
					this.buf.b += "M";
					var v6 = v;
					var $it3 = v6.keys();
					while( $it3.hasNext() ) {
						var k2 = $it3.next();
						var id = Reflect.field(k2,"__id__");
						Reflect.deleteField(k2,"__id__");
						this.serialize(k2);
						k2.__id__ = id;
						this.serialize(v6.h[k2.__id__]);
					}
					this.buf.b += "h";
					break;
				case haxe.io.Bytes:
					var v7 = v;
					var i2 = 0;
					var max = v7.length - 2;
					var charsBuf = new StringBuf();
					var b64 = haxe.Serializer.BASE64;
					while(i2 < max) {
						var b1 = v7.get(i2++);
						var b2 = v7.get(i2++);
						var b3 = v7.get(i2++);
						charsBuf.add(b64.charAt(b1 >> 2));
						charsBuf.add(b64.charAt((b1 << 4 | b2 >> 4) & 63));
						charsBuf.add(b64.charAt((b2 << 2 | b3 >> 6) & 63));
						charsBuf.add(b64.charAt(b3 & 63));
					}
					if(i2 == max) {
						var b11 = v7.get(i2++);
						var b21 = v7.get(i2++);
						charsBuf.add(b64.charAt(b11 >> 2));
						charsBuf.add(b64.charAt((b11 << 4 | b21 >> 4) & 63));
						charsBuf.add(b64.charAt(b21 << 2 & 63));
					} else if(i2 == max + 1) {
						var b12 = v7.get(i2++);
						charsBuf.add(b64.charAt(b12 >> 2));
						charsBuf.add(b64.charAt(b12 << 4 & 63));
					}
					var chars = charsBuf.b;
					this.buf.b += "s";
					this.buf.b += "" + chars.length;
					this.buf.b += ":";
					this.buf.b += chars;
					break;
				default:
					if(this.useCache) this.cache.pop();
					if(v.hxSerialize != null) {
						this.buf.b += "C";
						this.serializeString(Type.getClassName(c));
						if(this.useCache) this.cache.push(v);
						v.hxSerialize(this);
						this.buf.b += "g";
					} else {
						this.buf.b += "c";
						this.serializeString(Type.getClassName(c));
						if(this.useCache) this.cache.push(v);
						this.serializeFields(v);
					}
				}
				break;
			case 4:
				if(this.useCache && this.serializeRef(v)) return;
				this.buf.b += "o";
				this.serializeFields(v);
				break;
			case 7:
				var e = _g[2];
				if(this.useCache) {
					if(this.serializeRef(v)) return;
					this.cache.pop();
				}
				if(this.useEnumIndex) this.buf.b += "j"; else this.buf.b += "w";
				this.serializeString(Type.getEnumName(e));
				if(this.useEnumIndex) {
					this.buf.b += ":";
					this.buf.b += Std.string(v[1]);
				} else this.serializeString(v[0]);
				this.buf.b += ":";
				var l1 = v.length;
				this.buf.b += "" + (l1 - 2);
				var _g11 = 2;
				while(_g11 < l1) {
					var i3 = _g11++;
					this.serialize(v[i3]);
				}
				if(this.useCache) this.cache.push(v);
				break;
			case 5:
				throw "Cannot serialize function";
				break;
			default:
				throw "Cannot serialize " + Std.string(v);
			}
		}
	}
	,__class__: haxe.Serializer
};
haxe.Timer = function() { };
$hxClasses["haxe.Timer"] = haxe.Timer;
haxe.Timer.__name__ = ["haxe","Timer"];
haxe.Timer.stamp = function() {
	return new Date().getTime() / 1000;
};
haxe.Unserializer = function(buf) {
	this.buf = buf;
	this.length = buf.length;
	this.pos = 0;
	this.scache = new Array();
	this.cache = new Array();
	var r = haxe.Unserializer.DEFAULT_RESOLVER;
	if(r == null) {
		r = Type;
		haxe.Unserializer.DEFAULT_RESOLVER = r;
	}
	this.setResolver(r);
};
$hxClasses["haxe.Unserializer"] = haxe.Unserializer;
haxe.Unserializer.__name__ = ["haxe","Unserializer"];
haxe.Unserializer.initCodes = function() {
	var codes = new Array();
	var _g1 = 0;
	var _g = haxe.Unserializer.BASE64.length;
	while(_g1 < _g) {
		var i = _g1++;
		codes[haxe.Unserializer.BASE64.charCodeAt(i)] = i;
	}
	return codes;
};
haxe.Unserializer.prototype = {
	setResolver: function(r) {
		if(r == null) this.resolver = { resolveClass : function(_) {
			return null;
		}, resolveEnum : function(_1) {
			return null;
		}}; else this.resolver = r;
	}
	,get: function(p) {
		return this.buf.charCodeAt(p);
	}
	,readDigits: function() {
		var k = 0;
		var s = false;
		var fpos = this.pos;
		while(true) {
			var c = this.buf.charCodeAt(this.pos);
			if(c != c) break;
			if(c == 45) {
				if(this.pos != fpos) break;
				s = true;
				this.pos++;
				continue;
			}
			if(c < 48 || c > 57) break;
			k = k * 10 + (c - 48);
			this.pos++;
		}
		if(s) k *= -1;
		return k;
	}
	,unserializeObject: function(o) {
		while(true) {
			if(this.pos >= this.length) throw "Invalid object";
			if(this.buf.charCodeAt(this.pos) == 103) break;
			var k = this.unserialize();
			if(!(typeof(k) == "string")) throw "Invalid object key";
			var v = this.unserialize();
			o[k] = v;
		}
		this.pos++;
	}
	,unserializeEnum: function(edecl,tag) {
		if(this.get(this.pos++) != 58) throw "Invalid enum format";
		var nargs = this.readDigits();
		if(nargs == 0) return Type.createEnum(edecl,tag);
		var args = new Array();
		while(nargs-- > 0) args.push(this.unserialize());
		return Type.createEnum(edecl,tag,args);
	}
	,unserialize: function() {
		var _g = this.get(this.pos++);
		switch(_g) {
		case 110:
			return null;
		case 116:
			return true;
		case 102:
			return false;
		case 122:
			return 0;
		case 105:
			return this.readDigits();
		case 100:
			var p1 = this.pos;
			while(true) {
				var c = this.buf.charCodeAt(this.pos);
				if(c >= 43 && c < 58 || c == 101 || c == 69) this.pos++; else break;
			}
			return Std.parseFloat(HxOverrides.substr(this.buf,p1,this.pos - p1));
		case 121:
			var len = this.readDigits();
			if(this.get(this.pos++) != 58 || this.length - this.pos < len) throw "Invalid string length";
			var s = HxOverrides.substr(this.buf,this.pos,len);
			this.pos += len;
			s = decodeURIComponent(s.split("+").join(" "));
			this.scache.push(s);
			return s;
		case 107:
			return Math.NaN;
		case 109:
			return Math.NEGATIVE_INFINITY;
		case 112:
			return Math.POSITIVE_INFINITY;
		case 97:
			var buf = this.buf;
			var a = new Array();
			this.cache.push(a);
			while(true) {
				var c1 = this.buf.charCodeAt(this.pos);
				if(c1 == 104) {
					this.pos++;
					break;
				}
				if(c1 == 117) {
					this.pos++;
					var n = this.readDigits();
					a[a.length + n - 1] = null;
				} else a.push(this.unserialize());
			}
			return a;
		case 111:
			var o = { };
			this.cache.push(o);
			this.unserializeObject(o);
			return o;
		case 114:
			var n1 = this.readDigits();
			if(n1 < 0 || n1 >= this.cache.length) throw "Invalid reference";
			return this.cache[n1];
		case 82:
			var n2 = this.readDigits();
			if(n2 < 0 || n2 >= this.scache.length) throw "Invalid string reference";
			return this.scache[n2];
		case 120:
			throw this.unserialize();
			break;
		case 99:
			var name = this.unserialize();
			var cl = this.resolver.resolveClass(name);
			if(cl == null) throw "Class not found " + name;
			var o1 = Type.createEmptyInstance(cl);
			this.cache.push(o1);
			this.unserializeObject(o1);
			return o1;
		case 119:
			var name1 = this.unserialize();
			var edecl = this.resolver.resolveEnum(name1);
			if(edecl == null) throw "Enum not found " + name1;
			var e = this.unserializeEnum(edecl,this.unserialize());
			this.cache.push(e);
			return e;
		case 106:
			var name2 = this.unserialize();
			var edecl1 = this.resolver.resolveEnum(name2);
			if(edecl1 == null) throw "Enum not found " + name2;
			this.pos++;
			var index = this.readDigits();
			var tag = Type.getEnumConstructs(edecl1)[index];
			if(tag == null) throw "Unknown enum index " + name2 + "@" + index;
			var e1 = this.unserializeEnum(edecl1,tag);
			this.cache.push(e1);
			return e1;
		case 108:
			var l = new List();
			this.cache.push(l);
			var buf1 = this.buf;
			while(this.buf.charCodeAt(this.pos) != 104) l.add(this.unserialize());
			this.pos++;
			return l;
		case 98:
			var h = new haxe.ds.StringMap();
			this.cache.push(h);
			var buf2 = this.buf;
			while(this.buf.charCodeAt(this.pos) != 104) {
				var s1 = this.unserialize();
				h.set(s1,this.unserialize());
			}
			this.pos++;
			return h;
		case 113:
			var h1 = new haxe.ds.IntMap();
			this.cache.push(h1);
			var buf3 = this.buf;
			var c2 = this.get(this.pos++);
			while(c2 == 58) {
				var i = this.readDigits();
				h1.set(i,this.unserialize());
				c2 = this.get(this.pos++);
			}
			if(c2 != 104) throw "Invalid IntMap format";
			return h1;
		case 77:
			var h2 = new haxe.ds.ObjectMap();
			this.cache.push(h2);
			var buf4 = this.buf;
			while(this.buf.charCodeAt(this.pos) != 104) {
				var s2 = this.unserialize();
				h2.set(s2,this.unserialize());
			}
			this.pos++;
			return h2;
		case 118:
			var d;
			var s3 = HxOverrides.substr(this.buf,this.pos,19);
			d = HxOverrides.strDate(s3);
			this.cache.push(d);
			this.pos += 19;
			return d;
		case 115:
			var len1 = this.readDigits();
			var buf5 = this.buf;
			if(this.get(this.pos++) != 58 || this.length - this.pos < len1) throw "Invalid bytes length";
			var codes = haxe.Unserializer.CODES;
			if(codes == null) {
				codes = haxe.Unserializer.initCodes();
				haxe.Unserializer.CODES = codes;
			}
			var i1 = this.pos;
			var rest = len1 & 3;
			var size;
			size = (len1 >> 2) * 3 + (rest >= 2?rest - 1:0);
			var max = i1 + (len1 - rest);
			var bytes = haxe.io.Bytes.alloc(size);
			var bpos = 0;
			while(i1 < max) {
				var c11 = codes[StringTools.fastCodeAt(buf5,i1++)];
				var c21 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c11 << 2 | c21 >> 4);
				var c3 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c21 << 4 | c3 >> 2);
				var c4 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c3 << 6 | c4);
			}
			if(rest >= 2) {
				var c12 = codes[StringTools.fastCodeAt(buf5,i1++)];
				var c22 = codes[StringTools.fastCodeAt(buf5,i1++)];
				bytes.set(bpos++,c12 << 2 | c22 >> 4);
				if(rest == 3) {
					var c31 = codes[StringTools.fastCodeAt(buf5,i1++)];
					bytes.set(bpos++,c22 << 4 | c31 >> 2);
				}
			}
			this.pos += len1;
			this.cache.push(bytes);
			return bytes;
		case 67:
			var name3 = this.unserialize();
			var cl1 = this.resolver.resolveClass(name3);
			if(cl1 == null) throw "Class not found " + name3;
			var o2 = Type.createEmptyInstance(cl1);
			this.cache.push(o2);
			o2.hxUnserialize(this);
			if(this.get(this.pos++) != 103) throw "Invalid custom data";
			return o2;
		default:
		}
		this.pos--;
		throw "Invalid char " + this.buf.charAt(this.pos) + " at position " + this.pos;
	}
	,__class__: haxe.Unserializer
};
haxe.crypto = {};
haxe.crypto.BaseCode = function(base) {
	var len = base.length;
	var nbits = 1;
	while(len > 1 << nbits) nbits++;
	if(nbits > 8 || len != 1 << nbits) throw "BaseCode : base length must be a power of two.";
	this.base = base;
	this.nbits = nbits;
};
$hxClasses["haxe.crypto.BaseCode"] = haxe.crypto.BaseCode;
haxe.crypto.BaseCode.__name__ = ["haxe","crypto","BaseCode"];
haxe.crypto.BaseCode.prototype = {
	encodeBytes: function(b) {
		var nbits = this.nbits;
		var base = this.base;
		var size = b.length * 8 / nbits | 0;
		var out = haxe.io.Bytes.alloc(size + (b.length * 8 % nbits == 0?0:1));
		var buf = 0;
		var curbits = 0;
		var mask = (1 << nbits) - 1;
		var pin = 0;
		var pout = 0;
		while(pout < size) {
			while(curbits < nbits) {
				curbits += 8;
				buf <<= 8;
				buf |= b.get(pin++);
			}
			curbits -= nbits;
			out.set(pout++,base.b[buf >> curbits & mask]);
		}
		if(curbits > 0) out.set(pout++,base.b[buf << nbits - curbits & mask]);
		return out;
	}
	,__class__: haxe.crypto.BaseCode
};
haxe.ds = {};
haxe.ds.IntMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.IntMap"] = haxe.ds.IntMap;
haxe.ds.IntMap.__name__ = ["haxe","ds","IntMap"];
haxe.ds.IntMap.__interfaces__ = [IMap];
haxe.ds.IntMap.prototype = {
	set: function(key,value) {
		this.h[key] = value;
	}
	,get: function(key) {
		return this.h[key];
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key | 0);
		}
		return HxOverrides.iter(a);
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref[i];
		}};
	}
	,__class__: haxe.ds.IntMap
};
haxe.ds.ObjectMap = function() {
	this.h = { };
	this.h.__keys__ = { };
};
$hxClasses["haxe.ds.ObjectMap"] = haxe.ds.ObjectMap;
haxe.ds.ObjectMap.__name__ = ["haxe","ds","ObjectMap"];
haxe.ds.ObjectMap.__interfaces__ = [IMap];
haxe.ds.ObjectMap.prototype = {
	set: function(key,value) {
		var id = key.__id__ || (key.__id__ = ++haxe.ds.ObjectMap.count);
		this.h[id] = value;
		this.h.__keys__[id] = key;
	}
	,get: function(key) {
		return this.h[key.__id__];
	}
	,exists: function(key) {
		return this.h.__keys__[key.__id__] != null;
	}
	,remove: function(key) {
		var id = key.__id__;
		if(this.h.__keys__[id] == null) return false;
		delete(this.h[id]);
		delete(this.h.__keys__[id]);
		return true;
	}
	,keys: function() {
		var a = [];
		for( var key in this.h.__keys__ ) {
		if(this.h.hasOwnProperty(key)) a.push(this.h.__keys__[key]);
		}
		return HxOverrides.iter(a);
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref[i.__id__];
		}};
	}
	,__class__: haxe.ds.ObjectMap
};
haxe.ds.StringMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.StringMap"] = haxe.ds.StringMap;
haxe.ds.StringMap.__name__ = ["haxe","ds","StringMap"];
haxe.ds.StringMap.__interfaces__ = [IMap];
haxe.ds.StringMap.prototype = {
	set: function(key,value) {
		this.h["$" + key] = value;
	}
	,get: function(key) {
		return this.h["$" + key];
	}
	,exists: function(key) {
		return this.h.hasOwnProperty("$" + key);
	}
	,remove: function(key) {
		key = "$" + key;
		if(!this.h.hasOwnProperty(key)) return false;
		delete(this.h[key]);
		return true;
	}
	,keys: function() {
		var a = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) a.push(key.substr(1));
		}
		return HxOverrides.iter(a);
	}
	,iterator: function() {
		return { ref : this.h, it : this.keys(), hasNext : function() {
			return this.it.hasNext();
		}, next : function() {
			var i = this.it.next();
			return this.ref["$" + i];
		}};
	}
	,toString: function() {
		var s = new StringBuf();
		s.b += "{";
		var it = this.keys();
		while( it.hasNext() ) {
			var i = it.next();
			s.b += i;
			s.b += " => ";
			s.add(Std.string(this.get(i)));
			if(it.hasNext()) s.b += ", ";
		}
		s.b += "}";
		return s.b;
	}
	,__class__: haxe.ds.StringMap
};
haxe.ds._Vector = {};
haxe.ds._Vector.Vector_Impl_ = function() { };
$hxClasses["haxe.ds._Vector.Vector_Impl_"] = haxe.ds._Vector.Vector_Impl_;
haxe.ds._Vector.Vector_Impl_.__name__ = ["haxe","ds","_Vector","Vector_Impl_"];
haxe.ds._Vector.Vector_Impl_.blit = function(src,srcPos,dest,destPos,len) {
	var _g = 0;
	while(_g < len) {
		var i = _g++;
		dest[destPos + i] = src[srcPos + i];
	}
};
haxe.io = {};
haxe.io.Bytes = function(length,b) {
	this.length = length;
	this.b = b;
};
$hxClasses["haxe.io.Bytes"] = haxe.io.Bytes;
haxe.io.Bytes.__name__ = ["haxe","io","Bytes"];
haxe.io.Bytes.alloc = function(length) {
	var a = new Array();
	var _g = 0;
	while(_g < length) {
		var i = _g++;
		a.push(0);
	}
	return new haxe.io.Bytes(length,a);
};
haxe.io.Bytes.ofString = function(s) {
	var a = new Array();
	var _g1 = 0;
	var _g = s.length;
	while(_g1 < _g) {
		var i = _g1++;
		var c = s.charCodeAt(i);
		if(c <= 127) a.push(c); else if(c <= 2047) {
			a.push(192 | c >> 6);
			a.push(128 | c & 63);
		} else if(c <= 65535) {
			a.push(224 | c >> 12);
			a.push(128 | c >> 6 & 63);
			a.push(128 | c & 63);
		} else {
			a.push(240 | c >> 18);
			a.push(128 | c >> 12 & 63);
			a.push(128 | c >> 6 & 63);
			a.push(128 | c & 63);
		}
	}
	return new haxe.io.Bytes(a.length,a);
};
haxe.io.Bytes.ofData = function(b) {
	return new haxe.io.Bytes(b.length,b);
};
haxe.io.Bytes.prototype = {
	get: function(pos) {
		return this.b[pos];
	}
	,set: function(pos,v) {
		this.b[pos] = v & 255;
	}
	,readString: function(pos,len) {
		if(pos < 0 || len < 0 || pos + len > this.length) throw haxe.io.Error.OutsideBounds;
		var s = "";
		var b = this.b;
		var fcc = String.fromCharCode;
		var i = pos;
		var max = pos + len;
		while(i < max) {
			var c = b[i++];
			if(c < 128) {
				if(c == 0) break;
				s += fcc(c);
			} else if(c < 224) s += fcc((c & 63) << 6 | b[i++] & 127); else if(c < 240) {
				var c2 = b[i++];
				s += fcc((c & 31) << 12 | (c2 & 127) << 6 | b[i++] & 127);
			} else {
				var c21 = b[i++];
				var c3 = b[i++];
				s += fcc((c & 15) << 18 | (c21 & 127) << 12 | c3 << 6 & 127 | b[i++] & 127);
			}
		}
		return s;
	}
	,toString: function() {
		return this.readString(0,this.length);
	}
	,__class__: haxe.io.Bytes
};
haxe.io.Eof = function() { };
$hxClasses["haxe.io.Eof"] = haxe.io.Eof;
haxe.io.Eof.__name__ = ["haxe","io","Eof"];
haxe.io.Eof.prototype = {
	toString: function() {
		return "Eof";
	}
	,__class__: haxe.io.Eof
};
haxe.io.Error = $hxClasses["haxe.io.Error"] = { __ename__ : ["haxe","io","Error"], __constructs__ : ["Blocked","Overflow","OutsideBounds","Custom"] };
haxe.io.Error.Blocked = ["Blocked",0];
haxe.io.Error.Blocked.toString = $estr;
haxe.io.Error.Blocked.__enum__ = haxe.io.Error;
haxe.io.Error.Overflow = ["Overflow",1];
haxe.io.Error.Overflow.toString = $estr;
haxe.io.Error.Overflow.__enum__ = haxe.io.Error;
haxe.io.Error.OutsideBounds = ["OutsideBounds",2];
haxe.io.Error.OutsideBounds.toString = $estr;
haxe.io.Error.OutsideBounds.__enum__ = haxe.io.Error;
haxe.io.Error.Custom = function(e) { var $x = ["Custom",3,e]; $x.__enum__ = haxe.io.Error; $x.toString = $estr; return $x; };
haxe.io.Path = function(path) {
	var c1 = path.lastIndexOf("/");
	var c2 = path.lastIndexOf("\\");
	if(c1 < c2) {
		this.dir = HxOverrides.substr(path,0,c2);
		path = HxOverrides.substr(path,c2 + 1,null);
		this.backslash = true;
	} else if(c2 < c1) {
		this.dir = HxOverrides.substr(path,0,c1);
		path = HxOverrides.substr(path,c1 + 1,null);
	} else this.dir = null;
	var cp = path.lastIndexOf(".");
	if(cp != -1) {
		this.ext = HxOverrides.substr(path,cp + 1,null);
		this.file = HxOverrides.substr(path,0,cp);
	} else {
		this.ext = null;
		this.file = path;
	}
};
$hxClasses["haxe.io.Path"] = haxe.io.Path;
haxe.io.Path.__name__ = ["haxe","io","Path"];
haxe.io.Path.withoutExtension = function(path) {
	var s = new haxe.io.Path(path);
	s.ext = null;
	return s.toString();
};
haxe.io.Path.prototype = {
	toString: function() {
		return (this.dir == null?"":this.dir + (this.backslash?"\\":"/")) + this.file + (this.ext == null?"":"." + this.ext);
	}
	,__class__: haxe.io.Path
};
haxe.xml = {};
haxe.xml._Fast = {};
haxe.xml._Fast.NodeAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.NodeAccess"] = haxe.xml._Fast.NodeAccess;
haxe.xml._Fast.NodeAccess.__name__ = ["haxe","xml","_Fast","NodeAccess"];
haxe.xml._Fast.NodeAccess.prototype = {
	resolve: function(name) {
		var x = this.__x.elementsNamed(name).next();
		if(x == null) {
			var xname;
			if(this.__x.nodeType == Xml.Document) xname = "Document"; else xname = this.__x.get_nodeName();
			throw xname + " is missing element " + name;
		}
		return new haxe.xml.Fast(x);
	}
	,__class__: haxe.xml._Fast.NodeAccess
};
haxe.xml._Fast.AttribAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.AttribAccess"] = haxe.xml._Fast.AttribAccess;
haxe.xml._Fast.AttribAccess.__name__ = ["haxe","xml","_Fast","AttribAccess"];
haxe.xml._Fast.AttribAccess.prototype = {
	resolve: function(name) {
		if(this.__x.nodeType == Xml.Document) throw "Cannot access document attribute " + name;
		var v = this.__x.get(name);
		if(v == null) throw this.__x.get_nodeName() + " is missing attribute " + name;
		return v;
	}
	,__class__: haxe.xml._Fast.AttribAccess
};
haxe.xml._Fast.HasAttribAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.HasAttribAccess"] = haxe.xml._Fast.HasAttribAccess;
haxe.xml._Fast.HasAttribAccess.__name__ = ["haxe","xml","_Fast","HasAttribAccess"];
haxe.xml._Fast.HasAttribAccess.prototype = {
	resolve: function(name) {
		if(this.__x.nodeType == Xml.Document) throw "Cannot access document attribute " + name;
		return this.__x.exists(name);
	}
	,__class__: haxe.xml._Fast.HasAttribAccess
};
haxe.xml._Fast.HasNodeAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.HasNodeAccess"] = haxe.xml._Fast.HasNodeAccess;
haxe.xml._Fast.HasNodeAccess.__name__ = ["haxe","xml","_Fast","HasNodeAccess"];
haxe.xml._Fast.HasNodeAccess.prototype = {
	resolve: function(name) {
		return this.__x.elementsNamed(name).hasNext();
	}
	,__class__: haxe.xml._Fast.HasNodeAccess
};
haxe.xml._Fast.NodeListAccess = function(x) {
	this.__x = x;
};
$hxClasses["haxe.xml._Fast.NodeListAccess"] = haxe.xml._Fast.NodeListAccess;
haxe.xml._Fast.NodeListAccess.__name__ = ["haxe","xml","_Fast","NodeListAccess"];
haxe.xml._Fast.NodeListAccess.prototype = {
	resolve: function(name) {
		var l = new List();
		var $it0 = this.__x.elementsNamed(name);
		while( $it0.hasNext() ) {
			var x = $it0.next();
			l.add(new haxe.xml.Fast(x));
		}
		return l;
	}
	,__class__: haxe.xml._Fast.NodeListAccess
};
haxe.xml.Fast = function(x) {
	if(x.nodeType != Xml.Document && x.nodeType != Xml.Element) throw "Invalid nodeType " + Std.string(x.nodeType);
	this.x = x;
	this.node = new haxe.xml._Fast.NodeAccess(x);
	this.nodes = new haxe.xml._Fast.NodeListAccess(x);
	this.att = new haxe.xml._Fast.AttribAccess(x);
	this.has = new haxe.xml._Fast.HasAttribAccess(x);
	this.hasNode = new haxe.xml._Fast.HasNodeAccess(x);
};
$hxClasses["haxe.xml.Fast"] = haxe.xml.Fast;
haxe.xml.Fast.__name__ = ["haxe","xml","Fast"];
haxe.xml.Fast.prototype = {
	get_name: function() {
		if(this.x.nodeType == Xml.Document) return "Document"; else return this.x.get_nodeName();
	}
	,get_innerHTML: function() {
		var s = new StringBuf();
		var $it0 = this.x.iterator();
		while( $it0.hasNext() ) {
			var x = $it0.next();
			s.add(x.toString());
		}
		return s.b;
	}
	,get_elements: function() {
		var it = this.x.elements();
		return { hasNext : $bind(it,it.hasNext), next : function() {
			var x = it.next();
			if(x == null) return null;
			return new haxe.xml.Fast(x);
		}};
	}
	,__class__: haxe.xml.Fast
	,__properties__: {get_elements:"get_elements",get_innerHTML:"get_innerHTML",get_name:"get_name"}
};
haxe.xml.Parser = function() { };
$hxClasses["haxe.xml.Parser"] = haxe.xml.Parser;
haxe.xml.Parser.__name__ = ["haxe","xml","Parser"];
haxe.xml.Parser.parse = function(str) {
	var doc = Xml.createDocument();
	haxe.xml.Parser.doParse(str,0,doc);
	return doc;
};
haxe.xml.Parser.doParse = function(str,p,parent) {
	if(p == null) p = 0;
	var xml = null;
	var state = 1;
	var next = 1;
	var aname = null;
	var start = 0;
	var nsubs = 0;
	var nbrackets = 0;
	var c = str.charCodeAt(p);
	var buf = new StringBuf();
	while(!(c != c)) {
		switch(state) {
		case 0:
			switch(c) {
			case 10:case 13:case 9:case 32:
				break;
			default:
				state = next;
				continue;
			}
			break;
		case 1:
			switch(c) {
			case 60:
				state = 0;
				next = 2;
				break;
			default:
				start = p;
				state = 13;
				continue;
			}
			break;
		case 13:
			if(c == 60) {
				var child = Xml.createPCData(buf.b + HxOverrides.substr(str,start,p - start));
				buf = new StringBuf();
				parent.addChild(child);
				nsubs++;
				state = 0;
				next = 2;
			} else if(c == 38) {
				buf.addSub(str,start,p - start);
				state = 18;
				next = 13;
				start = p + 1;
			}
			break;
		case 17:
			if(c == 93 && str.charCodeAt(p + 1) == 93 && str.charCodeAt(p + 2) == 62) {
				var child1 = Xml.createCData(HxOverrides.substr(str,start,p - start));
				parent.addChild(child1);
				nsubs++;
				p += 2;
				state = 1;
			}
			break;
		case 2:
			switch(c) {
			case 33:
				if(str.charCodeAt(p + 1) == 91) {
					p += 2;
					if(HxOverrides.substr(str,p,6).toUpperCase() != "CDATA[") throw "Expected <![CDATA[";
					p += 5;
					state = 17;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) == 68 || str.charCodeAt(p + 1) == 100) {
					if(HxOverrides.substr(str,p + 2,6).toUpperCase() != "OCTYPE") throw "Expected <!DOCTYPE";
					p += 8;
					state = 16;
					start = p + 1;
				} else if(str.charCodeAt(p + 1) != 45 || str.charCodeAt(p + 2) != 45) throw "Expected <!--"; else {
					p += 2;
					state = 15;
					start = p + 1;
				}
				break;
			case 63:
				state = 14;
				start = p;
				break;
			case 47:
				if(parent == null) throw "Expected node name";
				start = p + 1;
				state = 0;
				next = 10;
				break;
			default:
				state = 3;
				start = p;
				continue;
			}
			break;
		case 3:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(p == start) throw "Expected node name";
				xml = Xml.createElement(HxOverrides.substr(str,start,p - start));
				parent.addChild(xml);
				state = 0;
				next = 4;
				continue;
			}
			break;
		case 4:
			switch(c) {
			case 47:
				state = 11;
				nsubs++;
				break;
			case 62:
				state = 9;
				nsubs++;
				break;
			default:
				state = 5;
				start = p;
				continue;
			}
			break;
		case 5:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				var tmp;
				if(start == p) throw "Expected attribute name";
				tmp = HxOverrides.substr(str,start,p - start);
				aname = tmp;
				if(xml.exists(aname)) throw "Duplicate attribute";
				state = 0;
				next = 6;
				continue;
			}
			break;
		case 6:
			switch(c) {
			case 61:
				state = 0;
				next = 7;
				break;
			default:
				throw "Expected =";
			}
			break;
		case 7:
			switch(c) {
			case 34:case 39:
				state = 8;
				start = p;
				break;
			default:
				throw "Expected \"";
			}
			break;
		case 8:
			if(c == str.charCodeAt(start)) {
				var val = HxOverrides.substr(str,start + 1,p - start - 1);
				xml.set(aname,val);
				state = 0;
				next = 4;
			}
			break;
		case 9:
			p = haxe.xml.Parser.doParse(str,p,xml);
			start = p;
			state = 1;
			break;
		case 11:
			switch(c) {
			case 62:
				state = 1;
				break;
			default:
				throw "Expected >";
			}
			break;
		case 12:
			switch(c) {
			case 62:
				if(nsubs == 0) parent.addChild(Xml.createPCData(""));
				return p;
			default:
				throw "Expected >";
			}
			break;
		case 10:
			if(!(c >= 97 && c <= 122 || c >= 65 && c <= 90 || c >= 48 && c <= 57 || c == 58 || c == 46 || c == 95 || c == 45)) {
				if(start == p) throw "Expected node name";
				var v = HxOverrides.substr(str,start,p - start);
				if(v != parent.get_nodeName()) throw "Expected </" + parent.get_nodeName() + ">";
				state = 0;
				next = 12;
				continue;
			}
			break;
		case 15:
			if(c == 45 && str.charCodeAt(p + 1) == 45 && str.charCodeAt(p + 2) == 62) {
				parent.addChild(Xml.createComment(HxOverrides.substr(str,start,p - start)));
				p += 2;
				state = 1;
			}
			break;
		case 16:
			if(c == 91) nbrackets++; else if(c == 93) nbrackets--; else if(c == 62 && nbrackets == 0) {
				parent.addChild(Xml.createDocType(HxOverrides.substr(str,start,p - start)));
				state = 1;
			}
			break;
		case 14:
			if(c == 63 && str.charCodeAt(p + 1) == 62) {
				p++;
				var str1 = HxOverrides.substr(str,start + 1,p - start - 2);
				parent.addChild(Xml.createProcessingInstruction(str1));
				state = 1;
			}
			break;
		case 18:
			if(c == 59) {
				var s = HxOverrides.substr(str,start,p - start);
				if(s.charCodeAt(0) == 35) {
					var i;
					if(s.charCodeAt(1) == 120) i = Std.parseInt("0" + HxOverrides.substr(s,1,s.length - 1)); else i = Std.parseInt(HxOverrides.substr(s,1,s.length - 1));
					buf.add(String.fromCharCode(i));
				} else if(!haxe.xml.Parser.escapes.exists(s)) buf.b += "&" + s + ";"; else buf.add(haxe.xml.Parser.escapes.get(s));
				start = p + 1;
				state = next;
			}
			break;
		}
		c = StringTools.fastCodeAt(str,++p);
	}
	if(state == 1) {
		start = p;
		state = 13;
	}
	if(state == 13) {
		if(p != start || nsubs == 0) parent.addChild(Xml.createPCData(buf.b + HxOverrides.substr(str,start,p - start)));
		return p;
	}
	throw "Unexpected end";
};
var js = {};
js.Boot = function() { };
$hxClasses["js.Boot"] = js.Boot;
js.Boot.__name__ = ["js","Boot"];
js.Boot.__unhtml = function(s) {
	return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
};
js.Boot.__trace = function(v,i) {
	var msg;
	if(i != null) msg = i.fileName + ":" + i.lineNumber + ": "; else msg = "";
	msg += js.Boot.__string_rec(v,"");
	if(i != null && i.customParams != null) {
		var _g = 0;
		var _g1 = i.customParams;
		while(_g < _g1.length) {
			var v1 = _g1[_g];
			++_g;
			msg += "," + js.Boot.__string_rec(v1,"");
		}
	}
	var d;
	if(typeof(document) != "undefined" && (d = document.getElementById("haxe:trace")) != null) d.innerHTML += js.Boot.__unhtml(msg) + "<br/>"; else if(typeof console != "undefined" && console.log != null) console.log(msg);
};
js.Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else return o.__class__;
};
js.Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i = _g1++;
					if(i != 2) str += "," + js.Boot.__string_rec(o[i],s); else str += js.Boot.__string_rec(o[i],s);
				}
				return str + ")";
			}
			var l = o.length;
			var i1;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js.Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			return "???";
		}
		if(tostr != null && tostr != Object.toString) {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str2 = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str2.length != 2) str2 += ", \n";
		str2 += s + k + " : " + js.Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str2 += "\n" + s + "}";
		return str2;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js.Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js.Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js.Boot.__interfLoop(cc.__super__,cl);
};
js.Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js.Boot.__interfLoop(js.Boot.getClass(o),cl)) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js.Boot.__cast = function(o,t) {
	if(js.Boot.__instanceof(o,t)) return o; else throw "Cannot cast " + Std.string(o) + " to " + Std.string(t);
};
js.Browser = function() { };
$hxClasses["js.Browser"] = js.Browser;
js.Browser.__name__ = ["js","Browser"];
js.Browser.getLocalStorage = function() {
	try {
		var s = window.localStorage;
		s.getItem("");
		return s;
	} catch( e ) {
		return null;
	}
};
var motion = {};
motion.actuators = {};
motion.actuators.IGenericActuator = function() { };
$hxClasses["motion.actuators.IGenericActuator"] = motion.actuators.IGenericActuator;
motion.actuators.IGenericActuator.__name__ = ["motion","actuators","IGenericActuator"];
motion.actuators.IGenericActuator.prototype = {
	__class__: motion.actuators.IGenericActuator
};
motion.actuators.GenericActuator = function(target,duration,properties) {
	this._autoVisible = true;
	this._delay = 0;
	this._reflect = false;
	this._repeat = 0;
	this._reverse = false;
	this._smartRotation = false;
	this._snapping = false;
	this.special = false;
	this.target = target;
	this.properties = properties;
	this.duration = duration;
	this._ease = motion.Actuate.defaultEase;
};
$hxClasses["motion.actuators.GenericActuator"] = motion.actuators.GenericActuator;
motion.actuators.GenericActuator.__name__ = ["motion","actuators","GenericActuator"];
motion.actuators.GenericActuator.__interfaces__ = [motion.actuators.IGenericActuator];
motion.actuators.GenericActuator.prototype = {
	apply: function() {
		var _g = 0;
		var _g1 = Reflect.fields(this.properties);
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			if(Object.prototype.hasOwnProperty.call(this.target,i)) Reflect.setField(this.target,i,Reflect.field(this.properties,i)); else Reflect.setProperty(this.target,i,Reflect.field(this.properties,i));
		}
	}
	,autoVisible: function(value) {
		if(value == null) value = true;
		this._autoVisible = value;
		return this;
	}
	,callMethod: function(method,params) {
		if(params == null) params = [];
		return method.apply(method,params);
	}
	,change: function() {
		if(this._onUpdate != null) this.callMethod(this._onUpdate,this._onUpdateParams);
	}
	,complete: function(sendEvent) {
		if(sendEvent == null) sendEvent = true;
		if(sendEvent) {
			this.change();
			if(this._onComplete != null) this.callMethod(this._onComplete,this._onCompleteParams);
		}
		motion.Actuate.unload(this);
	}
	,delay: function(duration) {
		this._delay = duration;
		return this;
	}
	,ease: function(easing) {
		this._ease = easing;
		return this;
	}
	,move: function() {
	}
	,onComplete: function(handler,parameters) {
		this._onComplete = handler;
		if(parameters == null) this._onCompleteParams = []; else this._onCompleteParams = parameters;
		if(this.duration == 0) this.complete();
		return this;
	}
	,onRepeat: function(handler,parameters) {
		this._onRepeat = handler;
		if(parameters == null) this._onRepeatParams = []; else this._onRepeatParams = parameters;
		return this;
	}
	,onUpdate: function(handler,parameters) {
		this._onUpdate = handler;
		if(parameters == null) this._onUpdateParams = []; else this._onUpdateParams = parameters;
		return this;
	}
	,pause: function() {
	}
	,reflect: function(value) {
		if(value == null) value = true;
		this._reflect = value;
		this.special = true;
		return this;
	}
	,repeat: function(times) {
		if(times == null) times = -1;
		this._repeat = times;
		return this;
	}
	,resume: function() {
	}
	,reverse: function(value) {
		if(value == null) value = true;
		this._reverse = value;
		this.special = true;
		return this;
	}
	,smartRotation: function(value) {
		if(value == null) value = true;
		this._smartRotation = value;
		this.special = true;
		return this;
	}
	,snapping: function(value) {
		if(value == null) value = true;
		this._snapping = value;
		this.special = true;
		return this;
	}
	,stop: function(properties,complete,sendEvent) {
	}
	,__class__: motion.actuators.GenericActuator
};
motion.actuators.SimpleActuator = function(target,duration,properties) {
	this.active = true;
	this.propertyDetails = new Array();
	this.sendChange = false;
	this.paused = false;
	this.cacheVisible = false;
	this.initialized = false;
	this.setVisible = false;
	this.toggleVisible = false;
	this.startTime = openfl.Lib.getTimer() / 1000;
	motion.actuators.GenericActuator.call(this,target,duration,properties);
	if(!motion.actuators.SimpleActuator.addedEvent) {
		motion.actuators.SimpleActuator.addedEvent = true;
		openfl.Lib.current.stage.addEventListener(openfl.events.Event.ENTER_FRAME,motion.actuators.SimpleActuator.stage_onEnterFrame);
	}
};
$hxClasses["motion.actuators.SimpleActuator"] = motion.actuators.SimpleActuator;
motion.actuators.SimpleActuator.__name__ = ["motion","actuators","SimpleActuator"];
motion.actuators.SimpleActuator.stage_onEnterFrame = function(event) {
	var currentTime = openfl.Lib.getTimer() / 1000;
	var actuator;
	var j = 0;
	var cleanup = false;
	var _g1 = 0;
	var _g = motion.actuators.SimpleActuator.actuatorsLength;
	while(_g1 < _g) {
		var i = _g1++;
		actuator = motion.actuators.SimpleActuator.actuators[j];
		if(actuator != null && actuator.active) {
			if(currentTime > actuator.timeOffset) actuator.update(currentTime);
			j++;
		} else {
			motion.actuators.SimpleActuator.actuators.splice(j,1);
			--motion.actuators.SimpleActuator.actuatorsLength;
		}
	}
};
motion.actuators.SimpleActuator.__super__ = motion.actuators.GenericActuator;
motion.actuators.SimpleActuator.prototype = $extend(motion.actuators.GenericActuator.prototype,{
	autoVisible: function(value) {
		if(value == null) value = true;
		this._autoVisible = value;
		if(!value) {
			this.toggleVisible = false;
			if(this.setVisible) this.setField(this.target,"visible",this.cacheVisible);
		}
		return this;
	}
	,delay: function(duration) {
		this._delay = duration;
		this.timeOffset = this.startTime + duration;
		return this;
	}
	,getField: function(target,propertyName) {
		var value = null;
		if(Object.prototype.hasOwnProperty.call(target,propertyName)) value = Reflect.field(target,propertyName); else value = Reflect.getProperty(target,propertyName);
		return value;
	}
	,initialize: function() {
		var details;
		var start;
		var _g = 0;
		var _g1 = Reflect.fields(this.properties);
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			var isField = true;
			if(Object.prototype.hasOwnProperty.call(this.target,i) && (!this.target.__properties__ || !this.target.__properties__["set_" + i])) start = Reflect.field(this.target,i); else {
				isField = false;
				start = Reflect.getProperty(this.target,i);
			}
			if(typeof(start) == "number") {
				details = new motion.actuators.PropertyDetails(this.target,i,start,this.getField(this.properties,i) - start,isField);
				this.propertyDetails.push(details);
			}
		}
		this.detailsLength = this.propertyDetails.length;
		this.initialized = true;
	}
	,move: function() {
		this.toggleVisible = Object.prototype.hasOwnProperty.call(this.properties,"alpha") && js.Boot.__instanceof(this.target,openfl.display.DisplayObject);
		if(this.toggleVisible && this.properties.alpha != 0 && !this.getField(this.target,"visible")) {
			this.setVisible = true;
			this.cacheVisible = this.getField(this.target,"visible");
			this.setField(this.target,"visible",true);
		}
		this.timeOffset = this.startTime;
		motion.actuators.SimpleActuator.actuators.push(this);
		++motion.actuators.SimpleActuator.actuatorsLength;
	}
	,onUpdate: function(handler,parameters) {
		this._onUpdate = handler;
		if(parameters == null) this._onUpdateParams = []; else this._onUpdateParams = parameters;
		this.sendChange = true;
		return this;
	}
	,pause: function() {
		this.paused = true;
		this.pauseTime = openfl.Lib.getTimer();
	}
	,resume: function() {
		if(this.paused) {
			this.paused = false;
			this.timeOffset += (openfl.Lib.getTimer() - this.pauseTime) / 1000;
		}
	}
	,setField: function(target,propertyName,value) {
		if(Object.prototype.hasOwnProperty.call(target,propertyName)) target[propertyName] = value; else Reflect.setProperty(target,propertyName,value);
	}
	,setProperty: function(details,value) {
		if(details.isField) details.target[details.propertyName] = value; else Reflect.setProperty(details.target,details.propertyName,value);
	}
	,stop: function(properties,complete,sendEvent) {
		if(this.active) {
			if(properties == null) {
				this.active = false;
				if(complete) this.apply();
				this.complete(sendEvent);
				return;
			}
			var _g = 0;
			var _g1 = Reflect.fields(properties);
			while(_g < _g1.length) {
				var i = _g1[_g];
				++_g;
				if(Object.prototype.hasOwnProperty.call(this.properties,i)) {
					this.active = false;
					if(complete) this.apply();
					this.complete(sendEvent);
					return;
				}
			}
		}
	}
	,update: function(currentTime) {
		if(!this.paused) {
			var details;
			var easing;
			var i;
			var tweenPosition = (currentTime - this.timeOffset) / this.duration;
			if(tweenPosition > 1) tweenPosition = 1;
			if(!this.initialized) this.initialize();
			if(!this.special) {
				easing = this._ease.calculate(tweenPosition);
				var _g1 = 0;
				var _g = this.detailsLength;
				while(_g1 < _g) {
					var i1 = _g1++;
					details = this.propertyDetails[i1];
					this.setProperty(details,details.start + details.change * easing);
				}
			} else {
				if(!this._reverse) easing = this._ease.calculate(tweenPosition); else easing = this._ease.calculate(1 - tweenPosition);
				var endValue;
				var _g11 = 0;
				var _g2 = this.detailsLength;
				while(_g11 < _g2) {
					var i2 = _g11++;
					details = this.propertyDetails[i2];
					if(this._smartRotation && (details.propertyName == "rotation" || details.propertyName == "rotationX" || details.propertyName == "rotationY" || details.propertyName == "rotationZ")) {
						var rotation = details.change % 360;
						if(rotation > 180) rotation -= 360; else if(rotation < -180) rotation += 360;
						endValue = details.start + rotation * easing;
					} else endValue = details.start + details.change * easing;
					if(!this._snapping) {
						if(details.isField) details.target[details.propertyName] = endValue; else Reflect.setProperty(details.target,details.propertyName,endValue);
					} else this.setProperty(details,Math.round(endValue));
				}
			}
			if(tweenPosition == 1) {
				if(this._repeat == 0) {
					this.active = false;
					if(this.toggleVisible && this.getField(this.target,"alpha") == 0) this.setField(this.target,"visible",false);
					this.complete(true);
					return;
				} else {
					if(this._onRepeat != null) this.callMethod(this._onRepeat,this._onRepeatParams);
					if(this._reflect) this._reverse = !this._reverse;
					this.startTime = currentTime;
					this.timeOffset = this.startTime + this._delay;
					if(this._repeat > 0) this._repeat--;
				}
			}
			if(this.sendChange) this.change();
		}
	}
	,__class__: motion.actuators.SimpleActuator
});
motion.easing = {};
motion.easing.Expo = function() { };
$hxClasses["motion.easing.Expo"] = motion.easing.Expo;
motion.easing.Expo.__name__ = ["motion","easing","Expo"];
motion.easing.Expo.__properties__ = {get_easeOut:"get_easeOut",get_easeInOut:"get_easeInOut",get_easeIn:"get_easeIn"}
motion.easing.Expo.get_easeIn = function() {
	return new motion.easing.ExpoEaseIn();
};
motion.easing.Expo.get_easeInOut = function() {
	return new motion.easing.ExpoEaseInOut();
};
motion.easing.Expo.get_easeOut = function() {
	return new motion.easing.ExpoEaseOut();
};
motion.easing.IEasing = function() { };
$hxClasses["motion.easing.IEasing"] = motion.easing.IEasing;
motion.easing.IEasing.__name__ = ["motion","easing","IEasing"];
motion.easing.IEasing.prototype = {
	__class__: motion.easing.IEasing
};
motion.easing.ExpoEaseOut = function() {
};
$hxClasses["motion.easing.ExpoEaseOut"] = motion.easing.ExpoEaseOut;
motion.easing.ExpoEaseOut.__name__ = ["motion","easing","ExpoEaseOut"];
motion.easing.ExpoEaseOut.__interfaces__ = [motion.easing.IEasing];
motion.easing.ExpoEaseOut.prototype = {
	calculate: function(k) {
		if(k == 1) return 1; else return 1 - Math.pow(2,-10 * k);
	}
	,ease: function(t,b,c,d) {
		if(t == d) return b + c; else return c * (1 - Math.pow(2,-10 * t / d)) + b;
	}
	,__class__: motion.easing.ExpoEaseOut
};
motion.Actuate = function() { };
$hxClasses["motion.Actuate"] = motion.Actuate;
motion.Actuate.__name__ = ["motion","Actuate"];
motion.Actuate.apply = function(target,properties,customActuator) {
	motion.Actuate.stop(target,properties);
	if(customActuator == null) customActuator = motion.Actuate.defaultActuator;
	var actuator = Type.createInstance(customActuator,[target,0,properties]);
	actuator.apply();
	return actuator;
};
motion.Actuate.effects = function(target,duration,overwrite) {
	if(overwrite == null) overwrite = true;
	return new motion._Actuate.EffectsOptions(target,duration,overwrite);
};
motion.Actuate.getLibrary = function(target,allowCreation) {
	if(allowCreation == null) allowCreation = true;
	if(!motion.Actuate.targetLibraries.exists(target) && allowCreation) motion.Actuate.targetLibraries.set(target,new Array());
	return motion.Actuate.targetLibraries.get(target);
};
motion.Actuate.motionPath = function(target,duration,properties,overwrite) {
	if(overwrite == null) overwrite = true;
	return motion.Actuate.tween(target,duration,properties,overwrite,motion.actuators.MotionPathActuator);
};
motion.Actuate.pause = function(target) {
	if(js.Boot.__instanceof(target,motion.actuators.GenericActuator)) (js.Boot.__cast(target , motion.actuators.GenericActuator)).pause(); else {
		var library = motion.Actuate.getLibrary(target,false);
		if(library != null) {
			var _g = 0;
			while(_g < library.length) {
				var actuator = library[_g];
				++_g;
				actuator.pause();
			}
		}
	}
};
motion.Actuate.pauseAll = function() {
	var $it0 = motion.Actuate.targetLibraries.iterator();
	while( $it0.hasNext() ) {
		var library = $it0.next();
		var _g = 0;
		while(_g < library.length) {
			var actuator = library[_g];
			++_g;
			actuator.pause();
		}
	}
};
motion.Actuate.reset = function() {
	var $it0 = motion.Actuate.targetLibraries.iterator();
	while( $it0.hasNext() ) {
		var library = $it0.next();
		var i = library.length - 1;
		while(i >= 0) {
			library[i].stop(null,false,false);
			i--;
		}
	}
	motion.Actuate.targetLibraries = new haxe.ds.ObjectMap();
};
motion.Actuate.resume = function(target) {
	if(js.Boot.__instanceof(target,motion.actuators.GenericActuator)) (js.Boot.__cast(target , motion.actuators.GenericActuator)).resume(); else {
		var library = motion.Actuate.getLibrary(target,false);
		if(library != null) {
			var _g = 0;
			while(_g < library.length) {
				var actuator = library[_g];
				++_g;
				actuator.resume();
			}
		}
	}
};
motion.Actuate.resumeAll = function() {
	var $it0 = motion.Actuate.targetLibraries.iterator();
	while( $it0.hasNext() ) {
		var library = $it0.next();
		var _g = 0;
		while(_g < library.length) {
			var actuator = library[_g];
			++_g;
			actuator.resume();
		}
	}
};
motion.Actuate.stop = function(target,properties,complete,sendEvent) {
	if(sendEvent == null) sendEvent = true;
	if(complete == null) complete = false;
	if(target != null) {
		if(js.Boot.__instanceof(target,motion.actuators.GenericActuator)) (js.Boot.__cast(target , motion.actuators.GenericActuator)).stop(null,complete,sendEvent); else {
			var library = motion.Actuate.getLibrary(target,false);
			if(library != null) {
				if(typeof(properties) == "string") {
					var temp = { };
					Reflect.setField(temp,properties,null);
					properties = temp;
				} else if((properties instanceof Array) && properties.__enum__ == null) {
					var temp1 = { };
					var _g = 0;
					var _g1;
					_g1 = js.Boot.__cast(properties , Array);
					while(_g < _g1.length) {
						var property = _g1[_g];
						++_g;
						Reflect.setField(temp1,property,null);
					}
					properties = temp1;
				}
				var i = library.length - 1;
				while(i >= 0) {
					library[i].stop(properties,complete,sendEvent);
					i--;
				}
			}
		}
	}
};
motion.Actuate.timer = function(duration,customActuator) {
	return motion.Actuate.tween(new motion._Actuate.TweenTimer(0),duration,new motion._Actuate.TweenTimer(1),false,customActuator);
};
motion.Actuate.transform = function(target,duration,overwrite) {
	if(overwrite == null) overwrite = true;
	if(duration == null) duration = 0;
	return new motion._Actuate.TransformOptions(target,duration,overwrite);
};
motion.Actuate.tween = function(target,duration,properties,overwrite,customActuator) {
	if(overwrite == null) overwrite = true;
	if(target != null) {
		if(duration > 0) {
			if(customActuator == null) customActuator = motion.Actuate.defaultActuator;
			var actuator = Type.createInstance(customActuator,[target,duration,properties]);
			var library = motion.Actuate.getLibrary(actuator.target);
			if(overwrite) {
				var i = library.length - 1;
				while(i >= 0) {
					library[i].stop(actuator.properties,false,false);
					i--;
				}
				library = motion.Actuate.getLibrary(actuator.target);
			}
			library.push(actuator);
			actuator.move();
			return actuator;
		} else return motion.Actuate.apply(target,properties,customActuator);
	}
	return null;
};
motion.Actuate.unload = function(actuator) {
	var target = actuator.target;
	if(motion.Actuate.targetLibraries.h.__keys__[target.__id__] != null) {
		HxOverrides.remove(motion.Actuate.targetLibraries.h[target.__id__],actuator);
		if(motion.Actuate.targetLibraries.h[target.__id__].length == 0) motion.Actuate.targetLibraries.remove(target);
	}
};
motion.Actuate.update = function(target,duration,start,end,overwrite) {
	if(overwrite == null) overwrite = true;
	var properties = { start : start, end : end};
	return motion.Actuate.tween(target,duration,properties,overwrite,motion.actuators.MethodActuator);
};
motion._Actuate = {};
motion._Actuate.EffectsOptions = function(target,duration,overwrite) {
	this.target = target;
	this.duration = duration;
	this.overwrite = overwrite;
};
$hxClasses["motion._Actuate.EffectsOptions"] = motion._Actuate.EffectsOptions;
motion._Actuate.EffectsOptions.__name__ = ["motion","_Actuate","EffectsOptions"];
motion._Actuate.EffectsOptions.prototype = {
	filter: function(reference,properties) {
		properties.filter = reference;
		return motion.Actuate.tween(this.target,this.duration,properties,this.overwrite,motion.actuators.FilterActuator);
	}
	,__class__: motion._Actuate.EffectsOptions
};
motion._Actuate.TransformOptions = function(target,duration,overwrite) {
	this.target = target;
	this.duration = duration;
	this.overwrite = overwrite;
};
$hxClasses["motion._Actuate.TransformOptions"] = motion._Actuate.TransformOptions;
motion._Actuate.TransformOptions.__name__ = ["motion","_Actuate","TransformOptions"];
motion._Actuate.TransformOptions.prototype = {
	color: function(value,strength,alpha) {
		if(strength == null) strength = 1;
		if(value == null) value = 0;
		var properties = { colorValue : value, colorStrength : strength};
		if(alpha != null) properties.colorAlpha = alpha;
		return motion.Actuate.tween(this.target,this.duration,properties,this.overwrite,motion.actuators.TransformActuator);
	}
	,sound: function(volume,pan) {
		var properties = { };
		if(volume != null) properties.soundVolume = volume;
		if(pan != null) properties.soundPan = pan;
		return motion.Actuate.tween(this.target,this.duration,properties,this.overwrite,motion.actuators.TransformActuator);
	}
	,__class__: motion._Actuate.TransformOptions
};
motion._Actuate.TweenTimer = function(progress) {
	this.progress = progress;
};
$hxClasses["motion._Actuate.TweenTimer"] = motion._Actuate.TweenTimer;
motion._Actuate.TweenTimer.__name__ = ["motion","_Actuate","TweenTimer"];
motion._Actuate.TweenTimer.prototype = {
	__class__: motion._Actuate.TweenTimer
};
motion.MotionPath = function() {
	this._x = new motion.ComponentPath();
	this._y = new motion.ComponentPath();
	this._rotation = null;
};
$hxClasses["motion.MotionPath"] = motion.MotionPath;
motion.MotionPath.__name__ = ["motion","MotionPath"];
motion.MotionPath.prototype = {
	bezier: function(x,y,controlX,controlY,strength) {
		if(strength == null) strength = 1;
		this._x.addPath(new motion.BezierPath(x,controlX,strength));
		this._y.addPath(new motion.BezierPath(y,controlY,strength));
		return this;
	}
	,line: function(x,y,strength) {
		if(strength == null) strength = 1;
		this._x.addPath(new motion.LinearPath(x,strength));
		this._y.addPath(new motion.LinearPath(y,strength));
		return this;
	}
	,get_rotation: function() {
		if(this._rotation == null) this._rotation = new motion.RotationPath(this._x,this._y);
		return this._rotation;
	}
	,get_x: function() {
		return this._x;
	}
	,get_y: function() {
		return this._y;
	}
	,__class__: motion.MotionPath
	,__properties__: {get_y:"get_y",get_x:"get_x",get_rotation:"get_rotation"}
};
motion.IComponentPath = function() { };
$hxClasses["motion.IComponentPath"] = motion.IComponentPath;
motion.IComponentPath.__name__ = ["motion","IComponentPath"];
motion.IComponentPath.prototype = {
	__class__: motion.IComponentPath
};
motion.ComponentPath = function() {
	this.paths = new Array();
	this.start = 0;
	this.totalStrength = 0;
};
$hxClasses["motion.ComponentPath"] = motion.ComponentPath;
motion.ComponentPath.__name__ = ["motion","ComponentPath"];
motion.ComponentPath.__interfaces__ = [motion.IComponentPath];
motion.ComponentPath.prototype = {
	addPath: function(path) {
		this.paths.push(path);
		this.totalStrength += path.strength;
	}
	,calculate: function(k) {
		if(this.paths.length == 1) return this.paths[0].calculate(this.start,k); else {
			var ratio = k * this.totalStrength;
			var lastEnd = this.start;
			var _g = 0;
			var _g1 = this.paths;
			while(_g < _g1.length) {
				var path = _g1[_g];
				++_g;
				if(ratio > path.strength) {
					ratio -= path.strength;
					lastEnd = path.end;
				} else return path.calculate(lastEnd,ratio / path.strength);
			}
		}
		return 0;
	}
	,get_end: function() {
		if(this.paths.length > 0) {
			var path = this.paths[this.paths.length - 1];
			return path.end;
		} else return this.start;
	}
	,__class__: motion.ComponentPath
	,__properties__: {get_end:"get_end"}
};
motion.BezierPath = function(end,control,strength) {
	this.end = end;
	this.control = control;
	this.strength = strength;
};
$hxClasses["motion.BezierPath"] = motion.BezierPath;
motion.BezierPath.__name__ = ["motion","BezierPath"];
motion.BezierPath.prototype = {
	calculate: function(start,k) {
		return (1 - k) * (1 - k) * start + 2 * (1 - k) * k * this.control + k * k * this.end;
	}
	,__class__: motion.BezierPath
};
motion.LinearPath = function(end,strength) {
	motion.BezierPath.call(this,end,0,strength);
};
$hxClasses["motion.LinearPath"] = motion.LinearPath;
motion.LinearPath.__name__ = ["motion","LinearPath"];
motion.LinearPath.__super__ = motion.BezierPath;
motion.LinearPath.prototype = $extend(motion.BezierPath.prototype,{
	calculate: function(start,k) {
		return start + k * (this.end - start);
	}
	,__class__: motion.LinearPath
});
motion.RotationPath = function(x,y) {
	this.step = 0.01;
	this._x = x;
	this._y = y;
	this.offset = 0;
	this.start = this.calculate(0.0);
};
$hxClasses["motion.RotationPath"] = motion.RotationPath;
motion.RotationPath.__name__ = ["motion","RotationPath"];
motion.RotationPath.__interfaces__ = [motion.IComponentPath];
motion.RotationPath.prototype = {
	calculate: function(k) {
		var dX = this._x.calculate(k) - this._x.calculate(k + this.step);
		var dY = this._y.calculate(k) - this._y.calculate(k + this.step);
		var angle = Math.atan2(dY,dX) * (180 / Math.PI);
		angle = (angle + this.offset) % 360;
		return angle;
	}
	,get_end: function() {
		return this.calculate(1.0);
	}
	,__class__: motion.RotationPath
	,__properties__: {get_end:"get_end"}
};
motion.actuators.FilterActuator = function(target,duration,properties) {
	this.filterIndex = -1;
	motion.actuators.SimpleActuator.call(this,target,duration,properties);
	if(js.Boot.__instanceof(properties.filter,Class)) {
		this.filterClass = properties.filter;
		var _g = 0;
		var _g1 = (js.Boot.__cast(target , openfl.display.DisplayObject)).get_filters();
		while(_g < _g1.length) {
			var filter = _g1[_g];
			++_g;
			if(js.Boot.__instanceof(filter,this.filterClass)) this.filter = filter;
		}
	} else {
		this.filterIndex = properties.filter;
		this.filter = (js.Boot.__cast(target , openfl.display.DisplayObject)).get_filters()[this.filterIndex];
	}
};
$hxClasses["motion.actuators.FilterActuator"] = motion.actuators.FilterActuator;
motion.actuators.FilterActuator.__name__ = ["motion","actuators","FilterActuator"];
motion.actuators.FilterActuator.__super__ = motion.actuators.SimpleActuator;
motion.actuators.FilterActuator.prototype = $extend(motion.actuators.SimpleActuator.prototype,{
	apply: function() {
		var _g = 0;
		var _g1 = Reflect.fields(this.properties);
		while(_g < _g1.length) {
			var propertyName = _g1[_g];
			++_g;
			if(propertyName != "filter") Reflect.setField(this.filter,propertyName,Reflect.field(this.properties,propertyName));
		}
		var filters = this.getField(this.target,"filters");
		Reflect.setField(filters,this.properties.filter,this.filter);
		this.setField(this.target,"filters",filters);
	}
	,initialize: function() {
		var details;
		var start;
		var _g = 0;
		var _g1 = Reflect.fields(this.properties);
		while(_g < _g1.length) {
			var propertyName = _g1[_g];
			++_g;
			if(propertyName != "filter") {
				start = this.getField(this.filter,propertyName);
				details = new motion.actuators.PropertyDetails(this.filter,propertyName,start,Reflect.field(this.properties,propertyName) - start);
				this.propertyDetails.push(details);
			}
		}
		this.detailsLength = this.propertyDetails.length;
		this.initialized = true;
	}
	,update: function(currentTime) {
		motion.actuators.SimpleActuator.prototype.update.call(this,currentTime);
		var filters = (js.Boot.__cast(this.target , openfl.display.DisplayObject)).get_filters();
		if(this.filterIndex > -1) Reflect.setField(filters,this.properties.filter,this.filter); else {
			var _g1 = 0;
			var _g = filters.length;
			while(_g1 < _g) {
				var i = _g1++;
				if(js.Boot.__instanceof(filters[i],this.filterClass)) filters[i] = this.filter;
			}
		}
		this.setField(this.target,"filters",filters);
	}
	,__class__: motion.actuators.FilterActuator
});
motion.actuators.MethodActuator = function(target,duration,properties) {
	this.currentParameters = new Array();
	this.tweenProperties = { };
	motion.actuators.SimpleActuator.call(this,target,duration,properties);
	if(!Object.prototype.hasOwnProperty.call(properties,"start")) this.properties.start = new Array();
	if(!Object.prototype.hasOwnProperty.call(properties,"end")) this.properties.end = this.properties.start;
	var _g1 = 0;
	var _g = this.properties.start.length;
	while(_g1 < _g) {
		var i = _g1++;
		this.currentParameters.push(null);
	}
};
$hxClasses["motion.actuators.MethodActuator"] = motion.actuators.MethodActuator;
motion.actuators.MethodActuator.__name__ = ["motion","actuators","MethodActuator"];
motion.actuators.MethodActuator.__super__ = motion.actuators.SimpleActuator;
motion.actuators.MethodActuator.prototype = $extend(motion.actuators.SimpleActuator.prototype,{
	apply: function() {
		this.callMethod(this.target,this.properties.end);
	}
	,complete: function(sendEvent) {
		if(sendEvent == null) sendEvent = true;
		var _g1 = 0;
		var _g = this.properties.start.length;
		while(_g1 < _g) {
			var i = _g1++;
			this.currentParameters[i] = Reflect.field(this.tweenProperties,"param" + i);
		}
		this.callMethod(this.target,this.currentParameters);
		motion.actuators.SimpleActuator.prototype.complete.call(this,sendEvent);
	}
	,initialize: function() {
		var details;
		var propertyName;
		var start;
		var _g1 = 0;
		var _g = this.properties.start.length;
		while(_g1 < _g) {
			var i = _g1++;
			propertyName = "param" + i;
			start = this.properties.start[i];
			this.tweenProperties[propertyName] = start;
			if(typeof(start) == "number" || ((start | 0) === start)) {
				details = new motion.actuators.PropertyDetails(this.tweenProperties,propertyName,start,this.properties.end[i] - start);
				this.propertyDetails.push(details);
			}
		}
		this.detailsLength = this.propertyDetails.length;
		this.initialized = true;
	}
	,update: function(currentTime) {
		motion.actuators.SimpleActuator.prototype.update.call(this,currentTime);
		if(this.active) {
			var _g1 = 0;
			var _g = this.properties.start.length;
			while(_g1 < _g) {
				var i = _g1++;
				this.currentParameters[i] = Reflect.field(this.tweenProperties,"param" + i);
			}
			this.callMethod(this.target,this.currentParameters);
		}
	}
	,__class__: motion.actuators.MethodActuator
});
motion.actuators.MotionPathActuator = function(target,duration,properties) {
	motion.actuators.SimpleActuator.call(this,target,duration,properties);
};
$hxClasses["motion.actuators.MotionPathActuator"] = motion.actuators.MotionPathActuator;
motion.actuators.MotionPathActuator.__name__ = ["motion","actuators","MotionPathActuator"];
motion.actuators.MotionPathActuator.__super__ = motion.actuators.SimpleActuator;
motion.actuators.MotionPathActuator.prototype = $extend(motion.actuators.SimpleActuator.prototype,{
	apply: function() {
		var _g = 0;
		var _g1 = Reflect.fields(this.properties);
		while(_g < _g1.length) {
			var propertyName = _g1[_g];
			++_g;
			if(Object.prototype.hasOwnProperty.call(this.target,propertyName)) Reflect.setField(this.target,propertyName,(js.Boot.__cast(Reflect.field(this.properties,propertyName) , motion.IComponentPath)).get_end()); else Reflect.setProperty(this.target,propertyName,(js.Boot.__cast(Reflect.field(this.properties,propertyName) , motion.IComponentPath)).get_end());
		}
	}
	,initialize: function() {
		var details;
		var path;
		var _g = 0;
		var _g1 = Reflect.fields(this.properties);
		while(_g < _g1.length) {
			var propertyName = _g1[_g];
			++_g;
			path = js.Boot.__cast(Reflect.field(this.properties,propertyName) , motion.IComponentPath);
			if(path != null) {
				var isField = true;
				if(Object.prototype.hasOwnProperty.call(this.target,propertyName)) path.start = Reflect.field(this.target,propertyName); else {
					isField = false;
					path.start = Reflect.getProperty(this.target,propertyName);
				}
				details = new motion.actuators.PropertyPathDetails(this.target,propertyName,path,isField);
				this.propertyDetails.push(details);
			}
		}
		this.detailsLength = this.propertyDetails.length;
		this.initialized = true;
	}
	,update: function(currentTime) {
		if(!this.paused) {
			var details;
			var easing;
			var tweenPosition = (currentTime - this.timeOffset) / this.duration;
			if(tweenPosition > 1) tweenPosition = 1;
			if(!this.initialized) this.initialize();
			if(!this.special) {
				easing = this._ease.calculate(tweenPosition);
				var _g = 0;
				var _g1 = this.propertyDetails;
				while(_g < _g1.length) {
					var details1 = _g1[_g];
					++_g;
					if(details1.isField) Reflect.setField(details1.target,details1.propertyName,(js.Boot.__cast(details1 , motion.actuators.PropertyPathDetails)).path.calculate(easing)); else Reflect.setProperty(details1.target,details1.propertyName,(js.Boot.__cast(details1 , motion.actuators.PropertyPathDetails)).path.calculate(easing));
				}
			} else {
				if(!this._reverse) easing = this._ease.calculate(tweenPosition); else easing = this._ease.calculate(1 - tweenPosition);
				var endValue;
				var _g2 = 0;
				var _g11 = this.propertyDetails;
				while(_g2 < _g11.length) {
					var details2 = _g11[_g2];
					++_g2;
					if(!this._snapping) {
						if(details2.isField) Reflect.setField(details2.target,details2.propertyName,(js.Boot.__cast(details2 , motion.actuators.PropertyPathDetails)).path.calculate(easing)); else Reflect.setProperty(details2.target,details2.propertyName,(js.Boot.__cast(details2 , motion.actuators.PropertyPathDetails)).path.calculate(easing));
					} else if(details2.isField) Reflect.setField(details2.target,details2.propertyName,Math.round((js.Boot.__cast(details2 , motion.actuators.PropertyPathDetails)).path.calculate(easing))); else Reflect.setProperty(details2.target,details2.propertyName,Math.round((js.Boot.__cast(details2 , motion.actuators.PropertyPathDetails)).path.calculate(easing)));
				}
			}
			if(tweenPosition == 1) {
				if(this._repeat == 0) {
					this.active = false;
					if(this.toggleVisible && this.getField(this.target,"alpha") == 0) this.setField(this.target,"visible",false);
					this.complete(true);
					return;
				} else {
					if(this._reflect) this._reverse = !this._reverse;
					this.startTime = currentTime;
					this.timeOffset = this.startTime + this._delay;
					if(this._repeat > 0) this._repeat--;
				}
			}
			if(this.sendChange) this.change();
		}
	}
	,__class__: motion.actuators.MotionPathActuator
});
motion.actuators.PropertyDetails = function(target,propertyName,start,change,isField) {
	if(isField == null) isField = true;
	this.target = target;
	this.propertyName = propertyName;
	this.start = start;
	this.change = change;
	this.isField = isField;
};
$hxClasses["motion.actuators.PropertyDetails"] = motion.actuators.PropertyDetails;
motion.actuators.PropertyDetails.__name__ = ["motion","actuators","PropertyDetails"];
motion.actuators.PropertyDetails.prototype = {
	__class__: motion.actuators.PropertyDetails
};
motion.actuators.PropertyPathDetails = function(target,propertyName,path,isField) {
	if(isField == null) isField = true;
	motion.actuators.PropertyDetails.call(this,target,propertyName,0,0,isField);
	this.path = path;
};
$hxClasses["motion.actuators.PropertyPathDetails"] = motion.actuators.PropertyPathDetails;
motion.actuators.PropertyPathDetails.__name__ = ["motion","actuators","PropertyPathDetails"];
motion.actuators.PropertyPathDetails.__super__ = motion.actuators.PropertyDetails;
motion.actuators.PropertyPathDetails.prototype = $extend(motion.actuators.PropertyDetails.prototype,{
	__class__: motion.actuators.PropertyPathDetails
});
motion.actuators.TransformActuator = function(target,duration,properties) {
	motion.actuators.SimpleActuator.call(this,target,duration,properties);
};
$hxClasses["motion.actuators.TransformActuator"] = motion.actuators.TransformActuator;
motion.actuators.TransformActuator.__name__ = ["motion","actuators","TransformActuator"];
motion.actuators.TransformActuator.__super__ = motion.actuators.SimpleActuator;
motion.actuators.TransformActuator.prototype = $extend(motion.actuators.SimpleActuator.prototype,{
	apply: function() {
		this.initialize();
		if(this.endColorTransform != null) {
			var transform = this.getField(this.target,"transform");
			this.setField(transform,"colorTransform",this.endColorTransform);
		}
		if(this.endSoundTransform != null) this.setField(this.target,"soundTransform",this.endSoundTransform);
	}
	,initialize: function() {
		if(Object.prototype.hasOwnProperty.call(this.properties,"colorValue") && js.Boot.__instanceof(this.target,openfl.display.DisplayObject)) this.initializeColor();
		if(Object.prototype.hasOwnProperty.call(this.properties,"soundVolume") || Object.prototype.hasOwnProperty.call(this.properties,"soundPan")) this.initializeSound();
		this.detailsLength = this.propertyDetails.length;
		this.initialized = true;
	}
	,initializeColor: function() {
		this.endColorTransform = new openfl.geom.ColorTransform();
		var color = this.properties.colorValue;
		var strength = this.properties.colorStrength;
		if(strength < 1) {
			var multiplier;
			var offset;
			if(strength < 0.5) {
				multiplier = 1;
				offset = strength * 2;
			} else {
				multiplier = 1 - (strength - 0.5) * 2;
				offset = 1;
			}
			this.endColorTransform.redMultiplier = multiplier;
			this.endColorTransform.greenMultiplier = multiplier;
			this.endColorTransform.blueMultiplier = multiplier;
			this.endColorTransform.redOffset = offset * (color >> 16 & 255);
			this.endColorTransform.greenOffset = offset * (color >> 8 & 255);
			this.endColorTransform.blueOffset = offset * (color & 255);
		} else {
			this.endColorTransform.redMultiplier = 0;
			this.endColorTransform.greenMultiplier = 0;
			this.endColorTransform.blueMultiplier = 0;
			this.endColorTransform.redOffset = color >> 16 & 255;
			this.endColorTransform.greenOffset = color >> 8 & 255;
			this.endColorTransform.blueOffset = color & 255;
		}
		var propertyNames = ["redMultiplier","greenMultiplier","blueMultiplier","redOffset","greenOffset","blueOffset"];
		if(Object.prototype.hasOwnProperty.call(this.properties,"colorAlpha")) {
			this.endColorTransform.alphaMultiplier = this.properties.colorAlpha;
			propertyNames.push("alphaMultiplier");
		} else this.endColorTransform.alphaMultiplier = this.getField(this.target,"alpha");
		var transform = this.getField(this.target,"transform");
		var begin = this.getField(transform,"colorTransform");
		this.tweenColorTransform = new openfl.geom.ColorTransform();
		var details;
		var start;
		var _g = 0;
		while(_g < propertyNames.length) {
			var propertyName = propertyNames[_g];
			++_g;
			start = this.getField(begin,propertyName);
			details = new motion.actuators.PropertyDetails(this.tweenColorTransform,propertyName,start,this.getField(this.endColorTransform,propertyName) - start);
			this.propertyDetails.push(details);
		}
	}
	,initializeSound: function() {
		if(this.getField(this.target,"soundTransform") == null) this.setField(this.target,"soundTransform",new openfl.media.SoundTransform());
		var start = this.getField(this.target,"soundTransform");
		this.endSoundTransform = this.getField(this.target,"soundTransform");
		this.tweenSoundTransform = new openfl.media.SoundTransform();
		if(Object.prototype.hasOwnProperty.call(this.properties,"soundVolume")) {
			this.endSoundTransform.volume = this.properties.soundVolume;
			this.propertyDetails.push(new motion.actuators.PropertyDetails(this.tweenSoundTransform,"volume",start.volume,this.endSoundTransform.volume - start.volume));
		}
		if(Object.prototype.hasOwnProperty.call(this.properties,"soundPan")) {
			this.endSoundTransform.pan = this.properties.soundPan;
			this.propertyDetails.push(new motion.actuators.PropertyDetails(this.tweenSoundTransform,"pan",start.pan,this.endSoundTransform.pan - start.pan));
		}
	}
	,update: function(currentTime) {
		motion.actuators.SimpleActuator.prototype.update.call(this,currentTime);
		if(this.endColorTransform != null) {
			var transform = this.getField(this.target,"transform");
			this.setField(transform,"colorTransform",this.tweenColorTransform);
		}
		if(this.endSoundTransform != null) this.setField(this.target,"soundTransform",this.tweenSoundTransform);
	}
	,__class__: motion.actuators.TransformActuator
});
motion.easing.ExpoEaseIn = function() {
};
$hxClasses["motion.easing.ExpoEaseIn"] = motion.easing.ExpoEaseIn;
motion.easing.ExpoEaseIn.__name__ = ["motion","easing","ExpoEaseIn"];
motion.easing.ExpoEaseIn.__interfaces__ = [motion.easing.IEasing];
motion.easing.ExpoEaseIn.prototype = {
	calculate: function(k) {
		if(k == 0) return 0; else return Math.pow(2,10 * (k - 1));
	}
	,ease: function(t,b,c,d) {
		if(t == 0) return b; else return c * Math.pow(2,10 * (t / d - 1)) + b;
	}
	,__class__: motion.easing.ExpoEaseIn
};
motion.easing.ExpoEaseInOut = function() {
};
$hxClasses["motion.easing.ExpoEaseInOut"] = motion.easing.ExpoEaseInOut;
motion.easing.ExpoEaseInOut.__name__ = ["motion","easing","ExpoEaseInOut"];
motion.easing.ExpoEaseInOut.__interfaces__ = [motion.easing.IEasing];
motion.easing.ExpoEaseInOut.prototype = {
	calculate: function(k) {
		if(k == 0) return 0;
		if(k == 1) return 1;
		if((k /= 0.5) < 1.0) return 0.5 * Math.pow(2,10 * (k - 1));
		return 0.5 * (2 - Math.pow(2,-10 * --k));
	}
	,ease: function(t,b,c,d) {
		if(t == 0) return b;
		if(t == d) return b + c;
		if((t /= d / 2.0) < 1.0) return c / 2 * Math.pow(2,10 * (t - 1)) + b;
		return c / 2 * (2 - Math.pow(2,-10 * --t)) + b;
	}
	,__class__: motion.easing.ExpoEaseInOut
};
motion.easing.Quint = function() { };
$hxClasses["motion.easing.Quint"] = motion.easing.Quint;
motion.easing.Quint.__name__ = ["motion","easing","Quint"];
motion.easing.Quint.__properties__ = {get_easeOut:"get_easeOut",get_easeInOut:"get_easeInOut",get_easeIn:"get_easeIn"}
motion.easing.Quint.get_easeIn = function() {
	return new motion.easing.QuintEaseIn();
};
motion.easing.Quint.get_easeInOut = function() {
	return new motion.easing.QuintEaseInOut();
};
motion.easing.Quint.get_easeOut = function() {
	return new motion.easing.QuintEaseOut();
};
motion.easing.QuintEaseIn = function() {
};
$hxClasses["motion.easing.QuintEaseIn"] = motion.easing.QuintEaseIn;
motion.easing.QuintEaseIn.__name__ = ["motion","easing","QuintEaseIn"];
motion.easing.QuintEaseIn.__interfaces__ = [motion.easing.IEasing];
motion.easing.QuintEaseIn.prototype = {
	calculate: function(k) {
		return k * k * k * k * k;
	}
	,ease: function(t,b,c,d) {
		return c * (t /= d) * t * t * t * t + b;
	}
	,__class__: motion.easing.QuintEaseIn
};
motion.easing.QuintEaseInOut = function() {
};
$hxClasses["motion.easing.QuintEaseInOut"] = motion.easing.QuintEaseInOut;
motion.easing.QuintEaseInOut.__name__ = ["motion","easing","QuintEaseInOut"];
motion.easing.QuintEaseInOut.__interfaces__ = [motion.easing.IEasing];
motion.easing.QuintEaseInOut.prototype = {
	calculate: function(k) {
		if((k *= 2) < 1) return 0.5 * k * k * k * k * k;
		return 0.5 * ((k -= 2) * k * k * k * k + 2);
	}
	,ease: function(t,b,c,d) {
		if((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
		return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
	}
	,__class__: motion.easing.QuintEaseInOut
};
motion.easing.QuintEaseOut = function() {
};
$hxClasses["motion.easing.QuintEaseOut"] = motion.easing.QuintEaseOut;
motion.easing.QuintEaseOut.__name__ = ["motion","easing","QuintEaseOut"];
motion.easing.QuintEaseOut.__interfaces__ = [motion.easing.IEasing];
motion.easing.QuintEaseOut.prototype = {
	calculate: function(k) {
		return --k * k * k * k * k + 1;
	}
	,ease: function(t,b,c,d) {
		return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
	}
	,__class__: motion.easing.QuintEaseOut
};
var msignal = {};
msignal.Signal = function(valueClasses) {
	if(valueClasses == null) valueClasses = [];
	this.valueClasses = valueClasses;
	this.slots = msignal.SlotList.NIL;
	this.priorityBased = false;
};
$hxClasses["msignal.Signal"] = msignal.Signal;
msignal.Signal.__name__ = ["msignal","Signal"];
msignal.Signal.prototype = {
	add: function(listener) {
		return this.registerListener(listener);
	}
	,addOnce: function(listener) {
		return this.registerListener(listener,true);
	}
	,addWithPriority: function(listener,priority) {
		if(priority == null) priority = 0;
		return this.registerListener(listener,false,priority);
	}
	,addOnceWithPriority: function(listener,priority) {
		if(priority == null) priority = 0;
		return this.registerListener(listener,true,priority);
	}
	,remove: function(listener) {
		var slot = this.slots.find(listener);
		if(slot == null) return null;
		this.slots = this.slots.filterNot(listener);
		return slot;
	}
	,removeAll: function() {
		this.slots = msignal.SlotList.NIL;
	}
	,registerListener: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		if(this.registrationPossible(listener,once)) {
			var newSlot = this.createSlot(listener,once,priority);
			if(!this.priorityBased && priority != 0) this.priorityBased = true;
			if(!this.priorityBased && priority == 0) this.slots = this.slots.prepend(newSlot); else this.slots = this.slots.insertWithPriority(newSlot);
			return newSlot;
		}
		return this.slots.find(listener);
	}
	,registrationPossible: function(listener,once) {
		if(!this.slots.nonEmpty) return true;
		var existingSlot = this.slots.find(listener);
		if(existingSlot == null) return true;
		if(existingSlot.once != once) throw "You cannot addOnce() then add() the same listener without removing the relationship first.";
		return false;
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return null;
	}
	,get_numListeners: function() {
		return this.slots.get_length();
	}
	,__class__: msignal.Signal
	,__properties__: {get_numListeners:"get_numListeners"}
};
msignal.Signal0 = function() {
	msignal.Signal.call(this);
};
$hxClasses["msignal.Signal0"] = msignal.Signal0;
msignal.Signal0.__name__ = ["msignal","Signal0"];
msignal.Signal0.__super__ = msignal.Signal;
msignal.Signal0.prototype = $extend(msignal.Signal.prototype,{
	dispatch: function() {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute();
			slotsToProcess = slotsToProcess.tail;
		}
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return new msignal.Slot0(this,listener,once,priority);
	}
	,__class__: msignal.Signal0
});
msignal.Signal1 = function(type) {
	msignal.Signal.call(this,[type]);
};
$hxClasses["msignal.Signal1"] = msignal.Signal1;
msignal.Signal1.__name__ = ["msignal","Signal1"];
msignal.Signal1.__super__ = msignal.Signal;
msignal.Signal1.prototype = $extend(msignal.Signal.prototype,{
	dispatch: function(value) {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute(value);
			slotsToProcess = slotsToProcess.tail;
		}
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return new msignal.Slot1(this,listener,once,priority);
	}
	,__class__: msignal.Signal1
});
msignal.Signal2 = function(type1,type2) {
	msignal.Signal.call(this,[type1,type2]);
};
$hxClasses["msignal.Signal2"] = msignal.Signal2;
msignal.Signal2.__name__ = ["msignal","Signal2"];
msignal.Signal2.__super__ = msignal.Signal;
msignal.Signal2.prototype = $extend(msignal.Signal.prototype,{
	dispatch: function(value1,value2) {
		var slotsToProcess = this.slots;
		while(slotsToProcess.nonEmpty) {
			slotsToProcess.head.execute(value1,value2);
			slotsToProcess = slotsToProcess.tail;
		}
	}
	,createSlot: function(listener,once,priority) {
		if(priority == null) priority = 0;
		if(once == null) once = false;
		return new msignal.Slot2(this,listener,once,priority);
	}
	,__class__: msignal.Signal2
});
msignal.Slot = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	this.signal = signal;
	this.set_listener(listener);
	this.once = once;
	this.priority = priority;
	this.enabled = true;
};
$hxClasses["msignal.Slot"] = msignal.Slot;
msignal.Slot.__name__ = ["msignal","Slot"];
msignal.Slot.prototype = {
	remove: function() {
		this.signal.remove(this.listener);
	}
	,set_listener: function(value) {
		if(value == null) throw "listener cannot be null";
		return this.listener = value;
	}
	,__class__: msignal.Slot
	,__properties__: {set_listener:"set_listener"}
};
msignal.Slot0 = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	msignal.Slot.call(this,signal,listener,once,priority);
};
$hxClasses["msignal.Slot0"] = msignal.Slot0;
msignal.Slot0.__name__ = ["msignal","Slot0"];
msignal.Slot0.__super__ = msignal.Slot;
msignal.Slot0.prototype = $extend(msignal.Slot.prototype,{
	execute: function() {
		if(!this.enabled) return;
		if(this.once) this.remove();
		this.listener();
	}
	,__class__: msignal.Slot0
});
msignal.Slot1 = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	msignal.Slot.call(this,signal,listener,once,priority);
};
$hxClasses["msignal.Slot1"] = msignal.Slot1;
msignal.Slot1.__name__ = ["msignal","Slot1"];
msignal.Slot1.__super__ = msignal.Slot;
msignal.Slot1.prototype = $extend(msignal.Slot.prototype,{
	execute: function(value1) {
		if(!this.enabled) return;
		if(this.once) this.remove();
		if(this.param != null) value1 = this.param;
		this.listener(value1);
	}
	,__class__: msignal.Slot1
});
msignal.Slot2 = function(signal,listener,once,priority) {
	if(priority == null) priority = 0;
	if(once == null) once = false;
	msignal.Slot.call(this,signal,listener,once,priority);
};
$hxClasses["msignal.Slot2"] = msignal.Slot2;
msignal.Slot2.__name__ = ["msignal","Slot2"];
msignal.Slot2.__super__ = msignal.Slot;
msignal.Slot2.prototype = $extend(msignal.Slot.prototype,{
	execute: function(value1,value2) {
		if(!this.enabled) return;
		if(this.once) this.remove();
		if(this.param1 != null) value1 = this.param1;
		if(this.param2 != null) value2 = this.param2;
		this.listener(value1,value2);
	}
	,__class__: msignal.Slot2
});
msignal.SlotList = function(head,tail) {
	this.nonEmpty = false;
	if(head == null && tail == null) {
		if(msignal.SlotList.NIL != null) throw "Parameters head and tail are null. Use the NIL element instead.";
		this.nonEmpty = false;
	} else if(head == null) throw "Parameter head cannot be null."; else {
		this.head = head;
		if(tail == null) this.tail = msignal.SlotList.NIL; else this.tail = tail;
		this.nonEmpty = true;
	}
};
$hxClasses["msignal.SlotList"] = msignal.SlotList;
msignal.SlotList.__name__ = ["msignal","SlotList"];
msignal.SlotList.NIL = null;
msignal.SlotList.prototype = {
	get_length: function() {
		if(!this.nonEmpty) return 0;
		if(this.tail == msignal.SlotList.NIL) return 1;
		var result = 0;
		var p = this;
		while(p.nonEmpty) {
			++result;
			p = p.tail;
		}
		return result;
	}
	,prepend: function(slot) {
		return new msignal.SlotList(slot,this);
	}
	,append: function(slot) {
		if(slot == null) return this;
		if(!this.nonEmpty) return new msignal.SlotList(slot);
		if(this.tail == msignal.SlotList.NIL) return new msignal.SlotList(slot).prepend(this.head);
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		subClone.tail = new msignal.SlotList(slot);
		return wholeClone;
	}
	,insertWithPriority: function(slot) {
		if(!this.nonEmpty) return new msignal.SlotList(slot);
		var priority = slot.priority;
		if(priority >= this.head.priority) return this.prepend(slot);
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			if(priority > current.head.priority) {
				subClone.tail = current.prepend(slot);
				return wholeClone;
			}
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		subClone.tail = new msignal.SlotList(slot);
		return wholeClone;
	}
	,filterNot: function(listener) {
		if(!this.nonEmpty || listener == null) return this;
		if(Reflect.compareMethods(this.head.listener,listener)) return this.tail;
		var wholeClone = new msignal.SlotList(this.head);
		var subClone = wholeClone;
		var current = this.tail;
		while(current.nonEmpty) {
			if(Reflect.compareMethods(current.head.listener,listener)) {
				subClone.tail = current.tail;
				return wholeClone;
			}
			subClone = subClone.tail = new msignal.SlotList(current.head);
			current = current.tail;
		}
		return this;
	}
	,contains: function(listener) {
		if(!this.nonEmpty) return false;
		var p = this;
		while(p.nonEmpty) {
			if(Reflect.compareMethods(p.head.listener,listener)) return true;
			p = p.tail;
		}
		return false;
	}
	,find: function(listener) {
		if(!this.nonEmpty) return null;
		var p = this;
		while(p.nonEmpty) {
			if(Reflect.compareMethods(p.head.listener,listener)) return p.head;
			p = p.tail;
		}
		return null;
	}
	,__class__: msignal.SlotList
	,__properties__: {get_length:"get_length"}
};
openfl.AssetCache = function() {
	this.enabled = true;
	this.bitmapData = new haxe.ds.StringMap();
	this.font = new haxe.ds.StringMap();
	this.sound = new haxe.ds.StringMap();
};
$hxClasses["openfl.AssetCache"] = openfl.AssetCache;
openfl.AssetCache.__name__ = ["openfl","AssetCache"];
openfl.AssetCache.prototype = {
	clear: function(prefix) {
		if(prefix == null) {
			this.bitmapData = new haxe.ds.StringMap();
			this.font = new haxe.ds.StringMap();
			this.sound = new haxe.ds.StringMap();
		} else {
			var keys = this.bitmapData.keys();
			while( keys.hasNext() ) {
				var key = keys.next();
				if(StringTools.startsWith(key,prefix)) this.bitmapData.remove(key);
			}
			var keys1 = this.font.keys();
			while( keys1.hasNext() ) {
				var key1 = keys1.next();
				if(StringTools.startsWith(key1,prefix)) this.font.remove(key1);
			}
			var keys2 = this.sound.keys();
			while( keys2.hasNext() ) {
				var key2 = keys2.next();
				if(StringTools.startsWith(key2,prefix)) this.sound.remove(key2);
			}
		}
	}
	,__class__: openfl.AssetCache
};
openfl.Assets = function() { };
$hxClasses["openfl.Assets"] = openfl.Assets;
openfl.Assets.__name__ = ["openfl","Assets"];
openfl.Assets.addEventListener = function(type,listener,useCapture,priority,useWeakReference) {
	if(useWeakReference == null) useWeakReference = false;
	if(priority == null) priority = 0;
	if(useCapture == null) useCapture = false;
	openfl.Assets.initialize();
	openfl.Assets.dispatcher.addEventListener(type,listener,useCapture,priority,useWeakReference);
};
openfl.Assets.dispatchEvent = function(event) {
	openfl.Assets.initialize();
	return openfl.Assets.dispatcher.dispatchEvent(event);
};
openfl.Assets.exists = function(id,type) {
	openfl.Assets.initialize();
	if(type == null) type = openfl.AssetType.BINARY;
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) return library.exists(symbolName,type);
	return false;
};
openfl.Assets.getBitmapData = function(id,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.bitmapData.exists(id)) {
		var bitmapData = openfl.Assets.cache.bitmapData.get(id);
		if(openfl.Assets.isValidBitmapData(bitmapData)) return bitmapData;
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.IMAGE)) {
			if(library.isLocal(symbolName,openfl.AssetType.IMAGE)) {
				var bitmapData1 = library.getBitmapData(symbolName);
				if(useCache && openfl.Assets.cache.enabled) openfl.Assets.cache.bitmapData.set(id,bitmapData1);
				return bitmapData1;
			} else haxe.Log.trace("[openfl.Assets] BitmapData asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 139, className : "openfl.Assets", methodName : "getBitmapData"});
		} else haxe.Log.trace("[openfl.Assets] There is no BitmapData asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 145, className : "openfl.Assets", methodName : "getBitmapData"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 151, className : "openfl.Assets", methodName : "getBitmapData"});
	return null;
};
openfl.Assets.getBytes = function(id) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.BINARY)) {
			if(library.isLocal(symbolName,openfl.AssetType.BINARY)) return library.getBytes(symbolName); else haxe.Log.trace("[openfl.Assets] String or ByteArray asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 188, className : "openfl.Assets", methodName : "getBytes"});
		} else haxe.Log.trace("[openfl.Assets] There is no String or ByteArray asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 194, className : "openfl.Assets", methodName : "getBytes"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 200, className : "openfl.Assets", methodName : "getBytes"});
	return null;
};
openfl.Assets.getFont = function(id,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.font.exists(id)) return openfl.Assets.cache.font.get(id);
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.FONT)) {
			if(library.isLocal(symbolName,openfl.AssetType.FONT)) {
				var font = library.getFont(symbolName);
				if(useCache && openfl.Assets.cache.enabled) openfl.Assets.cache.font.set(id,font);
				return font;
			} else haxe.Log.trace("[openfl.Assets] Font asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 251, className : "openfl.Assets", methodName : "getFont"});
		} else haxe.Log.trace("[openfl.Assets] There is no Font asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 257, className : "openfl.Assets", methodName : "getFont"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 263, className : "openfl.Assets", methodName : "getFont"});
	return null;
};
openfl.Assets.getLibrary = function(name) {
	if(name == null || name == "") name = "default";
	return openfl.Assets.libraries.get(name);
};
openfl.Assets.getMovieClip = function(id) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.MOVIE_CLIP)) {
			if(library.isLocal(symbolName,openfl.AssetType.MOVIE_CLIP)) return library.getMovieClip(symbolName); else haxe.Log.trace("[openfl.Assets] MovieClip asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 313, className : "openfl.Assets", methodName : "getMovieClip"});
		} else haxe.Log.trace("[openfl.Assets] There is no MovieClip asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 319, className : "openfl.Assets", methodName : "getMovieClip"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 325, className : "openfl.Assets", methodName : "getMovieClip"});
	return null;
};
openfl.Assets.getMusic = function(id,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.sound.exists(id)) {
		var sound = openfl.Assets.cache.sound.get(id);
		if(openfl.Assets.isValidSound(sound)) return sound;
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.MUSIC)) {
			if(library.isLocal(symbolName,openfl.AssetType.MUSIC)) {
				var sound1 = library.getMusic(symbolName);
				if(useCache && openfl.Assets.cache.enabled) openfl.Assets.cache.sound.set(id,sound1);
				return sound1;
			} else haxe.Log.trace("[openfl.Assets] Sound asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 382, className : "openfl.Assets", methodName : "getMusic"});
		} else haxe.Log.trace("[openfl.Assets] There is no Sound asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 388, className : "openfl.Assets", methodName : "getMusic"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 394, className : "openfl.Assets", methodName : "getMusic"});
	return null;
};
openfl.Assets.getPath = function(id) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,null)) return library.getPath(symbolName); else haxe.Log.trace("[openfl.Assets] There is no asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 429, className : "openfl.Assets", methodName : "getPath"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 435, className : "openfl.Assets", methodName : "getPath"});
	return null;
};
openfl.Assets.getSound = function(id,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.sound.exists(id)) {
		var sound = openfl.Assets.cache.sound.get(id);
		if(openfl.Assets.isValidSound(sound)) return sound;
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.SOUND)) {
			if(library.isLocal(symbolName,openfl.AssetType.SOUND)) {
				var sound1 = library.getSound(symbolName);
				if(useCache && openfl.Assets.cache.enabled) openfl.Assets.cache.sound.set(id,sound1);
				return sound1;
			} else haxe.Log.trace("[openfl.Assets] Sound asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 492, className : "openfl.Assets", methodName : "getSound"});
		} else haxe.Log.trace("[openfl.Assets] There is no Sound asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 498, className : "openfl.Assets", methodName : "getSound"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 504, className : "openfl.Assets", methodName : "getSound"});
	return null;
};
openfl.Assets.getText = function(id) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.TEXT)) {
			if(library.isLocal(symbolName,openfl.AssetType.TEXT)) return library.getText(symbolName); else haxe.Log.trace("[openfl.Assets] String asset \"" + id + "\" exists, but only asynchronously",{ fileName : "Assets.hx", lineNumber : 541, className : "openfl.Assets", methodName : "getText"});
		} else haxe.Log.trace("[openfl.Assets] There is no String asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 547, className : "openfl.Assets", methodName : "getText"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 553, className : "openfl.Assets", methodName : "getText"});
	return null;
};
openfl.Assets.hasEventListener = function(type) {
	openfl.Assets.initialize();
	return openfl.Assets.dispatcher.hasEventListener(type);
};
openfl.Assets.initialize = function() {
	if(!openfl.Assets.initialized) {
		openfl.Assets.registerLibrary("default",new DefaultAssetLibrary());
		openfl.Assets.initialized = true;
	}
};
openfl.Assets.isLocal = function(id,type,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled) {
		if(type == openfl.AssetType.IMAGE || type == null) {
			if(openfl.Assets.cache.bitmapData.exists(id)) return true;
		}
		if(type == openfl.AssetType.FONT || type == null) {
			if(openfl.Assets.cache.font.exists(id)) return true;
		}
		if(type == openfl.AssetType.SOUND || type == openfl.AssetType.MUSIC || type == null) {
			if(openfl.Assets.cache.sound.exists(id)) return true;
		}
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) return library.isLocal(symbolName,type);
	return false;
};
openfl.Assets.isValidBitmapData = function(bitmapData) {
	return bitmapData.__sourceImage != null || bitmapData.__sourceCanvas != null;
	return true;
};
openfl.Assets.isValidSound = function(sound) {
	return true;
};
openfl.Assets.list = function(type) {
	openfl.Assets.initialize();
	var items = [];
	var $it0 = openfl.Assets.libraries.iterator();
	while( $it0.hasNext() ) {
		var library = $it0.next();
		var libraryItems = library.list(type);
		if(libraryItems != null) items = items.concat(libraryItems);
	}
	return items;
};
openfl.Assets.loadBitmapData = function(id,handler,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.bitmapData.exists(id)) {
		var bitmapData = openfl.Assets.cache.bitmapData.get(id);
		if(openfl.Assets.isValidBitmapData(bitmapData)) {
			handler(bitmapData);
			return;
		}
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.IMAGE)) {
			if(useCache && openfl.Assets.cache.enabled) library.loadBitmapData(symbolName,function(bitmapData1) {
				openfl.Assets.cache.bitmapData.set(id,bitmapData1);
				handler(bitmapData1);
			}); else library.loadBitmapData(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no BitmapData asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 750, className : "openfl.Assets", methodName : "loadBitmapData"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 756, className : "openfl.Assets", methodName : "loadBitmapData"});
	handler(null);
};
openfl.Assets.loadBytes = function(id,handler) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.BINARY)) {
			library.loadBytes(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no String or ByteArray asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 786, className : "openfl.Assets", methodName : "loadBytes"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 792, className : "openfl.Assets", methodName : "loadBytes"});
	handler(null);
};
openfl.Assets.loadFont = function(id,handler,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.font.exists(id)) {
		handler(openfl.Assets.cache.font.get(id));
		return;
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.FONT)) {
			if(useCache && openfl.Assets.cache.enabled) library.loadFont(symbolName,function(font) {
				openfl.Assets.cache.font.set(id,font);
				handler(font);
			}); else library.loadFont(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no Font asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 843, className : "openfl.Assets", methodName : "loadFont"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 849, className : "openfl.Assets", methodName : "loadFont"});
	handler(null);
};
openfl.Assets.loadLibrary = function(name,handler) {
	openfl.Assets.initialize();
	var data = openfl.Assets.getText("libraries/" + name + ".dat");
	if(data != null && data != "") {
		var unserializer = new haxe.Unserializer(data);
		unserializer.setResolver({ resolveEnum : openfl.Assets.resolveEnum, resolveClass : openfl.Assets.resolveClass});
		var library = unserializer.unserialize();
		openfl.Assets.libraries.set(name,library);
		library.eventCallback = openfl.Assets.library_onEvent;
		library.load(handler);
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + name + "\"",{ fileName : "Assets.hx", lineNumber : 880, className : "openfl.Assets", methodName : "loadLibrary"});
};
openfl.Assets.loadMusic = function(id,handler,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.sound.exists(id)) {
		var sound = openfl.Assets.cache.sound.get(id);
		if(openfl.Assets.isValidSound(sound)) {
			handler(sound);
			return;
		}
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.MUSIC)) {
			if(useCache && openfl.Assets.cache.enabled) library.loadMusic(symbolName,function(sound1) {
				openfl.Assets.cache.sound.set(id,sound1);
				handler(sound1);
			}); else library.loadMusic(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no Sound asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 935, className : "openfl.Assets", methodName : "loadMusic"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 941, className : "openfl.Assets", methodName : "loadMusic"});
	handler(null);
};
openfl.Assets.loadMovieClip = function(id,handler) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.MOVIE_CLIP)) {
			library.loadMovieClip(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no MovieClip asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 971, className : "openfl.Assets", methodName : "loadMovieClip"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 977, className : "openfl.Assets", methodName : "loadMovieClip"});
	handler(null);
};
openfl.Assets.loadSound = function(id,handler,useCache) {
	if(useCache == null) useCache = true;
	openfl.Assets.initialize();
	if(useCache && openfl.Assets.cache.enabled && openfl.Assets.cache.sound.exists(id)) {
		var sound = openfl.Assets.cache.sound.get(id);
		if(openfl.Assets.isValidSound(sound)) {
			handler(sound);
			return;
		}
	}
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.SOUND)) {
			if(useCache && openfl.Assets.cache.enabled) library.loadSound(symbolName,function(sound1) {
				openfl.Assets.cache.sound.set(id,sound1);
				handler(sound1);
			}); else library.loadSound(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no Sound asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 1034, className : "openfl.Assets", methodName : "loadSound"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 1040, className : "openfl.Assets", methodName : "loadSound"});
	handler(null);
};
openfl.Assets.loadText = function(id,handler) {
	openfl.Assets.initialize();
	var libraryName = id.substring(0,id.indexOf(":"));
	var symbolName;
	var pos = id.indexOf(":") + 1;
	symbolName = HxOverrides.substr(id,pos,null);
	var library = openfl.Assets.getLibrary(libraryName);
	if(library != null) {
		if(library.exists(symbolName,openfl.AssetType.TEXT)) {
			library.loadText(symbolName,handler);
			return;
		} else haxe.Log.trace("[openfl.Assets] There is no String asset with an ID of \"" + id + "\"",{ fileName : "Assets.hx", lineNumber : 1070, className : "openfl.Assets", methodName : "loadText"});
	} else haxe.Log.trace("[openfl.Assets] There is no asset library named \"" + libraryName + "\"",{ fileName : "Assets.hx", lineNumber : 1076, className : "openfl.Assets", methodName : "loadText"});
	handler(null);
};
openfl.Assets.registerLibrary = function(name,library) {
	if(openfl.Assets.libraries.exists(name)) openfl.Assets.unloadLibrary(name);
	if(library != null) library.eventCallback = openfl.Assets.library_onEvent;
	openfl.Assets.libraries.set(name,library);
};
openfl.Assets.removeEventListener = function(type,listener,capture) {
	if(capture == null) capture = false;
	openfl.Assets.initialize();
	openfl.Assets.dispatcher.removeEventListener(type,listener,capture);
};
openfl.Assets.resolveClass = function(name) {
	return Type.resolveClass(name);
};
openfl.Assets.resolveEnum = function(name) {
	var value = Type.resolveEnum(name);
	return value;
};
openfl.Assets.unloadLibrary = function(name) {
	openfl.Assets.initialize();
	var library = openfl.Assets.libraries.get(name);
	if(library != null) {
		openfl.Assets.cache.clear(name + ":");
		library.eventCallback = null;
	}
	openfl.Assets.libraries.remove(name);
};
openfl.Assets.library_onEvent = function(library,type) {
	if(type == "change") {
		openfl.Assets.cache.clear();
		openfl.Assets.dispatchEvent(new openfl.events.Event(openfl.events.Event.CHANGE));
	}
};
openfl.AssetData = function() {
};
$hxClasses["openfl.AssetData"] = openfl.AssetData;
openfl.AssetData.__name__ = ["openfl","AssetData"];
openfl.AssetData.prototype = {
	__class__: openfl.AssetData
};
openfl.AssetType = $hxClasses["openfl.AssetType"] = { __ename__ : ["openfl","AssetType"], __constructs__ : ["BINARY","FONT","IMAGE","MOVIE_CLIP","MUSIC","SOUND","TEMPLATE","TEXT"] };
openfl.AssetType.BINARY = ["BINARY",0];
openfl.AssetType.BINARY.toString = $estr;
openfl.AssetType.BINARY.__enum__ = openfl.AssetType;
openfl.AssetType.FONT = ["FONT",1];
openfl.AssetType.FONT.toString = $estr;
openfl.AssetType.FONT.__enum__ = openfl.AssetType;
openfl.AssetType.IMAGE = ["IMAGE",2];
openfl.AssetType.IMAGE.toString = $estr;
openfl.AssetType.IMAGE.__enum__ = openfl.AssetType;
openfl.AssetType.MOVIE_CLIP = ["MOVIE_CLIP",3];
openfl.AssetType.MOVIE_CLIP.toString = $estr;
openfl.AssetType.MOVIE_CLIP.__enum__ = openfl.AssetType;
openfl.AssetType.MUSIC = ["MUSIC",4];
openfl.AssetType.MUSIC.toString = $estr;
openfl.AssetType.MUSIC.__enum__ = openfl.AssetType;
openfl.AssetType.SOUND = ["SOUND",5];
openfl.AssetType.SOUND.toString = $estr;
openfl.AssetType.SOUND.__enum__ = openfl.AssetType;
openfl.AssetType.TEMPLATE = ["TEMPLATE",6];
openfl.AssetType.TEMPLATE.toString = $estr;
openfl.AssetType.TEMPLATE.__enum__ = openfl.AssetType;
openfl.AssetType.TEXT = ["TEXT",7];
openfl.AssetType.TEXT.toString = $estr;
openfl.AssetType.TEXT.__enum__ = openfl.AssetType;
openfl.Lib = function() { };
$hxClasses["openfl.Lib"] = openfl.Lib;
openfl.Lib.__name__ = ["openfl","Lib"];
openfl.Lib.current = null;
openfl.Lib.as = function(v,c) {
	if(js.Boot.__instanceof(v,c)) return v; else return null;
};
openfl.Lib.attach = function(name) {
	return new openfl.display.MovieClip();
};
openfl.Lib.create = function(element,width,height,backgroundColor) {
	if(width == null) width = 0;
	if(height == null) height = 0;
	
			var lastTime = 0;
			var vendors = ['ms', 'moz', 'webkit', 'o'];
			for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
				window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
				window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
										   || window[vendors[x]+'CancelRequestAnimationFrame'];
			}
			
			if (!window.requestAnimationFrame)
				window.requestAnimationFrame = function(callback, element) {
					var currTime = new Date().getTime();
					var timeToCall = Math.max(0, 16 - (currTime - lastTime));
					var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
					  timeToCall);
					lastTime = currTime + timeToCall;
					return id;
				};
			
			if (!window.cancelAnimationFrame)
				window.cancelAnimationFrame = function(id) {
					clearTimeout(id);
				};
			
			window.requestAnimFrame = window.requestAnimationFrame;
		;
	var stage = new openfl.display.Stage(width,height,element,backgroundColor);
	if(openfl.Lib.current == null) {
		openfl.Lib.current = new openfl.display.MovieClip();
		stage.addChild(openfl.Lib.current);
	}
};
openfl.Lib.getTimer = function() {
	return Std.int((haxe.Timer.stamp() - openfl.Lib.__startTime) * 1000);
};
openfl.Lib.getURL = function(request,target) {
	if(target == null) target = "_blank";
	window.open(request.url,target);
};
openfl.Lib.notImplemented = function(api) {
	if(!openfl.Lib.__sentWarnings.exists(api)) {
		openfl.Lib.__sentWarnings.set(api,true);
		haxe.Log.trace("Warning: " + api + " is not implemented",{ fileName : "Lib.hx", lineNumber : 114, className : "openfl.Lib", methodName : "notImplemented"});
	}
};
openfl.Lib.preventDefaultTouchMove = function() {
	window.document.addEventListener("touchmove",function(evt) {
		evt.preventDefault();
	},false);
};
openfl.Lib.trace = function(arg) {
	haxe.Log.trace(arg,{ fileName : "Lib.hx", lineNumber : 134, className : "openfl.Lib", methodName : "trace"});
};
openfl.Memory = function() { };
$hxClasses["openfl.Memory"] = openfl.Memory;
openfl.Memory.__name__ = ["openfl","Memory"];
openfl.Memory.gcRef = null;
openfl.Memory.len = null;
openfl.Memory._setPositionTemporarily = function(position,action) {
	var oldPosition = openfl.Memory.gcRef.position;
	openfl.Memory.gcRef.position = position;
	var value = action();
	openfl.Memory.gcRef.position = oldPosition;
	return value;
};
openfl.Memory.getByte = function(addr) {
	if(addr < 0 || addr + 1 > openfl.Memory.len) throw "Bad address";
	return openfl.Memory.gcRef.__get(addr);
};
openfl.Memory.getDouble = function(addr) {
	if(addr < 0 || addr + 8 > openfl.Memory.len) throw "Bad address";
	return openfl.Memory._setPositionTemporarily(addr,function() {
		return openfl.Memory.gcRef.readDouble();
	});
};
openfl.Memory.getFloat = function(addr) {
	if(addr < 0 || addr + 4 > openfl.Memory.len) throw "Bad address";
	return openfl.Memory._setPositionTemporarily(addr,function() {
		return openfl.Memory.gcRef.readFloat();
	});
};
openfl.Memory.getI32 = function(addr) {
	if(addr < 0 || addr + 4 > openfl.Memory.len) throw "Bad address";
	return openfl.Memory._setPositionTemporarily(addr,function() {
		return openfl.Memory.gcRef.readInt();
	});
};
openfl.Memory.getUI16 = function(addr) {
	if(addr < 0 || addr + 2 > openfl.Memory.len) throw "Bad address";
	return openfl.Memory._setPositionTemporarily(addr,function() {
		return openfl.Memory.gcRef.readUnsignedShort();
	});
};
openfl.Memory.select = function(inBytes) {
	openfl.Memory.gcRef = inBytes;
	if(inBytes != null) openfl.Memory.len = inBytes.length; else openfl.Memory.len = 0;
};
openfl.Memory.setByte = function(addr,v) {
	if(addr < 0 || addr + 1 > openfl.Memory.len) throw "Bad address";
	openfl.Memory.gcRef.__set(addr,v);
};
openfl.Memory.setDouble = function(addr,v) {
	if(addr < 0 || addr + 8 > openfl.Memory.len) throw "Bad address";
	openfl.Memory._setPositionTemporarily(addr,function() {
		openfl.Memory.gcRef.writeDouble(v);
	});
};
openfl.Memory.setFloat = function(addr,v) {
	if(addr < 0 || addr + 4 > openfl.Memory.len) throw "Bad address";
	openfl.Memory._setPositionTemporarily(addr,function() {
		openfl.Memory.gcRef.writeFloat(v);
	});
};
openfl.Memory.setI16 = function(addr,v) {
	if(addr < 0 || addr + 2 > openfl.Memory.len) throw "Bad address";
	openfl.Memory._setPositionTemporarily(addr,function() {
		openfl.Memory.gcRef.writeUnsignedShort(v);
	});
};
openfl.Memory.setI32 = function(addr,v) {
	if(addr < 0 || addr + 4 > openfl.Memory.len) throw "Bad address";
	openfl.Memory._setPositionTemporarily(addr,function() {
		openfl.Memory.gcRef.writeInt(v);
	});
};
openfl._Vector = {};
openfl._Vector.Vector_Impl_ = function() { };
$hxClasses["openfl._Vector.Vector_Impl_"] = openfl._Vector.Vector_Impl_;
openfl._Vector.Vector_Impl_.__name__ = ["openfl","_Vector","Vector_Impl_"];
openfl._Vector.Vector_Impl_.__properties__ = {set_fixed:"set_fixed",get_fixed:"get_fixed",set_length:"set_length",get_length:"get_length"}
openfl._Vector.Vector_Impl_._new = function(length,fixed) {
	if(fixed == null) fixed = false;
	if(length == null) length = 0;
	var this1;
	this1 = new openfl.VectorData();
	var this2;
	this2 = new Array(length);
	this1.data = this2;
	this1.length = length;
	this1.fixed = fixed;
	return this1;
};
openfl._Vector.Vector_Impl_.concat = function(this1,a) {
	var vectorData = new openfl.VectorData();
	if(a != null) vectorData.length = this1.length + a.length; else vectorData.length = this1.length;
	vectorData.fixed = false;
	var this2;
	this2 = new Array(vectorData.length);
	vectorData.data = this2;
	haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,vectorData.data,0,this1.length);
	if(a != null) haxe.ds._Vector.Vector_Impl_.blit(a.data,0,vectorData.data,this1.length,a.length);
	return vectorData;
};
openfl._Vector.Vector_Impl_.copy = function(this1) {
	var vectorData = new openfl.VectorData();
	vectorData.length = this1.length;
	vectorData.fixed = this1.fixed;
	var this2;
	this2 = new Array(this1.length);
	vectorData.data = this2;
	haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,vectorData.data,0,this1.length);
	return vectorData;
};
openfl._Vector.Vector_Impl_.iterator = function(this1) {
	return new openfl.VectorDataIterator(this1);
};
openfl._Vector.Vector_Impl_.join = function(this1,sep) {
	var output = "";
	var _g1 = 0;
	var _g = this1.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(i > 0) output += sep;
		output += Std.string(this1.data[i]);
	}
	return output;
};
openfl._Vector.Vector_Impl_.pop = function(this1) {
	if(!this1.fixed) {
		if(this1.length > 0) {
			this1.length--;
			return this1.data[this1.length];
		}
	}
	return null;
};
openfl._Vector.Vector_Impl_.push = function(this1,x) {
	if(!this1.fixed) {
		this1.length++;
		if(this1.data.length < this1.length) {
			var data;
			var this2;
			this2 = new Array(this1.data.length + 10);
			data = this2;
			haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,data,0,this1.data.length);
			this1.data = data;
		}
		this1.data[this1.length - 1] = x;
	}
	return this1.length;
};
openfl._Vector.Vector_Impl_.reverse = function(this1) {
	var data;
	var this2;
	this2 = new Array(this1.length);
	data = this2;
	var _g1 = 0;
	var _g = this1.length;
	while(_g1 < _g) {
		var i = _g1++;
		data[this1.length - 1 - i] = this1.data[i];
	}
	this1.data = data;
};
openfl._Vector.Vector_Impl_.shift = function(this1) {
	if(!this1.fixed && this1.length > 0) {
		var value = this1.data[0];
		this1.length--;
		haxe.ds._Vector.Vector_Impl_.blit(this1.data,1,this1.data,0,this1.length);
		return value;
	}
	return null;
};
openfl._Vector.Vector_Impl_.unshift = function(this1,x) {
	if(!this1.fixed) {
		this1.length++;
		if(this1.data.length < this1.length) {
			var data;
			var this2;
			this2 = new Array(this1.length + 10);
			data = this2;
			haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,data,1,this1.data.length);
			this1.data = data;
		} else haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,this1.data,1,this1.length - 1);
		this1.data[0] = x;
	}
};
openfl._Vector.Vector_Impl_.slice = function(this1,pos,end) {
	if(end == null) end = 0;
	if(pos == null) pos = 0;
	if(pos < 0) pos += this1.length;
	if(end <= 0) end += this1.length;
	if(end > this1.length) end = this1.length;
	var length = end - pos;
	if(length <= 0 || length > this1.length) length = this1.length;
	var vectorData = new openfl.VectorData();
	vectorData.length = end - pos;
	vectorData.fixed = true;
	var this2;
	this2 = new Array(length);
	vectorData.data = this2;
	haxe.ds._Vector.Vector_Impl_.blit(this1.data,pos,vectorData.data,0,length);
	return vectorData;
};
openfl._Vector.Vector_Impl_.sort = function(this1,f) {
	var array = this1.data;
	array.sort(f);
	var vec;
	var this2;
	this2 = new Array(array.length);
	vec = this2;
	var _g1 = 0;
	var _g = array.length;
	while(_g1 < _g) {
		var i = _g1++;
		vec[i] = array[i];
	}
	this1.data = vec;
};
openfl._Vector.Vector_Impl_.splice = function(this1,pos,len) {
	if(pos < 0) pos += this1.length;
	if(pos + len > this1.length) len = this1.length - pos;
	if(len < 0) len = 0;
	var vectorData = new openfl.VectorData();
	vectorData.length = len;
	vectorData.fixed = false;
	var this2;
	this2 = new Array(len);
	vectorData.data = this2;
	haxe.ds._Vector.Vector_Impl_.blit(this1.data,pos,vectorData.data,0,len);
	if(len > 0) {
		this1.length -= len;
		haxe.ds._Vector.Vector_Impl_.blit(this1.data,pos + len,this1.data,pos,this1.length - pos);
	}
	return vectorData;
};
openfl._Vector.Vector_Impl_.toString = function(this1) {
	return "";
};
openfl._Vector.Vector_Impl_.indexOf = function(this1,x,from) {
	if(from == null) from = 0;
	var _g1 = from;
	var _g = this1.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(this1.data[i] == x) return i;
	}
	return -1;
};
openfl._Vector.Vector_Impl_.lastIndexOf = function(this1,x,from) {
	if(from == null) from = 0;
	var i = this1.length - 1;
	while(i >= from) {
		if(this1.data[i] == x) return i;
		i--;
	}
	return -1;
};
openfl._Vector.Vector_Impl_.ofArray = function(a) {
	var vectorData = new openfl.VectorData();
	vectorData.length = a.length;
	vectorData.fixed = true;
	var vec;
	var this1;
	this1 = new Array(a.length);
	vec = this1;
	var _g1 = 0;
	var _g = a.length;
	while(_g1 < _g) {
		var i = _g1++;
		vec[i] = a[i];
	}
	vectorData.data = vec;
	return vectorData;
};
openfl._Vector.Vector_Impl_.convert = function(v) {
	return v;
};
openfl._Vector.Vector_Impl_.arrayAccess = function(this1,key) {
	return this1.data[key];
};
openfl._Vector.Vector_Impl_.arrayWrite = function(this1,key,value) {
	if(key >= this1.length && !this1.fixed) this1.length = key + 1;
	return this1.data[key] = value;
};
openfl._Vector.Vector_Impl_.fromArray = function(value) {
	var vectorData = new openfl.VectorData();
	vectorData.length = value.length;
	vectorData.fixed = true;
	var vec;
	var this1;
	this1 = new Array(value.length);
	vec = this1;
	var _g1 = 0;
	var _g = value.length;
	while(_g1 < _g) {
		var i = _g1++;
		vec[i] = value[i];
	}
	vectorData.data = vec;
	return vectorData;
};
openfl._Vector.Vector_Impl_.toArray = function(this1) {
	var value = new Array();
	var _g1 = 0;
	var _g = this1.data.length;
	while(_g1 < _g) {
		var i = _g1++;
		value.push(this1.data[i]);
	}
	return value;
};
openfl._Vector.Vector_Impl_.fromHaxeVector = function(value) {
	var vectorData = new openfl.VectorData();
	vectorData.length = value.length;
	vectorData.fixed = true;
	vectorData.data = value;
	return vectorData;
};
openfl._Vector.Vector_Impl_.toHaxeVector = function(this1) {
	return this1.data;
};
openfl._Vector.Vector_Impl_.fromVectorData = function(value) {
	return value;
};
openfl._Vector.Vector_Impl_.toVectorData = function(this1) {
	return this1;
};
openfl._Vector.Vector_Impl_.get_length = function(this1) {
	return this1.length;
};
openfl._Vector.Vector_Impl_.set_length = function(this1,value) {
	if(!this1.fixed) {
		if(value > this1.length) {
			var data;
			var this2;
			this2 = new Array(value);
			data = this2;
			haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,data,0,Std.int(Math.min(this1.data.length,value)));
			this1.data = data;
		}
		this1.length = value;
	}
	return value;
};
openfl._Vector.Vector_Impl_.get_fixed = function(this1) {
	return this1.fixed;
};
openfl._Vector.Vector_Impl_.set_fixed = function(this1,value) {
	return this1.fixed = value;
};
openfl.VectorData = function() {
	this.length = 0;
};
$hxClasses["openfl.VectorData"] = openfl.VectorData;
openfl.VectorData.__name__ = ["openfl","VectorData"];
openfl.VectorData.prototype = {
	__class__: openfl.VectorData
};
openfl.VectorDataIterator = function(data) {
	this.index = 0;
	this.vectorData = data;
};
$hxClasses["openfl.VectorDataIterator"] = openfl.VectorDataIterator;
openfl.VectorDataIterator.__name__ = ["openfl","VectorDataIterator"];
openfl.VectorDataIterator.prototype = {
	hasNext: function() {
		return this.index < this.vectorData.length;
	}
	,next: function() {
		return this.vectorData.data[this.index++];
	}
	,__class__: openfl.VectorDataIterator
};
openfl.display.Bitmap = function(bitmapData,pixelSnapping,smoothing) {
	if(smoothing == null) smoothing = false;
	openfl.display.DisplayObjectContainer.call(this);
	this.bitmapData = bitmapData;
	this.pixelSnapping = pixelSnapping;
	this.smoothing = smoothing;
	if(pixelSnapping == null) this.pixelSnapping = openfl.display.PixelSnapping.AUTO;
};
$hxClasses["openfl.display.Bitmap"] = openfl.display.Bitmap;
openfl.display.Bitmap.__name__ = ["openfl","display","Bitmap"];
openfl.display.Bitmap.__super__ = openfl.display.DisplayObjectContainer;
openfl.display.Bitmap.prototype = $extend(openfl.display.DisplayObjectContainer.prototype,{
	__getBounds: function(rect,matrix) {
		if(this.bitmapData != null) {
			var bounds = new openfl.geom.Rectangle(0,0,this.bitmapData.width,this.bitmapData.height);
			bounds = bounds.transform(this.__worldTransform);
			rect.__expand(bounds.x,bounds.y,bounds.width,bounds.height);
		}
	}
	,__hitTest: function(x,y,shapeFlag,stack,interactiveOnly) {
		if(!this.get_visible() || this.bitmapData == null) return false;
		var point = this.globalToLocal(new openfl.geom.Point(x,y));
		if(point.x > 0 && point.y > 0 && point.x <= this.bitmapData.width && point.y <= this.bitmapData.height) {
			if(stack != null) stack.push(this);
			return true;
		}
		return false;
	}
	,__renderCanvas: function(renderSession) {
		if(!this.__renderable || this.__worldAlpha <= 0) return;
		var context = renderSession.context;
		if(this.bitmapData != null && this.bitmapData.__valid) {
			if(this.__mask != null) renderSession.maskManager.pushMask(this.__mask);
			this.bitmapData.__syncImageData();
			context.globalAlpha = this.__worldAlpha;
			var transform = this.__worldTransform;
			if(renderSession.roundPixels) context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx | 0,transform.ty | 0); else context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx,transform.ty);
			if(!this.smoothing) {
				context.mozImageSmoothingEnabled = false;
				context.webkitImageSmoothingEnabled = false;
				context.imageSmoothingEnabled = false;
			}
			if(this.get_scrollRect() == null) {
				if(this.bitmapData.__sourceImage != null) context.drawImage(this.bitmapData.__sourceImage,0,0); else context.drawImage(this.bitmapData.__sourceCanvas,0,0);
			} else if(this.bitmapData.__sourceImage != null) context.drawImage(this.bitmapData.__sourceImage,this.get_scrollRect().x,this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height,this.get_scrollRect().x,this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height); else context.drawImage(this.bitmapData.__sourceCanvas,this.get_scrollRect().x,this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height,this.get_scrollRect().x,this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height);
			if(!this.smoothing) {
				context.mozImageSmoothingEnabled = true;
				context.webkitImageSmoothingEnabled = true;
				context.imageSmoothingEnabled = true;
			}
			if(this.__mask != null) renderSession.maskManager.popMask();
		}
	}
	,__renderDOM: function(renderSession) {
		if(this.stage != null && this.__worldVisible && this.__renderable && this.bitmapData != null && this.bitmapData.__valid) {
			if(this.bitmapData.__sourceImage != null) this.__renderDOMImage(renderSession); else this.__renderDOMCanvas(renderSession);
		} else {
			if(this.__image != null) {
				renderSession.element.removeChild(this.__image);
				this.__image = null;
				this.__style = null;
			}
			if(this.__canvas != null) {
				renderSession.element.removeChild(this.__canvas);
				this.__canvas = null;
				this.__style = null;
			}
		}
	}
	,__renderDOMCanvas: function(renderSession) {
		if(this.__image != null) {
			renderSession.element.removeChild(this.__image);
			this.__image = null;
		}
		if(this.__canvas == null) {
			this.__canvas = window.document.createElement("canvas");
			this.__canvasContext = this.__canvas.getContext("2d");
			if(!this.smoothing) {
				this.__canvasContext.mozImageSmoothingEnabled = false;
				this.__canvasContext.webkitImageSmoothingEnabled = false;
				this.__canvasContext.imageSmoothingEnabled = false;
			}
			this.__initializeElement(this.__canvas,renderSession);
		}
		this.bitmapData.__syncImageData();
		this.__canvas.width = this.bitmapData.width;
		this.__canvas.height = this.bitmapData.height;
		this.__canvasContext.globalAlpha = this.__worldAlpha;
		this.__canvasContext.drawImage(this.bitmapData.__sourceCanvas,0,0);
		this.__applyStyle(renderSession,true,false,true);
	}
	,__renderDOMImage: function(renderSession) {
		if(this.__canvas != null) {
			renderSession.element.removeChild(this.__canvas);
			this.__canvas = null;
		}
		if(this.__image == null) {
			this.__image = window.document.createElement("img");
			this.__image.src = this.bitmapData.__sourceImage.src;
			this.__initializeElement(this.__image,renderSession);
		}
		this.__applyStyle(renderSession,true,true,true);
	}
	,__renderMask: function(renderSession) {
		renderSession.context.rect(0,0,this.get_width(),this.get_height());
	}
	,get_height: function() {
		if(this.bitmapData != null) return this.bitmapData.height * this.get_scaleY();
		return 0;
	}
	,set_height: function(value) {
		if(this.bitmapData != null) {
			if(value != this.bitmapData.height) {
				if(!this.__transformDirty) {
					this.__transformDirty = true;
					openfl.display.DisplayObject.__worldTransformDirty++;
				}
				this.set_scaleY(value / this.bitmapData.height);
			}
			return value;
		}
		return 0;
	}
	,get_width: function() {
		if(this.bitmapData != null) return this.bitmapData.width * this.get_scaleX();
		return 0;
	}
	,set_width: function(value) {
		if(this.bitmapData != null) {
			if(value != this.bitmapData.width) {
				if(!this.__transformDirty) {
					this.__transformDirty = true;
					openfl.display.DisplayObject.__worldTransformDirty++;
				}
				this.set_scaleX(value / this.bitmapData.width);
			}
			return value;
		}
		return 0;
	}
	,__class__: openfl.display.Bitmap
});
openfl.display.BitmapData = function(width,height,transparent,fillColor) {
	if(fillColor == null) fillColor = -1;
	if(transparent == null) transparent = true;
	this.transparent = transparent;
	if(width > 0 && height > 0) {
		this.width = width;
		this.height = height;
		this.rect = new openfl.geom.Rectangle(0,0,width,height);
		this.__createCanvas(width,height);
		if(!transparent) fillColor = -16777216 | fillColor & 16777215;
		this.__fillRect(new openfl.geom.Rectangle(0,0,width,height),fillColor);
	}
};
$hxClasses["openfl.display.BitmapData"] = openfl.display.BitmapData;
openfl.display.BitmapData.__name__ = ["openfl","display","BitmapData"];
openfl.display.BitmapData.__interfaces__ = [openfl.display.IBitmapDrawable];
openfl.display.BitmapData.__base64Encoder = null;
openfl.display.BitmapData.fromBase64 = function(base64,type,onload) {
	var bitmapData = new openfl.display.BitmapData(0,0,true);
	bitmapData.__loadFromBase64(base64,type,onload);
	return bitmapData;
};
openfl.display.BitmapData.fromBytes = function(bytes,rawAlpha,onload) {
	var bitmapData = new openfl.display.BitmapData(0,0,true);
	bitmapData.__loadFromBytes(bytes,rawAlpha,onload);
	return bitmapData;
};
openfl.display.BitmapData.fromFile = function(path,onload,onfail) {
	var bitmapData = new openfl.display.BitmapData(0,0,true);
	bitmapData.__sourceImage = new Image();
	bitmapData.__sourceImage.onload = function(_) {
		bitmapData.width = bitmapData.__sourceImage.width;
		bitmapData.height = bitmapData.__sourceImage.height;
		bitmapData.rect = new openfl.geom.Rectangle(0,0,bitmapData.__sourceImage.width,bitmapData.__sourceImage.height);
		bitmapData.__valid = true;
		if(onload != null) onload(bitmapData);
	};
	bitmapData.__sourceImage.onerror = function(_1) {
		bitmapData.__valid = false;
		if(onfail != null) onfail();
	};
	bitmapData.__sourceImage.src = path;
	if(bitmapData.__sourceImage.complete) {
	}
	return bitmapData;
};
openfl.display.BitmapData.fromImage = function(image,transparent) {
	if(transparent == null) transparent = true;
	var bitmapData = new openfl.display.BitmapData(0,0,transparent);
	bitmapData.__sourceImage = image;
	bitmapData.width = image.width;
	bitmapData.height = image.height;
	bitmapData.rect = new openfl.geom.Rectangle(0,0,image.width,image.height);
	bitmapData.__valid = true;
	return bitmapData;
};
openfl.display.BitmapData.fromCanvas = function(canvas,transparent) {
	if(transparent == null) transparent = true;
	var bitmapData = new openfl.display.BitmapData(0,0,transparent);
	bitmapData.width = canvas.width;
	bitmapData.height = canvas.height;
	bitmapData.rect = new openfl.geom.Rectangle(0,0,canvas.width,canvas.height);
	bitmapData.__createCanvas(canvas.width,canvas.height);
	bitmapData.__sourceContext.drawImage(canvas,0,0);
	return bitmapData;
};
openfl.display.BitmapData.__base64Encode = function(bytes) {
	var extension;
	var _g = bytes.length % 3;
	switch(_g) {
	case 1:
		extension = "==";
		break;
	case 2:
		extension = "=";
		break;
	default:
		extension = "";
	}
	if(openfl.display.BitmapData.__base64Encoder == null) openfl.display.BitmapData.__base64Encoder = new haxe.crypto.BaseCode(haxe.io.Bytes.ofString(openfl.display.BitmapData.__base64Chars));
	return openfl.display.BitmapData.__base64Encoder.encodeBytes(haxe.io.Bytes.ofData(bytes.byteView)).toString() + extension;
};
openfl.display.BitmapData.__flipPixel = function(pixel) {
	return (pixel & 255) << 24 | (pixel >> 8 & 255) << 16 | (pixel >> 16 & 255) << 8 | pixel >> 24 & 255;
};
openfl.display.BitmapData.__isJPG = function(bytes) {
	bytes.position = 0;
	return bytes.readByte() == 255 && bytes.readByte() == 216;
};
openfl.display.BitmapData.__isPNG = function(bytes) {
	bytes.position = 0;
	return bytes.readByte() == 137 && bytes.readByte() == 80 && bytes.readByte() == 78 && bytes.readByte() == 71 && bytes.readByte() == 13 && bytes.readByte() == 10 && bytes.readByte() == 26 && bytes.readByte() == 10;
};
openfl.display.BitmapData.__isGIF = function(bytes) {
	bytes.position = 0;
	if(bytes.readByte() == 71 && bytes.readByte() == 73 && bytes.readByte() == 70 && bytes.readByte() == 38) {
		var b = bytes.readByte();
		return (b == 7 || b == 9) && bytes.readByte() == 97;
	}
	return false;
};
openfl.display.BitmapData.__ucompare = function(n1,n2) {
	var tmp1;
	var tmp2;
	tmp1 = n1 >> 24 & 255;
	tmp2 = n2 >> 24 & 255;
	if(tmp1 != tmp2) if(tmp1 > tmp2) return 1; else return -1; else {
		tmp1 = n1 >> 16 & 255;
		tmp2 = n2 >> 16 & 255;
		if(tmp1 != tmp2) if(tmp1 > tmp2) return 1; else return -1; else {
			tmp1 = n1 >> 8 & 255;
			tmp2 = n2 >> 8 & 255;
			if(tmp1 != tmp2) if(tmp1 > tmp2) return 1; else return -1; else {
				tmp1 = n1 & 255;
				tmp2 = n2 & 255;
				if(tmp1 != tmp2) if(tmp1 > tmp2) return 1; else return -1; else return 0;
			}
		}
	}
};
openfl.display.BitmapData.prototype = {
	applyFilter: function(sourceBitmapData,sourceRect,destPoint,filter) {
		if(!this.__valid || sourceBitmapData == null || !sourceBitmapData.__valid) return;
		this.__convertToCanvas();
		this.__createImageData();
		sourceBitmapData.__convertToCanvas();
		sourceBitmapData.__createImageData();
		filter.__applyFilter(this.__sourceImageData,sourceBitmapData.__sourceImageData,sourceRect,destPoint);
		this.__sourceImageDataChanged = true;
	}
	,clone: function() {
		this.__syncImageData();
		if(!this.__valid) return new openfl.display.BitmapData(this.width,this.height,this.transparent); else if(this.__sourceImage != null) return openfl.display.BitmapData.fromImage(this.__sourceImage,this.transparent); else return openfl.display.BitmapData.fromCanvas(this.__sourceCanvas,this.transparent);
	}
	,colorTransform: function(rect,colorTransform) {
		rect = this.__clipRect(rect);
		if(!this.__valid || rect == null) return;
		this.__convertToCanvas();
		this.__createImageData();
		var data = this.__sourceImageData.data;
		var stride = this.width * 4;
		var offset;
		var _g1 = rect.y | 0;
		var _g = rect.height | 0;
		while(_g1 < _g) {
			var row = _g1++;
			var _g3 = rect.x | 0;
			var _g2 = rect.width | 0;
			while(_g3 < _g2) {
				var column = _g3++;
				offset = row * stride + column * 4;
				data[offset] = data[offset] * colorTransform.redMultiplier + colorTransform.redOffset | 0;
				data[offset + 1] = data[offset + 1] * colorTransform.greenMultiplier + colorTransform.greenOffset | 0;
				data[offset + 2] = data[offset + 2] * colorTransform.blueMultiplier + colorTransform.blueOffset | 0;
				data[offset + 3] = data[offset + 3] * colorTransform.alphaMultiplier + colorTransform.alphaOffset | 0;
			}
		}
		this.__sourceImageDataChanged = true;
	}
	,copyChannel: function(sourceBitmapData,sourceRect,destPoint,sourceChannel,destChannel) {
		sourceRect = this.__clipRect(sourceRect);
		if(!this.__valid || sourceRect == null) return;
		if(destChannel == 8 && !this.transparent) return;
		if(sourceRect.width <= 0 || sourceRect.height <= 0) return;
		if(sourceRect.x + sourceRect.width > sourceBitmapData.width) sourceRect.width = sourceBitmapData.width - sourceRect.x;
		if(sourceRect.y + sourceRect.height > sourceBitmapData.height) sourceRect.height = sourceBitmapData.height - sourceRect.y;
		var destIdx = -1;
		if(destChannel == 8) destIdx = 3; else if(destChannel == 4) destIdx = 2; else if(destChannel == 2) destIdx = 1; else if(destChannel == 1) destIdx = 0; else throw "Invalid destination BitmapDataChannel passed to BitmapData::copyChannel.";
		var srcIdx = -1;
		if(sourceChannel == 8) srcIdx = 3; else if(sourceChannel == 4) srcIdx = 2; else if(sourceChannel == 2) srcIdx = 1; else if(sourceChannel == 1) srcIdx = 0; else throw "Invalid source BitmapDataChannel passed to BitmapData::copyChannel.";
		sourceBitmapData.__convertToCanvas();
		sourceBitmapData.__createImageData();
		var srcData = sourceBitmapData.__sourceImageData.data;
		var srcStride = sourceBitmapData.width * 4 | 0;
		var srcPosition = sourceRect.x * 4 + srcStride * sourceRect.y + srcIdx | 0;
		var srcRowOffset = srcStride - (4 * sourceRect.width | 0);
		var srcRowEnd = 4 * (sourceRect.x + sourceRect.width) | 0;
		this.__convertToCanvas();
		this.__createImageData();
		var destData = this.__sourceImageData.data;
		var destStride = this.width * 4 | 0;
		var destPosition = destPoint.x * 4 + destStride * destPoint.y + destIdx | 0;
		var destRowOffset = destStride - (4 * sourceRect.width | 0);
		var destRowEnd = 4 * (destPoint.x + sourceRect.width) | 0;
		var length = sourceRect.width * sourceRect.height | 0;
		var _g = 0;
		while(_g < length) {
			var i = _g++;
			destData[destPosition] = srcData[srcPosition];
			srcPosition += 4;
			destPosition += 4;
			if(srcPosition % srcStride > srcRowEnd) srcPosition += srcRowOffset;
			if(destPosition % destStride > destRowEnd) destPosition += destRowOffset;
		}
		this.__sourceImageDataChanged = true;
	}
	,copyPixels: function(sourceBitmapData,sourceRect,destPoint,alphaBitmapData,alphaPoint,mergeAlpha) {
		if(mergeAlpha == null) mergeAlpha = false;
		if(!this.__valid || sourceBitmapData == null) return;
		if(sourceRect.x + sourceRect.width > sourceBitmapData.width) sourceRect.width = sourceBitmapData.width - sourceRect.x;
		if(sourceRect.y + sourceRect.height > sourceBitmapData.height) sourceRect.height = sourceBitmapData.height - sourceRect.y;
		if(sourceRect.width <= 0 || sourceRect.height <= 0) return;
		if(alphaBitmapData != null && alphaBitmapData.transparent) {
			if(alphaPoint == null) alphaPoint = new openfl.geom.Point();
			var tempData = this.clone();
			tempData.copyChannel(alphaBitmapData,new openfl.geom.Rectangle(alphaPoint.x,alphaPoint.y,sourceRect.width,sourceRect.height),new openfl.geom.Point(sourceRect.x,sourceRect.y),8,8);
			sourceBitmapData = tempData;
		}
		this.__syncImageData();
		if(!mergeAlpha) {
			if(this.transparent && sourceBitmapData.transparent) this.__sourceContext.clearRect(destPoint.x,destPoint.y,sourceRect.width,sourceRect.height);
		}
		sourceBitmapData.__syncImageData();
		if(sourceBitmapData.__sourceImage != null) this.__sourceContext.drawImage(sourceBitmapData.__sourceImage,sourceRect.x | 0,sourceRect.y | 0,sourceRect.width | 0,sourceRect.height | 0,destPoint.x | 0,destPoint.y | 0,sourceRect.width | 0,sourceRect.height | 0); else if(sourceBitmapData.__sourceCanvas != null) this.__sourceContext.drawImage(sourceBitmapData.__sourceCanvas,sourceRect.x | 0,sourceRect.y | 0,sourceRect.width | 0,sourceRect.height | 0,destPoint.x | 0,destPoint.y | 0,sourceRect.width | 0,sourceRect.height | 0);
	}
	,dispose: function() {
		this.__sourceImage = null;
		this.__sourceCanvas = null;
		this.__sourceContext = null;
		this.width = 0;
		this.height = 0;
		this.rect = null;
		this.__valid = false;
	}
	,draw: function(source,matrix,colorTransform,blendMode,clipRect,smoothing) {
		if(smoothing == null) smoothing = false;
		if(!this.__valid) return;
		this.__convertToCanvas();
		this.__syncImageData();
		var renderSession = new openfl.display.RenderSession();
		renderSession.context = this.__sourceContext;
		renderSession.roundPixels = true;
		if(!smoothing) {
			this.__sourceContext.mozImageSmoothingEnabled = false;
			this.__sourceContext.webkitImageSmoothingEnabled = false;
			this.__sourceContext.imageSmoothingEnabled = false;
		}
		var matrixCache = source.__worldTransform;
		if(matrix != null) source.__worldTransform = matrix; else source.__worldTransform = new openfl.geom.Matrix();
		source.__updateChildren(false);
		source.__renderCanvas(renderSession);
		source.__worldTransform = matrixCache;
		source.__updateChildren(true);
		if(!smoothing) {
			this.__sourceContext.mozImageSmoothingEnabled = true;
			this.__sourceContext.webkitImageSmoothingEnabled = true;
			this.__sourceContext.imageSmoothingEnabled = true;
		}
		this.__sourceContext.setTransform(1,0,0,1,0,0);
	}
	,encode: function(rect,compressor,byteArray) {
		openfl.Lib.notImplemented("BitmapData.encode");
		return null;
	}
	,fillRect: function(rect,color) {
		rect = this.__clipRect(rect);
		if(!this.__valid || rect == null) return;
		this.__convertToCanvas();
		this.__syncImageData();
		if(rect.x == 0 && rect.y == 0 && rect.width == this.width && rect.height == this.height) {
			if(this.transparent && (color & -16777216) == 0) {
				this.__sourceCanvas.width = this.width;
				return;
			}
		}
		this.__fillRect(rect,color);
	}
	,floodFill: function(x,y,color) {
		if(!this.__valid) return;
		this.__convertToCanvas();
		this.__createImageData();
		var data = this.__sourceImageData.data;
		var offset = y * (this.width * 4) + x * 4;
		var hitColorR = data[offset];
		var hitColorG = data[offset + 1];
		var hitColorB = data[offset + 2];
		var hitColorA;
		if(this.transparent) hitColorA = data[offset + 3]; else hitColorA = 255;
		var r = (color & 16711680) >>> 16;
		var g = (color & 65280) >>> 8;
		var b = color & 255;
		var a;
		if(this.transparent) a = (color & -16777216) >>> 24; else a = 255;
		if(hitColorR == r && hitColorG == g && hitColorB == b && hitColorA == a) return;
		var dx = [0,-1,1,0];
		var dy = [-1,0,0,1];
		var queue = new Array();
		queue.push(x);
		queue.push(y);
		while(queue.length > 0) {
			var curPointY = queue.pop();
			var curPointX = queue.pop();
			var _g = 0;
			while(_g < 4) {
				var i = _g++;
				var nextPointX = curPointX + dx[i];
				var nextPointY = curPointY + dy[i];
				if(nextPointX < 0 || nextPointY < 0 || nextPointX >= this.width || nextPointY >= this.height) continue;
				var nextPointOffset = (nextPointY * this.width + nextPointX) * 4;
				if(data[nextPointOffset] == hitColorR && data[nextPointOffset + 1] == hitColorG && data[nextPointOffset + 2] == hitColorB && data[nextPointOffset + 3] == hitColorA) {
					data[nextPointOffset] = r;
					data[nextPointOffset + 1] = g;
					data[nextPointOffset + 2] = b;
					data[nextPointOffset + 3] = a;
					queue.push(nextPointX);
					queue.push(nextPointY);
				}
			}
		}
		this.__sourceImageDataChanged = true;
	}
	,generateFilterRect: function(sourceRect,filter) {
		return sourceRect.clone();
	}
	,getColorBoundsRect: function(mask,color,findColor) {
		if(findColor == null) findColor = true;
		return this.rect.clone();
	}
	,getPixel: function(x,y) {
		if(!this.__valid || x < 0 || y < 0 || x >= this.width || y >= this.height) return 0;
		this.__convertToCanvas();
		this.__createImageData();
		var offset = 4 * y * this.width + x * 4;
		return this.__sourceImageData.data[offset] << 16 | this.__sourceImageData.data[offset + 1] << 8 | this.__sourceImageData.data[offset + 2];
	}
	,getPixel32: function(x,y) {
		if(!this.__valid || x < 0 || y < 0 || x >= this.width || y >= this.height) return 0;
		this.__convertToCanvas();
		this.__createImageData();
		return this.__getInt32(4 * y * this.width + x * 4,this.__sourceImageData.data);
	}
	,getPixels: function(rect) {
		if(!this.__valid) return null;
		this.__convertToCanvas();
		this.__createImageData();
		var byteArray = new openfl.utils.ByteArray();
		if(rect == null || rect.equals(this.rect)) {
			byteArray.set_length(this.__sourceImageData.data.length);
			byteArray.byteView.set(this.__sourceImageData.data);
		} else {
			var srcData = this.__sourceImageData.data;
			var srcStride = this.width * 4 | 0;
			var srcPosition = rect.x * 4 + srcStride * rect.y | 0;
			var srcRowOffset = srcStride - (4 * rect.width | 0);
			var srcRowEnd = 4 * (rect.x + rect.width) | 0;
			var length = 4 * rect.width * rect.height | 0;
			if(byteArray.allocated < length) byteArray.___resizeBuffer(byteArray.allocated = Std.int(Math.max(length,byteArray.allocated * 2))); else if(byteArray.allocated > length) byteArray.___resizeBuffer(byteArray.allocated = length);
			byteArray.length = length;
			length;
			var _g = 0;
			while(_g < length) {
				var i = _g++;
				byteArray.__set(i,srcData[srcPosition++]);
				if(srcPosition % srcStride > srcRowEnd) srcPosition += srcRowOffset;
			}
		}
		byteArray.position = 0;
		return byteArray;
	}
	,getVector: function(rect) {
		var pixels = this.getPixels(rect);
		var result = openfl._Vector.Vector_Impl_._new();
		var _g1 = 0;
		var _g = pixels.length / 4 | 0;
		while(_g1 < _g) {
			var i = _g1++;
			var this1 = result;
			var x = pixels.readUnsignedInt();
			if(!this1.fixed) {
				this1.length++;
				if(this1.data.length < this1.length) {
					var data;
					var this2;
					this2 = new Array(this1.data.length + 10);
					data = this2;
					haxe.ds._Vector.Vector_Impl_.blit(this1.data,0,data,0,this1.data.length);
					this1.data = data;
				}
				this1.data[this1.length - 1] = x;
			}
			this1.length;
		}
		return result;
	}
	,histogram: function(hRect) {
		var rect;
		if(hRect != null) rect = hRect; else rect = new openfl.geom.Rectangle(0,0,this.width,this.height);
		var pixels = this.getPixels(rect);
		var result;
		var _g = [];
		var _g1 = 0;
		while(_g1 < 4) {
			var i = _g1++;
			_g.push((function($this) {
				var $r;
				var _g2 = [];
				{
					var _g3 = 0;
					while(_g3 < 256) {
						var j = _g3++;
						_g2.push(0);
					}
				}
				$r = _g2;
				return $r;
			}(this)));
		}
		result = _g;
		var _g21 = 0;
		var _g11 = pixels.length;
		while(_g21 < _g11) {
			var i1 = _g21++;
			++result[i1 % 4][pixels.readUnsignedByte()];
		}
		return result;
	}
	,hitTest: function(firstPoint,firstAlphaThreshold,secondObject,secondBitmapDataPoint,secondAlphaThreshold) {
		if(secondAlphaThreshold == null) secondAlphaThreshold = 1;
		if(!this.__valid) return false;
		openfl.Lib.notImplemented("BitmapData.hitTest");
		return false;
	}
	,lock: function() {
	}
	,noise: function(randomSeed,low,high,channelOptions,grayScale) {
		if(grayScale == null) grayScale = false;
		if(channelOptions == null) channelOptions = 7;
		if(high == null) high = 255;
		if(low == null) low = 0;
		if(!this.__valid) return;
		openfl.Lib.notImplemented("BitmapData.noise");
	}
	,paletteMap: function(sourceBitmapData,sourceRect,destPoint,redArray,greenArray,blueArray,alphaArray) {
		var memory = new openfl.utils.ByteArray();
		var sw = sourceRect.width | 0;
		var sh = sourceRect.height | 0;
		memory.set_length(sw * sh * 4);
		memory = this.getPixels(sourceRect);
		memory.position = 0;
		openfl.Memory.select(memory);
		var position;
		var pixelValue;
		var r;
		var g;
		var b;
		var color;
		var _g1 = 0;
		var _g = sh * sw;
		while(_g1 < _g) {
			var i = _g1++;
			position = i * 4;
			pixelValue = openfl.Memory.getI32(position);
			r = pixelValue >> 8 & 255;
			g = pixelValue >> 16 & 255;
			b = pixelValue >> 24 & 255;
			color = openfl.display.BitmapData.__flipPixel(-16777216 | redArray[r] | greenArray[g] | blueArray[b]);
			openfl.Memory.setI32(position,color);
		}
		memory.position = 0;
		var destRect = new openfl.geom.Rectangle(destPoint.x,destPoint.y,sw,sh);
		this.setPixels(destRect,memory);
		openfl.Memory.select(null);
	}
	,perlinNoise: function(baseX,baseY,numOctaves,randomSeed,stitch,fractalNoise,channelOptions,grayScale,offsets) {
		if(grayScale == null) grayScale = false;
		if(channelOptions == null) channelOptions = 7;
		openfl.Lib.notImplemented("BitmapData.perlinNoise");
	}
	,scroll: function(x,y) {
		openfl.Lib.notImplemented("BitmapData.scroll");
	}
	,setVector: function(rect,inputVector) {
		var byteArray = new openfl.utils.ByteArray();
		byteArray.set_length(inputVector.length * 4);
		var $it0 = $iterator(openfl._Vector.Vector_Impl_)(inputVector);
		while( $it0.hasNext() ) {
			var color = $it0.next();
			byteArray.writeUnsignedInt(color);
		}
		byteArray.position = 0;
		this.setPixels(rect,byteArray);
	}
	,setPixel: function(x,y,color) {
		if(!this.__valid || x < 0 || y < 0 || x >= this.width || y >= this.height) return;
		this.__convertToCanvas();
		this.__createImageData();
		var offset = 4 * y * this.width + x * 4;
		this.__sourceImageData.data[offset] = (color & 16711680) >>> 16;
		this.__sourceImageData.data[offset + 1] = (color & 65280) >>> 8;
		this.__sourceImageData.data[offset + 2] = color & 255;
		if(this.transparent) this.__sourceImageData.data[offset + 3] = 255;
		this.__sourceImageDataChanged = true;
	}
	,setPixel32: function(x,y,color) {
		if(!this.__valid || x < 0 || y < 0 || x >= this.width || y >= this.height) return;
		this.__convertToCanvas();
		this.__createImageData();
		var offset = 4 * y * this.width + x * 4;
		this.__sourceImageData.data[offset] = (color & 16711680) >>> 16;
		this.__sourceImageData.data[offset + 1] = (color & 65280) >>> 8;
		this.__sourceImageData.data[offset + 2] = color & 255;
		if(this.transparent) this.__sourceImageData.data[offset + 3] = (color & -16777216) >>> 24; else this.__sourceImageData.data[offset + 3] = 255;
		this.__sourceImageDataChanged = true;
	}
	,setPixels: function(rect,byteArray) {
		rect = this.__clipRect(rect);
		if(!this.__valid || rect == null) return;
		this.__convertToCanvas();
		var len = Math.round(4 * rect.width * rect.height);
		if(rect.x == 0 && rect.y == 0 && rect.width == this.width && rect.height == this.height) {
			if(this.__sourceImageData == null) this.__sourceImageData = this.__sourceContext.createImageData(this.width,this.height);
			this.__sourceImageData.data.set(byteArray.byteView);
		} else {
			this.__createImageData();
			var offset = Math.round(4 * this.width * rect.y + rect.x * 4);
			var pos = offset;
			var boundR = Math.round(4 * (rect.x + rect.width));
			var data = this.__sourceImageData.data;
			var _g = 0;
			while(_g < len) {
				var i = _g++;
				if(pos % (this.width * 4) > boundR - 1) pos += this.width * 4 - boundR;
				data[pos] = byteArray.readByte();
				pos++;
			}
		}
		this.__sourceImageDataChanged = true;
	}
	,threshold: function(sourceBitmapData,sourceRect,destPoint,operation,threshold,color,mask,copySource) {
		if(copySource == null) copySource = false;
		if(mask == null) mask = -1;
		if(color == null) color = 0;
		if(sourceBitmapData == this && sourceRect.equals(this.rect) && destPoint.x == 0 && destPoint.y == 0) {
			var hits = 0;
			threshold = (threshold & 255) << 24 | (threshold >> 8 & 255) << 16 | (threshold >> 16 & 255) << 8 | threshold >> 24 & 255;
			color = (color & 255) << 24 | (color >> 8 & 255) << 16 | (color >> 16 & 255) << 8 | color >> 24 & 255;
			var memory = new openfl.utils.ByteArray();
			memory.set_length(this.width * this.height * 4);
			memory = this.getPixels(this.rect);
			memory.position = 0;
			openfl.Memory.select(memory);
			var thresholdMask = threshold & mask;
			var width_yy;
			var position;
			var pixelMask;
			var pixelValue;
			var i;
			var test;
			var _g1 = 0;
			var _g = this.height;
			while(_g1 < _g) {
				var yy = _g1++;
				width_yy = this.width * yy;
				var _g3 = 0;
				var _g2 = this.width;
				while(_g3 < _g2) {
					var xx = _g3++;
					position = (width_yy + xx) * 4;
					pixelValue = openfl.Memory.getI32(position);
					pixelMask = pixelValue & mask;
					i = openfl.display.BitmapData.__ucompare(pixelMask,thresholdMask);
					test = false;
					if(operation == "==") test = i == 0; else if(operation == "<") test = i == -1; else if(operation == ">") test = i == 1; else if(operation == "!=") test = i != 0; else if(operation == "<=") test = i == 0 || i == -1; else if(operation == ">=") test = i == 0 || i == 1;
					if(test) {
						openfl.Memory.setI32(position,color);
						hits++;
					}
				}
			}
			memory.position = 0;
			this.setPixels(this.rect,memory);
			openfl.Memory.select(null);
			return hits;
		} else {
			var sx = sourceRect.x | 0;
			var sy = sourceRect.y | 0;
			var sw = sourceBitmapData.width | 0;
			var sh = sourceBitmapData.height | 0;
			var dx = destPoint.x | 0;
			var dy = destPoint.y | 0;
			var bw = this.width - sw - dx;
			var bh = this.height - sh - dy;
			var dw;
			if(bw < 0) dw = sw + (this.width - sw - dx); else dw = sw;
			var dh;
			if(bw < 0) dh = sh + (this.height - sh - dy); else dh = sh;
			var hits1 = 0;
			threshold = (threshold & 255) << 24 | (threshold >> 8 & 255) << 16 | (threshold >> 16 & 255) << 8 | threshold >> 24 & 255;
			color = (color & 255) << 24 | (color >> 8 & 255) << 16 | (color >> 16 & 255) << 8 | color >> 24 & 255;
			var canvasMemory = sw * sh * 4;
			var sourceMemory = 0;
			if(copySource) sourceMemory = sw * sh * 4;
			var totalMemory = canvasMemory + sourceMemory;
			var memory1 = new openfl.utils.ByteArray();
			if(memory1.allocated < totalMemory) memory1.___resizeBuffer(memory1.allocated = Std.int(Math.max(totalMemory,memory1.allocated * 2))); else if(memory1.allocated > totalMemory) memory1.___resizeBuffer(memory1.allocated = totalMemory);
			memory1.length = totalMemory;
			totalMemory;
			memory1.position = 0;
			var bitmapData = sourceBitmapData.clone();
			var pixels = bitmapData.getPixels(sourceRect);
			memory1.writeBytes(pixels);
			memory1.position = canvasMemory;
			if(copySource) memory1.writeBytes(pixels);
			memory1.position = 0;
			openfl.Memory.select(memory1);
			var thresholdMask1 = threshold & mask;
			var position1;
			var pixelMask1;
			var pixelValue1;
			var i1;
			var test1;
			var _g4 = 0;
			while(_g4 < dh) {
				var yy1 = _g4++;
				var _g11 = 0;
				while(_g11 < dw) {
					var xx1 = _g11++;
					position1 = (xx1 + sx + (yy1 + sy) * sw) * 4;
					pixelValue1 = openfl.Memory.getI32(position1);
					pixelMask1 = pixelValue1 & mask;
					i1 = openfl.display.BitmapData.__ucompare(pixelMask1,thresholdMask1);
					test1 = false;
					if(operation == "==") test1 = i1 == 0; else if(operation == "<") test1 = i1 == -1; else if(operation == ">") test1 = i1 == 1; else if(operation == "!=") test1 = i1 != 0; else if(operation == "<=") test1 = i1 == 0 || i1 == -1; else if(operation == ">=") test1 = i1 == 0 || i1 == 1;
					if(test1) {
						openfl.Memory.setI32(position1,color);
						hits1++;
					} else if(copySource) openfl.Memory.setI32(position1,openfl.Memory.getI32(canvasMemory + position1));
				}
			}
			memory1.position = 0;
			bitmapData.setPixels(sourceRect,memory1);
			this.copyPixels(bitmapData,bitmapData.rect,destPoint);
			openfl.Memory.select(null);
			return hits1;
		}
	}
	,unlock: function(changeRect) {
	}
	,__clipRect: function(r) {
		if(r == null) return null;
		if(r.x < 0) {
			r.width -= -r.x;
			r.x = 0;
			if(r.x + r.width <= 0) return null;
		}
		if(r.y < 0) {
			r.height -= -r.y;
			r.y = 0;
			if(r.y + r.height <= 0) return null;
		}
		if(r.x + r.width >= this.width) {
			r.width -= r.x + r.width - this.width;
			if(r.width <= 0) return null;
		}
		if(r.y + r.height >= this.height) {
			r.height -= r.y + r.height - this.height;
			if(r.height <= 0) return null;
		}
		return r;
	}
	,__convertToCanvas: function() {
		if(this.__loading) return;
		if(this.__sourceImage != null) {
			if(this.__sourceCanvas == null) {
				this.__createCanvas(this.__sourceImage.width,this.__sourceImage.height);
				this.__sourceContext.drawImage(this.__sourceImage,0,0);
			}
			this.__sourceImage = null;
		}
	}
	,__createCanvas: function(width,height) {
		if(this.__sourceCanvas == null) {
			this.__sourceCanvas = window.document.createElement("canvas");
			this.__sourceCanvas.width = width;
			this.__sourceCanvas.height = height;
			if(!this.transparent) {
				if(!this.transparent) this.__sourceCanvas.setAttribute("moz-opaque","true");
				this.__sourceContext = this.__sourceCanvas.getContext ("2d", { alpha: false });
			} else this.__sourceContext = this.__sourceCanvas.getContext("2d");
			this.__sourceContext.mozImageSmoothingEnabled = false;
			this.__sourceContext.webkitImageSmoothingEnabled = false;
			this.__sourceContext.imageSmoothingEnabled = false;
			this.__valid = true;
		}
	}
	,__createImageData: function() {
		if(this.__sourceImageData == null) this.__sourceImageData = this.__sourceContext.getImageData(0,0,this.width,this.height);
	}
	,__fillRect: function(rect,color) {
		var a;
		if(this.transparent) a = (color & -16777216) >>> 24; else a = 255;
		var r = (color & 16711680) >>> 16;
		var g = (color & 65280) >>> 8;
		var b = color & 255;
		this.__sourceContext.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a / 255 + ")";
		this.__sourceContext.fillRect(rect.x,rect.y,rect.width,rect.height);
	}
	,__getInt32: function(offset,data) {
		return (this.transparent?data[offset + 3]:255) << 24 | data[offset] << 16 | data[offset + 1] << 8 | data[offset + 2];
	}
	,__loadFromBase64: function(base64,type,onload) {
		var _g = this;
		this.__sourceImage = window.document.createElement("img");
		var image_onLoaded = function(event) {
			if(_g.__sourceImage == null) _g.__sourceImage = event.target;
			_g.width = _g.__sourceImage.width;
			_g.height = _g.__sourceImage.height;
			_g.rect = new openfl.geom.Rectangle(0,0,_g.width,_g.height);
			_g.__valid = true;
			if(onload != null) onload(_g);
		};
		this.__sourceImage.addEventListener("load",image_onLoaded,false);
		this.__sourceImage.src = "data:" + type + ";base64," + base64;
	}
	,__loadFromBytes: function(bytes,rawAlpha,onload) {
		var _g = this;
		var type = "";
		if(openfl.display.BitmapData.__isPNG(bytes)) type = "image/png"; else if(openfl.display.BitmapData.__isJPG(bytes)) type = "image/jpeg"; else if(openfl.display.BitmapData.__isGIF(bytes)) type = "image/gif"; else throw new openfl.errors.IOError("BitmapData tried to read a PNG/JPG ByteArray, but found an invalid header.");
		if(rawAlpha != null) this.__loadFromBase64(openfl.display.BitmapData.__base64Encode(bytes),type,function(_) {
			_g.__convertToCanvas();
			_g.__createImageData();
			var data = _g.__sourceImageData.data;
			var _g2 = 0;
			var _g1 = rawAlpha.length;
			while(_g2 < _g1) {
				var i = _g2++;
				data[i * 4 + 3] = rawAlpha.readUnsignedByte();
			}
			_g.__sourceImageDataChanged = true;
			if(onload != null) onload(_g);
		}); else this.__loadFromBase64(openfl.display.BitmapData.__base64Encode(bytes),type,onload);
	}
	,__renderCanvas: function(renderSession) {
		if(!this.__valid) return;
		this.__syncImageData();
		var context = renderSession.context;
		if(this.__worldTransform == null) this.__worldTransform = new openfl.geom.Matrix();
		context.globalAlpha = 1;
		var transform = this.__worldTransform;
		if(renderSession.roundPixels) context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx | 0,transform.ty | 0); else context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx,transform.ty);
		if(this.__sourceImage != null) context.drawImage(this.__sourceImage,0,0); else context.drawImage(this.__sourceCanvas,0,0);
	}
	,__renderMask: function(renderSession) {
	}
	,__syncImageData: function() {
		if(this.__sourceImageDataChanged) {
			this.__sourceContext.putImageData(this.__sourceImageData,0,0);
			this.__sourceImageData = null;
			this.__sourceImageDataChanged = false;
		}
	}
	,__updateChildren: function(transformOnly) {
	}
	,__class__: openfl.display.BitmapData
};
openfl.display.BitmapDataChannel = function() { };
$hxClasses["openfl.display.BitmapDataChannel"] = openfl.display.BitmapDataChannel;
openfl.display.BitmapDataChannel.__name__ = ["openfl","display","BitmapDataChannel"];
openfl.display.BlendMode = $hxClasses["openfl.display.BlendMode"] = { __ename__ : ["openfl","display","BlendMode"], __constructs__ : ["ADD","ALPHA","DARKEN","DIFFERENCE","ERASE","HARDLIGHT","INVERT","LAYER","LIGHTEN","MULTIPLY","NORMAL","OVERLAY","SCREEN","SUBTRACT"] };
openfl.display.BlendMode.ADD = ["ADD",0];
openfl.display.BlendMode.ADD.toString = $estr;
openfl.display.BlendMode.ADD.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.ALPHA = ["ALPHA",1];
openfl.display.BlendMode.ALPHA.toString = $estr;
openfl.display.BlendMode.ALPHA.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.DARKEN = ["DARKEN",2];
openfl.display.BlendMode.DARKEN.toString = $estr;
openfl.display.BlendMode.DARKEN.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.DIFFERENCE = ["DIFFERENCE",3];
openfl.display.BlendMode.DIFFERENCE.toString = $estr;
openfl.display.BlendMode.DIFFERENCE.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.ERASE = ["ERASE",4];
openfl.display.BlendMode.ERASE.toString = $estr;
openfl.display.BlendMode.ERASE.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.HARDLIGHT = ["HARDLIGHT",5];
openfl.display.BlendMode.HARDLIGHT.toString = $estr;
openfl.display.BlendMode.HARDLIGHT.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.INVERT = ["INVERT",6];
openfl.display.BlendMode.INVERT.toString = $estr;
openfl.display.BlendMode.INVERT.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.LAYER = ["LAYER",7];
openfl.display.BlendMode.LAYER.toString = $estr;
openfl.display.BlendMode.LAYER.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.LIGHTEN = ["LIGHTEN",8];
openfl.display.BlendMode.LIGHTEN.toString = $estr;
openfl.display.BlendMode.LIGHTEN.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.MULTIPLY = ["MULTIPLY",9];
openfl.display.BlendMode.MULTIPLY.toString = $estr;
openfl.display.BlendMode.MULTIPLY.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.NORMAL = ["NORMAL",10];
openfl.display.BlendMode.NORMAL.toString = $estr;
openfl.display.BlendMode.NORMAL.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.OVERLAY = ["OVERLAY",11];
openfl.display.BlendMode.OVERLAY.toString = $estr;
openfl.display.BlendMode.OVERLAY.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.SCREEN = ["SCREEN",12];
openfl.display.BlendMode.SCREEN.toString = $estr;
openfl.display.BlendMode.SCREEN.__enum__ = openfl.display.BlendMode;
openfl.display.BlendMode.SUBTRACT = ["SUBTRACT",13];
openfl.display.BlendMode.SUBTRACT.toString = $estr;
openfl.display.BlendMode.SUBTRACT.__enum__ = openfl.display.BlendMode;
openfl.display.CapsStyle = $hxClasses["openfl.display.CapsStyle"] = { __ename__ : ["openfl","display","CapsStyle"], __constructs__ : ["NONE","ROUND","SQUARE"] };
openfl.display.CapsStyle.NONE = ["NONE",0];
openfl.display.CapsStyle.NONE.toString = $estr;
openfl.display.CapsStyle.NONE.__enum__ = openfl.display.CapsStyle;
openfl.display.CapsStyle.ROUND = ["ROUND",1];
openfl.display.CapsStyle.ROUND.toString = $estr;
openfl.display.CapsStyle.ROUND.__enum__ = openfl.display.CapsStyle;
openfl.display.CapsStyle.SQUARE = ["SQUARE",2];
openfl.display.CapsStyle.SQUARE.toString = $estr;
openfl.display.CapsStyle.SQUARE.__enum__ = openfl.display.CapsStyle;
openfl.display.FrameLabel = function(name,frame) {
	openfl.events.EventDispatcher.call(this);
	this.__name = name;
	this.__frame = frame;
};
$hxClasses["openfl.display.FrameLabel"] = openfl.display.FrameLabel;
openfl.display.FrameLabel.__name__ = ["openfl","display","FrameLabel"];
openfl.display.FrameLabel.__super__ = openfl.events.EventDispatcher;
openfl.display.FrameLabel.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	get_frame: function() {
		return this.__frame;
	}
	,get_name: function() {
		return this.__name;
	}
	,__class__: openfl.display.FrameLabel
	,__properties__: {get_name:"get_name",get_frame:"get_frame"}
});
openfl.display.GradientType = $hxClasses["openfl.display.GradientType"] = { __ename__ : ["openfl","display","GradientType"], __constructs__ : ["RADIAL","LINEAR"] };
openfl.display.GradientType.RADIAL = ["RADIAL",0];
openfl.display.GradientType.RADIAL.toString = $estr;
openfl.display.GradientType.RADIAL.__enum__ = openfl.display.GradientType;
openfl.display.GradientType.LINEAR = ["LINEAR",1];
openfl.display.GradientType.LINEAR.toString = $estr;
openfl.display.GradientType.LINEAR.__enum__ = openfl.display.GradientType;
openfl.display.Graphics = function() {
	this.__commands = new Array();
	this.__halfStrokeWidth = 0;
	this.__positionX = 0;
	this.__positionY = 0;
};
$hxClasses["openfl.display.Graphics"] = openfl.display.Graphics;
openfl.display.Graphics.__name__ = ["openfl","display","Graphics"];
openfl.display.Graphics.prototype = {
	beginBitmapFill: function(bitmap,matrix,repeat,smooth) {
		if(smooth == null) smooth = false;
		if(repeat == null) repeat = true;
		this.__commands.push(openfl.display.DrawCommand.BeginBitmapFill(bitmap,matrix,repeat,smooth));
		this.__visible = true;
	}
	,beginFill: function(rgb,alpha) {
		if(alpha == null) alpha = 1;
		this.__commands.push(openfl.display.DrawCommand.BeginFill(rgb & 16777215,alpha));
		if(alpha > 0) this.__visible = true;
	}
	,beginGradientFill: function(type,colors,alphas,ratios,matrix,spreadMethod,interpolationMethod,focalPointRatio) {
		openfl.Lib.notImplemented("Graphics.beginGradientFill");
	}
	,clear: function() {
		this.__commands = new Array();
		this.__halfStrokeWidth = 0;
		if(this.__bounds != null) {
			this.__dirty = true;
			this.__bounds = null;
		}
		this.__visible = false;
	}
	,curveTo: function(cx,cy,x,y) {
		this.__inflateBounds(this.__positionX - this.__halfStrokeWidth,this.__positionY - this.__halfStrokeWidth);
		this.__inflateBounds(this.__positionX + this.__halfStrokeWidth,this.__positionY + this.__halfStrokeWidth);
		this.__inflateBounds(cx,cy);
		this.__positionX = x;
		this.__positionY = y;
		this.__inflateBounds(this.__positionX - this.__halfStrokeWidth,this.__positionY - this.__halfStrokeWidth);
		this.__inflateBounds(this.__positionX + this.__halfStrokeWidth,this.__positionY + this.__halfStrokeWidth);
		this.__commands.push(openfl.display.DrawCommand.CurveTo(cx,cy,x,y));
		this.__dirty = true;
	}
	,drawCircle: function(x,y,radius) {
		if(radius <= 0) return;
		this.__inflateBounds(x - radius - this.__halfStrokeWidth,y - radius - this.__halfStrokeWidth);
		this.__inflateBounds(x + radius + this.__halfStrokeWidth,y + radius + this.__halfStrokeWidth);
		this.__commands.push(openfl.display.DrawCommand.DrawCircle(x,y,radius));
		this.__dirty = true;
	}
	,drawEllipse: function(x,y,width,height) {
		if(width <= 0 || height <= 0) return;
		this.__inflateBounds(x - this.__halfStrokeWidth,y - this.__halfStrokeWidth);
		this.__inflateBounds(x + width + this.__halfStrokeWidth,y + height + this.__halfStrokeWidth);
		this.__commands.push(openfl.display.DrawCommand.DrawEllipse(x,y,width,height));
		this.__dirty = true;
	}
	,drawGraphicsData: function(graphicsData) {
		openfl.Lib.notImplemented("Graphics.drawGraphicsData");
	}
	,drawPath: function(commands,data,winding) {
		openfl.Lib.notImplemented("Graphics.drawPath");
	}
	,drawRect: function(x,y,width,height) {
		if(width <= 0 || height <= 0) return;
		this.__inflateBounds(x - this.__halfStrokeWidth,y - this.__halfStrokeWidth);
		this.__inflateBounds(x + width + this.__halfStrokeWidth,y + height + this.__halfStrokeWidth);
		this.__commands.push(openfl.display.DrawCommand.DrawRect(x,y,width,height));
		this.__dirty = true;
	}
	,drawRoundRect: function(x,y,width,height,rx,ry) {
		if(ry == null) ry = -1;
		openfl.Lib.notImplemented("Graphics.drawRoundRect");
	}
	,drawRoundRectComplex: function(x,y,width,height,topLeftRadius,topRightRadius,bottomLeftRadius,bottomRightRadius) {
		openfl.Lib.notImplemented("Graphics.drawRoundRectComplex");
	}
	,drawTiles: function(sheet,tileData,smooth,flags,count) {
		if(count == null) count = -1;
		if(flags == null) flags = 0;
		if(smooth == null) smooth = false;
		this.__inflateBounds(0,0);
		this.__inflateBounds(openfl.Lib.current.stage.stageWidth,openfl.Lib.current.stage.stageHeight);
		this.__commands.push(openfl.display.DrawCommand.DrawTiles(sheet,tileData,smooth,flags,count));
		this.__dirty = true;
		this.__visible = true;
	}
	,drawTriangles: function(vertices,indices,uvtData,culling) {
		openfl.Lib.notImplemented("Graphics.drawTriangles");
	}
	,endFill: function() {
		this.__commands.push(openfl.display.DrawCommand.EndFill);
	}
	,lineBitmapStyle: function(bitmap,matrix,repeat,smooth) {
		if(smooth == null) smooth = false;
		if(repeat == null) repeat = true;
		openfl.Lib.notImplemented("Graphics.lineBitmapStyle");
	}
	,lineGradientStyle: function(type,colors,alphas,ratios,matrix,spreadMethod,interpolationMethod,focalPointRatio) {
		openfl.Lib.notImplemented("Graphics.lineGradientStyle");
	}
	,lineStyle: function(thickness,color,alpha,pixelHinting,scaleMode,caps,joints,miterLimit) {
		if(thickness != null) this.__halfStrokeWidth = thickness / 2; else this.__halfStrokeWidth = 0;
		this.__commands.push(openfl.display.DrawCommand.LineStyle(thickness,color,alpha,pixelHinting,scaleMode,caps,joints,miterLimit));
		if(thickness != null) this.__visible = true;
	}
	,lineTo: function(x,y) {
		this.__inflateBounds(this.__positionX - this.__halfStrokeWidth,this.__positionY - this.__halfStrokeWidth);
		this.__inflateBounds(this.__positionX + this.__halfStrokeWidth,this.__positionY + this.__halfStrokeWidth);
		this.__positionX = x;
		this.__positionY = y;
		this.__inflateBounds(this.__positionX - this.__halfStrokeWidth,this.__positionY - this.__halfStrokeWidth);
		this.__inflateBounds(this.__positionX + this.__halfStrokeWidth,this.__positionY + this.__halfStrokeWidth);
		this.__commands.push(openfl.display.DrawCommand.LineTo(x,y));
		this.__dirty = true;
	}
	,moveTo: function(x,y) {
		this.__commands.push(openfl.display.DrawCommand.MoveTo(x,y));
		this.__positionX = x;
		this.__positionY = y;
	}
	,__beginPath: function() {
		if(!this.__inPath) {
			this.__context.beginPath();
			this.__inPath = true;
		}
	}
	,__beginPatternFill: function(bitmapFill,bitmapRepeat) {
		if(this.__setFill || bitmapFill == null) return;
		if(this.__pattern == null) {
			if(bitmapFill.__sourceImage != null) this.__pattern = this.__context.createPattern(bitmapFill.__sourceImage,bitmapRepeat?"repeat":"no-repeat"); else this.__pattern = this.__context.createPattern(bitmapFill.__sourceCanvas,bitmapRepeat?"repeat":"no-repeat");
		}
		this.__context.fillStyle = this.__pattern;
		this.__setFill = true;
	}
	,__closePath: function(closeFill) {
		if(this.__inPath) {
			if(this.__hasFill) {
				this.__context.translate(-this.__bounds.x,-this.__bounds.y);
				if(this.__pendingMatrix != null) {
					this.__context.transform(this.__pendingMatrix.a,this.__pendingMatrix.b,this.__pendingMatrix.c,this.__pendingMatrix.d,this.__pendingMatrix.tx,this.__pendingMatrix.ty);
					this.__context.fill();
					this.__context.transform(this.__inversePendingMatrix.a,this.__inversePendingMatrix.b,this.__inversePendingMatrix.c,this.__inversePendingMatrix.d,this.__inversePendingMatrix.tx,this.__inversePendingMatrix.ty);
				} else this.__context.fill();
				this.__context.translate(this.__bounds.x,this.__bounds.y);
			}
			this.__context.closePath();
			if(this.__hasStroke) this.__context.stroke();
		}
		this.__inPath = false;
		if(closeFill) {
			this.__hasFill = false;
			this.__hasStroke = false;
			this.__pendingMatrix = null;
			this.__inversePendingMatrix = null;
		}
	}
	,__getBounds: function(rect,matrix) {
		if(this.__bounds == null) return;
		var bounds = this.__bounds.clone().transform(matrix);
		rect.__expand(bounds.x,bounds.y,bounds.width,bounds.height);
	}
	,__hitTest: function(x,y,shapeFlag,matrix) {
		if(this.__bounds == null) return false;
		var bounds = this.__bounds.clone().transform(matrix);
		return x > bounds.x && y > bounds.y && x <= bounds.get_right() && y <= bounds.get_bottom();
	}
	,__inflateBounds: function(x,y) {
		if(this.__bounds == null) {
			this.__bounds = new openfl.geom.Rectangle(x,y,0,0);
			return;
		}
		if(x < this.__bounds.x) {
			this.__bounds.width += this.__bounds.x - x;
			this.__bounds.x = x;
		}
		if(y < this.__bounds.y) {
			this.__bounds.height += this.__bounds.y - y;
			this.__bounds.y = y;
		}
		if(x > this.__bounds.x + this.__bounds.width) this.__bounds.width = x - this.__bounds.x;
		if(y > this.__bounds.y + this.__bounds.height) this.__bounds.height = y - this.__bounds.y;
	}
	,__render: function() {
		if(this.__dirty) {
			this.__hasFill = false;
			this.__hasStroke = false;
			this.__inPath = false;
			this.__positionX = 0;
			this.__positionY = 0;
			if(!this.__visible || this.__commands.length == 0 || this.__bounds == null || this.__bounds.width == 0 || this.__bounds.height == 0) {
				this.__canvas = null;
				this.__context = null;
			} else {
				if(this.__canvas == null) {
					this.__canvas = window.document.createElement("canvas");
					this.__context = this.__canvas.getContext("2d");
				}
				this.__canvas.width = Math.ceil(this.__bounds.width);
				this.__canvas.height = Math.ceil(this.__bounds.height);
				var offsetX = this.__bounds.x;
				var offsetY = this.__bounds.y;
				var bitmapFill = null;
				var bitmapRepeat = false;
				var _g = 0;
				var _g1 = this.__commands;
				while(_g < _g1.length) {
					var command = _g1[_g];
					++_g;
					switch(command[1]) {
					case 0:
						var smooth = command[5];
						var repeat = command[4];
						var matrix = command[3];
						var bitmap = command[2];
						this.__closePath(false);
						if(bitmap != bitmapFill || repeat != bitmapRepeat) {
							bitmapFill = bitmap;
							bitmapRepeat = repeat;
							this.__pattern = null;
							this.__setFill = false;
							bitmap.__syncImageData();
						}
						if(matrix != null) {
							this.__pendingMatrix = matrix;
							this.__inversePendingMatrix = new openfl.geom.Matrix(matrix.a,matrix.b,matrix.c,matrix.d,matrix.tx,matrix.ty);
							this.__inversePendingMatrix.invert();
						} else {
							this.__pendingMatrix = null;
							this.__inversePendingMatrix = null;
						}
						this.__hasFill = true;
						break;
					case 1:
						var alpha = command[3];
						var rgb = command[2];
						this.__closePath(false);
						if(alpha == 1) this.__context.fillStyle = "#" + StringTools.hex(rgb,6); else {
							var r = (rgb & 16711680) >>> 16;
							var g = (rgb & 65280) >>> 8;
							var b = rgb & 255;
							this.__context.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
						}
						bitmapFill = null;
						this.__setFill = true;
						this.__hasFill = true;
						break;
					case 2:
						var y = command[5];
						var x = command[4];
						var cy = command[3];
						var cx = command[2];
						this.__beginPatternFill(bitmapFill,bitmapRepeat);
						this.__beginPath();
						this.__context.quadraticCurveTo(cx - offsetX,cy - offsetY,x - offsetX,y - offsetY);
						this.__positionX = x;
						this.__positionY = y;
						break;
					case 3:
						var radius = command[4];
						var y1 = command[3];
						var x1 = command[2];
						this.__beginPatternFill(bitmapFill,bitmapRepeat);
						this.__beginPath();
						this.__context.moveTo(x1 - offsetX + radius,y1 - offsetY);
						this.__context.arc(x1 - offsetX,y1 - offsetY,radius,0,Math.PI * 2,true);
						break;
					case 4:
						var height = command[5];
						var width = command[4];
						var y2 = command[3];
						var x2 = command[2];
						x2 -= offsetX;
						y2 -= offsetY;
						var kappa = .5522848;
						var ox = width / 2 * kappa;
						var oy = height / 2 * kappa;
						var xe = x2 + width;
						var ye = y2 + height;
						var xm = x2 + width / 2;
						var ym = y2 + height / 2;
						this.__beginPatternFill(bitmapFill,bitmapRepeat);
						this.__beginPath();
						this.__context.moveTo(x2,ym);
						this.__context.bezierCurveTo(x2,ym - oy,xm - ox,y2,xm,y2);
						this.__context.bezierCurveTo(xm + ox,y2,xe,ym - oy,xe,ym);
						this.__context.bezierCurveTo(xe,ym + oy,xm + ox,ye,xm,ye);
						this.__context.bezierCurveTo(xm - ox,ye,x2,ym + oy,x2,ym);
						break;
					case 5:
						var height1 = command[5];
						var width1 = command[4];
						var y3 = command[3];
						var x3 = command[2];
						var optimizationUsed = false;
						if(bitmapFill != null) {
							var st = 0;
							var sr = 0;
							var sb = 0;
							var sl = 0;
							var canOptimizeMatrix = true;
							if(this.__pendingMatrix != null) {
								if(this.__pendingMatrix.b != 0 || this.__pendingMatrix.c != 0) canOptimizeMatrix = false; else {
									var stl = this.__inversePendingMatrix.transformPoint(new openfl.geom.Point(x3,y3));
									var sbr = this.__inversePendingMatrix.transformPoint(new openfl.geom.Point(x3 + width1,y3 + height1));
									st = stl.y;
									sl = stl.x;
									sb = sbr.y;
									sr = sbr.x;
								}
							} else {
								st = y3;
								sl = x3;
								sb = y3 + height1;
								sr = x3 + width1;
							}
							if(canOptimizeMatrix && st >= 0 && sl >= 0 && sr <= bitmapFill.width && sb <= bitmapFill.height) {
								optimizationUsed = true;
								if(bitmapFill.__sourceImage != null) this.__context.drawImage(bitmapFill.__sourceImage,sl,st,sr - sl,sb - st,x3,y3,width1,height1); else this.__context.drawImage(bitmapFill.__sourceCanvas,sl,st,sr - sl,sb - st,x3,y3,width1,height1);
							}
						}
						if(!optimizationUsed) {
							this.__beginPatternFill(bitmapFill,bitmapRepeat);
							this.__beginPath();
							this.__context.rect(x3 - offsetX,y3 - offsetY,width1,height1);
						}
						break;
					case 6:
						var count = command[6];
						var flags = command[5];
						var smooth1 = command[4];
						var tileData = command[3];
						var sheet = command[2];
						this.__closePath(false);
						var useScale = (flags & 1) > 0;
						var useRotation = (flags & 2) > 0;
						var useTransform = (flags & 16) > 0;
						var useRGB = (flags & 4) > 0;
						var useAlpha = (flags & 8) > 0;
						if(useTransform) {
							useScale = false;
							useRotation = false;
						}
						var scaleIndex = 0;
						var rotationIndex = 0;
						var rgbIndex = 0;
						var alphaIndex = 0;
						var transformIndex = 0;
						var numValues = 3;
						if(useScale) {
							scaleIndex = numValues;
							numValues++;
						}
						if(useRotation) {
							rotationIndex = numValues;
							numValues++;
						}
						if(useTransform) {
							transformIndex = numValues;
							numValues += 4;
						}
						if(useRGB) {
							rgbIndex = numValues;
							numValues += 3;
						}
						if(useAlpha) {
							alphaIndex = numValues;
							numValues++;
						}
						var totalCount = tileData.length;
						if(count >= 0 && totalCount > count) totalCount = count;
						var itemCount = totalCount / numValues | 0;
						var index = 0;
						var rect = null;
						var center = null;
						var previousTileID = -1;
						var surface;
						sheet.__bitmap.__syncImageData();
						if(sheet.__bitmap.__sourceImage != null) surface = sheet.__bitmap.__sourceImage; else surface = sheet.__bitmap.__sourceCanvas;
						while(index < totalCount) {
							var tileID = tileData[index + 2] | 0;
							if(tileID != previousTileID) {
								rect = sheet.__tileRects[tileID];
								center = sheet.__centerPoints[tileID];
								previousTileID = tileID;
							}
							if(rect != null && rect.width > 0 && rect.height > 0 && center != null) {
								this.__context.save();
								this.__context.translate(tileData[index],tileData[index + 1]);
								if(useRotation) this.__context.rotate(tileData[index + rotationIndex]);
								var scale = 1.0;
								if(useScale) scale = tileData[index + scaleIndex];
								if(useTransform) this.__context.transform(tileData[index + transformIndex],tileData[index + transformIndex + 1],tileData[index + transformIndex + 2],tileData[index + transformIndex + 3],0,0);
								if(useAlpha) this.__context.globalAlpha = tileData[index + alphaIndex];
								this.__context.drawImage(surface,rect.x,rect.y,rect.width,rect.height,-center.x * scale,-center.y * scale,rect.width * scale,rect.height * scale);
								this.__context.restore();
							}
							index += numValues;
						}
						break;
					case 7:
						this.__closePath(true);
						break;
					case 8:
						var miterLimit = command[9];
						var joints = command[8];
						var caps = command[7];
						var scaleMode = command[6];
						var pixelHinting = command[5];
						var alpha1 = command[4];
						var color = command[3];
						var thickness = command[2];
						this.__closePath(false);
						if(thickness == null) this.__hasStroke = false; else {
							this.__context.lineWidth = thickness;
							this.__context.lineJoin = Std.string(joints).toLowerCase();
							switch(caps[1]) {
							case 0:
								this.__context.lineCap = "butt";
								break;
							default:
								this.__context.lineCap = Std.string(caps).toLowerCase();
							}
							this.__context.miterLimit = miterLimit;
							this.__context.strokeStyle = "#" + StringTools.hex(color,6);
							this.__hasStroke = true;
						}
						break;
					case 9:
						var y4 = command[3];
						var x4 = command[2];
						this.__beginPatternFill(bitmapFill,bitmapRepeat);
						this.__beginPath();
						this.__context.lineTo(x4 - offsetX,y4 - offsetY);
						this.__positionX = x4;
						this.__positionY = y4;
						break;
					case 10:
						var y5 = command[3];
						var x5 = command[2];
						this.__beginPath();
						this.__context.moveTo(x5 - offsetX,y5 - offsetY);
						this.__positionX = x5;
						this.__positionY = y5;
						break;
					}
				}
			}
			this.__dirty = false;
			this.__closePath(false);
		}
	}
	,__renderMask: function(renderSession) {
		if(this.__commands.length != 0) {
			var __context = renderSession.context;
			var __positionX = 0.0;
			var __positionY = 0.0;
			var offsetX = 0;
			var offsetY = 0;
			var _g = 0;
			var _g1 = this.__commands;
			while(_g < _g1.length) {
				var command = _g1[_g];
				++_g;
				switch(command[1]) {
				case 2:
					var y = command[5];
					var x = command[4];
					var cy = command[3];
					var cx = command[2];
					__context.quadraticCurveTo(cx,cy,x,y);
					__positionX = x;
					__positionY = y;
					break;
				case 3:
					var radius = command[4];
					var y1 = command[3];
					var x1 = command[2];
					__context.arc(x1 - offsetX,y1 - offsetY,radius,0,Math.PI * 2,true);
					break;
				case 4:
					var height = command[5];
					var width = command[4];
					var y2 = command[3];
					var x2 = command[2];
					x2 -= offsetX;
					y2 -= offsetY;
					var kappa = .5522848;
					var ox = width / 2 * kappa;
					var oy = height / 2 * kappa;
					var xe = x2 + width;
					var ye = y2 + height;
					var xm = x2 + width / 2;
					var ym = y2 + height / 2;
					__context.moveTo(x2,ym);
					__context.bezierCurveTo(x2,ym - oy,xm - ox,y2,xm,y2);
					__context.bezierCurveTo(xm + ox,y2,xe,ym - oy,xe,ym);
					__context.bezierCurveTo(xe,ym + oy,xm + ox,ye,xm,ye);
					__context.bezierCurveTo(xm - ox,ye,x2,ym + oy,x2,ym);
					break;
				case 5:
					var height1 = command[5];
					var width1 = command[4];
					var y3 = command[3];
					var x3 = command[2];
					__context.rect(x3 - offsetX,y3 - offsetY,width1,height1);
					break;
				case 9:
					var y4 = command[3];
					var x4 = command[2];
					__context.lineTo(x4 - offsetX,y4 - offsetY);
					__positionX = x4;
					__positionY = y4;
					break;
				case 10:
					var y5 = command[3];
					var x5 = command[2];
					__context.moveTo(x5 - offsetX,y5 - offsetY);
					__positionX = x5;
					__positionY = y5;
					break;
				default:
				}
			}
		}
	}
	,__class__: openfl.display.Graphics
};
openfl.display.DrawCommand = $hxClasses["openfl.display.DrawCommand"] = { __ename__ : ["openfl","display","DrawCommand"], __constructs__ : ["BeginBitmapFill","BeginFill","CurveTo","DrawCircle","DrawEllipse","DrawRect","DrawTiles","EndFill","LineStyle","LineTo","MoveTo"] };
openfl.display.DrawCommand.BeginBitmapFill = function(bitmap,matrix,repeat,smooth) { var $x = ["BeginBitmapFill",0,bitmap,matrix,repeat,smooth]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.BeginFill = function(rgb,alpha) { var $x = ["BeginFill",1,rgb,alpha]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.CurveTo = function(cx,cy,x,y) { var $x = ["CurveTo",2,cx,cy,x,y]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.DrawCircle = function(x,y,radius) { var $x = ["DrawCircle",3,x,y,radius]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.DrawEllipse = function(x,y,width,height) { var $x = ["DrawEllipse",4,x,y,width,height]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.DrawRect = function(x,y,width,height) { var $x = ["DrawRect",5,x,y,width,height]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.DrawTiles = function(sheet,tileData,smooth,flags,count) { var $x = ["DrawTiles",6,sheet,tileData,smooth,flags,count]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.EndFill = ["EndFill",7];
openfl.display.DrawCommand.EndFill.toString = $estr;
openfl.display.DrawCommand.EndFill.__enum__ = openfl.display.DrawCommand;
openfl.display.DrawCommand.LineStyle = function(thickness,color,alpha,pixelHinting,scaleMode,caps,joints,miterLimit) { var $x = ["LineStyle",8,thickness,color,alpha,pixelHinting,scaleMode,caps,joints,miterLimit]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.LineTo = function(x,y) { var $x = ["LineTo",9,x,y]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.DrawCommand.MoveTo = function(x,y) { var $x = ["MoveTo",10,x,y]; $x.__enum__ = openfl.display.DrawCommand; $x.toString = $estr; return $x; };
openfl.display.GraphicsPathWinding = $hxClasses["openfl.display.GraphicsPathWinding"] = { __ename__ : ["openfl","display","GraphicsPathWinding"], __constructs__ : ["EVEN_ODD","NON_ZERO"] };
openfl.display.GraphicsPathWinding.EVEN_ODD = ["EVEN_ODD",0];
openfl.display.GraphicsPathWinding.EVEN_ODD.toString = $estr;
openfl.display.GraphicsPathWinding.EVEN_ODD.__enum__ = openfl.display.GraphicsPathWinding;
openfl.display.GraphicsPathWinding.NON_ZERO = ["NON_ZERO",1];
openfl.display.GraphicsPathWinding.NON_ZERO.toString = $estr;
openfl.display.GraphicsPathWinding.NON_ZERO.__enum__ = openfl.display.GraphicsPathWinding;
openfl.display.IGraphicsData = function() { };
$hxClasses["openfl.display.IGraphicsData"] = openfl.display.IGraphicsData;
openfl.display.IGraphicsData.__name__ = ["openfl","display","IGraphicsData"];
openfl.display.IGraphicsData.prototype = {
	__class__: openfl.display.IGraphicsData
};
openfl.display.GraphicsDataType = $hxClasses["openfl.display.GraphicsDataType"] = { __ename__ : ["openfl","display","GraphicsDataType"], __constructs__ : ["STROKE","SOLID","GRADIENT","PATH","BITMAP","END"] };
openfl.display.GraphicsDataType.STROKE = ["STROKE",0];
openfl.display.GraphicsDataType.STROKE.toString = $estr;
openfl.display.GraphicsDataType.STROKE.__enum__ = openfl.display.GraphicsDataType;
openfl.display.GraphicsDataType.SOLID = ["SOLID",1];
openfl.display.GraphicsDataType.SOLID.toString = $estr;
openfl.display.GraphicsDataType.SOLID.__enum__ = openfl.display.GraphicsDataType;
openfl.display.GraphicsDataType.GRADIENT = ["GRADIENT",2];
openfl.display.GraphicsDataType.GRADIENT.toString = $estr;
openfl.display.GraphicsDataType.GRADIENT.__enum__ = openfl.display.GraphicsDataType;
openfl.display.GraphicsDataType.PATH = ["PATH",3];
openfl.display.GraphicsDataType.PATH.toString = $estr;
openfl.display.GraphicsDataType.PATH.__enum__ = openfl.display.GraphicsDataType;
openfl.display.GraphicsDataType.BITMAP = ["BITMAP",4];
openfl.display.GraphicsDataType.BITMAP.toString = $estr;
openfl.display.GraphicsDataType.BITMAP.__enum__ = openfl.display.GraphicsDataType;
openfl.display.GraphicsDataType.END = ["END",5];
openfl.display.GraphicsDataType.END.toString = $estr;
openfl.display.GraphicsDataType.END.__enum__ = openfl.display.GraphicsDataType;
openfl.display.InterpolationMethod = $hxClasses["openfl.display.InterpolationMethod"] = { __ename__ : ["openfl","display","InterpolationMethod"], __constructs__ : ["RGB","LINEAR_RGB"] };
openfl.display.InterpolationMethod.RGB = ["RGB",0];
openfl.display.InterpolationMethod.RGB.toString = $estr;
openfl.display.InterpolationMethod.RGB.__enum__ = openfl.display.InterpolationMethod;
openfl.display.InterpolationMethod.LINEAR_RGB = ["LINEAR_RGB",1];
openfl.display.InterpolationMethod.LINEAR_RGB.toString = $estr;
openfl.display.InterpolationMethod.LINEAR_RGB.__enum__ = openfl.display.InterpolationMethod;
openfl.display.JointStyle = $hxClasses["openfl.display.JointStyle"] = { __ename__ : ["openfl","display","JointStyle"], __constructs__ : ["MITER","ROUND","BEVEL"] };
openfl.display.JointStyle.MITER = ["MITER",0];
openfl.display.JointStyle.MITER.toString = $estr;
openfl.display.JointStyle.MITER.__enum__ = openfl.display.JointStyle;
openfl.display.JointStyle.ROUND = ["ROUND",1];
openfl.display.JointStyle.ROUND.toString = $estr;
openfl.display.JointStyle.ROUND.__enum__ = openfl.display.JointStyle;
openfl.display.JointStyle.BEVEL = ["BEVEL",2];
openfl.display.JointStyle.BEVEL.toString = $estr;
openfl.display.JointStyle.BEVEL.__enum__ = openfl.display.JointStyle;
openfl.display.LineScaleMode = $hxClasses["openfl.display.LineScaleMode"] = { __ename__ : ["openfl","display","LineScaleMode"], __constructs__ : ["HORIZONTAL","NONE","NORMAL","VERTICAL"] };
openfl.display.LineScaleMode.HORIZONTAL = ["HORIZONTAL",0];
openfl.display.LineScaleMode.HORIZONTAL.toString = $estr;
openfl.display.LineScaleMode.HORIZONTAL.__enum__ = openfl.display.LineScaleMode;
openfl.display.LineScaleMode.NONE = ["NONE",1];
openfl.display.LineScaleMode.NONE.toString = $estr;
openfl.display.LineScaleMode.NONE.__enum__ = openfl.display.LineScaleMode;
openfl.display.LineScaleMode.NORMAL = ["NORMAL",2];
openfl.display.LineScaleMode.NORMAL.toString = $estr;
openfl.display.LineScaleMode.NORMAL.__enum__ = openfl.display.LineScaleMode;
openfl.display.LineScaleMode.VERTICAL = ["VERTICAL",3];
openfl.display.LineScaleMode.VERTICAL.toString = $estr;
openfl.display.LineScaleMode.VERTICAL.__enum__ = openfl.display.LineScaleMode;
openfl.display.Loader = function() {
	openfl.display.Sprite.call(this);
	this.contentLoaderInfo = openfl.display.LoaderInfo.create(this);
};
$hxClasses["openfl.display.Loader"] = openfl.display.Loader;
openfl.display.Loader.__name__ = ["openfl","display","Loader"];
openfl.display.Loader.__super__ = openfl.display.Sprite;
openfl.display.Loader.prototype = $extend(openfl.display.Sprite.prototype,{
	load: function(request,context) {
		var extension = "";
		var parts = request.url.split(".");
		if(parts.length > 0) extension = parts[parts.length - 1].toLowerCase();
		var transparent = true;
		this.contentLoaderInfo.url = request.url;
		if(request.contentType == null && request.contentType != "") switch(extension) {
		case "swf":
			this.contentLoaderInfo.contentType = "application/x-shockwave-flash";
			break;
		case "jpg":case "jpeg":
			transparent = false;
			this.contentLoaderInfo.contentType = "image/jpeg";
			break;
		case "png":
			this.contentLoaderInfo.contentType = "image/png";
			break;
		case "gif":
			this.contentLoaderInfo.contentType = "image/gif";
			break;
		default:
			this.contentLoaderInfo.contentType = "application/x-www-form-urlencoded";
		} else this.contentLoaderInfo.contentType = request.contentType;
		openfl.display.BitmapData.fromFile(request.url,$bind(this,this.BitmapData_onLoad),$bind(this,this.BitmapData_onError));
	}
	,loadBytes: function(buffer) {
		openfl.display.BitmapData.fromBytes(buffer,null,$bind(this,this.BitmapData_onLoad));
	}
	,unload: function() {
		if(this.get_numChildren() > 0) {
			while(this.get_numChildren() > 0) this.removeChildAt(0);
			this.content = null;
			this.contentLoaderInfo.url = null;
			this.contentLoaderInfo.contentType = null;
			this.contentLoaderInfo.content = null;
			this.contentLoaderInfo.bytesLoaded = 0;
			this.contentLoaderInfo.bytesTotal = 0;
			this.contentLoaderInfo.width = 0;
			this.contentLoaderInfo.height = 0;
			var event = new openfl.events.Event(openfl.events.Event.UNLOAD);
			event.currentTarget = this;
			this.dispatchEvent(event);
		}
	}
	,BitmapData_onLoad: function(bitmapData) {
		this.contentLoaderInfo.content = new openfl.display.Bitmap(bitmapData);
		this.content = this.contentLoaderInfo.content;
		this.addChild(this.contentLoaderInfo.content);
		var event = new openfl.events.Event(openfl.events.Event.COMPLETE);
		event.target = this.contentLoaderInfo;
		event.currentTarget = this.contentLoaderInfo;
		this.contentLoaderInfo.dispatchEvent(event);
	}
	,BitmapData_onError: function() {
		var event = new openfl.events.IOErrorEvent(openfl.events.IOErrorEvent.IO_ERROR);
		event.target = this.contentLoaderInfo;
		event.currentTarget = this.contentLoaderInfo;
		this.contentLoaderInfo.dispatchEvent(event);
	}
	,__class__: openfl.display.Loader
});
openfl.display.LoaderInfo = function() {
	openfl.events.EventDispatcher.call(this);
	this.applicationDomain = openfl.system.ApplicationDomain.currentDomain;
	this.bytesLoaded = 0;
	this.bytesTotal = 0;
	this.childAllowsParent = true;
	this.parameters = { };
};
$hxClasses["openfl.display.LoaderInfo"] = openfl.display.LoaderInfo;
openfl.display.LoaderInfo.__name__ = ["openfl","display","LoaderInfo"];
openfl.display.LoaderInfo.create = function(ldr) {
	var li = new openfl.display.LoaderInfo();
	if(ldr != null) li.loader = ldr; else li.url = "";
	return li;
};
openfl.display.LoaderInfo.__super__ = openfl.events.EventDispatcher;
openfl.display.LoaderInfo.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	__class__: openfl.display.LoaderInfo
});
openfl.display.MovieClip = function() {
	openfl.display.Sprite.call(this);
	this.__currentFrame = 0;
	this.__currentLabels = [];
	this.__totalFrames = 0;
	this.enabled = true;
	this.loaderInfo = openfl.display.LoaderInfo.create(null);
};
$hxClasses["openfl.display.MovieClip"] = openfl.display.MovieClip;
openfl.display.MovieClip.__name__ = ["openfl","display","MovieClip"];
openfl.display.MovieClip.__super__ = openfl.display.Sprite;
openfl.display.MovieClip.prototype = $extend(openfl.display.Sprite.prototype,{
	gotoAndPlay: function(frame,scene) {
	}
	,gotoAndStop: function(frame,scene) {
	}
	,nextFrame: function() {
	}
	,play: function() {
	}
	,prevFrame: function() {
	}
	,stop: function() {
	}
	,get_currentFrame: function() {
		return this.__currentFrame;
	}
	,get_currentFrameLabel: function() {
		return this.__currentFrameLabel;
	}
	,get_currentLabel: function() {
		return this.__currentLabel;
	}
	,get_currentLabels: function() {
		return this.__currentLabels;
	}
	,get_framesLoaded: function() {
		return this.__totalFrames;
	}
	,get_totalFrames: function() {
		return this.__totalFrames;
	}
	,__class__: openfl.display.MovieClip
	,__properties__: $extend(openfl.display.Sprite.prototype.__properties__,{get_totalFrames:"get_totalFrames",get_framesLoaded:"get_framesLoaded",get_currentLabels:"get_currentLabels",get_currentLabel:"get_currentLabel",get_currentFrameLabel:"get_currentFrameLabel",get_currentFrame:"get_currentFrame"})
});
openfl.display.PixelSnapping = $hxClasses["openfl.display.PixelSnapping"] = { __ename__ : ["openfl","display","PixelSnapping"], __constructs__ : ["NEVER","AUTO","ALWAYS"] };
openfl.display.PixelSnapping.NEVER = ["NEVER",0];
openfl.display.PixelSnapping.NEVER.toString = $estr;
openfl.display.PixelSnapping.NEVER.__enum__ = openfl.display.PixelSnapping;
openfl.display.PixelSnapping.AUTO = ["AUTO",1];
openfl.display.PixelSnapping.AUTO.toString = $estr;
openfl.display.PixelSnapping.AUTO.__enum__ = openfl.display.PixelSnapping;
openfl.display.PixelSnapping.ALWAYS = ["ALWAYS",2];
openfl.display.PixelSnapping.ALWAYS.toString = $estr;
openfl.display.PixelSnapping.ALWAYS.__enum__ = openfl.display.PixelSnapping;
openfl.display.Shape = function() {
	openfl.display.DisplayObject.call(this);
};
$hxClasses["openfl.display.Shape"] = openfl.display.Shape;
openfl.display.Shape.__name__ = ["openfl","display","Shape"];
openfl.display.Shape.__super__ = openfl.display.DisplayObject;
openfl.display.Shape.prototype = $extend(openfl.display.DisplayObject.prototype,{
	__getBounds: function(rect,matrix) {
		if(this.__graphics != null) this.__graphics.__getBounds(rect,this.__worldTransform);
	}
	,__hitTest: function(x,y,shapeFlag,stack,interactiveOnly) {
		if(this.get_visible() && this.__graphics != null && this.__graphics.__hitTest(x,y,shapeFlag,this.__worldTransform)) {
			if(!interactiveOnly) stack.push(this);
			return true;
		}
		return false;
	}
	,__renderCanvas: function(renderSession) {
		if(!this.__renderable || this.__worldAlpha <= 0) return;
		if(this.__graphics != null) {
			this.__graphics.__render();
			if(this.__graphics.__canvas != null) {
				var context = renderSession.context;
				context.globalAlpha = this.__worldAlpha;
				var transform = this.__worldTransform;
				if(renderSession.roundPixels) context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx | 0,transform.ty | 0); else context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx,transform.ty);
				if(this.get_scrollRect() == null) context.drawImage(this.__graphics.__canvas,this.__graphics.__bounds.x,this.__graphics.__bounds.y); else context.drawImage(this.__graphics.__canvas,this.get_scrollRect().x - this.__graphics.__bounds.x,this.get_scrollRect().y - this.__graphics.__bounds.y,this.get_scrollRect().width,this.get_scrollRect().height,this.__graphics.__bounds.x + this.get_scrollRect().x,this.__graphics.__bounds.y + this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height);
			}
		}
	}
	,__renderDOM: function(renderSession) {
		if(this.stage != null && this.__worldVisible && this.__renderable && this.__graphics != null) {
			if(this.__graphics.__dirty || this.__worldAlphaChanged || this.__canvas == null && this.__graphics.__canvas != null) {
				this.__graphics.__render();
				if(this.__graphics.__canvas != null) {
					if(this.__canvas == null) {
						this.__canvas = window.document.createElement("canvas");
						this.__canvasContext = this.__canvas.getContext("2d");
						this.__initializeElement(this.__canvas,renderSession);
					}
					this.__canvas.width = this.__graphics.__canvas.width;
					this.__canvas.height = this.__graphics.__canvas.height;
					this.__canvasContext.globalAlpha = this.__worldAlpha;
					this.__canvasContext.drawImage(this.__graphics.__canvas,0,0);
				} else if(this.__canvas != null) {
					renderSession.element.removeChild(this.__canvas);
					this.__canvas = null;
					this.__style = null;
				}
			}
			if(this.__canvas != null) {
				if(this.__worldTransformChanged) {
					var transform = new openfl.geom.Matrix();
					transform.translate(this.__graphics.__bounds.x,this.__graphics.__bounds.y);
					transform = transform.mult(this.__worldTransform);
					this.__style.setProperty(renderSession.transformProperty,renderSession.roundPixels?"matrix3d(" + transform.a + ", " + transform.b + ", " + "0, 0, " + transform.c + ", " + transform.d + ", " + "0, 0, 0, 0, 1, 0, " + (transform.tx | 0) + ", " + (transform.ty | 0) + ", 0, 1)":"matrix3d(" + transform.a + ", " + transform.b + ", " + "0, 0, " + transform.c + ", " + transform.d + ", " + "0, 0, 0, 0, 1, 0, " + transform.tx + ", " + transform.ty + ", 0, 1)",null);
				}
				this.__applyStyle(renderSession,false,false,true);
			}
		} else if(this.__canvas != null) {
			renderSession.element.removeChild(this.__canvas);
			this.__canvas = null;
			this.__style = null;
		}
	}
	,get_graphics: function() {
		if(this.__graphics == null) this.__graphics = new openfl.display.Graphics();
		return this.__graphics;
	}
	,__class__: openfl.display.Shape
	,__properties__: $extend(openfl.display.DisplayObject.prototype.__properties__,{get_graphics:"get_graphics"})
});
openfl.display.SpreadMethod = $hxClasses["openfl.display.SpreadMethod"] = { __ename__ : ["openfl","display","SpreadMethod"], __constructs__ : ["REPEAT","REFLECT","PAD"] };
openfl.display.SpreadMethod.REPEAT = ["REPEAT",0];
openfl.display.SpreadMethod.REPEAT.toString = $estr;
openfl.display.SpreadMethod.REPEAT.__enum__ = openfl.display.SpreadMethod;
openfl.display.SpreadMethod.REFLECT = ["REFLECT",1];
openfl.display.SpreadMethod.REFLECT.toString = $estr;
openfl.display.SpreadMethod.REFLECT.__enum__ = openfl.display.SpreadMethod;
openfl.display.SpreadMethod.PAD = ["PAD",2];
openfl.display.SpreadMethod.PAD.toString = $estr;
openfl.display.SpreadMethod.PAD.__enum__ = openfl.display.SpreadMethod;
openfl.display.Stage = function(width,height,element,color) {
	this.__mouseY = 0;
	this.__mouseX = 0;
	openfl.display.Sprite.call(this);
	this.__element = element;
	if(color == null) {
		this.__transparent = true;
		this.set_color(0);
	} else this.set_color(color);
	this.set_name(null);
	this.__mouseX = 0;
	this.__mouseY = 0;
	this.__initializeDOM();
	this.__originalWidth = width;
	this.__originalHeight = height;
	if(width == 0 && height == 0) {
		if(element != null) {
			width = element.clientWidth;
			height = element.clientHeight;
		} else {
			width = window.innerWidth;
			height = window.innerHeight;
		}
		this.__fullscreen = true;
	}
	this.stageWidth = width;
	this.stageHeight = height;
	if(this.__canvas != null) {
		this.__canvas.width = width;
		this.__canvas.height = height;
	} else {
		this.__div.style.width = width + "px";
		this.__div.style.height = height + "px";
	}
	this.__resize();
	window.addEventListener("resize",$bind(this,this.window_onResize));
	window.addEventListener("focus",$bind(this,this.window_onFocus));
	window.addEventListener("blur",$bind(this,this.window_onBlur));
	if(element != null) {
		if(this.__canvas != null) {
			if(element != this.__canvas) element.appendChild(this.__canvas);
		} else element.appendChild(this.__div);
	}
	this.stage = this;
	this.align = openfl.display.StageAlign.TOP_LEFT;
	this.allowsFullScreen = false;
	this.set_displayState(openfl.display.StageDisplayState.NORMAL);
	this.frameRate = 60;
	this.quality = openfl.display.StageQuality.HIGH;
	this.scaleMode = openfl.display.StageScaleMode.NO_SCALE;
	this.stageFocusRect = true;
	this.__clearBeforeRender = true;
	this.__stack = [];
	var keyEvents = ["keydown","keyup"];
	var touchEvents = ["touchstart","touchmove","touchend"];
	var mouseEvents = ["mousedown","mousemove","mouseup","dblclick","mousewheel"];
	var focusEvents = ["focus","blur"];
	var element1;
	if(this.__canvas != null) element1 = this.__canvas; else element1 = this.__div;
	var _g = 0;
	while(_g < keyEvents.length) {
		var type = keyEvents[_g];
		++_g;
		window.addEventListener(type,$bind(this,this.window_onKey),false);
	}
	var _g1 = 0;
	while(_g1 < touchEvents.length) {
		var type1 = touchEvents[_g1];
		++_g1;
		element1.addEventListener(type1,$bind(this,this.element_onTouch),true);
	}
	var _g2 = 0;
	while(_g2 < mouseEvents.length) {
		var type2 = mouseEvents[_g2];
		++_g2;
		element1.addEventListener(type2,$bind(this,this.element_onMouse),true);
	}
	var _g3 = 0;
	while(_g3 < focusEvents.length) {
		var type3 = focusEvents[_g3];
		++_g3;
		element1.addEventListener(type3,$bind(this,this.element_onFocus),true);
	}
	window.requestAnimationFrame($bind(this,this.__render));
};
$hxClasses["openfl.display.Stage"] = openfl.display.Stage;
openfl.display.Stage.__name__ = ["openfl","display","Stage"];
openfl.display.Stage.__super__ = openfl.display.Sprite;
openfl.display.Stage.prototype = $extend(openfl.display.Sprite.prototype,{
	globalToLocal: function(pos) {
		return pos;
	}
	,invalidate: function() {
		this.__invalidated = true;
	}
	,localToGlobal: function(pos) {
		return pos;
	}
	,__drag: function(mouse) {
		var parent = this.__dragObject.parent;
		if(parent != null) mouse = parent.globalToLocal(mouse);
		var x = mouse.x + this.__dragOffsetX;
		var y = mouse.y + this.__dragOffsetY;
		if(this.__dragBounds != null) {
			if(x < this.__dragBounds.x) x = this.__dragBounds.x; else if(x > this.__dragBounds.get_right()) x = this.__dragBounds.get_right();
			if(y < this.__dragBounds.y) y = this.__dragBounds.y; else if(y > this.__dragBounds.get_bottom()) y = this.__dragBounds.get_bottom();
		}
		this.__dragObject.set_x(x);
		this.__dragObject.set_y(y);
	}
	,__fireEvent: function(event,stack) {
		var length = stack.length;
		if(length == 0) {
			event.eventPhase = openfl.events.EventPhase.AT_TARGET;
			event.target.__broadcast(event,false);
		} else {
			event.eventPhase = openfl.events.EventPhase.CAPTURING_PHASE;
			event.target = stack[stack.length - 1];
			var _g1 = 0;
			var _g = length - 1;
			while(_g1 < _g) {
				var i = _g1++;
				stack[i].__broadcast(event,false);
				if(event.__isCancelled) return;
			}
			event.eventPhase = openfl.events.EventPhase.AT_TARGET;
			event.target.__broadcast(event,false);
			if(event.__isCancelled) return;
			if(event.bubbles) {
				event.eventPhase = openfl.events.EventPhase.BUBBLING_PHASE;
				var i1 = length - 2;
				while(i1 >= 0) {
					stack[i1].__broadcast(event,false);
					if(event.__isCancelled) return;
					i1--;
				}
			}
		}
	}
	,__getInteractive: function(stack) {
		stack.push(this);
	}
	,__initializeCanvas: function() {
		if(js.Boot.__instanceof(this.__element,HTMLCanvasElement)) this.__canvas = this.__element; else this.__canvas = window.document.createElement("canvas");
		if(this.__transparent) this.__context = this.__canvas.getContext("2d"); else {
			this.__canvas.setAttribute("moz-opaque","true");
			this.__context = this.__canvas.getContext ("2d", { alpha: false });
		}
		var style = this.__canvas.style;
		style.setProperty("-webkit-transform","translateZ(0)",null);
		style.setProperty("transform","translateZ(0)",null);
		this.__renderSession = new openfl.display.RenderSession();
		this.__renderSession.context = this.__context;
		this.__renderSession.roundPixels = true;
	}
	,__initializeDOM: function() {
		this.__div = window.document.createElement("div");
		var style = this.__div.style;
		if(!this.__transparent) style.backgroundColor = this.__colorString;
		style.setProperty("-webkit-transform","translate3D(0,0,0)",null);
		style.setProperty("transform","translate3D(0,0,0)",null);
		style.position = "relative";
		style.overflow = "hidden";
		style.setProperty("-webkit-user-select","none",null);
		style.setProperty("-moz-user-select","none",null);
		style.setProperty("-ms-user-select","none",null);
		style.setProperty("-o-user-select","none",null);
		window.document.addEventListener("dragstart",function(e) {
			if(e.target.nodeName.toLowerCase() == "img") {
				e.preventDefault();
				return false;
			}
			return true;
		},false);
		this.__renderSession = new openfl.display.RenderSession();
		this.__renderSession.element = this.__div;
		this.__renderSession.roundPixels = true;
		var prefix = (function () {
		  var styles = window.getComputedStyle(document.documentElement, ''),
			pre = (Array.prototype.slice
			  .call(styles)
			  .join('') 
			  .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
			)[1],
			dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
		  return {
			dom: dom,
			lowercase: pre,
			css: '-' + pre + '-',
			js: pre[0].toUpperCase() + pre.substr(1)
		  };
		})();
		this.__renderSession.vendorPrefix = prefix.lowercase;
		if(prefix.lowercase == "webkit") this.__renderSession.transformProperty = "-webkit-transform"; else this.__renderSession.transformProperty = "transform";
		if(prefix.lowercase == "webkit") this.__renderSession.transformOriginProperty = "-webkit-transform-origin"; else this.__renderSession.transformOriginProperty = "transform-origin";
	}
	,__initializeGL: function() {
		return false;
	}
	,__render: function() {
		this.__broadcast(new openfl.events.Event(openfl.events.Event.ENTER_FRAME),true);
		if(this.__invalidated) {
			this.__invalidated = false;
			this.__broadcast(new openfl.events.Event(openfl.events.Event.RENDER),true);
		}
		this.__renderable = true;
		this.__update(false,true);
		if(this.__canvas != null) {
			if(!this.__fullscreen || this.__element != this.__canvas) {
				if(this.stageWidth != this.__canvas.width || this.stageHeight != this.__canvas.height) {
					this.__canvas.width = this.stageWidth;
					this.__canvas.height = this.stageHeight;
				}
			} else {
				this.stageWidth = this.__canvas.width;
				this.stageHeight = this.__canvas.height;
			}
			if(this.__gl != null) {
				if(!this.__glContextLost) {
					this.__gl.viewport(0,0,this.stageWidth,this.stageHeight);
					this.__gl.bindFramebuffer(36160,null);
					if(this.__transparent) this.__gl.clearColor(0,0,0,0); else this.__gl.clearColor(this.__colorSplit[0],this.__colorSplit[1],this.__colorSplit[2],1);
					this.__gl.clear(16384);
					this.__renderGL(this.__renderSession);
				}
			} else {
				this.__context.setTransform(1,0,0,1,0,0);
				this.__context.globalAlpha = 1;
				if(!this.__transparent && this.__clearBeforeRender) {
					this.__context.fillStyle = this.__colorString;
					this.__context.fillRect(0,0,this.stageWidth,this.stageHeight);
				} else if(this.__transparent && this.__clearBeforeRender) this.__context.clearRect(0,0,this.stageWidth,this.stageHeight);
				this.__renderCanvas(this.__renderSession);
			}
		} else {
			this.__renderSession.z = 1;
			this.__renderDOM(this.__renderSession);
		}
		window.requestAnimationFrame($bind(this,this.__render));
	}
	,__resize: function() {
		if(this.__element != null && (this.__div == null || this.__div != null && this.__fullscreen)) {
			if(this.__fullscreen) {
				this.stageWidth = this.__element.clientWidth;
				this.stageHeight = this.__element.clientHeight;
				if(this.__canvas != null) {
					if(this.__element != this.__canvas) {
						this.__canvas.width = this.stageWidth;
						this.__canvas.height = this.stageHeight;
					}
				} else {
					this.__div.style.width = this.stageWidth + "px";
					this.__div.style.height = this.stageHeight + "px";
				}
			} else {
				var scaleX = this.__element.clientWidth / this.__originalWidth;
				var scaleY = this.__element.clientHeight / this.__originalHeight;
				var currentRatio = scaleX / scaleY;
				var targetRatio = Math.min(scaleX,scaleY);
				if(this.__canvas != null) {
					if(this.__element != this.__canvas) {
						this.__canvas.style.width = this.__originalWidth * targetRatio + "px";
						this.__canvas.style.height = this.__originalHeight * targetRatio + "px";
						this.__canvas.style.marginLeft = (this.__element.clientWidth - this.__originalWidth * targetRatio) / 2 + "px";
						this.__canvas.style.marginTop = (this.__element.clientHeight - this.__originalHeight * targetRatio) / 2 + "px";
					}
				} else {
					this.__div.style.width = this.__originalWidth * targetRatio + "px";
					this.__div.style.height = this.__originalHeight * targetRatio + "px";
					this.__div.style.marginLeft = (this.__element.clientWidth - this.__originalWidth * targetRatio) / 2 + "px";
					this.__div.style.marginTop = (this.__element.clientHeight - this.__originalHeight * targetRatio) / 2 + "px";
				}
			}
		}
	}
	,__setCursor: function(cursor) {
		if(this.__cursor != cursor) {
			this.__cursor = cursor;
			if(!this.__cursorHidden) {
				var element;
				if(this.__canvas != null) element = this.__canvas; else element = this.__div;
				element.style.cursor = cursor;
			}
		}
	}
	,__setCursorHidden: function(value) {
		if(this.__cursorHidden != value) {
			this.__cursorHidden = value;
			var element;
			if(this.__canvas != null) element = this.__canvas; else element = this.__div;
			if(value) element.style.cursor = "none"; else element.style.cursor = this.__cursor;
		}
	}
	,__startDrag: function(sprite,lockCenter,bounds) {
		if(bounds == null) this.__dragBounds = null; else this.__dragBounds = bounds.clone();
		this.__dragObject = sprite;
		if(this.__dragObject != null) {
			if(lockCenter) {
				this.__dragOffsetX = -this.__dragObject.get_width() / 2;
				this.__dragOffsetY = -this.__dragObject.get_height() / 2;
			} else {
				var mouse = new openfl.geom.Point(this.get_mouseX(),this.get_mouseY());
				var parent = this.__dragObject.parent;
				if(parent != null) mouse = parent.globalToLocal(mouse);
				this.__dragOffsetX = this.__dragObject.get_x() - mouse.x;
				this.__dragOffsetY = this.__dragObject.get_y() - mouse.y;
			}
		}
	}
	,__stopDrag: function(sprite) {
		this.__dragBounds = null;
		this.__dragObject = null;
	}
	,__update: function(transformOnly,updateChildren) {
		if(transformOnly) {
			if(openfl.display.DisplayObject.__worldTransformDirty > 0) {
				openfl.display.Sprite.prototype.__update.call(this,true,updateChildren);
				if(updateChildren) {
					openfl.display.DisplayObject.__worldTransformDirty = 0;
					this.__dirty = true;
				}
			}
		} else if(openfl.display.DisplayObject.__worldTransformDirty > 0 || this.__dirty || openfl.display.DisplayObject.__worldRenderDirty > 0) {
			openfl.display.Sprite.prototype.__update.call(this,false,updateChildren);
			if(updateChildren) {
				this.__wasDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty = 0;
				openfl.display.DisplayObject.__worldRenderDirty = 0;
				this.__dirty = false;
			}
		} else if(this.__wasDirty) {
			openfl.display.Sprite.prototype.__update.call(this,false,updateChildren);
			if(updateChildren) this.__wasDirty = false;
		}
	}
	,get_mouseX: function() {
		return this.__mouseX;
	}
	,get_mouseY: function() {
		return this.__mouseY;
	}
	,canvas_onContextLost: function(event) {
		this.__glContextLost = true;
	}
	,canvas_onContextRestored: function(event) {
		this.__glContextLost = false;
	}
	,element_onFocus: function(event) {
	}
	,element_onTouch: function(event) {
		event.preventDefault();
		var rect;
		if(this.__canvas != null) rect = this.__canvas.getBoundingClientRect(); else rect = this.__div.getBoundingClientRect();
		var touch = event.changedTouches[0];
		var point = new openfl.geom.Point((touch.pageX - rect.left) * (this.stageWidth / rect.width),(touch.pageY - rect.top) * (this.stageHeight / rect.height));
		this.__mouseX = point.x;
		this.__mouseY = point.y;
		this.__stack = [];
		var type = null;
		var mouseType = null;
		var _g = event.type;
		switch(_g) {
		case "touchstart":
			type = "touchBegin";
			mouseType = openfl.events.MouseEvent.MOUSE_DOWN;
			break;
		case "touchmove":
			type = "touchMove";
			mouseType = openfl.events.MouseEvent.MOUSE_MOVE;
			break;
		case "touchend":
			type = "touchEnd";
			mouseType = openfl.events.MouseEvent.MOUSE_UP;
			break;
		default:
		}
		if(this.__hitTest(this.get_mouseX(),this.get_mouseY(),false,this.__stack,true)) {
			var target = this.__stack[this.__stack.length - 1];
			var localPoint = target.globalToLocal(point);
			var touchEvent = openfl.events.TouchEvent.__create(type,event,touch,localPoint,target);
			touchEvent.touchPointID = touch.identifier;
			touchEvent.isPrimaryTouchPoint = true;
			var mouseEvent = openfl.events.MouseEvent.__create(mouseType,event,localPoint,target);
			mouseEvent.buttonDown = type != "touchEnd";
			this.__fireEvent(touchEvent,this.__stack);
			this.__fireEvent(mouseEvent,this.__stack);
		} else {
			var touchEvent1 = openfl.events.TouchEvent.__create(type,event,touch,point,this);
			touchEvent1.touchPointID = touch.identifier;
			touchEvent1.isPrimaryTouchPoint = true;
			var mouseEvent1 = openfl.events.MouseEvent.__create(mouseType,event,point,this);
			mouseEvent1.buttonDown = type != "touchEnd";
			this.__fireEvent(touchEvent1,[this]);
			this.__fireEvent(mouseEvent1,[this]);
		}
		if(type == "touchMove" && this.__dragObject != null) this.__drag(point);
	}
	,element_onMouse: function(event) {
		var rect;
		if(this.__canvas != null) {
			rect = this.__canvas.getBoundingClientRect();
			this.__mouseX = (event.clientX - rect.left) * (this.stageWidth / rect.width);
			this.__mouseY = (event.clientY - rect.top) * (this.stageHeight / rect.height);
		} else {
			rect = this.__div.getBoundingClientRect();
			this.__mouseX = event.clientX - rect.left;
			this.__mouseY = event.clientY - rect.top;
		}
		this.__stack = [];
		var type;
		var _g = event.type;
		switch(_g) {
		case "mousedown":
			type = openfl.events.MouseEvent.MOUSE_DOWN;
			break;
		case "mouseup":
			type = openfl.events.MouseEvent.MOUSE_UP;
			break;
		case "mousemove":
			type = openfl.events.MouseEvent.MOUSE_MOVE;
			break;
		case "dblclick":
			type = openfl.events.MouseEvent.DOUBLE_CLICK;
			break;
		case "mousewheel":
			type = openfl.events.MouseEvent.MOUSE_WHEEL;
			break;
		default:
			type = null;
		}
		if(this.__hitTest(this.get_mouseX(),this.get_mouseY(),false,this.__stack,true)) {
			var target = this.__stack[this.__stack.length - 1];
			this.__setCursor(target.buttonMode?"pointer":"default");
			this.__fireEvent(openfl.events.MouseEvent.__create(type,event,target.globalToLocal(new openfl.geom.Point(this.get_mouseX(),this.get_mouseY())),target),this.__stack);
			if(type == openfl.events.MouseEvent.MOUSE_UP) this.__fireEvent(openfl.events.MouseEvent.__create(openfl.events.MouseEvent.CLICK,event,target.globalToLocal(new openfl.geom.Point(this.get_mouseX(),this.get_mouseY())),target),this.__stack);
		} else {
			this.__setCursor(this.buttonMode?"pointer":"default");
			this.__fireEvent(openfl.events.MouseEvent.__create(type,event,new openfl.geom.Point(this.get_mouseX(),this.get_mouseY()),this),[this]);
			if(type == openfl.events.MouseEvent.MOUSE_UP) this.__fireEvent(openfl.events.MouseEvent.__create(openfl.events.MouseEvent.CLICK,event,new openfl.geom.Point(this.get_mouseX(),this.get_mouseY()),this),[this]);
		}
		if(this.__dragObject != null) this.__drag(new openfl.geom.Point(this.get_mouseX(),this.get_mouseY()));
	}
	,window_onKey: function(event) {
		var keyCode;
		if(event.keyCode != null) keyCode = event.keyCode; else keyCode = event.which;
		keyCode = openfl.ui.Keyboard.__convertMozillaCode(keyCode);
		var location;
		if(event.location != null) location = event.location; else location = event.keyLocation;
		var keyLocation = Type.createEnumIndex(openfl.ui.KeyLocation,location);
		var stack = new Array();
		if(this.__focus == null) this.__getInteractive(stack); else this.__focus.__getInteractive(stack);
		if(stack.length > 0) {
			stack.reverse();
			this.__fireEvent(new openfl.events.KeyboardEvent(event.type == "keydown"?openfl.events.KeyboardEvent.KEY_DOWN:openfl.events.KeyboardEvent.KEY_UP,true,false,event.charCode,keyCode,keyLocation,event.ctrlKey,event.altKey,event.shiftKey),stack);
		}
	}
	,window_onResize: function(event) {
		this.__resize();
		var event1 = new openfl.events.Event(openfl.events.Event.RESIZE);
		this.__broadcast(event1,false);
	}
	,window_onFocus: function(event) {
		var event1 = new openfl.events.Event(openfl.events.Event.ACTIVATE);
		this.__broadcast(event1,true);
	}
	,window_onBlur: function(event) {
		var event1 = new openfl.events.Event(openfl.events.Event.DEACTIVATE);
		this.__broadcast(event1,true);
	}
	,get_color: function() {
		return this.__color;
	}
	,set_color: function(value) {
		var r = (value & 16711680) >>> 16;
		var g = (value & 65280) >>> 8;
		var b = value & 255;
		this.__colorSplit = [r / 255,g / 255,b / 255];
		this.__colorString = "#" + StringTools.hex(value,6);
		return this.__color = value;
	}
	,get_focus: function() {
		return this.__focus;
	}
	,set_focus: function(value) {
		if(value != this.__focus) {
			if(this.__focus != null) {
				var event = new openfl.events.FocusEvent(openfl.events.FocusEvent.FOCUS_OUT,true,false,value,false,0);
				this.__stack = [];
				this.__focus.__getInteractive(this.__stack);
				this.__stack.reverse();
				this.__fireEvent(event,this.__stack);
			}
			if(value != null) {
				var event1 = new openfl.events.FocusEvent(openfl.events.FocusEvent.FOCUS_IN,true,false,this.__focus,false,0);
				this.__stack = [];
				value.__getInteractive(this.__stack);
				this.__stack.reverse();
				this.__fireEvent(event1,this.__stack);
			}
			this.__focus = value;
		}
		return this.__focus;
	}
	,set_displayState: function(value) {
		switch(value[1]) {
		case 0:
			var fs_exit_function = function() {
			    if (document.exitFullscreen) {
			      document.exitFullscreen();
			    } else if (document.msExitFullscreen) {
			      document.msExitFullscreen();
			    } else if (document.mozCancelFullScreen) {
			      document.mozCancelFullScreen();
			    } else if (document.webkitExitFullscreen) {
			      document.webkitExitFullscreen();
			    }
				}
			fs_exit_function();
			break;
		case 1:case 2:
			var fsfunction = function(elem) {
					if (elem.requestFullscreen) elem.requestFullscreen();
					else if (elem.msRequestFullscreen) elem.msRequestFullscreen();
					else if (elem.mozRequestFullScreen) elem.mozRequestFullScreen();
					else if (elem.webkitRequestFullscreen) elem.webkitRequestFullscreen();
				}
			fsfunction(this.__element);
			break;
		}
		this.displayState = value;
		return value;
	}
	,__class__: openfl.display.Stage
	,__properties__: $extend(openfl.display.Sprite.prototype.__properties__,{set_focus:"set_focus",get_focus:"get_focus",set_displayState:"set_displayState",set_color:"set_color",get_color:"get_color"})
});
openfl.display.RenderSession = function() {
	this.maskManager = new openfl.display.MaskManager(this);
};
$hxClasses["openfl.display.RenderSession"] = openfl.display.RenderSession;
openfl.display.RenderSession.__name__ = ["openfl","display","RenderSession"];
openfl.display.RenderSession.prototype = {
	__class__: openfl.display.RenderSession
};
openfl.display.MaskManager = function(renderSession) {
	this.renderSession = renderSession;
};
$hxClasses["openfl.display.MaskManager"] = openfl.display.MaskManager;
openfl.display.MaskManager.__name__ = ["openfl","display","MaskManager"];
openfl.display.MaskManager.prototype = {
	pushMask: function(mask) {
		var context = this.renderSession.context;
		context.save();
		var transform = mask.__worldTransform;
		if(transform == null) transform = new openfl.geom.Matrix();
		context.setTransform(transform.a,transform.c,transform.b,transform.d,transform.tx,transform.ty);
		context.beginPath();
		mask.__renderMask(this.renderSession);
		context.clip();
	}
	,pushRect: function(rect,transform) {
		var context = this.renderSession.context;
		context.save();
		context.setTransform(transform.a,transform.c,transform.b,transform.d,transform.tx,transform.ty);
		context.beginPath();
		context.rect(rect.x,rect.y,rect.width,rect.height);
		context.clip();
	}
	,popMask: function() {
		this.renderSession.context.restore();
	}
	,__class__: openfl.display.MaskManager
};
openfl.display.StageAlign = $hxClasses["openfl.display.StageAlign"] = { __ename__ : ["openfl","display","StageAlign"], __constructs__ : ["TOP_RIGHT","TOP_LEFT","TOP","RIGHT","LEFT","BOTTOM_RIGHT","BOTTOM_LEFT","BOTTOM"] };
openfl.display.StageAlign.TOP_RIGHT = ["TOP_RIGHT",0];
openfl.display.StageAlign.TOP_RIGHT.toString = $estr;
openfl.display.StageAlign.TOP_RIGHT.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.TOP_LEFT = ["TOP_LEFT",1];
openfl.display.StageAlign.TOP_LEFT.toString = $estr;
openfl.display.StageAlign.TOP_LEFT.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.TOP = ["TOP",2];
openfl.display.StageAlign.TOP.toString = $estr;
openfl.display.StageAlign.TOP.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.RIGHT = ["RIGHT",3];
openfl.display.StageAlign.RIGHT.toString = $estr;
openfl.display.StageAlign.RIGHT.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.LEFT = ["LEFT",4];
openfl.display.StageAlign.LEFT.toString = $estr;
openfl.display.StageAlign.LEFT.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.BOTTOM_RIGHT = ["BOTTOM_RIGHT",5];
openfl.display.StageAlign.BOTTOM_RIGHT.toString = $estr;
openfl.display.StageAlign.BOTTOM_RIGHT.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.BOTTOM_LEFT = ["BOTTOM_LEFT",6];
openfl.display.StageAlign.BOTTOM_LEFT.toString = $estr;
openfl.display.StageAlign.BOTTOM_LEFT.__enum__ = openfl.display.StageAlign;
openfl.display.StageAlign.BOTTOM = ["BOTTOM",7];
openfl.display.StageAlign.BOTTOM.toString = $estr;
openfl.display.StageAlign.BOTTOM.__enum__ = openfl.display.StageAlign;
openfl.display.StageDisplayState = $hxClasses["openfl.display.StageDisplayState"] = { __ename__ : ["openfl","display","StageDisplayState"], __constructs__ : ["NORMAL","FULL_SCREEN","FULL_SCREEN_INTERACTIVE"] };
openfl.display.StageDisplayState.NORMAL = ["NORMAL",0];
openfl.display.StageDisplayState.NORMAL.toString = $estr;
openfl.display.StageDisplayState.NORMAL.__enum__ = openfl.display.StageDisplayState;
openfl.display.StageDisplayState.FULL_SCREEN = ["FULL_SCREEN",1];
openfl.display.StageDisplayState.FULL_SCREEN.toString = $estr;
openfl.display.StageDisplayState.FULL_SCREEN.__enum__ = openfl.display.StageDisplayState;
openfl.display.StageDisplayState.FULL_SCREEN_INTERACTIVE = ["FULL_SCREEN_INTERACTIVE",2];
openfl.display.StageDisplayState.FULL_SCREEN_INTERACTIVE.toString = $estr;
openfl.display.StageDisplayState.FULL_SCREEN_INTERACTIVE.__enum__ = openfl.display.StageDisplayState;
openfl.display.StageQuality = $hxClasses["openfl.display.StageQuality"] = { __ename__ : ["openfl","display","StageQuality"], __constructs__ : ["BEST","HIGH","MEDIUM","LOW"] };
openfl.display.StageQuality.BEST = ["BEST",0];
openfl.display.StageQuality.BEST.toString = $estr;
openfl.display.StageQuality.BEST.__enum__ = openfl.display.StageQuality;
openfl.display.StageQuality.HIGH = ["HIGH",1];
openfl.display.StageQuality.HIGH.toString = $estr;
openfl.display.StageQuality.HIGH.__enum__ = openfl.display.StageQuality;
openfl.display.StageQuality.MEDIUM = ["MEDIUM",2];
openfl.display.StageQuality.MEDIUM.toString = $estr;
openfl.display.StageQuality.MEDIUM.__enum__ = openfl.display.StageQuality;
openfl.display.StageQuality.LOW = ["LOW",3];
openfl.display.StageQuality.LOW.toString = $estr;
openfl.display.StageQuality.LOW.__enum__ = openfl.display.StageQuality;
openfl.display.StageScaleMode = $hxClasses["openfl.display.StageScaleMode"] = { __ename__ : ["openfl","display","StageScaleMode"], __constructs__ : ["SHOW_ALL","NO_SCALE","NO_BORDER","EXACT_FIT"] };
openfl.display.StageScaleMode.SHOW_ALL = ["SHOW_ALL",0];
openfl.display.StageScaleMode.SHOW_ALL.toString = $estr;
openfl.display.StageScaleMode.SHOW_ALL.__enum__ = openfl.display.StageScaleMode;
openfl.display.StageScaleMode.NO_SCALE = ["NO_SCALE",1];
openfl.display.StageScaleMode.NO_SCALE.toString = $estr;
openfl.display.StageScaleMode.NO_SCALE.__enum__ = openfl.display.StageScaleMode;
openfl.display.StageScaleMode.NO_BORDER = ["NO_BORDER",2];
openfl.display.StageScaleMode.NO_BORDER.toString = $estr;
openfl.display.StageScaleMode.NO_BORDER.__enum__ = openfl.display.StageScaleMode;
openfl.display.StageScaleMode.EXACT_FIT = ["EXACT_FIT",3];
openfl.display.StageScaleMode.EXACT_FIT.toString = $estr;
openfl.display.StageScaleMode.EXACT_FIT.__enum__ = openfl.display.StageScaleMode;
openfl.display.Tilesheet = function(image) {
	this.__bitmap = image;
	this.__centerPoints = new Array();
	this.__tileRects = new Array();
	this.__tileUVs = new Array();
};
$hxClasses["openfl.display.Tilesheet"] = openfl.display.Tilesheet;
openfl.display.Tilesheet.__name__ = ["openfl","display","Tilesheet"];
openfl.display.Tilesheet.prototype = {
	addTileRect: function(rectangle,centerPoint) {
		this.__tileRects.push(rectangle);
		if(centerPoint == null) centerPoint = new openfl.geom.Point();
		this.__centerPoints.push(centerPoint);
		this.__tileUVs.push(new openfl.geom.Rectangle(rectangle.get_left() / this.__bitmap.width,rectangle.get_top() / this.__bitmap.height,rectangle.get_right() / this.__bitmap.width,rectangle.get_bottom() / this.__bitmap.height));
		return this.__tileRects.length - 1;
	}
	,drawTiles: function(graphics,tileData,smooth,flags,count) {
		if(count == null) count = -1;
		if(flags == null) flags = 0;
		if(smooth == null) smooth = false;
		graphics.drawTiles(this,tileData,smooth,flags,count);
	}
	,getTileCenter: function(index) {
		return this.__centerPoints[index];
	}
	,getTileRect: function(index) {
		return this.__tileRects[index];
	}
	,getTileUVs: function(index) {
		return this.__tileUVs[index];
	}
	,__class__: openfl.display.Tilesheet
};
openfl.display.TriangleCulling = $hxClasses["openfl.display.TriangleCulling"] = { __ename__ : ["openfl","display","TriangleCulling"], __constructs__ : ["NEGATIVE","NONE","POSITIVE"] };
openfl.display.TriangleCulling.NEGATIVE = ["NEGATIVE",0];
openfl.display.TriangleCulling.NEGATIVE.toString = $estr;
openfl.display.TriangleCulling.NEGATIVE.__enum__ = openfl.display.TriangleCulling;
openfl.display.TriangleCulling.NONE = ["NONE",1];
openfl.display.TriangleCulling.NONE.toString = $estr;
openfl.display.TriangleCulling.NONE.__enum__ = openfl.display.TriangleCulling;
openfl.display.TriangleCulling.POSITIVE = ["POSITIVE",2];
openfl.display.TriangleCulling.POSITIVE.toString = $estr;
openfl.display.TriangleCulling.POSITIVE.__enum__ = openfl.display.TriangleCulling;
openfl.errors = {};
openfl.errors.Error = function(message,id) {
	if(id == null) id = 0;
	if(message == null) message = "";
	this.message = message;
	this.errorID = id;
	this.name = "Error";
};
$hxClasses["openfl.errors.Error"] = openfl.errors.Error;
openfl.errors.Error.__name__ = ["openfl","errors","Error"];
openfl.errors.Error.prototype = {
	getStackTrace: function() {
		return haxe.CallStack.toString(haxe.CallStack.exceptionStack());
	}
	,toString: function() {
		if(this.message != null) return this.message; else return "Error";
	}
	,__class__: openfl.errors.Error
};
openfl.errors.IOError = function(message) {
	if(message == null) message = "";
	openfl.errors.Error.call(this,message);
};
$hxClasses["openfl.errors.IOError"] = openfl.errors.IOError;
openfl.errors.IOError.__name__ = ["openfl","errors","IOError"];
openfl.errors.IOError.__super__ = openfl.errors.Error;
openfl.errors.IOError.prototype = $extend(openfl.errors.Error.prototype,{
	__class__: openfl.errors.IOError
});
openfl.errors.RangeError = function(inMessage) {
	if(inMessage == null) inMessage = "";
	openfl.errors.Error.call(this,inMessage,0);
};
$hxClasses["openfl.errors.RangeError"] = openfl.errors.RangeError;
openfl.errors.RangeError.__name__ = ["openfl","errors","RangeError"];
openfl.errors.RangeError.__super__ = openfl.errors.Error;
openfl.errors.RangeError.prototype = $extend(openfl.errors.Error.prototype,{
	__class__: openfl.errors.RangeError
});
openfl.errors.TypeError = function(inMessage) {
	if(inMessage == null) inMessage = "";
	openfl.errors.Error.call(this,inMessage,0);
};
$hxClasses["openfl.errors.TypeError"] = openfl.errors.TypeError;
openfl.errors.TypeError.__name__ = ["openfl","errors","TypeError"];
openfl.errors.TypeError.__super__ = openfl.errors.Error;
openfl.errors.TypeError.prototype = $extend(openfl.errors.Error.prototype,{
	__class__: openfl.errors.TypeError
});
openfl.events.Event = function(type,bubbles,cancelable) {
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	this.type = type;
	this.bubbles = bubbles;
	this.cancelable = cancelable;
	this.eventPhase = openfl.events.EventPhase.AT_TARGET;
};
$hxClasses["openfl.events.Event"] = openfl.events.Event;
openfl.events.Event.__name__ = ["openfl","events","Event"];
openfl.events.Event.prototype = {
	clone: function() {
		var event = new openfl.events.Event(this.type,this.bubbles,this.cancelable);
		event.eventPhase = this.eventPhase;
		event.target = this.target;
		event.currentTarget = this.currentTarget;
		return event;
	}
	,isDefaultPrevented: function() {
		return this.__isCancelled || this.__isCancelledNow;
	}
	,stopImmediatePropagation: function() {
		this.__isCancelled = true;
		this.__isCancelledNow = true;
	}
	,stopPropagation: function() {
		this.__isCancelled = true;
	}
	,toString: function() {
		return "[Event type=" + this.type + " bubbles=" + ("" + this.bubbles) + " cancelable=" + ("" + this.cancelable) + "]";
	}
	,__class__: openfl.events.Event
};
openfl.events.TextEvent = function(type,bubbles,cancelable,text) {
	if(text == null) text = "";
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.text = text;
};
$hxClasses["openfl.events.TextEvent"] = openfl.events.TextEvent;
openfl.events.TextEvent.__name__ = ["openfl","events","TextEvent"];
openfl.events.TextEvent.__super__ = openfl.events.Event;
openfl.events.TextEvent.prototype = $extend(openfl.events.Event.prototype,{
	__class__: openfl.events.TextEvent
});
openfl.events.ErrorEvent = function(type,bubbles,cancelable,text,id) {
	if(id == null) id = 0;
	if(text == null) text = "";
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.TextEvent.call(this,type,bubbles,cancelable);
	this.text = text;
	this.errorID = id;
};
$hxClasses["openfl.events.ErrorEvent"] = openfl.events.ErrorEvent;
openfl.events.ErrorEvent.__name__ = ["openfl","events","ErrorEvent"];
openfl.events.ErrorEvent.__super__ = openfl.events.TextEvent;
openfl.events.ErrorEvent.prototype = $extend(openfl.events.TextEvent.prototype,{
	__class__: openfl.events.ErrorEvent
});
openfl.events._EventDispatcher = {};
openfl.events._EventDispatcher.Listener = function(callback,useCapture,priority) {
	this.callback = callback;
	this.useCapture = useCapture;
	this.priority = priority;
};
$hxClasses["openfl.events._EventDispatcher.Listener"] = openfl.events._EventDispatcher.Listener;
openfl.events._EventDispatcher.Listener.__name__ = ["openfl","events","_EventDispatcher","Listener"];
openfl.events._EventDispatcher.Listener.prototype = {
	match: function(callback,useCapture) {
		return this.callback == callback && this.useCapture == useCapture;
	}
	,__class__: openfl.events._EventDispatcher.Listener
};
openfl.events.EventPhase = $hxClasses["openfl.events.EventPhase"] = { __ename__ : ["openfl","events","EventPhase"], __constructs__ : ["CAPTURING_PHASE","AT_TARGET","BUBBLING_PHASE"] };
openfl.events.EventPhase.CAPTURING_PHASE = ["CAPTURING_PHASE",0];
openfl.events.EventPhase.CAPTURING_PHASE.toString = $estr;
openfl.events.EventPhase.CAPTURING_PHASE.__enum__ = openfl.events.EventPhase;
openfl.events.EventPhase.AT_TARGET = ["AT_TARGET",1];
openfl.events.EventPhase.AT_TARGET.toString = $estr;
openfl.events.EventPhase.AT_TARGET.__enum__ = openfl.events.EventPhase;
openfl.events.EventPhase.BUBBLING_PHASE = ["BUBBLING_PHASE",2];
openfl.events.EventPhase.BUBBLING_PHASE.toString = $estr;
openfl.events.EventPhase.BUBBLING_PHASE.__enum__ = openfl.events.EventPhase;
openfl.events.FocusEvent = function(type,bubbles,cancelable,relatedObject,shiftKey,keyCode) {
	if(keyCode == null) keyCode = 0;
	if(shiftKey == null) shiftKey = false;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.keyCode = keyCode;
	if(shiftKey == null) this.shiftKey = false; else this.shiftKey = shiftKey;
	this.relatedObject = relatedObject;
};
$hxClasses["openfl.events.FocusEvent"] = openfl.events.FocusEvent;
openfl.events.FocusEvent.__name__ = ["openfl","events","FocusEvent"];
openfl.events.FocusEvent.__super__ = openfl.events.Event;
openfl.events.FocusEvent.prototype = $extend(openfl.events.Event.prototype,{
	clone: function() {
		var event = new openfl.events.FocusEvent(this.type,this.bubbles,this.cancelable,this.relatedObject,this.shiftKey,this.keyCode);
		event.target = this.target;
		event.currentTarget = this.currentTarget;
		event.eventPhase = this.eventPhase;
		return event;
	}
	,__class__: openfl.events.FocusEvent
});
openfl.events.HTTPStatusEvent = function(type,bubbles,cancelable,status) {
	if(status == null) status = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	this.status = status;
	openfl.events.Event.call(this,type,bubbles,cancelable);
};
$hxClasses["openfl.events.HTTPStatusEvent"] = openfl.events.HTTPStatusEvent;
openfl.events.HTTPStatusEvent.__name__ = ["openfl","events","HTTPStatusEvent"];
openfl.events.HTTPStatusEvent.__super__ = openfl.events.Event;
openfl.events.HTTPStatusEvent.prototype = $extend(openfl.events.Event.prototype,{
	__class__: openfl.events.HTTPStatusEvent
});
openfl.events.IOErrorEvent = function(type,bubbles,cancelable,text,id) {
	if(id == null) id = 0;
	if(text == null) text = "";
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = true;
	openfl.events.ErrorEvent.call(this,type,bubbles,cancelable,text,id);
};
$hxClasses["openfl.events.IOErrorEvent"] = openfl.events.IOErrorEvent;
openfl.events.IOErrorEvent.__name__ = ["openfl","events","IOErrorEvent"];
openfl.events.IOErrorEvent.__super__ = openfl.events.ErrorEvent;
openfl.events.IOErrorEvent.prototype = $extend(openfl.events.ErrorEvent.prototype,{
	clone: function() {
		return new openfl.events.IOErrorEvent(this.type,this.bubbles,this.cancelable,this.text,this.errorID);
	}
	,toString: function() {
		return "[IOErrorEvent type=" + this.type + " bubbles=" + ("" + this.bubbles) + " cancelable=" + ("" + this.cancelable) + " text=" + this.text + " errorID=" + this.errorID + "]";
	}
	,__class__: openfl.events.IOErrorEvent
});
openfl.events.KeyboardEvent = function(type,bubbles,cancelable,charCodeValue,keyCodeValue,keyLocationValue,ctrlKeyValue,altKeyValue,shiftKeyValue,controlKeyValue,commandKeyValue) {
	if(commandKeyValue == null) commandKeyValue = false;
	if(controlKeyValue == null) controlKeyValue = false;
	if(shiftKeyValue == null) shiftKeyValue = false;
	if(altKeyValue == null) altKeyValue = false;
	if(ctrlKeyValue == null) ctrlKeyValue = false;
	if(keyCodeValue == null) keyCodeValue = 0;
	if(charCodeValue == null) charCodeValue = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.charCode = charCodeValue;
	this.keyCode = keyCodeValue;
	if(keyLocationValue != null) this.keyLocation = keyLocationValue; else this.keyLocation = openfl.ui.KeyLocation.STANDARD;
	this.ctrlKey = ctrlKeyValue;
	this.altKey = altKeyValue;
	this.shiftKey = shiftKeyValue;
	this.controlKey = controlKeyValue;
	this.commandKey = commandKeyValue;
};
$hxClasses["openfl.events.KeyboardEvent"] = openfl.events.KeyboardEvent;
openfl.events.KeyboardEvent.__name__ = ["openfl","events","KeyboardEvent"];
openfl.events.KeyboardEvent.__super__ = openfl.events.Event;
openfl.events.KeyboardEvent.prototype = $extend(openfl.events.Event.prototype,{
	__class__: openfl.events.KeyboardEvent
});
openfl.events.MouseEvent = function(type,bubbles,cancelable,localX,localY,relatedObject,ctrlKey,altKey,shiftKey,buttonDown,delta,commandKey,clickCount) {
	if(clickCount == null) clickCount = 0;
	if(commandKey == null) commandKey = false;
	if(delta == null) delta = 0;
	if(buttonDown == null) buttonDown = false;
	if(shiftKey == null) shiftKey = false;
	if(altKey == null) altKey = false;
	if(ctrlKey == null) ctrlKey = false;
	if(localY == null) localY = 0;
	if(localX == null) localX = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = true;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.shiftKey = shiftKey;
	this.altKey = altKey;
	this.ctrlKey = ctrlKey;
	this.bubbles = bubbles;
	this.relatedObject = relatedObject;
	this.delta = delta;
	this.localX = localX;
	this.localY = localY;
	this.buttonDown = buttonDown;
	this.commandKey = commandKey;
	this.clickCount = clickCount;
};
$hxClasses["openfl.events.MouseEvent"] = openfl.events.MouseEvent;
openfl.events.MouseEvent.__name__ = ["openfl","events","MouseEvent"];
openfl.events.MouseEvent.__buttonDown = null;
openfl.events.MouseEvent.__create = function(type,event,local,target) {
	var delta = 2;
	if(type == openfl.events.MouseEvent.MOUSE_WHEEL) {
		var mouseEvent = event;
		if(mouseEvent.wheelDelta) delta = mouseEvent.wheelDelta / 120 | 0; else if(mouseEvent.detail) -mouseEvent.detail | 0;
	}
	if(type == openfl.events.MouseEvent.MOUSE_DOWN) openfl.events.MouseEvent.__buttonDown = true; else if(type == openfl.events.MouseEvent.MOUSE_UP) openfl.events.MouseEvent.__buttonDown = false;
	var pseudoEvent = new openfl.events.MouseEvent(type,true,false,local.x,local.y,null,event.ctrlKey,event.altKey,event.shiftKey,openfl.events.MouseEvent.__buttonDown,delta);
	pseudoEvent.stageX = openfl.Lib.current.stage.get_mouseX();
	pseudoEvent.stageY = openfl.Lib.current.stage.get_mouseY();
	pseudoEvent.target = target;
	return pseudoEvent;
};
openfl.events.MouseEvent.__super__ = openfl.events.Event;
openfl.events.MouseEvent.prototype = $extend(openfl.events.Event.prototype,{
	updateAfterEvent: function() {
	}
	,__class__: openfl.events.MouseEvent
});
openfl.events.ProgressEvent = function(type,bubbles,cancelable,bytesLoaded,bytesTotal) {
	if(bytesTotal == null) bytesTotal = 0;
	if(bytesLoaded == null) bytesLoaded = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.bytesLoaded = bytesLoaded;
	this.bytesTotal = bytesTotal;
};
$hxClasses["openfl.events.ProgressEvent"] = openfl.events.ProgressEvent;
openfl.events.ProgressEvent.__name__ = ["openfl","events","ProgressEvent"];
openfl.events.ProgressEvent.__super__ = openfl.events.Event;
openfl.events.ProgressEvent.prototype = $extend(openfl.events.Event.prototype,{
	__class__: openfl.events.ProgressEvent
});
openfl.events.SecurityErrorEvent = function(type,bubbles,cancelable,text) {
	if(text == null) text = "";
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.ErrorEvent.call(this,type,bubbles,cancelable);
	this.text = text;
};
$hxClasses["openfl.events.SecurityErrorEvent"] = openfl.events.SecurityErrorEvent;
openfl.events.SecurityErrorEvent.__name__ = ["openfl","events","SecurityErrorEvent"];
openfl.events.SecurityErrorEvent.__super__ = openfl.events.ErrorEvent;
openfl.events.SecurityErrorEvent.prototype = $extend(openfl.events.ErrorEvent.prototype,{
	__class__: openfl.events.SecurityErrorEvent
});
openfl.events.TimerEvent = function(type,bubbles,cancelable) {
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.Event.call(this,type,bubbles,cancelable);
};
$hxClasses["openfl.events.TimerEvent"] = openfl.events.TimerEvent;
openfl.events.TimerEvent.__name__ = ["openfl","events","TimerEvent"];
openfl.events.TimerEvent.__super__ = openfl.events.Event;
openfl.events.TimerEvent.prototype = $extend(openfl.events.Event.prototype,{
	updateAfterEvent: function() {
	}
	,__class__: openfl.events.TimerEvent
});
openfl.events.TouchEvent = function(type,bubbles,cancelable,localX,localY,sizeX,sizeY,relatedObject,ctrlKey,altKey,shiftKey,buttonDown,delta,commandKey,clickCount) {
	if(clickCount == null) clickCount = 0;
	if(commandKey == null) commandKey = false;
	if(delta == null) delta = 0;
	if(buttonDown == null) buttonDown = false;
	if(shiftKey == null) shiftKey = false;
	if(altKey == null) altKey = false;
	if(ctrlKey == null) ctrlKey = false;
	if(sizeY == null) sizeY = 1;
	if(sizeX == null) sizeX = 1;
	if(localY == null) localY = 0;
	if(localX == null) localX = 0;
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = true;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.shiftKey = shiftKey;
	this.altKey = altKey;
	this.ctrlKey = ctrlKey;
	this.bubbles = bubbles;
	this.relatedObject = relatedObject;
	this.delta = delta;
	this.localX = localX;
	this.localY = localY;
	this.sizeX = sizeX;
	this.sizeY = sizeY;
	this.buttonDown = buttonDown;
	this.commandKey = commandKey;
	this.pressure = 1;
	this.touchPointID = 0;
	this.isPrimaryTouchPoint = true;
};
$hxClasses["openfl.events.TouchEvent"] = openfl.events.TouchEvent;
openfl.events.TouchEvent.__name__ = ["openfl","events","TouchEvent"];
openfl.events.TouchEvent.__create = function(type,event,touch,local,target) {
	var evt = new openfl.events.TouchEvent(type,true,false,local.x,local.y,null,null,null,event.ctrlKey,event.altKey,event.shiftKey,false,0,null,0);
	evt.stageX = openfl.Lib.current.stage.get_mouseX();
	evt.stageY = openfl.Lib.current.stage.get_mouseY();
	evt.target = target;
	return evt;
};
openfl.events.TouchEvent.__super__ = openfl.events.Event;
openfl.events.TouchEvent.prototype = $extend(openfl.events.Event.prototype,{
	updateAfterEvent: function() {
	}
	,__class__: openfl.events.TouchEvent
});
openfl.external = {};
openfl.external.ExternalInterface = function() { };
$hxClasses["openfl.external.ExternalInterface"] = openfl.external.ExternalInterface;
openfl.external.ExternalInterface.__name__ = ["openfl","external","ExternalInterface"];
openfl.external.ExternalInterface.objectID = null;
openfl.external.ExternalInterface.addCallback = function(functionName,closure) {
	if(openfl.Lib.current.stage.__element != null) openfl.Lib.current.stage.__element[functionName] = closure;
};
openfl.external.ExternalInterface.call = function(functionName,p1,p2,p3,p4,p5) {
	var callResponse = null;
	var thisArg = functionName.split(".").slice(0,-1).join(".");
	if(thisArg.length > 0) functionName += ".bind(" + thisArg + ")";
	if(p1 == null) callResponse = eval(functionName)(); else if(p2 == null) callResponse = eval(functionName)(p1); else if(p3 == null) callResponse = eval(functionName)(p1,p2); else if(p4 == null) callResponse = eval(functionName)(p1,p2,p3); else if(p5 == null) callResponse = eval(functionName)(p1,p2,p3,p4); else callResponse = eval(functionName)(p1,p2,p3,p4,p5);
	return callResponse;
};
openfl.filters = {};
openfl.filters.BitmapFilter = function() {
};
$hxClasses["openfl.filters.BitmapFilter"] = openfl.filters.BitmapFilter;
openfl.filters.BitmapFilter.__name__ = ["openfl","filters","BitmapFilter"];
openfl.filters.BitmapFilter.prototype = {
	clone: function() {
		return new openfl.filters.BitmapFilter();
	}
	,__applyFilter: function(sourceData,targetData,sourceRect,destPoint) {
	}
	,__class__: openfl.filters.BitmapFilter
};
openfl.filters.BitmapFilterQuality = function() { };
$hxClasses["openfl.filters.BitmapFilterQuality"] = openfl.filters.BitmapFilterQuality;
openfl.filters.BitmapFilterQuality.__name__ = ["openfl","filters","BitmapFilterQuality"];
openfl.filters.GlowFilter = function(color,alpha,blurX,blurY,strength,quality,inner,knockout) {
	if(knockout == null) knockout = false;
	if(inner == null) inner = false;
	if(quality == null) quality = 1;
	if(strength == null) strength = 2;
	if(blurY == null) blurY = 6;
	if(blurX == null) blurX = 6;
	if(alpha == null) alpha = 1;
	if(color == null) color = 0;
	openfl.filters.BitmapFilter.call(this);
	this.color = color;
	this.alpha = alpha;
	this.blurX = blurX;
	this.blurY = blurY;
	this.strength = strength;
	this.quality = quality;
	this.inner = inner;
	this.knockout = knockout;
};
$hxClasses["openfl.filters.GlowFilter"] = openfl.filters.GlowFilter;
openfl.filters.GlowFilter.__name__ = ["openfl","filters","GlowFilter"];
openfl.filters.GlowFilter.__super__ = openfl.filters.BitmapFilter;
openfl.filters.GlowFilter.prototype = $extend(openfl.filters.BitmapFilter.prototype,{
	clone: function() {
		return new openfl.filters.GlowFilter(this.color,this.alpha,this.blurX,this.blurY,this.strength,this.quality,this.inner,this.knockout);
	}
	,__class__: openfl.filters.GlowFilter
});
openfl.geom = {};
openfl.geom.ColorTransform = function(redMultiplier,greenMultiplier,blueMultiplier,alphaMultiplier,redOffset,greenOffset,blueOffset,alphaOffset) {
	if(alphaOffset == null) alphaOffset = 0;
	if(blueOffset == null) blueOffset = 0;
	if(greenOffset == null) greenOffset = 0;
	if(redOffset == null) redOffset = 0;
	if(alphaMultiplier == null) alphaMultiplier = 1;
	if(blueMultiplier == null) blueMultiplier = 1;
	if(greenMultiplier == null) greenMultiplier = 1;
	if(redMultiplier == null) redMultiplier = 1;
	this.redMultiplier = redMultiplier;
	this.greenMultiplier = greenMultiplier;
	this.blueMultiplier = blueMultiplier;
	this.alphaMultiplier = alphaMultiplier;
	this.redOffset = redOffset;
	this.greenOffset = greenOffset;
	this.blueOffset = blueOffset;
	this.alphaOffset = alphaOffset;
};
$hxClasses["openfl.geom.ColorTransform"] = openfl.geom.ColorTransform;
openfl.geom.ColorTransform.__name__ = ["openfl","geom","ColorTransform"];
openfl.geom.ColorTransform.prototype = {
	concat: function(second) {
		this.redMultiplier += second.redMultiplier;
		this.greenMultiplier += second.greenMultiplier;
		this.blueMultiplier += second.blueMultiplier;
		this.alphaMultiplier += second.alphaMultiplier;
	}
	,get_color: function() {
		return (this.redOffset | 0) << 16 | (this.greenOffset | 0) << 8 | (this.blueOffset | 0);
	}
	,set_color: function(value) {
		this.redOffset = value >> 16 & 255;
		this.greenOffset = value >> 8 & 255;
		this.blueOffset = value & 255;
		this.redMultiplier = 0;
		this.greenMultiplier = 0;
		this.blueMultiplier = 0;
		return this.get_color();
	}
	,__class__: openfl.geom.ColorTransform
	,__properties__: {set_color:"set_color",get_color:"get_color"}
};
openfl.geom.Matrix = function(a,b,c,d,tx,ty) {
	if(ty == null) ty = 0;
	if(tx == null) tx = 0;
	if(d == null) d = 1;
	if(c == null) c = 0;
	if(b == null) b = 0;
	if(a == null) a = 1;
	this.a = a;
	this.b = b;
	this.c = c;
	this.d = d;
	this.tx = tx;
	this.ty = ty;
};
$hxClasses["openfl.geom.Matrix"] = openfl.geom.Matrix;
openfl.geom.Matrix.__name__ = ["openfl","geom","Matrix"];
openfl.geom.Matrix.prototype = {
	clone: function() {
		return new openfl.geom.Matrix(this.a,this.b,this.c,this.d,this.tx,this.ty);
	}
	,concat: function(m) {
		var a1 = this.a * m.a + this.b * m.c;
		this.b = this.a * m.b + this.b * m.d;
		this.a = a1;
		var c1 = this.c * m.a + this.d * m.c;
		this.d = this.c * m.b + this.d * m.d;
		this.c = c1;
		var tx1 = this.tx * m.a + this.ty * m.c + m.tx;
		this.ty = this.tx * m.b + this.ty * m.d + m.ty;
		this.tx = tx1;
	}
	,copyColumnFrom: function(column,vector3D) {
		if(column > 2) throw "Column " + column + " out of bounds (2)"; else if(column == 0) {
			this.a = vector3D.x;
			this.c = vector3D.y;
		} else if(column == 1) {
			this.b = vector3D.x;
			this.d = vector3D.y;
		} else {
			this.tx = vector3D.x;
			this.ty = vector3D.y;
		}
	}
	,copyColumnTo: function(column,vector3D) {
		if(column > 2) throw "Column " + column + " out of bounds (2)"; else if(column == 0) {
			vector3D.x = this.a;
			vector3D.y = this.c;
			vector3D.z = 0;
		} else if(column == 1) {
			vector3D.x = this.b;
			vector3D.y = this.d;
			vector3D.z = 0;
		} else {
			vector3D.x = this.tx;
			vector3D.y = this.ty;
			vector3D.z = 1;
		}
	}
	,copyFrom: function(sourceMatrix) {
		this.a = sourceMatrix.a;
		this.b = sourceMatrix.b;
		this.c = sourceMatrix.c;
		this.d = sourceMatrix.d;
		this.tx = sourceMatrix.tx;
		this.ty = sourceMatrix.ty;
	}
	,copyRowFrom: function(row,vector3D) {
		if(row > 2) throw "Row " + row + " out of bounds (2)"; else if(row == 0) {
			this.a = vector3D.x;
			this.c = vector3D.y;
		} else if(row == 1) {
			this.b = vector3D.x;
			this.d = vector3D.y;
		} else {
			this.tx = vector3D.x;
			this.ty = vector3D.y;
		}
	}
	,copyRowTo: function(row,vector3D) {
		if(row > 2) throw "Row " + row + " out of bounds (2)"; else if(row == 0) {
			vector3D.x = this.a;
			vector3D.y = this.b;
			vector3D.z = this.tx;
		} else if(row == 1) {
			vector3D.x = this.c;
			vector3D.y = this.d;
			vector3D.z = this.ty;
		} else {
			vector3D.x = 0;
			vector3D.y = 0;
			vector3D.z = 1;
		}
	}
	,createBox: function(scaleX,scaleY,rotation,tx,ty) {
		if(ty == null) ty = 0;
		if(tx == null) tx = 0;
		if(rotation == null) rotation = 0;
		this.a = scaleX;
		this.d = scaleY;
		this.b = rotation;
		this.tx = tx;
		this.ty = ty;
	}
	,createGradientBox: function(width,height,rotation,tx,ty) {
		if(ty == null) ty = 0;
		if(tx == null) tx = 0;
		if(rotation == null) rotation = 0;
		this.a = width / 1638.4;
		this.d = height / 1638.4;
		if(rotation != 0) {
			var cos = Math.cos(rotation);
			var sin = Math.sin(rotation);
			this.b = sin * this.d;
			this.c = -sin * this.a;
			this.a *= cos;
			this.d *= cos;
		} else {
			this.b = 0;
			this.c = 0;
		}
		this.tx = tx + width / 2;
		this.ty = ty + height / 2;
	}
	,equals: function(matrix) {
		return matrix != null && this.tx == matrix.tx && this.ty == matrix.ty && this.a == matrix.a && this.b == matrix.b && this.c == matrix.c && this.d == matrix.d;
	}
	,deltaTransformPoint: function(point) {
		return new openfl.geom.Point(point.x * this.a + point.y * this.c,point.x * this.b + point.y * this.d);
	}
	,identity: function() {
		this.a = 1;
		this.b = 0;
		this.c = 0;
		this.d = 1;
		this.tx = 0;
		this.ty = 0;
	}
	,invert: function() {
		var norm = this.a * this.d - this.b * this.c;
		if(norm == 0) {
			this.a = this.b = this.c = this.d = 0;
			this.tx = -this.tx;
			this.ty = -this.ty;
		} else {
			norm = 1.0 / norm;
			var a1 = this.d * norm;
			this.d = this.a * norm;
			this.a = a1;
			this.b *= -norm;
			this.c *= -norm;
			var tx1 = -this.a * this.tx - this.c * this.ty;
			this.ty = -this.b * this.tx - this.d * this.ty;
			this.tx = tx1;
		}
		return this;
	}
	,mult: function(m) {
		var result = new openfl.geom.Matrix(this.a,this.b,this.c,this.d,this.tx,this.ty);
		result.concat(m);
		return result;
	}
	,rotate: function(theta) {
		var cos = Math.cos(theta);
		var sin = Math.sin(theta);
		var a1 = this.a * cos - this.b * sin;
		this.b = this.a * sin + this.b * cos;
		this.a = a1;
		var c1 = this.c * cos - this.d * sin;
		this.d = this.c * sin + this.d * cos;
		this.c = c1;
		var tx1 = this.tx * cos - this.ty * sin;
		this.ty = this.tx * sin + this.ty * cos;
		this.tx = tx1;
	}
	,scale: function(sx,sy) {
		this.a *= sx;
		this.b *= sy;
		this.c *= sx;
		this.d *= sy;
		this.tx *= sx;
		this.ty *= sy;
	}
	,setRotation: function(theta,scale) {
		if(scale == null) scale = 1;
		this.a = Math.cos(theta) * scale;
		this.c = Math.sin(theta) * scale;
		this.b = -this.c;
		this.d = this.a;
	}
	,setTo: function(a,b,c,d,tx,ty) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.tx = tx;
		this.ty = ty;
	}
	,to3DString: function(roundPixels) {
		if(roundPixels == null) roundPixels = false;
		if(roundPixels) return "matrix3d(" + this.a + ", " + this.b + ", " + "0, 0, " + this.c + ", " + this.d + ", " + "0, 0, 0, 0, 1, 0, " + (this.tx | 0) + ", " + (this.ty | 0) + ", 0, 1)"; else return "matrix3d(" + this.a + ", " + this.b + ", " + "0, 0, " + this.c + ", " + this.d + ", " + "0, 0, 0, 0, 1, 0, " + this.tx + ", " + this.ty + ", 0, 1)";
	}
	,toMozString: function() {
		return "matrix(" + this.a + ", " + this.b + ", " + this.c + ", " + this.d + ", " + this.tx + "px, " + this.ty + "px)";
	}
	,toString: function() {
		return "matrix(" + this.a + ", " + this.b + ", " + this.c + ", " + this.d + ", " + this.tx + ", " + this.ty + ")";
	}
	,transformPoint: function(pos) {
		return new openfl.geom.Point(pos.x * this.a + pos.y * this.c + this.tx,pos.x * this.b + pos.y * this.d + this.ty);
	}
	,translate: function(dx,dy) {
		var m = new openfl.geom.Matrix();
		m.tx = dx;
		m.ty = dy;
		this.concat(m);
	}
	,__cleanValues: function() {
		this.a = Math.round(this.a * 1000) / 1000;
		this.b = Math.round(this.b * 1000) / 1000;
		this.c = Math.round(this.c * 1000) / 1000;
		this.d = Math.round(this.d * 1000) / 1000;
		this.tx = Math.round(this.tx * 10) / 10;
		this.ty = Math.round(this.ty * 10) / 10;
	}
	,__transformX: function(pos) {
		return pos.x * this.a + pos.y * this.c + this.tx;
	}
	,__transformY: function(pos) {
		return pos.x * this.b + pos.y * this.d + this.ty;
	}
	,__translateTransformed: function(pos) {
		this.tx = pos.x * this.a + pos.y * this.c + this.tx;
		this.ty = pos.x * this.b + pos.y * this.d + this.ty;
	}
	,__class__: openfl.geom.Matrix
};
openfl.geom.Point = function(x,y) {
	if(y == null) y = 0;
	if(x == null) x = 0;
	this.x = x;
	this.y = y;
};
$hxClasses["openfl.geom.Point"] = openfl.geom.Point;
openfl.geom.Point.__name__ = ["openfl","geom","Point"];
openfl.geom.Point.distance = function(pt1,pt2) {
	var dx = pt1.x - pt2.x;
	var dy = pt1.y - pt2.y;
	return Math.sqrt(dx * dx + dy * dy);
};
openfl.geom.Point.interpolate = function(pt1,pt2,f) {
	return new openfl.geom.Point(pt2.x + f * (pt1.x - pt2.x),pt2.y + f * (pt1.y - pt2.y));
};
openfl.geom.Point.polar = function(len,angle) {
	return new openfl.geom.Point(len * Math.cos(angle),len * Math.sin(angle));
};
openfl.geom.Point.prototype = {
	add: function(v) {
		return new openfl.geom.Point(v.x + this.x,v.y + this.y);
	}
	,clone: function() {
		return new openfl.geom.Point(this.x,this.y);
	}
	,equals: function(toCompare) {
		return toCompare != null && toCompare.x == this.x && toCompare.y == this.y;
	}
	,normalize: function(thickness) {
		if(this.x == 0 && this.y == 0) return; else {
			var norm = thickness / Math.sqrt(this.x * this.x + this.y * this.y);
			this.x *= norm;
			this.y *= norm;
		}
	}
	,offset: function(dx,dy) {
		this.x += dx;
		this.y += dy;
	}
	,setTo: function(xa,ya) {
		this.x = xa;
		this.y = ya;
	}
	,subtract: function(v) {
		return new openfl.geom.Point(this.x - v.x,this.y - v.y);
	}
	,get_length: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	,__class__: openfl.geom.Point
	,__properties__: {get_length:"get_length"}
};
openfl.geom.Rectangle = function(x,y,width,height) {
	if(height == null) height = 0;
	if(width == null) width = 0;
	if(y == null) y = 0;
	if(x == null) x = 0;
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
};
$hxClasses["openfl.geom.Rectangle"] = openfl.geom.Rectangle;
openfl.geom.Rectangle.__name__ = ["openfl","geom","Rectangle"];
openfl.geom.Rectangle.prototype = {
	clone: function() {
		return new openfl.geom.Rectangle(this.x,this.y,this.width,this.height);
	}
	,contains: function(x,y) {
		return x >= this.x && y >= this.y && x < this.get_right() && y < this.get_bottom();
	}
	,containsPoint: function(point) {
		return this.contains(point.x,point.y);
	}
	,containsRect: function(rect) {
		if(rect.width <= 0 || rect.height <= 0) return rect.x > this.x && rect.y > this.y && rect.get_right() < this.get_right() && rect.get_bottom() < this.get_bottom(); else return rect.x >= this.x && rect.y >= this.y && rect.get_right() <= this.get_right() && rect.get_bottom() <= this.get_bottom();
	}
	,copyFrom: function(sourceRect) {
		this.x = sourceRect.x;
		this.y = sourceRect.y;
		this.width = sourceRect.width;
		this.height = sourceRect.height;
	}
	,equals: function(toCompare) {
		return toCompare != null && this.x == toCompare.x && this.y == toCompare.y && this.width == toCompare.width && this.height == toCompare.height;
	}
	,inflate: function(dx,dy) {
		this.x -= dx;
		this.width += dx * 2;
		this.y -= dy;
		this.height += dy * 2;
	}
	,inflatePoint: function(point) {
		this.inflate(point.x,point.y);
	}
	,intersection: function(toIntersect) {
		var x0;
		if(this.x < toIntersect.x) x0 = toIntersect.x; else x0 = this.x;
		var x1;
		if(this.get_right() > toIntersect.get_right()) x1 = toIntersect.get_right(); else x1 = this.get_right();
		if(x1 <= x0) return new openfl.geom.Rectangle();
		var y0;
		if(this.y < toIntersect.y) y0 = toIntersect.y; else y0 = this.y;
		var y1;
		if(this.get_bottom() > toIntersect.get_bottom()) y1 = toIntersect.get_bottom(); else y1 = this.get_bottom();
		if(y1 <= y0) return new openfl.geom.Rectangle();
		return new openfl.geom.Rectangle(x0,y0,x1 - x0,y1 - y0);
	}
	,intersects: function(toIntersect) {
		var x0;
		if(this.x < toIntersect.x) x0 = toIntersect.x; else x0 = this.x;
		var x1;
		if(this.get_right() > toIntersect.get_right()) x1 = toIntersect.get_right(); else x1 = this.get_right();
		if(x1 <= x0) return false;
		var y0;
		if(this.y < toIntersect.y) y0 = toIntersect.y; else y0 = this.y;
		var y1;
		if(this.get_bottom() > toIntersect.get_bottom()) y1 = toIntersect.get_bottom(); else y1 = this.get_bottom();
		return y1 > y0;
	}
	,isEmpty: function() {
		return this.width <= 0 || this.height <= 0;
	}
	,offset: function(dx,dy) {
		this.x += dx;
		this.y += dy;
	}
	,offsetPoint: function(point) {
		this.x += point.x;
		this.y += point.y;
	}
	,setEmpty: function() {
		this.x = this.y = this.width = this.height = 0;
	}
	,setTo: function(xa,ya,widtha,heighta) {
		this.x = xa;
		this.y = ya;
		this.width = widtha;
		this.height = heighta;
	}
	,transform: function(m) {
		var tx0 = m.a * this.x + m.c * this.y;
		var tx1 = tx0;
		var ty0 = m.b * this.x + m.d * this.y;
		var ty1 = tx0;
		var tx = m.a * (this.x + this.width) + m.c * this.y;
		var ty = m.b * (this.x + this.width) + m.d * this.y;
		if(tx < tx0) tx0 = tx;
		if(ty < ty0) ty0 = ty;
		if(tx > tx1) tx1 = tx;
		if(ty > ty1) ty1 = ty;
		tx = m.a * (this.x + this.width) + m.c * (this.y + this.height);
		ty = m.b * (this.x + this.width) + m.d * (this.y + this.height);
		if(tx < tx0) tx0 = tx;
		if(ty < ty0) ty0 = ty;
		if(tx > tx1) tx1 = tx;
		if(ty > ty1) ty1 = ty;
		tx = m.a * this.x + m.c * (this.y + this.height);
		ty = m.b * this.x + m.d * (this.y + this.height);
		if(tx < tx0) tx0 = tx;
		if(ty < ty0) ty0 = ty;
		if(tx > tx1) tx1 = tx;
		if(ty > ty1) ty1 = ty;
		return new openfl.geom.Rectangle(tx0 + m.tx,ty0 + m.ty,tx1 - tx0,ty1 - ty0);
	}
	,union: function(toUnion) {
		if(this.width == 0 || this.height == 0) return toUnion.clone(); else if(toUnion.width == 0 || toUnion.height == 0) return this.clone();
		var x0;
		if(this.x > toUnion.x) x0 = toUnion.x; else x0 = this.x;
		var x1;
		if(this.get_right() < toUnion.get_right()) x1 = toUnion.get_right(); else x1 = this.get_right();
		var y0;
		if(this.y > toUnion.y) y0 = toUnion.y; else y0 = this.y;
		var y1;
		if(this.get_bottom() < toUnion.get_bottom()) y1 = toUnion.get_bottom(); else y1 = this.get_bottom();
		return new openfl.geom.Rectangle(x0,y0,x1 - x0,y1 - y0);
	}
	,__contract: function(x,y,width,height) {
		if(this.width == 0 && this.height == 0) return;
		var cacheRight = this.get_right();
		var cacheBottom = this.get_bottom();
		if(this.x < x) this.x = x;
		if(this.y < y) this.y = y;
		if(this.get_right() > x + width) this.width = x + width - this.x;
		if(this.get_bottom() > y + height) this.height = y + height - this.y;
	}
	,__expand: function(x,y,width,height) {
		if(this.width == 0 && this.height == 0) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
			return;
		}
		var cacheRight = this.get_right();
		var cacheBottom = this.get_bottom();
		if(this.x > x) this.x = x;
		if(this.y > y) this.y = y;
		if(cacheRight < x + width) this.width = x + width - this.x;
		if(cacheBottom < y + height) this.height = y + height - this.y;
	}
	,get_bottom: function() {
		return this.y + this.height;
	}
	,set_bottom: function(b) {
		this.height = b - this.y;
		return b;
	}
	,get_bottomRight: function() {
		return new openfl.geom.Point(this.x + this.width,this.y + this.height);
	}
	,set_bottomRight: function(p) {
		this.width = p.x - this.x;
		this.height = p.y - this.y;
		return p.clone();
	}
	,get_left: function() {
		return this.x;
	}
	,set_left: function(l) {
		this.width -= l - this.x;
		this.x = l;
		return l;
	}
	,get_right: function() {
		return this.x + this.width;
	}
	,set_right: function(r) {
		this.width = r - this.x;
		return r;
	}
	,get_size: function() {
		return new openfl.geom.Point(this.width,this.height);
	}
	,set_size: function(p) {
		this.width = p.x;
		this.height = p.y;
		return p.clone();
	}
	,get_top: function() {
		return this.y;
	}
	,set_top: function(t) {
		this.height -= t - this.y;
		this.y = t;
		return t;
	}
	,get_topLeft: function() {
		return new openfl.geom.Point(this.x,this.y);
	}
	,set_topLeft: function(p) {
		this.x = p.x;
		this.y = p.y;
		return p.clone();
	}
	,__class__: openfl.geom.Rectangle
	,__properties__: {set_topLeft:"set_topLeft",get_topLeft:"get_topLeft",set_top:"set_top",get_top:"get_top",set_size:"set_size",get_size:"get_size",set_right:"set_right",get_right:"get_right",set_left:"set_left",get_left:"get_left",set_bottomRight:"set_bottomRight",get_bottomRight:"get_bottomRight",set_bottom:"set_bottom",get_bottom:"get_bottom"}
};
openfl.geom.Transform = function(displayObject) {
	this.colorTransform = new openfl.geom.ColorTransform();
	this.concatenatedColorTransform = new openfl.geom.ColorTransform();
	this.concatenatedMatrix = new openfl.geom.Matrix();
	this.pixelBounds = new openfl.geom.Rectangle();
	this.__displayObject = displayObject;
	this.__matrix = new openfl.geom.Matrix();
};
$hxClasses["openfl.geom.Transform"] = openfl.geom.Transform;
openfl.geom.Transform.__name__ = ["openfl","geom","Transform"];
openfl.geom.Transform.prototype = {
	get_matrix: function() {
		if(this.__matrix != null) {
			this.__matrix.identity();
			this.__matrix.scale(this.__displayObject.get_scaleX(),this.__displayObject.get_scaleY());
			this.__matrix.rotate(this.__displayObject.get_rotation() * (Math.PI / 180));
			this.__matrix.translate(this.__displayObject.get_x(),this.__displayObject.get_y());
			return this.__matrix.clone();
		}
		return null;
	}
	,set_matrix: function(value) {
		if(value == null) return this.__matrix = null;
		if(this.__displayObject != null) {
			this.__displayObject.set_x(value.tx);
			this.__displayObject.set_y(value.ty);
			this.__displayObject.set_scaleX(Math.sqrt(value.a * value.a + value.b * value.b));
			this.__displayObject.set_scaleY(Math.sqrt(value.c * value.c + value.d * value.d));
			this.__displayObject.set_rotation(Math.atan2(value.b,value.a) * (180 / Math.PI));
		}
		return value;
	}
	,__class__: openfl.geom.Transform
	,__properties__: {set_matrix:"set_matrix",get_matrix:"get_matrix"}
};
openfl.geom.Vector3D = function(x,y,z,w) {
	if(w == null) w = 0.;
	if(z == null) z = 0.;
	if(y == null) y = 0.;
	if(x == null) x = 0.;
	this.w = w;
	this.x = x;
	this.y = y;
	this.z = z;
};
$hxClasses["openfl.geom.Vector3D"] = openfl.geom.Vector3D;
openfl.geom.Vector3D.__name__ = ["openfl","geom","Vector3D"];
openfl.geom.Vector3D.__properties__ = {get_Z_AXIS:"get_Z_AXIS",get_Y_AXIS:"get_Y_AXIS",get_X_AXIS:"get_X_AXIS"}
openfl.geom.Vector3D.X_AXIS = null;
openfl.geom.Vector3D.Y_AXIS = null;
openfl.geom.Vector3D.Z_AXIS = null;
openfl.geom.Vector3D.angleBetween = function(a,b) {
	var a0 = new openfl.geom.Vector3D(a.x,a.y,a.z,a.w);
	a0.normalize();
	var b0 = new openfl.geom.Vector3D(b.x,b.y,b.z,b.w);
	b0.normalize();
	return Math.acos(a0.x * b0.x + a0.y * b0.y + a0.z * b0.z);
};
openfl.geom.Vector3D.distance = function(pt1,pt2) {
	var x = pt2.x - pt1.x;
	var y = pt2.y - pt1.y;
	var z = pt2.z - pt1.z;
	return Math.sqrt(x * x + y * y + z * z);
};
openfl.geom.Vector3D.get_X_AXIS = function() {
	return new openfl.geom.Vector3D(1,0,0);
};
openfl.geom.Vector3D.get_Y_AXIS = function() {
	return new openfl.geom.Vector3D(0,1,0);
};
openfl.geom.Vector3D.get_Z_AXIS = function() {
	return new openfl.geom.Vector3D(0,0,1);
};
openfl.geom.Vector3D.prototype = {
	add: function(a) {
		return new openfl.geom.Vector3D(this.x + a.x,this.y + a.y,this.z + a.z);
	}
	,clone: function() {
		return new openfl.geom.Vector3D(this.x,this.y,this.z,this.w);
	}
	,copyFrom: function(sourceVector3D) {
		this.x = sourceVector3D.x;
		this.y = sourceVector3D.y;
		this.z = sourceVector3D.z;
	}
	,crossProduct: function(a) {
		return new openfl.geom.Vector3D(this.y * a.z - this.z * a.y,this.z * a.x - this.x * a.z,this.x * a.y - this.y * a.x,1);
	}
	,decrementBy: function(a) {
		this.x -= a.x;
		this.y -= a.y;
		this.z -= a.z;
	}
	,dotProduct: function(a) {
		return this.x * a.x + this.y * a.y + this.z * a.z;
	}
	,equals: function(toCompare,allFour) {
		if(allFour == null) allFour = false;
		return this.x == toCompare.x && this.y == toCompare.y && this.z == toCompare.z && (!allFour || this.w == toCompare.w);
	}
	,incrementBy: function(a) {
		this.x += a.x;
		this.y += a.y;
		this.z += a.z;
	}
	,nearEquals: function(toCompare,tolerance,allFour) {
		if(allFour == null) allFour = false;
		return Math.abs(this.x - toCompare.x) < tolerance && Math.abs(this.y - toCompare.y) < tolerance && Math.abs(this.z - toCompare.z) < tolerance && (!allFour || Math.abs(this.w - toCompare.w) < tolerance);
	}
	,negate: function() {
		this.x *= -1;
		this.y *= -1;
		this.z *= -1;
	}
	,normalize: function() {
		var l = Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
		if(l != 0) {
			this.x /= l;
			this.y /= l;
			this.z /= l;
		}
		return l;
	}
	,project: function() {
		this.x /= this.w;
		this.y /= this.w;
		this.z /= this.w;
	}
	,scaleBy: function(s) {
		this.x *= s;
		this.y *= s;
		this.z *= s;
	}
	,setTo: function(xa,ya,za) {
		this.x = xa;
		this.y = ya;
		this.z = za;
	}
	,subtract: function(a) {
		return new openfl.geom.Vector3D(this.x - a.x,this.y - a.y,this.z - a.z);
	}
	,toString: function() {
		return "Vector3D(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
	,get_length: function() {
		return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
	}
	,get_lengthSquared: function() {
		return this.x * this.x + this.y * this.y + this.z * this.z;
	}
	,__class__: openfl.geom.Vector3D
	,__properties__: {get_lengthSquared:"get_lengthSquared",get_length:"get_length"}
};
openfl.media = {};
openfl.media.ID3Info = function() {
};
$hxClasses["openfl.media.ID3Info"] = openfl.media.ID3Info;
openfl.media.ID3Info.__name__ = ["openfl","media","ID3Info"];
openfl.media.ID3Info.prototype = {
	__class__: openfl.media.ID3Info
};
openfl.media.Sound = function(stream,context) {
	openfl.events.EventDispatcher.call(this,this);
	this.bytesLoaded = 0;
	this.bytesTotal = 0;
	this.id3 = null;
	this.isBuffering = false;
	this.length = 0;
	this.url = null;
	if(stream != null) this.load(stream,context);
};
$hxClasses["openfl.media.Sound"] = openfl.media.Sound;
openfl.media.Sound.__name__ = ["openfl","media","Sound"];
openfl.media.Sound.__super__ = openfl.events.EventDispatcher;
openfl.media.Sound.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	close: function() {
		if(openfl.media.Sound.__registeredSounds.exists(this.__soundID)) createjs.Sound.removeSound(this.__soundID);
	}
	,load: function(stream,context) {
		this.url = stream.url;
		this.__soundID = haxe.io.Path.withoutExtension(stream.url);
		if(!openfl.media.Sound.__registeredSounds.exists(this.__soundID)) {
			openfl.media.Sound.__registeredSounds.set(this.__soundID,true);
			createjs.Sound.addEventListener("fileload",$bind(this,this.SoundJS_onFileLoad));
			createjs.Sound.registerSound(this.url,this.__soundID);
		} else this.dispatchEvent(new openfl.events.Event(openfl.events.Event.COMPLETE));
	}
	,loadCompressedDataFromByteArray: function(bytes,bytesLength) {
		openfl.Lib.notImplemented("Sound.loadCompressedDataFromByteArray");
	}
	,loadPCMFromByteArray: function(bytes,samples,format,stereo,sampleRate) {
		if(sampleRate == null) sampleRate = 44100;
		if(stereo == null) stereo = true;
		openfl.Lib.notImplemented("Sound.loadPCMFromByteArray");
	}
	,play: function(startTime,loops,sndTransform) {
		if(loops == null) loops = 0;
		if(startTime == null) startTime = 0.0;
		if(sndTransform == null) sndTransform = new openfl.media.SoundTransform(1,0);
		var instance = createjs.Sound.play(this.__soundID,"any",0,startTime | 0,loops,sndTransform.volume,sndTransform.pan);
		return new openfl.media.SoundChannel(instance);
	}
	,get_id3: function() {
		return new openfl.media.ID3Info();
	}
	,SoundJS_onFileLoad: function(event) {
		if(event.id == this.__soundID) {
			createjs.Sound.removeEventListener("fileload",$bind(this,this.SoundJS_onFileLoad));
			this.dispatchEvent(new openfl.events.Event(openfl.events.Event.COMPLETE));
		}
	}
	,__class__: openfl.media.Sound
	,__properties__: {get_id3:"get_id3"}
});
openfl.media.SoundChannel = function(soundInstance) {
	openfl.events.EventDispatcher.call(this,this);
	this.__soundInstance = soundInstance;
	this.__soundInstance.addEventListener("complete",$bind(this,this.soundInstance_onComplete));
};
$hxClasses["openfl.media.SoundChannel"] = openfl.media.SoundChannel;
openfl.media.SoundChannel.__name__ = ["openfl","media","SoundChannel"];
openfl.media.SoundChannel.__super__ = openfl.events.EventDispatcher;
openfl.media.SoundChannel.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	stop: function() {
		this.__soundInstance.stop();
	}
	,__dispose: function() {
		this.__soundInstance.stop();
		this.__soundInstance = null;
	}
	,get_position: function() {
		return this.__soundInstance.getPosition();
	}
	,set_position: function(value) {
		this.__soundInstance.setPosition(value | 0);
		return this.__soundInstance.getPosition();
	}
	,get_soundTransform: function() {
		return new openfl.media.SoundTransform(this.__soundInstance.getVolume(),this.__soundInstance.getPan());
	}
	,set_soundTransform: function(value) {
		this.__soundInstance.setVolume(value.volume);
		this.__soundInstance.setPan(value.pan);
		return value;
	}
	,soundInstance_onComplete: function(_) {
		this.dispatchEvent(new openfl.events.Event(openfl.events.Event.SOUND_COMPLETE));
	}
	,__class__: openfl.media.SoundChannel
	,__properties__: {set_soundTransform:"set_soundTransform",get_soundTransform:"get_soundTransform",set_position:"set_position",get_position:"get_position"}
});
openfl.media.SoundLoaderContext = function(bufferTime,checkPolicyFile) {
	if(checkPolicyFile == null) checkPolicyFile = false;
	if(bufferTime == null) bufferTime = 0;
	this.bufferTime = bufferTime;
	this.checkPolicyFile = checkPolicyFile;
};
$hxClasses["openfl.media.SoundLoaderContext"] = openfl.media.SoundLoaderContext;
openfl.media.SoundLoaderContext.__name__ = ["openfl","media","SoundLoaderContext"];
openfl.media.SoundLoaderContext.prototype = {
	__class__: openfl.media.SoundLoaderContext
};
openfl.media.SoundTransform = function(vol,panning) {
	if(panning == null) panning = 0;
	if(vol == null) vol = 1;
	this.volume = vol;
	this.pan = panning;
	this.leftToLeft = 0;
	this.leftToRight = 0;
	this.rightToLeft = 0;
	this.rightToRight = 0;
};
$hxClasses["openfl.media.SoundTransform"] = openfl.media.SoundTransform;
openfl.media.SoundTransform.__name__ = ["openfl","media","SoundTransform"];
openfl.media.SoundTransform.prototype = {
	__class__: openfl.media.SoundTransform
};
openfl.net = {};
openfl.net.SharedObject = function() {
	openfl.events.EventDispatcher.call(this);
};
$hxClasses["openfl.net.SharedObject"] = openfl.net.SharedObject;
openfl.net.SharedObject.__name__ = ["openfl","net","SharedObject"];
openfl.net.SharedObject.getLocal = function(name,localPath,secure) {
	if(secure == null) secure = false;
	if(localPath == null) localPath = window.location.href;
	var so = new openfl.net.SharedObject();
	so.__key = localPath + ":" + name;
	var rawData = null;
	try {
		rawData = openfl.net.SharedObject.__getLocalStorage().getItem(so.__key);
	} catch( e ) {
	}
	so.data = { };
	if(rawData != null && rawData != "") {
		var unserializer = new haxe.Unserializer(rawData);
		unserializer.setResolver({ resolveEnum : Type.resolveEnum, resolveClass : openfl.net.SharedObject.resolveClass});
		so.data = unserializer.unserialize();
	}
	if(so.data == null) so.data = { };
	return so;
};
openfl.net.SharedObject.__getLocalStorage = function() {
	var res = js.Browser.getLocalStorage();
	if(res == null) throw new openfl.errors.Error("SharedObject not supported");
	return res;
};
openfl.net.SharedObject.resolveClass = function(name) {
	if(name != null) return Type.resolveClass(StringTools.replace(StringTools.replace(name,"jeash.","flash."),"browser.","flash."));
	return null;
};
openfl.net.SharedObject.__super__ = openfl.events.EventDispatcher;
openfl.net.SharedObject.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	clear: function() {
		this.data = { };
		try {
			openfl.net.SharedObject.__getLocalStorage().removeItem(this.__key);
		} catch( e ) {
		}
		this.flush();
	}
	,flush: function() {
		var data = haxe.Serializer.run(this.data);
		try {
			openfl.net.SharedObject.__getLocalStorage().removeItem(this.__key);
			openfl.net.SharedObject.__getLocalStorage().setItem(this.__key,data);
		} catch( e ) {
			return openfl.net.SharedObjectFlushStatus.PENDING;
		}
		return openfl.net.SharedObjectFlushStatus.FLUSHED;
	}
	,setProperty: function(propertyName,value) {
		if(this.data != null) this.data[propertyName] = value;
	}
	,get_size: function() {
		var d = haxe.Serializer.run(this.data);
		return haxe.io.Bytes.ofString(d).length;
	}
	,__class__: openfl.net.SharedObject
	,__properties__: {get_size:"get_size"}
});
openfl.net.SharedObjectFlushStatus = $hxClasses["openfl.net.SharedObjectFlushStatus"] = { __ename__ : ["openfl","net","SharedObjectFlushStatus"], __constructs__ : ["FLUSHED","PENDING"] };
openfl.net.SharedObjectFlushStatus.FLUSHED = ["FLUSHED",0];
openfl.net.SharedObjectFlushStatus.FLUSHED.toString = $estr;
openfl.net.SharedObjectFlushStatus.FLUSHED.__enum__ = openfl.net.SharedObjectFlushStatus;
openfl.net.SharedObjectFlushStatus.PENDING = ["PENDING",1];
openfl.net.SharedObjectFlushStatus.PENDING.toString = $estr;
openfl.net.SharedObjectFlushStatus.PENDING.__enum__ = openfl.net.SharedObjectFlushStatus;
openfl.net.URLLoader = function(request) {
	openfl.events.EventDispatcher.call(this);
	this.bytesLoaded = 0;
	this.bytesTotal = 0;
	this.set_dataFormat(openfl.net.URLLoaderDataFormat.TEXT);
	if(request != null) this.load(request);
};
$hxClasses["openfl.net.URLLoader"] = openfl.net.URLLoader;
openfl.net.URLLoader.__name__ = ["openfl","net","URLLoader"];
openfl.net.URLLoader.__super__ = openfl.events.EventDispatcher;
openfl.net.URLLoader.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	close: function() {
	}
	,getData: function() {
		return null;
	}
	,load: function(request) {
		this.requestUrl(request.url,request.method,request.data,request.formatRequestHeaders());
	}
	,registerEvents: function(subject) {
		var self = this;
		if(typeof XMLHttpRequestProgressEvent != "undefined") subject.addEventListener("progress",$bind(this,this.onProgress),false);
		subject.onreadystatechange = function() {
			if(subject.readyState != 4) return;
			var s;
			try {
				s = subject.status;
			} catch( e ) {
				s = null;
			}
			if(s == undefined) s = null;
			if(s != null) self.onStatus(s);
			if(s != null && s >= 200 && s < 400) self.onData(subject.response); else if(s == null) self.onError("Failed to connect or resolve host"); else if(s == 12029) self.onError("Failed to connect to host"); else if(s == 12007) self.onError("Unknown host"); else if(s == 0) {
				self.onError("Unable to make request (may be blocked due to cross-domain permissions)");
				self.onSecurityError("Unable to make request (may be blocked due to cross-domain permissions)");
			} else self.onError("Http Error #" + subject.status);
		};
	}
	,requestUrl: function(url,method,data,requestHeaders) {
		var xmlHttpRequest = new XMLHttpRequest();
		this.registerEvents(xmlHttpRequest);
		var uri = "";
		if(js.Boot.__instanceof(data,openfl.utils.ByteArray)) {
			var data1 = data;
			var _g = this.dataFormat;
			switch(_g[1]) {
			case 0:
				uri = data1.data.buffer;
				break;
			default:
				uri = data1.readUTFBytes(data1.length);
			}
		} else if(js.Boot.__instanceof(data,openfl.net.URLVariables)) {
			var data2 = data;
			var _g1 = 0;
			var _g11 = Reflect.fields(data2);
			while(_g1 < _g11.length) {
				var p = _g11[_g1];
				++_g1;
				if(uri.length != 0) uri += "&";
				uri += encodeURIComponent(p) + "=" + StringTools.urlEncode(Reflect.field(data2,p));
			}
		} else if(data != null) uri = data.toString();
		try {
			if(method == "GET" && uri != null && uri != "") {
				var question = url.split("?").length <= 1;
				xmlHttpRequest.open(method,url + (question?"?":"&") + Std.string(uri),true);
				uri = "";
			} else xmlHttpRequest.open(method,url,true);
		} catch( e ) {
			this.onError(e.toString());
			return;
		}
		var _g2 = this.dataFormat;
		switch(_g2[1]) {
		case 0:
			xmlHttpRequest.responseType = "arraybuffer";
			break;
		default:
		}
		var _g3 = 0;
		while(_g3 < requestHeaders.length) {
			var header = requestHeaders[_g3];
			++_g3;
			xmlHttpRequest.setRequestHeader(header.name,header.value);
		}
		xmlHttpRequest.send(uri);
		this.onOpen();
		this.getData = function() {
			if(xmlHttpRequest.response != null) return xmlHttpRequest.response; else return xmlHttpRequest.responseText;
		};
	}
	,onData: function(_) {
		var content = this.getData();
		var _g = this.dataFormat;
		switch(_g[1]) {
		case 0:
			this.data = openfl.utils.ByteArray.__ofBuffer(content);
			break;
		default:
			this.data = Std.string(content);
		}
		var evt = new openfl.events.Event(openfl.events.Event.COMPLETE);
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onError: function(msg) {
		var evt = new openfl.events.IOErrorEvent(openfl.events.IOErrorEvent.IO_ERROR);
		evt.text = msg;
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onOpen: function() {
		var evt = new openfl.events.Event(openfl.events.Event.OPEN);
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onProgress: function(event) {
		var evt = new openfl.events.ProgressEvent(openfl.events.ProgressEvent.PROGRESS);
		evt.currentTarget = this;
		evt.bytesLoaded = event.loaded;
		evt.bytesTotal = event.total;
		this.dispatchEvent(evt);
	}
	,onSecurityError: function(msg) {
		var evt = new openfl.events.SecurityErrorEvent(openfl.events.SecurityErrorEvent.SECURITY_ERROR);
		evt.text = msg;
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,onStatus: function(status) {
		var evt = new openfl.events.HTTPStatusEvent(openfl.events.HTTPStatusEvent.HTTP_STATUS,false,false,status);
		evt.currentTarget = this;
		this.dispatchEvent(evt);
	}
	,set_dataFormat: function(inputVal) {
		if(inputVal == openfl.net.URLLoaderDataFormat.BINARY && !Reflect.hasField(window,"ArrayBuffer")) this.dataFormat = openfl.net.URLLoaderDataFormat.TEXT; else this.dataFormat = inputVal;
		return this.dataFormat;
	}
	,__class__: openfl.net.URLLoader
	,__properties__: {set_dataFormat:"set_dataFormat"}
});
openfl.net.URLLoaderDataFormat = $hxClasses["openfl.net.URLLoaderDataFormat"] = { __ename__ : ["openfl","net","URLLoaderDataFormat"], __constructs__ : ["BINARY","TEXT","VARIABLES"] };
openfl.net.URLLoaderDataFormat.BINARY = ["BINARY",0];
openfl.net.URLLoaderDataFormat.BINARY.toString = $estr;
openfl.net.URLLoaderDataFormat.BINARY.__enum__ = openfl.net.URLLoaderDataFormat;
openfl.net.URLLoaderDataFormat.TEXT = ["TEXT",1];
openfl.net.URLLoaderDataFormat.TEXT.toString = $estr;
openfl.net.URLLoaderDataFormat.TEXT.__enum__ = openfl.net.URLLoaderDataFormat;
openfl.net.URLLoaderDataFormat.VARIABLES = ["VARIABLES",2];
openfl.net.URLLoaderDataFormat.VARIABLES.toString = $estr;
openfl.net.URLLoaderDataFormat.VARIABLES.__enum__ = openfl.net.URLLoaderDataFormat;
openfl.net.URLRequest = function(inURL) {
	if(inURL != null) this.url = inURL;
	this.requestHeaders = [];
	this.method = openfl.net.URLRequestMethod.GET;
	this.contentType = null;
};
$hxClasses["openfl.net.URLRequest"] = openfl.net.URLRequest;
openfl.net.URLRequest.__name__ = ["openfl","net","URLRequest"];
openfl.net.URLRequest.prototype = {
	formatRequestHeaders: function() {
		var res = this.requestHeaders;
		if(res == null) res = [];
		if(this.method == openfl.net.URLRequestMethod.GET || this.data == null) return res;
		if(typeof(this.data) == "string" || js.Boot.__instanceof(this.data,openfl.utils.ByteArray)) {
			res = res.slice();
			res.push(new openfl.net.URLRequestHeader("Content-Type",this.contentType != null?this.contentType:"application/x-www-form-urlencoded"));
		}
		return res;
	}
	,__class__: openfl.net.URLRequest
};
openfl.net.URLRequestHeader = function(name,value) {
	if(value == null) value = "";
	if(name == null) name = "";
	this.name = name;
	this.value = value;
};
$hxClasses["openfl.net.URLRequestHeader"] = openfl.net.URLRequestHeader;
openfl.net.URLRequestHeader.__name__ = ["openfl","net","URLRequestHeader"];
openfl.net.URLRequestHeader.prototype = {
	__class__: openfl.net.URLRequestHeader
};
openfl.net.URLRequestMethod = function() { };
$hxClasses["openfl.net.URLRequestMethod"] = openfl.net.URLRequestMethod;
openfl.net.URLRequestMethod.__name__ = ["openfl","net","URLRequestMethod"];
openfl.net.URLVariables = function(inEncoded) {
	if(inEncoded != null) this.decode(inEncoded);
};
$hxClasses["openfl.net.URLVariables"] = openfl.net.URLVariables;
openfl.net.URLVariables.__name__ = ["openfl","net","URLVariables"];
openfl.net.URLVariables.prototype = {
	decode: function(inVars) {
		var fields = Reflect.fields(this);
		var _g = 0;
		while(_g < fields.length) {
			var f = fields[_g];
			++_g;
			Reflect.deleteField(this,f);
		}
		var fields1 = inVars.split(";").join("&").split("&");
		var _g1 = 0;
		while(_g1 < fields1.length) {
			var f1 = fields1[_g1];
			++_g1;
			var eq = f1.indexOf("=");
			if(eq > 0) Reflect.setField(this,StringTools.urlDecode(HxOverrides.substr(f1,0,eq)),StringTools.urlDecode(HxOverrides.substr(f1,eq + 1,null))); else if(eq != 0) Reflect.setField(this,decodeURIComponent(f1.split("+").join(" ")),"");
		}
	}
	,toString: function() {
		var result = new Array();
		var fields = Reflect.fields(this);
		var _g = 0;
		while(_g < fields.length) {
			var f = fields[_g];
			++_g;
			result.push(encodeURIComponent(f) + "=" + StringTools.urlEncode(Reflect.field(this,f)));
		}
		return result.join("&");
	}
	,__class__: openfl.net.URLVariables
};
openfl.system = {};
openfl.system.ApplicationDomain = function(parentDomain) {
	if(parentDomain != null) this.parentDomain = parentDomain; else this.parentDomain = openfl.system.ApplicationDomain.currentDomain;
};
$hxClasses["openfl.system.ApplicationDomain"] = openfl.system.ApplicationDomain;
openfl.system.ApplicationDomain.__name__ = ["openfl","system","ApplicationDomain"];
openfl.system.ApplicationDomain.prototype = {
	getDefinition: function(name) {
		return Type.resolveClass(name);
	}
	,hasDefinition: function(name) {
		return Type.resolveClass(name) != null;
	}
	,__class__: openfl.system.ApplicationDomain
};
openfl.system.LoaderContext = function(checkPolicyFile,applicationDomain,securityDomain) {
	if(checkPolicyFile == null) checkPolicyFile = false;
	this.checkPolicyFile = checkPolicyFile;
	this.securityDomain = securityDomain;
	this.applicationDomain = applicationDomain;
	this.allowCodeImport = true;
	this.allowLoadBytesCodeExecution = true;
};
$hxClasses["openfl.system.LoaderContext"] = openfl.system.LoaderContext;
openfl.system.LoaderContext.__name__ = ["openfl","system","LoaderContext"];
openfl.system.LoaderContext.prototype = {
	__class__: openfl.system.LoaderContext
};
openfl.system.SecurityDomain = function() {
};
$hxClasses["openfl.system.SecurityDomain"] = openfl.system.SecurityDomain;
openfl.system.SecurityDomain.__name__ = ["openfl","system","SecurityDomain"];
openfl.system.SecurityDomain.prototype = {
	__class__: openfl.system.SecurityDomain
};
openfl.text = {};
openfl.text.AntiAliasType = $hxClasses["openfl.text.AntiAliasType"] = { __ename__ : ["openfl","text","AntiAliasType"], __constructs__ : ["ADVANCED","NORMAL"] };
openfl.text.AntiAliasType.ADVANCED = ["ADVANCED",0];
openfl.text.AntiAliasType.ADVANCED.toString = $estr;
openfl.text.AntiAliasType.ADVANCED.__enum__ = openfl.text.AntiAliasType;
openfl.text.AntiAliasType.NORMAL = ["NORMAL",1];
openfl.text.AntiAliasType.NORMAL.toString = $estr;
openfl.text.AntiAliasType.NORMAL.__enum__ = openfl.text.AntiAliasType;
openfl.text.Font = function() {
};
$hxClasses["openfl.text.Font"] = openfl.text.Font;
openfl.text.Font.__name__ = ["openfl","text","Font"];
openfl.text.Font.enumerateFonts = function(enumerateDeviceFonts) {
	if(enumerateDeviceFonts == null) enumerateDeviceFonts = false;
	return [];
};
openfl.text.Font.registerFont = function(font) {
};
openfl.text.Font.prototype = {
	__class__: openfl.text.Font
};
openfl.text.FontStyle = $hxClasses["openfl.text.FontStyle"] = { __ename__ : ["openfl","text","FontStyle"], __constructs__ : ["REGULAR","ITALIC","BOLD_ITALIC","BOLD"] };
openfl.text.FontStyle.REGULAR = ["REGULAR",0];
openfl.text.FontStyle.REGULAR.toString = $estr;
openfl.text.FontStyle.REGULAR.__enum__ = openfl.text.FontStyle;
openfl.text.FontStyle.ITALIC = ["ITALIC",1];
openfl.text.FontStyle.ITALIC.toString = $estr;
openfl.text.FontStyle.ITALIC.__enum__ = openfl.text.FontStyle;
openfl.text.FontStyle.BOLD_ITALIC = ["BOLD_ITALIC",2];
openfl.text.FontStyle.BOLD_ITALIC.toString = $estr;
openfl.text.FontStyle.BOLD_ITALIC.__enum__ = openfl.text.FontStyle;
openfl.text.FontStyle.BOLD = ["BOLD",3];
openfl.text.FontStyle.BOLD.toString = $estr;
openfl.text.FontStyle.BOLD.__enum__ = openfl.text.FontStyle;
openfl.text.FontType = $hxClasses["openfl.text.FontType"] = { __ename__ : ["openfl","text","FontType"], __constructs__ : ["DEVICE","EMBEDDED","EMBEDDED_CFF"] };
openfl.text.FontType.DEVICE = ["DEVICE",0];
openfl.text.FontType.DEVICE.toString = $estr;
openfl.text.FontType.DEVICE.__enum__ = openfl.text.FontType;
openfl.text.FontType.EMBEDDED = ["EMBEDDED",1];
openfl.text.FontType.EMBEDDED.toString = $estr;
openfl.text.FontType.EMBEDDED.__enum__ = openfl.text.FontType;
openfl.text.FontType.EMBEDDED_CFF = ["EMBEDDED_CFF",2];
openfl.text.FontType.EMBEDDED_CFF.toString = $estr;
openfl.text.FontType.EMBEDDED_CFF.__enum__ = openfl.text.FontType;
openfl.text.GridFitType = $hxClasses["openfl.text.GridFitType"] = { __ename__ : ["openfl","text","GridFitType"], __constructs__ : ["NONE","PIXEL","SUBPIXEL"] };
openfl.text.GridFitType.NONE = ["NONE",0];
openfl.text.GridFitType.NONE.toString = $estr;
openfl.text.GridFitType.NONE.__enum__ = openfl.text.GridFitType;
openfl.text.GridFitType.PIXEL = ["PIXEL",1];
openfl.text.GridFitType.PIXEL.toString = $estr;
openfl.text.GridFitType.PIXEL.__enum__ = openfl.text.GridFitType;
openfl.text.GridFitType.SUBPIXEL = ["SUBPIXEL",2];
openfl.text.GridFitType.SUBPIXEL.toString = $estr;
openfl.text.GridFitType.SUBPIXEL.__enum__ = openfl.text.GridFitType;
openfl.text.TextField = function() {
	openfl.display.InteractiveObject.call(this);
	this.__width = 100;
	this.__height = 100;
	this.__text = "";
	this.set_type(openfl.text.TextFieldType.DYNAMIC);
	this.set_autoSize(openfl.text.TextFieldAutoSize.NONE);
	this.displayAsPassword = false;
	this.embedFonts = false;
	this.selectable = true;
	this.set_borderColor(0);
	this.set_border(false);
	this.set_backgroundColor(16777215);
	this.set_background(false);
	this.gridFitType = openfl.text.GridFitType.PIXEL;
	this.maxChars = 0;
	this.multiline = false;
	this.sharpness = 0;
	this.scrollH = 0;
	this.scrollV = 1;
	this.set_wordWrap(false);
	if(openfl.text.TextField.__defaultTextFormat == null) {
		openfl.text.TextField.__defaultTextFormat = new openfl.text.TextFormat("Times New Roman",12,0,false,false,false,"","",openfl.text.TextFormatAlign.LEFT,0,0,0,0);
		openfl.text.TextField.__defaultTextFormat.blockIndent = 0;
		openfl.text.TextField.__defaultTextFormat.bullet = false;
		openfl.text.TextField.__defaultTextFormat.letterSpacing = 0;
		openfl.text.TextField.__defaultTextFormat.kerning = false;
	}
	this.__textFormat = openfl.text.TextField.__defaultTextFormat.clone();
};
$hxClasses["openfl.text.TextField"] = openfl.text.TextField;
openfl.text.TextField.__name__ = ["openfl","text","TextField"];
openfl.text.TextField.__defaultTextFormat = null;
openfl.text.TextField.__super__ = openfl.display.InteractiveObject;
openfl.text.TextField.prototype = $extend(openfl.display.InteractiveObject.prototype,{
	appendText: function(text) {
		var _g = this;
		_g.set_text(_g.get_text() + text);
	}
	,getCharBoundaries: function(a) {
		openfl.Lib.notImplemented("TextField.getCharBoundaries");
		return null;
	}
	,getCharIndexAtPoint: function(x,y) {
		openfl.Lib.notImplemented("TextField.getCharIndexAtPoint");
		return 0;
	}
	,getLineIndexAtPoint: function(x,y) {
		openfl.Lib.notImplemented("TextField.getLineIndexAtPoint");
		return 0;
	}
	,getLineMetrics: function(lineIndex) {
		openfl.Lib.notImplemented("TextField.getLineMetrics");
		return null;
	}
	,getLineOffset: function(lineIndex) {
		openfl.Lib.notImplemented("TextField.getLineOffset");
		return 0;
	}
	,getLineText: function(lineIndex) {
		openfl.Lib.notImplemented("TextField.getLineText");
		return "";
	}
	,getTextFormat: function(beginIndex,endIndex) {
		if(endIndex == null) endIndex = 0;
		if(beginIndex == null) beginIndex = 0;
		return this.__textFormat.clone();
	}
	,setSelection: function(beginIndex,endIndex) {
		openfl.Lib.notImplemented("TextField.setSelection");
	}
	,setTextFormat: function(format,beginIndex,endIndex) {
		if(endIndex == null) endIndex = 0;
		if(beginIndex == null) beginIndex = 0;
		if(format.font != null) this.__textFormat.font = format.font;
		if(format.size != null) this.__textFormat.size = format.size;
		if(format.color != null) this.__textFormat.color = format.color;
		if(format.bold != null) this.__textFormat.bold = format.bold;
		if(format.italic != null) this.__textFormat.italic = format.italic;
		if(format.underline != null) this.__textFormat.underline = format.underline;
		if(format.url != null) this.__textFormat.url = format.url;
		if(format.target != null) this.__textFormat.target = format.target;
		if(format.align != null) this.__textFormat.align = format.align;
		if(format.leftMargin != null) this.__textFormat.leftMargin = format.leftMargin;
		if(format.rightMargin != null) this.__textFormat.rightMargin = format.rightMargin;
		if(format.indent != null) this.__textFormat.indent = format.indent;
		if(format.leading != null) this.__textFormat.leading = format.leading;
		if(format.blockIndent != null) this.__textFormat.blockIndent = format.blockIndent;
		if(format.bullet != null) this.__textFormat.bullet = format.bullet;
		if(format.kerning != null) this.__textFormat.kerning = format.kerning;
		if(format.letterSpacing != null) this.__textFormat.letterSpacing = format.letterSpacing;
		if(format.tabStops != null) this.__textFormat.tabStops = format.tabStops;
		this.__dirty = true;
	}
	,__getBounds: function(rect,matrix) {
		var bounds = new openfl.geom.Rectangle(0,0,this.__width,this.__height);
		bounds.transform(this.__worldTransform);
		rect.__expand(bounds.x,bounds.y,bounds.width,bounds.height);
	}
	,__getFont: function(format) {
		var font;
		if(format.italic) font = "italic "; else font = "normal ";
		font += "normal ";
		if(format.bold) font += "bold "; else font += "normal ";
		font += format.size + "px";
		font += "/" + (format.size + format.leading + 4) + "px ";
		font += "'" + (function($this) {
			var $r;
			var _g = format.font;
			$r = (function($this) {
				var $r;
				switch(_g) {
				case "_sans":
					$r = "sans-serif";
					break;
				case "_serif":
					$r = "serif";
					break;
				case "_typewriter":
					$r = "monospace";
					break;
				default:
					$r = format.font;
				}
				return $r;
			}($this));
			return $r;
		}(this));
		font += "'";
		return font;
	}
	,__hitTest: function(x,y,shapeFlag,stack,interactiveOnly) {
		if(!this.get_visible() || interactiveOnly && !this.mouseEnabled) return false;
		var point = this.globalToLocal(new openfl.geom.Point(x,y));
		if(point.x > 0 && point.y > 0 && point.x <= this.__width && point.y <= this.__height) {
			if(stack != null) stack.push(this);
			return true;
		}
		return false;
	}
	,__measureText: function() {
		if(this.__ranges == null) {
			this.__context.font = this.__getFont(this.__textFormat);
			return [this.__context.measureText(this.__text).width];
		} else {
			var measurements = [];
			var _g = 0;
			var _g1 = this.__ranges;
			while(_g < _g1.length) {
				var range = _g1[_g];
				++_g;
				this.__context.font = this.__getFont(range.format);
				measurements.push(this.__context.measureText(this.get_text().substring(range.start,range.end)).width);
			}
			return measurements;
		}
	}
	,__measureTextWithDOM: function() {
		var div = this.__div;
		if(this.__div == null) {
			div = window.document.createElement("div");
			div.innerHTML = this.__text;
			div.style.setProperty("font",this.__getFont(this.__textFormat),null);
			div.style.position = "absolute";
			div.style.top = "110%";
			window.document.body.appendChild(div);
		}
		this.__measuredWidth = div.clientWidth;
		if(this.__div == null) div.style.width = "" + this.__width + "px";
		this.__measuredHeight = div.clientHeight;
		if(this.__div == null) window.document.body.removeChild(div);
	}
	,__renderCanvas: function(renderSession) {
		if(!this.__renderable || this.__worldAlpha <= 0) return;
		if(this.__dirty) {
			if((this.__text == null || this.__text == "") && !this.background && !this.border || (this.get_width() <= 0 || this.get_height() <= 0) && this.autoSize != openfl.text.TextFieldAutoSize.LEFT) {
				this.__canvas = null;
				this.__context = null;
			} else {
				if(this.__canvas == null) {
					this.__canvas = window.document.createElement("canvas");
					this.__context = this.__canvas.getContext("2d");
				}
				if(this.__text != null && this.__text != "") {
					var measurements = this.__measureText();
					var textWidth = 0.0;
					var _g = 0;
					while(_g < measurements.length) {
						var measurement = measurements[_g];
						++_g;
						textWidth += measurement;
					}
					if(this.autoSize == openfl.text.TextFieldAutoSize.LEFT) this.__width = textWidth + 4;
					this.__canvas.width = Math.ceil(this.__width);
					this.__canvas.height = Math.ceil(this.__height);
					if(this.border || this.background) {
						this.__context.rect(0.5,0.5,this.__width - 1,this.__height - 1);
						if(this.background) {
							this.__context.fillStyle = "#" + StringTools.hex(this.backgroundColor,6);
							this.__context.fill();
						}
						if(this.border) {
							this.__context.lineWidth = 1;
							this.__context.strokeStyle = "#" + StringTools.hex(this.borderColor,6);
							this.__context.stroke();
						}
					}
					if(this.__ranges == null) this.__renderText(this.get_text(),this.__textFormat,0); else {
						var currentIndex = 0;
						var range;
						var offsetX = 0.0;
						var _g1 = 0;
						var _g2 = this.__ranges.length;
						while(_g1 < _g2) {
							var i = _g1++;
							range = this.__ranges[i];
							this.__renderText(this.get_text().substring(range.start,range.end),range.format,offsetX);
							offsetX += measurements[i];
						}
					}
				} else {
					if(this.autoSize == openfl.text.TextFieldAutoSize.LEFT) this.__width = 4;
					this.__canvas.width = Math.ceil(this.__width);
					this.__canvas.height = Math.ceil(this.__height);
					if(this.border || this.background) {
						if(this.border) this.__context.rect(0.5,0.5,this.__width - 1,this.__height - 1); else this.__context.rect(0,0,this.__width,this.__height);
						if(this.background) {
							this.__context.fillStyle = "#" + StringTools.hex(this.backgroundColor,6);
							this.__context.fill();
						}
						if(this.border) {
							this.__context.lineWidth = 1;
							this.__context.lineCap = "square";
							this.__context.strokeStyle = "#" + StringTools.hex(this.borderColor,6);
							this.__context.stroke();
						}
					}
				}
			}
			this.__dirty = false;
		}
		if(this.__canvas != null) {
			var context = renderSession.context;
			context.globalAlpha = this.__worldAlpha;
			var transform = this.__worldTransform;
			if(renderSession.roundPixels) context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx | 0,transform.ty | 0); else context.setTransform(transform.a,transform.b,transform.c,transform.d,transform.tx,transform.ty);
			if(this.get_scrollRect() == null) context.drawImage(this.__canvas,0,0); else context.drawImage(this.__canvas,this.get_scrollRect().x,this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height,this.get_scrollRect().x,this.get_scrollRect().y,this.get_scrollRect().width,this.get_scrollRect().height);
		}
	}
	,__renderDOM: function(renderSession) {
		if(this.stage != null && this.__worldVisible && this.__renderable) {
			if(this.__dirty || this.__div == null) {
				if(this.__text != "" || this.background || this.border) {
					if(this.__div == null) {
						this.__div = window.document.createElement("div");
						this.__initializeElement(this.__div,renderSession);
						this.__style.setProperty("cursor","inherit",null);
					}
					this.__div.innerHTML = this.__text;
					if(this.background) this.__style.setProperty("background-color","#" + StringTools.hex(this.backgroundColor,6),null); else this.__style.removeProperty("background-color");
					if(this.border) this.__style.setProperty("border","solid 1px #" + StringTools.hex(this.borderColor,6),null); else this.__style.removeProperty("border");
					this.__style.setProperty("font",this.__getFont(this.__textFormat),null);
					this.__style.setProperty("color","#" + StringTools.hex(this.__textFormat.color,6),null);
					if(this.autoSize != openfl.text.TextFieldAutoSize.NONE) this.__style.setProperty("width","auto",null); else this.__style.setProperty("width",this.__width + "px",null);
					this.__style.setProperty("height",this.__height + "px",null);
					var _g = this.__textFormat.align;
					switch(_g[1]) {
					case 3:
						this.__style.setProperty("text-align","center",null);
						break;
					case 1:
						this.__style.setProperty("text-align","right",null);
						break;
					default:
						this.__style.setProperty("text-align","left",null);
					}
					this.__dirty = false;
				} else if(this.__div != null) {
					renderSession.element.removeChild(this.__div);
					this.__div = null;
				}
			}
			if(this.__div != null) this.__applyStyle(renderSession,true,true,false);
		} else if(this.__div != null) {
			renderSession.element.removeChild(this.__div);
			this.__div = null;
			this.__style = null;
		}
	}
	,__renderText: function(text,format,offsetX) {
		this.__context.font = this.__getFont(format);
		this.__context.textBaseline = "top";
		this.__context.fillStyle = "#" + StringTools.hex(format.color,6);
		var lines = text.split("\n");
		var yOffset = 0;
		var _g = 0;
		while(_g < lines.length) {
			var line = lines[_g];
			++_g;
			var _g1 = format.align;
			switch(_g1[1]) {
			case 3:
				this.__context.textAlign = "center";
				this.__context.fillText(line,this.__width / 2,2 + yOffset,this.__width - 4);
				break;
			case 1:
				this.__context.textAlign = "end";
				this.__context.fillText(line,this.__width - 2,2 + yOffset,this.__width - 4);
				break;
			default:
				this.__context.textAlign = "start";
				this.__context.fillText(line,2 + offsetX,2 + yOffset,this.__width - 4);
			}
			yOffset += this.get_textHeight();
		}
	}
	,set_autoSize: function(value) {
		if(value != this.autoSize) this.__dirty = true;
		return this.autoSize = value;
	}
	,set_background: function(value) {
		if(value != this.background) this.__dirty = true;
		return this.background = value;
	}
	,set_backgroundColor: function(value) {
		if(value != this.backgroundColor) this.__dirty = true;
		return this.backgroundColor = value;
	}
	,set_border: function(value) {
		if(value != this.border) this.__dirty = true;
		return this.border = value;
	}
	,set_borderColor: function(value) {
		if(value != this.borderColor) this.__dirty = true;
		return this.borderColor = value;
	}
	,get_bottomScrollV: function() {
		return this.get_numLines();
	}
	,get_caretPos: function() {
		return 0;
	}
	,get_defaultTextFormat: function() {
		return this.__textFormat.clone();
	}
	,set_defaultTextFormat: function(value) {
		this.__textFormat.__merge(value);
		return value;
	}
	,get_height: function() {
		return this.__height * this.get_scaleY();
	}
	,set_height: function(value) {
		if(this.get_scaleY() != 1 || value != this.__height) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
			this.__dirty = true;
		}
		this.set_scaleY(1);
		return this.__height = value;
	}
	,get_htmlText: function() {
		return this.__text;
	}
	,set_htmlText: function(value) {
		if(!this.__isHTML || this.__text != value) this.__dirty = true;
		this.__ranges = null;
		this.__isHTML = true;
		return this.__text = value;
	}
	,get_maxScrollH: function() {
		return 0;
	}
	,get_maxScrollV: function() {
		return 1;
	}
	,get_numLines: function() {
		if(this.get_text() != "" && this.get_text() != null) {
			var count = this.get_text().split("\n").length;
			if(this.__isHTML) count += this.get_text().split("<br>").length - 1;
			return count;
		}
		return 1;
	}
	,get_text: function() {
		if(this.__isHTML) {
		}
		return this.__text;
	}
	,set_text: function(value) {
		if(this.__isHTML || this.__text != value) this.__dirty = true;
		this.__ranges = null;
		this.__isHTML = false;
		return this.__text = value;
	}
	,get_textColor: function() {
		return this.__textFormat.color;
	}
	,set_textColor: function(value) {
		if(value != this.__textFormat.color) this.__dirty = true;
		if(this.__ranges != null) {
			var _g = 0;
			var _g1 = this.__ranges;
			while(_g < _g1.length) {
				var range = _g1[_g];
				++_g;
				range.format.color = value;
			}
		}
		return this.__textFormat.color = value;
	}
	,get_textWidth: function() {
		if(this.__canvas != null) {
			var sizes = this.__measureText();
			var total = 0;
			var _g = 0;
			while(_g < sizes.length) {
				var size = sizes[_g];
				++_g;
				total += size;
			}
			return total;
		} else if(this.__div != null) return this.__div.clientWidth; else {
			this.__measureTextWithDOM();
			return this.__measuredWidth;
		}
	}
	,get_textHeight: function() {
		if(this.__canvas != null) return this.__textFormat.size * 1.185; else if(this.__div != null) return this.__div.clientHeight; else {
			this.__measureTextWithDOM();
			return this.__measuredHeight + this.__textFormat.size * 0.185;
		}
	}
	,set_type: function(value) {
		return this.type = value;
	}
	,get_width: function() {
		if(this.autoSize == openfl.text.TextFieldAutoSize.LEFT) return (this.get_textWidth() + 4) * this.get_scaleX(); else return this.__width * this.get_scaleX();
	}
	,set_width: function(value) {
		if(this.get_scaleX() != 1 || this.__width != value) {
			if(!this.__transformDirty) {
				this.__transformDirty = true;
				openfl.display.DisplayObject.__worldTransformDirty++;
			}
			this.__dirty = true;
		}
		this.set_scaleX(1);
		return this.__width = value;
	}
	,get_wordWrap: function() {
		return this.wordWrap;
	}
	,set_wordWrap: function(value) {
		return this.wordWrap = value;
	}
	,__class__: openfl.text.TextField
	,__properties__: $extend(openfl.display.InteractiveObject.prototype.__properties__,{set_wordWrap:"set_wordWrap",get_wordWrap:"get_wordWrap",set_type:"set_type",get_textWidth:"get_textWidth",get_textHeight:"get_textHeight",set_textColor:"set_textColor",get_textColor:"get_textColor",set_text:"set_text",get_text:"get_text",get_numLines:"get_numLines",get_maxScrollV:"get_maxScrollV",get_maxScrollH:"get_maxScrollH",set_htmlText:"set_htmlText",get_htmlText:"get_htmlText",set_defaultTextFormat:"set_defaultTextFormat",get_defaultTextFormat:"get_defaultTextFormat",get_caretPos:"get_caretPos",get_bottomScrollV:"get_bottomScrollV",set_borderColor:"set_borderColor",set_border:"set_border",set_backgroundColor:"set_backgroundColor",set_background:"set_background",set_autoSize:"set_autoSize"})
});
openfl.text.TextFormatRange = function(format,start,end) {
	this.format = format;
	this.start = start;
	this.end = end;
};
$hxClasses["openfl.text.TextFormatRange"] = openfl.text.TextFormatRange;
openfl.text.TextFormatRange.__name__ = ["openfl","text","TextFormatRange"];
openfl.text.TextFormatRange.prototype = {
	__class__: openfl.text.TextFormatRange
};
openfl.text.TextFieldAutoSize = $hxClasses["openfl.text.TextFieldAutoSize"] = { __ename__ : ["openfl","text","TextFieldAutoSize"], __constructs__ : ["CENTER","LEFT","NONE","RIGHT"] };
openfl.text.TextFieldAutoSize.CENTER = ["CENTER",0];
openfl.text.TextFieldAutoSize.CENTER.toString = $estr;
openfl.text.TextFieldAutoSize.CENTER.__enum__ = openfl.text.TextFieldAutoSize;
openfl.text.TextFieldAutoSize.LEFT = ["LEFT",1];
openfl.text.TextFieldAutoSize.LEFT.toString = $estr;
openfl.text.TextFieldAutoSize.LEFT.__enum__ = openfl.text.TextFieldAutoSize;
openfl.text.TextFieldAutoSize.NONE = ["NONE",2];
openfl.text.TextFieldAutoSize.NONE.toString = $estr;
openfl.text.TextFieldAutoSize.NONE.__enum__ = openfl.text.TextFieldAutoSize;
openfl.text.TextFieldAutoSize.RIGHT = ["RIGHT",3];
openfl.text.TextFieldAutoSize.RIGHT.toString = $estr;
openfl.text.TextFieldAutoSize.RIGHT.__enum__ = openfl.text.TextFieldAutoSize;
openfl.text.TextFieldType = $hxClasses["openfl.text.TextFieldType"] = { __ename__ : ["openfl","text","TextFieldType"], __constructs__ : ["DYNAMIC","INPUT"] };
openfl.text.TextFieldType.DYNAMIC = ["DYNAMIC",0];
openfl.text.TextFieldType.DYNAMIC.toString = $estr;
openfl.text.TextFieldType.DYNAMIC.__enum__ = openfl.text.TextFieldType;
openfl.text.TextFieldType.INPUT = ["INPUT",1];
openfl.text.TextFieldType.INPUT.toString = $estr;
openfl.text.TextFieldType.INPUT.__enum__ = openfl.text.TextFieldType;
openfl.text.TextFormat = function(font,size,color,bold,italic,underline,url,target,align,leftMargin,rightMargin,indent,leading) {
	this.font = font;
	this.size = size;
	this.color = color;
	this.bold = bold;
	this.italic = italic;
	this.underline = underline;
	this.url = url;
	this.target = target;
	this.align = align;
	this.leftMargin = leftMargin;
	this.rightMargin = rightMargin;
	this.indent = indent;
	this.leading = leading;
};
$hxClasses["openfl.text.TextFormat"] = openfl.text.TextFormat;
openfl.text.TextFormat.__name__ = ["openfl","text","TextFormat"];
openfl.text.TextFormat.prototype = {
	clone: function() {
		var newFormat = new openfl.text.TextFormat(this.font,this.size,this.color,this.bold,this.italic,this.underline,this.url,this.target);
		newFormat.align = this.align;
		newFormat.leftMargin = this.leftMargin;
		newFormat.rightMargin = this.rightMargin;
		newFormat.indent = this.indent;
		newFormat.leading = this.leading;
		newFormat.blockIndent = this.blockIndent;
		newFormat.bullet = this.bullet;
		newFormat.kerning = this.kerning;
		newFormat.letterSpacing = this.letterSpacing;
		newFormat.tabStops = this.tabStops;
		return newFormat;
	}
	,__merge: function(format) {
		if(format.font != null) this.font = format.font;
		if(format.size != null) this.size = format.size;
		if(format.color != null) this.color = format.color;
		if(format.bold != null) this.bold = format.bold;
		if(format.italic != null) this.italic = format.italic;
		if(format.underline != null) this.underline = format.underline;
		if(format.url != null) this.url = format.url;
		if(format.target != null) this.target = format.target;
		if(format.align != null) this.align = format.align;
		if(format.leftMargin != null) this.leftMargin = format.leftMargin;
		if(format.rightMargin != null) this.rightMargin = format.rightMargin;
		if(format.indent != null) this.indent = format.indent;
		if(format.leading != null) this.leading = format.leading;
		if(format.blockIndent != null) this.blockIndent = format.blockIndent;
		if(format.bullet != null) this.bullet = format.bullet;
		if(format.kerning != null) this.kerning = format.kerning;
		if(format.letterSpacing != null) this.letterSpacing = format.letterSpacing;
		if(format.tabStops != null) this.tabStops = format.tabStops;
	}
	,__class__: openfl.text.TextFormat
};
openfl.text.TextFormatAlign = $hxClasses["openfl.text.TextFormatAlign"] = { __ename__ : ["openfl","text","TextFormatAlign"], __constructs__ : ["LEFT","RIGHT","JUSTIFY","CENTER"] };
openfl.text.TextFormatAlign.LEFT = ["LEFT",0];
openfl.text.TextFormatAlign.LEFT.toString = $estr;
openfl.text.TextFormatAlign.LEFT.__enum__ = openfl.text.TextFormatAlign;
openfl.text.TextFormatAlign.RIGHT = ["RIGHT",1];
openfl.text.TextFormatAlign.RIGHT.toString = $estr;
openfl.text.TextFormatAlign.RIGHT.__enum__ = openfl.text.TextFormatAlign;
openfl.text.TextFormatAlign.JUSTIFY = ["JUSTIFY",2];
openfl.text.TextFormatAlign.JUSTIFY.toString = $estr;
openfl.text.TextFormatAlign.JUSTIFY.__enum__ = openfl.text.TextFormatAlign;
openfl.text.TextFormatAlign.CENTER = ["CENTER",3];
openfl.text.TextFormatAlign.CENTER.toString = $estr;
openfl.text.TextFormatAlign.CENTER.__enum__ = openfl.text.TextFormatAlign;
openfl.text.TextLineMetrics = function(x,width,height,ascent,descent,leading) {
	this.x = x;
	this.width = width;
	this.height = height;
	this.ascent = ascent;
	this.descent = descent;
	this.leading = leading;
};
$hxClasses["openfl.text.TextLineMetrics"] = openfl.text.TextLineMetrics;
openfl.text.TextLineMetrics.__name__ = ["openfl","text","TextLineMetrics"];
openfl.text.TextLineMetrics.prototype = {
	__class__: openfl.text.TextLineMetrics
};
openfl.ui = {};
openfl.ui.KeyLocation = $hxClasses["openfl.ui.KeyLocation"] = { __ename__ : ["openfl","ui","KeyLocation"], __constructs__ : ["STANDARD","LEFT","NUM_PAD","RIGHT"] };
openfl.ui.KeyLocation.STANDARD = ["STANDARD",0];
openfl.ui.KeyLocation.STANDARD.toString = $estr;
openfl.ui.KeyLocation.STANDARD.__enum__ = openfl.ui.KeyLocation;
openfl.ui.KeyLocation.LEFT = ["LEFT",1];
openfl.ui.KeyLocation.LEFT.toString = $estr;
openfl.ui.KeyLocation.LEFT.__enum__ = openfl.ui.KeyLocation;
openfl.ui.KeyLocation.NUM_PAD = ["NUM_PAD",2];
openfl.ui.KeyLocation.NUM_PAD.toString = $estr;
openfl.ui.KeyLocation.NUM_PAD.__enum__ = openfl.ui.KeyLocation;
openfl.ui.KeyLocation.RIGHT = ["RIGHT",3];
openfl.ui.KeyLocation.RIGHT.toString = $estr;
openfl.ui.KeyLocation.RIGHT.__enum__ = openfl.ui.KeyLocation;
openfl.ui.Keyboard = function() { };
$hxClasses["openfl.ui.Keyboard"] = openfl.ui.Keyboard;
openfl.ui.Keyboard.__name__ = ["openfl","ui","Keyboard"];
openfl.ui.Keyboard.capsLock = null;
openfl.ui.Keyboard.numLock = null;
openfl.ui.Keyboard.isAccessible = function() {
	return false;
};
openfl.ui.Keyboard.__convertMozillaCode = function(code) {
	switch(code) {
	case 8:
		return 8;
	case 9:
		return 9;
	case 13:
		return 13;
	case 14:
		return 13;
	case 16:
		return 16;
	case 17:
		return 17;
	case 20:
		return 20;
	case 27:
		return 27;
	case 32:
		return 32;
	case 33:
		return 33;
	case 34:
		return 34;
	case 35:
		return 35;
	case 36:
		return 36;
	case 37:
		return 37;
	case 39:
		return 39;
	case 38:
		return 38;
	case 40:
		return 40;
	case 45:
		return 45;
	case 46:
		return 46;
	case 144:
		return 144;
	default:
		return code;
	}
};
openfl.ui.Keyboard.__convertWebkitCode = function(code) {
	var _g = code.toLowerCase();
	switch(_g) {
	case "backspace":
		return 8;
	case "tab":
		return 9;
	case "enter":
		return 13;
	case "shift":
		return 16;
	case "control":
		return 17;
	case "capslock":
		return 20;
	case "escape":
		return 27;
	case "space":
		return 32;
	case "pageup":
		return 33;
	case "pagedown":
		return 34;
	case "end":
		return 35;
	case "home":
		return 36;
	case "left":
		return 37;
	case "right":
		return 39;
	case "up":
		return 38;
	case "down":
		return 40;
	case "insert":
		return 45;
	case "delete":
		return 46;
	case "numlock":
		return 144;
	case "break":
		return 19;
	}
	if(code.indexOf("U+") == 0) return Std.parseInt("0x" + HxOverrides.substr(code,3,null));
	throw "Unrecognized key code: " + code;
	return 0;
};
openfl.utils = {};
openfl.utils.ByteArray = function() {
	this.littleEndian = false;
	this.allocated = 0;
	this.position = 0;
	this.length = 0;
	this.___resizeBuffer(this.allocated);
};
$hxClasses["openfl.utils.ByteArray"] = openfl.utils.ByteArray;
openfl.utils.ByteArray.__name__ = ["openfl","utils","ByteArray"];
openfl.utils.ByteArray.fromBytes = function(inBytes) {
	var result = new openfl.utils.ByteArray();
	result.byteView = new Uint8Array(inBytes.b);
	result.set_length(result.byteView.length);
	result.allocated = result.length;
	return result;
};
openfl.utils.ByteArray.__ofBuffer = function(buffer) {
	var bytes = new openfl.utils.ByteArray();
	bytes.set_length(bytes.allocated = buffer.byteLength);
	bytes.data = new DataView(buffer);
	bytes.byteView = new Uint8Array(buffer);
	return bytes;
};
openfl.utils.ByteArray.prototype = {
	clear: function() {
		if(this.allocated < 0) this.___resizeBuffer(this.allocated = Std.int(Math.max(0,this.allocated * 2))); else if(this.allocated > 0) this.___resizeBuffer(this.allocated = 0);
		this.length = 0;
		0;
		this.position = 0;
	}
	,readBoolean: function() {
		return this.readByte() != 0;
	}
	,readByte: function() {
		var data = this.data;
		return data.getInt8(this.position++);
	}
	,readBytes: function(bytes,offset,length) {
		if(length == null) length = 0;
		if(offset == null) offset = 0;
		if(offset < 0 || length < 0) throw new openfl.errors.IOError("Read error - Out of bounds");
		if(length == 0) length = this.length - this.position;
		var lengthToEnsure = offset + length;
		if(bytes.length < lengthToEnsure) {
			if(bytes.allocated < lengthToEnsure) bytes.___resizeBuffer(bytes.allocated = Std.int(Math.max(lengthToEnsure,bytes.allocated * 2))); else if(bytes.allocated > lengthToEnsure) bytes.___resizeBuffer(bytes.allocated = lengthToEnsure);
			bytes.length = lengthToEnsure;
			lengthToEnsure;
		}
		bytes.byteView.set(this.byteView.subarray(this.position,this.position + length),offset);
		bytes.position = offset;
		this.position += length;
		if(bytes.position + length > bytes.length) bytes.set_length(bytes.position + length);
	}
	,readDouble: function() {
		var double = this.data.getFloat64(this.position,this.littleEndian);
		this.position += 8;
		return double;
	}
	,readFloat: function() {
		var float = this.data.getFloat32(this.position,this.littleEndian);
		this.position += 4;
		return float;
	}
	,readFullBytes: function(bytes,pos,len) {
		if(this.length < len) {
			if(this.allocated < len) this.___resizeBuffer(this.allocated = Std.int(Math.max(len,this.allocated * 2))); else if(this.allocated > len) this.___resizeBuffer(this.allocated = len);
			this.length = len;
			len;
		}
		var _g1 = pos;
		var _g = pos + len;
		while(_g1 < _g) {
			var i = _g1++;
			var data = this.data;
			data.setInt8(this.position++,bytes.b[i]);
		}
	}
	,readInt: function() {
		var int = this.data.getInt32(this.position,this.littleEndian);
		this.position += 4;
		return int;
	}
	,readMultiByte: function(length,charSet) {
		return this.readUTFBytes(length);
	}
	,readShort: function() {
		var short = this.data.getInt16(this.position,this.littleEndian);
		this.position += 2;
		return short;
	}
	,readUnsignedByte: function() {
		var data = this.data;
		return data.getUint8(this.position++);
	}
	,readUnsignedInt: function() {
		var uInt = this.data.getUint32(this.position,this.littleEndian);
		this.position += 4;
		return uInt;
	}
	,readUnsignedShort: function() {
		var uShort = this.data.getUint16(this.position,this.littleEndian);
		this.position += 2;
		return uShort;
	}
	,readUTF: function() {
		var bytesCount = this.readUnsignedShort();
		return this.readUTFBytes(bytesCount);
	}
	,readUTFBytes: function(len) {
		var value = "";
		var max = this.position + len;
		while(this.position < max) {
			var data = this.data;
			var c = data.getUint8(this.position++);
			if(c < 128) {
				if(c == 0) break;
				value += String.fromCharCode(c);
			} else if(c < 224) value += String.fromCharCode((c & 63) << 6 | data.getUint8(this.position++) & 127); else if(c < 240) {
				var c2 = data.getUint8(this.position++);
				value += String.fromCharCode((c & 31) << 12 | (c2 & 127) << 6 | data.getUint8(this.position++) & 127);
			} else {
				var c21 = data.getUint8(this.position++);
				var c3 = data.getUint8(this.position++);
				value += String.fromCharCode((c & 15) << 18 | (c21 & 127) << 12 | c3 << 6 & 127 | data.getUint8(this.position++) & 127);
			}
		}
		return value;
	}
	,toString: function() {
		var cachePosition = this.position;
		this.position = 0;
		var value = this.readUTFBytes(this.length);
		this.position = cachePosition;
		return value;
	}
	,writeBoolean: function(value) {
		this.writeByte(value?1:0);
	}
	,writeByte: function(value) {
		var lengthToEnsure = this.position + 1;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		var data = this.data;
		data.setInt8(this.position,value);
		this.position += 1;
	}
	,writeBytes: function(bytes,offset,length) {
		if(length == null) length = 0;
		if(offset == null) offset = 0;
		if(offset < 0 || length < 0) throw new openfl.errors.IOError("Write error - Out of bounds");
		if(length == 0) length = bytes.length;
		var lengthToEnsure = this.position + length;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.byteView.set(bytes.byteView.subarray(offset,offset + length),this.position);
		this.position += length;
	}
	,writeDouble: function(x) {
		var lengthToEnsure = this.position + 8;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.data.setFloat64(this.position,x,this.littleEndian);
		this.position += 8;
	}
	,writeFloat: function(x) {
		var lengthToEnsure = this.position + 4;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.data.setFloat32(this.position,x,this.littleEndian);
		this.position += 4;
	}
	,writeInt: function(value) {
		var lengthToEnsure = this.position + 4;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.data.setInt32(this.position,value,this.littleEndian);
		this.position += 4;
	}
	,writeShort: function(value) {
		var lengthToEnsure = this.position + 2;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.data.setInt16(this.position,value,this.littleEndian);
		this.position += 2;
	}
	,writeUnsignedInt: function(value) {
		var lengthToEnsure = this.position + 4;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.data.setUint32(this.position,value,this.littleEndian);
		this.position += 4;
	}
	,writeUnsignedShort: function(value) {
		var lengthToEnsure = this.position + 2;
		if(this.length < lengthToEnsure) {
			if(this.allocated < lengthToEnsure) this.___resizeBuffer(this.allocated = Std.int(Math.max(lengthToEnsure,this.allocated * 2))); else if(this.allocated > lengthToEnsure) this.___resizeBuffer(this.allocated = lengthToEnsure);
			this.length = lengthToEnsure;
			lengthToEnsure;
		}
		this.data.setUint16(this.position,value,this.littleEndian);
		this.position += 2;
	}
	,writeUTF: function(value) {
		this.writeUnsignedShort(this._getUTFBytesCount(value));
		this.writeUTFBytes(value);
	}
	,writeUTFBytes: function(value) {
		var _g1 = 0;
		var _g = value.length;
		while(_g1 < _g) {
			var i = _g1++;
			var c = value.charCodeAt(i);
			if(c <= 127) this.writeByte(c); else if(c <= 2047) {
				this.writeByte(192 | c >> 6);
				this.writeByte(128 | c & 63);
			} else if(c <= 65535) {
				this.writeByte(224 | c >> 12);
				this.writeByte(128 | c >> 6 & 63);
				this.writeByte(128 | c & 63);
			} else {
				this.writeByte(240 | c >> 18);
				this.writeByte(128 | c >> 12 & 63);
				this.writeByte(128 | c >> 6 & 63);
				this.writeByte(128 | c & 63);
			}
		}
	}
	,__fromBytes: function(inBytes) {
		this.byteView = new Uint8Array(inBytes.b);
		this.set_length(this.byteView.length);
		this.allocated = this.length;
	}
	,__get: function(pos) {
		return this.data.getInt8(pos);
	}
	,_getUTFBytesCount: function(value) {
		var count = 0;
		var _g1 = 0;
		var _g = value.length;
		while(_g1 < _g) {
			var i = _g1++;
			var c = value.charCodeAt(i);
			if(c <= 127) count += 1; else if(c <= 2047) count += 2; else if(c <= 65535) count += 3; else count += 4;
		}
		return count;
	}
	,___resizeBuffer: function(len) {
		var oldByteView = this.byteView;
		var newByteView = new Uint8Array(len);
		if(oldByteView != null) {
			if(oldByteView.length <= len) newByteView.set(oldByteView); else newByteView.set(oldByteView.subarray(0,len));
		}
		this.byteView = newByteView;
		this.data = new DataView(newByteView.buffer);
	}
	,__getBuffer: function() {
		return this.data.buffer;
	}
	,__set: function(pos,v) {
		this.data.setUint8(pos,v);
	}
	,get_bytesAvailable: function() {
		return this.length - this.position;
	}
	,get_endian: function() {
		if(this.littleEndian) return "littleEndian"; else return "bigEndian";
	}
	,set_endian: function(endian) {
		this.littleEndian = endian == "littleEndian";
		return endian;
	}
	,set_length: function(value) {
		if(this.allocated < value) this.___resizeBuffer(this.allocated = Std.int(Math.max(value,this.allocated * 2))); else if(this.allocated > value) this.___resizeBuffer(this.allocated = value);
		this.length = value;
		return value;
	}
	,__class__: openfl.utils.ByteArray
	,__properties__: {set_length:"set_length",set_endian:"set_endian",get_endian:"get_endian",get_bytesAvailable:"get_bytesAvailable"}
};
openfl.utils.Endian = function() { };
$hxClasses["openfl.utils.Endian"] = openfl.utils.Endian;
openfl.utils.Endian.__name__ = ["openfl","utils","Endian"];
openfl.utils.Timer = function(delay,repeatCount) {
	if(repeatCount == null) repeatCount = 0;
	openfl.events.EventDispatcher.call(this);
	this.running = false;
	this.set_delay(delay);
	this.set_repeatCount(repeatCount);
	this.currentCount = 0;
};
$hxClasses["openfl.utils.Timer"] = openfl.utils.Timer;
openfl.utils.Timer.__name__ = ["openfl","utils","Timer"];
openfl.utils.Timer.__super__ = openfl.events.EventDispatcher;
openfl.utils.Timer.prototype = $extend(openfl.events.EventDispatcher.prototype,{
	reset: function() {
		this.stop();
		this.currentCount = 0;
	}
	,start: function() {
		if(this.running) return;
		this.running = true;
		this.timerId = window.setInterval($bind(this,this.__onInterval),this.delay | 0);
	}
	,stop: function() {
		if(this.timerId != null) {
			window.clearInterval(this.timerId);
			this.timerId = null;
		}
		this.running = false;
	}
	,__onInterval: function() {
		this.currentCount++;
		if(this.repeatCount > 0 && this.currentCount >= this.repeatCount) {
			this.stop();
			this.dispatchEvent(new openfl.events.TimerEvent("timer"));
			this.dispatchEvent(new openfl.events.TimerEvent("timerComplete"));
		} else this.dispatchEvent(new openfl.events.TimerEvent("timer"));
	}
	,set_delay: function(v) {
		if(v != this.delay) {
			var wasRunning = this.running;
			if(this.running) this.stop();
			this.delay = v;
			if(wasRunning) this.start();
		}
		return v;
	}
	,set_repeatCount: function(v) {
		if(this.running && v != 0 && v <= this.currentCount) this.stop();
		this.repeatCount = v;
		return v;
	}
	,__class__: openfl.utils.Timer
	,__properties__: {set_repeatCount:"set_repeatCount",set_delay:"set_delay"}
});
unbox.framework.CourseContext = function(dataUrl,view) {
	this.bgLayer = new openfl.display.Sprite();
	this.bgLayer.set_name("bgLayer");
	this.pageLayer = new openfl.display.Sprite();
	this.pageLayer.set_name("pageLayer");
	this.uiLayer = new openfl.display.Sprite();
	this.uiLayer.set_name("uiLayer");
	view.addChild(this.bgLayer);
	view.addChild(this.pageLayer);
	view.addChild(this.uiLayer);
	this.loader = new unbox.framework.services.LoadService();
	this.loader.loadSingleString(dataUrl,$bind(this,this.init));
};
$hxClasses["unbox.framework.CourseContext"] = unbox.framework.CourseContext;
unbox.framework.CourseContext.__name__ = ["unbox","framework","CourseContext"];
unbox.framework.CourseContext.prototype = {
	init: function(e) {
		var courseData = new haxe.xml.Fast(Xml.parse(e.target.data).firstElement());
		this.courseModel = new unbox.framework.model.CourseModel(courseData);
		this.dataController = new unbox.framework.controller.DataController();
		this.dataController.start(courseData.node.resolve("config"),this.courseModel.get_totalPages() - 1,this.courseModel.get_totalModules() - 1);
		this.loader.add(this.courseModel.get_uiModelUrl(),"xml/ebook_ui.xml");
		this.loader.add(this.courseModel.get_stylesUrl());
		this.loader.add("index",this.courseModel.get_indexViewUrl());
		this.loader.add("nav",this.courseModel.get_uiViewUrl());
		this.loader.start($bind(this,this.onAssetsLoaded));
	}
	,onAssetsLoaded: function(assets) {
		haxe.Log.trace("CourseContext.onAssetsLoaded",{ fileName : "CourseContext.hx", lineNumber : 132, className : "unbox.framework.CourseContext", methodName : "onAssetsLoaded"});
		this.createControllers();
		this.createView(assets);
		this.createSignals();
		this.navController.set_currentPageGlobalID(this.dataController.get_status().get_currentPage());
		this.navController.get_onChange().dispatch(this.navController.get_currentPageGlobalID(),0);
		motion.Actuate.timer(0.1).onComplete(($_=this.dataController,$bind($_,$_.loadBookmarks)),[this.courseModel.get_pages()]);
	}
	,createControllers: function() {
		haxe.Log.trace("CourseContext.createControllers",{ fileName : "CourseContext.hx", lineNumber : 155, className : "unbox.framework.CourseContext", methodName : "createControllers"});
		this.navController = new unbox.framework.controller.NavController(this.courseModel.get_pages(),this.uiLayer);
		this.pageController = new unbox.framework.controller.PageController(this.pageLayer,this);
	}
	,createView: function(assets) {
		haxe.Log.trace("CourseContext.createView",{ fileName : "CourseContext.hx", lineNumber : 177, className : "unbox.framework.CourseContext", methodName : "createView"});
		var uiData = Xml.parse((function($this) {
			var $r;
			var key = $this.courseModel.get_uiModelUrl();
			$r = assets.get(key);
			return $r;
		}(this)));
		this.uiModel = new unbox.framework.model.UIModel(uiData);
		this.dashboard = null;
		this.dashboard = new unbox.framework.view.Dashboard(this.uiLayer,this.uiModel.get_dashboardData(),(function($this) {
			var $r;
			var key1 = $this.courseModel.get_uiViewUrl();
			$r = assets.get(key1);
			return $r;
		}(this)));
		this.navBar = new unbox.framework.view.NavBar(this.uiLayer,this.uiModel.get_navBarData(),(function($this) {
			var $r;
			var key2 = $this.courseModel.get_uiViewUrl();
			$r = assets.get(key2);
			return $r;
		}(this)));
		this.progressMeter = new unbox.framework.view.components.BarMeter(this.uiLayer,this.uiModel.get_progressMeterData());
		this.progressMeter.hide();
		this.ttService = new unbox.framework.services.TooltipService();
		var indexView = null;
		indexView = (js.Boot.__cast((function($this) {
			var $r;
			var key3 = $this.courseModel.get_indexViewUrl();
			$r = assets.get(key3);
			return $r;
		}(this)) , openfl.AssetLibrary)).getMovieClip("");
		this.bgLayer.addChild(indexView);
		this.courseModel.set_indexView(indexView);
		var navView = null;
		navView = (js.Boot.__cast((function($this) {
			var $r;
			var key4 = $this.courseModel.get_uiViewUrl();
			$r = assets.get(key4);
			return $r;
		}(this)) , openfl.AssetLibrary)).getMovieClip("");
		this.courseModel.set_navView(navView);
	}
	,createSignals: function() {
		this.navBar.get_onButtonPressed().add(($_=this.navController,$bind($_,$_.handleNavInput)));
		this.navBar.onDashboardToggle.add(($_=this.dashboard,$bind($_,$_.toggle)));
		this.navBar.onHelpToggle.add(($_=this.dashboard,$bind($_,$_.toggleHelp)));
		this.navBar.onBookmarkToggle.add(($_=this.dataController,$bind($_,$_.addBookmark)));
		this.dataController.onBookmarkUpdated.add(($_=this.dashboard,$bind($_,$_.updateBookmarks)));
		this.dataController.onPageBookmarkChecked.add(($_=this.navBar,$bind($_,$_.setBookmarkStatus)));
		this.dataController.populateBookmarks.add(($_=this.dashboard,$bind($_,$_.populateBookmarks)));
		this.pageController.get_onBeforePageChanged().add(($_=this.navBar,$bind($_,$_.updateNavView)));
		this.pageController.get_onPageChanged().add(($_=this.navBar,$bind($_,$_.afterPageChange)));
		this.pageController.get_onPageChanged().add(($_=this.dataController,$bind($_,$_.save)));
		this.pageController.get_onPageChanged().add(($_=this.dataController,$bind($_,$_.checkBookmark)));
		this.pageController.get_onPageChanged().add($bind(this,this.updateProgress));
		this.navController.get_onChangeNavStatus().add(($_=this.navBar,$bind($_,$_.setButtons)));
		this.navController.get_onChange().add($bind(this,this.onPageRequest));
		var searchBox = this.dashboard.getSearchBox();
		searchBox.responder.request.add(($_=this.navController,$bind($_,$_.onPageDataRequest)));
		searchBox.responder.response.add(($_=(js.Boot.__cast(this.dashboard.getPanel(3) , unbox.framework.view.components.SearchPanel)),$bind($_,$_.onSearchDataReceived)));
		this.dashboard.onPanelButtonPressed.add(($_=this.navController,$bind($_,$_.handleNavInput)));
	}
	,onPageRequest: function(pageId,direction) {
		var pData = this.courseModel.getPage(pageId);
		haxe.Log.trace("Starting load operation for page '" + pageId + "' with url : " + pData.get_src(),{ fileName : "CourseContext.hx", lineNumber : 318, className : "unbox.framework.CourseContext", methodName : "onPageRequest"});
		this.pageController.setup(this.courseModel.getPage(pageId),direction);
		this.loader.add(pData.get_id(),pData.get_src());
		var $it0 = (function($this) {
			var $r;
			var this1 = pData.get_contentUrls();
			$r = this1.keys();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var key = $it0.next();
			haxe.Log.trace("Page data asset key : " + key + " | value type : " + (function($this) {
				var $r;
				var this2 = pData.get_contentUrls();
				$r = this2.get(key);
				return $r;
			}(this)),{ fileName : "CourseContext.hx", lineNumber : 329, className : "unbox.framework.CourseContext", methodName : "onPageRequest"});
			this.loader.add((function($this) {
				var $r;
				var this3 = pData.get_contentUrls();
				$r = this3.get(key);
				return $r;
			}(this)),key);
		}
		this.loader.start(($_=this.pageController,$bind($_,$_.onPageAssetsLoaded)));
	}
	,updateProgress: function(page) {
		if(page.data.get_showProgress()) this.progressMeter.show(); else this.progressMeter.hide();
		this.progressMeter.setProgress(page.data.get_localIndex(),page.data.get_numPagesInModule());
	}
	,__class__: unbox.framework.CourseContext
};
unbox.framework.com = {};
unbox.framework.com.hybrid = {};
unbox.framework.com.hybrid.ui = {};
unbox.framework.com.hybrid.ui.ToolTip = function() {
	this.defaultHookPos = new openfl.geom.Point();
	this._padding = 0;
	this._followCursor = true;
	this._bgAlpha = 1;
	this._borderSize = 1;
	this._hookSize = 20;
	this._hideDelay = 0;
	this._showDelay = 0;
	this._hookEnabled = false;
	this._autoSize = false;
	this._bgColors = [16777215,10263708];
	this._cornerRadius = 12;
	this._align = "center";
	this._buffer = 10;
	this._defaultWidth = 200;
	this._contentEmbed = false;
	this._titleEmbed = false;
	this._contentOverride = false;
	this._titleOverride = false;
	this._styleOverride = false;
	this.boxSp = new openfl.display.Sprite();
	this.hookSp = new openfl.display.Sprite();
	this._contentContainer = new openfl.display.Sprite();
	openfl.display.Sprite.call(this);
	this.mouseEnabled = false;
	this.buttonMode = false;
	this.mouseChildren = false;
	this._showTimer = new openfl.utils.Timer(this._showDelay,1);
	this._hideTimer = new openfl.utils.Timer(this._hideDelay,1);
	this._showTimer.addEventListener("timer",$bind(this,this.showTimerHandler));
	this._hideTimer.addEventListener("timer",$bind(this,this.hideTimerHandler));
};
$hxClasses["unbox.framework.com.hybrid.ui.ToolTip"] = unbox.framework.com.hybrid.ui.ToolTip;
unbox.framework.com.hybrid.ui.ToolTip.__name__ = ["unbox","framework","com","hybrid","ui","ToolTip"];
unbox.framework.com.hybrid.ui.ToolTip.__super__ = openfl.display.Sprite;
unbox.framework.com.hybrid.ui.ToolTip.prototype = $extend(openfl.display.Sprite.prototype,{
	setContent: function(title,content) {
		this.boxSp.get_graphics().clear();
		this.addCopy(title,content);
		this.setOffset();
		this.drawBG();
	}
	,show: function(p,title,content) {
		this._stage = p.stage;
		this._parentObject = p;
		var onStage = this.addedToStage(this._contentContainer);
		if(!onStage) {
			this.addChild(this.boxSp);
			this.addChild(this.hookSp);
			this.addChild(this._contentContainer);
		}
		this.addCopy(title,content);
		this.setOffset();
		this.drawBG();
		this.bgGlow();
		var parentCoords = new openfl.geom.Point(this._parentObject.get_mouseX(),this._parentObject.get_mouseY());
		var globalPoint = p.localToGlobal(parentCoords);
		this.set_x(globalPoint.x + this._offSet);
		this.set_y(globalPoint.y - this.get_height() - this._hookSize);
		this.set_alpha(0);
		this._stage.addChild(this);
		this._parentObject.addEventListener(openfl.events.MouseEvent.MOUSE_OUT,$bind(this,this.onMouseOut));
		this.follow(this._followCursor);
		motion.Actuate.stop(this);
		this._hideTimer.reset();
		this._showTimer.start();
	}
	,hide: function() {
		this._parentObject.removeEventListener(openfl.events.MouseEvent.MOUSE_OUT,$bind(this,this.onMouseOut));
		this._showTimer.reset();
		this._hideTimer.start();
	}
	,showTimerHandler: function(event) {
		this.animate(true);
	}
	,hideTimerHandler: function(event) {
		this.animate(false);
	}
	,onMouseOut: function(event) {
		event.currentTarget.removeEventListener(event.type,$bind(this,this.onMouseOut));
		this._hideTimer.start();
	}
	,follow: function(value) {
		if(value) this.addEventListener(openfl.events.Event.ENTER_FRAME,$bind(this,this.eof)); else this.removeEventListener(openfl.events.Event.ENTER_FRAME,$bind(this,this.eof));
	}
	,eof: function(event) {
		this.position();
	}
	,position: function() {
		var speed = .5;
		var parentCoords = new openfl.geom.Point(this._parentObject.get_mouseX(),this._parentObject.get_mouseY());
		var globalPoint = this._parentObject.localToGlobal(parentCoords);
		var xp = globalPoint.x + this._offSet;
		var yp = globalPoint.y - this.get_height() - this._buffer;
		var overhangRight = xp + this.get_width();
		if(overhangRight > this.stage.stageWidth) xp = this.stage.stageWidth - this.get_width() - this._padding;
		if(xp < 0) xp = this._padding;
		if(yp < 0) yp = globalPoint.y + this._hookSize + this._buffer;
		if(this._hookEnabled) {
			var mouseLocalX = this.get_mouseX() - this._hookSize / 2;
			var hookX = this._hookOffSet - this._hookSize / 2;
			var hookY = 1;
			var hookXMin = this._buffer;
			var hookXMax = this._defaultWidth - this._hookSize - this._buffer;
			if(overhangRight > this.stage.stageWidth) if(mouseLocalX < hookXMax) hookX = mouseLocalX; else hookX = hookXMax;
			if(globalPoint.x + this._offSet < 0) if(mouseLocalX < hookXMin) hookX = hookXMin; else hookX = mouseLocalX;
			if(globalPoint.y - this.get_height() - this._buffer < 0) {
				hookY = 1;
				this.hookSp.set_scaleY(-1);
			} else {
				hookY = this.defaultHookPos.y;
				this.hookSp.set_scaleY(1);
			}
			this.hookSp.set_y(hookY);
			this.hookSp.set_x(hookX);
		}
		var _g = this;
		_g.set_x(_g.get_x() + (xp - this.get_x()) * speed);
		var _g1 = this;
		_g1.set_y(_g1.get_y() + (yp - this.get_y()) * speed);
	}
	,addCopy: function(title,content) {
		if(this._tf == null) this._tf = this.createField(this._titleEmbed);
		this._tf.set_htmlText(title);
		if(!this._styleOverride) {
			if(!this._titleOverride) this.initTitleFormat();
			this._tf.setTextFormat(this._titleFormat);
		}
		if(this._autoSize) this._defaultWidth = this._tf.get_textWidth() + 4 + this._buffer * 2; else {
			this._tf.multiline = true;
			this._tf.set_wordWrap(true);
			this._tf.set_width(this._defaultWidth - this._buffer * 2);
		}
		this._tf.set_x(this._tf.set_y(this._buffer));
		this._contentContainer.addChild(this._tf);
		if(content != null) {
			if(this._cf == null) this._cf = this.createField(this._contentEmbed);
			this._cf.set_htmlText(content);
			if(!this._styleOverride) {
				if(!this._contentOverride) this.initContentFormat();
				this._cf.setTextFormat(this._contentFormat);
			}
			var bounds = this.getBounds(this);
			this._cf.set_x(this._buffer);
			this._cf.set_y(this._tf.get_y() + this._tf.get_textHeight());
			if(this._autoSize) {
				var cfWidth = this._cf.get_textWidth() + 4 + this._buffer * 2;
				if(cfWidth > this._defaultWidth) this._defaultWidth = cfWidth; else this._defaultWidth = this._defaultWidth;
			} else {
				this._cf.multiline = true;
				this._cf.set_wordWrap(true);
				this._cf.set_width(this._defaultWidth - this._buffer * 2);
			}
			this._contentContainer.addChild(this._cf);
		}
	}
	,createField: function(embed) {
		var tf = new openfl.text.TextField();
		tf.embedFonts = embed;
		tf.antiAliasType = openfl.text.AntiAliasType.ADVANCED;
		tf.gridFitType = openfl.text.GridFitType.PIXEL;
		tf.set_autoSize(openfl.text.TextFieldAutoSize.LEFT);
		tf.selectable = false;
		if(!this._autoSize) {
			tf.multiline = true;
			tf.set_wordWrap(true);
		}
		return tf;
	}
	,drawBG: function() {
		this.boxSp.get_graphics().clear();
		this.hookSp.get_graphics().clear();
		var bounds = this.getBounds(this);
		var h;
		if(Math.isNaN(this._defaultHeight)) h = bounds.height + this._buffer * 2; else h = this._defaultHeight;
		var fillType = openfl.display.GradientType.LINEAR;
		var alphas = [this._bgAlpha,this._bgAlpha];
		var ratios = [0,255];
		var matr = new openfl.geom.Matrix();
		var radians = 90 * Math.PI / 180;
		matr.createGradientBox(this._defaultWidth,h,radians,0,0);
		var spreadMethod = openfl.display.SpreadMethod.PAD;
		this.boxSp.get_graphics().beginGradientFill(fillType,this._bgColors,alphas,ratios,matr,spreadMethod);
		this.boxSp.get_graphics().drawRoundRect(0,0,this._defaultWidth,h,this._cornerRadius);
		if(this._hookEnabled) {
			var xp = 0;
			var yp = 0;
			this.hookSp.set_x(this._hookOffSet - this._hookSize / 2);
			this.hookSp.set_y(h - 1);
			this.defaultHookPos = new openfl.geom.Point(this.hookSp.get_x(),this.hookSp.get_y());
			this.hookSp.get_graphics().beginFill(this._bgColors[1],alphas[1]);
			this.hookSp.get_graphics().moveTo(0,0);
			this.hookSp.get_graphics().lineTo(this._hookSize,0);
			this.hookSp.get_graphics().lineTo(this._hookSize / 2,this._hookSize / 2);
			this.hookSp.get_graphics().lineTo(0,0);
			this.hookSp.get_graphics().endFill();
		}
	}
	,animate: function(show) {
		if(show) motion.Actuate.tween(this,.5,{ alpha : 1}); else {
			this._showTimer.reset();
			motion.Actuate.tween(this,.2,{ alpha : 0}).onComplete($bind(this,this.cleanUp));
		}
	}
	,onComplete: function() {
		this.cleanUp();
	}
	,set_buffer: function(value) {
		this._buffer = value;
		return value;
	}
	,get_buffer: function() {
		return this._buffer;
	}
	,set_bgAlpha: function(value) {
		this._bgAlpha = value;
		return value;
	}
	,get_bgAlpha: function() {
		return this._bgAlpha;
	}
	,set_tipWidth: function(value) {
		this._defaultWidth = value;
		return value;
	}
	,set_titleFormat: function(tf) {
		this._titleFormat = tf;
		if(this._titleFormat.font == null) this._titleFormat.font = "_sans";
		this._titleOverride = true;
		return tf;
	}
	,set_contentFormat: function(tf) {
		this._contentFormat = tf;
		if(this._contentFormat.font == null) this._contentFormat.font = "_sans";
		this._contentOverride = true;
		return tf;
	}
	,set_align: function(value) {
		var a = value.toLowerCase();
		var values = "right left center";
		if(values.indexOf(value) == -1) throw new openfl.errors.Error(Std.string(this) + " : Invalid Align Property, options are: 'right', 'left' & 'center'"); else this._align = a;
		return value;
	}
	,set_showDelay: function(value) {
		this._showDelay = value;
		this._showTimer.set_delay(value);
		return value;
	}
	,set_hideDelay: function(value) {
		this._hideDelay = value;
		this._hideTimer.set_delay(value);
		return value;
	}
	,set_hook: function(value) {
		this._hookEnabled = value;
		return value;
	}
	,set_hookSize: function(value) {
		this._hookSize = value;
		return value;
	}
	,set_cornerRadius: function(value) {
		this._cornerRadius = value;
		return value;
	}
	,set_colors: function(colArray) {
		this._bgColors = colArray;
		return colArray;
	}
	,set_autoSize: function(value) {
		this._autoSize = value;
		return value;
	}
	,set_border: function(value) {
		this._border = value;
		return value;
	}
	,set_borderSize: function(value) {
		this._borderSize = value;
		return value;
	}
	,set_tipHeight: function(value) {
		this._defaultHeight = value;
		return value;
	}
	,set_titleEmbed: function(value) {
		this._titleEmbed = value;
		return value;
	}
	,set_contentEmbed: function(value) {
		this._contentEmbed = value;
		return value;
	}
	,get_followCursor: function() {
		return this._followCursor;
	}
	,set_followCursor: function(value) {
		this._followCursor = value;
		return value;
	}
	,get_padding: function() {
		return this._padding;
	}
	,set_padding: function(value) {
		this._padding = value;
		return value;
	}
	,textGlow: function(field) {
		var color = 0;
		var alpha = 0.35;
		var blurX = 2;
		var blurY = 2;
		var strength = 1;
		var inner = false;
		var knockout = false;
		var quality = openfl.filters.BitmapFilterQuality.HIGH;
		var filter = new openfl.filters.GlowFilter(color,alpha,blurX,blurY,strength,quality,inner,knockout);
		var myFilters = new Array();
		myFilters.push(filter);
		field.set_filters(myFilters);
	}
	,bgGlow: function() {
		var color = 0;
		var alpha = 0.60;
		var blurX = 5;
		var blurY = 5;
		var strength = 1;
		var inner = false;
		var knockout = false;
		var quality = openfl.filters.BitmapFilterQuality.HIGH;
		var filter = new openfl.filters.GlowFilter(color,alpha,blurX,blurY,strength,quality,inner,knockout);
		var myFilters = new Array();
		myFilters.push(filter);
		this.set_filters(myFilters);
	}
	,initTitleFormat: function() {
		this._titleFormat = new openfl.text.TextFormat();
		this._titleFormat.font = "_sans";
		this._titleFormat.bold = true;
		this._titleFormat.size = 20;
		this._titleFormat.color = 3355443;
	}
	,initContentFormat: function() {
		this._contentFormat = new openfl.text.TextFormat();
		this._contentFormat.font = "_sans";
		this._contentFormat.bold = false;
		this._contentFormat.size = 14;
		this._contentFormat.color = 3355443;
	}
	,addedToStage: function(displayObject) {
		var hasStage = displayObject.stage;
		if(hasStage == null) return false; else return true;
	}
	,setOffset: function() {
		var _sw0_ = this._align;
		switch(_sw0_) {
		case "left":
			this._offSet = -this._defaultWidth + this._buffer + this._hookSize;
			this._hookOffSet = this._defaultWidth - this._buffer - this._hookSize;
			break;
		case "right":
			this._offSet = this._buffer - this._hookSize;
			this._hookOffSet = this._buffer + this._hookSize;
			break;
		case "center":
			this._offSet = -(this._defaultWidth / 2);
			this._hookOffSet = this._defaultWidth / 2;
			break;
		default:
			this._offSet = -(this._defaultWidth / 2);
			this._hookOffSet = this._defaultWidth / 2;
		}
	}
	,cleanUp: function() {
		this._parentObject.removeEventListener(openfl.events.MouseEvent.MOUSE_OUT,$bind(this,this.onMouseOut));
		this.follow(false);
		this._tf.set_filters([]);
		this.set_filters([]);
		this._contentContainer.removeChild(this._tf);
		this._tf = null;
		if(this._cf != null) {
			this._cf.set_filters([]);
			this._contentContainer.removeChild(this._cf);
		}
		this.boxSp.get_graphics().clear();
		this.removeChild(this._contentContainer);
		this.parent.removeChild(this);
		if(this._hookEnabled && this.hookSp != null) {
			this.hookSp.get_graphics().clear();
			this.removeChild(this.hookSp);
		}
	}
	,__class__: unbox.framework.com.hybrid.ui.ToolTip
	,__properties__: $extend(openfl.display.Sprite.prototype.__properties__,{set_padding:"set_padding",get_padding:"get_padding",set_followCursor:"set_followCursor",get_followCursor:"get_followCursor",set_contentEmbed:"set_contentEmbed",set_titleEmbed:"set_titleEmbed",set_tipHeight:"set_tipHeight",set_borderSize:"set_borderSize",set_border:"set_border",set_autoSize:"set_autoSize",set_colors:"set_colors",set_cornerRadius:"set_cornerRadius",set_hookSize:"set_hookSize",set_hook:"set_hook",set_hideDelay:"set_hideDelay",set_showDelay:"set_showDelay",set_align:"set_align",set_contentFormat:"set_contentFormat",set_titleFormat:"set_titleFormat",set_tipWidth:"set_tipWidth",set_bgAlpha:"set_bgAlpha",get_bgAlpha:"get_bgAlpha",set_buffer:"set_buffer",get_buffer:"get_buffer"})
});
unbox.framework.com.pipwerks = {};
unbox.framework.com.pipwerks.SCORM = function() {
	this._debugMode = true;
	this.__connectionActive = false;
	var is_EI_available = openfl.external.ExternalInterface.available;
	var wrapperFound = false;
	var debugMsg = "Initializing SCORM class. Checking dependencies: ";
	if(is_EI_available) {
		debugMsg += "ExternalInterface.available evaluates true. ";
		wrapperFound = js.Boot.__cast(openfl.external.ExternalInterface.call("pipwerks.SCORM.isAvailable") , Bool);
		debugMsg += "SCORM.isAvailable() evaluates " + ("" + wrapperFound) + ". ";
		if(wrapperFound) debugMsg += "SCORM class file ready to go!  :) "; else debugMsg += "The required JavaScript SCORM API wrapper cannot be found in the HTML document.  Course cannot load.";
	} else debugMsg += "ExternalInterface is NOT available (this may be due to an outdated version of Flash Player).  Course cannot load.";
	this.__displayDebugInfo(debugMsg);
};
$hxClasses["unbox.framework.com.pipwerks.SCORM"] = unbox.framework.com.pipwerks.SCORM;
unbox.framework.com.pipwerks.SCORM.__name__ = ["unbox","framework","com","pipwerks","SCORM"];
unbox.framework.com.pipwerks.SCORM.prototype = {
	set_debugMode: function(status) {
		this._debugMode = status;
		return status;
	}
	,get_debugMode: function() {
		return this._debugMode;
	}
	,connect: function() {
		this.__displayDebugInfo("pipwerks.SCORM.connect() called from class file");
		return this.__connect();
	}
	,disconnect: function() {
		return this.__disconnect();
	}
	,get: function(param) {
		var str = this.__get(param);
		this.__displayDebugInfo("public function get returned: " + str);
		return str;
	}
	,set: function(parameter,value) {
		return this.__set(parameter,value);
	}
	,save: function() {
		return this.__save();
	}
	,__connect: function() {
		var result = false;
		if(!this.__connectionActive) {
			var eiCall = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.init"));
			if(eiCall == "true") result = true; else result = false;
			if(result) this.__connectionActive = true; else {
				var errorCode = this.__getDebugCode();
				if(errorCode != 0) {
					var debugInfo = this.__getDebugInfo(errorCode);
					this.__displayDebugInfo("pipwerks.SCORM.init() failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
				} else this.__displayDebugInfo("pipwerks.SCORM.init failed: no response from server.");
			}
		} else this.__displayDebugInfo("pipwerks.SCORM.init aborted: connection already active.");
		this.__displayDebugInfo("__connectionActive: " + ("" + this.__connectionActive));
		return result;
	}
	,__disconnect: function() {
		var result = false;
		if(this.__connectionActive) {
			var eiCall = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.quit"));
			if(eiCall == "true") result = true; else result = false;
			if(result) this.__connectionActive = false; else {
				var errorCode = this.__getDebugCode();
				var debugInfo = this.__getDebugInfo(errorCode);
				this.__displayDebugInfo("pipwerks.SCORM.quit() failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
			}
		} else this.__displayDebugInfo("pipwerks.SCORM.quit aborted: connection already inactive.");
		return result;
	}
	,__get: function(parameter) {
		var returnedValue = "";
		if(this.__connectionActive) {
			returnedValue = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.get",parameter));
			var errorCode = this.__getDebugCode();
			if(returnedValue == "" && errorCode != 0) {
				var debugInfo = this.__getDebugInfo(errorCode);
				this.__displayDebugInfo("pipwerks.SCORM.get(" + parameter + ") failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
			}
		} else this.__displayDebugInfo("pipwerks.SCORM.get(" + parameter + ") failed: connection is inactive.");
		return returnedValue;
	}
	,__set: function(parameter,value) {
		var result = false;
		if(this.__connectionActive) {
			var eiCall = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.set",parameter,value));
			if(eiCall == "true") result = true; else result = false;
			if(!result) {
				var errorCode = this.__getDebugCode();
				var debugInfo = this.__getDebugInfo(errorCode);
				this.__displayDebugInfo("pipwerks.SCORM.set(" + parameter + ") failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
			}
		} else this.__displayDebugInfo("pipwerks.SCORM.set(" + parameter + ") failed: connection is inactive.");
		return result;
	}
	,__save: function() {
		var result = false;
		if(this.__connectionActive) {
			var eiCall = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.save"));
			if(eiCall == "true") result = true; else result = false;
			if(!result) {
				var errorCode = this.__getDebugCode();
				var debugInfo = this.__getDebugInfo(errorCode);
				this.__displayDebugInfo("pipwerks.SCORM.save() failed. \n" + "Error code: " + errorCode + "\n" + "Error info: " + debugInfo);
			}
		} else this.__displayDebugInfo("pipwerks.SCORM.save() failed: API connection is inactive.");
		return result;
	}
	,__getDebugCode: function() {
		var code = Std.parseInt(openfl.external.ExternalInterface.call("pipwerks.SCORM.debug.getCode"));
		return code;
	}
	,__getDebugInfo: function(errorCode) {
		var result = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.debug.getInfo",errorCode));
		return result;
	}
	,__getDiagnosticInfo: function(errorCode) {
		var result = Std.string(openfl.external.ExternalInterface.call("pipwerks.SCORM.debug.getDiagnosticInfo",errorCode));
		return result;
	}
	,__displayDebugInfo: function(msg) {
		if(this._debugMode) openfl.external.ExternalInterface.call("pipwerks.UTILS.trace",msg);
	}
	,__class__: unbox.framework.com.pipwerks.SCORM
	,__properties__: {set_debugMode:"set_debugMode",get_debugMode:"get_debugMode"}
};
unbox.framework.components = {};
unbox.framework.components.Assessment = function(parent,data) {
	this.parent = parent;
	this.data = data;
	this._onExerciseChanged = new msignal.Signal1();
	this._onComplete = new msignal.Signal1();
};
$hxClasses["unbox.framework.components.Assessment"] = unbox.framework.components.Assessment;
unbox.framework.components.Assessment.__name__ = ["unbox","framework","components","Assessment"];
unbox.framework.components.Assessment.prototype = {
	init: function() {
		this.currentExerciseId = 0;
		this.userScore = 0;
		this.totalExercises = this.data.node.resolve("exercises").nodes.resolve("content").length;
		this.maxTries = Std.parseInt(this.data.node.resolve("config").att.resolve("maxTries"));
		this.minScore = Std.parseInt(this.data.node.resolve("config").att.resolve("minScore"));
		this.scorePerExercise = Std.parseInt(this.data.node.resolve("config").att.resolve("scorePerExercise"));
		this.numberOfQuestions = Std.parseInt(this.data.node.resolve("config").att.resolve("numberOfQuestions"));
		haxe.Log.trace("numberOfQuestions : " + this.numberOfQuestions,{ fileName : "Assessment.hx", lineNumber : 64, className : "unbox.framework.components.Assessment", methodName : "init"});
		if(this.data.node.resolve("config").att.resolve("randomizeExercises") == "true") this.randomizeExercises = true; else this.randomizeExercises = false;
		if(this.data.node.resolve("config").att.resolve("keepHigherScore") == "true") this.keepHigherScore = true; else this.keepHigherScore = false;
		this.exerciseSequence = new Array();
		var _g1 = 0;
		var _g = this.totalExercises;
		while(_g1 < _g) {
			var i = _g1++;
			this.exerciseSequence.push(i);
		}
		var randomSequence = unbox.framework.utils.ArrayUtils.sortNonRepeatSeq(0,this.exerciseSequence.length,this.exerciseSequence.length + 1);
		if(this.randomizeExercises == true) this.exerciseSequence = randomSequence; else false;
		this.parent.context.dataController.get_ebookData().set_scoreMax(100);
		this.parent.context.dataController.get_status().set_currentPage(this.parent.data.get_globalIndex() - 1);
		this.parent.context.dataController.save();
		this.loadExercise();
	}
	,loadExercise: function() {
		var exerciseData = null;
		var count = 0;
		var $it0 = this.data.node.resolve("exercises").nodes.resolve("content").iterator();
		while( $it0.hasNext() ) {
			var data = $it0.next();
			if(count == this.exerciseSequence[this.currentExerciseId]) exerciseData = data;
			count++;
		}
		var exerciseType = exerciseData.att.resolve("type");
		if(this.currentExercise != null) {
			this.parent.view.removeChild(this.currentExercise.get_view());
			this.currentExercise = null;
		}
		var classRef = Type.resolveClass(exerciseType);
		var exercise = Type.createInstance(classRef,[this.parent,exerciseData]);
		exercise.get_onComplete().add($bind(this,this.onExerciseCompleted));
		exercise.set_exerciseId(this.currentExerciseId + 1);
		exercise.init();
		exercise.get_view().set_alpha(0);
		exercise.get_view().set_name("exercise_" + this.currentExerciseId);
		motion.Actuate.tween(exercise.get_view(),0.5,{ alpha : 1});
		this.currentExercise = exercise;
		this.parent.context.navController.setNavButtons("000");
		this.parent.context.navController.onBeforeNextPage = null;
		this.get_onExerciseChanged().dispatch(this.currentExerciseId + 1);
	}
	,onExerciseCompleted: function(points) {
		haxe.Log.trace("Assessment.onExerciseCompleted > points : " + points,{ fileName : "Assessment.hx", lineNumber : 135, className : "unbox.framework.components.Assessment", methodName : "onExerciseCompleted"});
		if(points < this.currentExercise.get_maxPoints()) points = 0; else points = 1;
		this.userScore += points;
		if(this.currentExerciseId < this.numberOfQuestions - 1) {
			this.currentExerciseId++;
			haxe.Log.trace("Next exercise ID : " + this.currentExerciseId,{ fileName : "Assessment.hx", lineNumber : 143, className : "unbox.framework.components.Assessment", methodName : "onExerciseCompleted"});
			this.parent.context.navController.onBeforeNextPage = $bind(this,this.disposeOfCurrentExercise);
		} else {
			haxe.Log.trace("This was the last question!",{ fileName : "Assessment.hx", lineNumber : 149, className : "unbox.framework.components.Assessment", methodName : "onExerciseCompleted"});
			this.parent.context.navController.onBeforeNextPage = null;
			var scoreMultiplier = 100 / this.numberOfQuestions | 0;
			var newScore;
			if(this.userScore == this.numberOfQuestions) newScore = 100; else newScore = this.userScore * scoreMultiplier;
			if(this.keepHigherScore) this.parent.context.dataController.get_status().set_quizScore(this.parent.context.dataController.get_status().get_quizScore() > newScore?this.parent.context.dataController.get_status().get_quizScore():newScore); else this.parent.context.dataController.get_status().set_quizScore(newScore);
			var _g = this.parent.context.dataController.get_status();
			var _g1 = _g.get_quizTries();
			_g.set_quizTries(_g1 + 1);
			_g1;
			this.parent.context.dataController.get_status().set_currentPage(this.parent.data.get_globalIndex());
		}
		this.parent.context.navController.setNavButtons("001");
	}
	,disposeOfCurrentExercise: function() {
		motion.Actuate.tween(this.currentExercise.get_view(),0.5,{ alpha : 0}).onComplete($bind(this,this.loadExercise));
	}
	,get_onExerciseChanged: function() {
		return this._onExerciseChanged;
	}
	,set_onExerciseChanged: function(value) {
		return this._onExerciseChanged = value;
	}
	,get_onComplete: function() {
		return this._onComplete;
	}
	,set_onComplete: function(value) {
		return this._onComplete = value;
	}
	,__class__: unbox.framework.components.Assessment
	,__properties__: {set_onComplete:"set_onComplete",get_onComplete:"get_onComplete",set_onExerciseChanged:"set_onExerciseChanged",get_onExerciseChanged:"get_onExerciseChanged"}
};
unbox.framework.components.IExercise = function() { };
$hxClasses["unbox.framework.components.IExercise"] = unbox.framework.components.IExercise;
unbox.framework.components.IExercise.__name__ = ["unbox","framework","components","IExercise"];
unbox.framework.components.IExercise.prototype = {
	__class__: unbox.framework.components.IExercise
};
unbox.framework.components.Quiz = function(parent,data) {
	this.data = data;
	this.parent = parent;
	haxe.Log.trace("view : " + data.node.resolve("config").node.resolve("view").att.resolve("classDef"),{ fileName : "Quiz.hx", lineNumber : 58, className : "unbox.framework.components.Quiz", methodName : "new"});
	this._view = new openfl.display.Sprite();
	this._onComplete = new msignal.Signal1();
	this._maxPoints = 1;
};
$hxClasses["unbox.framework.components.Quiz"] = unbox.framework.components.Quiz;
unbox.framework.components.Quiz.__name__ = ["unbox","framework","components","Quiz"];
unbox.framework.components.Quiz.__interfaces__ = [unbox.framework.components.IExercise];
unbox.framework.components.Quiz.prototype = {
	init: function() {
		this._view.set_x(Std.parseFloat(this.data.node.resolve("config").node.resolve("view").att.resolve("x")));
		this._view.set_y(Std.parseFloat(this.data.node.resolve("config").node.resolve("view").att.resolve("y")));
		this.exId = this._exerciseId;
		this.questionTxt = js.Boot.__cast(this._view.getChildByName("question_txt") , openfl.text.TextField);
		this.questionTxt.embedFonts = true;
		this.questionTxt.antiAliasType = openfl.text.AntiAliasType.ADVANCED;
		this.questionTxt.gridFitType = openfl.text.GridFitType.SUBPIXEL;
		var strippedText = this.data.node.resolve("question").node.resolve("content").get_innerHTML();
		strippedText = StringTools.replace(strippedText,"<![CDATA[","");
		strippedText = StringTools.replace(strippedText,"]]>","");
		strippedText = StringTools.trim(strippedText);
		strippedText = unbox.framework.utils.StringUtils.parseTextVars(strippedText,this);
		this.questionTxt.set_htmlText(strippedText);
		this.correctAnswer = Std.parseInt(this.data.node.resolve("config").node.resolve("alternatives").att.resolve("correct"));
		haxe.Log.trace("correctAnswer : " + this.correctAnswer,{ fileName : "Quiz.hx", lineNumber : 109, className : "unbox.framework.components.Quiz", methodName : "init"});
		var count = 0;
		this.options = new Array();
		this.tickers = new Array();
		this.selectedOption = -1;
		var $it0 = this.data.node.resolve("options").nodes.resolve("button").iterator();
		while( $it0.hasNext() ) {
			var option = $it0.next();
			var btnView;
			btnView = js.Boot.__cast(this._view.getChildByName("option_" + count) , openfl.display.Sprite);
			var button = new unbox.framework.view.button.StyleButton(btnView.get_x(),btnView.get_y(),btnView,"option_" + count);
			this._view.removeChild(btnView);
			button.setClasses("bOver","bDown","bUp","bSelected");
			button.set_label(unbox.framework.utils.ContentParser.parseTextField(option,button.get_view()));
			button.add(this._view);
			button.addHandlers($bind(this,this.handleOptionDown));
			this.options.push(button);
			var ticker = new openfl.display.Sprite();
			ticker.set_name("ticker");
			ticker.set_x(btnView.get_x() - 30);
			ticker.set_y(btnView.get_y());
			this._view.addChild(ticker);
			this.tickers.push(ticker);
			ticker.mouseEnabled = ticker.mouseChildren = false;
			count++;
		}
		var submitView;
		submitView = js.Boot.__cast(this._view.getChildByName("btCheck") , openfl.display.Sprite);
		this.submitBtn = new unbox.framework.view.button.GenericButton(submitView.get_x(),submitView.get_y(),submitView,"submitBtn");
		this._view.removeChild(submitView);
		this.submitBtn.set_label(unbox.framework.utils.ContentParser.parseTextField(this.data.node.resolve("submitButton"),this.submitBtn.get_view()));
		this.submitBtn.add(this._view);
		this.submitBtn.addHandlers($bind(this,this.handleSubmitDown));
		this.submitBtn.setEnabled(false);
		this._view.tabChildren = true;
		this.parent.view.addChild(this._view);
	}
	,handleOptionDown: function(e) {
		if(this.selectedOption == Std.parseInt(e.name.split("_")[1])) {
			this.options[this.selectedOption].setSelected(false);
			this.selectedOption = -1;
		} else {
			if(this.selectedOption > -1) this.options[this.selectedOption].setSelected(false);
			this.selectedOption = Std.parseInt(e.name.split("_")[1]);
			this.options[this.selectedOption].setSelected(true);
		}
		if(this.selectedOption == -1) this.submitBtn.setEnabled(false); else this.submitBtn.setEnabled(true);
	}
	,handleSubmitDown: function(e) {
		haxe.Log.trace("Submit button down! Selected opt : " + this.selectedOption + " | Correct opt : " + this.correctAnswer,{ fileName : "Quiz.hx", lineNumber : 195, className : "unbox.framework.components.Quiz", methodName : "handleSubmitDown"});
		var isCorrect = this.selectedOption == this.correctAnswer;
		var points = 0;
		if(isCorrect) {
			this.tickers[this.selectedOption].addChild(this.tRight);
			points++;
			haxe.Log.trace("Correct answer!",{ fileName : "Quiz.hx", lineNumber : 203, className : "unbox.framework.components.Quiz", methodName : "handleSubmitDown"});
		} else {
			this.tickers[this.selectedOption].addChild(this.tWrong);
			this.tickers[this.correctAnswer].addChild(this.tAnswer);
			haxe.Log.trace("Incorrect answer!",{ fileName : "Quiz.hx", lineNumber : 211, className : "unbox.framework.components.Quiz", methodName : "handleSubmitDown"});
		}
		var _g = 0;
		var _g1 = this.options;
		while(_g < _g1.length) {
			var btn = _g1[_g];
			++_g;
			btn.setEnabled(false);
		}
		this.submitBtn.setEnabled(false);
		this._onComplete.dispatch(points);
	}
	,get_view: function() {
		return this._view;
	}
	,set_view: function(value) {
		return this._view = value;
	}
	,get_onComplete: function() {
		return this._onComplete;
	}
	,set_onComplete: function(value) {
		return this._onComplete = value;
	}
	,get_maxPoints: function() {
		return this._maxPoints;
	}
	,set_maxPoints: function(value) {
		return this._maxPoints = value;
	}
	,get_exerciseId: function() {
		return this._exerciseId;
	}
	,set_exerciseId: function(value) {
		return this._exerciseId = value;
	}
	,__class__: unbox.framework.components.Quiz
	,__properties__: {set_exerciseId:"set_exerciseId",get_exerciseId:"get_exerciseId",set_maxPoints:"set_maxPoints",get_maxPoints:"get_maxPoints",set_onComplete:"set_onComplete",get_onComplete:"get_onComplete",set_view:"set_view",get_view:"get_view"}
};
unbox.framework.controller = {};
unbox.framework.controller.DataController = function() {
	this._isConsultMode = false;
	this._enableAlerts = false;
	this._dataServiceType = "SharedObject";
	this.isDataServiceAvaiable = false;
	this.isExtIntAvaiable = openfl.external.ExternalInterface.available;
};
$hxClasses["unbox.framework.controller.DataController"] = unbox.framework.controller.DataController;
unbox.framework.controller.DataController.__name__ = ["unbox","framework","controller","DataController"];
unbox.framework.controller.DataController.arrayCompare = function(a,b) {
	if(a > b) return 1; else if(a < b) return -1; else return 0;
};
unbox.framework.controller.DataController.prototype = {
	start: function(data,maxPageId,maxModuleId) {
		haxe.Log.trace("DataController.start",{ fileName : "DataController.hx", lineNumber : 60, className : "unbox.framework.controller.DataController", methodName : "start"});
		this._ebookData = new unbox.framework.model.vo.EbookData();
		this._status = new unbox.framework.model.Status();
		this._status.set_maxPage(maxPageId);
		this._status.set_maxModule(maxModuleId);
		if(data.att.resolve("consultMode") == "true") this._isConsultMode = true; else this._isConsultMode = false;
		this._dataServiceType = data.att.resolve("dataServiceType");
		if(data.att.resolve("enableDebugPanel") == "true") this._enableDebugPanel = true; else this._enableDebugPanel = false;
		this._status.get_lessonStatus().set_maxPoints(unbox.framework.utils.ArrayUtils.toNumber(data.node.resolve("customData").node.resolve("maxPoints").get_innerHTML().split(",")));
		this._status.get_lessonStatus().set_lessonStatus(unbox.framework.utils.ArrayUtils.toNumber(data.node.resolve("customData").node.resolve("lessonStatus").get_innerHTML().split(",")));
		this._status.get_lessonStatus().set_userPoints(unbox.framework.utils.ArrayUtils.fillArray(this.get_status().get_lessonStatus().get_maxPoints().length,-1));
		this.onBookmarkUpdated = new msignal.Signal2();
		this.onPageBookmarkChecked = new msignal.Signal1();
		this.populateBookmarks = new msignal.Signal1();
		this.sessionTimer = new unbox.framework.model.SessionTimer();
		if(this._isConsultMode) this.startBrowseMode(); else this.initDataService();
	}
	,startBrowseMode: function() {
		haxe.Log.trace("DataController.startBrowseMode",{ fileName : "DataController.hx", lineNumber : 101, className : "unbox.framework.controller.DataController", methodName : "startBrowseMode"});
		haxe.Log.trace("*** RUNNING BROWSE MODE ***",{ fileName : "DataController.hx", lineNumber : 102, className : "unbox.framework.controller.DataController", methodName : "startBrowseMode"});
		if(this.isExtIntAvaiable && this._enableAlerts) openfl.external.ExternalInterface.call("alert","Iniciando o treinamento em modo CONSULTA.\n\nSeus dados não serão gravados.");
		this._isConsultMode = true;
		this._status.set_currentModule(0);
		this._status.set_currentPage(0);
	}
	,initDataService: function() {
		haxe.Log.trace("DataController.initDataService",{ fileName : "DataController.hx", lineNumber : 114, className : "unbox.framework.controller.DataController", methodName : "initDataService"});
		if(this._dataServiceType == "SCORM" && this.isExtIntAvaiable) this.dataService = new unbox.framework.services.ScormDataService(); else {
			haxe.Log.trace("DataController.initDataService >> DataService SCORM not available! Using SharedObject instead.",{ fileName : "DataController.hx", lineNumber : 119, className : "unbox.framework.controller.DataController", methodName : "initDataService"});
			this._dataServiceType = "SharedObject";
			this.dataService = new unbox.framework.services.SolDataService();
		}
		if(this.isExtIntAvaiable) openfl.external.ExternalInterface.addCallback("jsCall",$bind(this,this.jsCall));
		this.dataService.get_onLoaded().add($bind(this,this.onDataLoaded));
		this.dataService.get_onSaved().add($bind(this,this.onDataSaved));
		this.dataService.get_onLoadError().add($bind(this,this.onDataLoadError));
		this.dataService.get_onSaveError().add($bind(this,this.onDataSaveError));
		this.dataService.load();
	}
	,onDataLoaded: function(data) {
		haxe.Log.trace("DataController.onDataLoaded",{ fileName : "DataController.hx", lineNumber : 136, className : "unbox.framework.controller.DataController", methodName : "onDataLoaded"});
		this.isDataServiceAvaiable = true;
		this._ebookData = data;
		var _g = 0;
		var _g1 = Reflect.fields(this._ebookData);
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			haxe.Log.trace("\tLOAD SCORM DATA >> " + i + ", value : " + Std.string(Reflect.field(this._ebookData,i)),{ fileName : "DataController.hx", lineNumber : 140, className : "unbox.framework.controller.DataController", methodName : "onDataLoaded"});
		}
		if(this._ebookData.get_lesson_mode() == "browse") this.startBrowseMode(); else if(this._ebookData.get_suspend_data() == null || this._ebookData.get_suspend_data() == "" || this._ebookData.get_lesson_status() == "not attempted") {
			haxe.Log.trace("DataController.initDataService >> SUSPEND_DATA null OR empty. New user!",{ fileName : "DataController.hx", lineNumber : 149, className : "unbox.framework.controller.DataController", methodName : "onDataLoaded"});
			this._ebookData.set_lesson_status("incomplete");
			this._status.set_status(1);
			this.sessionTimer.initSession();
			this.save();
		} else {
			haxe.Log.trace("DataController.initDataService >> SUSPEND_DATA read.",{ fileName : "DataController.hx", lineNumber : 156, className : "unbox.framework.controller.DataController", methodName : "onDataLoaded"});
			this._status.parseData(this._ebookData.get_suspend_data());
			haxe.Log.trace(this._status.get_lessonStatus().toString(),{ fileName : "DataController.hx", lineNumber : 159, className : "unbox.framework.controller.DataController", methodName : "onDataLoaded"});
			this.sessionTimer.initSession();
		}
	}
	,onDataSaved: function() {
		haxe.Log.trace("DataController.onDataSaved >> Saved successfully!",{ fileName : "DataController.hx", lineNumber : 167, className : "unbox.framework.controller.DataController", methodName : "onDataSaved"});
	}
	,onDataLoadError: function(msg) {
		haxe.Log.trace("DataController.onDataLoadError >> " + msg,{ fileName : "DataController.hx", lineNumber : 172, className : "unbox.framework.controller.DataController", methodName : "onDataLoadError"});
		if(this.isExtIntAvaiable && this._enableAlerts) openfl.external.ExternalInterface.call("alert","Erro ao carregar dados do DATASERVICE (" + this._dataServiceType + ").\n\nContate o administrador do sistema.\n\n" + msg);
		this.isDataServiceAvaiable = false;
		this.startBrowseMode();
	}
	,onDataSaveError: function(msg) {
		haxe.Log.trace("DataController.onDataSaveError >> " + msg,{ fileName : "DataController.hx", lineNumber : 182, className : "unbox.framework.controller.DataController", methodName : "onDataSaveError"});
		if(this.isExtIntAvaiable && this._enableAlerts) openfl.external.ExternalInterface.call("alert","Erro ao salvar dados no DATASERVICE (" + this._dataServiceType + ").\n\nContate o administrador do sistema.\n\n" + msg);
	}
	,save: function(page) {
		if(page != null) this.get_status().set_currentPage(page.data.get_globalIndex());
		haxe.Log.trace("DataController.save",{ fileName : "DataController.hx", lineNumber : 195, className : "unbox.framework.controller.DataController", methodName : "save"});
		if(this.isDataServiceAvaiable && !this._isConsultMode && this._ebookData.get_lesson_status() == "incomplete") {
			if(this._status.get_status() == 2) this._ebookData.set_lesson_status("completed");
			this._ebookData.set_suspend_data(this._status.toString());
			this._ebookData.set_session_time(this.sessionTimer.getCMISessionTime());
			var _g = 0;
			var _g1 = Reflect.fields(this._ebookData);
			while(_g < _g1.length) {
				var i = _g1[_g];
				++_g;
				haxe.Log.trace("\tSAVE EBOOK DATA >> " + i + ", value : " + Std.string(Reflect.field(this._ebookData,i)),{ fileName : "DataController.hx", lineNumber : 207, className : "unbox.framework.controller.DataController", methodName : "save"});
			}
			this.dataService.save(this._ebookData);
		} else haxe.Log.trace("DataController.save >> Not allowed to save!",{ fileName : "DataController.hx", lineNumber : 212, className : "unbox.framework.controller.DataController", methodName : "save"});
	}
	,finishEbook: function() {
		haxe.Log.trace("DataController.finishEbook",{ fileName : "DataController.hx", lineNumber : 218, className : "unbox.framework.controller.DataController", methodName : "finishEbook"});
		if(!this._isConsultMode && this._status.get_status() == 1) {
			this._status.set_endDate(new Date());
			this._status.set_status(2);
			this._ebookData.set_exit("logout");
			this.save();
		} else haxe.Log.trace("DataController.finishEbook >> Course STATUS_COMPLETED, not allowed to save any more data!",{ fileName : "DataController.hx", lineNumber : 226, className : "unbox.framework.controller.DataController", methodName : "finishEbook"});
	}
	,closeBrowser: function() {
		haxe.Log.trace("DataController.closeBrowser",{ fileName : "DataController.hx", lineNumber : 232, className : "unbox.framework.controller.DataController", methodName : "closeBrowser"});
		if(this.isExtIntAvaiable) {
			openfl.external.ExternalInterface.call("scorm.save");
			openfl.external.ExternalInterface.call("API_Extended.SetNavCommand('exit')");
			openfl.external.ExternalInterface.call("scorm.quit");
			openfl.external.ExternalInterface.call("exit");
		} else haxe.Log.trace("DataController.closeBrowser >> ExternalInterface not available!",{ fileName : "DataController.hx", lineNumber : 241, className : "unbox.framework.controller.DataController", methodName : "closeBrowser"});
	}
	,jsCall: function(functionName,params) {
		haxe.Log.trace("DataController.jsCall > functionName : " + functionName + ", params : " + params,{ fileName : "DataController.hx", lineNumber : 247, className : "unbox.framework.controller.DataController", methodName : "jsCall"});
		switch(functionName) {
		case "save":
			this.save();
			break;
		case "exit":
			this.closeBrowser();
			break;
		default:
		}
	}
	,addBookmark: function(data) {
		var pageFound = unbox.framework.utils.ArrayUtils.binarySearch(this.get_status().get_lessonStatus().get_bookmarks(),data.get_globalIndex());
		var added = false;
		if(pageFound == -1) {
			this.get_status().get_lessonStatus().get_bookmarks().push(data.get_globalIndex());
			this.get_status().get_lessonStatus().get_bookmarks().sort(unbox.framework.controller.DataController.arrayCompare);
			added = true;
			haxe.Log.trace("Bookmark '" + data.get_globalIndex() + "' Added : " + Std.string(this.get_status().get_lessonStatus().get_bookmarks()),{ fileName : "DataController.hx", lineNumber : 276, className : "unbox.framework.controller.DataController", methodName : "addBookmark"});
		} else {
			this.get_status().get_lessonStatus().get_bookmarks().splice(pageFound,1);
			haxe.Log.trace("Bookmark '" + data.get_globalIndex() + "' Removed : " + Std.string(this.get_status().get_lessonStatus().get_bookmarks()),{ fileName : "DataController.hx", lineNumber : 279, className : "unbox.framework.controller.DataController", methodName : "addBookmark"});
			added = false;
		}
		this.save();
		this.onBookmarkUpdated.dispatch(added,data);
	}
	,checkBookmark: function(page) {
		haxe.Log.trace("DataController.checkBookmark > page : " + Std.string(page),{ fileName : "DataController.hx", lineNumber : 295, className : "unbox.framework.controller.DataController", methodName : "checkBookmark"});
		var pageFound = unbox.framework.utils.ArrayUtils.binarySearch(this.get_status().get_lessonStatus().get_bookmarks(),page.data.get_globalIndex());
		var pageHasBookmark;
		if(pageFound > -1) pageHasBookmark = true; else pageHasBookmark = false;
		haxe.Log.trace("pageHasBookmark : " + ("" + pageHasBookmark),{ fileName : "DataController.hx", lineNumber : 299, className : "unbox.framework.controller.DataController", methodName : "checkBookmark"});
		this.onPageBookmarkChecked.dispatch(pageHasBookmark);
	}
	,loadBookmarks: function(pages) {
		var bookmarks = new Array();
		var numPages = pages.length;
		var _g = 0;
		while(_g < numPages) {
			var i = _g++;
			var numBookmarks = this.get_status().get_lessonStatus().get_bookmarks().length;
			var _g1 = 0;
			while(_g1 < numBookmarks) {
				var j = _g1++;
				if(this.get_status().get_lessonStatus().get_bookmarks()[j] == pages[i].get_globalIndex()) bookmarks.push(pages[i]);
			}
		}
		if(bookmarks.length > 0) this.populateBookmarks.dispatch(bookmarks);
	}
	,get_isConsultMode: function() {
		return this._isConsultMode;
	}
	,set_isConsultMode: function(value) {
		this._isConsultMode = value;
		return value;
	}
	,get_ebookData: function() {
		return this._ebookData;
	}
	,set_ebookData: function(value) {
		this._ebookData = value;
		return value;
	}
	,get_dataServiceType: function() {
		return this._dataServiceType;
	}
	,set_dataServiceType: function(value) {
		this._dataServiceType = value;
		return value;
	}
	,get_enableAlerts: function() {
		return this._enableAlerts;
	}
	,set_enableAlerts: function(value) {
		this._enableAlerts = value;
		return value;
	}
	,get_hasAccessibility: function() {
		return this._hasAccessibility;
	}
	,set_hasAccessibility: function(value) {
		this._hasAccessibility = value;
		return value;
	}
	,get_enableDebugPanel: function() {
		return this._enableDebugPanel;
	}
	,set_enableDebugPanel: function(value) {
		this._enableDebugPanel = value;
		return value;
	}
	,get_status: function() {
		return this._status;
	}
	,set_status: function(value) {
		return this._status = value;
	}
	,__class__: unbox.framework.controller.DataController
	,__properties__: {set_status:"set_status",get_status:"get_status",set_enableDebugPanel:"set_enableDebugPanel",get_enableDebugPanel:"get_enableDebugPanel",set_hasAccessibility:"set_hasAccessibility",get_hasAccessibility:"get_hasAccessibility",set_enableAlerts:"set_enableAlerts",get_enableAlerts:"get_enableAlerts",set_dataServiceType:"set_dataServiceType",get_dataServiceType:"get_dataServiceType",set_ebookData:"set_ebookData",get_ebookData:"get_ebookData",set_isConsultMode:"set_isConsultMode",get_isConsultMode:"get_isConsultMode"}
};
unbox.framework.controller.NavController = function(pages,navLayer) {
	this.pages = pages;
	haxe.Log.trace("Page length : " + pages.length,{ fileName : "NavController.hx", lineNumber : 33, className : "unbox.framework.controller.NavController", methodName : "new"});
	this.navLayer = navLayer;
	this.currentPageLocalID = 0;
	this._currentPageGlobalID = 0;
	this.currentModuleID = 0;
	this._buttons = new Array();
	this._onChange = new msignal.Signal2();
	this._onChangeNavStatus = new msignal.Signal1();
};
$hxClasses["unbox.framework.controller.NavController"] = unbox.framework.controller.NavController;
unbox.framework.controller.NavController.__name__ = ["unbox","framework","controller","NavController"];
unbox.framework.controller.NavController.prototype = {
	handleNavInput: function(id) {
		haxe.Log.trace("NavController.handleNavInput > id : " + id,{ fileName : "NavController.hx", lineNumber : 45, className : "unbox.framework.controller.NavController", methodName : "handleNavInput"});
		switch(id) {
		case -2:
			haxe.Log.trace("Button functionality not implemented!",{ fileName : "NavController.hx", lineNumber : 49, className : "unbox.framework.controller.NavController", methodName : "handleNavInput"});
			break;
		case -1:
			this.gotoPreviousPage();
			break;
		case 0:
			this.gotoMenu();
			break;
		case 1:
			this.gotoNextPage();
			break;
		default:
			if(id == this._currentPageGlobalID) return; else this.gotoPage(id);
		}
	}
	,gotoNextPage: function() {
		haxe.Log.trace("NavController.gotoNextPage",{ fileName : "NavController.hx", lineNumber : 63, className : "unbox.framework.controller.NavController", methodName : "gotoNextPage"});
		if(this.onBeforeNextPage != null) this.onBeforeNextPage(); else if(this._currentPageGlobalID < this.pages.length - 1) {
			this._lastPage = this._currentPageGlobalID;
			this._currentPageGlobalID++;
			this._onChange.dispatch(this._currentPageGlobalID,1);
		}
	}
	,gotoPreviousPage: function() {
		if(this.onBeforePreviousPage != null) this.onBeforePreviousPage(); else if(this._currentPageGlobalID > 0) {
			this._lastPage = this._currentPageGlobalID;
			this._currentPageGlobalID--;
			this._onChange.dispatch(this._currentPageGlobalID,-1);
		}
	}
	,gotoPage: function(id) {
		if(id > 0 && id <= this.pages.length - 1) {
			this._lastPage = this._currentPageGlobalID;
			this._currentPageGlobalID = id;
			var direction;
			if(this._currentPageGlobalID < this._lastPage) direction = -1; else direction = 1;
			this._onChange.dispatch(this._currentPageGlobalID,direction);
		} else haxe.Log.trace("Page id '" + id + "'does not exist!",{ fileName : "NavController.hx", lineNumber : 102, className : "unbox.framework.controller.NavController", methodName : "gotoPage"});
	}
	,gotoMenu: function() {
		haxe.Log.trace("NavController.gotoMenu",{ fileName : "NavController.hx", lineNumber : 109, className : "unbox.framework.controller.NavController", methodName : "gotoMenu"});
		if(this._currentPageGlobalID != 1) {
			this._lastPage = this._currentPageGlobalID;
			this._currentPageGlobalID = 1;
			this._onChange.dispatch(this._currentPageGlobalID,-1);
		}
	}
	,setNavButtons: function(value) {
		this._onChangeNavStatus.dispatch(value);
	}
	,onPageDataRequest: function(response,keyword) {
		haxe.Log.trace("SearchRequest received. Sending response...",{ fileName : "NavController.hx", lineNumber : 127, className : "unbox.framework.controller.NavController", methodName : "onPageDataRequest"});
		response.dispatch(this.pages,keyword);
	}
	,get_onChange: function() {
		return this._onChange;
	}
	,get_currentPageGlobalID: function() {
		return this._currentPageGlobalID;
	}
	,set_currentPageGlobalID: function(value) {
		return this._currentPageGlobalID = value;
	}
	,get_lastPage: function() {
		return this._lastPage;
	}
	,get_onChangeNavStatus: function() {
		return this._onChangeNavStatus;
	}
	,set_onChangeNavStatus: function(value) {
		return this._onChangeNavStatus = value;
	}
	,__class__: unbox.framework.controller.NavController
	,__properties__: {set_onChangeNavStatus:"set_onChangeNavStatus",get_onChangeNavStatus:"get_onChangeNavStatus",get_lastPage:"get_lastPage",set_currentPageGlobalID:"set_currentPageGlobalID",get_currentPageGlobalID:"get_currentPageGlobalID",get_onChange:"get_onChange"}
};
unbox.framework.controller.PageController = function(pageLayer,context) {
	this.pageLayer = pageLayer;
	this.context = context;
	this.nextPageDirection = 0;
	this._onPageChanged = new msignal.Signal1();
	this._onBeforePageChanged = new msignal.Signal1();
};
$hxClasses["unbox.framework.controller.PageController"] = unbox.framework.controller.PageController;
unbox.framework.controller.PageController.__name__ = ["unbox","framework","controller","PageController"];
unbox.framework.controller.PageController.prototype = {
	setup: function(nextPageData,direction) {
		this.lastPage = this.currentPage;
		this.lastPageContent = this.currentPageContent;
		this.lastPageData = this.currentPageData;
		this.currentPage = null;
		this.pView = null;
		this.currentPageContent = null;
		this.currentPageData = nextPageData;
		this.nextPageDirection = direction;
	}
	,onPageAssetsLoaded: function(pageAssets) {
		var $it0 = pageAssets.keys();
		while( $it0.hasNext() ) {
			var key = $it0.next();
			if(key == "contentXML") {
				var pageXml = Xml.parse(pageAssets.get(key));
				this.currentPageContent = new haxe.xml.Fast(pageXml.firstElement());
				var this1 = this.currentPageData.get_assets();
				this1.set(key,this.currentPageContent);
			} else if(key.indexOf("_P_") > -1 || key.indexOf("cover") > -1) {
				var p = null;
				p = (js.Boot.__cast(pageAssets.get(key) , openfl.AssetLibrary)).getMovieClip("com.unbox.assets.PageView");
				this.pView = p;
				if(this.pView.get_name().indexOf("instance") < 0) this.pView.set_name("pageView");
			} else {
				var asset = pageAssets.get(key);
				var additionalContent = null;
				if(Type.getClassName(Type.getClass(asset)) == "String") {
					var addData = Xml.parse(asset);
					additionalContent = new haxe.xml.Fast(addData.firstElement());
				}
				var this2 = this.currentPageData.get_assets();
				this2.set(key,additionalContent);
			}
		}
		if(this.pView != null && this.currentPageContent != null) {
			var mask = new openfl.display.Shape();
			mask.get_graphics().beginFill(16711935,1);
			mask.get_graphics().drawRect(0,0,this.pageLayer.stage.stageWidth,this.pageLayer.stage.stageHeight);
			this.pView.set_mask(mask);
			this.pView.addChild(mask);
			if(this.currentPageContent.hasNode.resolve("page") && this.currentPageContent.node.resolve("page").att.resolve("className") != "null") {
				var className = this.currentPageContent.node.resolve("page").att.resolve("className");
				haxe.Log.trace("className : " + className,{ fileName : "PageController.hx", lineNumber : 118, className : "unbox.framework.controller.PageController", methodName : "onPageAssetsLoaded"});
				var classRef = Type.resolveClass(className);
				haxe.Log.trace("classRef class name : " + Type.getClassName(classRef),{ fileName : "PageController.hx", lineNumber : 120, className : "unbox.framework.controller.PageController", methodName : "onPageAssetsLoaded"});
				this.currentPage = Type.createInstance(classRef,[]);
				this.currentPage.view = this.pView;
				haxe.Log.trace("currentPage : " + Std.string(this.currentPage),{ fileName : "PageController.hx", lineNumber : 124, className : "unbox.framework.controller.PageController", methodName : "onPageAssetsLoaded"});
				this.currentPage.name = this.currentPageContent.node.resolve("page").att.resolve("name");
			} else {
				this.currentPage = new unbox.framework.view.CoursePage();
				this.currentPage.view = this.pView;
				this.currentPage.name = this.currentPageContent.node.resolve("page").att.resolve("name");
			}
			haxe.Log.trace("### currentPage : " + Std.string(this.currentPage),{ fileName : "PageController.hx", lineNumber : 133, className : "unbox.framework.controller.PageController", methodName : "onPageAssetsLoaded"});
			this.currentPage.name = this.currentPageContent.node.resolve("page").att.resolve("name");
			this.currentPage.data = this.currentPageData;
			this.currentPage.context = this.context;
			unbox.framework.utils.ContentParser.parsePageContent(this.currentPageContent,this.currentPage,$bind(this,this.changePage));
		}
	}
	,changePage: function() {
		if(this.lastPage != null && this.lastPageData.get_pageTransitionOut() != "none") {
			this.lastPage.transitionOut();
			unbox.framework.utils.PageAnimator.animate(this.lastPage.view,0.5,this.nextPageDirection,this.lastPageData.get_pageTransitionOut(),$bind(this,this.animateContent),[this.lastPage,this.lastPageData]);
		} else if(this.lastPage != null) {
			this.lastPage.transitionOut();
			this.animateContent(this.lastPage,this.lastPageData);
		}
		this.pageLayer.addChild(this.currentPage.view);
		this._onBeforePageChanged.dispatch(this.currentPage);
		if(this.currentPageData.get_pageTransitionIn() != "none") {
			this.currentPage.transitionIn();
			unbox.framework.utils.PageAnimator.animate(this.currentPage.view,0.5,this.nextPageDirection,this.currentPageData.get_pageTransitionIn(),$bind(this,this.animateContent),[this.currentPage,this.currentPageData]);
		} else {
			this.lastPage.transitionOut();
			this.animateContent(this.currentPage,this.currentPageData);
		}
	}
	,animateContent: function(page,pageData) {
		if(page == this.currentPage) {
			if(pageData.get_contentTransitionIn() != "none") unbox.framework.utils.PageAnimator.contentFadeIn(page.view,0.5,0,$bind(this,this.onTransitionComplete),[page]); else this.onTransitionComplete(page);
		} else if(pageData.get_contentTransitionOut() != "none") unbox.framework.utils.PageAnimator.contentFadeOut(page.view,0.5,0,$bind(this,this.onTransitionComplete),[page]); else this.onTransitionComplete(page);
	}
	,onTransitionComplete: function(page) {
		if(page == this.currentPage) {
			if(this.currentPageContent.hasNode.resolve("tween")) unbox.framework.utils.TweenParser.parse(page.view,this.currentPageContent.node.resolve("tween"));
			page.transitionInComplete();
			this._onPageChanged.dispatch(page);
		} else {
			page.transitionOutComplete();
			this.pageLayer.removeChild(page.view);
		}
	}
	,get_onPageChanged: function() {
		return this._onPageChanged;
	}
	,set_onPageChanged: function(value) {
		return this._onPageChanged = value;
	}
	,get_onBeforePageChanged: function() {
		return this._onBeforePageChanged;
	}
	,set_onBeforePageChanged: function(value) {
		return this._onBeforePageChanged = value;
	}
	,__class__: unbox.framework.controller.PageController
	,__properties__: {set_onBeforePageChanged:"set_onBeforePageChanged",get_onBeforePageChanged:"get_onBeforePageChanged",set_onPageChanged:"set_onPageChanged",get_onPageChanged:"get_onPageChanged"}
};
unbox.framework.model = {};
unbox.framework.model.CourseModel = function(courseData) {
	this.rawData = courseData;
	this.init();
};
$hxClasses["unbox.framework.model.CourseModel"] = unbox.framework.model.CourseModel;
unbox.framework.model.CourseModel.__name__ = ["unbox","framework","model","CourseModel"];
unbox.framework.model.CourseModel.prototype = {
	init: function() {
		var config = this.rawData.node.resolve("config");
		this._configData = new unbox.framework.model.vo.ConfigData();
		this._configData.set_browseMode(config.att.resolve("consultMode") == "true"?true:false);
		this._configData.set_accessible(config.att.resolve("accessible") == "true"?true:false);
		this._configData.set_debugActive(config.att.resolve("debugActive") == "true"?true:false);
		this._configData.set_dataServiceType(config.att.resolve("dataServiceType"));
		var indexPage = this.rawData.node.resolve("page");
		this._indexViewUrl = indexPage.att.resolve("src");
		haxe.Log.trace("_indexViewUrl : " + this._indexViewUrl,{ fileName : "CourseModel.hx", lineNumber : 64, className : "unbox.framework.model.CourseModel", methodName : "init"});
		var $it0 = indexPage.nodes.resolve("asset").iterator();
		while( $it0.hasNext() ) {
			var asset = $it0.next();
			var _g = asset.att.resolve("id");
			switch(_g) {
			case "fonts":
				this._fontClasses = asset.att.resolve("fonts").split(",");
				this._fontsUrl = asset.att.resolve("src");
				break;
			case "styles":
				this._stylesUrl = asset.att.resolve("src");
				break;
			}
		}
		var navPage = indexPage.node.resolve("page");
		this._uiViewUrl = navPage.att.resolve("src");
		this._uiModelUrl = navPage.node.resolve("asset").att.resolve("src");
		var modules = new Array();
		this._pages = new Array();
		var localCounter = 0;
		var globalCounter = 0;
		var moduleCounter = 0;
		var $it1 = navPage.nodes.resolve("page").iterator();
		while( $it1.hasNext() ) {
			var module = $it1.next();
			if(module.att.resolve("id") == "cover") {
				var cpData = new unbox.framework.model.vo.PageData();
				cpData.set_id(module.att.resolve("id"));
				cpData.set_src(module.att.resolve("src"));
				cpData.set_title(module.att.resolve("title"));
				cpData.set_localIndex(localCounter);
				cpData.set_globalIndex(globalCounter);
				cpData.set_numPagesInModule(1);
				cpData.set_moduleId(module.att.resolve("id"));
				cpData.set_moduleIndex(moduleCounter);
				cpData.set_contentUrls(new haxe.ds.StringMap());
				cpData.set_assets(new haxe.ds.StringMap());
				var $it2 = module.nodes.resolve("asset").iterator();
				while( $it2.hasNext() ) {
					var asset1 = $it2.next();
					var this1 = cpData.get_contentUrls();
					var key = asset1.att.resolve("id");
					var value = asset1.att.resolve("src");
					this1.set(key,value);
				}
				cpData.set_pageTransitionIn(module.att.resolve("pageTransitionIn"));
				cpData.set_pageTransitionOut(module.att.resolve("pageTransitionOut"));
				cpData.set_contentTransitionIn(module.att.resolve("contentTransitionIn"));
				cpData.set_contentTransitionOut(module.att.resolve("contentTransitionOut"));
				cpData.set_showProgress(module.att.resolve("showProgress") == "true"?true:false);
				this._pages.push(cpData);
				globalCounter++;
			}
			modules.push(module);
			var $it3 = module.nodes.resolve("page").iterator();
			while( $it3.hasNext() ) {
				var page = $it3.next();
				var pData = new unbox.framework.model.vo.PageData();
				pData.set_id(page.att.resolve("id"));
				pData.set_src(page.att.resolve("src"));
				pData.set_title(page.att.resolve("title"));
				pData.set_localIndex(localCounter);
				pData.set_globalIndex(globalCounter);
				pData.set_numPagesInModule(module.nodes.resolve("page").length);
				pData.set_moduleId(module.att.resolve("id"));
				pData.set_moduleIndex(moduleCounter);
				pData.set_contentUrls(new haxe.ds.StringMap());
				pData.set_assets(new haxe.ds.StringMap());
				var $it4 = page.nodes.resolve("asset").iterator();
				while( $it4.hasNext() ) {
					var asset2 = $it4.next();
					var this2 = pData.get_contentUrls();
					var key1 = asset2.att.resolve("id");
					var value1 = asset2.att.resolve("src");
					this2.set(key1,value1);
				}
				pData.set_pageTransitionIn(page.att.resolve("pageTransitionIn"));
				pData.set_pageTransitionOut(page.att.resolve("pageTransitionOut"));
				pData.set_contentTransitionIn(page.att.resolve("contentTransitionIn"));
				pData.set_contentTransitionOut(page.att.resolve("contentTransitionOut"));
				pData.set_showProgress(page.att.resolve("showProgress") == "true"?true:false);
				var pgProps = page.att.resolve("navbarStatus").split("");
				if(pgProps != null && pgProps.length > 0) {
					pData.set_pageProps(new Array());
					var _g1 = 0;
					var _g2 = pgProps.length;
					while(_g1 < _g2) {
						var i = _g1++;
						pData.get_pageProps()[i] = Std.parseInt(pgProps[i]);
					}
				}
				this._pages.push(pData);
				localCounter++;
				globalCounter++;
			}
			localCounter = 0;
			moduleCounter++;
		}
		this._totalModules = modules.length;
		this._totalPages = this.get_pages().length;
	}
	,getPage: function(id) {
		if(id < 0 || id >= this.get_pages().length) {
			unbox.framework.utils.Logger.log("Invalid page id!",{ fileName : "CourseModel.hx", lineNumber : 169, className : "unbox.framework.model.CourseModel", methodName : "getPage"});
			return null;
		}
		return this.get_pages()[id];
	}
	,registerFonts: function(container,appDom) {
		var numFonts = this._fontClasses.length;
		var _g = 0;
		while(_g < numFonts) {
			var i = _g++;
			if(appDom.hasDefinition(this._fontClasses[i]) == true) {
			} else unbox.framework.utils.Logger.log("ERROR : Font class '" + this._fontClasses[i] + "' is NOT available!",{ fileName : "CourseModel.hx", lineNumber : 191, className : "unbox.framework.model.CourseModel", methodName : "registerFonts"});
		}
	}
	,debugFonts: function(view) {
		unbox.framework.utils.Logger.log("Registered fonts : " + Std.string(openfl.text.Font.enumerateFonts(false)),{ fileName : "CourseModel.hx", lineNumber : 199, className : "unbox.framework.model.CourseModel", methodName : "debugFonts"});
		var numFonts = this._fontClasses.length;
		var _g = 0;
		while(_g < numFonts) {
			var i = _g++;
			var txt = new openfl.text.TextField();
			txt.embedFonts = true;
			txt.antiAliasType = openfl.text.AntiAliasType.ADVANCED;
			txt.gridFitType = openfl.text.GridFitType.SUBPIXEL;
			txt.set_wordWrap(true);
			txt.selectable = false;
			txt.set_width(100);
			txt.set_height(300);
			txt.set_x(20 + 100 * i + i * 20);
			txt.set_y(20);
			switch(i) {
			case 0:
				txt.set_htmlText("<p class=\"tipText\">abcdefghi ABCDEFGHI<p>");
				break;
			case 1:
				txt.set_htmlText("<p class=\"altDown\">abcdefghi ABCDEFGHI<p>");
				break;
			case 2:
				txt.set_htmlText("<p class=\"button\">abcdefghi ABCDEFGHI<p>");
				break;
			case 3:
				txt.set_htmlText("<h5>abcdefghi ABCDEFGHI<h5>");
				break;
			case 4:
				txt.set_htmlText("<h3>abcdefghi ABCDEFGHI<h3>");
				break;
			}
			view.addChild(txt);
		}
	}
	,get_totalModules: function() {
		return this._totalModules;
	}
	,set_totalModules: function(value) {
		return this._totalModules = value;
	}
	,get_totalPages: function() {
		return this._totalPages;
	}
	,set_totalPages: function(value) {
		return this._totalPages = value;
	}
	,get_indexViewUrl: function() {
		return this._indexViewUrl;
	}
	,get_fontClasses: function() {
		return this._fontClasses;
	}
	,get_fontsUrl: function() {
		return this._fontsUrl;
	}
	,get_stylesUrl: function() {
		return this._stylesUrl;
	}
	,get_uiViewUrl: function() {
		return this._uiViewUrl;
	}
	,get_uiModelUrl: function() {
		return this._uiModelUrl;
	}
	,get_navView: function() {
		return this._navView;
	}
	,set_navView: function(value) {
		return this._navView = value;
	}
	,get_indexView: function() {
		return this._indexView;
	}
	,set_indexView: function(value) {
		return this._indexView = value;
	}
	,get_fonts: function() {
		return this._fonts;
	}
	,set_fonts: function(value) {
		return this._fonts = value;
	}
	,get_pages: function() {
		return this._pages;
	}
	,get_configData: function() {
		return this._configData;
	}
	,set_configData: function(value) {
		return this._configData = value;
	}
	,__class__: unbox.framework.model.CourseModel
	,__properties__: {set_configData:"set_configData",get_configData:"get_configData",get_pages:"get_pages",set_fonts:"set_fonts",get_fonts:"get_fonts",set_indexView:"set_indexView",get_indexView:"get_indexView",set_navView:"set_navView",get_navView:"get_navView",get_uiModelUrl:"get_uiModelUrl",get_uiViewUrl:"get_uiViewUrl",get_stylesUrl:"get_stylesUrl",get_fontsUrl:"get_fontsUrl",get_fontClasses:"get_fontClasses",get_indexViewUrl:"get_indexViewUrl",set_totalPages:"set_totalPages",get_totalPages:"get_totalPages",set_totalModules:"set_totalModules",get_totalModules:"get_totalModules"}
};
unbox.framework.model.SessionTimer = function() {
};
$hxClasses["unbox.framework.model.SessionTimer"] = unbox.framework.model.SessionTimer;
unbox.framework.model.SessionTimer.__name__ = ["unbox","framework","model","SessionTimer"];
unbox.framework.model.SessionTimer.startTime = null;
unbox.framework.model.SessionTimer.endTime = null;
unbox.framework.model.SessionTimer.sessionTime = null;
unbox.framework.model.SessionTimer.d = null;
unbox.framework.model.SessionTimer.prototype = {
	initSession: function() {
		unbox.framework.model.SessionTimer.d = new Date();
		unbox.framework.model.SessionTimer.startTime = unbox.framework.model.SessionTimer.d.getTime();
	}
	,getSessionTime: function() {
		unbox.framework.model.SessionTimer.d = new Date();
		unbox.framework.model.SessionTimer.sessionTime = (unbox.framework.model.SessionTimer.d.getTime() - unbox.framework.model.SessionTimer.startTime) / 1000;
		return unbox.framework.model.SessionTimer.sessionTime;
	}
	,getCMISessionTime: function() {
		return this.secondsToCMITime(this.getSessionTime());
	}
	,secondsToCMITime: function(seconds) {
		var hr = seconds / 3600 | 0;
		var min = seconds / 60 - hr * 60 | 0;
		var sec = unbox.framework.utils.NumberUtils.fmtNumDec(seconds % 60 | 0,2);
		var cmiTime = unbox.framework.utils.NumberUtils.fixPadding(hr,4) + ":" + unbox.framework.utils.NumberUtils.fixPadding(min,2) + ":" + unbox.framework.utils.NumberUtils.fixPadding(sec,2);
		return cmiTime;
	}
	,cmiTimeToSeconds: function(cmiTime) {
		var timeArr = cmiTime.split(":");
		var seconds = 0;
		var _g1 = 0;
		var _g = timeArr.length;
		while(_g1 < _g) {
			var i = _g1++;
			var time = Std.parseFloat(timeArr[i]);
			seconds += time * Math.pow(60,timeArr.length - 1 - i);
		}
		return seconds;
	}
	,__class__: unbox.framework.model.SessionTimer
};
unbox.framework.model.Status = function() {
	this._ebookVersion = "UNBOX_HAXEFRAMEWORK_V0.3";
	this._status = 0;
	this._maxPage = 0;
	this._maxModule = 0;
	this._currentPage = 0;
	this._currentModule = 0;
	this._currentLesson = 0;
	this._quizTries = 0;
	this._quizStatus = 0;
	this._lessonStatus = new unbox.framework.model.vo.CustomData();
	this._startDate = new Date();
	this._endDate = new Date();
};
$hxClasses["unbox.framework.model.Status"] = unbox.framework.model.Status;
unbox.framework.model.Status.__name__ = ["unbox","framework","model","Status"];
unbox.framework.model.Status.prototype = {
	parseData: function(values) {
		if(values != null) {
			if(values.length > 4096) throw new openfl.errors.Error("*** WARNING: Data overflow! ParseData string > 4096 chars ***");
			values = unbox.framework.utils.StringUtils.replace(values,"'","\"");
			values = unbox.framework.utils.StringUtils.replace(values,String.fromCharCode(180),"\"");
			var data = values.split("|");
			this._ebookVersion = data[0];
			this._status = Std.parseInt(data[1]);
			this._maxPage = Std.parseInt(data[2]);
			this._maxModule = Std.parseInt(data[3]);
			this._currentPage = Std.parseInt(data[4]);
			this._currentModule = Std.parseInt(data[5]);
			this._currentLesson = Std.parseInt(data[6]);
			this._quizStatus = Std.parseInt(data[7]);
			this._quizTries = Std.parseInt(data[8]);
			this._quizScore = Std.parseInt(data[9]);
			this._startDate = HxOverrides.strDate(data[10]);
			this._endDate = HxOverrides.strDate(data[11]);
			this._lessonStatus = new unbox.framework.model.vo.CustomData(Std.string(data[12]));
		}
	}
	,toString: function() {
		var data = new Array();
		data.push(this._ebookVersion);
		data.push(this._status);
		data.push(this._maxPage);
		data.push(this._maxModule);
		data.push(this._currentPage);
		data.push(this._currentModule);
		data.push(this._currentLesson);
		data.push(this._quizStatus);
		data.push(this._quizTries);
		data.push(this._quizScore);
		data.push(Std.string(this._startDate));
		data.push(Std.string(this._endDate));
		data.push(JSON.stringify(this._lessonStatus));
		var ret = data.join("|");
		ret = unbox.framework.utils.StringUtils.replace(ret,"\"","'");
		return ret;
	}
	,get_ebookVersion: function() {
		return this._ebookVersion;
	}
	,set_ebookVersion: function(ebookVersion) {
		this._ebookVersion = ebookVersion;
		return ebookVersion;
	}
	,get_maxPage: function() {
		return this._maxPage;
	}
	,set_maxPage: function(maxPage) {
		this._maxPage = maxPage;
		return maxPage;
	}
	,get_currentPage: function() {
		return this._currentPage;
	}
	,set_currentPage: function(currentPage) {
		this._currentPage = currentPage;
		return currentPage;
	}
	,get_currentModule: function() {
		return this._currentModule;
	}
	,set_currentModule: function(currentModule) {
		this._currentModule = currentModule;
		return currentModule;
	}
	,get_currentLesson: function() {
		return this._currentLesson;
	}
	,set_currentLesson: function(currentLesson) {
		this._currentLesson = currentLesson;
		return currentLesson;
	}
	,get_quizTries: function() {
		return this._quizTries;
	}
	,set_quizTries: function(quizCount) {
		this._quizTries = quizCount;
		return quizCount;
	}
	,get_quizScore: function() {
		return this._quizScore;
	}
	,set_quizScore: function(quizScore) {
		this._quizScore = quizScore;
		return quizScore;
	}
	,get_quizStatus: function() {
		return this._quizStatus;
	}
	,set_quizStatus: function(quizStatus) {
		this._quizStatus = quizStatus;
		return quizStatus;
	}
	,get_lessonStatus: function() {
		return this._lessonStatus;
	}
	,set_lessonStatus: function(lessonStatus) {
		this._lessonStatus = lessonStatus;
		return lessonStatus;
	}
	,get_status: function() {
		return this._status;
	}
	,set_status: function(status) {
		this._status = status;
		return status;
	}
	,get_maxModule: function() {
		return this._maxModule;
	}
	,set_maxModule: function(value) {
		this._maxModule = value;
		return value;
	}
	,get_startDate: function() {
		return this._startDate;
	}
	,set_startDate: function(value) {
		this._startDate = value;
		return value;
	}
	,get_endDate: function() {
		return this._endDate;
	}
	,set_endDate: function(value) {
		this._endDate = value;
		return value;
	}
	,__class__: unbox.framework.model.Status
	,__properties__: {set_endDate:"set_endDate",get_endDate:"get_endDate",set_startDate:"set_startDate",get_startDate:"get_startDate",set_maxModule:"set_maxModule",get_maxModule:"get_maxModule",set_status:"set_status",get_status:"get_status",set_lessonStatus:"set_lessonStatus",get_lessonStatus:"get_lessonStatus",set_quizStatus:"set_quizStatus",get_quizStatus:"get_quizStatus",set_quizScore:"set_quizScore",get_quizScore:"get_quizScore",set_quizTries:"set_quizTries",get_quizTries:"get_quizTries",set_currentLesson:"set_currentLesson",get_currentLesson:"get_currentLesson",set_currentModule:"set_currentModule",get_currentModule:"get_currentModule",set_currentPage:"set_currentPage",get_currentPage:"get_currentPage",set_maxPage:"set_maxPage",get_maxPage:"get_maxPage",set_ebookVersion:"set_ebookVersion",get_ebookVersion:"get_ebookVersion"}
};
unbox.framework.model.UIModel = function(navData) {
	this._rawData = new haxe.xml.Fast(navData.firstElement());
	this.init();
};
$hxClasses["unbox.framework.model.UIModel"] = unbox.framework.model.UIModel;
unbox.framework.model.UIModel.__name__ = ["unbox","framework","model","UIModel"];
unbox.framework.model.UIModel.prototype = {
	init: function() {
		var $it0 = this._rawData.nodes.resolve("object").iterator();
		while( $it0.hasNext() ) {
			var object = $it0.next();
			var _g = object.att.resolve("type");
			switch(_g) {
			case "ProgressMeter":
				this.parseProgressMeter(object);
				break;
			case "NavBar":
				this.parseNavBar(object);
				break;
			case "Dashboard":
				this.parseDashboard(object);
				break;
			default:
				unbox.framework.utils.Logger.log("Xml parsing problem: Nav <object> type '" + object.att.resolve("type") + "' mapping not yet implemented.",{ fileName : "UIModel.hx", lineNumber : 51, className : "unbox.framework.model.UIModel", methodName : "init"});
			}
		}
	}
	,tracelogData: function() {
		unbox.framework.utils.Logger.log("@@@ UI MODEL @@@ \n" + this._progressMeterData.toString() + "\n" + this._navBarData.toString() + "\n" + this._dashboardData.toString() + "\n@@@ END @@@",{ fileName : "UIModel.hx", lineNumber : 58, className : "unbox.framework.model.UIModel", methodName : "tracelogData"});
	}
	,parseProgressMeter: function(data) {
		haxe.Log.trace("UIModel.parseProgressMeter",{ fileName : "UIModel.hx", lineNumber : 63, className : "unbox.framework.model.UIModel", methodName : "parseProgressMeter"});
		this._progressMeterData = new unbox.framework.model.vo.ProgressMeterData();
		this._progressMeterData.set_className(data.att.resolve("className"));
		this._progressMeterData.set_depth(Std.parseInt(data.att.resolve("depth")));
		this._progressMeterData.set_instanceName(data.att.resolve("instanceName"));
		this._progressMeterData.set_x(Std.parseFloat(data.att.resolve("x")));
		this._progressMeterData.set_y(Std.parseFloat(data.att.resolve("y")));
		this._progressMeterData.set_openX(Std.parseFloat(data.att.resolve("openX")));
		this._progressMeterData.set_openY(Std.parseFloat(data.att.resolve("openY")));
		this._progressMeterData.set_width(Std.parseInt(data.att.resolve("width")));
		this._progressMeterData.set_height(Std.parseInt(data.att.resolve("height")));
		this._progressMeterData.set_scrollbarColor(Std.parseInt(data.att.resolve("scrollbarColor")));
		this._progressMeterData.set_scrollbarColorAlpha(Std.parseFloat(data.att.resolve("scrollbarColorAlpha")));
		this._progressMeterData.set_thumbColor(Std.parseInt(data.att.resolve("thumbColor")));
		this._progressMeterData.set_thumbColorAlpha(Std.parseFloat(data.att.resolve("thumbColorAlpha")));
		this._progressMeterData.set_thumbFillMode(data.att.resolve("thumbFillMode") == "true"?true:false);
		this._progressMeterData.set_autoHideThumb(data.att.resolve("autoHideThumb") == "true"?true:false);
	}
	,parseNavBar: function(data) {
		haxe.Log.trace("UIModel.parseNavBar",{ fileName : "UIModel.hx", lineNumber : 84, className : "unbox.framework.model.UIModel", methodName : "parseNavBar"});
		this._navBarData = new unbox.framework.model.vo.NavBarData();
		this._navBarData.set_texts(new haxe.ds.StringMap());
		this._navBarData.set_instanceName(data.att.resolve("instanceName"));
		this._navBarData.set_x(Std.parseFloat(data.att.resolve("x")));
		this._navBarData.set_y(Std.parseFloat(data.att.resolve("y")));
		var $it0 = data.nodes.resolve("content").iterator();
		while( $it0.hasNext() ) {
			var content = $it0.next();
			if(content.att.resolve("type") == "tooltip") {
				this._navBarData.set_tooltips(new unbox.framework.model.vo.TooltipData());
				this._navBarData.get_tooltips().set_hook(content.att.resolve("hook") == "true"?true:false);
				this._navBarData.get_tooltips().set_hookSize(Std.parseInt(content.att.resolve("hookSize")));
				this._navBarData.get_tooltips().set_autoSize(content.att.resolve("autoSize") == "true"?true:false);
				this._navBarData.get_tooltips().set_followCursor(content.att.resolve("followCursor") == "true"?true:false);
				this._navBarData.get_tooltips().set_align(content.att.resolve("align"));
				this._navBarData.get_tooltips().set_cornerRadius(Std.parseFloat(content.att.resolve("cornerRadius")));
				this._navBarData.get_tooltips().set_showDelay(Std.parseInt(content.att.resolve("showDelay")));
				this._navBarData.get_tooltips().set_hideDelay(Std.parseInt(content.att.resolve("hideDelay")));
				this._navBarData.get_tooltips().set_bgAlpha(Std.parseFloat(content.att.resolve("bgAlpha")));
				this._navBarData.get_tooltips().set_width(Std.parseInt(content.att.resolve("width")));
				this._navBarData.get_tooltips().set_padding(Std.parseFloat(content.att.resolve("padding")));
				this._navBarData.get_tooltips().set_colors(new Array());
				var rawColors = content.att.resolve("colors").split(",");
				this._navBarData.get_tooltips().get_colors()[0] = Std.parseInt(rawColors[0]);
				this._navBarData.get_tooltips().get_colors()[1] = Std.parseInt(rawColors[1]);
				this._navBarData.get_tooltips().set_items(new haxe.ds.StringMap());
				var i = 0;
				var $it1 = content.nodes.resolve("content").iterator();
				while( $it1.hasNext() ) {
					var tooltip = $it1.next();
					var nodeData;
					if(tooltip.hasNode.resolve("body")) nodeData = tooltip.node.resolve("body").get_innerHTML(); else if(tooltip.hasNode.resolve("title")) nodeData = tooltip.node.resolve("title").get_innerHTML(); else nodeData = "<![CDATA[<p>ERROR:NODATA</p>]]>";
					if(tooltip.has.resolve("instanceName")) {
						var this1 = this._navBarData.get_tooltips().get_items();
						var key = tooltip.att.resolve("instanceName");
						this1.set(key,nodeData);
					} else {
						var this2 = this._navBarData.get_tooltips().get_items();
						this2.set("tooltip_" + i,nodeData);
					}
					i++;
				}
			} else if(content.att.resolve("type") == "text") {
				var $it2 = content.nodes.resolve("content").iterator();
				while( $it2.hasNext() ) {
					var text = $it2.next();
					var this3 = this._navBarData.get_texts();
					var key1 = text.att.resolve("instanceName");
					var value = text.node.resolve("title").get_innerHTML();
					this3.set(key1,value);
				}
			}
		}
	}
	,parseDashboard: function(data) {
		this._dashboardData = new unbox.framework.model.vo.DashboardData();
		var $it0 = data.nodes.resolve("object").iterator();
		while( $it0.hasNext() ) {
			var object = $it0.next();
			var _g = object.att.resolve("type");
			switch(_g) {
			case "aboutPanel":
				this.parseAboutPanel(object);
				break;
			case "bookmarkPanel":
				this.parseBookmarkPanel(object);
				break;
			case "indexPanel":
				this.parseIndexPanel(object);
				break;
			case "searchPanel":
				this.parseSearchPanel(object);
				break;
			case "searchBox":
				this.parseSearchBox(object);
				break;
			case "HelpPanel":
				this.parseHelpPanel(object);
				break;
			case "tooltip":
				this.parseTooltip(object);
				break;
			default:
				unbox.framework.utils.Logger.log("Xml parsing problem: Dashboard <object> type '" + object.att.resolve("type") + "' mapping not yet implemented.",{ fileName : "UIModel.hx", lineNumber : 158, className : "unbox.framework.model.UIModel", methodName : "parseDashboard"});
			}
		}
	}
	,parseAboutPanel: function(data) {
		haxe.Log.trace("UIModel.parseAboutPanel",{ fileName : "UIModel.hx", lineNumber : 164, className : "unbox.framework.model.UIModel", methodName : "parseAboutPanel"});
		this._dashboardData.set_aboutPanel(new unbox.framework.model.vo.AboutPanelData());
		this._dashboardData.get_aboutPanel().set_title(data.att.resolve("title"));
		this._dashboardData.get_aboutPanel().set_x(Std.parseFloat(data.att.resolve("x")));
		this._dashboardData.get_aboutPanel().set_y(Std.parseFloat(data.att.resolve("y")));
		this._dashboardData.get_aboutPanel().viewClass = data.att.resolve("viewClass");
		this._dashboardData.get_aboutPanel().set_texts(new haxe.ds.StringMap());
		var i = 0;
		var $it0 = data.node.resolve("content").nodes.resolve("content").iterator();
		while( $it0.hasNext() ) {
			var text = $it0.next();
			if(text.has.resolve("instanceName")) {
				var this1 = this._dashboardData.get_aboutPanel().get_texts();
				var key = text.att.resolve("instanceName");
				this1.set(key,text);
			} else {
				var this2 = this._dashboardData.get_aboutPanel().get_texts();
				this2.set("text_" + i,text);
				i++;
			}
		}
	}
	,parseBookmarkPanel: function(data) {
		haxe.Log.trace("UIModel.parseBookmarkPanel",{ fileName : "UIModel.hx", lineNumber : 186, className : "unbox.framework.model.UIModel", methodName : "parseBookmarkPanel"});
		this._dashboardData.set_bookmarkPanel(new unbox.framework.model.vo.BookmarkPanelData());
		this._dashboardData.get_bookmarkPanel().set_title(data.att.resolve("title"));
		this._dashboardData.get_bookmarkPanel().set_x(Std.parseFloat(data.att.resolve("x")));
		this._dashboardData.get_bookmarkPanel().set_y(Std.parseFloat(data.att.resolve("y")));
		this._dashboardData.get_bookmarkPanel().set_list(new unbox.framework.model.vo.ListData());
		this._dashboardData.get_bookmarkPanel().set_scrollbar(new unbox.framework.model.vo.ScrollbarData());
		this._dashboardData.get_bookmarkPanel().set_texts(new haxe.ds.StringMap());
		this._dashboardData.get_bookmarkPanel().viewClass = data.att.resolve("viewClass");
		var $it0 = data.nodes.resolve("object").iterator();
		while( $it0.hasNext() ) {
			var subObject = $it0.next();
			if(subObject.att.resolve("type") == "List") {
				this._dashboardData.get_bookmarkPanel().get_list().set_name(subObject.att.resolve("name"));
				this._dashboardData.get_bookmarkPanel().get_list().set_x(Std.parseFloat(subObject.att.resolve("x")));
				this._dashboardData.get_bookmarkPanel().get_list().set_y(Std.parseFloat(subObject.att.resolve("y")));
				this._dashboardData.get_bookmarkPanel().get_list().set_width(Std.parseInt(subObject.att.resolve("width")));
				this._dashboardData.get_bookmarkPanel().get_list().set_height(Std.parseInt(subObject.att.resolve("height")));
				this._dashboardData.get_bookmarkPanel().get_list().set_columns(Std.parseInt(subObject.att.resolve("columns")));
				this._dashboardData.get_bookmarkPanel().get_list().set_rows(Std.parseInt(subObject.att.resolve("rows")));
				this._dashboardData.get_bookmarkPanel().get_list().set_spacing(Std.parseInt(subObject.att.resolve("spacing")));
				this._dashboardData.get_bookmarkPanel().get_list().viewClass = subObject.att.resolve("viewClass");
			} else if(subObject.att.resolve("type") == "Scrollbar") {
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_x(Std.parseFloat(subObject.att.resolve("x")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_y(Std.parseFloat(subObject.att.resolve("y")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_width(Std.parseInt(subObject.att.resolve("width")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_height(Std.parseInt(subObject.att.resolve("height")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_scrollbarColor(Std.parseInt(subObject.att.resolve("scrollbarColor")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_scrollbarColorAlpha(Std.parseFloat(subObject.att.resolve("scrollbarColorAlpha")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_thumbColor(Std.parseInt(subObject.att.resolve("thumbColor")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_thumbColorAlpha(Std.parseFloat(subObject.att.resolve("thumbColorAlpha")));
				this._dashboardData.get_bookmarkPanel().get_scrollbar().set_autoHideThumb(subObject.att.resolve("autoHideThumb") == "true"?true:false);
			}
		}
		var i = 0;
		var $it1 = data.node.resolve("content").nodes.resolve("content").iterator();
		while( $it1.hasNext() ) {
			var text = $it1.next();
			if(text.has.resolve("instanceName")) {
				var this1 = this._dashboardData.get_bookmarkPanel().get_texts();
				var key = text.att.resolve("instanceName");
				this1.set(key,text);
			} else {
				var this2 = this._dashboardData.get_bookmarkPanel().get_texts();
				this2.set("text_" + i,text);
				i++;
			}
		}
	}
	,parseIndexPanel: function(data) {
		haxe.Log.trace("UIModel.parseIndexPanel",{ fileName : "UIModel.hx", lineNumber : 236, className : "unbox.framework.model.UIModel", methodName : "parseIndexPanel"});
		this._dashboardData.set_indexPanel(new unbox.framework.model.vo.IndexPanelData());
		this._dashboardData.get_indexPanel().set_title(data.att.resolve("title"));
		this._dashboardData.get_indexPanel().set_x(Std.parseFloat(data.att.resolve("x")));
		this._dashboardData.get_indexPanel().set_y(Std.parseFloat(data.att.resolve("y")));
		this._dashboardData.get_indexPanel().viewClass = data.att.resolve("viewClass");
		this._dashboardData.get_indexPanel().set_menuLists(new Array());
		this._dashboardData.get_indexPanel().set_scrollbars(new Array());
		var $it0 = data.nodes.resolve("object").iterator();
		while( $it0.hasNext() ) {
			var subObject = $it0.next();
			if(subObject.att.resolve("type") == "List") {
				var list = new unbox.framework.model.vo.ListData();
				list.set_name(subObject.att.resolve("name"));
				list.set_x(Std.parseFloat(subObject.att.resolve("x")));
				list.set_y(Std.parseFloat(subObject.att.resolve("y")));
				list.set_width(Std.parseInt(subObject.att.resolve("width")));
				list.set_height(Std.parseInt(subObject.att.resolve("height")));
				list.set_columns(Std.parseInt(subObject.att.resolve("columns")));
				list.set_rows(Std.parseInt(subObject.att.resolve("rows")));
				list.set_spacing(Std.parseInt(subObject.att.resolve("spacing")));
				list.viewClass = subObject.att.resolve("viewClass");
				this._dashboardData.get_indexPanel().get_menuLists().push(list);
			} else if(subObject.att.resolve("type") == "Scrollbar") {
				var scrollbar = new unbox.framework.model.vo.ScrollbarData();
				scrollbar.set_x(Std.parseFloat(subObject.att.resolve("x")));
				scrollbar.set_y(Std.parseFloat(subObject.att.resolve("y")));
				scrollbar.set_width(Std.parseInt(subObject.att.resolve("width")));
				scrollbar.set_height(Std.parseInt(subObject.att.resolve("height")));
				scrollbar.set_scrollbarColor(Std.parseInt(subObject.att.resolve("scrollbarColor")));
				scrollbar.set_scrollbarColorAlpha(Std.parseFloat(subObject.att.resolve("scrollbarColorAlpha")));
				scrollbar.set_thumbColor(Std.parseInt(subObject.att.resolve("thumbColor")));
				scrollbar.set_thumbColorAlpha(Std.parseFloat(subObject.att.resolve("thumbColorAlpha")));
				scrollbar.set_autoHideThumb(subObject.att.resolve("autoHideThumb") == "true"?true:false);
				this._dashboardData.get_indexPanel().get_scrollbars().push(scrollbar);
			}
		}
		this._dashboardData.get_indexPanel().set_menuData(new Array());
		this._dashboardData.get_indexPanel().set_texts(new haxe.ds.StringMap());
		var i = 0;
		var $it1 = data.nodes.resolve("content").iterator();
		while( $it1.hasNext() ) {
			var content = $it1.next();
			if(content.att.resolve("type") == "menu") {
				var $it2 = content.nodes.resolve("content").iterator();
				while( $it2.hasNext() ) {
					var menuItem = $it2.next();
					var menuData = new unbox.framework.model.vo.MenuItemData();
					menuData.set_title(menuItem);
					menuData.set_pageId(menuItem.att.resolve("pageID"));
					menuData.set_lastPageId(menuItem.att.resolve("lastPageID"));
					menuData.set_submenuItems(new Array());
					var $it3 = menuItem.nodes.resolve("content").iterator();
					while( $it3.hasNext() ) {
						var submenuItem = $it3.next();
						var submenuItemData = new unbox.framework.model.vo.SubMenuItemData();
						submenuItemData.set_title(submenuItem);
						submenuItemData.set_pageId(submenuItem.att.resolve("pageID"));
						submenuItemData.set_lastPageId(submenuItem.att.resolve("lastPageID"));
						menuData.get_submenuItems().push(submenuItemData);
					}
					this._dashboardData.get_indexPanel().get_menuData().push(menuData);
				}
			} else if(content.att.resolve("type") == "text") {
				var $it4 = content.nodes.resolve("content").iterator();
				while( $it4.hasNext() ) {
					var textNode = $it4.next();
					if(textNode.has.resolve("instanceName")) {
						var this1 = this._dashboardData.get_indexPanel().get_texts();
						var key = textNode.att.resolve("instanceName");
						this1.set(key,textNode);
					} else {
						var this2 = this._dashboardData.get_indexPanel().get_texts();
						this2.set("text_" + i,textNode);
						i++;
					}
				}
			}
		}
	}
	,parseSearchPanel: function(data) {
		haxe.Log.trace("UIModel.parseSearchPanel",{ fileName : "UIModel.hx", lineNumber : 316, className : "unbox.framework.model.UIModel", methodName : "parseSearchPanel"});
		this._dashboardData.set_searchPanel(new unbox.framework.model.vo.SearchPanelData());
		this._dashboardData.get_searchPanel().set_title(data.att.resolve("title"));
		this._dashboardData.get_searchPanel().set_x(Std.parseFloat(data.att.resolve("x")));
		this._dashboardData.get_searchPanel().set_y(Std.parseFloat(data.att.resolve("y")));
		this._dashboardData.get_searchPanel().set_summaryLength(Std.parseInt(data.att.resolve("summaryLength")));
		this._dashboardData.get_searchPanel().viewClass = data.att.resolve("viewClass");
		this._dashboardData.get_searchPanel().set_list(new unbox.framework.model.vo.ListData());
		this._dashboardData.get_searchPanel().set_scrollbar(new unbox.framework.model.vo.ScrollbarData());
		this._dashboardData.get_searchPanel().set_texts(new haxe.ds.StringMap());
		var $it0 = data.nodes.resolve("object").iterator();
		while( $it0.hasNext() ) {
			var object = $it0.next();
			if(object.att.resolve("type") == "List") {
				this._dashboardData.get_searchPanel().get_list().set_name(object.att.resolve("name"));
				this._dashboardData.get_searchPanel().get_list().set_x(Std.parseFloat(object.att.resolve("x")));
				this._dashboardData.get_searchPanel().get_list().set_y(Std.parseFloat(object.att.resolve("y")));
				this._dashboardData.get_searchPanel().get_list().set_width(Std.parseInt(object.att.resolve("width")));
				this._dashboardData.get_searchPanel().get_list().set_height(Std.parseInt(object.att.resolve("height")));
				this._dashboardData.get_searchPanel().get_list().set_columns(Std.parseInt(object.att.resolve("columns")));
				this._dashboardData.get_searchPanel().get_list().set_rows(Std.parseInt(object.att.resolve("rows")));
				this._dashboardData.get_searchPanel().get_list().set_spacing(Std.parseInt(object.att.resolve("spacing")));
				this._dashboardData.get_searchPanel().get_list().viewClass = object.att.resolve("viewClass");
			} else if(object.att.resolve("type") == "Scrollbar") {
				this._dashboardData.get_searchPanel().get_scrollbar().set_x(Std.parseFloat(object.att.resolve("x")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_y(Std.parseFloat(object.att.resolve("y")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_width(Std.parseInt(object.att.resolve("width")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_height(Std.parseInt(object.att.resolve("height")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_scrollbarColor(Std.parseInt(object.att.resolve("scrollbarColor")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_scrollbarColorAlpha(Std.parseFloat(object.att.resolve("scrollbarColorAlpha")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_thumbColor(Std.parseInt(object.att.resolve("thumbColor")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_thumbColorAlpha(Std.parseFloat(object.att.resolve("thumbColorAlpha")));
				this._dashboardData.get_searchPanel().get_scrollbar().set_autoHideThumb(object.att.resolve("autoHideThumb") == "true"?true:false);
			}
		}
		var i = 0;
		var $it1 = data.node.resolve("content").nodes.resolve("content").iterator();
		while( $it1.hasNext() ) {
			var text = $it1.next();
			if(text.has.resolve("instanceName")) {
				var this1 = this._dashboardData.get_searchPanel().get_texts();
				var key = text.att.resolve("instanceName");
				this1.set(key,text);
			} else {
				var this2 = this._dashboardData.get_searchPanel().get_texts();
				this2.set("text_" + i,text);
				i++;
			}
		}
	}
	,parseSearchBox: function(data) {
		haxe.Log.trace("UIModel.parseSearchBox",{ fileName : "UIModel.hx", lineNumber : 366, className : "unbox.framework.model.UIModel", methodName : "parseSearchBox"});
		this._dashboardData.set_searchBox(new unbox.framework.model.vo.SearchBoxData());
		this._dashboardData.get_searchBox().set_title(data.att.resolve("title"));
		this._dashboardData.get_searchBox().set_x(Std.parseFloat(data.att.resolve("x")));
		this._dashboardData.get_searchBox().set_y(Std.parseFloat(data.att.resolve("y")));
		this._dashboardData.get_searchBox().set_loadTitle(data.att.resolve("loadTitle"));
		this._dashboardData.get_searchBox().set_minChars(Std.parseInt(data.att.resolve("minChars")));
		this._dashboardData.get_searchBox().viewClass = data.att.resolve("viewClass");
		this._dashboardData.get_searchBox().set_texts(new haxe.ds.StringMap());
		if(data.att.resolve("alwaysVisible") == "true") this._dashboardData.get_searchBox().alwaysVisible = true; else this._dashboardData.get_searchBox().alwaysVisible = false;
		var i = 0;
		var $it0 = data.node.resolve("content").nodes.resolve("content").iterator();
		while( $it0.hasNext() ) {
			var text = $it0.next();
			if(text.has.resolve("instanceName")) {
				var this1 = this._dashboardData.get_searchBox().get_texts();
				var key = text.att.resolve("instanceName");
				this1.set(key,text);
			} else {
				var this2 = this._dashboardData.get_searchBox().get_texts();
				this2.set("text_" + i,text);
				i++;
			}
		}
	}
	,parseTooltip: function(data) {
		haxe.Log.trace("UIModel.parseTooltip",{ fileName : "UIModel.hx", lineNumber : 390, className : "unbox.framework.model.UIModel", methodName : "parseTooltip"});
		this._dashboardData.set_tooltips(new unbox.framework.model.vo.TooltipData());
		this._dashboardData.get_tooltips().set_hook(data.att.resolve("hook") == "true"?true:false);
		this._dashboardData.get_tooltips().set_hookSize(Std.parseInt(data.att.resolve("hookSize")));
		this._dashboardData.get_tooltips().set_autoSize(data.att.resolve("autoSize") == "true"?true:false);
		this._dashboardData.get_tooltips().set_followCursor(data.att.resolve("followCursor") == "true"?true:false);
		this._dashboardData.get_tooltips().set_align(data.att.resolve("align"));
		this._dashboardData.get_tooltips().set_cornerRadius(Std.parseFloat(data.att.resolve("cornerRadius")));
		this._dashboardData.get_tooltips().set_showDelay(Std.parseInt(data.att.resolve("showDelay")));
		this._dashboardData.get_tooltips().set_hideDelay(Std.parseInt(data.att.resolve("hideDelay")));
		this._dashboardData.get_tooltips().set_bgAlpha(Std.parseFloat(data.att.resolve("bgAlpha")));
		this._dashboardData.get_tooltips().set_width(Std.parseInt(data.att.resolve("width")));
		this._dashboardData.get_tooltips().set_padding(Std.parseFloat(data.att.resolve("padding")));
		this._dashboardData.get_tooltips().set_colors(new Array());
		var rawColors = data.att.resolve("colors").split(",");
		this._dashboardData.get_tooltips().get_colors()[0] = Std.parseInt(rawColors[0]);
		this._dashboardData.get_tooltips().get_colors()[1] = Std.parseInt(rawColors[1]);
		this._dashboardData.get_tooltips().set_items(new haxe.ds.StringMap());
		var i = 0;
		var $it0 = data.nodes.resolve("content").iterator();
		while( $it0.hasNext() ) {
			var tooltip = $it0.next();
			var nodeData;
			if(tooltip.hasNode.resolve("body")) nodeData = tooltip.node.resolve("body").get_innerHTML(); else if(tooltip.hasNode.resolve("title")) nodeData = tooltip.node.resolve("title").get_innerHTML(); else nodeData = "<![CDATA[<p>ERROR:NODATA</p>]]>";
			if(tooltip.has.resolve("instanceName")) {
				var this1 = this._dashboardData.get_tooltips().get_items();
				var key = tooltip.att.resolve("instanceName");
				this1.set(key,nodeData);
			} else {
				var this2 = this._dashboardData.get_tooltips().get_items();
				this2.set("tooltip_" + i,nodeData);
			}
			i++;
		}
	}
	,parseHelpPanel: function(data) {
		haxe.Log.trace("UIModel.parseHelpPanel",{ fileName : "UIModel.hx", lineNumber : 422, className : "unbox.framework.model.UIModel", methodName : "parseHelpPanel"});
		this._dashboardData.set_helpPanel(new unbox.framework.model.vo.HelpPanelData());
		this._dashboardData.get_helpPanel().set_depth(Std.parseInt(data.att.resolve("depth")));
		this._dashboardData.get_helpPanel().set_title(data.att.resolve("title"));
		this._dashboardData.get_helpPanel().set_x(Std.parseFloat(data.att.resolve("x")));
		this._dashboardData.get_helpPanel().set_y(Std.parseFloat(data.att.resolve("y")));
		this._dashboardData.get_helpPanel().set_width(Std.parseInt(data.att.resolve("width")));
		this._dashboardData.get_helpPanel().set_height(Std.parseInt(data.att.resolve("height")));
		this._dashboardData.get_helpPanel().viewClass = data.att.resolve("viewClass");
		this._dashboardData.get_helpPanel().set_texts(new haxe.ds.StringMap());
		var i = 0;
		var $it0 = data.node.resolve("content").nodes.resolve("content").iterator();
		while( $it0.hasNext() ) {
			var text = $it0.next();
			if(text.has.resolve("instanceName")) {
				var this1 = this._dashboardData.get_helpPanel().get_texts();
				var key = text.att.resolve("instanceName");
				this1.set(key,text);
			} else {
				var this2 = this._dashboardData.get_helpPanel().get_texts();
				this2.set("text_" + i,text);
				i++;
			}
		}
	}
	,get_progressMeterData: function() {
		return this._progressMeterData;
	}
	,set_progressMeterData: function(value) {
		return this._progressMeterData = value;
	}
	,get_navBarData: function() {
		return this._navBarData;
	}
	,set_navBarData: function(value) {
		return this._navBarData = value;
	}
	,get_dashboardData: function() {
		return this._dashboardData;
	}
	,set_dashboardData: function(value) {
		return this._dashboardData = value;
	}
	,__class__: unbox.framework.model.UIModel
	,__properties__: {set_dashboardData:"set_dashboardData",get_dashboardData:"get_dashboardData",set_navBarData:"set_navBarData",get_navBarData:"get_navBarData",set_progressMeterData:"set_progressMeterData",get_progressMeterData:"get_progressMeterData"}
};
unbox.framework.model.vo = {};
unbox.framework.model.vo.IData = function() { };
$hxClasses["unbox.framework.model.vo.IData"] = unbox.framework.model.vo.IData;
unbox.framework.model.vo.IData.__name__ = ["unbox","framework","model","vo","IData"];
unbox.framework.model.vo.IData.prototype = {
	__class__: unbox.framework.model.vo.IData
};
unbox.framework.model.vo.AboutPanelData = function() {
};
$hxClasses["unbox.framework.model.vo.AboutPanelData"] = unbox.framework.model.vo.AboutPanelData;
unbox.framework.model.vo.AboutPanelData.__name__ = ["unbox","framework","model","vo","AboutPanelData"];
unbox.framework.model.vo.AboutPanelData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.AboutPanelData.prototype = {
	toString: function() {
		return "### AboutPanelData ### \ntitle : " + this._title + "\nx : " + this._x + "\ny : " + this._y + "\ntexts : " + this._texts.toString() + "\n### END ###";
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,__class__: unbox.framework.model.vo.AboutPanelData
	,__properties__: {set_texts:"set_texts",get_texts:"get_texts",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_title:"set_title",get_title:"get_title"}
};
unbox.framework.model.vo.BookmarkPanelData = function() {
};
$hxClasses["unbox.framework.model.vo.BookmarkPanelData"] = unbox.framework.model.vo.BookmarkPanelData;
unbox.framework.model.vo.BookmarkPanelData.__name__ = ["unbox","framework","model","vo","BookmarkPanelData"];
unbox.framework.model.vo.BookmarkPanelData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.BookmarkPanelData.prototype = {
	toString: function() {
		return " ### BookmarkPanelData ### \ntitle : " + this._title + "\nx : " + this._x + "\ny : " + this._y + "\nlist : " + this._list.toString() + "\nscrollBar : " + this._scrollbar.toString() + "\ntexts : " + this._texts.toString() + "\n### END ###";
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_list: function() {
		return this._list;
	}
	,set_list: function(value) {
		return this._list = value;
	}
	,get_scrollbar: function() {
		return this._scrollbar;
	}
	,set_scrollbar: function(value) {
		return this._scrollbar = value;
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,__class__: unbox.framework.model.vo.BookmarkPanelData
	,__properties__: {set_texts:"set_texts",get_texts:"get_texts",set_scrollbar:"set_scrollbar",get_scrollbar:"get_scrollbar",set_list:"set_list",get_list:"get_list",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_title:"set_title",get_title:"get_title"}
};
unbox.framework.model.vo.ConfigData = function() {
};
$hxClasses["unbox.framework.model.vo.ConfigData"] = unbox.framework.model.vo.ConfigData;
unbox.framework.model.vo.ConfigData.__name__ = ["unbox","framework","model","vo","ConfigData"];
unbox.framework.model.vo.ConfigData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.ConfigData.prototype = {
	toString: function() {
		return "### ConfigData ### \nbrowseMode : " + ("" + this._browseMode) + "\ndebugActive : " + ("" + this._debugActive) + "\ndataServiceType : " + this._dataServiceType + "\n### END ###";
	}
	,get_browseMode: function() {
		return this._browseMode;
	}
	,set_browseMode: function(value) {
		return this._browseMode = value;
	}
	,get_accessible: function() {
		return this._accessible;
	}
	,set_accessible: function(value) {
		return this._accessible = value;
	}
	,get_debugActive: function() {
		return this._debugActive;
	}
	,set_debugActive: function(value) {
		return this._debugActive = value;
	}
	,get_dataServiceType: function() {
		return this._dataServiceType;
	}
	,set_dataServiceType: function(value) {
		return this._dataServiceType = value;
	}
	,__class__: unbox.framework.model.vo.ConfigData
	,__properties__: {set_dataServiceType:"set_dataServiceType",get_dataServiceType:"get_dataServiceType",set_debugActive:"set_debugActive",get_debugActive:"get_debugActive",set_accessible:"set_accessible",get_accessible:"get_accessible",set_browseMode:"set_browseMode",get_browseMode:"get_browseMode"}
};
unbox.framework.model.vo.CustomData = function(dataString) {
	this._maxPoints = [0];
	this._userPoints = [-1];
	this._lessonStatus = [0];
	this._bookmarks = new Array();
	if(dataString != null && dataString != "") this.parseData(dataString);
};
$hxClasses["unbox.framework.model.vo.CustomData"] = unbox.framework.model.vo.CustomData;
unbox.framework.model.vo.CustomData.__name__ = ["unbox","framework","model","vo","CustomData"];
unbox.framework.model.vo.CustomData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.CustomData.prototype = {
	setLessonStatus: function(lessonId,status) {
		this._lessonStatus[lessonId] = status;
	}
	,getLessonStatus: function(lessonId) {
		return this._lessonStatus[lessonId];
	}
	,setPoints: function(value,index) {
		this.get_userPoints()[index] = value;
	}
	,getPoints: function(index) {
		return this.get_userPoints()[index];
	}
	,getTotalPoints: function() {
		var total = 0;
		var _g1 = 0;
		var _g = this.get_userPoints().length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.get_userPoints()[i] > 0) total += this.get_userPoints()[i];
		}
		return total;
	}
	,getTotalMaxPoints: function() {
		var total = 0;
		var _g1 = 0;
		var _g = this.get_maxPoints().length;
		while(_g1 < _g) {
			var i = _g1++;
			total += this.get_maxPoints()[i];
		}
		return total;
	}
	,getGrade: function() {
		var grade = Std.int(100 * this.getTotalPoints() / this.getTotalMaxPoints());
		return grade;
	}
	,isTestDone: function(index) {
		return this.get_userPoints()[index] > -1;
	}
	,get_maxPoints: function() {
		return this._maxPoints;
	}
	,set_maxPoints: function(value) {
		this._maxPoints = value;
		return value;
	}
	,get_userPoints: function() {
		return this._userPoints;
	}
	,set_userPoints: function(value) {
		this._userPoints = value;
		return value;
	}
	,get_lessonStatus: function() {
		return this._lessonStatus;
	}
	,set_lessonStatus: function(value) {
		this._lessonStatus = value;
		return value;
	}
	,get_bookmarks: function() {
		return this._bookmarks;
	}
	,set_bookmarks: function(value) {
		this._bookmarks = value;
		return value;
	}
	,parseData: function(dataStr) {
		var retObj = JSON.parse(dataStr);
		var _g = 0;
		var _g1 = Reflect.fields(retObj);
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			Reflect.setField(this,i,Reflect.field(retObj,i));
		}
	}
	,toString: function() {
		return "maxPoints : " + Std.string(this._maxPoints) + "\nuserPoints : " + Std.string(this._userPoints) + "\nlessonStatus : " + Std.string(this._lessonStatus) + "\nbookmarks : " + Std.string(this._bookmarks);
	}
	,__class__: unbox.framework.model.vo.CustomData
	,__properties__: {set_bookmarks:"set_bookmarks",get_bookmarks:"get_bookmarks",set_lessonStatus:"set_lessonStatus",get_lessonStatus:"get_lessonStatus",set_userPoints:"set_userPoints",get_userPoints:"get_userPoints",set_maxPoints:"set_maxPoints",get_maxPoints:"get_maxPoints"}
};
unbox.framework.model.vo.DashboardData = function() {
};
$hxClasses["unbox.framework.model.vo.DashboardData"] = unbox.framework.model.vo.DashboardData;
unbox.framework.model.vo.DashboardData.__name__ = ["unbox","framework","model","vo","DashboardData"];
unbox.framework.model.vo.DashboardData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.DashboardData.prototype = {
	toString: function() {
		return "";
	}
	,get_aboutPanel: function() {
		return this._aboutPanel;
	}
	,set_aboutPanel: function(value) {
		return this._aboutPanel = value;
	}
	,get_bookmarkPanel: function() {
		return this._bookmarkPanel;
	}
	,set_bookmarkPanel: function(value) {
		return this._bookmarkPanel = value;
	}
	,get_indexPanel: function() {
		return this._indexPanel;
	}
	,set_indexPanel: function(value) {
		return this._indexPanel = value;
	}
	,get_searchPanel: function() {
		return this._searchPanel;
	}
	,set_searchPanel: function(value) {
		return this._searchPanel = value;
	}
	,get_searchBox: function() {
		return this._searchBox;
	}
	,set_searchBox: function(value) {
		return this._searchBox = value;
	}
	,get_helpPanel: function() {
		return this._helpPanel;
	}
	,set_helpPanel: function(value) {
		return this._helpPanel = value;
	}
	,get_tooltips: function() {
		return this._tooltips;
	}
	,set_tooltips: function(value) {
		return this._tooltips = value;
	}
	,__class__: unbox.framework.model.vo.DashboardData
	,__properties__: {set_tooltips:"set_tooltips",get_tooltips:"get_tooltips",set_helpPanel:"set_helpPanel",get_helpPanel:"get_helpPanel",set_searchBox:"set_searchBox",get_searchBox:"get_searchBox",set_searchPanel:"set_searchPanel",get_searchPanel:"get_searchPanel",set_indexPanel:"set_indexPanel",get_indexPanel:"get_indexPanel",set_bookmarkPanel:"set_bookmarkPanel",get_bookmarkPanel:"get_bookmarkPanel",set_aboutPanel:"set_aboutPanel",get_aboutPanel:"get_aboutPanel"}
};
unbox.framework.model.vo.EbookData = function() {
	this._comments = "comments";
	this._credit = "credit";
	this._entry = "resume";
	this._exit = "suspend";
	this._launch_data = "";
	this._lesson_location = "";
	this._lesson_mode = "normal";
	this._lesson_status = "not attempted";
	this._scoreMax = 100;
	this._scoreMin = 0;
	this._session_time = "0000:00:00.00";
	this._student_id = "";
	this._student_name = "TREINANDO";
	this._suspend_data = "";
	this._total_time = "0000:00:00.00";
};
$hxClasses["unbox.framework.model.vo.EbookData"] = unbox.framework.model.vo.EbookData;
unbox.framework.model.vo.EbookData.__name__ = ["unbox","framework","model","vo","EbookData"];
unbox.framework.model.vo.EbookData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.EbookData.prototype = {
	get_comments: function() {
		return this._comments;
	}
	,set_comments: function(value) {
		if(value != null) this._comments = value;
		return value;
	}
	,get_credit: function() {
		return this._credit;
	}
	,set_credit: function(value) {
		if(value != null) this._credit = value;
		return value;
	}
	,get_entry: function() {
		return this._entry;
	}
	,set_entry: function(value) {
		if(value != null) this._entry = value;
		return value;
	}
	,get_exit: function() {
		return this._exit;
	}
	,set_exit: function(value) {
		if(value != null) this._exit = value;
		return value;
	}
	,get_launch_data: function() {
		return this._launch_data;
	}
	,set_launch_data: function(value) {
		if(value != null) this._launch_data = value;
		return value;
	}
	,get_lesson_location: function() {
		return this._lesson_location;
	}
	,set_lesson_location: function(value) {
		if(value != null) this._lesson_location = value;
		return value;
	}
	,get_lesson_mode: function() {
		return this._lesson_mode;
	}
	,set_lesson_mode: function(value) {
		if(value != null) this._lesson_mode = value;
		return value;
	}
	,get_lesson_status: function() {
		return this._lesson_status;
	}
	,set_lesson_status: function(value) {
		if(value != null) this._lesson_status = value;
		return value;
	}
	,get_scoreMax: function() {
		return this._scoreMax;
	}
	,set_scoreMax: function(value) {
		if(!Math.isNaN(value)) this._scoreMax = value;
		return value;
	}
	,get_scoreMin: function() {
		return this._scoreMin;
	}
	,set_scoreMin: function(value) {
		if(!Math.isNaN(value)) this._scoreMin = value;
		return value;
	}
	,get_scoreRaw: function() {
		return this._scoreRaw;
	}
	,set_scoreRaw: function(value) {
		if(!Math.isNaN(value)) this._scoreRaw = value;
		return value;
	}
	,get_session_time: function() {
		return this._session_time;
	}
	,set_session_time: function(value) {
		if(value != null) this._session_time = value;
		return value;
	}
	,get_student_id: function() {
		return this._student_id;
	}
	,set_student_id: function(value) {
		if(value != null) this._student_id = value;
		return value;
	}
	,get_student_name: function() {
		return this._student_name;
	}
	,set_student_name: function(value) {
		if(value != null) this._student_name = value;
		return value;
	}
	,get_suspend_data: function() {
		return this._suspend_data;
	}
	,set_suspend_data: function(value) {
		if(value != null) this._suspend_data = value;
		return value;
	}
	,get_total_time: function() {
		return this._total_time;
	}
	,set_total_time: function(value) {
		if(value != null) this._total_time = value;
		return value;
	}
	,toString: function() {
		var str = "";
		return str;
	}
	,__class__: unbox.framework.model.vo.EbookData
	,__properties__: {set_total_time:"set_total_time",get_total_time:"get_total_time",set_suspend_data:"set_suspend_data",get_suspend_data:"get_suspend_data",set_student_name:"set_student_name",get_student_name:"get_student_name",set_student_id:"set_student_id",get_student_id:"get_student_id",set_session_time:"set_session_time",get_session_time:"get_session_time",set_scoreRaw:"set_scoreRaw",get_scoreRaw:"get_scoreRaw",set_scoreMin:"set_scoreMin",get_scoreMin:"get_scoreMin",set_scoreMax:"set_scoreMax",get_scoreMax:"get_scoreMax",set_lesson_status:"set_lesson_status",get_lesson_status:"get_lesson_status",set_lesson_mode:"set_lesson_mode",get_lesson_mode:"get_lesson_mode",set_lesson_location:"set_lesson_location",get_lesson_location:"get_lesson_location",set_launch_data:"set_launch_data",get_launch_data:"get_launch_data",set_exit:"set_exit",get_exit:"get_exit",set_entry:"set_entry",get_entry:"get_entry",set_credit:"set_credit",get_credit:"get_credit",set_comments:"set_comments",get_comments:"get_comments"}
};
unbox.framework.model.vo.HelpPanelData = function() {
};
$hxClasses["unbox.framework.model.vo.HelpPanelData"] = unbox.framework.model.vo.HelpPanelData;
unbox.framework.model.vo.HelpPanelData.__name__ = ["unbox","framework","model","vo","HelpPanelData"];
unbox.framework.model.vo.HelpPanelData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.HelpPanelData.prototype = {
	toString: function() {
		return "### HelpPanelData ### \ndepth : " + this._depth + "\ntitle : " + this._title + "\nx : " + this._x + "\ny : " + this._y + "\nwidth : " + this._width + "\nheight : " + this._height + "\ntexts : " + this._texts.toString() + "\n### END ###";
	}
	,get_depth: function() {
		return this._depth;
	}
	,set_depth: function(value) {
		return this._depth = value;
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_width: function() {
		return this._width;
	}
	,set_width: function(value) {
		return this._width = value;
	}
	,get_height: function() {
		return this._height;
	}
	,set_height: function(value) {
		return this._height = value;
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,__class__: unbox.framework.model.vo.HelpPanelData
	,__properties__: {set_texts:"set_texts",get_texts:"get_texts",set_height:"set_height",get_height:"get_height",set_width:"set_width",get_width:"get_width",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_title:"set_title",get_title:"get_title",set_depth:"set_depth",get_depth:"get_depth"}
};
unbox.framework.model.vo.IndexPanelData = function() {
};
$hxClasses["unbox.framework.model.vo.IndexPanelData"] = unbox.framework.model.vo.IndexPanelData;
unbox.framework.model.vo.IndexPanelData.__name__ = ["unbox","framework","model","vo","IndexPanelData"];
unbox.framework.model.vo.IndexPanelData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.IndexPanelData.prototype = {
	toString: function() {
		return "### IndexPanelData ### \ntitle : " + this._title + "\nx : " + this._x + "\ny : " + this._y + "\nmenuLists : " + this._menuLists.toString() + "\nscrollbars : " + this._scrollbars.toString() + "\ntexts : " + this._texts.toString() + "\nmenuData : " + this._menuData.toString() + "\n### END ###";
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,get_menuLists: function() {
		return this._menuLists;
	}
	,set_menuLists: function(value) {
		return this._menuLists = value;
	}
	,get_scrollbars: function() {
		return this._scrollbars;
	}
	,set_scrollbars: function(value) {
		return this._scrollbars = value;
	}
	,get_menuData: function() {
		return this._menuData;
	}
	,set_menuData: function(value) {
		return this._menuData = value;
	}
	,__class__: unbox.framework.model.vo.IndexPanelData
	,__properties__: {set_menuData:"set_menuData",get_menuData:"get_menuData",set_scrollbars:"set_scrollbars",get_scrollbars:"get_scrollbars",set_menuLists:"set_menuLists",get_menuLists:"get_menuLists",set_texts:"set_texts",get_texts:"get_texts",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_title:"set_title",get_title:"get_title"}
};
unbox.framework.model.vo.ListData = function() {
};
$hxClasses["unbox.framework.model.vo.ListData"] = unbox.framework.model.vo.ListData;
unbox.framework.model.vo.ListData.__name__ = ["unbox","framework","model","vo","ListData"];
unbox.framework.model.vo.ListData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.ListData.prototype = {
	toString: function() {
		return "### ListData ### \nname : " + this._name + "\nx : " + this._x + "\ny : " + this._y + "\nwidth : " + this._width + "\nheight : " + this._height + "\ncolumns : " + this._columns + "\nrows : " + this._rows + "\nspacing : " + this._spacing + "\n### END ###";
	}
	,get_name: function() {
		return this._name;
	}
	,set_name: function(value) {
		return this._name = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_width: function() {
		return this._width;
	}
	,set_width: function(value) {
		return this._width = value;
	}
	,get_height: function() {
		return this._height;
	}
	,set_height: function(value) {
		return this._height = value;
	}
	,get_columns: function() {
		return this._columns;
	}
	,set_columns: function(value) {
		return this._columns = value;
	}
	,get_rows: function() {
		return this._rows;
	}
	,set_rows: function(value) {
		return this._rows = value;
	}
	,get_spacing: function() {
		return this._spacing;
	}
	,set_spacing: function(value) {
		return this._spacing = value;
	}
	,__class__: unbox.framework.model.vo.ListData
	,__properties__: {set_spacing:"set_spacing",get_spacing:"get_spacing",set_rows:"set_rows",get_rows:"get_rows",set_columns:"set_columns",get_columns:"get_columns",set_height:"set_height",get_height:"get_height",set_width:"set_width",get_width:"get_width",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_name:"set_name",get_name:"get_name"}
};
unbox.framework.model.vo.MenuItemData = function() {
};
$hxClasses["unbox.framework.model.vo.MenuItemData"] = unbox.framework.model.vo.MenuItemData;
unbox.framework.model.vo.MenuItemData.__name__ = ["unbox","framework","model","vo","MenuItemData"];
unbox.framework.model.vo.MenuItemData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.MenuItemData.prototype = {
	toString: function() {
		return "### MenuItemData ### \npageId : " + this._pageId + "\nlastPageId : " + this._lastPageId + "\ntitle : " + Std.string(this._title) + "\nsubmenuItems : " + this._submenuItems.toString() + "\n### END ###";
	}
	,get_pageId: function() {
		return this._pageId;
	}
	,set_pageId: function(value) {
		return this._pageId = value;
	}
	,get_lastPageId: function() {
		return this._lastPageId;
	}
	,set_lastPageId: function(value) {
		return this._lastPageId = value;
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_submenuItems: function() {
		return this._submenuItems;
	}
	,set_submenuItems: function(value) {
		return this._submenuItems = value;
	}
	,__class__: unbox.framework.model.vo.MenuItemData
	,__properties__: {set_submenuItems:"set_submenuItems",get_submenuItems:"get_submenuItems",set_title:"set_title",get_title:"get_title",set_lastPageId:"set_lastPageId",get_lastPageId:"get_lastPageId",set_pageId:"set_pageId",get_pageId:"get_pageId"}
};
unbox.framework.model.vo.NavBarData = function() {
};
$hxClasses["unbox.framework.model.vo.NavBarData"] = unbox.framework.model.vo.NavBarData;
unbox.framework.model.vo.NavBarData.__name__ = ["unbox","framework","model","vo","NavBarData"];
unbox.framework.model.vo.NavBarData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.NavBarData.prototype = {
	toString: function() {
		return "### NavBarData ###" + "\ninstanceName : " + this._instanceName + "\nposition : " + this._x + "," + this._y + "\ntexts : " + this._texts.toString() + "\ntooltips : " + this._tooltips.toString() + "\n### END ###";
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,get_tooltips: function() {
		return this._tooltips;
	}
	,set_tooltips: function(value) {
		return this._tooltips = value;
	}
	,get_instanceName: function() {
		return this._instanceName;
	}
	,set_instanceName: function(value) {
		return this._instanceName = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,__class__: unbox.framework.model.vo.NavBarData
	,__properties__: {set_x:"set_x",get_x:"get_x",set_y:"set_y",get_y:"get_y",set_instanceName:"set_instanceName",get_instanceName:"get_instanceName",set_tooltips:"set_tooltips",get_tooltips:"get_tooltips",set_texts:"set_texts",get_texts:"get_texts"}
};
unbox.framework.model.vo.PageData = function() {
};
$hxClasses["unbox.framework.model.vo.PageData"] = unbox.framework.model.vo.PageData;
unbox.framework.model.vo.PageData.__name__ = ["unbox","framework","model","vo","PageData"];
unbox.framework.model.vo.PageData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.PageData.prototype = {
	toString: function() {
		return "\n### PageData ###\n\t • id : " + this._id + "\n\t • src : " + this._src + "\n\t • title : " + this._title + "\n\t • localIndex : " + this._localIndex + "\n\t • globalIndex : " + this._globalIndex + "\n\t • moduleId : " + this._moduleId + "\n\t • moduleIndex : " + this._moduleIndex + "\n\t • numPagesInModule : " + this._numPagesInModule + "\n\t • contentUrl : " + this._contentUrls.toString() + "\n### END ###";
	}
	,get_id: function() {
		return this._id;
	}
	,set_id: function(value) {
		return this._id = value;
	}
	,get_src: function() {
		return this._src;
	}
	,set_src: function(value) {
		return this._src = value;
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_localIndex: function() {
		return this._localIndex;
	}
	,set_localIndex: function(value) {
		return this._localIndex = value;
	}
	,get_globalIndex: function() {
		return this._globalIndex;
	}
	,set_globalIndex: function(value) {
		return this._globalIndex = value;
	}
	,get_numPagesInModule: function() {
		return this._numPagesInModule;
	}
	,set_numPagesInModule: function(value) {
		return this._numPagesInModule = value;
	}
	,get_moduleId: function() {
		return this._moduleId;
	}
	,set_moduleId: function(value) {
		return this._moduleId = value;
	}
	,get_moduleIndex: function() {
		return this._moduleIndex;
	}
	,set_moduleIndex: function(value) {
		return this._moduleIndex = value;
	}
	,get_contentUrls: function() {
		return this._contentUrls;
	}
	,set_contentUrls: function(value) {
		return this._contentUrls = value;
	}
	,get_pageTransitionIn: function() {
		return this._pageTransitionIn;
	}
	,set_pageTransitionIn: function(value) {
		return this._pageTransitionIn = value;
	}
	,get_showProgress: function() {
		return this._showProgress;
	}
	,set_showProgress: function(value) {
		return this._showProgress = value;
	}
	,get_pageTransitionOut: function() {
		return this._pageTransitionOut;
	}
	,set_pageTransitionOut: function(value) {
		return this._pageTransitionOut = value;
	}
	,get_contentTransitionIn: function() {
		return this._contentTransitionIn;
	}
	,set_contentTransitionIn: function(value) {
		return this._contentTransitionIn = value;
	}
	,get_contentTransitionOut: function() {
		return this._contentTransitionOut;
	}
	,set_contentTransitionOut: function(value) {
		return this._contentTransitionOut = value;
	}
	,get_pageProps: function() {
		return this._pageProps;
	}
	,set_pageProps: function(value) {
		return this._pageProps = value;
	}
	,get_assets: function() {
		return this._assets;
	}
	,set_assets: function(value) {
		return this._assets = value;
	}
	,__class__: unbox.framework.model.vo.PageData
	,__properties__: {set_assets:"set_assets",get_assets:"get_assets",set_pageProps:"set_pageProps",get_pageProps:"get_pageProps",set_contentTransitionOut:"set_contentTransitionOut",get_contentTransitionOut:"get_contentTransitionOut",set_contentTransitionIn:"set_contentTransitionIn",get_contentTransitionIn:"get_contentTransitionIn",set_pageTransitionOut:"set_pageTransitionOut",get_pageTransitionOut:"get_pageTransitionOut",set_showProgress:"set_showProgress",get_showProgress:"get_showProgress",set_pageTransitionIn:"set_pageTransitionIn",get_pageTransitionIn:"get_pageTransitionIn",set_contentUrls:"set_contentUrls",get_contentUrls:"get_contentUrls",set_moduleIndex:"set_moduleIndex",get_moduleIndex:"get_moduleIndex",set_moduleId:"set_moduleId",get_moduleId:"get_moduleId",set_numPagesInModule:"set_numPagesInModule",get_numPagesInModule:"get_numPagesInModule",set_globalIndex:"set_globalIndex",get_globalIndex:"get_globalIndex",set_localIndex:"set_localIndex",get_localIndex:"get_localIndex",set_title:"set_title",get_title:"get_title",set_src:"set_src",get_src:"get_src",set_id:"set_id",get_id:"get_id"}
};
unbox.framework.model.vo.PageDataType = $hxClasses["unbox.framework.model.vo.PageDataType"] = { __ename__ : ["unbox","framework","model","vo","PageDataType"], __constructs__ : ["Scorm","Navigation","Custom"] };
unbox.framework.model.vo.PageDataType.Scorm = ["Scorm",0];
unbox.framework.model.vo.PageDataType.Scorm.toString = $estr;
unbox.framework.model.vo.PageDataType.Scorm.__enum__ = unbox.framework.model.vo.PageDataType;
unbox.framework.model.vo.PageDataType.Navigation = ["Navigation",1];
unbox.framework.model.vo.PageDataType.Navigation.toString = $estr;
unbox.framework.model.vo.PageDataType.Navigation.__enum__ = unbox.framework.model.vo.PageDataType;
unbox.framework.model.vo.PageDataType.Custom = ["Custom",2];
unbox.framework.model.vo.PageDataType.Custom.toString = $estr;
unbox.framework.model.vo.PageDataType.Custom.__enum__ = unbox.framework.model.vo.PageDataType;
unbox.framework.model.vo.ProgressMeterData = function() {
};
$hxClasses["unbox.framework.model.vo.ProgressMeterData"] = unbox.framework.model.vo.ProgressMeterData;
unbox.framework.model.vo.ProgressMeterData.__name__ = ["unbox","framework","model","vo","ProgressMeterData"];
unbox.framework.model.vo.ProgressMeterData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.ProgressMeterData.prototype = {
	toString: function() {
		return "### ProgressMeterData ### \nclassName : " + this._className + "\ndepth : " + this._depth + "\ninstanceName : " + this._instanceName + "\nx : " + this._x + "\ny : " + this._y + "\nopenX : " + this._openX + "\nopenY : " + this._openY + "\nwidth : " + this._width + "\nheight : " + this._height + "\nscrollbarColor : " + this._scrollbarColor + "\nscrollbarColorAlpha : " + this._scrollbarColorAlpha + "\nthumbColor : " + this._thumbColor + "\nthumbColorAlpha : " + this._thumbColorAlpha + "\nthumbFillMode : " + ("" + this._thumbFillMode) + "\n### END ###";
	}
	,get_className: function() {
		return this._className;
	}
	,set_className: function(value) {
		return this._className = value;
	}
	,get_depth: function() {
		return this._depth;
	}
	,set_depth: function(value) {
		return this._depth = value;
	}
	,get_instanceName: function() {
		return this._instanceName;
	}
	,set_instanceName: function(value) {
		return this._instanceName = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_openX: function() {
		return this._openX;
	}
	,set_openX: function(value) {
		return this._openX = value;
	}
	,get_openY: function() {
		return this._openY;
	}
	,set_openY: function(value) {
		return this._openY = value;
	}
	,get_width: function() {
		return this._width;
	}
	,set_width: function(value) {
		return this._width = value;
	}
	,get_height: function() {
		return this._height;
	}
	,set_height: function(value) {
		return this._height = value;
	}
	,get_scrollbarColor: function() {
		return this._scrollbarColor;
	}
	,set_scrollbarColor: function(value) {
		return this._scrollbarColor = value;
	}
	,get_scrollbarColorAlpha: function() {
		return this._scrollbarColorAlpha;
	}
	,set_scrollbarColorAlpha: function(value) {
		return this._scrollbarColorAlpha = value;
	}
	,get_thumbColor: function() {
		return this._thumbColor;
	}
	,set_thumbColor: function(value) {
		return this._thumbColor = value;
	}
	,get_thumbColorAlpha: function() {
		return this._thumbColorAlpha;
	}
	,set_thumbColorAlpha: function(value) {
		return this._thumbColorAlpha = value;
	}
	,get_thumbFillMode: function() {
		return this._thumbFillMode;
	}
	,set_thumbFillMode: function(value) {
		return this._thumbFillMode = value;
	}
	,get_autoHideThumb: function() {
		return this._autoHideThumb;
	}
	,set_autoHideThumb: function(value) {
		return this._autoHideThumb = value;
	}
	,__class__: unbox.framework.model.vo.ProgressMeterData
	,__properties__: {set_autoHideThumb:"set_autoHideThumb",get_autoHideThumb:"get_autoHideThumb",set_thumbFillMode:"set_thumbFillMode",get_thumbFillMode:"get_thumbFillMode",set_thumbColorAlpha:"set_thumbColorAlpha",get_thumbColorAlpha:"get_thumbColorAlpha",set_thumbColor:"set_thumbColor",get_thumbColor:"get_thumbColor",set_scrollbarColorAlpha:"set_scrollbarColorAlpha",get_scrollbarColorAlpha:"get_scrollbarColorAlpha",set_scrollbarColor:"set_scrollbarColor",get_scrollbarColor:"get_scrollbarColor",set_height:"set_height",get_height:"get_height",set_width:"set_width",get_width:"get_width",set_openY:"set_openY",get_openY:"get_openY",set_openX:"set_openX",get_openX:"get_openX",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_instanceName:"set_instanceName",get_instanceName:"get_instanceName",set_depth:"set_depth",get_depth:"get_depth",set_className:"set_className",get_className:"get_className"}
};
unbox.framework.model.vo.ScormParams = function() {
};
$hxClasses["unbox.framework.model.vo.ScormParams"] = unbox.framework.model.vo.ScormParams;
unbox.framework.model.vo.ScormParams.__name__ = ["unbox","framework","model","vo","ScormParams"];
unbox.framework.model.vo.ScormParams.prototype = {
	__class__: unbox.framework.model.vo.ScormParams
};
unbox.framework.model.vo.ScrollbarData = function() {
};
$hxClasses["unbox.framework.model.vo.ScrollbarData"] = unbox.framework.model.vo.ScrollbarData;
unbox.framework.model.vo.ScrollbarData.__name__ = ["unbox","framework","model","vo","ScrollbarData"];
unbox.framework.model.vo.ScrollbarData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.ScrollbarData.prototype = {
	toString: function() {
		return "### ScrollbarData ### \nx : " + this._x + "\ny : " + this._y + "\nwidth : " + this._width + "\nheight : " + this._height + "\nscrollbarColor : " + this._scrollbarColor + "\nscrollbarColorAlpha : " + this._scrollbarColorAlpha + "\nthumbColor : " + this._thumbColor + "\nthumbColorAlpha : " + this._thumbColorAlpha + "\nautoHideThumb : " + ("" + this._autoHideThumb) + "\n### END ###";
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_width: function() {
		return this._width;
	}
	,set_width: function(value) {
		return this._width = value;
	}
	,get_height: function() {
		return this._height;
	}
	,set_height: function(value) {
		return this._height = value;
	}
	,get_scrollbarColor: function() {
		return this._scrollbarColor;
	}
	,set_scrollbarColor: function(value) {
		return this._scrollbarColor = value;
	}
	,get_scrollbarColorAlpha: function() {
		return this._scrollbarColorAlpha;
	}
	,set_scrollbarColorAlpha: function(value) {
		return this._scrollbarColorAlpha = value;
	}
	,get_thumbColor: function() {
		return this._thumbColor;
	}
	,set_thumbColor: function(value) {
		return this._thumbColor = value;
	}
	,get_thumbColorAlpha: function() {
		return this._thumbColorAlpha;
	}
	,set_thumbColorAlpha: function(value) {
		return this._thumbColorAlpha = value;
	}
	,get_autoHideThumb: function() {
		return this._autoHideThumb;
	}
	,set_autoHideThumb: function(value) {
		return this._autoHideThumb = value;
	}
	,__class__: unbox.framework.model.vo.ScrollbarData
	,__properties__: {set_autoHideThumb:"set_autoHideThumb",get_autoHideThumb:"get_autoHideThumb",set_thumbColorAlpha:"set_thumbColorAlpha",get_thumbColorAlpha:"get_thumbColorAlpha",set_thumbColor:"set_thumbColor",get_thumbColor:"get_thumbColor",set_scrollbarColorAlpha:"set_scrollbarColorAlpha",get_scrollbarColorAlpha:"get_scrollbarColorAlpha",set_scrollbarColor:"set_scrollbarColor",get_scrollbarColor:"get_scrollbarColor",set_height:"set_height",get_height:"get_height",set_width:"set_width",get_width:"get_width",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x"}
};
unbox.framework.model.vo.SearchBoxData = function() {
};
$hxClasses["unbox.framework.model.vo.SearchBoxData"] = unbox.framework.model.vo.SearchBoxData;
unbox.framework.model.vo.SearchBoxData.__name__ = ["unbox","framework","model","vo","SearchBoxData"];
unbox.framework.model.vo.SearchBoxData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.SearchBoxData.prototype = {
	toString: function() {
		return "### SearchBoxData ### \ntitle : " + this._title + "\nx : " + this._x + "\ny : " + this._y + "\nloadTitle : " + this._loadTitle + "\nminChars : " + this._minChars + "\n### END ###";
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_loadTitle: function() {
		return this._loadTitle;
	}
	,set_loadTitle: function(value) {
		return this._loadTitle = value;
	}
	,get_minChars: function() {
		return this._minChars;
	}
	,set_minChars: function(value) {
		return this._minChars = value;
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,__class__: unbox.framework.model.vo.SearchBoxData
	,__properties__: {set_texts:"set_texts",get_texts:"get_texts",set_minChars:"set_minChars",get_minChars:"get_minChars",set_loadTitle:"set_loadTitle",get_loadTitle:"get_loadTitle",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_title:"set_title",get_title:"get_title"}
};
unbox.framework.model.vo.SearchPanelData = function() {
};
$hxClasses["unbox.framework.model.vo.SearchPanelData"] = unbox.framework.model.vo.SearchPanelData;
unbox.framework.model.vo.SearchPanelData.__name__ = ["unbox","framework","model","vo","SearchPanelData"];
unbox.framework.model.vo.SearchPanelData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.SearchPanelData.prototype = {
	toString: function() {
		return "### SearchPanelData ### \ntitle : " + this._title + "\nx : " + this._x + "\ny : " + this._y + "\nsummaryLength : " + this._summaryLength + "\nlist : " + this._list.toString() + "\nscrollbar : " + this._scrollbar.toString() + "\ntexts : " + this._texts.toString() + "\n### END ###";
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		return this._y = value;
	}
	,get_summaryLength: function() {
		return this._summaryLength;
	}
	,set_summaryLength: function(value) {
		return this._summaryLength = value;
	}
	,get_list: function() {
		return this._list;
	}
	,set_list: function(value) {
		return this._list = value;
	}
	,get_scrollbar: function() {
		return this._scrollbar;
	}
	,set_scrollbar: function(value) {
		return this._scrollbar = value;
	}
	,get_texts: function() {
		return this._texts;
	}
	,set_texts: function(value) {
		return this._texts = value;
	}
	,__class__: unbox.framework.model.vo.SearchPanelData
	,__properties__: {set_texts:"set_texts",get_texts:"get_texts",set_scrollbar:"set_scrollbar",get_scrollbar:"get_scrollbar",set_list:"set_list",get_list:"get_list",set_summaryLength:"set_summaryLength",get_summaryLength:"get_summaryLength",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x",set_title:"set_title",get_title:"get_title"}
};
unbox.framework.model.vo.SubMenuItemData = function() {
};
$hxClasses["unbox.framework.model.vo.SubMenuItemData"] = unbox.framework.model.vo.SubMenuItemData;
unbox.framework.model.vo.SubMenuItemData.__name__ = ["unbox","framework","model","vo","SubMenuItemData"];
unbox.framework.model.vo.SubMenuItemData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.SubMenuItemData.prototype = {
	toString: function() {
		return "### SubMenuItemData ### \npageId : " + this._pageId + "\nlastPageId : " + this._lastPageId + "\ntitle : " + Std.string(this._title) + "\n### END ###";
	}
	,get_pageId: function() {
		return this._pageId;
	}
	,set_pageId: function(value) {
		return this._pageId = value;
	}
	,get_lastPageId: function() {
		return this._lastPageId;
	}
	,set_lastPageId: function(value) {
		return this._lastPageId = value;
	}
	,get_title: function() {
		return this._title;
	}
	,set_title: function(value) {
		return this._title = value;
	}
	,__class__: unbox.framework.model.vo.SubMenuItemData
	,__properties__: {set_title:"set_title",get_title:"get_title",set_lastPageId:"set_lastPageId",get_lastPageId:"get_lastPageId",set_pageId:"set_pageId",get_pageId:"get_pageId"}
};
unbox.framework.model.vo.TooltipData = function() {
};
$hxClasses["unbox.framework.model.vo.TooltipData"] = unbox.framework.model.vo.TooltipData;
unbox.framework.model.vo.TooltipData.__name__ = ["unbox","framework","model","vo","TooltipData"];
unbox.framework.model.vo.TooltipData.__interfaces__ = [unbox.framework.model.vo.IData];
unbox.framework.model.vo.TooltipData.prototype = {
	toString: function() {
		return "### TooltipData ### \nhook : " + ("" + this._hook) + "\nhookSize : " + this._hookSize + "\nautoSize : " + ("" + this._autoSize) + "\nfollowCursor : " + ("" + this._followCursor) + "\nalign : " + this._align + "\ncornerRadius : " + this._cornerRadius + "\nshowDelay : " + this._showDelay + "\nhideDelay : " + this._hideDelay + "\nbgAlpha : " + this._bgAlpha + "\nwidth : " + this._width + "\npadding : " + this._padding + "\ncolors : " + Std.string(this._colors) + "\nitems : " + this._items.toString() + "\n### END ###";
	}
	,get_hook: function() {
		return this._hook;
	}
	,set_hook: function(value) {
		return this._hook = value;
	}
	,get_hookSize: function() {
		return this._hookSize;
	}
	,set_hookSize: function(value) {
		return this._hookSize = value;
	}
	,get_autoSize: function() {
		return this._autoSize;
	}
	,set_autoSize: function(value) {
		return this._autoSize = value;
	}
	,get_followCursor: function() {
		return this._followCursor;
	}
	,set_followCursor: function(value) {
		return this._followCursor = value;
	}
	,get_align: function() {
		return this._align;
	}
	,set_align: function(value) {
		return this._align = value;
	}
	,get_cornerRadius: function() {
		return this._cornerRadius;
	}
	,set_cornerRadius: function(value) {
		return this._cornerRadius = value;
	}
	,get_showDelay: function() {
		return this._showDelay;
	}
	,set_showDelay: function(value) {
		return this._showDelay = value;
	}
	,get_hideDelay: function() {
		return this._hideDelay;
	}
	,set_hideDelay: function(value) {
		return this._hideDelay = value;
	}
	,get_bgAlpha: function() {
		return this._bgAlpha;
	}
	,set_bgAlpha: function(value) {
		return this._bgAlpha = value;
	}
	,get_width: function() {
		return this._width;
	}
	,set_width: function(value) {
		return this._width = value;
	}
	,get_colors: function() {
		return this._colors;
	}
	,set_colors: function(value) {
		return this._colors = value;
	}
	,get_items: function() {
		return this._items;
	}
	,set_items: function(value) {
		return this._items = value;
	}
	,get_padding: function() {
		return this._padding;
	}
	,set_padding: function(value) {
		return this._padding = value;
	}
	,__class__: unbox.framework.model.vo.TooltipData
	,__properties__: {set_padding:"set_padding",get_padding:"get_padding",set_items:"set_items",get_items:"get_items",set_colors:"set_colors",get_colors:"get_colors",set_width:"set_width",get_width:"get_width",set_bgAlpha:"set_bgAlpha",get_bgAlpha:"get_bgAlpha",set_hideDelay:"set_hideDelay",get_hideDelay:"get_hideDelay",set_showDelay:"set_showDelay",get_showDelay:"get_showDelay",set_cornerRadius:"set_cornerRadius",get_cornerRadius:"get_cornerRadius",set_align:"set_align",get_align:"get_align",set_followCursor:"set_followCursor",get_followCursor:"get_followCursor",set_autoSize:"set_autoSize",get_autoSize:"get_autoSize",set_hookSize:"set_hookSize",get_hookSize:"get_hookSize",set_hook:"set_hook",get_hook:"get_hook"}
};
unbox.framework.responder = {};
unbox.framework.responder.SearchResponder = function() {
	this.request = new msignal.Signal2();
	this.response = new msignal.Signal2();
};
$hxClasses["unbox.framework.responder.SearchResponder"] = unbox.framework.responder.SearchResponder;
unbox.framework.responder.SearchResponder.__name__ = ["unbox","framework","responder","SearchResponder"];
unbox.framework.responder.SearchResponder.prototype = {
	removeAll: function() {
		this.request.removeAll();
		this.response.removeAll();
	}
	,__class__: unbox.framework.responder.SearchResponder
};
unbox.framework.responder.TooltipResponder = function() {
	this.request = new msignal.Signal2();
	this.response = new msignal.Signal1();
};
$hxClasses["unbox.framework.responder.TooltipResponder"] = unbox.framework.responder.TooltipResponder;
unbox.framework.responder.TooltipResponder.__name__ = ["unbox","framework","responder","TooltipResponder"];
unbox.framework.responder.TooltipResponder.prototype = {
	removeAll: function() {
		this.request.removeAll();
		this.response.removeAll();
	}
	,__class__: unbox.framework.responder.TooltipResponder
};
unbox.framework.services = {};
unbox.framework.services.IEbookDataService = function() { };
$hxClasses["unbox.framework.services.IEbookDataService"] = unbox.framework.services.IEbookDataService;
unbox.framework.services.IEbookDataService.__name__ = ["unbox","framework","services","IEbookDataService"];
unbox.framework.services.IEbookDataService.prototype = {
	__class__: unbox.framework.services.IEbookDataService
};
unbox.framework.services.LoadService = function() {
	this.init();
};
$hxClasses["unbox.framework.services.LoadService"] = unbox.framework.services.LoadService;
unbox.framework.services.LoadService.__name__ = ["unbox","framework","services","LoadService"];
unbox.framework.services.LoadService.prototype = {
	init: function() {
		this.stringLoader = new openfl.net.URLLoader();
		this.stringLoader.addEventListener(openfl.events.HTTPStatusEvent.HTTP_STATUS,$bind(this,this.loaderEvent));
		this.stringLoader.addEventListener(openfl.events.Event.COMPLETE,$bind(this,this.loaderEvent));
		this.stringLoader.addEventListener(openfl.events.IOErrorEvent.IO_ERROR,$bind(this,this.loaderEvent));
		this.stringLoader.addEventListener(openfl.events.SecurityErrorEvent.SECURITY_ERROR,$bind(this,this.loaderEvent));
		this.viewLoader = new openfl.display.Loader();
		this._loaderDomain = new openfl.system.ApplicationDomain(openfl.system.ApplicationDomain.currentDomain);
		this.viewLoader.contentLoaderInfo.addEventListener(openfl.events.HTTPStatusEvent.HTTP_STATUS,$bind(this,this.loaderEvent));
		this.viewLoader.contentLoaderInfo.addEventListener(openfl.events.Event.COMPLETE,$bind(this,this.loaderEvent));
		this.viewLoader.contentLoaderInfo.addEventListener(openfl.events.IOErrorEvent.IO_ERROR,$bind(this,this.loaderEvent));
		this.viewLoader.contentLoaderInfo.addEventListener(openfl.events.SecurityErrorEvent.SECURITY_ERROR,$bind(this,this.loaderEvent));
		this.onCompleteAll = new msignal.Signal1();
		this.loadedAssets = new haxe.ds.StringMap();
		this.loadQueue = new Array();
		this.loadIds = new Array();
		this.isLoading = false;
	}
	,add: function(url,id) {
		if(this.isLoading) return;
		this.loadQueue.push(url);
		if(id == null) {
			haxe.Log.trace("Adding swf to queue with id '" + id + "'",{ fileName : "LoadService.hx", lineNumber : 90, className : "unbox.framework.services.LoadService", methodName : "add"});
			this.loadIds.push(id);
		} else this.loadIds.push(id);
	}
	,start: function(globalCallback) {
		haxe.Log.trace("LoadService.start",{ fileName : "LoadService.hx", lineNumber : 109, className : "unbox.framework.services.LoadService", methodName : "start"});
		this.loadIndex = 0;
		this.isLoading = true;
		this.loadedAssets = null;
		this.loadedAssets = new haxe.ds.StringMap();
		this.onCompleteAll.addOnce(globalCallback);
		this.load();
	}
	,load: function() {
		haxe.Log.trace("LoadService.load -> ID : " + this.loadQueue[this.loadIndex],{ fileName : "LoadService.hx", lineNumber : 121, className : "unbox.framework.services.LoadService", methodName : "load"});
		var fileExt = HxOverrides.substr(this.loadQueue[this.loadIndex],this.loadQueue[this.loadIndex].length - 4,4);
		if(fileExt.indexOf(".") < 0) openfl.Assets.loadLibrary(this.loadQueue[this.loadIndex],$bind(this,this.onLoadComplete));
		switch(fileExt) {
		case ".xml":case ".css":
			this.stringLoader.load(new openfl.net.URLRequest(this.loadQueue[this.loadIndex]));
			break;
		case ".jpeg":case ".png":case ".gif":
			haxe.Log.trace("Image loading not implemented yet!",{ fileName : "LoadService.hx", lineNumber : 141, className : "unbox.framework.services.LoadService", methodName : "load"});
			break;
		default:
			this.isLoading = false;
		}
	}
	,onLoadComplete: function(e) {
		haxe.Log.trace("LoadService.onLoadComplete > ID : " + this.loadQueue[this.loadIndex],{ fileName : "LoadService.hx", lineNumber : 149, className : "unbox.framework.services.LoadService", methodName : "onLoadComplete"});
		var data = null;
		if(js.Boot.__instanceof(e,openfl.AssetLibrary)) data = e; else data = e.target.data;
		var value = data;
		this.loadedAssets.set(this.loadIds[this.loadIndex],value);
		if(this.loadIndex < this.loadQueue.length - 1) {
			this.loadIndex++;
			this.load();
		} else {
			this.clear();
			this.onCompleteAll.dispatch(this.loadedAssets);
		}
	}
	,clear: function() {
		this.loadQueue = null;
		this.loadIds = null;
		this.isLoading = false;
		this.loadQueue = new Array();
		this.loadIds = new Array();
	}
	,loadLibComplete: function(lib) {
		haxe.Log.trace("LoadService.loadLibComplete > lib : " + Std.string(lib),{ fileName : "LoadService.hx", lineNumber : 197, className : "unbox.framework.services.LoadService", methodName : "loadLibComplete"});
		haxe.Log.trace("Works!",{ fileName : "LoadService.hx", lineNumber : 198, className : "unbox.framework.services.LoadService", methodName : "loadLibComplete"});
	}
	,loadSingleString: function(url,onComplete) {
		var request = new openfl.net.URLRequest(url);
		var loader = new openfl.net.URLLoader();
		loader.addEventListener(openfl.events.Event.COMPLETE,onComplete);
		loader.load(request);
	}
	,loadSingleView: function(url,onComplete) {
		var request = new openfl.net.URLRequest(url);
		var loader = new openfl.display.Loader();
		var context = new openfl.system.LoaderContext(false,this._loaderDomain,null);
		loader.contentLoaderInfo.addEventListener(openfl.events.Event.COMPLETE,onComplete);
	}
	,loaderEvent: function(e) {
		var _g = e.type;
		switch(_g) {
		case openfl.events.HTTPStatusEvent.HTTP_STATUS:
			this.loadStatus(e.status);
			break;
		case openfl.events.Event.COMPLETE:
			this.onLoadComplete(e);
			break;
		case openfl.events.IOErrorEvent.IO_ERROR:
			this.loadError(e.toString());
			break;
		case openfl.events.SecurityErrorEvent.SECURITY_ERROR:
			this.loadSecurityError(e.toString());
			break;
		}
	}
	,loadStatus: function(status) {
	}
	,loadError: function(error) {
		haxe.Log.trace("Error while loading asset with id '" + this.loadIds[this.loadIndex] + "' - Error details : " + error,{ fileName : "LoadService.hx", lineNumber : 248, className : "unbox.framework.services.LoadService", methodName : "loadError"});
	}
	,loadSecurityError: function(error) {
		haxe.Log.trace("Security Error while loading asset with id '" + this.loadIds[this.loadIndex] + "' : " + error,{ fileName : "LoadService.hx", lineNumber : 253, className : "unbox.framework.services.LoadService", methodName : "loadSecurityError"});
	}
	,__class__: unbox.framework.services.LoadService
};
unbox.framework.services.ScormDataService = function() {
	haxe.Log.trace("ScormDataService.ScormDataService",{ fileName : "ScormDataService.hx", lineNumber : 33, className : "unbox.framework.services.ScormDataService", methodName : "new"});
	this.init();
};
$hxClasses["unbox.framework.services.ScormDataService"] = unbox.framework.services.ScormDataService;
unbox.framework.services.ScormDataService.__name__ = ["unbox","framework","services","ScormDataService"];
unbox.framework.services.ScormDataService.__interfaces__ = [unbox.framework.services.IEbookDataService];
unbox.framework.services.ScormDataService.prototype = {
	init: function() {
		haxe.Log.trace("ScormDataService.init",{ fileName : "ScormDataService.hx", lineNumber : 39, className : "unbox.framework.services.ScormDataService", methodName : "init"});
		this.scormAPI = new unbox.framework.com.pipwerks.SCORM();
		this._isAvailable = this._isConnected = this.scormAPI.connect();
		haxe.Log.trace("ScormDataService.isAvailable: " + ("" + this._isAvailable),{ fileName : "ScormDataService.hx", lineNumber : 42, className : "unbox.framework.services.ScormDataService", methodName : "init"});
		this._onLoaded = new msignal.Signal1();
		this._onSaved = new msignal.Signal0();
		this._onLoadError = new msignal.Signal1();
		this._onSaveError = new msignal.Signal1();
	}
	,get_isAvailable: function() {
		return this._isAvailable;
	}
	,load: function() {
		haxe.Log.trace("ScormDataService.load",{ fileName : "ScormDataService.hx", lineNumber : 56, className : "unbox.framework.services.ScormDataService", methodName : "load"});
		if(this._isConnected) {
			var ebookData = new unbox.framework.model.vo.EbookData();
			ebookData.set_comments(this.getParam("cmi.comments"));
			ebookData.set_credit(this.getParam("cmi.core.credit"));
			ebookData.set_entry(this.getParam("cmi.core.entry"));
			ebookData.set_launch_data(this.getParam("cmi.launch_data"));
			ebookData.set_lesson_location(this.getParam("cmi.core.lesson_location"));
			ebookData.set_lesson_mode(this.getParam("cmi.core.lesson_mode"));
			ebookData.set_lesson_status(this.getParam("cmi.core.lesson_status"));
			ebookData.set_scoreMax(Std.parseInt(this.getParam("cmi.core.score.max")));
			ebookData.set_scoreMin(Std.parseInt(this.getParam("cmi.core.score.min")));
			ebookData.set_scoreRaw(Std.parseInt(this.getParam("cmi.core.score.raw")));
			ebookData.set_student_id(this.getParam("cmi.core.student_id"));
			ebookData.set_student_name(this.getParam("cmi.core.student_name"));
			ebookData.set_suspend_data(this.getParam("cmi.suspend_data"));
			ebookData.set_total_time(this.getParam("cmi.core.total_time"));
			this._onLoaded.dispatch(ebookData);
		} else this._onLoadError.dispatch("ScormDataService Load Error. ScormDataService.connect: " + ("" + this._isConnected));
	}
	,save: function(data) {
		this.update(data);
		var success = this.scormAPI.save();
		if(this._isConnected && success) this._onSaved.dispatch(); else this._onSaveError.dispatch("ScormDataService Save Error. ScormDataService.connect: " + ("" + this._isConnected));
	}
	,update: function(data) {
		this.setParam("cmi.core.exit",data.get_exit());
		this.setParam("cmi.core.lesson_location",data.get_lesson_location());
		this.setParam("cmi.core.lesson_status",data.get_lesson_status());
		this.setParam("cmi.core.score.max","" + data.get_scoreMax());
		this.setParam("cmi.core.score.min","" + data.get_scoreMin());
		this.setParam("cmi.core.score.raw","" + data.get_scoreRaw());
		this.setParam("cmi.core.session_time",data.get_session_time());
		this.setParam("cmi.suspend_data",data.get_suspend_data());
	}
	,setParam: function(param,value) {
		var ret = this.scormAPI.set(param,value);
		haxe.Log.trace("function setParam > param: " + param + " value: " + value + " saved: " + ("" + ret),{ fileName : "ScormDataService.hx", lineNumber : 114, className : "unbox.framework.services.ScormDataService", methodName : "setParam"});
		return ret;
	}
	,getParam: function(param) {
		var ret = this.scormAPI.get(param);
		haxe.Log.trace("ScormDataService.getParam > param: " + param + " ret: " + ret,{ fileName : "ScormDataService.hx", lineNumber : 121, className : "unbox.framework.services.ScormDataService", methodName : "getParam"});
		return ret;
	}
	,get_onLoadError: function() {
		return this._onLoadError;
	}
	,get_onSaveError: function() {
		return this._onSaveError;
	}
	,get_onLoaded: function() {
		return this._onLoaded;
	}
	,get_onSaved: function() {
		return this._onSaved;
	}
	,__class__: unbox.framework.services.ScormDataService
	,__properties__: {get_onSaved:"get_onSaved",get_onLoaded:"get_onLoaded",get_onSaveError:"get_onSaveError",get_onLoadError:"get_onLoadError",get_isAvailable:"get_isAvailable"}
};
unbox.framework.services.SolDataService = function() {
	this.init();
};
$hxClasses["unbox.framework.services.SolDataService"] = unbox.framework.services.SolDataService;
unbox.framework.services.SolDataService.__name__ = ["unbox","framework","services","SolDataService"];
unbox.framework.services.SolDataService.__interfaces__ = [unbox.framework.services.IEbookDataService];
unbox.framework.services.SolDataService.prototype = {
	init: function() {
		haxe.Log.trace("SolDataService.init",{ fileName : "SolDataService.hx", lineNumber : 35, className : "unbox.framework.services.SolDataService", methodName : "init"});
		this.sol = openfl.net.SharedObject.getLocal("EbookSolData");
		this._isAvailable = true;
		this._onLoaded = new msignal.Signal1();
		this._onSaved = new msignal.Signal0();
		this._onLoadError = new msignal.Signal1();
		this._onSaveError = new msignal.Signal1();
	}
	,load: function() {
		haxe.Log.trace("SolDataService.load",{ fileName : "SolDataService.hx", lineNumber : 47, className : "unbox.framework.services.SolDataService", methodName : "load"});
		var ebookData = new unbox.framework.model.vo.EbookData();
		if(this.getParam("cmi.suspend_data") != null) {
			ebookData.set_comments(this.getParam("cmi.comments"));
			ebookData.set_credit(this.getParam("cmi.core.credit"));
			ebookData.set_entry(this.getParam("cmi.core.entry"));
			ebookData.set_launch_data(this.getParam("cmi.launch_data"));
			ebookData.set_lesson_location(this.getParam("cmi.core.lesson_location"));
			ebookData.set_lesson_mode(this.getParam("cmi.core.lesson_mode"));
			ebookData.set_lesson_status(this.getParam("cmi.core.lesson_status"));
			ebookData.set_scoreMax(Std.parseInt(this.getParam("cmi.core.score.max")));
			ebookData.set_scoreMin(Std.parseInt(this.getParam("cmi.core.score.min")));
			ebookData.set_scoreRaw(Std.parseInt(this.getParam("cmi.core.score.raw")));
			ebookData.set_student_id(this.getParam("cmi.core.student_id"));
			ebookData.set_student_name(this.getParam("cmi.core.student_name"));
			ebookData.set_suspend_data(this.getParam("cmi.suspend_data"));
			ebookData.set_total_time(this.getParam("cmi.core.total_time"));
		}
		this._onLoaded.dispatch(ebookData);
	}
	,save: function(data) {
		this.update(data);
		this.sol.flush();
		this._onSaved.dispatch();
	}
	,get_isAvailable: function() {
		return this._isAvailable;
	}
	,update: function(data) {
		this.setParam("cmi.core.exit",data.get_exit());
		this.setParam("cmi.core.lesson_location",data.get_lesson_location());
		this.setParam("cmi.core.lesson_status",data.get_lesson_status());
		this.setParam("cmi.core.score.max","" + data.get_scoreMax());
		this.setParam("cmi.core.score.min","" + data.get_scoreMin());
		this.setParam("cmi.core.score.raw","" + data.get_scoreRaw());
		this.setParam("cmi.core.session_time",data.get_session_time());
		this.setParam("cmi.suspend_data",data.get_suspend_data());
	}
	,setParam: function(param,value) {
		haxe.Log.trace("setParam > param : " + param + ", value : " + value,{ fileName : "SolDataService.hx", lineNumber : 106, className : "unbox.framework.services.SolDataService", methodName : "setParam"});
		this.sol.data[param] = value;
	}
	,getParam: function(param) {
		var ret = Reflect.field(this.sol.data,param);
		haxe.Log.trace("SolDataService.getParam > param: " + param + " ret: " + ret,{ fileName : "SolDataService.hx", lineNumber : 113, className : "unbox.framework.services.SolDataService", methodName : "getParam"});
		return ret;
	}
	,get_onLoadError: function() {
		return this._onLoadError;
	}
	,get_onSaveError: function() {
		return this._onSaveError;
	}
	,get_onLoaded: function() {
		return this._onLoaded;
	}
	,get_onSaved: function() {
		return this._onSaved;
	}
	,__class__: unbox.framework.services.SolDataService
	,__properties__: {get_onSaved:"get_onSaved",get_onLoaded:"get_onLoaded",get_onSaveError:"get_onSaveError",get_onLoadError:"get_onLoadError",get_isAvailable:"get_isAvailable"}
};
unbox.framework.services.TooltipService = function() {
};
$hxClasses["unbox.framework.services.TooltipService"] = unbox.framework.services.TooltipService;
unbox.framework.services.TooltipService.__name__ = ["unbox","framework","services","TooltipService"];
unbox.framework.services.TooltipService.prototype = {
	getTooltip: function(response,data) {
		var tt = new unbox.framework.com.hybrid.ui.ToolTip();
		tt.set_titleEmbed(true);
		tt.set_contentEmbed(true);
		tt.set_align(data.get_align());
		tt.set_autoSize(data.get_autoSize());
		tt.set_followCursor(data.get_followCursor());
		tt.set_hook(data.get_hook());
		tt.set_hookSize(data.get_hookSize());
		tt.set_cornerRadius(data.get_cornerRadius());
		tt.set_showDelay(data.get_showDelay());
		tt.set_hideDelay(data.get_hideDelay());
		tt.set_bgAlpha(data.get_bgAlpha());
		tt.set_tipWidth(data.get_width());
		tt.set_colors(data.get_colors());
		tt.set_padding(data.get_padding());
		response.dispatch(tt);
	}
	,__class__: unbox.framework.services.TooltipService
};
unbox.framework.utils = {};
unbox.framework.utils.ArrayUtils = function() {
};
$hxClasses["unbox.framework.utils.ArrayUtils"] = unbox.framework.utils.ArrayUtils;
unbox.framework.utils.ArrayUtils.__name__ = ["unbox","framework","utils","ArrayUtils"];
unbox.framework.utils.ArrayUtils.binarySearch = function(arr,find) {
	var array = arr.slice(0);
	var l = 0;
	var r = array.length - 1;
	var k = 0;
	var found = false;
	while(l <= r && !found) {
		k = (l + r) / 2 | 0;
		if(find == array[k]) found = !found; else if(find < array[k]) r = k - 1; else l = k + 1;
	}
	if(found) return k; else return -1;
};
unbox.framework.utils.ArrayUtils.search = function(arr,value) {
	if(arr != null && !Math.isNaN(value)) {
		var i = 0;
		while(i < arr.length) {
			if(arr[i] == value) return i;
			i++;
		}
	}
	return -1;
};
unbox.framework.utils.ArrayUtils.toNumber = function(array,empty) {
	if(empty == null) empty = 0;
	var tmpArr = new Array();
	var _g1 = 0;
	var _g = array.length;
	while(_g1 < _g) {
		var i = _g1++;
		var tmpNum = Std.parseInt(array[i]);
		if(!Math.isNaN(tmpNum)) tmpArr[i] = tmpNum; else if(empty != 0) tmpArr[i] = empty;
	}
	return tmpArr;
};
unbox.framework.utils.ArrayUtils.sortNonRepeatSeq = function(start,end,length) {
	if(length == null) length = 0;
	var sortedSeq = [];
	if(length == 0) length = end - start;
	var amount = Std.int(Math.min(length,end - start));
	var _g = 0;
	while(_g < amount) {
		var j = _g++;
		var rndNum = unbox.framework.utils.NumberUtils.getRandomInt(start,end);
		var n = 0;
		while(n < sortedSeq.length) {
			if(rndNum == sortedSeq[n]) {
				rndNum = unbox.framework.utils.NumberUtils.getRandomInt(start,end);
				n = -1;
			}
			n++;
		}
		sortedSeq[j] = rndNum;
	}
	return sortedSeq;
};
unbox.framework.utils.ArrayUtils.randomizeArray = function(array) {
	var newArray = new Array();
	while(array.length > 0) newArray.push(array.splice(Math.floor(Math.random() * array.length),1));
	return newArray;
};
unbox.framework.utils.ArrayUtils.fillArray = function(size,value) {
	var newArray = [];
	while(size > 0) {
		newArray.push(value);
		size--;
	}
	return newArray;
};
unbox.framework.utils.ArrayUtils.fillSequence = function(start,length,ascending) {
	if(ascending == null) ascending = true;
	var seqArray = [];
	var i = 0;
	if(ascending) {
		var end = start + length;
		var _g = start;
		while(_g < end) {
			var i1 = _g++;
			seqArray.push(i1);
		}
	} else {
		i = start;
		while(i > start - length) {
			seqArray.push(i);
			i--;
		}
	}
	return seqArray;
};
unbox.framework.utils.ArrayUtils.prototype = {
	__class__: unbox.framework.utils.ArrayUtils
};
unbox.framework.utils.ContentParser = function() { };
$hxClasses["unbox.framework.utils.ContentParser"] = unbox.framework.utils.ContentParser;
unbox.framework.utils.ContentParser.__name__ = ["unbox","framework","utils","ContentParser"];
unbox.framework.utils.ContentParser.currentPage = null;
unbox.framework.utils.ContentParser.parsePageContent = function(pageContent,page,onComplete) {
	unbox.framework.utils.ContentParser.currentPage = page;
	var $it0 = pageContent.nodes.resolve("content").iterator();
	while( $it0.hasNext() ) {
		var content = $it0.next();
		var _g = content.att.resolve("type");
		switch(_g) {
		case "text":
			var $it1 = content.nodes.resolve("content").iterator();
			while( $it1.hasNext() ) {
				var textData = $it1.next();
				unbox.framework.utils.ContentParser.parseTextField(textData);
			}
			break;
		case "object":
			unbox.framework.utils.ContentParser.parseObject(content);
			break;
		default:
			haxe.Log.trace("Page parsing error : content type not recognized",{ fileName : "ContentParser.hx", lineNumber : 49, className : "unbox.framework.utils.ContentParser", methodName : "parsePageContent"});
		}
	}
	unbox.framework.utils.ContentParser.currentPage = null;
	onComplete();
};
unbox.framework.utils.ContentParser.parseTextField = function(textData,target,context) {
	var txt = new openfl.text.TextField();
	txt.embedFonts = true;
	txt.antiAliasType = openfl.text.AntiAliasType.ADVANCED;
	txt.gridFitType = openfl.text.GridFitType.SUBPIXEL;
	txt.set_wordWrap(true);
	if(textData.has.resolve("instanceName")) txt.set_name(textData.att.resolve("instanceName")); else txt.set_name("text");
	if(textData.hasNode.resolve("vars")) unbox.framework.utils.ContentParser.parseVars(txt,textData.node.resolve("vars"));
	txt.multiline = true;
	var strippedText = "";
	if(textData.hasNode.resolve("body")) strippedText = textData.node.resolve("body").get_innerHTML(); else strippedText = textData.node.resolve("title").get_innerHTML();
	strippedText = unbox.framework.utils.ContentParser.removeCDATA(strippedText);
	if(strippedText.indexOf("{$") > -1 && context != null) strippedText = unbox.framework.utils.StringUtils.parseTextVars(strippedText,context);
	txt.set_htmlText(strippedText);
	if(target == null) unbox.framework.utils.ContentParser.currentPage.view.addChild(txt); else target.addChild(txt);
	if(target == null && unbox.framework.utils.ContentParser.currentPage.data.get_contentTransitionIn() != "none") {
		haxe.Log.trace("Setting alpha to 0",{ fileName : "ContentParser.hx", lineNumber : 123, className : "unbox.framework.utils.ContentParser", methodName : "parseTextField"});
		txt.set_alpha(0);
		txt.set_visible(false);
	}
	return txt;
};
unbox.framework.utils.ContentParser.removeCDATA = function(txt) {
	txt = StringTools.replace(txt,"<![CDATA[","");
	txt = StringTools.replace(txt,"]]>","");
	txt = StringTools.trim(txt);
	txt = unbox.framework.utils.StringUtils.trimWhitespace(txt);
	return txt;
};
unbox.framework.utils.ContentParser.parseObject = function(objData) {
	var $it0 = objData.nodes.resolve("content").iterator();
	while( $it0.hasNext() ) {
		var content = $it0.next();
		var _g = content.att.resolve("type");
		switch(_g) {
		case "sprite":
			haxe.Log.trace("Sprite parsing not available yet!",{ fileName : "ContentParser.hx", lineNumber : 153, className : "unbox.framework.utils.ContentParser", methodName : "parseObject"});
			break;
		case "button":
			unbox.framework.utils.ContentParser.parseButtons(content);
			break;
		case "component":
			haxe.Log.trace("Component parsing not available yet!",{ fileName : "ContentParser.hx", lineNumber : 157, className : "unbox.framework.utils.ContentParser", methodName : "parseObject"});
			break;
		}
	}
};
unbox.framework.utils.ContentParser.parseButtons = function(btnData) {
	var $it0 = btnData.nodes.resolve("content").iterator();
	while( $it0.hasNext() ) {
		var content = $it0.next();
		var btnView = null;
		btnView = js.Boot.__cast(openfl.Assets.getMovieClip(unbox.framework.utils.ContentParser.currentPage.data.get_id() + ":" + content.att.resolve("className")) , openfl.display.Sprite);
		var button = new unbox.framework.view.button.GenericButton(0,0,btnView,content.att.resolve("instanceName"));
		unbox.framework.utils.ContentParser.parseVars(button.get_view(),content.node.resolve("vars"));
		button.set_label(unbox.framework.utils.ContentParser.parseTextField(content.node.resolve("label"),button.get_view()));
		button.add(unbox.framework.utils.ContentParser.currentPage.view);
		unbox.framework.utils.ContentParser.currentPage.buttons.push(button);
	}
};
unbox.framework.utils.ContentParser.parseVars = function(obj,vars) {
	var $it0 = vars.get_elements();
	while( $it0.hasNext() ) {
		var field = $it0.next();
		if(field.get_elements().hasNext() && Reflect.isObject(field)) {
			var subObj = null;
			subObj = Reflect.getProperty(obj,field.get_name());
			var $it1 = field.get_elements();
			while( $it1.hasNext() ) {
				var subField = $it1.next();
				if(subObj != null && Reflect.hasField(subObj,subField.get_name())) Reflect.setField(subObj,subField.get_name(),subField.get_innerHTML());
			}
			Reflect.setField(obj,field.get_name(),subObj);
		} else Reflect.setField(obj,field.get_name(),field.get_innerHTML());
	}
};
unbox.framework.utils.Logger = function() {
};
$hxClasses["unbox.framework.utils.Logger"] = unbox.framework.utils.Logger;
unbox.framework.utils.Logger.__name__ = ["unbox","framework","utils","Logger"];
unbox.framework.utils.Logger.log = function(text,pos) {
	haxe.Log.trace(text,{ fileName : "Logger.hx", lineNumber : 16, className : "unbox.framework.utils.Logger", methodName : "log"});
};
unbox.framework.utils.Logger.prototype = {
	__class__: unbox.framework.utils.Logger
};
unbox.framework.utils.NumberUtils = function() {
};
$hxClasses["unbox.framework.utils.NumberUtils"] = unbox.framework.utils.NumberUtils;
unbox.framework.utils.NumberUtils.__name__ = ["unbox","framework","utils","NumberUtils"];
unbox.framework.utils.NumberUtils.fmtNumDec = function(valor,decimals) {
	var decMult = Std.int(Math.pow(10,decimals));
	return Std.int(Math.round(valor * decMult) / decMult);
};
unbox.framework.utils.NumberUtils.getRandomInt = function(start,end) {
	var rndNum = start + Math.floor(Math.random() * (end - start));
	return rndNum;
};
unbox.framework.utils.NumberUtils.fixPadding = function(value,padding) {
	var ret = "" + value;
	while(ret.length < padding) ret = "0" + ret;
	return ret;
};
unbox.framework.utils.NumberUtils.prototype = {
	__class__: unbox.framework.utils.NumberUtils
};
unbox.framework.utils.PageAnimator = function() {
};
$hxClasses["unbox.framework.utils.PageAnimator"] = unbox.framework.utils.PageAnimator;
unbox.framework.utils.PageAnimator.__name__ = ["unbox","framework","utils","PageAnimator"];
unbox.framework.utils.PageAnimator.animate = function(target,duration,direction,transition,cBack,cBackParams) {
	switch(transition) {
	case "slideXIn":
		unbox.framework.utils.PageAnimator.slideXIn(target,duration,direction,cBack,cBackParams);
		break;
	case "slideXOut":
		unbox.framework.utils.PageAnimator.slideXOut(target,duration,direction,cBack,cBackParams);
		break;
	case "slideYIn":
		unbox.framework.utils.PageAnimator.slideYIn(target,duration,direction,cBack,cBackParams);
		break;
	case "slideYOut":
		unbox.framework.utils.PageAnimator.slideYOut(target,duration,direction,cBack,cBackParams);
		break;
	case "fadeIn":
		unbox.framework.utils.PageAnimator.fadeIn(target,duration,direction,cBack,cBackParams);
		break;
	case "fadeOut":
		unbox.framework.utils.PageAnimator.fadeOut(target,duration,direction,cBack,cBackParams);
		break;
	default:
		return;
	}
};
unbox.framework.utils.PageAnimator.slideXIn = function(target,duration,direction,cBack,cBackParams) {
	if(direction == null) direction = 1;
	if(duration == null) duration = 0.5;
	if(target != null) {
		target.set_alpha(1);
		target.set_x(direction * target.stage.stageWidth);
		motion.Actuate.tween(target,duration,{ x : 0}).ease(motion.easing.Expo.get_easeInOut()).onComplete(cBack,cBackParams);
	}
};
unbox.framework.utils.PageAnimator.slideXOut = function(target,duration,direction,cBack,cBackParams) {
	if(direction == null) direction = -1;
	if(duration == null) duration = 0.5;
	if(target != null) {
		target.set_x(0);
		motion.Actuate.tween(target,duration,{ x : direction * -target.stage.stageWidth}).ease(motion.easing.Expo.get_easeInOut()).onComplete(cBack,cBackParams);
	}
};
unbox.framework.utils.PageAnimator.slideYIn = function(target,duration,direction,cBack,cBackParams) {
	if(direction == null) direction = 1;
	if(duration == null) duration = 0.5;
	if(target != null) {
		target.set_alpha(1);
		target.set_y(-direction * target.stage.stageHeight);
		motion.Actuate.tween(target,duration,{ y : 0}).ease(motion.easing.Expo.get_easeInOut()).onComplete(cBack,cBackParams);
	}
};
unbox.framework.utils.PageAnimator.slideYOut = function(target,duration,direction,cBack,cBackParams) {
	if(direction == null) direction = -1;
	if(duration == null) duration = 0.5;
	if(target != null) {
		target.set_y(0);
		motion.Actuate.tween(target,duration,{ y : -direction * -target.stage.stageHeight}).ease(motion.easing.Expo.get_easeInOut()).onComplete(cBack,cBackParams);
	}
};
unbox.framework.utils.PageAnimator.fadeIn = function(target,duration,direction,cBack,cBackParams) {
	if(direction == null) direction = 1;
	if(duration == null) duration = .3;
	if(target != null) {
		target.set_alpha(0);
		motion.Actuate.tween(target,duration,{ alpha : 1}).onComplete(cBack,cBackParams);
	}
};
unbox.framework.utils.PageAnimator.fadeOut = function(target,duration,direction,cBack,cBackParams) {
	if(direction == null) direction = -1;
	if(duration == null) duration = .3;
	if(target != null) motion.Actuate.tween(target,duration,{ alpha : 0}).onComplete(cBack,cBackParams);
};
unbox.framework.utils.PageAnimator.contentFadeIn = function(page,duration,delay,cBack,cBackParams) {
	if(delay == null) delay = 0;
	if(duration == null) duration = .5;
	var target;
	target = js.Boot.__cast(page.getChildByName("view") , openfl.display.Sprite);
	if(target != null) {
		var itemCount = 0;
		var i = 0;
		var _g1 = 0;
		var _g = target.get_numChildren();
		while(_g1 < _g) {
			var i1 = _g1++;
			var displayitem = target.getChildAt(i1);
			if(js.Boot.__instanceof(displayitem,openfl.text.TextField)) {
				var myFieldLabel;
				myFieldLabel = js.Boot.__cast(displayitem , openfl.display.DisplayObject);
				var _g2 = myFieldLabel;
				_g2.set_x(_g2.get_x() - 20);
				motion.Actuate.tween(myFieldLabel,duration,{ alpha : 1, x : myFieldLabel.get_x() + 20}).delay(delay + itemCount * 0.1).ease(motion.easing.Expo.get_easeOut()).onComplete(cBack,cBackParams);
				itemCount++;
			}
		}
	}
};
unbox.framework.utils.PageAnimator.contentFadeOut = function(page,duration,delay,cBack,cBackParams) {
	if(delay == null) delay = 0;
	if(duration == null) duration = .5;
	var target;
	target = js.Boot.__cast(page.getChildByName("view") , openfl.display.Sprite);
	if(target != null) {
		var itemCount = 0;
		var i = 0;
		var _g1 = 0;
		var _g = target.get_numChildren();
		while(_g1 < _g) {
			var i1 = _g1++;
			var displayitem = target.getChildAt(i1);
			if(js.Boot.__instanceof(displayitem,openfl.text.TextField)) {
				var myFieldLabel;
				myFieldLabel = js.Boot.__cast(displayitem , openfl.display.DisplayObject);
				motion.Actuate.tween(myFieldLabel,duration,{ alpha : 0, x : myFieldLabel.get_x() - 20}).delay(delay + itemCount * 0.1).ease(motion.easing.Expo.get_easeOut()).onComplete(cBack,cBackParams);
				itemCount++;
			}
		}
	}
};
unbox.framework.utils.PageAnimator.prototype = {
	__class__: unbox.framework.utils.PageAnimator
};
unbox.framework.utils.StringUtils = function() {
};
$hxClasses["unbox.framework.utils.StringUtils"] = unbox.framework.utils.StringUtils;
unbox.framework.utils.StringUtils.__name__ = ["unbox","framework","utils","StringUtils"];
unbox.framework.utils.StringUtils.isValidEmail = function(email) {
	var emailExpression = new EReg("^[a-z][\\w.-]+@\\w[\\w.-]+\\.[\\w.-]*[a-z][a-z]$","i");
	return emailExpression.match(email);
};
unbox.framework.utils.StringUtils.removeHTML = function(htmlStr) {
	var removeHTML = new EReg("<[^>]*>","gi");
	var safeStr = removeHTML.replace(htmlStr,"");
	return safeStr;
};
unbox.framework.utils.StringUtils.replace = function(string,pattern,replacement) {
	return string.split(pattern).join(replacement);
};
unbox.framework.utils.StringUtils.parseTextVars = function(text,context) {
	var output = text;
	var i = 0;
	while(i < text.length) {
		var varName = "";
		if(text.charAt(i++) == "{" && text.charAt(i++) == "$") {
			while(text.charAt(i) != "}" && i < text.length) varName += text.charAt(i++);
			output = StringTools.replace(output,"{$" + varName + "}",Reflect.field(context,varName));
		}
	}
	return output;
};
unbox.framework.utils.StringUtils.trimWhitespace = function(string) {
	if(String == null) return "";
	var expr = new EReg("^\\s+|\\s+$","g");
	var trimmed = expr.replace(string,"");
	return trimmed;
};
unbox.framework.utils.StringUtils.removeCDATA = function(txt) {
	txt = StringTools.replace(txt,"<![CDATA[","");
	txt = StringTools.replace(txt,"]]>","");
	txt = StringTools.trim(txt);
	txt = unbox.framework.utils.StringUtils.trimWhitespace(txt);
	return txt;
};
unbox.framework.utils.StringUtils.getFullWord = function(fullStr,str) {
	var foundStr = fullStr.indexOf(str);
	var fstInd = foundStr;
	var lstInd = foundStr;
	var char = "";
	if(foundStr > -1) {
		var hasFoundFst = false;
		var hasFoundLst = false;
		var hasFound = false;
		while(!hasFound) {
			if(!hasFoundFst) {
				char = fullStr.charAt(--fstInd);
				if(char == " " || fstInd < 0) {
					fstInd++;
					hasFoundFst = true;
				}
			}
			if(!hasFoundLst) {
				char = fullStr.charAt(++lstInd);
				if(char == " " || lstInd == fullStr.length) hasFoundLst = true;
			}
			hasFound = hasFoundFst && hasFoundLst;
		}
	}
	var word = "";
	word = fullStr.substring(fstInd,lstInd);
	return word;
};
unbox.framework.utils.StringUtils.stripTags = function(data) {
	if(data == null) return "";
	return new EReg("<\\/?[^>]+>","igm").replace(data,"");
};
unbox.framework.utils.StringUtils.decomposeUnicode = function(str) {
	var pattern = new Array();
	pattern[0] = new EReg("Š","g");
	pattern[1] = new EReg("Œ","g");
	pattern[2] = new EReg("Ž","g");
	pattern[3] = new EReg("š","g");
	pattern[4] = new EReg("œ","g");
	pattern[5] = new EReg("ž","g");
	pattern[6] = new EReg("[ÀÁÂÃÄÅ]","g");
	pattern[7] = new EReg("Æ","g");
	pattern[8] = new EReg("Ç","g");
	pattern[9] = new EReg("[ÈÉÊË]","g");
	pattern[10] = new EReg("[ÌÍÎÏ]","g");
	pattern[11] = new EReg("Ð","g");
	pattern[12] = new EReg("Ñ","g");
	pattern[13] = new EReg("[ÒÓÔÕÖØ]","g");
	pattern[14] = new EReg("[ÙÚÛÜ]","g");
	pattern[15] = new EReg("[ŸÝ]","g");
	pattern[16] = new EReg("Þ","g");
	pattern[17] = new EReg("ß","g");
	pattern[18] = new EReg("[àáâãäå]","g");
	pattern[19] = new EReg("æ","g");
	pattern[20] = new EReg("ç","g");
	pattern[21] = new EReg("[èéêë]","g");
	pattern[22] = new EReg("[ìíîï]","g");
	pattern[23] = new EReg("ð","g");
	pattern[24] = new EReg("ñ","g");
	pattern[25] = new EReg("[òóôõöø]","g");
	pattern[26] = new EReg("[ùúûü]","g");
	pattern[27] = new EReg("[ýÿ]","g");
	pattern[28] = new EReg("þ","g");
	var patternReplace = ["S","Oe","Z","s","oe","z","A","Ae","C","E","I","D","N","O","U","Y","Th","ss","a","ae","c","e","i","d","n","o","u","y","th"];
	var _g1 = 0;
	var _g = pattern.length;
	while(_g1 < _g) {
		var i = _g1++;
		str = pattern[i].replace(str,patternReplace[i]);
	}
	return str;
};
unbox.framework.utils.StringUtils.cleanSpecialChars = function(s) {
	return unbox.framework.utils.StringUtils.decomposeUnicode(s);
};
unbox.framework.utils.StringUtils.prototype = {
	__class__: unbox.framework.utils.StringUtils
};
unbox.framework.utils.TweenParser = function() {
};
$hxClasses["unbox.framework.utils.TweenParser"] = unbox.framework.utils.TweenParser;
unbox.framework.utils.TweenParser.__name__ = ["unbox","framework","utils","TweenParser"];
unbox.framework.utils.TweenParser.parse = function(context,data) {
	haxe.Log.trace("TweenParser.parse ->> TODO: Complete implementation!",{ fileName : "TweenParser.hx", lineNumber : 92, className : "unbox.framework.utils.TweenParser", methodName : "parse"});
	var $it0 = data.nodes.resolve("tween").iterator();
	while( $it0.hasNext() ) {
		var tween = $it0.next();
	}
};
unbox.framework.utils.TweenParser.prototype = {
	__class__: unbox.framework.utils.TweenParser
};
unbox.framework.view = {};
unbox.framework.view.IPage = function() { };
$hxClasses["unbox.framework.view.IPage"] = unbox.framework.view.IPage;
unbox.framework.view.IPage.__name__ = ["unbox","framework","view","IPage"];
unbox.framework.view.IPage.prototype = {
	__class__: unbox.framework.view.IPage
};
unbox.framework.view.CoursePage = function() {
	haxe.Log.trace("CoursePage.new => view : " + Std.string(this.view),{ fileName : "CoursePage.hx", lineNumber : 34, className : "unbox.framework.view.CoursePage", methodName : "new"});
	this.buttons = new Array();
	this._sendData = new msignal.Signal2();
	this.init();
};
$hxClasses["unbox.framework.view.CoursePage"] = unbox.framework.view.CoursePage;
unbox.framework.view.CoursePage.__name__ = ["unbox","framework","view","CoursePage"];
unbox.framework.view.CoursePage.__interfaces__ = [unbox.framework.view.IPage];
unbox.framework.view.CoursePage.prototype = {
	init: function() {
	}
	,transitionIn: function(data) {
	}
	,transitionInComplete: function(data) {
	}
	,transitionOut: function(data) {
	}
	,transitionOutComplete: function(data) {
	}
	,get_sendData: function() {
		return this._sendData;
	}
	,set_sendData: function(value) {
		return this._sendData = value;
	}
	,__class__: unbox.framework.view.CoursePage
	,__properties__: {set_sendData:"set_sendData",get_sendData:"get_sendData"}
};
unbox.framework.view.Dashboard = function(navLayer,data,mcLibrary) {
	this.navLayer = navLayer;
	this.data = data;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.Dashboard"] = unbox.framework.view.Dashboard;
unbox.framework.view.Dashboard.__name__ = ["unbox","framework","view","Dashboard"];
unbox.framework.view.Dashboard.prototype = {
	init: function(mcLibrary) {
		haxe.Log.trace("Dashboard.init",{ fileName : "Dashboard.hx", lineNumber : 68, className : "unbox.framework.view.Dashboard", methodName : "init"});
		this.view = js.Boot.__cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardView") , openfl.display.Sprite);
		this.view.set_x(0);
		this.view.set_y(-this.view.get_height());
		this.navLayer.addChild(this.view);
		this.closedY = -this.view.get_height();
		this.openedY = 0;
		this.isOpened = false;
		this.buttons = new Array();
		var indexViewInstance = null;
		indexViewInstance = js.Boot.__cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardIndexBtn") , openfl.display.Sprite);
		var indexButton = new unbox.framework.view.button.GenericButton(133,50,indexViewInstance,"indexBtn");
		indexButton.addHandlers($bind(this,this.handleButtonDown));
		indexButton.add(this.view);
		this.buttons.push(indexButton);
		var bookmarkViewInstance = null;
		bookmarkViewInstance = js.Boot.__cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardBookmarkBtn") , openfl.display.Sprite);
		var bookmarkButton = new unbox.framework.view.button.GenericButton(183,50,bookmarkViewInstance,"bookmarkBtn");
		bookmarkButton.addHandlers($bind(this,this.handleButtonDown));
		bookmarkButton.add(this.view);
		this.buttons.push(bookmarkButton);
		var aboutViewInstance = null;
		aboutViewInstance = js.Boot.__cast(mcLibrary.getMovieClip("com.unbox.assets.DashboardAboutBtn") , openfl.display.Sprite);
		var aboutButton = new unbox.framework.view.button.GenericButton(233,50,aboutViewInstance,"aboutBtn");
		aboutButton.addHandlers($bind(this,this.handleButtonDown));
		aboutButton.add(this.view);
		this.buttons.push(aboutButton);
		this.panels = new Array();
		var indexPanel = null;
		indexPanel = new unbox.framework.view.components.IndexPanel(this.view,this.data.get_indexPanel(),mcLibrary);
		indexPanel.dashboard = this;
		this.panels.push(indexPanel);
		var bookmarkPanel = null;
		bookmarkPanel = new unbox.framework.view.components.BookmarkPanel(this.view,this.data.get_bookmarkPanel(),mcLibrary);
		bookmarkPanel.dashboard = this;
		this.panels.push(bookmarkPanel);
		var aboutPanel = null;
		aboutPanel = new unbox.framework.view.components.AboutPanel(this.view,this.data.get_aboutPanel(),mcLibrary);
		aboutPanel.dashboard = this;
		this.panels.push(aboutPanel);
		var searchPanel = null;
		searchPanel = new unbox.framework.view.components.SearchPanel(this.view,this.data.get_searchPanel(),mcLibrary);
		searchPanel.dashboard = this;
		this.panels.push(searchPanel);
		var helpPanel = null;
		helpPanel = new unbox.framework.view.components.HelpPanel(js.Boot.__cast(this.view.parent , openfl.display.Sprite),this.data.get_helpPanel(),mcLibrary);
		helpPanel.onUserClick.add($bind(this,this.toggleHelp));
		helpPanel.dashboard = this;
		this.panels.push(helpPanel);
		var sbParent;
		if(this.data.get_searchBox().alwaysVisible) sbParent = this.navLayer; else sbParent = this.view;
		this.searchBox = new unbox.framework.view.components.SearchBox(sbParent,this.data.get_searchBox(),mcLibrary);
		this.currentPanel = 0;
		this.onPanelButtonPressed = new msignal.Signal1();
		this.ttResponder = new unbox.framework.responder.TooltipResponder();
	}
	,requestTooltip: function() {
		this.ttResponder.request.dispatch(this.ttResponder.response,this.data.get_tooltips());
	}
	,onTooltipReceived: function(tt) {
		this.tt = tt;
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_tooltips().get_items();
			$r = this1.keys();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var key = $it0.next();
			var btn = this.getButton(key);
			if(btn != null) {
				var btnView = btn.get_view();
				btnView.addEventListener(openfl.events.MouseEvent.ROLL_OVER,$bind(this,this.tooltipHandler),false,0,true);
			}
		}
	}
	,tooltipHandler: function(e) {
		var btn;
		btn = js.Boot.__cast(e.target , openfl.display.Sprite);
		this.tt.show(btn,unbox.framework.utils.StringUtils.removeCDATA((function($this) {
			var $r;
			var this1 = $this.data.get_tooltips().get_items();
			var key = btn.get_name();
			$r = this1.get(key);
			return $r;
		}(this))),"");
	}
	,getButton: function(name) {
		var _g1 = 0;
		var _g = this.buttons.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.buttons[i].name == name) return this.buttons[i];
		}
		return null;
	}
	,handleButtonDown: function(view) {
		var _g = view.name;
		switch(_g) {
		case "indexBtn":
			if(this.currentPanel == 0) return; else this.changePanel(0);
			break;
		case "bookmarkBtn":
			if(this.currentPanel == 1) return; else this.changePanel(1);
			break;
		case "aboutBtn":
			if(this.currentPanel == 2) return; else this.changePanel(2);
			break;
		}
	}
	,updateStatus: function() {
		this.isOpened = !this.isOpened;
		if(this.isOpened) {
			this.panels[this.currentPanel].show();
			if(this.currentPanel < 3) this.buttons[this.currentPanel].setEnabled(false);
		}
	}
	,changePanel: function(id) {
		if(id != 3) this.searchBox.reset();
		this.panels[id].show();
		if(id < 3) this.buttons[id].setEnabled(false);
		if(id != this.currentPanel) {
			this.panels[this.currentPanel].hide();
			if(this.currentPanel < 3) this.buttons[this.currentPanel].setEnabled(true);
			this.currentPanel = id;
		}
	}
	,toggle: function() {
		if(this.isOpened) motion.Actuate.tween(this.view,0.15,{ y : this.closedY}).ease(motion.easing.Expo.get_easeOut()).onComplete($bind(this,this.updateStatus)); else motion.Actuate.tween(this.view,0.15,{ y : this.openedY}).ease(motion.easing.Expo.get_easeOut()).onComplete($bind(this,this.updateStatus));
	}
	,toggleHelp: function() {
		if(this.panels[4].isVisible) {
			this.panels[4].hide();
			this.toggle();
		} else {
			this.panels[4].show();
			this.changePanel(0);
			if(!this.isOpened) this.toggle();
		}
	}
	,populateBookmarks: function(pages) {
		var bkPanel;
		bkPanel = js.Boot.__cast(this.panels[1] , unbox.framework.view.components.BookmarkPanel);
		var numBookmarks = pages.length;
		var _g = 0;
		while(_g < numBookmarks) {
			var i = _g++;
			bkPanel.insert(pages[i]);
		}
	}
	,updateBookmarks: function(added,bookmarkData) {
		(js.Boot.__cast(this.panels[1] , unbox.framework.view.components.BookmarkPanel)).setBookmarks(added,bookmarkData);
	}
	,getPanel: function(id) {
		if(this.panels[id] == null) {
			haxe.Log.trace("Panel at '" + id + "' is null!",{ fileName : "Dashboard.hx", lineNumber : 366, className : "unbox.framework.view.Dashboard", methodName : "getPanel"});
			return null;
		}
		return this.panels[id];
	}
	,getSearchBox: function() {
		if(this.searchBox == null) {
			haxe.Log.trace("SearchBox is null!",{ fileName : "Dashboard.hx", lineNumber : 381, className : "unbox.framework.view.Dashboard", methodName : "getSearchBox"});
			return null;
		}
		return this.searchBox;
	}
	,__class__: unbox.framework.view.Dashboard
};
unbox.framework.view.INavBar = function() { };
$hxClasses["unbox.framework.view.INavBar"] = unbox.framework.view.INavBar;
unbox.framework.view.INavBar.__name__ = ["unbox","framework","view","INavBar"];
unbox.framework.view.INavBar.prototype = {
	__class__: unbox.framework.view.INavBar
};
unbox.framework.view.NavBar = function(navLayer,data,mcLibrary) {
	openfl.display.Sprite.call(this);
	this.navLayer = navLayer;
	this.data = data;
	this.isChangingPage = false;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.NavBar"] = unbox.framework.view.NavBar;
unbox.framework.view.NavBar.__name__ = ["unbox","framework","view","NavBar"];
unbox.framework.view.NavBar.__interfaces__ = [unbox.framework.view.INavBar];
unbox.framework.view.NavBar.__super__ = openfl.display.Sprite;
unbox.framework.view.NavBar.prototype = $extend(openfl.display.Sprite.prototype,{
	init: function(mcLibrary) {
		var navBarView = null;
		navBarView = mcLibrary.getMovieClip("com.unbox.assets.NavBarView");
		navBarView.set_x(0);
		navBarView.set_y(0);
		this.navLayer.addChild(navBarView);
		var navBarBtns = ["helpBtn","menuBtn","backBtn","bookmarkBtn","nextBtn"];
		this._buttons = new Array();
		var _g1 = 0;
		var _g = navBarBtns.length;
		while(_g1 < _g) {
			var i = _g1++;
			var btnView;
			btnView = js.Boot.__cast(navBarView.getChildByName(navBarBtns[i]) , openfl.display.Sprite);
			if(btnView == null) {
				haxe.Log.trace(navBarBtns[i] + " view does not exist! Please verify your nav.swf file.",{ fileName : "NavBar.hx", lineNumber : 86, className : "unbox.framework.view.NavBar", methodName : "init"});
				continue;
			}
			var button;
			if(navBarBtns[i] == "menuBtn" || navBarBtns[i] == "bookmarkBtn") button = new unbox.framework.view.button.ToggleButton(btnView.get_x(),btnView.get_y(),btnView,navBarBtns[i]); else button = new unbox.framework.view.button.GenericButton(btnView.get_x(),btnView.get_y(),btnView,navBarBtns[i]);
			navBarView.removeChild(btnView);
			button.addHandlers($bind(this,this.onButtonDown));
			button.add(navBarView);
			this._buttons.push(button);
		}
		var _g11 = 0;
		var _g2 = this._buttons.length;
		while(_g11 < _g2) {
			var j = _g11++;
			haxe.Log.trace("button '" + j + "' : " + this._buttons[j].name + " | " + Std.string(Type["typeof"](this._buttons[j])) + " | pos : " + this._buttons[j].get_view().get_x() + "," + this._buttons[j].get_view().get_y(),{ fileName : "NavBar.hx", lineNumber : 105, className : "unbox.framework.view.NavBar", methodName : "init"});
		}
		this.navLayer.stage.addEventListener(openfl.events.KeyboardEvent.KEY_DOWN,$bind(this,this.handleKeyPress));
		this.onCustomKey = new msignal.Signal1();
		this._onButtonPressed = new msignal.Signal1();
		this.onDashboardToggle = new msignal.Signal0();
		this.onHelpToggle = new msignal.Signal0();
		this.onBookmarkToggle = new msignal.Signal1();
		this.ttResponder = new unbox.framework.responder.TooltipResponder();
	}
	,requestTooltip: function() {
		haxe.Log.trace("Dispatching tooltip request!",{ fileName : "NavBar.hx", lineNumber : 134, className : "unbox.framework.view.NavBar", methodName : "requestTooltip"});
		this.ttResponder.request.dispatch(this.ttResponder.response,this.data.get_tooltips());
	}
	,onTooltipReceived: function(tt) {
		haxe.Log.trace("tooltip received : " + Std.string(tt),{ fileName : "NavBar.hx", lineNumber : 140, className : "unbox.framework.view.NavBar", methodName : "onTooltipReceived"});
		this.tt = tt;
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_tooltips().get_items();
			$r = this1.keys();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var key = $it0.next();
			haxe.Log.trace("current key : " + key,{ fileName : "NavBar.hx", lineNumber : 145, className : "unbox.framework.view.NavBar", methodName : "onTooltipReceived"});
			var btn = this.getButton(key);
			if(btn != null) {
				var btnView = btn.get_view();
				btnView.addEventListener(openfl.events.MouseEvent.ROLL_OVER,$bind(this,this.tooltipHandler),false,0,true);
			}
		}
	}
	,tooltipHandler: function(e) {
		var btn;
		btn = js.Boot.__cast(e.target , openfl.display.Sprite);
		this.tt.show(btn,unbox.framework.utils.StringUtils.removeCDATA((function($this) {
			var $r;
			var this1 = $this.data.get_tooltips().get_items();
			var key = btn.get_name();
			$r = this1.get(key);
			return $r;
		}(this))),"");
	}
	,getButton: function(name) {
		var _g1 = 0;
		var _g = this._buttons.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this._buttons[i].name == name) return this._buttons[i];
		}
		return null;
	}
	,updateNavView: function(page) {
		if(page.data.get_pageProps() != null) {
			var _g1 = 0;
			var _g = page.data.get_pageProps().length;
			while(_g1 < _g) {
				var i = _g1++;
				var value;
				if(page.data.get_pageProps()[i] == 0) value = false; else value = true;
				if(this._buttons[i] != null) {
					this._buttons[i].setVisible(true);
					this._buttons[i].setEnabled(value);
				}
			}
		} else {
			var _g11 = 0;
			var _g2 = this._buttons.length;
			while(_g11 < _g2) {
				var i1 = _g11++;
				if(this._buttons[i1].name == "nextBtn") this._buttons[i1].setEnabled(true); else this._buttons[i1].setVisible(false);
			}
		}
	}
	,setButtons: function(ids) {
		var values = ids.split("");
		var _g1 = 0;
		var _g = this._buttons.length;
		while(_g1 < _g) {
			var i = _g1++;
			var value = Std.parseInt(values[i]);
			if(value == 0) this._buttons[i].setEnabled(false); else this._buttons[i].setEnabled(true);
		}
		this.isChangingPage = false;
	}
	,afterPageChange: function(page) {
		this.isChangingPage = false;
		this.currentPage = page;
	}
	,setBookmarkStatus: function(status) {
		(js.Boot.__cast(this._buttons[3] , unbox.framework.view.button.ToggleButton)).setToggle(status);
	}
	,onButtonDown: function(e) {
		if(this.isChangingPage) return;
		var btnId = 0;
		var _g = e.name;
		switch(_g) {
		case "backBtn":
			btnId = -1;
			this.isChangingPage = true;
			this._onButtonPressed.dispatch(btnId);
			break;
		case "nextBtn":
			btnId = 1;
			this.isChangingPage = true;
			this._onButtonPressed.dispatch(btnId);
			break;
		case "menuBtn":
			this.onDashboardToggle.dispatch();
			break;
		case "helpBtn":
			this.onHelpToggle.dispatch();
			return;
		case "bookmarkBtn":
			this.onBookmarkToggle.dispatch(this.currentPage.data);
			return;
		default:
			haxe.Log.trace("Button name not recognized : " + Std.string(e.name),{ fileName : "NavBar.hx", lineNumber : 246, className : "unbox.framework.view.NavBar", methodName : "onButtonDown"});
			return;
		}
	}
	,handleKeyPress: function(e) {
		if(this.isChangingPage == true || e.target.name == "searchText") {
			e.stopImmediatePropagation();
			return;
		}
		var _g = e.keyCode;
		switch(_g) {
		case 101:
			(js.Boot.__cast(this._buttons[0] , unbox.framework.view.button.GenericButton)).activate();
			break;
		case 100:
			(js.Boot.__cast(this._buttons[1] , unbox.framework.view.button.GenericButton)).activate();
			break;
		case 102:
			(js.Boot.__cast(this._buttons[2] , unbox.framework.view.button.GenericButton)).activate();
			break;
		case 32:
			e.target.dispatchEvent(new unbox.framework.view.button.ButtonEvent(unbox.framework.view.button.ButtonEvent.ACTIVATE,e.target,true));
			break;
		case 69:
			haxe.Log.trace("currentPage view stats : " + this.currentPage.view.get_x() + "," + this.currentPage.view.get_y() + " | " + this.currentPage.view.get_width() + "x" + this.currentPage.view.get_height(),{ fileName : "NavBar.hx", lineNumber : 273, className : "unbox.framework.view.NavBar", methodName : "handleKeyPress"});
			haxe.Log.trace("currentPage visibility and alpha : " + ("" + this.currentPage.view.get_visible()) + " | " + this.currentPage.view.get_alpha(),{ fileName : "NavBar.hx", lineNumber : 274, className : "unbox.framework.view.NavBar", methodName : "handleKeyPress"});
			this.currentPage.view.get_graphics().beginFill(255,0.7);
			this.currentPage.view.get_graphics().drawRect(0,0,1000,600);
			this.currentPage.view.get_graphics().endFill();
			break;
		}
	}
	,get_onButtonPressed: function() {
		return this._onButtonPressed;
	}
	,set_onButtonPressed: function(value) {
		return this._onButtonPressed = value;
	}
	,__class__: unbox.framework.view.NavBar
	,__properties__: $extend(openfl.display.Sprite.prototype.__properties__,{set_onButtonPressed:"set_onButtonPressed",get_onButtonPressed:"get_onButtonPressed"})
});
unbox.framework.view.NavBarAcc = function(navLayer,data) {
	openfl.display.Sprite.call(this);
	this.navLayer = navLayer;
	this.data = data;
	this.isChangingPage = false;
	this.init();
};
$hxClasses["unbox.framework.view.NavBarAcc"] = unbox.framework.view.NavBarAcc;
unbox.framework.view.NavBarAcc.__name__ = ["unbox","framework","view","NavBarAcc"];
unbox.framework.view.NavBarAcc.__interfaces__ = [unbox.framework.view.INavBar];
unbox.framework.view.NavBarAcc.__super__ = openfl.display.Sprite;
unbox.framework.view.NavBarAcc.prototype = $extend(openfl.display.Sprite.prototype,{
	init: function() {
		var navBarInstance = null;
		var navBarView;
		navBarView = js.Boot.__cast(navBarInstance , openfl.display.Sprite);
		navBarView.set_x(0);
		navBarView.set_y(0);
		this.navLayer.addChild(navBarView);
		var navBarBtns = ["","menuBtn","backBtn","","nextBtn"];
		this._buttons = new Array();
		var _g1 = 0;
		var _g = navBarBtns.length;
		while(_g1 < _g) {
			var i = _g1++;
			var btnView;
			btnView = js.Boot.__cast(navBarInstance.getChildByName(navBarBtns[i]) , openfl.display.Sprite);
			if(btnView == null) {
				unbox.framework.utils.Logger.log(navBarBtns[i] + " view does not exist! Please verify your nav.swf file.",{ fileName : "NavBarAcc.hx", lineNumber : 79, className : "unbox.framework.view.NavBarAcc", methodName : "init"});
				continue;
			}
			var button = new unbox.framework.view.button.GenericButton(btnView.get_x(),btnView.get_y(),btnView,navBarBtns[i]);
			navBarInstance.removeChild(btnView);
			button.addHandlers($bind(this,this.onButtonDown));
			button.add(navBarView);
			this._buttons.push(button);
		}
		this.onCustomKey = new msignal.Signal1();
		this.navLayer.stage.addEventListener(openfl.events.KeyboardEvent.KEY_DOWN,$bind(this,this.handleKeyPress));
		this._onButtonPressed = new msignal.Signal1();
		this.onDashboardToggle = new msignal.Signal0();
		this.onHelpToggle = new msignal.Signal0();
	}
	,updateNavView: function(page) {
		haxe.Log.trace("NavBar.updateNavView > page : " + Std.string(page),{ fileName : "NavBarAcc.hx", lineNumber : 101, className : "unbox.framework.view.NavBarAcc", methodName : "updateNavView"});
		if(page.data.get_pageProps() != null) {
			var _g1 = 0;
			var _g = page.data.get_pageProps().length;
			while(_g1 < _g) {
				var i = _g1++;
				var value;
				if(page.data.get_pageProps()[i] == 0) value = false; else value = true;
				if(this._buttons[i] != null) {
					this._buttons[i].setVisible(true);
					this._buttons[i].setEnabled(value);
				}
			}
		} else {
			haxe.Log.trace("NavBar ->> _buttons.length : " + this._buttons.length,{ fileName : "NavBarAcc.hx", lineNumber : 116, className : "unbox.framework.view.NavBarAcc", methodName : "updateNavView"});
			var _g11 = 0;
			var _g2 = this._buttons.length;
			while(_g11 < _g2) {
				var i1 = _g11++;
				if(this._buttons[i1].name == "nextBtn") this._buttons[i1].setEnabled(true); else this._buttons[i1].setVisible(false);
			}
		}
	}
	,setButtons: function(ids) {
		haxe.Log.trace("NavBar.setButtons > ids : " + ids,{ fileName : "NavBarAcc.hx", lineNumber : 126, className : "unbox.framework.view.NavBarAcc", methodName : "setButtons"});
		var values = ids.split("");
		var _g1 = 0;
		var _g = this._buttons.length;
		while(_g1 < _g) {
			var i = _g1++;
			var value = Std.parseInt(values[i]);
			if(value == 0) this._buttons[i].setEnabled(false); else this._buttons[i].setEnabled(true);
		}
		this.isChangingPage = false;
	}
	,afterPageChange: function(page) {
		this.isChangingPage = false;
	}
	,onButtonDown: function(e) {
		if(this.isChangingPage) return;
		var btnId = 0;
		var _g = e.name;
		switch(_g) {
		case "backBtn":
			btnId = -1;
			this.isChangingPage = true;
			break;
		case "nextBtn":
			btnId = 1;
			this.isChangingPage = true;
			break;
		case "menuBtn":
			btnId = 0;
			this.isChangingPage = true;
			break;
		case "helpBtn":
			unbox.framework.utils.Logger.log("helpBtn logic still not implemented",{ fileName : "NavBarAcc.hx", lineNumber : 160, className : "unbox.framework.view.NavBarAcc", methodName : "onButtonDown"});
			break;
		case "bookmarkBtn":
			unbox.framework.utils.Logger.log("bookmarkBtn logic still not implemented",{ fileName : "NavBarAcc.hx", lineNumber : 162, className : "unbox.framework.view.NavBarAcc", methodName : "onButtonDown"});
			break;
		default:
			unbox.framework.utils.Logger.log("Button name not recognized : " + Std.string(e.target.name),{ fileName : "NavBarAcc.hx", lineNumber : 164, className : "unbox.framework.view.NavBarAcc", methodName : "onButtonDown"});
		}
		this._onButtonPressed.dispatch(btnId);
	}
	,handleKeyPress: function(e) {
		if(this.isChangingPage == true) {
			e.stopImmediatePropagation();
			return;
		}
		var _g = e.keyCode;
		switch(_g) {
		case 101:
			(js.Boot.__cast(this._buttons[0] , unbox.framework.view.button.GenericButton)).activate();
			break;
		case 100:
			(js.Boot.__cast(this._buttons[1] , unbox.framework.view.button.GenericButton)).activate();
			break;
		case 102:
			(js.Boot.__cast(this._buttons[2] , unbox.framework.view.button.GenericButton)).activate();
			break;
		case 32:
			e.target.dispatchEvent(new unbox.framework.view.button.ButtonEvent(unbox.framework.view.button.ButtonEvent.ACTIVATE,e.target,true));
			break;
		}
	}
	,setBookmarkStatus: function(status) {
	}
	,get_onButtonPressed: function() {
		return this._onButtonPressed;
	}
	,set_onButtonPressed: function(value) {
		return this._onButtonPressed = value;
	}
	,__class__: unbox.framework.view.NavBarAcc
	,__properties__: $extend(openfl.display.Sprite.prototype.__properties__,{set_onButtonPressed:"set_onButtonPressed",get_onButtonPressed:"get_onButtonPressed"})
});
unbox.framework.view.button = {};
unbox.framework.view.button.ButtonEvent = function(type,data,bubbles,cancelable) {
	if(cancelable == null) cancelable = false;
	if(bubbles == null) bubbles = false;
	openfl.events.Event.call(this,type,bubbles,cancelable);
	this.data = data;
};
$hxClasses["unbox.framework.view.button.ButtonEvent"] = unbox.framework.view.button.ButtonEvent;
unbox.framework.view.button.ButtonEvent.__name__ = ["unbox","framework","view","button","ButtonEvent"];
unbox.framework.view.button.ButtonEvent.__super__ = openfl.events.Event;
unbox.framework.view.button.ButtonEvent.prototype = $extend(openfl.events.Event.prototype,{
	clone: function() {
		return new unbox.framework.view.button.ButtonEvent(this.type,this.bubbles,this.cancelable);
	}
	,__class__: unbox.framework.view.button.ButtonEvent
});
unbox.framework.view.button.IButton = function() { };
$hxClasses["unbox.framework.view.button.IButton"] = unbox.framework.view.button.IButton;
unbox.framework.view.button.IButton.__name__ = ["unbox","framework","view","button","IButton"];
unbox.framework.view.button.IButton.prototype = {
	__class__: unbox.framework.view.button.IButton
};
unbox.framework.view.button.GenericButton = function(x,y,view,name) {
	this._view = view;
	this._view.addEventListener(unbox.framework.view.button.ButtonEvent.ACTIVATE,$bind(this,this.onButtonActivated));
	this._view.set_x(x);
	this._view.set_y(y);
	this.enabled = true;
	this._visible = true;
	if(view.get_name().indexOf("instance") > -1) this.name = this._view.set_name(name); else this.name = name;
	this.init();
};
$hxClasses["unbox.framework.view.button.GenericButton"] = unbox.framework.view.button.GenericButton;
unbox.framework.view.button.GenericButton.__name__ = ["unbox","framework","view","button","GenericButton"];
unbox.framework.view.button.GenericButton.__interfaces__ = [unbox.framework.view.button.IButton];
unbox.framework.view.button.GenericButton.prototype = {
	init: function() {
		this.upState = this._view.getChildByName("up");
		this.downState = this._view.getChildByName("down");
		this.disabledState = this._view.getChildByName("disabled");
		this.overState = this._view.getChildByName("over");
		this._view.addEventListener(openfl.events.MouseEvent.ROLL_OVER,$bind(this,this.handleOver));
		this._view.addEventListener(openfl.events.MouseEvent.ROLL_OUT,$bind(this,this.handleOut));
		this._view.addEventListener(openfl.events.MouseEvent.MOUSE_DOWN,$bind(this,this.handleDown));
		this._view.addEventListener(openfl.events.MouseEvent.MOUSE_UP,$bind(this,this.handleUp));
		this.upState.set_visible(true);
		this.overState.set_visible(false);
		this.disabledState.set_visible(false);
		this.downState.set_visible(false);
		this._view.buttonMode = true;
		this._view.mouseChildren = false;
		this._view.tabChildren = false;
	}
	,add: function(parent) {
		parent.addChild(this._view);
	}
	,remove: function(parent) {
		this._view.removeEventListener(openfl.events.MouseEvent.ROLL_OVER,$bind(this,this.handleOver));
		this._view.removeEventListener(openfl.events.MouseEvent.ROLL_OUT,$bind(this,this.handleOut));
		this._view.removeEventListener(openfl.events.MouseEvent.MOUSE_DOWN,$bind(this,this.handleDown));
		this._view.removeEventListener(openfl.events.MouseEvent.MOUSE_UP,$bind(this,this.handleUp));
		this.upState = null;
		this.downState = null;
		this.disabledState = null;
		this.overState = null;
		parent.removeChild(this._view);
	}
	,addHandlers: function(mouseDown,mouseUp) {
		if(mouseDown != null) {
			this.downSignal = new msignal.Signal1();
			this.downSignal.add(mouseDown);
		}
		if(mouseUp != null) {
			this.upSignal = new msignal.Signal1();
			this.upSignal.add(mouseUp);
		}
	}
	,setEnabled: function(value) {
		this.enabled = value;
		if(!this.enabled) {
			this.upState.set_visible(false);
			this.overState.set_visible(false);
			this.disabledState.set_visible(true);
			this.downState.set_visible(false);
			this._view.useHandCursor = false;
		} else {
			this._view.useHandCursor = true;
			this.upState.set_visible(true);
			this.overState.set_visible(false);
			this.disabledState.set_visible(false);
			this.downState.set_visible(false);
		}
	}
	,setVisible: function(value) {
		this._visible = this._view.set_visible(value);
		if(this._visible) {
			if(this.enabled) {
				this.upState.set_visible(true);
				this.overState.set_visible(false);
				this.disabledState.set_visible(false);
				this.downState.set_visible(false);
			} else {
				this.upState.set_visible(false);
				this.overState.set_visible(false);
				this.disabledState.set_visible(true);
				this.downState.set_visible(false);
			}
		}
	}
	,handleUp: function(e) {
		if(!this.enabled) return;
		if(this.isOver) {
			this.upState.set_visible(false);
			this.overState.set_visible(true);
			this.disabledState.set_visible(false);
			this.downState.set_visible(false);
		} else {
			this.upState.set_visible(true);
			this.overState.set_visible(false);
			this.disabledState.set_visible(false);
			this.downState.set_visible(false);
		}
		if(this.upSignal != null) this.upSignal.dispatch(this);
	}
	,handleDown: function(e) {
		if(!this.enabled) return;
		this.upState.set_visible(false);
		this.overState.set_visible(false);
		this.disabledState.set_visible(false);
		this.downState.set_visible(true);
		if(this.downSignal != null) this.downSignal.dispatch(this);
	}
	,handleOut: function(e) {
		if(!this.enabled) return;
		this.upState.set_visible(true);
		this.overState.set_visible(false);
		this.disabledState.set_visible(false);
		this.downState.set_visible(false);
		if(this.isOver) this.isOver = false;
	}
	,handleOver: function(e) {
		if(!this.enabled) return;
		this.upState.set_visible(false);
		this.overState.set_visible(true);
		this.disabledState.set_visible(false);
		this.downState.set_visible(false);
		if(!this.isOver) this.isOver = true;
	}
	,flip: function(horizontal,vertical) {
		if(horizontal == true) {
			this._view.set_scaleX(-1);
			var _g = this.upState;
			_g.set_x(_g.get_x() - this.upState.get_width());
			var _g1 = this.overState;
			_g1.set_x(_g1.get_x() - this.overState.get_width());
			var _g2 = this.disabledState;
			_g2.set_x(_g2.get_x() - this.disabledState.get_width());
			var _g3 = this.downState;
			_g3.set_x(_g3.get_x() - this.downState.get_width());
		}
		if(vertical == true) {
			this._view.set_scaleY(-1);
			var _g4 = this.upState;
			_g4.set_y(_g4.get_y() - this.upState.get_height());
			var _g5 = this.overState;
			_g5.set_y(_g5.get_y() - this.overState.get_height());
			var _g6 = this.disabledState;
			_g6.set_y(_g6.get_y() - this.disabledState.get_height());
			var _g7 = this.downState;
			_g7.set_y(_g7.get_y() - this.downState.get_height());
		}
	}
	,activate: function(data) {
		if(this.downSignal.get_numListeners() > 0) {
			if(data != null) this.downSignal.dispatch(data); else this.downSignal.dispatch(this.get_view());
		} else if(this.upSignal.get_numListeners() > 0) {
			if(data != null) this.upSignal.dispatch(data); else this.upSignal.dispatch(this.get_view());
		}
	}
	,onButtonActivated: function(e) {
		this.activate();
	}
	,get_x: function() {
		return this._x;
	}
	,set_x: function(value) {
		this._view.set_x(value);
		return this._x = value;
	}
	,get_y: function() {
		return this._y;
	}
	,set_y: function(value) {
		this._view.set_y(value);
		return this._y = value;
	}
	,get_view: function() {
		return this._view;
	}
	,get_visible: function() {
		return this._visible;
	}
	,set_visible: function(value) {
		return this._visible = value;
	}
	,get_label: function() {
		return this._label;
	}
	,set_label: function(value) {
		this._label = value;
		return this._label;
	}
	,__class__: unbox.framework.view.button.GenericButton
	,__properties__: {set_label:"set_label",get_label:"get_label",set_visible:"set_visible",get_visible:"get_visible",get_view:"get_view",set_y:"set_y",get_y:"get_y",set_x:"set_x",get_x:"get_x"}
};
unbox.framework.view.button.StyleButton = function(x,y,view,name) {
	unbox.framework.view.button.GenericButton.call(this,x,y,view,name);
	this.isSelected = false;
};
$hxClasses["unbox.framework.view.button.StyleButton"] = unbox.framework.view.button.StyleButton;
unbox.framework.view.button.StyleButton.__name__ = ["unbox","framework","view","button","StyleButton"];
unbox.framework.view.button.StyleButton.__super__ = unbox.framework.view.button.GenericButton;
unbox.framework.view.button.StyleButton.prototype = $extend(unbox.framework.view.button.GenericButton.prototype,{
	init: function() {
		unbox.framework.view.button.GenericButton.prototype.init.call(this);
	}
	,setClasses: function(over,down,up,selected) {
		this.classes = new Array();
		this.classes.push("<p class=\"" + over + "\">");
		this.classes.push("<p class=\"" + down + "\">");
		this.classes.push("<p class=\"" + up + "\">");
		if(selected != null) this.classes.push("<p class=\"" + selected + "\">");
		this.currentState = this.classes[2];
	}
	,handleOver: function(e) {
		unbox.framework.view.button.GenericButton.prototype.handleOver.call(this,e);
		if(!this.isSelected) {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[0]));
			this.currentState = this.classes[0];
		}
	}
	,handleOut: function(e) {
		unbox.framework.view.button.GenericButton.prototype.handleOut.call(this,e);
		if(!this.isSelected) {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[2]));
			this.currentState = this.classes[2];
		} else {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[3]));
			this.currentState = this.classes[3];
		}
	}
	,handleDown: function(e) {
		unbox.framework.view.button.GenericButton.prototype.handleDown.call(this,e);
		this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[1]));
		this.currentState = this.classes[1];
	}
	,handleUp: function(e) {
		unbox.framework.view.button.GenericButton.prototype.handleUp.call(this,e);
		if(!this.isSelected) {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[0]));
			this.currentState = this.classes[0];
		} else {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[3]));
			this.currentState = this.classes[3];
		}
	}
	,setSelected: function(value) {
		this.isSelected = value;
		if(!this.isSelected) {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[2]));
			this.currentState = this.classes[2];
		}
	}
	,activate: function(data) {
		if(this.downSignal.get_numListeners() > 0) {
			this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[1]));
			this.currentState = this.classes[1];
			if(data != null) this.downSignal.dispatch(data); else this.downSignal.dispatch(this.get_view());
		} else if(this.upSignal.get_numListeners() > 0) {
			if(!this.isSelected) {
				this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[0]));
				this.currentState = this.classes[0];
			} else {
				this._label.set_htmlText(StringTools.replace(this._label.get_htmlText(),this.currentState,this.classes[3]));
				this.currentState = this.classes[3];
			}
			if(data != null) this.upSignal.dispatch(data); else this.upSignal.dispatch(this.get_view());
		}
	}
	,__class__: unbox.framework.view.button.StyleButton
});
unbox.framework.view.button.ToggleButton = function(x,y,view,name) {
	this.view = view;
	view.addEventListener(unbox.framework.view.button.ButtonEvent.ACTIVATE,$bind(this,this.onButtonActivated));
	this.offBtn = new unbox.framework.view.button.GenericButton(0,0,js.Boot.__cast(this.get_view().getChildByName("offView") , openfl.display.Sprite),name);
	this.onBtn = new unbox.framework.view.button.GenericButton(0,0,js.Boot.__cast(this.get_view().getChildByName("onView") , openfl.display.Sprite),name);
	this.onBtn.setVisible(false);
	this.isToggled = false;
	this.enabled = true;
	this.visible = true;
	this.name = name;
};
$hxClasses["unbox.framework.view.button.ToggleButton"] = unbox.framework.view.button.ToggleButton;
unbox.framework.view.button.ToggleButton.__name__ = ["unbox","framework","view","button","ToggleButton"];
unbox.framework.view.button.ToggleButton.__interfaces__ = [unbox.framework.view.button.IButton];
unbox.framework.view.button.ToggleButton.prototype = {
	add: function(parent) {
		this.get_view().addEventListener(openfl.events.Event.ADDED,$bind(this,this.onAdded));
		parent.addChild(this.get_view());
	}
	,onAdded: function(e) {
		this.get_view().removeEventListener(openfl.events.Event.ADDED,$bind(this,this.onAdded));
		this.onBtn.add(e.target);
		this.offBtn.add(e.target);
	}
	,remove: function(parent) {
		parent.removeChild(this.get_view());
	}
	,addHandlers: function(mouseDown,mouseUp) {
		if(mouseDown != null) {
			this.downSignal = new msignal.Signal1();
			this.downSignal.add(mouseDown);
			this.offBtn.addHandlers($bind(this,this.handleMouseDown));
			this.onBtn.addHandlers($bind(this,this.handleMouseDown));
		}
		if(mouseUp != null) {
			this.upSignal = new msignal.Signal1();
			this.upSignal.add(mouseUp);
			this.offBtn.addHandlers(null,$bind(this,this.handleMouseUp));
			this.onBtn.addHandlers(null,$bind(this,this.handleMouseUp));
		}
	}
	,handleMouseDown: function(target) {
		if(this.isToggled) {
			this.onBtn.setVisible(false);
			this.offBtn.setVisible(true);
			this.isToggled = false;
		} else {
			this.onBtn.setVisible(true);
			this.offBtn.setVisible(false);
			this.isToggled = true;
		}
		this.downSignal.dispatch(target);
	}
	,handleMouseUp: function(target) {
		if(this.isToggled) {
			this.onBtn.setVisible(false);
			this.offBtn.setVisible(true);
			this.isToggled = false;
		} else {
			this.onBtn.setVisible(true);
			this.offBtn.setVisible(false);
			this.isToggled = true;
		}
		this.upSignal.dispatch(target);
	}
	,onButtonActivated: function(e) {
		haxe.Log.trace("Button activated! Data : " + Std.string(e.data),{ fileName : "ToggleButton.hx", lineNumber : 127, className : "unbox.framework.view.button.ToggleButton", methodName : "onButtonActivated"});
		this.checkToggle();
	}
	,checkToggle: function() {
		if(this.isToggled) this.offBtn.activate(); else this.onBtn.activate();
	}
	,updateView: function() {
		if(this.isToggled) {
			this.onBtn.setVisible(true);
			this.offBtn.setVisible(false);
		} else {
			this.onBtn.setVisible(false);
			this.offBtn.setVisible(true);
		}
	}
	,setToggle: function(status) {
		this.isToggled = status;
		this.updateView();
	}
	,flip: function(horizontal,vertical) {
		this.offBtn.flip(horizontal,vertical);
		this.onBtn.flip(horizontal,vertical);
	}
	,setEnabled: function(value) {
		this.offBtn.setEnabled(value);
		this.onBtn.setEnabled(value);
	}
	,setVisible: function(value) {
		this.get_view().set_visible(value);
	}
	,get_view: function() {
		return this.view;
	}
	,__class__: unbox.framework.view.button.ToggleButton
	,__properties__: {get_view:"get_view"}
};
unbox.framework.view.components = {};
unbox.framework.view.components.IPanel = function() { };
$hxClasses["unbox.framework.view.components.IPanel"] = unbox.framework.view.components.IPanel;
unbox.framework.view.components.IPanel.__name__ = ["unbox","framework","view","components","IPanel"];
unbox.framework.view.components.IPanel.prototype = {
	__class__: unbox.framework.view.components.IPanel
};
unbox.framework.view.components.AboutPanel = function(parent,data,mcLibrary) {
	this.parent = parent;
	this.data = data;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.AboutPanel"] = unbox.framework.view.components.AboutPanel;
unbox.framework.view.components.AboutPanel.__name__ = ["unbox","framework","view","components","AboutPanel"];
unbox.framework.view.components.AboutPanel.__interfaces__ = [unbox.framework.view.components.IPanel];
unbox.framework.view.components.AboutPanel.prototype = {
	init: function(mcLibrary) {
		this.view = js.Boot.__cast(mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.view.set_alpha(0);
		this.view.set_visible(false);
		this.parent.addChild(this.view);
		motion.Actuate.timer(0.1).onComplete($bind(this,this.insertTexts));
	}
	,insertTexts: function() {
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.iterator();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var textData = $it0.next();
			var txt = unbox.framework.utils.ContentParser.parseTextField(textData,this.view);
		}
	}
	,show: function() {
		motion.Actuate.tween(this.view,.25,{ alpha : 1}).ease(motion.easing.Quint.get_easeOut());
	}
	,hide: function() {
		motion.Actuate.tween(this.view,.25,{ alpha : 0}).ease(motion.easing.Quint.get_easeOut());
	}
	,__class__: unbox.framework.view.components.AboutPanel
};
unbox.framework.view.components.BarMeter = function(parent,data) {
	this.current = 1;
	this.steps = 1;
	this.barColor = 11411234;
	this.barColorAlpha = 1;
	this.thumbColor = 16777215;
	this.thumbColorAlpha = 1;
	this.autoHideThumb = false;
	this.parent = parent;
	this.data = data;
	this.hasInit = false;
	this.init();
};
$hxClasses["unbox.framework.view.components.BarMeter"] = unbox.framework.view.components.BarMeter;
unbox.framework.view.components.BarMeter.__name__ = ["unbox","framework","view","components","BarMeter"];
unbox.framework.view.components.BarMeter.prototype = {
	init: function() {
		this.view = new openfl.display.Sprite();
		this.view.set_name("barMeterView");
		this.parent.addChild(this.view);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.barWidth = this.data.get_width();
		this.barHeight = this.data.get_height();
		this.barColor = this.data.get_scrollbarColor();
		this.barColorAlpha = this.data.get_scrollbarColorAlpha();
		this.thumbColor = this.data.get_thumbColor();
		this.thumbColorAlpha = this.data.get_thumbColorAlpha();
		this.thumbFillMode = this.data.get_thumbFillMode();
		this.autoHideThumb = this.data.get_autoHideThumb();
		this.scrollbar = new openfl.display.Sprite();
		this.thumb = new openfl.display.Sprite();
		this.view.addChild(this.scrollbar);
		this.view.addChild(this.thumb);
		this.isVerticalScrolling = this.barHeight > this.barWidth;
		this.draw();
		this.update();
		this.hasInit = true;
	}
	,draw: function() {
		if(this.steps < 1) this.steps = 1; else this.steps = this.steps;
		this.scrollbar.get_graphics().clear();
		this.scrollbar.get_graphics().beginFill(this.barColor,this.barColorAlpha);
		this.scrollbar.get_graphics().drawRect(0,0,this.barWidth,this.barHeight);
		this.scrollbar.get_graphics().endFill();
		if(this.isVerticalScrolling) this.thumbWidth = this.barWidth; else this.thumbWidth = this.barWidth / this.steps;
		if(this.isVerticalScrolling) this.thumbHeight = this.barHeight / this.steps; else this.thumbHeight = this.barHeight;
		this.thumb.get_graphics().clear();
		this.thumb.get_graphics().beginFill(this.thumbColor,this.thumbColorAlpha);
		this.thumb.get_graphics().drawRect(0,0,this.thumbWidth,this.thumbHeight);
		this.thumb.get_graphics().endFill();
	}
	,update: function() {
		var _g = this;
		if(this.current > this.steps) this.current = this.steps; else this.current = this.current;
		if(this.current < 1) this.current = 1; else this.current = this.current;
		if(this.thumbFillMode) {
			var nW;
			if(this.isVerticalScrolling) nW = this.thumbWidth; else nW = this.thumbWidth * this.current;
			var nH;
			if(this.isVerticalScrolling) nH = this.thumbHeight * this.current; else nH = this.thumbHeight;
			motion.Actuate.tween(this.thumb,0.25,{ width : nW, height : nH, alpha : 1},true).onComplete((function($this) {
				var $r;
				var f = function() {
					if(_g.autoHideThumb) motion.Actuate.tween(_g.thumb,1,{ alpha : 0}).delay(.5);
				};
				$r = f;
				return $r;
			}(this)));
		} else {
			var nY;
			if(this.isVerticalScrolling) nY = this.thumbHeight * (this.current - 1); else nY = 0;
			var nX;
			if(this.isVerticalScrolling) nX = 0; else nX = this.thumbWidth * (this.current - 1);
			motion.Actuate.tween(this.thumb,0.25,{ x : nX, y : nY, alpha : 1},true).onComplete((function($this) {
				var $r;
				var f1 = function() {
					if(_g.autoHideThumb) motion.Actuate.tween(_g.thumb,1,{ alpha : 0}).delay(.5);
				};
				$r = f1;
				return $r;
			}(this)));
		}
	}
	,setCurrentIndex: function(index) {
		this.current = index;
		if(this.hasInit) this.update();
	}
	,setProgress: function(current,total) {
		var lastTotal = this.steps;
		this.current = current + 1;
		this.steps = total;
		if(lastTotal != total) this.draw();
		if(this.hasInit) this.update();
	}
	,show: function() {
		motion.Actuate.tween(this.view,0.25,{ alpha : 1});
	}
	,hide: function() {
		motion.Actuate.tween(this.view,0.25,{ alpha : 0});
	}
	,__class__: unbox.framework.view.components.BarMeter
};
unbox.framework.view.components.BookmarkPanel = function(parent,data,mcLibrary) {
	this.parent = parent;
	this.data = data;
	this.hasInit = false;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.BookmarkPanel"] = unbox.framework.view.components.BookmarkPanel;
unbox.framework.view.components.BookmarkPanel.__name__ = ["unbox","framework","view","components","BookmarkPanel"];
unbox.framework.view.components.BookmarkPanel.__interfaces__ = [unbox.framework.view.components.IPanel];
unbox.framework.view.components.BookmarkPanel.arrayCompare = function(a,b) {
	if(a.get_globalIndex() > b.get_globalIndex()) return 1; else if(a.get_globalIndex() < b.get_globalIndex()) return -1; else return 0;
};
unbox.framework.view.components.BookmarkPanel.prototype = {
	init: function(mcLibrary) {
		this.view = js.Boot.__cast(mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.view.set_alpha(0);
		this.view.set_visible(false);
		this.parent.addChild(this.view);
		this.buttons = new Array();
		var _g = 0;
		while(_g < 2) {
			var i = _g++;
			var btnView;
			btnView = js.Boot.__cast(this.view.getChildByName("btn_" + i) , openfl.display.Sprite);
			var button = new unbox.framework.view.button.GenericButton(btnView.get_x(),btnView.get_y(),btnView,"button_" + i);
			button.addHandlers($bind(this,this.onButtonDown));
			this.view.removeChild(btnView);
			button.add(this.view);
			this.buttons.push(button);
		}
		this.list = new unbox.framework.view.components.UIList(this.view,this.data.get_list(),mcLibrary);
		this.list.onChange.add($bind(this,this.onListChange));
		this.list.onClick.add($bind(this,this.onListClick));
		this.scrollbar = new unbox.framework.view.components.Scrollbar(this.view,this.data.get_scrollbar());
		motion.Actuate.timer(0.1).onComplete($bind(this,this.insertTexts));
		this.view.addEventListener(openfl.events.MouseEvent.MOUSE_WHEEL,$bind(this,this.onMouseWheel));
		this.bookmarks = new Array();
		this.hasInit = true;
	}
	,insertTexts: function() {
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.iterator();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var textData = $it0.next();
			var txt = unbox.framework.utils.ContentParser.parseTextField(textData,this.view,this);
			if(txt.get_name() == "statusTxt") {
				this.status = unbox.framework.utils.ContentParser.removeCDATA(textData.node.resolve("title").get_innerHTML());
				this.statusTxt = txt;
			}
		}
	}
	,onButtonDown: function(e) {
		var btnIndex = Std.int(e.name.split("_")[1]);
		switch(btnIndex) {
		case 0:
			this.list.backPage();
			break;
		case 1:
			this.list.nextPage();
			break;
		}
	}
	,onMouseWheel: function(e) {
		if(e.delta > 0) this.list.backPage(); else this.list.nextPage();
	}
	,onListChange: function(pList) {
		var isNextEnabled;
		isNextEnabled = (function($this) {
			var $r;
			var a = pList.totalItems;
			$r = a < 0?0 >= 0?false:a > 0:0 >= 0?a > 0:true;
			return $r;
		}(this)) && !(pList.currentPage == pList.totalPages - 1);
		var isBackEnabled;
		isBackEnabled = (function($this) {
			var $r;
			var a1 = pList.totalItems;
			$r = a1 < 0?0 >= 0?false:a1 > 0:0 >= 0?a1 > 0:true;
			return $r;
		}(this)) && pList.currentPage != 0;
		if(this.scrollbar != null) {
			this.scrollbar.set_steps(pList.totalPages);
			this.scrollbar.set_current(pList.currentPage + 1);
			this.buttons[0].setEnabled(isBackEnabled);
			this.buttons[1].setEnabled(isNextEnabled);
		}
		if(this.hasInit) this.updateStatus();
	}
	,updateStatus: function() {
		if((function($this) {
			var $r;
			var a = $this.list.totalItems;
			$r = a < 0?0 >= 0?false:a > 0:0 >= 0?a > 0:true;
			return $r;
		}(this))) {
			this.firstItem = this.list.maxItemsPerPage * this.list.currentPage + 1;
			this.lastItem = this.list.maxItemsPerPage * (this.list.currentPage + 1);
			if((function($this) {
				var $r;
				var a1 = $this.lastItem;
				var b = $this.list.totalItems;
				$r = a1 < 0?b >= 0?false:a1 > b:b >= 0?a1 > b:true;
				return $r;
			}(this))) this.lastItem = this.list.totalItems; else this.lastItem = this.lastItem;
			this.totalItems = this.list.totalItems;
			this.statusTxt.set_htmlText(unbox.framework.utils.StringUtils.parseTextVars(this.status,this));
		} else this.statusTxt.set_text("NULL");
	}
	,onListClick: function(pList,listButton) {
		var btnId = Std.parseInt(listButton.name.split("_")[1]);
		haxe.Log.trace("Bookmark panel item '" + btnId + "' clicked.",{ fileName : "BookmarkPanel.hx", lineNumber : 186, className : "unbox.framework.view.components.BookmarkPanel", methodName : "onListClick"});
	}
	,insert: function(page) {
		this.pageTitle = page.get_title();
		this.pageModuleIndex = page.get_moduleIndex() + 1;
		this.pageLocalIndex = page.get_localIndex() + 1;
		var labelNode;
		var this1 = this.data.get_texts();
		labelNode = this1.get("itemLabel");
		this.list.insert(labelNode,this);
		this.bookmarks.push(page);
		this.list.update();
	}
	,remove: function(page) {
		var _g1 = 0;
		var _g = this.bookmarks.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.bookmarks[i].get_globalIndex() == page.get_globalIndex()) {
				this.bookmarks.splice(i,1);
				this.list.remove(i);
				break;
			}
		}
		this.list.update();
	}
	,update: function(id) {
		if(id == null) id = -1;
		this.list.update();
	}
	,setBookmarks: function(added,bookmarkData) {
		if(added) this.insert(bookmarkData); else this.remove(bookmarkData);
	}
	,show: function() {
		this.view.set_visible(true);
		motion.Actuate.tween(this.view,.25,{ alpha : 1}).ease(motion.easing.Quint.get_easeOut());
	}
	,hide: function() {
		var _g = this;
		motion.Actuate.tween(this.view,.25,{ alpha : 0}).ease(motion.easing.Quint.get_easeOut()).onComplete((function($this) {
			var $r;
			var f = function() {
				_g.view.set_visible(false);
			};
			$r = f;
			return $r;
		}(this)));
	}
	,__class__: unbox.framework.view.components.BookmarkPanel
};
unbox.framework.view.components.HelpPanel = function(parent,data,mcLibrary) {
	this.parent = parent;
	this.data = data;
	this.onUserClick = new msignal.Signal0();
	this.isVisible = false;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.HelpPanel"] = unbox.framework.view.components.HelpPanel;
unbox.framework.view.components.HelpPanel.__name__ = ["unbox","framework","view","components","HelpPanel"];
unbox.framework.view.components.HelpPanel.__interfaces__ = [unbox.framework.view.components.IPanel];
unbox.framework.view.components.HelpPanel.prototype = {
	init: function(mcLibrary) {
		this.view = js.Boot.__cast(mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.view.set_alpha(0);
		this.view.set_visible(false);
		this.parent.addChild(this.view);
		motion.Actuate.timer(0.1).onComplete($bind(this,this.insertTexts));
	}
	,insertTexts: function() {
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.iterator();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var textData = $it0.next();
			var txt = unbox.framework.utils.ContentParser.parseTextField(textData,this.view);
		}
	}
	,show: function() {
		var _g = this;
		motion.Actuate.tween(this.view,0.1,{ x : 0}).onComplete((function($this) {
			var $r;
			var f = function() {
				motion.Actuate.tween(_g.view,.25,{ alpha : 1}).ease(motion.easing.Quint.get_easeOut());
				_g.view.useHandCursor = true;
				_g.view.addEventListener(openfl.events.MouseEvent.CLICK,$bind(_g,_g.onMouseClick));
				_g.isVisible = true;
			};
			$r = f;
			return $r;
		}(this)));
	}
	,hide: function() {
		var _g = this;
		motion.Actuate.tween(this.view,.25,{ alpha : 0}).ease(motion.easing.Quint.get_easeOut()).onComplete((function($this) {
			var $r;
			var f = function() {
				motion.Actuate.tween(_g.view,.1,{ x : -1000});
				_g.view.useHandCursor = false;
				_g.view.removeEventListener(openfl.events.MouseEvent.CLICK,$bind(_g,_g.onMouseClick));
				_g.isVisible = false;
			};
			$r = f;
			return $r;
		}(this)));
	}
	,onMouseClick: function(e) {
		this.onUserClick.dispatch();
	}
	,__class__: unbox.framework.view.components.HelpPanel
};
unbox.framework.view.components.IndexPanel = function(parent,data,mcLibrary) {
	this.data = data;
	this.parent = parent;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.IndexPanel"] = unbox.framework.view.components.IndexPanel;
unbox.framework.view.components.IndexPanel.__name__ = ["unbox","framework","view","components","IndexPanel"];
unbox.framework.view.components.IndexPanel.__interfaces__ = [unbox.framework.view.components.IPanel];
unbox.framework.view.components.IndexPanel.prototype = {
	init: function(mcLibrary) {
		this.view = js.Boot.__cast(mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.view.set_alpha(0);
		this.view.set_visible(false);
		this.parent.addChild(this.view);
		this.buttons = new Array();
		var _g = 0;
		while(_g < 4) {
			var i = _g++;
			var btnView;
			btnView = js.Boot.__cast(this.view.getChildByName("btn_" + i) , openfl.display.Sprite);
			var button = new unbox.framework.view.button.GenericButton(btnView.get_x(),btnView.get_y(),btnView,"button_" + i);
			button.addHandlers($bind(this,this.onButtonDown));
			this.view.removeChild(btnView);
			button.add(this.view);
			this.buttons.push(button);
		}
		this.list = new unbox.framework.view.components.UIList(this.view,this.data.get_menuLists()[0],mcLibrary);
		this.list.onChange.add($bind(this,this.onListChange));
		this.list.onClick.add($bind(this,this.onListClick));
		this.subList = new unbox.framework.view.components.UIList(this.view,this.data.get_menuLists()[1],mcLibrary);
		this.subList.onChange.add($bind(this,this.onListChange));
		this.subList.onClick.add($bind(this,this.onListClick));
		motion.Actuate.timer(0.1).onComplete($bind(this,this.insertTexts));
		this.listScrollbar = new unbox.framework.view.components.Scrollbar(this.view,this.data.get_scrollbars()[0]);
		this.subListScrollbar = new unbox.framework.view.components.Scrollbar(this.view,this.data.get_scrollbars()[1]);
		this.view.addEventListener(openfl.events.MouseEvent.MOUSE_WHEEL,$bind(this,this.onMouseWheel));
	}
	,insertTexts: function() {
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.iterator();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var textData = $it0.next();
			var txt = unbox.framework.utils.ContentParser.parseTextField(textData,this.view);
		}
		this.populateList(this.list);
	}
	,onButtonDown: function(e) {
		haxe.Log.trace("Button '" + Std.string(e.name) + "' clicked!",{ fileName : "IndexPanel.hx", lineNumber : 119, className : "unbox.framework.view.components.IndexPanel", methodName : "onButtonDown"});
		var btnIndex = Std.int(e.name.split("_")[1]);
		switch(btnIndex) {
		case 0:
			this.list.backPage();
			break;
		case 1:
			this.list.nextPage();
			break;
		case 2:
			this.subList.backPage();
			break;
		case 3:
			this.subList.nextPage();
			break;
		}
	}
	,onMouseWheel: function(e) {
		var tList;
		if(e.stageX > this.view.stage.stageWidth / 2) tList = this.subList; else tList = this.list;
		if(e.delta > 0) tList.backPage(); else tList.nextPage();
	}
	,populateList: function(pList,id) {
		if(pList != null) {
			if(id != null) {
				this.subList.destroy();
				var _g1 = 0;
				var _g = this.data.get_menuData()[id].get_submenuItems().length;
				while(_g1 < _g) {
					var i = _g1++;
					this.subList.insert(this.data.get_menuData()[id].get_submenuItems()[i].get_title());
				}
				this.subList.update();
			} else {
				this.list.destroy();
				var _g11 = 0;
				var _g2 = this.data.get_menuData().length;
				while(_g11 < _g2) {
					var i1 = _g11++;
					this.list.insert(this.data.get_menuData()[i1].get_title());
				}
				this.list.update();
			}
		}
	}
	,onListChange: function(pList) {
		var isNextEnabled;
		isNextEnabled = (function($this) {
			var $r;
			var a = pList.totalItems;
			$r = a < 0?0 >= 0?false:a > 0:0 >= 0?a > 0:true;
			return $r;
		}(this)) && !(pList.currentPage == pList.totalPages - 1);
		var isBackEnabled;
		isBackEnabled = (function($this) {
			var $r;
			var a1 = pList.totalItems;
			$r = a1 < 0?0 >= 0?false:a1 > 0:0 >= 0?a1 > 0:true;
			return $r;
		}(this)) && pList.currentPage != 0;
		if(pList == this.list && this.listScrollbar != null) {
			this.listScrollbar.set_steps(pList.totalPages);
			this.listScrollbar.set_current(pList.currentPage + 1);
			this.buttons[0].setEnabled(isBackEnabled);
			this.buttons[1].setEnabled(isNextEnabled);
		} else if(pList == this.subList && this.subListScrollbar != null) {
			this.subListScrollbar.set_steps(pList.totalPages);
			this.subListScrollbar.set_current(pList.currentPage + 1);
			this.buttons[2].setEnabled(isBackEnabled);
			this.buttons[3].setEnabled(isNextEnabled);
		}
	}
	,onListClick: function(pList,listButton) {
		haxe.Log.trace("IndexPanel.onListClick > listButton : " + listButton.name,{ fileName : "IndexPanel.hx", lineNumber : 189, className : "unbox.framework.view.components.IndexPanel", methodName : "onListClick"});
		var btnId = Std.parseInt(listButton.name.split("_")[1]);
		if(pList == this.list) this.populateList(this.subList,btnId); else {
		}
	}
	,show: function() {
		this.view.set_visible(true);
		motion.Actuate.tween(this.view,.25,{ alpha : 1}).ease(motion.easing.Quint.get_easeOut());
	}
	,hide: function() {
		var _g = this;
		motion.Actuate.tween(this.view,.25,{ alpha : 0}).ease(motion.easing.Quint.get_easeOut()).onComplete((function($this) {
			var $r;
			var f = function() {
				_g.view.set_visible(false);
			};
			$r = f;
			return $r;
		}(this)));
	}
	,__class__: unbox.framework.view.components.IndexPanel
};
unbox.framework.view.components.Scrollbar = function(parent,data) {
	this.parent = parent;
	this.data = data;
	this.hasInit = false;
	this._current = 1;
	this._steps = 1;
	this.barColor = 11411234;
	this.barColorAlpha = 1;
	this.autoHideThumb = false;
	this.thumbColor = 16777215;
	this.thumbColorAlpha = 1;
	this.barWidth = 0;
	this.barHeight = 0;
	this.init();
};
$hxClasses["unbox.framework.view.components.Scrollbar"] = unbox.framework.view.components.Scrollbar;
unbox.framework.view.components.Scrollbar.__name__ = ["unbox","framework","view","components","Scrollbar"];
unbox.framework.view.components.Scrollbar.prototype = {
	init: function() {
		this.barWidth = this.data.get_width();
		this.barHeight = this.data.get_height();
		this.barColor = this.data.get_scrollbarColor();
		this.barColorAlpha = this.data.get_scrollbarColorAlpha();
		this.thumbColor = this.data.get_thumbColor();
		this.thumbColorAlpha = this.data.get_thumbColorAlpha();
		this.autoHideThumb = this.data.get_autoHideThumb();
		this.view = new openfl.display.Sprite();
		this.bar = new openfl.display.Sprite();
		this.thumb = new openfl.display.Sprite();
		this.view.addChild(this.bar);
		this.view.addChild(this.thumb);
		this.parent.addChild(this.view);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.isVerticalScrolling = this.barHeight > this.barWidth;
		this.hasInit = true;
		this.draw();
		this.update();
	}
	,draw: function() {
		if(this._steps < 1) this._steps = 1; else this._steps = this._steps;
		this.bar.get_graphics().clear();
		this.bar.get_graphics().beginFill(this.barColor,this.barColorAlpha);
		this.bar.get_graphics().drawRect(0,0,this.barWidth,this.barHeight);
		this.bar.get_graphics().endFill();
		if(this.isVerticalScrolling) this.thumbWidth = this.barWidth; else this.thumbWidth = this.barWidth / this._steps;
		if(this.isVerticalScrolling) this.thumbHeight = this.barHeight / this._steps; else this.thumbHeight = this.barHeight;
		this.thumb.get_graphics().clear();
		this.thumb.get_graphics().beginFill(this.thumbColor,this.thumbColorAlpha);
		this.thumb.get_graphics().drawRect(0,0,this.thumbWidth,this.thumbHeight);
		this.thumb.get_graphics().endFill();
	}
	,update: function() {
		var _g = this;
		if(this._current > this._steps) this._current = this._steps; else this._current = this._current;
		if(this._current < 1) this._current = 1; else this._current = this._current;
		var nY;
		if(this.isVerticalScrolling) nY = this.thumbHeight * (this._current - 1); else nY = 0;
		var nX;
		if(this.isVerticalScrolling) nX = 0; else nX = this.thumbWidth * (this._current - 1);
		motion.Actuate.stop(this.thumb);
		motion.Actuate.tween(this.thumb,0.25,{ x : nX, y : nY, alpha : 1},true).onComplete((function($this) {
			var $r;
			var f = function() {
				if(_g.autoHideThumb) motion.Actuate.tween(_g.thumb,1,{ alpha : 0}).delay(0.5);
			};
			$r = f;
			return $r;
		}(this)));
	}
	,set_steps: function(value) {
		this._steps = value;
		if(this.hasInit) {
			this.draw();
			this.update();
		}
		return this._steps;
	}
	,set_current: function(value) {
		this._current = value;
		if(this.hasInit) this.update();
		return this._current;
	}
	,__class__: unbox.framework.view.components.Scrollbar
	,__properties__: {set_current:"set_current",set_steps:"set_steps"}
};
unbox.framework.view.components.SearchBox = function(parent,data,mcLibrary) {
	this.parent = parent;
	this.data = data;
	this.responder = new unbox.framework.responder.SearchResponder();
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.SearchBox"] = unbox.framework.view.components.SearchBox;
unbox.framework.view.components.SearchBox.__name__ = ["unbox","framework","view","components","SearchBox"];
unbox.framework.view.components.SearchBox.prototype = {
	init: function(mcLibrary) {
		this.view = js.Boot.__cast(mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.parent.addChild(this.view);
		var btnView;
		btnView = js.Boot.__cast(this.view.getChildByName("btn_0") , openfl.display.Sprite);
		this.button = new unbox.framework.view.button.GenericButton(btnView.get_x(),btnView.get_y(),btnView,"button_0");
		this.button.addHandlers($bind(this,this.searchHandler));
		this.view.removeChild(btnView);
		this.button.add(this.view);
		this.isFocusSearch = false;
		this.loadAnim = js.Boot.__cast(this.view.getChildByName("loaderIcon") , openfl.display.MovieClip);
		this.loadAnim.set_visible(false);
		this.loadAnim.stop();
		motion.Actuate.timer(0.1).onComplete($bind(this,this.insertTexts));
	}
	,insertTexts: function() {
		this.searchText = new openfl.text.TextField();
		this.searchText.set_type(openfl.text.TextFieldType.INPUT);
		unbox.framework.utils.ContentParser.parseVars(this.searchText,((function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.get("searchText");
			return $r;
		}(this))).node.resolve("vars"));
		this.searchText.embedFonts = false;
		this.searchText.set_htmlText(this.data.get_loadTitle());
		this.searchText.set_name("searchText");
		this.searchText.multiline = false;
		this.searchText.selectable = true;
		this.view.addChild(this.searchText);
		this.view.stage.set_focus(this.searchText);
		this.view.stage.addEventListener(openfl.events.KeyboardEvent.KEY_UP,$bind(this,this.keyboardHandler));
		this.searchText.addEventListener(openfl.events.FocusEvent.FOCUS_IN,$bind(this,this.searchFocusHandler));
		this.searchText.addEventListener(openfl.events.FocusEvent.FOCUS_OUT,$bind(this,this.searchFocusHandler));
		this.enable(true);
	}
	,keyboardHandler: function(e) {
		if(e.target == this.searchText) {
			var hasMinLength = this.searchText.get_text().length >= this.data.get_minChars();
			var hasChanged = this.searchText.get_text() != this.keyword;
			this.button.setEnabled(hasMinLength && hasChanged);
			this.button.setVisible(hasMinLength && hasChanged);
			if(e.keyCode == 13 && hasMinLength && hasChanged) {
				this.dispatchKeyword();
				this.button.setEnabled(false);
				motion.Actuate.timer(0.025).onComplete(($_=this.searchText,$bind($_,$_.setSelection)),[0,this.searchText.get_text().length]);
			}
			e.stopPropagation();
		}
	}
	,searchFocusHandler: function(e) {
		var _g = e.type;
		switch(_g) {
		case openfl.events.FocusEvent.FOCUS_IN:
			if(this.searchText.get_text() == this.data.get_title()) this.searchText.set_text("");
			this.isFocusSearch = true;
			motion.Actuate.timer(0.05).onComplete(($_=this.searchText,$bind($_,$_.setSelection)),[0,this.searchText.get_text().length]);
			break;
		case openfl.events.FocusEvent.FOCUS_OUT:
			if(this.searchText.get_text().length == 0) {
				this.button.setEnabled(false);
				this.searchText.set_text(this.data.get_title());
			}
			this.isFocusSearch = false;
			this.searchText.setSelection(0,0);
			break;
		}
		e.stopPropagation();
	}
	,searchHandler: function(e) {
		this.loadAnim.set_visible(true);
		this.loadAnim.play();
		this.dispatchKeyword();
	}
	,dispatchKeyword: function() {
		if(this.searchText.get_text() != this.keyword && this.searchText.get_text() != "") {
			this.keyword = this.searchText.get_text();
			this.enable(false);
			haxe.Log.trace("Responder SearchRequest dispatched! keyword : " + this.keyword,{ fileName : "SearchBox.hx", lineNumber : 175, className : "unbox.framework.view.components.SearchBox", methodName : "dispatchKeyword"});
			this.responder.request.dispatch(this.responder.response,this.keyword);
		}
	}
	,reset: function() {
		this.keyword = "";
		this.searchText.set_text(this.data.get_title());
		this.searchText.set_type(openfl.text.TextFieldType.INPUT);
		this.button.setEnabled(false);
	}
	,enable: function(value) {
		haxe.Log.trace("SearchBox.enable > value : " + ("" + value),{ fileName : "SearchBox.hx", lineNumber : 192, className : "unbox.framework.view.components.SearchBox", methodName : "enable"});
		this.button.setVisible(value);
		this.searchText.selectable = value;
		this.loadAnim.set_visible(!value);
		if(value) {
			if(this.searchText.get_text() == this.data.get_loadTitle()) this.searchText.set_text(this.data.get_title());
			this.searchText.set_type(openfl.text.TextFieldType.INPUT);
		} else this.searchText.set_type(openfl.text.TextFieldType.DYNAMIC);
	}
	,show: function() {
		this.view.set_visible(true);
		motion.Actuate.tween(this.view,0.25,{ alpha : 1});
	}
	,hide: function() {
		var _g = this;
		motion.Actuate.tween(this.view,0.25,{ alpha : 0}).onComplete((function($this) {
			var $r;
			var f = function() {
				_g.view.set_visible(false);
			};
			$r = f;
			return $r;
		}(this)));
	}
	,__class__: unbox.framework.view.components.SearchBox
};
unbox.framework.view.components.SearchPanel = function(parent,data,mcLibrary) {
	this.parent = parent;
	this.data = data;
	this.hasInit = false;
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.SearchPanel"] = unbox.framework.view.components.SearchPanel;
unbox.framework.view.components.SearchPanel.__name__ = ["unbox","framework","view","components","SearchPanel"];
unbox.framework.view.components.SearchPanel.__interfaces__ = [unbox.framework.view.components.IPanel];
unbox.framework.view.components.SearchPanel.prototype = {
	init: function(mcLibrary) {
		this.view = js.Boot.__cast(mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.view.set_alpha(0);
		this.view.set_visible(false);
		this.parent.addChild(this.view);
		this.buttons = new Array();
		var _g = 0;
		while(_g < 2) {
			var i = _g++;
			var btnView;
			btnView = js.Boot.__cast(this.view.getChildByName("btn_" + i) , openfl.display.Sprite);
			var button = new unbox.framework.view.button.GenericButton(btnView.get_x(),btnView.get_y(),btnView,"button_" + i);
			button.addHandlers($bind(this,this.onButtonDown));
			this.view.removeChild(btnView);
			button.add(this.view);
			this.buttons.push(button);
		}
		this.list = new unbox.framework.view.components.UIList(this.view,this.data.get_list(),mcLibrary);
		this.list.onChange.add($bind(this,this.onListChange));
		this.list.onClick.add($bind(this,this.onListClick));
		this.scrollbar = new unbox.framework.view.components.Scrollbar(this.view,this.data.get_scrollbar());
		motion.Actuate.timer(0.1).onComplete($bind(this,this.insertTexts));
		this.view.addEventListener(openfl.events.MouseEvent.MOUSE_WHEEL,$bind(this,this.onMouseWheel));
		this.cachedSearchData = new Array();
		this.cachedSearchIndices = new Array();
		this.hasInit = true;
		this.keyword = "";
		this.itemLabel = ((function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.get("itemLabel");
			return $r;
		}(this))).node.resolve("title").get_innerHTML();
	}
	,insertTexts: function() {
		var $it0 = (function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.iterator();
			return $r;
		}(this));
		while( $it0.hasNext() ) {
			var textData = $it0.next();
			var txt = unbox.framework.utils.ContentParser.parseTextField(textData,this.view);
			if(txt.get_name() == "statusTxt") {
				this.status = unbox.framework.utils.ContentParser.removeCDATA(textData.node.resolve("title").get_innerHTML());
				this.statusTxt = txt;
				this.statusTxt.set_autoSize(openfl.text.TextFieldAutoSize.RIGHT);
			}
		}
	}
	,onButtonDown: function(e) {
		var btnIndex = Std.int(e.name.split("_")[1]);
		switch(btnIndex) {
		case 0:
			this.list.backPage();
			break;
		case 1:
			this.list.nextPage();
			break;
		}
	}
	,onMouseWheel: function(e) {
		if(e.delta > 0) this.list.backPage(); else this.list.nextPage();
	}
	,onListChange: function(pList) {
		var isBackEnabled;
		isBackEnabled = (function($this) {
			var $r;
			var a = pList.totalItems;
			$r = a < 0?0 >= 0?false:a > 0:0 >= 0?a > 0:true;
			return $r;
		}(this)) && pList.currentPage != 0;
		var isNextEnabled;
		isNextEnabled = (function($this) {
			var $r;
			var a1 = pList.totalItems;
			$r = a1 < 0?0 >= 0?false:a1 > 0:0 >= 0?a1 > 0:true;
			return $r;
		}(this)) && !(pList.currentPage == pList.totalPages - 1);
		this.buttons[0].setEnabled(isBackEnabled);
		this.buttons[1].setEnabled(isNextEnabled);
		if(this.scrollbar != null) {
			this.scrollbar.set_steps(pList.totalPages);
			this.scrollbar.set_current(pList.currentPage + 1);
		}
		if((function($this) {
			var $r;
			var a2 = pList.totalItems;
			$r = a2 < 0?0 >= 0?false:a2 > 0:0 >= 0?a2 > 0:true;
			return $r;
		}(this))) {
			this.firstItem = this.list.maxItemsPerPage * this.list.currentPage + 1;
			this.lastItem = this.list.maxItemsPerPage * (this.list.currentPage + 1);
			if((function($this) {
				var $r;
				var a3 = $this.lastItem;
				var b = $this.list.totalItems;
				$r = a3 < 0?b >= 0?false:a3 > b:b >= 0?a3 > b:true;
				return $r;
			}(this))) this.lastItem = this.list.totalItems; else this.lastItem = this.lastItem;
			this.totalItems = this.list.totalItems;
			this.statusTxt.set_htmlText(unbox.framework.utils.StringUtils.parseTextVars(this.status,this));
		} else this.statusTxt.set_htmlText("");
	}
	,onListClick: function(pList,listButton) {
		haxe.Log.trace("Button '" + listButton.name + "' clicked!",{ fileName : "SearchPanel.hx", lineNumber : 188, className : "unbox.framework.view.components.SearchPanel", methodName : "onListClick"});
	}
	,show: function() {
		motion.Actuate.tween(this.view,.25,{ alpha : 1}).ease(motion.easing.Quint.get_easeOut());
	}
	,hide: function() {
		motion.Actuate.tween(this.view,.25,{ alpha : 0}).ease(motion.easing.Quint.get_easeOut());
	}
	,onSearchDataReceived: function(data,keyword) {
		this.keyword = keyword;
		this.cachedPageData = data;
		if(this.cachedSearchData.length == 0) {
			var numPages = this.cachedPageData.length;
			var loader = new unbox.framework.services.LoadService();
			var _g = 0;
			while(_g < numPages) {
				var i = _g++;
				var textData;
				textData = js.Boot.__cast((function($this) {
					var $r;
					var this1 = data[i].get_assets();
					$r = this1.get("contentXML");
					return $r;
				}(this)) , haxe.xml.Fast);
				loader.add((function($this) {
					var $r;
					var this2 = data[i].get_contentUrls();
					$r = this2.get("contentXML");
					return $r;
				}(this)),"Module " + data[i].get_moduleIndex() + ", Page " + data[i].get_localIndex());
				this.cachedSearchIndices.push("Module " + data[i].get_moduleIndex() + ", Page " + data[i].get_localIndex());
			}
			this.loadStart = openfl.Lib.getTimer();
			loader.start($bind(this,this.onSearchDataFetched));
		} else this.search();
	}
	,onSearchDataFetched: function(data) {
		haxe.Log.trace("SearchData fetched! Load time : " + ("" + (openfl.Lib.getTimer() - this.loadStart)) + " miliseconds.",{ fileName : "SearchPanel.hx", lineNumber : 244, className : "unbox.framework.view.components.SearchPanel", methodName : "onSearchDataFetched"});
		var count = 0;
		var _g1 = 0;
		var _g = this.cachedSearchIndices.length;
		while(_g1 < _g) {
			var i = _g1++;
			var rawData = Xml.parse(data.get(this.cachedSearchIndices[i]));
			var textData = new haxe.xml.Fast(rawData.firstElement());
			var $it0 = textData.nodes.resolve("content").iterator();
			while( $it0.hasNext() ) {
				var contentData = $it0.next();
				if(contentData.att.resolve("type") == "text") {
					var rawText = "";
					var $it1 = contentData.nodes.resolve("content").iterator();
					while( $it1.hasNext() ) {
						var textContent = $it1.next();
						var tempText = unbox.framework.utils.ContentParser.removeCDATA(textContent.node.resolve("body").get_innerHTML());
						tempText = unbox.framework.utils.StringUtils.stripTags(tempText);
						rawText += " " + tempText;
					}
					this.cachedSearchData.push(rawText);
				}
			}
		}
		this.search();
	}
	,search: function() {
		this.list.destroy();
		if(this.keyword != "" && this.cachedSearchData.length > 0) {
			var _g1 = 0;
			var _g = this.cachedSearchData.length;
			while(_g1 < _g) {
				var i = _g1++;
				var strIndex = this.cachedSearchData[i].toLowerCase().indexOf(this.keyword,0);
				if(strIndex > -1) {
					this.summary = this.parseSummary(this.cachedSearchData[i],this.keyword);
					if(this.summary.length > 3) this.insert(this.cachedPageData[i]);
				}
			}
			this.list.update();
			this.dashboard.changePanel(3);
			this.dashboard.getSearchBox().enable(true);
		}
	}
	,insert: function(pageData) {
		this.pageModuleIndex = pageData.get_moduleIndex() + 1;
		this.pageLocalIndex = pageData.get_localIndex() + 1;
		this.totalItems = this.list.totalItems + 1;
		this.statusTxt.set_htmlText(unbox.framework.utils.StringUtils.parseTextVars(this.status,this));
		this.list.insert((function($this) {
			var $r;
			var this1 = $this.data.get_texts();
			$r = this1.get("itemLabel");
			return $r;
		}(this)),this);
		this.list.update();
	}
	,parseSummary: function(value,keyword) {
		var kArr = value.split("]]>");
		var kArrLen = kArr.length;
		var tmpStr = "";
		var _g = 0;
		while(_g < kArrLen) {
			var i = _g++;
			var str = unbox.framework.utils.StringUtils.cleanSpecialChars(kArr[i].toLowerCase());
			var foundStr = str.indexOf(unbox.framework.utils.StringUtils.cleanSpecialChars(keyword));
			if(foundStr > -1) {
				var fstInd = 0;
				var lstInd = 0;
				var strLen = this.data.get_summaryLength();
				strLen -= keyword.length;
				lstInd = Std.int(Math.min(foundStr + strLen * .5,str.length));
				strLen -= lstInd - foundStr;
				fstInd = foundStr - strLen;
				while(fstInd >= 0) {
					if(str.charAt(fstInd) == " " || str.charAt(fstInd) == ".") {
						fstInd++;
						break;
					}
					fstInd--;
				}
				while(lstInd <= str.length) {
					if(str.charAt(lstInd) == " " || str.charAt(lstInd) == ".") break;
					lstInd++;
				}
				if(fstInd < 0) fstInd = 0;
				tmpStr = kArr[i].substring(fstInd,lstInd);
				if(fstInd > 0) tmpStr = "..." + tmpStr;
				if(lstInd < str.length) tmpStr += "...";
				break;
			}
		}
		return tmpStr;
	}
	,__class__: unbox.framework.view.components.SearchPanel
};
unbox.framework.view.components.UIList = function(parent,data,mcLibrary) {
	this.parent = parent;
	this.data = data;
	this.onChange = new msignal.Signal1();
	this.onClick = new msignal.Signal2();
	this.buttons = new Array();
	this.init(mcLibrary);
};
$hxClasses["unbox.framework.view.components.UIList"] = unbox.framework.view.components.UIList;
unbox.framework.view.components.UIList.__name__ = ["unbox","framework","view","components","UIList"];
unbox.framework.view.components.UIList.prototype = {
	init: function(mcLibrary) {
		this.currentPage = 0;
		this.totalPages = 0;
		this.totalItems = 0;
		this.maxWidth = this.data.get_width();
		this.maxHeight = this.data.get_height();
		this.maxItemsPerPage = this.data.get_columns() * this.data.get_rows();
		this.rowHeight = this.maxHeight / this.data.get_rows();
		this.columnWidth = this.maxWidth / this.data.get_columns();
		this.buttonPrefix = "btn";
		this.mcLibrary = mcLibrary;
		this.view = new openfl.display.Sprite();
		this.view.set_name("uiListView");
		this.view.set_x(this.data.get_x());
		this.view.set_y(this.data.get_y());
		this.parent.addChild(this.view);
		this.view.set_scrollRect(new openfl.geom.Rectangle(0,0,this.maxWidth,this.maxHeight));
		this.scrollRect = this.view.get_scrollRect().clone();
		this.onChange.dispatch(this);
	}
	,nextPage: function() {
		if((function($this) {
			var $r;
			var a = $this.currentPage;
			var b = $this.totalPages - 1;
			$r = b < 0?a >= 0?false:b > a:a >= 0?b > a:true;
			return $r;
		}(this))) this.currentPage++;
		this.loadPage();
	}
	,backPage: function() {
		if((function($this) {
			var $r;
			var a = $this.currentPage;
			$r = a < 0?0 >= 0?false:a > 0:0 >= 0?a > 0:true;
			return $r;
		}(this))) this.currentPage--;
		this.loadPage();
	}
	,gotoPage: function(index) {
		if((function($this) {
			var $r;
			var b = $this.totalPages;
			$r = index < 0?b >= 0?false:index >= b:b >= 0?index >= b:true;
			return $r;
		}(this))) index = this.totalPages - 1;
		if(0 < 0?index >= 0?false:0 > index:index >= 0?0 > index:true) index = 0;
		this.currentPage = index;
		this.loadPage();
	}
	,loadPage: function() {
		var _g = this;
		var nY;
		nY = (function($this) {
			var $r;
			var int = $this.currentPage;
			$r = int < 0?4294967296.0 + int:int;
			return $r;
		}(this)) * this.maxHeight;
		motion.Actuate.tween(this.scrollRect,.5,{ x : 0, y : nY}).ease(motion.easing.Quint.get_easeOut()).onUpdate((function($this) {
			var $r;
			var f = function() {
				_g.view.set_scrollRect(_g.scrollRect);
			};
			$r = f;
			return $r;
		}(this)));
		this.onChange.dispatch(this);
	}
	,insert: function(label,context) {
		var value = this.buttons.length;
		var contains = this.view.getChildByName(this.buttonPrefix + "_" + value) != null;
		if(!contains && label != null) {
			var btView = null;
			btView = js.Boot.__cast(this.mcLibrary.getMovieClip(this.data.viewClass) , openfl.display.Sprite);
			var btn = new unbox.framework.view.button.GenericButton(0,0,btView,this.buttonPrefix + "_" + value);
			var phTxt;
			phTxt = js.Boot.__cast(btView.getChildByName("text") , openfl.text.TextField);
			btView.removeChild(phTxt);
			var txt = unbox.framework.utils.ContentParser.parseTextField(label,btn.get_view(),context);
			txt.set_x(phTxt.get_x());
			txt.set_y(phTxt.get_y());
			txt.set_width(phTxt.get_width());
			txt.set_height(phTxt.get_height());
			btn.set_label(txt);
			btn.get_view().set_alpha(0);
			btn.addHandlers($bind(this,this.onBtnClick));
			this.buttons.push(btn);
			this.totalItems = this.buttons.length;
			this.totalPages = Math.ceil(this.totalItems / this.maxItemsPerPage);
			var index = this.buttons.length - 1;
			var colId = index % this.data.get_columns();
			var rowId = Std.int(index / this.data.get_columns());
			var sp = (this.maxHeight - this.data.get_rows() * btn.get_view().get_height()) / (2 * this.data.get_rows());
			var nX = colId * this.columnWidth;
			var nY = rowId * this.rowHeight + sp;
			if(this.data.get_columns() == 1) {
				nX = 0;
				nY = index * this.rowHeight;
			}
			btn.get_view().set_x(nX);
			btn.get_view().set_y(nY);
			btn.add(this.view);
			this.onChange.dispatch(this);
		}
	}
	,onBtnClick: function(e) {
		haxe.Log.trace("list button '" + Std.string(e.name) + "' has been clicked!",{ fileName : "UIList.hx", lineNumber : 201, className : "unbox.framework.view.components.UIList", methodName : "onBtnClick"});
		this.onClick.dispatch(this,e);
	}
	,remove: function(value) {
		var btn = this.buttons[value];
		this.view.removeChild(btn.get_view());
		this.buttons.splice(value,1);
		this.onChange.dispatch(this);
	}
	,update: function(value) {
		if(value == null) value = 0;
		this.totalItems = this.buttons.length;
		this.totalPages = Math.ceil(this.totalItems / this.maxItemsPerPage);
		if(value < 0) value = 0;
		if((function($this) {
			var $r;
			var a = $this.totalItems;
			$r = a < 0?0 >= 0?false:a > 0:0 >= 0?a > 0:true;
			return $r;
		}(this))) {
			var _g1 = value;
			var _g = this.buttons.length;
			while(_g1 < _g) {
				var i = _g1++;
				var btn = this.buttons[i];
				var colId = i % this.data.get_columns();
				var rowId = Std.int(i / this.data.get_columns());
				var sp = (this.maxHeight - this.data.get_rows() * btn.get_view().get_height()) / (2 * this.data.get_rows());
				var nX = colId * this.columnWidth;
				var nY = rowId * this.rowHeight + sp;
				if(this.data.get_columns() == 1) {
					nX = 0;
					nY = i * this.rowHeight + sp;
				}
				motion.Actuate.tween(btn.get_view(),.25,{ x : nX, y : nY, alpha : 1}).delay((i - value) * .01).ease(motion.easing.Quint.get_easeOut());
			}
		}
		this.gotoPage(this.currentPage);
	}
	,destroy: function() {
		var count = this.buttons.length - 1;
		while(count > -1) {
			this.remove(count);
			count--;
		}
		this.totalItems = 0;
		this.totalPages = 1;
		this.currentPage = 0;
		this.buttons = new Array();
		this.onChange.dispatch(this);
	}
	,getButton: function(index) {
		return this.buttons[index];
	}
	,__class__: unbox.framework.view.components.UIList
};
function $iterator(o) { if( o instanceof Array ) return function() { return HxOverrides.iter(o); }; return typeof(o.iterator) == 'function' ? $bind(o,o.iterator) : o.iterator; }
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
if(Array.prototype.indexOf) HxOverrides.indexOf = function(a,o,i) {
	return Array.prototype.indexOf.call(a,o,i);
};
Math.NaN = Number.NaN;
Math.NEGATIVE_INFINITY = Number.NEGATIVE_INFINITY;
Math.POSITIVE_INFINITY = Number.POSITIVE_INFINITY;
$hxClasses.Math = Math;
Math.isFinite = function(i) {
	return isFinite(i);
};
Math.isNaN = function(i1) {
	return isNaN(i1);
};
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = ["String"];
$hxClasses.Array = Array;
Array.__name__ = ["Array"];
Date.prototype.__class__ = $hxClasses.Date = Date;
Date.__name__ = ["Date"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = $hxClasses.Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
Xml.Element = "element";
Xml.PCData = "pcdata";
Xml.CData = "cdata";
Xml.Comment = "comment";
Xml.DocType = "doctype";
Xml.ProcessingInstruction = "processingInstruction";
Xml.Document = "document";
msignal.SlotList.NIL = new msignal.SlotList(null,null);
if(window.createjs != null) createjs.Sound.alternateExtensions = ["ogg","mp3","wav"];
ApplicationMain.images = new haxe.ds.StringMap();
ApplicationMain.urlLoaders = new haxe.ds.StringMap();
ApplicationMain.assetsLoaded = 0;
ApplicationMain.total = 0;
openfl.display.DisplayObject.__instanceCount = 0;
openfl.display.DisplayObject.__worldRenderDirty = 0;
openfl.display.DisplayObject.__worldTransformDirty = 0;
haxe.Serializer.USE_CACHE = false;
haxe.Serializer.USE_ENUM_INDEX = false;
haxe.Serializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe.Unserializer.DEFAULT_RESOLVER = Type;
haxe.Unserializer.BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";
haxe.Unserializer.CODES = null;
haxe.ds.ObjectMap.count = 0;
haxe.xml.Parser.escapes = (function($this) {
	var $r;
	var h = new haxe.ds.StringMap();
	h.set("lt","<");
	h.set("gt",">");
	h.set("amp","&");
	h.set("quot","\"");
	h.set("apos","'");
	h.set("nbsp",String.fromCharCode(160));
	$r = h;
	return $r;
}(this));
motion.actuators.SimpleActuator.actuators = new Array();
motion.actuators.SimpleActuator.actuatorsLength = 0;
motion.actuators.SimpleActuator.addedEvent = false;
motion.Actuate.defaultActuator = motion.actuators.SimpleActuator;
motion.Actuate.defaultEase = motion.easing.Expo.get_easeOut();
motion.Actuate.targetLibraries = new haxe.ds.ObjectMap();
openfl.Assets.cache = new openfl.AssetCache();
openfl.Assets.libraries = new haxe.ds.StringMap();
openfl.Assets.dispatcher = new openfl.events.EventDispatcher();
openfl.Assets.initialized = false;
openfl.Lib.__sentWarnings = new haxe.ds.StringMap();
openfl.Lib.__startTime = haxe.Timer.stamp();
openfl.display.BitmapData.__base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
openfl.display.BitmapDataChannel.ALPHA = 8;
openfl.display.BitmapDataChannel.BLUE = 4;
openfl.display.BitmapDataChannel.GREEN = 2;
openfl.display.BitmapDataChannel.RED = 1;
openfl.display.Graphics.TILE_SCALE = 1;
openfl.display.Graphics.TILE_ROTATION = 2;
openfl.display.Graphics.TILE_RGB = 4;
openfl.display.Graphics.TILE_ALPHA = 8;
openfl.display.Graphics.TILE_TRANS_2x2 = 16;
openfl.display.Graphics.TILE_BLEND_NORMAL = 0;
openfl.display.Graphics.TILE_BLEND_ADD = 65536;
openfl.display.Tilesheet.TILE_SCALE = 1;
openfl.display.Tilesheet.TILE_ROTATION = 2;
openfl.display.Tilesheet.TILE_RGB = 4;
openfl.display.Tilesheet.TILE_ALPHA = 8;
openfl.display.Tilesheet.TILE_TRANS_2x2 = 16;
openfl.display.Tilesheet.TILE_BLEND_NORMAL = 0;
openfl.display.Tilesheet.TILE_BLEND_ADD = 65536;
openfl.display.Tilesheet.TILE_BLEND_MULTIPLY = 131072;
openfl.display.Tilesheet.TILE_BLEND_SCREEN = 262144;
openfl.errors.Error.DEFAULT_TO_STRING = "Error";
openfl.events.Event.ACTIVATE = "activate";
openfl.events.Event.ADDED = "added";
openfl.events.Event.ADDED_TO_STAGE = "addedToStage";
openfl.events.Event.CANCEL = "cancel";
openfl.events.Event.CHANGE = "change";
openfl.events.Event.CLOSE = "close";
openfl.events.Event.COMPLETE = "complete";
openfl.events.Event.CONNECT = "connect";
openfl.events.Event.CONTEXT3D_CREATE = "context3DCreate";
openfl.events.Event.DEACTIVATE = "deactivate";
openfl.events.Event.ENTER_FRAME = "enterFrame";
openfl.events.Event.ID3 = "id3";
openfl.events.Event.INIT = "init";
openfl.events.Event.MOUSE_LEAVE = "mouseLeave";
openfl.events.Event.OPEN = "open";
openfl.events.Event.REMOVED = "removed";
openfl.events.Event.REMOVED_FROM_STAGE = "removedFromStage";
openfl.events.Event.RENDER = "render";
openfl.events.Event.RESIZE = "resize";
openfl.events.Event.SCROLL = "scroll";
openfl.events.Event.SELECT = "select";
openfl.events.Event.TAB_CHILDREN_CHANGE = "tabChildrenChange";
openfl.events.Event.TAB_ENABLED_CHANGE = "tabEnabledChange";
openfl.events.Event.TAB_INDEX_CHANGE = "tabIndexChange";
openfl.events.Event.UNLOAD = "unload";
openfl.events.Event.SOUND_COMPLETE = "soundComplete";
openfl.events.TextEvent.LINK = "link";
openfl.events.TextEvent.TEXT_INPUT = "textInput";
openfl.events.ErrorEvent.ERROR = "error";
openfl.events.FocusEvent.FOCUS_IN = "focusIn";
openfl.events.FocusEvent.FOCUS_OUT = "focusOut";
openfl.events.FocusEvent.KEY_FOCUS_CHANGE = "keyFocusChange";
openfl.events.FocusEvent.MOUSE_FOCUS_CHANGE = "mouseFocusChange";
openfl.events.HTTPStatusEvent.HTTP_RESPONSE_STATUS = "httpResponseStatus";
openfl.events.HTTPStatusEvent.HTTP_STATUS = "httpStatus";
openfl.events.IOErrorEvent.IO_ERROR = "ioError";
openfl.events.KeyboardEvent.KEY_DOWN = "keyDown";
openfl.events.KeyboardEvent.KEY_UP = "keyUp";
openfl.events.MouseEvent.CLICK = "click";
openfl.events.MouseEvent.DOUBLE_CLICK = "doubleClick";
openfl.events.MouseEvent.MIDDLE_CLICK = "middleClick";
openfl.events.MouseEvent.MIDDLE_MOUSE_DOWN = "middleMouseDown";
openfl.events.MouseEvent.MIDDLE_MOUSE_UP = "middleMouseUp";
openfl.events.MouseEvent.MOUSE_DOWN = "mouseDown";
openfl.events.MouseEvent.MOUSE_MOVE = "mouseMove";
openfl.events.MouseEvent.MOUSE_OUT = "mouseOut";
openfl.events.MouseEvent.MOUSE_OVER = "mouseOver";
openfl.events.MouseEvent.MOUSE_UP = "mouseUp";
openfl.events.MouseEvent.MOUSE_WHEEL = "mouseWheel";
openfl.events.MouseEvent.RIGHT_CLICK = "rightClick";
openfl.events.MouseEvent.RIGHT_MOUSE_DOWN = "rightMouseDown";
openfl.events.MouseEvent.RIGHT_MOUSE_UP = "rightMouseUp";
openfl.events.MouseEvent.ROLL_OUT = "rollOut";
openfl.events.MouseEvent.ROLL_OVER = "rollOver";
openfl.events.ProgressEvent.PROGRESS = "progress";
openfl.events.ProgressEvent.SOCKET_DATA = "socketData";
openfl.events.SecurityErrorEvent.SECURITY_ERROR = "securityError";
openfl.events.TimerEvent.TIMER = "timer";
openfl.events.TimerEvent.TIMER_COMPLETE = "timerComplete";
openfl.events.TouchEvent.TOUCH_BEGIN = "touchBegin";
openfl.events.TouchEvent.TOUCH_END = "touchEnd";
openfl.events.TouchEvent.TOUCH_MOVE = "touchMove";
openfl.events.TouchEvent.TOUCH_OUT = "touchOut";
openfl.events.TouchEvent.TOUCH_OVER = "touchOver";
openfl.events.TouchEvent.TOUCH_ROLL_OUT = "touchRollOut";
openfl.events.TouchEvent.TOUCH_ROLL_OVER = "touchRollOver";
openfl.events.TouchEvent.TOUCH_TAP = "touchTap";
openfl.external.ExternalInterface.available = true;
openfl.external.ExternalInterface.marshallExceptions = false;
openfl.filters.BitmapFilterQuality.HIGH = 3;
openfl.filters.BitmapFilterQuality.MEDIUM = 2;
openfl.filters.BitmapFilterQuality.LOW = 1;
openfl.geom.Matrix.__identity = new openfl.geom.Matrix();
openfl.media.Sound.__registeredSounds = new haxe.ds.StringMap();
openfl.net.URLRequestMethod.DELETE = "DELETE";
openfl.net.URLRequestMethod.GET = "GET";
openfl.net.URLRequestMethod.HEAD = "HEAD";
openfl.net.URLRequestMethod.OPTIONS = "OPTIONS";
openfl.net.URLRequestMethod.POST = "POST";
openfl.net.URLRequestMethod.PUT = "PUT";
openfl.system.ApplicationDomain.currentDomain = new openfl.system.ApplicationDomain(null);
openfl.system.SecurityDomain.currentDomain = new openfl.system.SecurityDomain();
openfl.ui.Keyboard.NUMBER_0 = 48;
openfl.ui.Keyboard.NUMBER_1 = 49;
openfl.ui.Keyboard.NUMBER_2 = 50;
openfl.ui.Keyboard.NUMBER_3 = 51;
openfl.ui.Keyboard.NUMBER_4 = 52;
openfl.ui.Keyboard.NUMBER_5 = 53;
openfl.ui.Keyboard.NUMBER_6 = 54;
openfl.ui.Keyboard.NUMBER_7 = 55;
openfl.ui.Keyboard.NUMBER_8 = 56;
openfl.ui.Keyboard.NUMBER_9 = 57;
openfl.ui.Keyboard.A = 65;
openfl.ui.Keyboard.B = 66;
openfl.ui.Keyboard.C = 67;
openfl.ui.Keyboard.D = 68;
openfl.ui.Keyboard.E = 69;
openfl.ui.Keyboard.F = 70;
openfl.ui.Keyboard.G = 71;
openfl.ui.Keyboard.H = 72;
openfl.ui.Keyboard.I = 73;
openfl.ui.Keyboard.J = 74;
openfl.ui.Keyboard.K = 75;
openfl.ui.Keyboard.L = 76;
openfl.ui.Keyboard.M = 77;
openfl.ui.Keyboard.N = 78;
openfl.ui.Keyboard.O = 79;
openfl.ui.Keyboard.P = 80;
openfl.ui.Keyboard.Q = 81;
openfl.ui.Keyboard.R = 82;
openfl.ui.Keyboard.S = 83;
openfl.ui.Keyboard.T = 84;
openfl.ui.Keyboard.U = 85;
openfl.ui.Keyboard.V = 86;
openfl.ui.Keyboard.W = 87;
openfl.ui.Keyboard.X = 88;
openfl.ui.Keyboard.Y = 89;
openfl.ui.Keyboard.Z = 90;
openfl.ui.Keyboard.NUMPAD_0 = 96;
openfl.ui.Keyboard.NUMPAD_1 = 97;
openfl.ui.Keyboard.NUMPAD_2 = 98;
openfl.ui.Keyboard.NUMPAD_3 = 99;
openfl.ui.Keyboard.NUMPAD_4 = 100;
openfl.ui.Keyboard.NUMPAD_5 = 101;
openfl.ui.Keyboard.NUMPAD_6 = 102;
openfl.ui.Keyboard.NUMPAD_7 = 103;
openfl.ui.Keyboard.NUMPAD_8 = 104;
openfl.ui.Keyboard.NUMPAD_9 = 105;
openfl.ui.Keyboard.NUMPAD_MULTIPLY = 106;
openfl.ui.Keyboard.NUMPAD_ADD = 107;
openfl.ui.Keyboard.NUMPAD_ENTER = 108;
openfl.ui.Keyboard.NUMPAD_SUBTRACT = 109;
openfl.ui.Keyboard.NUMPAD_DECIMAL = 110;
openfl.ui.Keyboard.NUMPAD_DIVIDE = 111;
openfl.ui.Keyboard.F1 = 112;
openfl.ui.Keyboard.F2 = 113;
openfl.ui.Keyboard.F3 = 114;
openfl.ui.Keyboard.F4 = 115;
openfl.ui.Keyboard.F5 = 116;
openfl.ui.Keyboard.F6 = 117;
openfl.ui.Keyboard.F7 = 118;
openfl.ui.Keyboard.F8 = 119;
openfl.ui.Keyboard.F9 = 120;
openfl.ui.Keyboard.F10 = 121;
openfl.ui.Keyboard.F11 = 122;
openfl.ui.Keyboard.F12 = 123;
openfl.ui.Keyboard.F13 = 124;
openfl.ui.Keyboard.F14 = 125;
openfl.ui.Keyboard.F15 = 126;
openfl.ui.Keyboard.BACKSPACE = 8;
openfl.ui.Keyboard.TAB = 9;
openfl.ui.Keyboard.ALTERNATE = 18;
openfl.ui.Keyboard.ENTER = 13;
openfl.ui.Keyboard.COMMAND = 15;
openfl.ui.Keyboard.SHIFT = 16;
openfl.ui.Keyboard.CONTROL = 17;
openfl.ui.Keyboard.CAPS_LOCK = 20;
openfl.ui.Keyboard.NUMPAD = 21;
openfl.ui.Keyboard.ESCAPE = 27;
openfl.ui.Keyboard.SPACE = 32;
openfl.ui.Keyboard.PAGE_UP = 33;
openfl.ui.Keyboard.PAGE_DOWN = 34;
openfl.ui.Keyboard.END = 35;
openfl.ui.Keyboard.HOME = 36;
openfl.ui.Keyboard.LEFT = 37;
openfl.ui.Keyboard.RIGHT = 39;
openfl.ui.Keyboard.UP = 38;
openfl.ui.Keyboard.DOWN = 40;
openfl.ui.Keyboard.INSERT = 45;
openfl.ui.Keyboard.DELETE = 46;
openfl.ui.Keyboard.NUMLOCK = 144;
openfl.ui.Keyboard.BREAK = 19;
openfl.ui.Keyboard.SEMICOLON = 186;
openfl.ui.Keyboard.EQUAL = 187;
openfl.ui.Keyboard.COMMA = 188;
openfl.ui.Keyboard.MINUS = 189;
openfl.ui.Keyboard.PERIOD = 190;
openfl.ui.Keyboard.SLASH = 191;
openfl.ui.Keyboard.BACKQUOTE = 192;
openfl.ui.Keyboard.LEFTBRACKET = 219;
openfl.ui.Keyboard.BACKSLASH = 220;
openfl.ui.Keyboard.RIGHTBRACKET = 221;
openfl.ui.Keyboard.QUOTE = 222;
openfl.ui.Keyboard.DOM_VK_CANCEL = 3;
openfl.ui.Keyboard.DOM_VK_HELP = 6;
openfl.ui.Keyboard.DOM_VK_BACK_SPACE = 8;
openfl.ui.Keyboard.DOM_VK_TAB = 9;
openfl.ui.Keyboard.DOM_VK_CLEAR = 12;
openfl.ui.Keyboard.DOM_VK_RETURN = 13;
openfl.ui.Keyboard.DOM_VK_ENTER = 14;
openfl.ui.Keyboard.DOM_VK_SHIFT = 16;
openfl.ui.Keyboard.DOM_VK_CONTROL = 17;
openfl.ui.Keyboard.DOM_VK_ALT = 18;
openfl.ui.Keyboard.DOM_VK_PAUSE = 19;
openfl.ui.Keyboard.DOM_VK_CAPS_LOCK = 20;
openfl.ui.Keyboard.DOM_VK_ESCAPE = 27;
openfl.ui.Keyboard.DOM_VK_SPACE = 32;
openfl.ui.Keyboard.DOM_VK_PAGE_UP = 33;
openfl.ui.Keyboard.DOM_VK_PAGE_DOWN = 34;
openfl.ui.Keyboard.DOM_VK_END = 35;
openfl.ui.Keyboard.DOM_VK_HOME = 36;
openfl.ui.Keyboard.DOM_VK_LEFT = 37;
openfl.ui.Keyboard.DOM_VK_UP = 38;
openfl.ui.Keyboard.DOM_VK_RIGHT = 39;
openfl.ui.Keyboard.DOM_VK_DOWN = 40;
openfl.ui.Keyboard.DOM_VK_PRINTSCREEN = 44;
openfl.ui.Keyboard.DOM_VK_INSERT = 45;
openfl.ui.Keyboard.DOM_VK_DELETE = 46;
openfl.ui.Keyboard.DOM_VK_0 = 48;
openfl.ui.Keyboard.DOM_VK_1 = 49;
openfl.ui.Keyboard.DOM_VK_2 = 50;
openfl.ui.Keyboard.DOM_VK_3 = 51;
openfl.ui.Keyboard.DOM_VK_4 = 52;
openfl.ui.Keyboard.DOM_VK_5 = 53;
openfl.ui.Keyboard.DOM_VK_6 = 54;
openfl.ui.Keyboard.DOM_VK_7 = 55;
openfl.ui.Keyboard.DOM_VK_8 = 56;
openfl.ui.Keyboard.DOM_VK_9 = 57;
openfl.ui.Keyboard.DOM_VK_SEMICOLON = 59;
openfl.ui.Keyboard.DOM_VK_EQUALS = 61;
openfl.ui.Keyboard.DOM_VK_A = 65;
openfl.ui.Keyboard.DOM_VK_B = 66;
openfl.ui.Keyboard.DOM_VK_C = 67;
openfl.ui.Keyboard.DOM_VK_D = 68;
openfl.ui.Keyboard.DOM_VK_E = 69;
openfl.ui.Keyboard.DOM_VK_F = 70;
openfl.ui.Keyboard.DOM_VK_G = 71;
openfl.ui.Keyboard.DOM_VK_H = 72;
openfl.ui.Keyboard.DOM_VK_I = 73;
openfl.ui.Keyboard.DOM_VK_J = 74;
openfl.ui.Keyboard.DOM_VK_K = 75;
openfl.ui.Keyboard.DOM_VK_L = 76;
openfl.ui.Keyboard.DOM_VK_M = 77;
openfl.ui.Keyboard.DOM_VK_N = 78;
openfl.ui.Keyboard.DOM_VK_O = 79;
openfl.ui.Keyboard.DOM_VK_P = 80;
openfl.ui.Keyboard.DOM_VK_Q = 81;
openfl.ui.Keyboard.DOM_VK_R = 82;
openfl.ui.Keyboard.DOM_VK_S = 83;
openfl.ui.Keyboard.DOM_VK_T = 84;
openfl.ui.Keyboard.DOM_VK_U = 85;
openfl.ui.Keyboard.DOM_VK_V = 86;
openfl.ui.Keyboard.DOM_VK_W = 87;
openfl.ui.Keyboard.DOM_VK_X = 88;
openfl.ui.Keyboard.DOM_VK_Y = 89;
openfl.ui.Keyboard.DOM_VK_Z = 90;
openfl.ui.Keyboard.DOM_VK_CONTEXT_MENU = 93;
openfl.ui.Keyboard.DOM_VK_NUMPAD0 = 96;
openfl.ui.Keyboard.DOM_VK_NUMPAD1 = 97;
openfl.ui.Keyboard.DOM_VK_NUMPAD2 = 98;
openfl.ui.Keyboard.DOM_VK_NUMPAD3 = 99;
openfl.ui.Keyboard.DOM_VK_NUMPAD4 = 100;
openfl.ui.Keyboard.DOM_VK_NUMPAD5 = 101;
openfl.ui.Keyboard.DOM_VK_NUMPAD6 = 102;
openfl.ui.Keyboard.DOM_VK_NUMPAD7 = 103;
openfl.ui.Keyboard.DOM_VK_NUMPAD8 = 104;
openfl.ui.Keyboard.DOM_VK_NUMPAD9 = 105;
openfl.ui.Keyboard.DOM_VK_MULTIPLY = 106;
openfl.ui.Keyboard.DOM_VK_ADD = 107;
openfl.ui.Keyboard.DOM_VK_SEPARATOR = 108;
openfl.ui.Keyboard.DOM_VK_SUBTRACT = 109;
openfl.ui.Keyboard.DOM_VK_DECIMAL = 110;
openfl.ui.Keyboard.DOM_VK_DIVIDE = 111;
openfl.ui.Keyboard.DOM_VK_F1 = 112;
openfl.ui.Keyboard.DOM_VK_F2 = 113;
openfl.ui.Keyboard.DOM_VK_F3 = 114;
openfl.ui.Keyboard.DOM_VK_F4 = 115;
openfl.ui.Keyboard.DOM_VK_F5 = 116;
openfl.ui.Keyboard.DOM_VK_F6 = 117;
openfl.ui.Keyboard.DOM_VK_F7 = 118;
openfl.ui.Keyboard.DOM_VK_F8 = 119;
openfl.ui.Keyboard.DOM_VK_F9 = 120;
openfl.ui.Keyboard.DOM_VK_F10 = 121;
openfl.ui.Keyboard.DOM_VK_F11 = 122;
openfl.ui.Keyboard.DOM_VK_F12 = 123;
openfl.ui.Keyboard.DOM_VK_F13 = 124;
openfl.ui.Keyboard.DOM_VK_F14 = 125;
openfl.ui.Keyboard.DOM_VK_F15 = 126;
openfl.ui.Keyboard.DOM_VK_F16 = 127;
openfl.ui.Keyboard.DOM_VK_F17 = 128;
openfl.ui.Keyboard.DOM_VK_F18 = 129;
openfl.ui.Keyboard.DOM_VK_F19 = 130;
openfl.ui.Keyboard.DOM_VK_F20 = 131;
openfl.ui.Keyboard.DOM_VK_F21 = 132;
openfl.ui.Keyboard.DOM_VK_F22 = 133;
openfl.ui.Keyboard.DOM_VK_F23 = 134;
openfl.ui.Keyboard.DOM_VK_F24 = 135;
openfl.ui.Keyboard.DOM_VK_NUM_LOCK = 144;
openfl.ui.Keyboard.DOM_VK_SCROLL_LOCK = 145;
openfl.ui.Keyboard.DOM_VK_COMMA = 188;
openfl.ui.Keyboard.DOM_VK_PERIOD = 190;
openfl.ui.Keyboard.DOM_VK_SLASH = 191;
openfl.ui.Keyboard.DOM_VK_BACK_QUOTE = 192;
openfl.ui.Keyboard.DOM_VK_OPEN_BRACKET = 219;
openfl.ui.Keyboard.DOM_VK_BACK_SLASH = 220;
openfl.ui.Keyboard.DOM_VK_CLOSE_BRACKET = 221;
openfl.ui.Keyboard.DOM_VK_QUOTE = 222;
openfl.ui.Keyboard.DOM_VK_META = 224;
openfl.ui.Keyboard.DOM_VK_KANA = 21;
openfl.ui.Keyboard.DOM_VK_HANGUL = 21;
openfl.ui.Keyboard.DOM_VK_JUNJA = 23;
openfl.ui.Keyboard.DOM_VK_FINAL = 24;
openfl.ui.Keyboard.DOM_VK_HANJA = 25;
openfl.ui.Keyboard.DOM_VK_KANJI = 25;
openfl.ui.Keyboard.DOM_VK_CONVERT = 28;
openfl.ui.Keyboard.DOM_VK_NONCONVERT = 29;
openfl.ui.Keyboard.DOM_VK_ACEPT = 30;
openfl.ui.Keyboard.DOM_VK_MODECHANGE = 31;
openfl.ui.Keyboard.DOM_VK_SELECT = 41;
openfl.ui.Keyboard.DOM_VK_PRINT = 42;
openfl.ui.Keyboard.DOM_VK_EXECUTE = 43;
openfl.ui.Keyboard.DOM_VK_SLEEP = 95;
openfl.utils.Endian.BIG_ENDIAN = "bigEndian";
openfl.utils.Endian.LITTLE_ENDIAN = "littleEndian";
unbox.framework.model.Status.STATUS_NOT_INITIALIZED = 0;
unbox.framework.model.Status.STATUS_INITIALIZED = 1;
unbox.framework.model.Status.STATUS_COMPLETED = 2;
unbox.framework.model.Status.DELIMITER = "|";
unbox.framework.model.Status.SUBDELIMITER = ",";
unbox.framework.model.vo.EbookData.ENTRY_ABINITIO = "ab-initio";
unbox.framework.model.vo.EbookData.ENTRY_RESUME = "resume";
unbox.framework.model.vo.EbookData.STATUS_PASSED = "passed";
unbox.framework.model.vo.EbookData.STATUS_COMPLETED = "completed";
unbox.framework.model.vo.EbookData.STATUS_FAILED = "failed";
unbox.framework.model.vo.EbookData.STATUS_INCOMPLETE = "incomplete";
unbox.framework.model.vo.EbookData.STATUS_BROWSED = "browsed";
unbox.framework.model.vo.EbookData.STATUS_NOT_ATTEMPTED = "not attempted";
unbox.framework.model.vo.EbookData.MODE_BROWSE = "browse";
unbox.framework.model.vo.EbookData.MODE_NORMAL = "normal";
unbox.framework.model.vo.EbookData.MODE_REVIEW = "review";
unbox.framework.model.vo.EbookData.EXIT_TIMEOUT = "time-out";
unbox.framework.model.vo.EbookData.EXIT_SUSPEND = "suspend";
unbox.framework.model.vo.EbookData.EXIT_LOGOUT = "logout";
unbox.framework.model.vo.ScormParams.PARAM_COMMENTS = "cmi.comments";
unbox.framework.model.vo.ScormParams.PARAM_CREDIT = "cmi.core.credit";
unbox.framework.model.vo.ScormParams.PARAM_ENTRY = "cmi.core.entry";
unbox.framework.model.vo.ScormParams.PARAM_EXIT = "cmi.core.exit";
unbox.framework.model.vo.ScormParams.PARAM_LAUNCHA_DATA = "cmi.launch_data";
unbox.framework.model.vo.ScormParams.PARAM_LESSON_LOCATION = "cmi.core.lesson_location";
unbox.framework.model.vo.ScormParams.PARAM_LESSON_MODE = "cmi.core.lesson_mode";
unbox.framework.model.vo.ScormParams.PARAM_LESSON_STATUS = "cmi.core.lesson_status";
unbox.framework.model.vo.ScormParams.PARAM_SCORE_MAX = "cmi.core.score.max";
unbox.framework.model.vo.ScormParams.PARAM_SCORE_MIN = "cmi.core.score.min";
unbox.framework.model.vo.ScormParams.PARAM_SCORE_RAW = "cmi.core.score.raw";
unbox.framework.model.vo.ScormParams.PARAM_SESSION_TIME = "cmi.core.session_time";
unbox.framework.model.vo.ScormParams.PARAM_STUDENT_ID = "cmi.core.student_id";
unbox.framework.model.vo.ScormParams.PARAM_STUDENT_NAME = "cmi.core.student_name";
unbox.framework.model.vo.ScormParams.PARAM_SUSPEND_DATA = "cmi.suspend_data";
unbox.framework.model.vo.ScormParams.PARAM_TOTAL_TIME = "cmi.core.total_time";
unbox.framework.view.button.ButtonEvent.ACTIVATE = "Activate";
ApplicationMain.main();
})(typeof window != "undefined" ? window : exports);

//# sourceMappingURL=UnboxFramework.js.map